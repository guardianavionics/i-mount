﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UploadFileOnDropBoxServiceStaging
{
    public partial class Service1 : ServiceBase
    {
        private System.Threading.Timer parseDataTimer = null;
        private readonly int TimerInterval = 60000;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            int startin = 60 - DateTime.Now.Second;
            int dueTime = startin * 1000;
            SetProcessingTimer(dueTime);
        }

        protected override void OnStop()
        {
        }

        public void SetProcessingTimer(int dueTime)
        {
            // m_Logger.LogDebug("==============SetProcessingTimer====================");
            // Create the delegate that invokes methods for the timer.
            TimerCallback timerDelegate = new TimerCallback(ProcessItems);

            // Create a timer that signals the delegate to invoke 
            // extracting, its a one click timer
            parseDataTimer = new System.Threading.Timer(timerDelegate, null, dueTime, TimerInterval);
        }

        private void StopProcessingTimer()
        {
            parseDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void StartProcessingTimer(int timerInterval)
        {
            parseDataTimer.Change(timerInterval, timerInterval);
        }

        public void ProcessItems(Object stateInfo)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                //timer.Stop();
                StopProcessingTimer();
                System.Diagnostics.Debugger.Launch();
                str.Append("File Upload Service Started at " + DateTime.Now + Environment.NewLine);

                AddDatalogToAircraft addDatalogToAircraft = new AddDatalogToAircraft();
                addDatalogToAircraft.AddDataLog();

                CreateGoogleMapSnap objMap = new CreateGoogleMapSnap();
                objMap.GenerateSnapShot();

                SendEmailAfterFlightFinish objEmail = new SendEmailAfterFlightFinish();
                objEmail.sendEmail();

                UploadFile uploadFile = new UploadFile();
                uploadFile.UploadFileOnDropBox();

                str.Append("File Upload Service parsing Complete");
                str.Clear();
                StartProcessingTimer(TimerInterval);
            }
            catch (Exception ex)
            {
                SetProcessingTimer(TimerInterval);
            }
        }
    }
}
