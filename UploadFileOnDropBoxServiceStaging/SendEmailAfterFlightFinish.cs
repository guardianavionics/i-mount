﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Script.Serialization;
using GA.DataLayer;
using System.Configuration;
using OfficeOpenXml;
using NLog;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using GA.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using Amazon.S3.Model;

namespace UploadFileOnDropBoxServiceStaging
{
    public class SendEmailAfterFlightFinish
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        public Logger logger = LogManager.GetCurrentClassLogger();
        public StringBuilder strLogger = new StringBuilder();

        public double g_startHobbs = 0.0;
        public double g_endHobbs = 0.0;
        public double g_StartTach = 0.0;
        public double g_endTach = 0.0;
        public string pilotEmails = "";
        public string CoPilotEmails = "";


        public void sendEmail()
        {
            //int hobbsTimePerUnit = Convert.ToInt32(ConfigurationManager.AppSettings["HobbsTimePerUnit"]);
            //logger.Fatal("DropDox Service Call Send Email Function");
            strLogger.Append("DropDox Service Call Send Email Function" + Environment.NewLine);
            var context = new GuardianAvionicsEntities();
            var pilotLogList = context.PilotLogs.Where(p => (p.IsEmailSent != true) && p.DayPIC != "00:00:00" && p.Finished && (p.DropBoxErrorMsg == null || p.DropBoxErrorMsg == "")).ToList();

            //logger.Fatal("PilotLogList Count" + pilotLogList.Count);
            strLogger.Append("PilotLogList Count" + pilotLogList.Count + Environment.NewLine);

            foreach (var pilotLog in pilotLogList)
            {
                //logger.Fatal("Foreachloop start");
                strLogger.Append("Foreachloop start" + Environment.NewLine);

                logger.Fatal("Start Send push notification to pilot and copilot");

                try
                {
                    //Send push notification to pilot and copilot
                    List<int> profileIdList = new List<int>();
                    if (pilotLog.PilotId != null)
                    {
                        profileIdList.Add(pilotLog.PilotId ?? 0);
                    }
                    if (pilotLog.CoPilotId != null)
                    {
                        profileIdList.Add(pilotLog.CoPilotId ?? 0);
                    }
                    if (pilotLog.AircraftProfile.OwnerProfileId != null)
                    {
                        profileIdList.Add(pilotLog.AircraftProfile.OwnerProfileId ?? 0);
                    }
                    string[] tokenIds = new string[] { };

                    if (profileIdList.Count > 0)
                    {
                        tokenIds = context.PushNotifications.Where(p => profileIdList.Contains(p.ProfileId) && p.TokenId != null).Select(s => s.TokenId).Distinct().ToArray();
                        logger.Fatal("tokenIds = " + Newtonsoft.Json.JsonConvert.SerializeObject(tokenIds));
                    }

                    if (tokenIds.Length > 0)
                    {
                        try
                        {
                            string endOfFlightTime = string.Empty;
                            DateTime dt = new DateTime();
                            var query = "select top 1 * from dbo.[" + pilotLog.AircraftId + "] where PilotLogId=" + pilotLog.Id + " order by id desc";
                            DataSet ds = (SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, query));
                            //  var airframedataLog = context.AirframeDatalogs.OrderByDescending(o => o.Id).FirstOrDefault(l => l.PilotLogId == pilotLog.Id);
                            //if (airframedataLog != null)
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    // dt = Convert.ToDateTime(airframedataLog.GPRMCDate);
                                    dt = Convert.ToDateTime(ds.Tables[0].Rows[0]["GPRMCDate"]);
                                    endOfFlightTime = ", end of flight time - " + string.Format("{0:MM/dd/yyyy HH:mm:ss}", dt);
                                }
                            }
                            string pushMessage = "You have taken a flight from Aircraft - " + pilotLog.AircraftProfile.Registration + ", total flight time - " + ConvertMinToOneTenthOFTime(ConvertHHMMToMinutes(pilotLog.DayPIC)) + endOfFlightTime;


                            //PushNotificationIPhone pushNotificationIPhone = new PushNotificationIPhone();
                            //pushNotificationIPhone.Push(tokenIds, pushMessage);
                            IphonePushNotification objPushMessage = new IphonePushNotification();
                            foreach (var id in tokenIds)
                            {
                                objPushMessage.PushToiPhone(id, pushMessage, "");
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Fatal("Error while sending push notification");
                        }
                    }
                    logger.Fatal("End Send push notification to pilot and copilot");
                }
                catch (Exception ex)
                {
                    logger.Fatal("Error on sending push notification " + ex.Message + "INNER EXCEPTION = " + ex.InnerException.Message);
                    pilotLog.IsEmailSent = true;
                    pilotLog.DropBoxErrorMsg = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                    context.SaveChanges();
                    strLogger.Append("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + Environment.NewLine);
                    continue;
                }

                logger.Fatal("Foreachloop start");
                strLogger.Append("Foreachloop start" + Environment.NewLine);

                try
                {

                    var dataLogList = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airframedatalog = context.AirframeDatalogs.Where(a => a.PilotLogId == pilotLog.Id).Select(s => s.DataLog).ToList();

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLog.Id;

                    DataSet dsDataLog = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);
                    logger.Fatal("dsDataLog");
                    if (dsDataLog.Tables.Count > 0)
                    {
                        logger.Fatal("A");
                        if (dsDataLog.Tables[0].Rows.Count > 0)
                        {
                            logger.Fatal("B");
                            var objairframedatalog = dsDataLog.Tables[0].AsEnumerable()
                         .Select(r => r.Field<string>("DataLog"))
                         .ToList();

                            foreach (var airframedata in objairframedatalog)
                            {
                                dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    double gallon = 0.0;
                    // check for null is done in query
                    // ReSharper disable once PossibleInvalidOperationException
                    dataLogList.ForEach(a => gallon += (Convert.ToDouble(string.IsNullOrEmpty(a.FF) ? 0.0 : Convert.ToDouble(a.FF)) / 3600));
                    logger.Fatal("C");
                    strLogger.Append("Owner Email Id = " + pilotLog.AircraftProfile.Profile.EmailId + Environment.NewLine);
                    strLogger.Append("Total Gallons Used =" + gallon);

                    var emailDataForFinishedFlights = context.EmailDataForFinishedFlights.FirstOrDefault(f => f.PilotLogId == pilotLog.Id);
                    if (emailDataForFinishedFlights == null)
                    {
                        g_startHobbs = 0;
                        g_endHobbs = ConvertHHMMToMinutes(pilotLog.DayPIC);
                        g_StartTach = 0;
                        g_endTach = ConvertHHMMToMinutes(pilotLog.DayPIC);
                    }
                    else
                    {
                        var arrEmail = emailDataForFinishedFlights.EmailData.Split(',');
                        g_startHobbs = Convert.ToDouble(arrEmail[0]) + Convert.ToDouble(arrEmail[1]);
                        g_endHobbs = Convert.ToDouble(arrEmail[0]) + Convert.ToDouble(arrEmail[1]) + ConvertHHMMToMinutes(pilotLog.DayPIC);
                        g_StartTach = Convert.ToDouble(arrEmail[2]) + Convert.ToDouble(arrEmail[3]);
                        g_endTach = Convert.ToDouble(arrEmail[2]) + Convert.ToDouble(arrEmail[3]) + ConvertHHMMToMinutes(pilotLog.DayPIC);
                    }
                    logger.Fatal("D");
                    if (pilotLog.PilotId != null)
                    {
                        var emailList = context.UserEmails.Where(p => p.ProfileId == pilotLog.PilotId && p.EmailId != pilotLog.Profile.EmailId).Select(s => s.EmailId).Distinct().ToList();
                        pilotEmails = pilotLog.Profile.EmailId;
                        if (emailList.Count > 0)
                        {
                            pilotEmails = pilotEmails + "," + string.Join(",", emailList);
                        }
                    }
                    else
                    {
                        pilotEmails = "";
                    }

                    if (pilotLog.CoPilotId != null)
                    {
                        var emailList = context.UserEmails.Where(p => p.ProfileId == pilotLog.CoPilotId && p.EmailId != pilotLog.Profile1.EmailId).Select(s => s.EmailId).Distinct().ToList();
                        CoPilotEmails = pilotLog.Profile1.EmailId;
                        if (emailList.Count > 0)
                        {
                            CoPilotEmails = CoPilotEmails + "," + string.Join(",", emailList);
                        }
                    }
                    else
                    {
                        CoPilotEmails = "";
                    }
                    logger.Fatal("E");
                    var obj = new EndOfFlightEmailModel()
                    {
                        PilotLogId = pilotLog.Id.ToString(),

                        PilotName = (pilotLog.PilotId == null) ? "Not Available" : (pilotLog.Profile.UserDetail.FirstName + " " + pilotLog.Profile.UserDetail.LastName),

                        PilotContact = (pilotLog.PilotId == null) ? "Not Available" : pilotLog.Profile.UserDetail.PhoneNumber,

                        CoPilotContactNo = (pilotLog.CoPilotId == null) ? "Not Available" : pilotLog.Profile1.UserDetail.PhoneNumber,

                        CoPilotName = (pilotLog.CoPilotId == null) ? "Not Available" : (pilotLog.Profile1.UserDetail.FirstName + " " + pilotLog.Profile1.UserDetail.LastName),

                        AircraftNnumber = pilotLog.AircraftProfile.Registration,

                        FlightTime = ConvertMinToOneTenthOFTime(ConvertHHMMToMinutes(pilotLog.DayPIC)),

                        FlightCompletedOn = (GetStringOnlyDateUS(pilotLog.Date) ?? string.Empty) + " " + Convert.ToDateTime(pilotLog.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),

                        StartHobbsTime = ConvertMinToOneTenthOFTime(g_startHobbs),

                        EndHobbsTime = ConvertMinToOneTenthOFTime(g_endHobbs),

                        PilotImage = (pilotLog.PilotId == null) ? "" : (string.IsNullOrEmpty(pilotLog.Profile.UserDetail.ImageUrl) ? "" : pilotLog.Profile.UserDetail.ImageUrl),

                        CoPilotImage = (pilotLog.CoPilotId == null) ? "" : (string.IsNullOrEmpty(pilotLog.Profile1.UserDetail.ImageUrl) ? "" : pilotLog.Profile1.UserDetail.ImageUrl),

                        StartTechTime = ConvertMinToOneTenthOFTime(g_StartTach), //TimeSpan.FromMinutes(prevTime.Minutes).ToString(),

                        EndTechTime = ConvertMinToOneTenthOFTime(g_endTach), // TimeSpan.FromMinutes(prevTime.Add(TimeSpan.Parse(pilotLog.DayPIC ?? "00:00:00")).Minutes).ToString(),

                        PilotEmailId = pilotEmails,

                        CoPilotEmailId = CoPilotEmails,

                        OwnerId = pilotLog.AircraftProfile.Profile.EmailId,

                        AircraftImage = string.IsNullOrEmpty(pilotLog.AircraftProfile.ImageUrl) ? "" : pilotLog.AircraftProfile.ImageUrl,

                        AircraftSerialNo = pilotLog.AircraftProfile.AircraftSerialNo,

                        AircraftOwnerName = pilotLog.AircraftProfile.Profile.UserDetail.FirstName + " " + pilotLog.AircraftProfile.Profile.UserDetail.LastName,

                        TotalGallonsused = (Math.Round(gallon, 2)).ToString() + "gph"

                    };

                    strLogger.Append("Obj " + Newtonsoft.Json.JsonConvert.SerializeObject(obj) + Environment.NewLine);
                    logger.Fatal("F");
                    if (!string.IsNullOrEmpty(obj.OwnerId))
                    {
                        strLogger.Append("Owner Name = " + pilotLog.AircraftProfile.Profile.UserDetail.FirstName + Environment.NewLine);
                        logger.Fatal("G");
                        SendEmailServer(obj, obj.OwnerId, pilotLog.AircraftProfile.Profile.UserDetail.FirstName + " " + pilotLog.AircraftProfile.Profile.UserDetail.LastName);
                    }


                    if (!string.IsNullOrEmpty(obj.PilotEmailId) && obj.OwnerId != obj.PilotEmailId)
                    {
                        SendEmailServer(obj, obj.PilotEmailId, obj.PilotName);
                    }

                    if (!string.IsNullOrEmpty(obj.CoPilotEmailId) && obj.CoPilotEmailId != obj.PilotEmailId && obj.CoPilotEmailId != obj.OwnerId)
                    {
                        SendEmailServer(obj, obj.CoPilotEmailId, obj.CoPilotName);
                    }

                    pilotLog.IsEmailSent = true;
                    context.SaveChanges();
                    logger.Fatal(strLogger);
                    //sendTFBOxmlFile(pilotLog.AircraftProfile.Profile.UserDetail.FirstName + " " + pilotLog.AircraftProfile.Profile.UserDetail.LastName, obj.OwnerId, obj.AircraftNnumber, pilotLog.AircraftId, pilotLog.AircraftProfile.ChargeBy, pilotLog.FlightId);
                }
                catch (Exception ex)
                {
                    strLogger.Append("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + Environment.NewLine);
                    logger.Fatal(strLogger);
                    pilotLog.IsEmailSent = true;
                    pilotLog.DropBoxErrorMsg = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                    context.SaveChanges();
                }
            }
        }

        public void sendTFBOxmlFile(string ownerName, string ownerEmailId, string aircraftNNumber, int aircraftId, int? chargeBy, int? flightId)
        {
            decimal quantity = 0;
            var context = new GuardianAvionicsEntities();
            var aircraftSalesDetail = context.AircraftSalesDetails.Where(f => f.AircraftId == aircraftId);
            double subTotal = 0.0;
            double price = 0.0;
            double endtHobbsQty = 0.0;
            double startHobbsQty = 0.0;
            double startTachQty = 0.0;
            double endtTachQty = 0.0;

            int ConvertMinToUnit = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertMinToUnit"]);
            switch (chargeBy)
            {
                case 1:
                    {
                        

                        startHobbsQty = (g_startHobbs / ConvertMinToUnit);
                        startHobbsQty = Math.Round(Convert.ToDouble(startHobbsQty), 1, MidpointRounding.ToEven); // Rounds to even

                        endtHobbsQty = (g_endHobbs / ConvertMinToUnit);
                        endtHobbsQty = Math.Round(Convert.ToDouble(endtHobbsQty), 1, MidpointRounding.ToEven); // Rounds to even

                        quantity = Convert.ToDecimal(endtHobbsQty) - Convert.ToDecimal(startHobbsQty);
                        break;
                    }
                case 2:
                    {
                        

                        startTachQty = (g_StartTach / ConvertMinToUnit);
                        startTachQty = Math.Round(Convert.ToDouble(startTachQty), 1, MidpointRounding.ToEven); // Rounds to even
                        endtTachQty = ((int)g_endTach / ConvertMinToUnit);
                        endtTachQty = Math.Round(Convert.ToDouble(endtTachQty), 1, MidpointRounding.ToEven); // Rounds to even
                        quantity = Convert.ToDecimal(endtTachQty) - Convert.ToDecimal(startTachQty);
                        break;
                    }
            }

            StringWriter stringwriter = new StringWriter();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringwriter);
            xmlTextWriter.Formatting = System.Xml.Formatting.Indented;

            xmlTextWriter.WriteStartElement("TransactionList");
            xmlTextWriter.WriteAttributeString("Version", "1.1");
            xmlTextWriter.WriteStartElement("Transaction");
            xmlTextWriter.WriteAttributeString("ProcessInvoice", "”No”");
            xmlTextWriter.WriteElementString("TransactionType", "I");
            xmlTextWriter.WriteElementString("TransactionState", "Pending");
            xmlTextWriter.WriteElementString("TransactionDate", DateTime.Now.Year.ToString() + ((DateTime.Now.Month > 9) ? (DateTime.Now.Month.ToString()) : ("0" + DateTime.Now.Month.ToString())) + ((DateTime.Now.Day > 9) ? (DateTime.Now.Day.ToString()) : ("0" + DateTime.Now.Day.ToString())));
            xmlTextWriter.WriteElementString("AircraftNumber", aircraftNNumber);
            logger.Fatal("Month = " + DateTime.Now.Month.ToString() + " Day = " + DateTime.Now.Day.ToString());
            xmlTextWriter.WriteStartElement("SalesDetail");
            xmlTextWriter.WriteElementString("AircraftNumber", aircraftNNumber);
            xmlTextWriter.WriteElementString("DetailType", "Rental");
            xmlTextWriter.WriteElementString("HobbsOut", Convert.ToString(startHobbsQty));
            xmlTextWriter.WriteElementString("HobbsIn", Convert.ToString(endtHobbsQty));
            xmlTextWriter.WriteElementString("TachOut", Convert.ToString(startTachQty));
            xmlTextWriter.WriteElementString("TachIn", Convert.ToString(endtTachQty));
            xmlTextWriter.WriteElementString("Quantity", Convert.ToString(Math.Round(quantity, 2)));

            var saleDetails = aircraftSalesDetail.FirstOrDefault(f => f.SalesDetailMaster.DetailType == "Rental");

            xmlTextWriter.WriteElementString("SalePrice", Convert.ToString(saleDetails.Price));
            xmlTextWriter.WriteElementString("GLAccountNumber", saleDetails.GLAccountNumber);
            xmlTextWriter.WriteElementString("ProductSubTotal", Convert.ToString(Math.Round((Convert.ToDecimal(quantity) * saleDetails.Price), 2)));
            xmlTextWriter.WriteEndElement();

            saleDetails = aircraftSalesDetail.FirstOrDefault(f => f.SalesDetailMaster.DetailType == "Instruction");
            xmlTextWriter.WriteStartElement("SalesDetail");
            xmlTextWriter.WriteElementString("AircraftNumber", aircraftNNumber);
            xmlTextWriter.WriteElementString("DetailType", "Instruction");
            xmlTextWriter.WriteElementString("Quantity", Convert.ToString(Math.Round(quantity, 2)));
            xmlTextWriter.WriteElementString("SalePrice", Convert.ToString(saleDetails.Price));
            xmlTextWriter.WriteElementString("GLAccountNumber", saleDetails.GLAccountNumber);
            xmlTextWriter.WriteElementString("ProductSubTotal", Convert.ToString(Math.Round((Convert.ToDecimal(quantity) * saleDetails.Price), 2)));
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteEndElement();
            //XmlDocument docSave = new XmlDocument();
            //docSave.LoadXml(stringwriter.ToString());
            ////write the path where you want to save the Xml file
            //docSave.Save(ConfigurationManager.AppSettings["TFBOxmlFilePath"] + "\\" + aircraftNNumber + "-" + flightId + ".xml");

            string xmlText = stringwriter.ToString().Remove(63, 1).Remove(67, 1);
            System.IO.File.WriteAllText(ConfigurationManager.AppSettings["TFBOxmlFilePath"] + "\\" + aircraftNNumber + "-" + flightId + ".xml", xmlText);

            string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "logo1.png";
            string cidFBImageIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "facebook.png";
            string cidTwitterIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "twitter.png";
            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\emailTFBO.html";
            string serverUrl = ConfigurationManager.AppSettings["ServerUrl"];
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string h1 = string.Format(htmlBody,
                ownerName,
                cid,
                cidFBImageIcon,
                cidTwitterIcon
                );
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            

            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
            mail.To.Add(ownerEmailId);
            mail.CC.Add(ConfigurationManager.AppSettings["EmailToCC"]);
            mail.Subject = "TFBO Export File ";
            mail.Attachments.Add(new Attachment(ConfigurationManager.AppSettings["TFBOxmlFilePath"] + "\\" + aircraftNNumber + "-" + flightId + ".xml"));
            mail.Body = h1;
            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);
        }

        public static string GetStringOnlyDateUS(DateTime? objDate)
        {
            if (objDate != null)
            {
                var obj = (DateTime)objDate;
                string[] test = obj.Date.ToString("G", CultureInfo.CreateSpecificCulture("es-US")).Split(' ');
                return test[0];
            }
            return string.Empty;
        }

        public void SendEmailServer(EndOfFlightEmailModel model, string EmailId, string Name)
        {
            try
            {
                
                logger.Fatal("H");
                var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\email.html";

                if (model.CoPilotName == "Not Available")
                {
                    pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\emailWithoutCopilotDetail.html";
                }
                var pathStyleSheet = System.Windows.Forms.Application.StartupPath + "\\Content\\StyleEmail.css";
                string stylesheet = System.IO.File.ReadAllText(pathStyleSheet);
                string serverUrl = ConfigurationManager.AppSettings["ServerUrl"];
                string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
                logger.Fatal("I");
                string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "smart_plane.png";
                string cidFBImageIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "facebook.png";
                string cidTwitterIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "twitter.png";
                string cidPilotImage = "";
                string cidCoPilotImage = "";
                string cidAircraftImage = "";
                string cidGoogleMapPath = "";
                string cidGoogleGraphPath = "";
                string cidGoogleGraphPathFF = "";
                string cidGoogleGraphPathRPM = "";
                string cidGoogleGraphPathCHT = "";
                string cidGoogleGraphPathEGT = "";
                logger.Fatal("snapshot path = " + ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + ".png");
                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + ".png"))
                {
                    logger.Fatal("snapshot path file exist");
                    cidGoogleMapPath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + ".png";
                    logger.Fatal("cidGoogleMapPath = " + cidGoogleMapPath);
                }

                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + "-Speed.png"))
                {
                    logger.Fatal("Graph snapshot path file exist");
                    cidGoogleGraphPath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + "-Speed.png";
                    logger.Fatal("cidGoogleMapPath = " + cidGoogleGraphPath);
                }

                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + "-FF.png"))
                {
                    logger.Fatal("FF Graph snapshot path file exist");
                    cidGoogleGraphPathFF = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + "-FF.png";
                    logger.Fatal("cidGoogleMapPathFF = " + cidGoogleGraphPathFF);
                }

                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + "-RPM.png"))
                {
                    logger.Fatal("RPM Graph snapshot path file exist");
                    cidGoogleGraphPathRPM = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + "-RPM.png";
                    logger.Fatal("cidGoogleMapPathRPM = " + cidGoogleGraphPathRPM);
                }

                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + "-EGT.png"))
                {
                    logger.Fatal("EGT Graph snapshot path file exist");
                    cidGoogleGraphPathEGT = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + "-EGT.png";
                    logger.Fatal("cidGoogleMapPathEGT = " + cidGoogleGraphPathEGT);
                }

                if (File.Exists(ConfigurationReader.GoogleMapSnapshopPath + model.PilotLogId + "-CHT.png"))
                {
                    logger.Fatal("CHT Graph snapshot path file exist");
                    cidGoogleGraphPathCHT = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGoogleMapSnapshot + model.PilotLogId + "-CHT.png";
                    logger.Fatal("cidGoogleMapPathCHT = " + cidGoogleGraphPathCHT);
                }

                if (model.PilotImage == "")
                {
                    cidPilotImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "profilePic.png";
                }
                else
                {
                    cidPilotImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + model.PilotImage;
                }

                if (model.CoPilotImage == "")
                {
                    cidCoPilotImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "profilePic.png";
                }
                else
                {
                    cidCoPilotImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + model.CoPilotImage;
                }

                if (model.AircraftImage == "")
                {
                    cidAircraftImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "aircraftPic.png";
                }
                else
                {
                    cidAircraftImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + model.AircraftImage;
                }

                string emailHeaderText = "The smartMFD app has completed the recording of your most recent flight on " + model.AircraftNnumber + " on " + model.FlightCompletedOn + " and the details are listed below. For more detailed information, please log in to your account at " + ConfigurationReader.ServerUrl + " for full flight details and analysis.";

                string h1 = string.Format(htmlBody,
                    Name, //0
                    model.AircraftNnumber, //1
                    model.FlightCompletedOn, //2
                    model.FlightTime,  //3
                    model.StartTechTime, //4
                    model.EndTechTime, //5
                    model.StartHobbsTime, //6
                    model.EndHobbsTime, //7
                    cid,  //8
                    cidPilotImage, //9
                    cidCoPilotImage,  //10
                    stylesheet,  //11
                    serverUrl, //12
                    model.PilotName, //13
                    model.CoPilotName, //14
                    model.PilotContact,  //15
                    "", //16
                    model.CoPilotContactNo, //17
                    "", //18
                    model.AircraftSerialNo, //19
                    model.AircraftOwnerName, //20
                    cidAircraftImage, //21
                    model.TotalGallonsused, //22
                    cidFBImageIcon, //23
                    cidTwitterIcon, //24
                    cidGoogleMapPath, //25
                    emailHeaderText, //26
                    cidGoogleGraphPath, //27
                    cidGoogleGraphPathFF, //28
                    cidGoogleGraphPathRPM, //29
                    cidGoogleGraphPathEGT,  //30
                    cidGoogleGraphPathCHT  //31
                    );


                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);
                bool isFileExist = true;
                Stream fStream;

                fStream = ReadFileFromS3("DefaultImages", "smart_plane.png", out isFileExist);
                LinkedResource inline = new LinkedResource(fStream, contentType: new ContentType("image/png"));
                inline.ContentId = cid;//Guid.NewGuid().ToString();
                inline.ContentType.Name = "Logo";

                

                MailMessage mail = new MailMessage();
                mail.AlternateViews.Add(avHtml);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);

                mail.To.Add(EmailId);


                mail.Subject = "End of flight notification from the Pilot EFB App";
                mail.Body = h1;

                mail.IsBodyHtml = true;
                SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
                smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
                smpt.UseDefaultCredentials = false;
                smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailPassword"]);
                smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                smpt.Send(mail);
            }
            catch (Exception e)
            {
                logger.Fatal("Exception : " + Newtonsoft.Json.JsonConvert.SerializeObject(e));
                //ExceptionHandler.ReportError(e, Newtonsoft.Json.JsonConvert.SerializeObject(model == default(PilotLogSummaryEmailModel) ? new PilotLogSummaryEmailModel() : model));
            }
        }

        public string addTime(string time1, string time2)
        {
            strLogger.Append("Add Time(" + time1 + "," + time2 + ")" + Environment.NewLine);

            if (string.IsNullOrEmpty(time1))
            {
                time1 = "00:00:00";
            }
            if (string.IsNullOrEmpty(time2))
            {
                time2 = "00:00:00";
            }

            string[] arr1 = time1.Split(':');
            string[] arr2 = time2.Split(':');

            int[] t1 = { Convert.ToInt32(arr1[0]), Convert.ToInt32(arr1[1]), Convert.ToInt32(arr1[2]) };
            int[] t2 = { Convert.ToInt32(arr2[0]), Convert.ToInt32(arr2[1]), Convert.ToInt32(arr2[2]) };

            int sec = 0, min = 0, hrs = 0;

            if ((t1[2] + t2[2]) >= 60)
            {
                sec = ((t1[2] + t2[2]) % 60);
                min = min + 1;
            }
            else
            {
                sec = (t1[2] + t2[2]);
            }

            if ((t1[1] + t2[1] + min) >= 60)
            {
                min = ((t1[1] + t2[1] + min) % 60);
                hrs = hrs + 1;
            }
            else
            {
                min = (t1[1] + t2[1] + min);
            }

            hrs = hrs + t1[0] + t2[0];

            strLogger.Append("Result of add time " + ((hrs < 10) ? "0" + Convert.ToString(hrs) : Convert.ToString(hrs)) + ":" + ((min < 10) ? "0" + Convert.ToString(min) : Convert.ToString(min)) + ":" + ((sec < 10) ? "0" + Convert.ToString(sec) : Convert.ToString(sec) + Environment.NewLine));
            return ((hrs < 10) ? "0" + Convert.ToString(hrs) : Convert.ToString(hrs)) + ":" + ((min < 10) ? "0" + Convert.ToString(min) : Convert.ToString(min)) + ":" + ((sec < 10) ? "0" + Convert.ToString(sec) : Convert.ToString(sec));
        }

        public string ConvertMinToHHMM(double totalMinutes)
        {
            string minute = "0";
            string hours = "0";

            if (totalMinutes < 60)
            {
                minute = Convert.ToString(totalMinutes);
            }
            else
            {
                minute = Convert.ToString(totalMinutes % 60);
            }

            hours = Convert.ToString(Convert.ToInt32((Int32)totalMinutes / 60));
            if (Convert.ToDouble(hours) < 1)
            {
                hours = "00";
            }
            else if (Convert.ToDouble(hours) < 10)
            {
                hours = "0" + hours;
            }

            if (Convert.ToDouble(minute) < 10)
            {
                minute = "0" + minute;
            }

            //return (hours + ":" + minute + ":00");
            return (hours + ":" + minute);
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));
        }

        public static string ConvertMinToOneTenthOFTime(double min)
        {
            double response = (min / 60);
            response = Math.Round(Convert.ToDouble(response), 1, MidpointRounding.ToEven); // Rounds to even
            return response.ToString().Split('.').Length > 1 ? response.ToString() : (response.ToString() + ".0");
        }


        public Stream ReadFileFromS3(string folderName, string fileName, out bool isFileExist)
        {
            isFileExist = true;
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
            {
                Stream rs = new System.IO.MemoryStream();
                GetObjectRequest getObjectRequest = new GetObjectRequest();
                getObjectRequest.BucketName = ConfigurationReader.S3Bucket;
                getObjectRequest.Key = GetS3FolderName(folderName) + fileName;
                try
                {
                    using (var getObjectResponse = client.GetObject(getObjectRequest))
                    {
                        return getObjectResponse.ResponseStream;

                        //below 3 lines of code is to save the s3 bucket file to the local path
                        //getObjectResponse.ResponseStream.CopyTo(rs);
                        //System.Drawing.Image img = System.Drawing.Image.FromStream(rs);
                        //img.Save("D:\\ttt.png", ImageFormat.Png);
                    }
                }
                catch (Exception ex)
                {
                    //Error occurs when the file is not exist on the server
                    isFileExist = false;
                }
                return rs;
            }
        }

        public string GetS3FolderName(string folderName)
        {
            string path = "";
            switch (folderName)
            {
                case "Doc":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketDocPath;
                    break;
                case "MapFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketMapFilePath;
                    break;
                case "Image":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages;
                    break;
                case "IPassVideo":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassVideo;
                    break;
                case "IPassImage":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassImage;
                    break;
                case "IPassPDF":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassPDF;
                    break;
                case "PlateJson":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlateJsonFolder;
                    break;
                case "PlatePNGAndTxt":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlatePNGAndTxtFolder;
                    break;
                case "AudioFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketAudioFilePath;
                    break;
                case "HardWareFirmware":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketHardwareFirmwareFilePath;
                    break;
                case "DefaultImages":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages;
                    break;
                case "KmlFiles":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketKmlFile;
                    break;
            }
            return path;
        }

    }
}
