﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataLayer;

using System.Data.SqlClient;
using System.Data;

namespace UploadFileOnDropBoxServiceStaging
{
    public class AddDatalogToAircraft
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        public void AddDataLog()
        {
            var context = new GuardianAvionicsEntities();
            var pilotLogList = context.PilotLogs.Where(p => (p.IsEmailSent != true) && p.Finished && (p.DropBoxErrorMsg == null || p.DropBoxErrorMsg == "")).ToList();

            try
            {

                foreach (var pilotLog in pilotLogList)
                {
                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "aircraftId";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLog.AircraftId;

                    param[1] = new SqlParameter();
                    param[1].ParameterName = "pilotlogId";
                    param[1].SqlDbType = SqlDbType.Int;
                    param[1].Value = pilotLog.Id;


                    Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spAddDataLogToAircraft", param);
                }
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Upload File On Dropbox Service";
                errorLog.ErrorSource = "AddDataLog";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "No Request Parameter";
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();

            }
        }

    }
}
