﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using GA.DataLayer;
using System.Configuration;
using OfficeOpenXml;
using NLog;
using Dropbox.Api;
using System.Data.SqlClient;
using System.Data;

namespace UploadFileOnDropBoxServiceStaging
{
    public class UploadFile
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public void UploadFileOnDropBox()
        {

            string engineCommandType = "JPI";

            var context = new GuardianAvionicsEntities();
            //Select all pilot logs whose flight is finished and file is not saved on dropbox
            var pilotLogList = context.PilotLogs.Where(p => p.Finished && p.IsSavedOnDropbox == false && (p.DropBoxErrorMsg == null || p.DropBoxErrorMsg == "")).ToList();

            foreach (var pilotLog in pilotLogList)
            {

                try
                {
                    //Check the DropBox is sync for Pilot or Copilot or aircraft owner
                    var preferenceList = context.Preferences.Where(p => (p.ProfileId == pilotLog.PilotId || p.ProfileId == pilotLog.CoPilotId || p.ProfileId == pilotLog.AircraftProfile.OwnerProfileId) &&
                                                                    p.IsDropboxSync != null && p.IsDropboxSync == true).ToList();
                    if (preferenceList.Count == 0)
                    {
                        pilotLog.DropBoxErrorMsg = "DropBox not sync with any of the user- pilot, copilot and owner";
                        context.SaveChanges();
                        continue;
                    }


                    if (pilotLog.CommandRecFrom != null)
                    {
                        engineCommandType = pilotLog.CommandRecFrom;
                    }
                    else
                    {
                        engineCommandType = "JPI";
                    }

                    var dataLogList = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();


                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLog.Id;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            var airframedatalog = ds.Tables[0].AsEnumerable()
                              .Select(r => r.Field<string>("DataLog"))
                              .ToList();
                            foreach (var airframedata in airframedatalog)
                            {
                                dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                            }

                        }
                    }
                    //var airframedatalog = context.AirframeDatalogs.Where(a => a.PilotLogId == pilotLog.Id).Select(s => s.DataLog).ToList();
                    if (dataLogList.Count > 0)
                    {
                        //Validate all the values , if any garbage value then will be replaced by 0
                        dataLogList.ForEach(f =>
                        {
                            f.Cht1 = ValidateStringValue(f.Cht1);
                            f.Cht2 = ValidateStringValue(f.Cht2);
                            f.Cht3 = ValidateStringValue(f.Cht3);
                            f.Cht4 = ValidateStringValue(f.Cht4);
                            f.Cht5 = ValidateStringValue(f.Cht5);
                            f.Cht6 = ValidateStringValue(f.Cht6);
                            f.Egt1 = ValidateStringValue(f.Egt1);
                            f.Egt2 = ValidateStringValue(f.Egt2);
                            f.Egt3 = ValidateStringValue(f.Egt3);
                            f.Egt4 = ValidateStringValue(f.Egt4);
                            f.Egt5 = ValidateStringValue(f.Egt5);
                            f.Egt6 = ValidateStringValue(f.Egt6);
                            f.Oat = ValidateStringValue(f.Oat);
                            f.Speed = ValidateStringValue(f.Speed);
                            f.VOLTS = ValidateStringValue(f.VOLTS);
                            f.FQL = ValidateStringValue(f.FQL);
                            f.FQR = ValidateStringValue(f.FQR);
                            f.FF = ValidateStringValue(f.FF);
                            f.MAP = ValidateStringValue(f.MAP);
                            f.RPM = ValidateStringValue(f.RPM);
                            f.TIT1 = ValidateStringValue(f.TIT1);
                            f.Altitude = ValidateStringValue(f.Altitude);
                            f.HP = ValidateStringValue(f.HP);
                        }
                        );

                        var collection = dataLogList.AsEnumerable().Select(s => new DataLogExcelSheets
                        {
                            Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                            Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                            utcOfst = "",
                            WayPoint = "",
                            Latitude = Convert.ToDouble(s.Latitude),
                            Longitude = Convert.ToDouble(s.Longitude),
                            AltB = "",
                            BaroA = "",
                            AltMSL = "",
                            Oat = Convert.ToDouble(s.Oat),
                            IAS = "",
                            GndSpd = Convert.ToDouble(s.Speed),
                            VSpd = "",
                            Pitch = "",
                            Roll = "",
                            LatAc = "",
                            NormAc = "",
                            HDG = "",
                            TRK = "",
                            Volt1 = Convert.ToDouble(s.VOLTS),
                            Volt2 = "",
                            AMP1 = Convert.ToString(s.AMP),
                            AMP2 = "",
                            FQL = Convert.ToString(s.FQL),
                            FQR = Convert.ToString(s.FQR),
                            FF = Convert.ToDouble(s.FF),
                            OILP = s.OILP,
                            OILT = s.OILT,
                            MAP = Convert.ToDouble(s.MAP),
                            RPM = Convert.ToDouble(s.RPM),
                            Cht1 = Convert.ToDouble(s.Cht1),
                            Cht2 = Convert.ToDouble(s.Cht2),
                            Cht3 = Convert.ToDouble(s.Cht3),
                            Cht4 = Convert.ToDouble(s.Cht4),
                            Cht5 = Convert.ToDouble(s.Cht5),
                            Cht6 = Convert.ToDouble(s.Cht6),
                            Egt1 = Convert.ToDouble(s.Egt1),
                            Egt2 = Convert.ToDouble(s.Egt2),
                            Egt3 = Convert.ToDouble(s.Egt3),
                            Egt4 = Convert.ToDouble(s.Egt4),
                            Egt5 = Convert.ToDouble(s.Egt5),
                            Egt6 = Convert.ToDouble(s.Egt6),
                            TIT1 = (s.TIT1),
                            GpsAltitude = Convert.ToDouble(s.Altitude),
                            TAS = "",
                            HSIS = "",
                            CRS = "",
                            NAV1 = "",
                            NAV2 = "",
                            COM1 = "",
                            COM2 = "",
                            HCDI = "",
                            VCDI = "",
                            WndSpd = "",
                            WndDr = "",
                            WptDst = "",
                            WptBrg = "",
                            MagVar = "",
                            AfcsOn = "",
                            RollM = "",
                            PitchM = "",
                            RollC = "",
                            PichC = "",
                            VSpdG = "",
                            GPSfix = "",
                            HAL = "",
                            VAL = "",
                            HPLwas = Convert.ToString(s.HP),
                            HPLfd = "",
                            VPLwas = "",
                            Yaw = "",
                            CDT = s.CDT,
                            CLD = s.CLD,
                            END = s.END,
                            FP = s.FP,
                            IAT = s.IAT,
                            MPG = s.MPG,
                            REM = s.REM,
                            REQ = s.REQ,
                            RES = s.RES,
                            USD = s.USD,
                            TIT2 = s.TIT2,
                            ECON = s.ECON,
                            GPH = s.GPH,
                            CalculatedFuelRemaining = s.CalculatedFuelRemaining,
                            TotalAircraftTime = s.TotalAircraftTime,
                            EngineTime = s.EngineTime,
                            ElevatorTrimPosition = s.ElevatorTrimPosition,
                            UnitsIndicator = s.UnitsIndicator,
                            FlapPosition = s.FlapPosition,
                            UnitsIndicator2 = s.UnitsIndicator2,
                            CarbTemp = s.CarbTemp,
                            UnitsIndicator3 = s.UnitsIndicator3,
                            CoolantPressure = s.CoolantPressure,
                            UnitsIndicator4 = s.UnitsIndicator4,
                            CoolantTemperature = s.CoolantTemperature,
                            UnitsIndicator5 = s.UnitsIndicator5,
                            UnitsIndicator6 = s.UnitsIndicator6,
                            AileronTrimPosition = s.AileronTrimPosition,
                            UnitsIndicator7 = s.UnitsIndicator7,
                            RubberTrimPosition = s.RubberTrimPosition,
                            UnitsIndicator8 = s.UnitsIndicator8,
                            FuelQty3 = s.FuelQty3,
                            UnitsIndicator9 = s.UnitsIndicator9,
                            FuelQty4 = s.FuelQty4,
                            UnitsIndicator10 = s.UnitsIndicator10,
                            DiscreteInput1 = s.DiscreteInput1,
                            DiscreteInput2 = s.DiscreteInput2,
                            DiscreteInput3 = s.DiscreteInput3,
                            DiscreteInput4 = s.DiscreteInput4,
                            IgnStatus = s.IgnStatus,
                            SensorStatus = s.SensorStatus,
                            ThrottlePosition = s.ThrottlePosition,
                            Baro = s.Baro,
                            Airtemp = s.Airtemp,
                            EcuTemp = s.EcuTemp,
                            Batteryvoltage = s.Batteryvoltage,
                            Sen1 = s.Sen1,
                            Sen2 = s.Sen2,
                            Sen3 = s.Sen3,
                            Sen4 = s.Sen4,
                            Sen5 = s.Sen5

                        });


                        string pilotName = (pilotLog.PilotId != null) ? pilotLog.Profile.UserDetail.FirstName + " " + pilotLog.Profile.UserDetail.LastName : "";
                        string coPilotName = (pilotLog.CoPilotId != null) ? pilotLog.Profile1.UserDetail.FirstName + " " + pilotLog.Profile1.UserDetail.LastName : "";
                        pilotName = string.IsNullOrEmpty(pilotName)
                                    ? string.IsNullOrEmpty(coPilotName)
                                        ? "Pilot Name : Not Available , CoPoilotName : Not Available"
                                        : "Pilot Name : Not Available , CoPoilotName : " + coPilotName
                                    : "Pilot Name : " + pilotName + " " + (string.IsNullOrEmpty(coPilotName)
                                                                            ? "CoPilot Name : Not Available"
                                                                            : "CoPilot Name : " + coPilotName);

                        string fileName = DateTime.Now.Ticks.ToString() + ".xls";

                        bool IsFileDumpSuccessfully = DumpExcel(collection, pilotLog.AircraftProfile.Registration,
                                      pilotLog.AircraftProfile.AircraftType,
                                      pilotLog.AircraftProfile.AeroUnitNo,
                                      pilotName,
                                      fileName, engineCommandType
                                   );

                        if (!IsFileDumpSuccessfully)
                        {
                            pilotLog.IsSavedOnDropbox = true;
                            pilotLog.DropBoxErrorMsg = "Exception while Saving the excel file on the local system for uploading this file on DropBo";
                            context.SaveChanges();
                            continue;
                        }


                        foreach (var preference in preferenceList)
                        {
                            var flightNumber = 0;
                            if (preference.ProfileId == pilotLog.AircraftProfile.OwnerProfileId)
                            {
                                flightNumber = context.PilotLogs.Count(p => p.Id <= pilotLog.Id
                                                                && p.AircraftId == pilotLog.AircraftId
                                                                && !p.Deleted);
                            }
                            else
                            {
                                flightNumber = context.PilotLogs.Count(p => p.Id <= pilotLog.Id
                                                                && (p.PilotId == preference.ProfileId || p.CoPilotId == preference.ProfileId)
                                                                && !p.Deleted);
                            }

                            //Select the flightNo from the PilotLogs
                            string flightName = string.Format("Flight {0} - {1} {2} {3} {4}.xlsx"
                                                           , flightNumber
                                                           , (pilotLog.AircraftProfile.Registration ?? string.Empty).ToUpper()
                                                           , (preference.Profile.UserDetail.FirstName ?? string.Empty)
                                                           , (preference.Profile.UserDetail.LastName ?? string.Empty)
                                                           , pilotLog.Date.ToShortDateString()
                                                           .Replace(" ", "").Replace(":", "-").Replace(@"/", "-").Replace(@"\", "-"));
                            if (string.IsNullOrEmpty(preference.AccessToken))
                            {
                                continue;
                            }
                            var response = new DropboxFileUpload().UploadFileDropbox(preference.AccessToken, ConfigurationManager.AppSettings["DropboxExcelSheetPath"].Trim() + @"\" + fileName, flightName);


                        }
                        pilotLog.IsSavedOnDropbox = true;
                        context.SaveChanges();

                        if (System.IO.File.Exists(ConfigurationManager.AppSettings["DropboxExcelSheetPath"].Trim() + @"\" + fileName))
                            System.IO.File.Delete(ConfigurationManager.AppSettings["DropboxExcelSheetPath"].Trim() + @"\" + fileName);
                    }

                }
                catch (Exception e)
                {
                    pilotLog.IsSavedOnDropbox = true;
                    pilotLog.DropBoxErrorMsg = "Exception while uploading the file";
                    context.SaveChanges();
                    logger.Fatal("Exception while  uploading this file on DropBox ", e);
                }
            }

        }

        private bool DumpExcel(IEnumerable<DataLogExcelSheets> jpi, string registration,
         string aircraftType, string aeroUnitNo, string pilotName, string fileName, string engineCommandType)
        {
            try
            {
                var filepath = ConfigurationManager.AppSettings["DropboxExcelSheetPath"].Trim() + @"\" + fileName;
                var newFile = new FileInfo(@filepath);

                using (var pck = new ExcelPackage(newFile))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Engine Data");

                    #region HeadersAndUnits , Aircraft details

                    ws.Cells["A1"].Value = "Aircraft N-Number" + ":" + registration;
                    ws.Cells["B1"].Value = "Aircraft Type" + ":" + aircraftType;
                    ws.Cells["C1"].Value = "A/C Hardware S/N" + ":" + aeroUnitNo;
                    ws.Cells["D1"].Value = pilotName;

                    ws.Cells["A3"].Value = "Lcl Date";
                    ws.Cells["B3"].Value = "Lcl Time";
                    ws.Cells["C3"].Value = "UTCOfst";
                    ws.Cells["D3"].Value = "AtvWpt";
                    ws.Cells["E3"].Value = "Latitude";
                    ws.Cells["F3"].Value = "Longitude";
                    ws.Cells["G3"].Value = "AltB";
                    ws.Cells["H3"].Value = "BaroA";
                    ws.Cells["I3"].Value = "AltMSL";
                    ws.Cells["J3"].Value = "OAT";
                    ws.Cells["K3"].Value = "IAS";
                    ws.Cells["L3"].Value = "GndSpd";
                    ws.Cells["M3"].Value = "VSpd";
                    ws.Cells["N3"].Value = "Pitch";
                    ws.Cells["O3"].Value = "Roll";
                    ws.Cells["P3"].Value = "LatAc";
                    ws.Cells["Q3"].Value = "NormAc";
                    ws.Cells["R3"].Value = "HDG";
                    ws.Cells["S3"].Value = "TRK";
                    ws.Cells["T3"].Value = "Volt1";
                    ws.Cells["U3"].Value = "Volt2";
                    ws.Cells["V3"].Value = "AMP1";
                    ws.Cells["W3"].Value = "AMP2";
                    ws.Cells["X3"].Value = "FQtyL";
                    ws.Cells["Y3"].Value = "FQtyR";
                    ws.Cells["Z3"].Value = "E1 FFlow";
                    ws.Cells["AA3"].Value = "E1 OilP";
                    ws.Cells["AB3"].Value = "E1 OilT";
                    ws.Cells["AC3"].Value = "E1 MAP";
                    ws.Cells["AD3"].Value = "E1 RPM";
                    ws.Cells["AE3"].Value = "E1 Cht1";
                    ws.Cells["AF3"].Value = "E1 Cht2";
                    ws.Cells["AG3"].Value = "E1 Cht3";
                    ws.Cells["AH3"].Value = "E1 Cht4";
                    ws.Cells["AI3"].Value = "E1 Cht5";
                    ws.Cells["AJ3"].Value = "E1 Cht6";
                    ws.Cells["AK3"].Value = "E1 Egt1";
                    ws.Cells["AL3"].Value = "E1 Egt2";
                    ws.Cells["AM3"].Value = "E1 Egt3";
                    ws.Cells["AN3"].Value = "E1 Egt4";
                    ws.Cells["AO3"].Value = "E1 Egt5";
                    ws.Cells["AP3"].Value = "E1 Egt6";
                    ws.Cells["AQ3"].Value = "E1 TitL";
                    ws.Cells["AR3"].Value = "AltGPS";
                    ws.Cells["AS3"].Value = "TAS";
                    ws.Cells["AT3"].Value = "HSIS";
                    ws.Cells["AU3"].Value = "CRS";
                    ws.Cells["AV3"].Value = "NAV1";
                    ws.Cells["AW3"].Value = "NAV2";
                    ws.Cells["AX3"].Value = "COM1";
                    ws.Cells["AY3"].Value = "COM2";
                    ws.Cells["AZ3"].Value = "HCDI";
                    ws.Cells["BA3"].Value = "VCDI";
                    ws.Cells["BB3"].Value = "WndSpd";
                    ws.Cells["BC3"].Value = "WndDr";
                    ws.Cells["BD3"].Value = "WptDst";
                    ws.Cells["BE3"].Value = "WptBrg";
                    ws.Cells["BF3"].Value = "MagVar";
                    ws.Cells["BG3"].Value = "AfcsOn";
                    ws.Cells["BH3"].Value = "RollM";
                    ws.Cells["BI3"].Value = "PitchM";
                    ws.Cells["BJ3"].Value = "RollC";
                    ws.Cells["BK3"].Value = "PichC";
                    ws.Cells["BL3"].Value = "VSpdG";
                    ws.Cells["BM3"].Value = "GPSfix";
                    ws.Cells["BN3"].Value = "HAL";
                    ws.Cells["BO3"].Value = "VAL";
                    ws.Cells["BP3"].Value = "HPLwas";
                    ws.Cells["BQ3"].Value = "HPLfd";
                    ws.Cells["BR3"].Value = "VPLwas";
                    ws.Cells["BS3"].Value = "YAW";
                    ws.Cells["BT3"].Value = "CDT";
                    ws.Cells["BU3"].Value = "CLD";
                    ws.Cells["BV3"].Value = "END";
                    ws.Cells["BW3"].Value = "FP";
                    ws.Cells["BX3"].Value = "IAT";
                    ws.Cells["BY3"].Value = "MPG";
                    ws.Cells["BZ3"].Value = "REM";
                    ws.Cells["CA3"].Value = "REQ";
                    ws.Cells["CB3"].Value = "RES";
                    ws.Cells["CC3"].Value = "USD";
                    ws.Cells["CD3"].Value = "E1 TITR";
                    ws.Cells["CE3"].Value = "ECON";
                    ws.Cells["CF3"].Value = "GPH";
                    if (engineCommandType.Contains("Garmin"))
                    {
                        ws.Cells["CG3"].Value = "CalculatedFuelRemaining";
                        ws.Cells["CH3"].Value = "TotalAircraftTime";
                        ws.Cells["CI3"].Value = "EngineTime";
                        ws.Cells["CJ3"].Value = "ElevatorTrimPosition";
                        ws.Cells["CK3"].Value = "UnitsIndicator";
                        ws.Cells["CL3"].Value = "FlapPosition";
                        ws.Cells["CM3"].Value = "UnitsIndicator2";
                        ws.Cells["CN3"].Value = "CarbTemp";
                        ws.Cells["CO3"].Value = "UnitsIndicator3";
                        ws.Cells["CP3"].Value = "CoolantPressure";
                        ws.Cells["CQ3"].Value = "UnitsIndicator4";
                        ws.Cells["CR3"].Value = "CoolantTemperature";
                        ws.Cells["CS3"].Value = "UnitsIndicator5";
                        ws.Cells["CT3"].Value = "UnitsIndicator6";
                        ws.Cells["CU3"].Value = "AileronTrimPosition";
                        ws.Cells["CV3"].Value = "UnitsIndicator7";
                        ws.Cells["CW3"].Value = "RudderTrimPosition";
                        ws.Cells["CX3"].Value = "UnitsIndicator8";
                        ws.Cells["CY3"].Value = "FuelQty3";
                        ws.Cells["CZ3"].Value = "UnitsIndicator9";
                        ws.Cells["DA3"].Value = "FuelQty4";
                        ws.Cells["DB3"].Value = "UnitsIndicator10";
                        ws.Cells["DC3"].Value = "DiscreteInput1";
                        ws.Cells["DD3"].Value = "DiscreteInput2";
                        ws.Cells["DE3"].Value = "DiscreteInput3";
                        ws.Cells["DF3"].Value = "DiscreteInput4";
                    }
                    else if (engineCommandType.Contains("ULP"))
                    {
                        ws.Cells["CG3"].Value = "IgnStatus";
                        ws.Cells["CH3"].Value = "SensorStatus";
                        ws.Cells["CI3"].Value = "ThrottlePosition";
                        ws.Cells["CJ3"].Value = "Baro";
                        ws.Cells["CK3"].Value = "Airtemp";
                        ws.Cells["CL3"].Value = "EcuTemp";
                        ws.Cells["CM3"].Value = "Batteryvoltage";
                        ws.Cells["CN3"].Value = "Sen1";
                        ws.Cells["CO3"].Value = "Sen2";
                        ws.Cells["CP3"].Value = "Sen3";
                        ws.Cells["CQ3"].Value = "Sen4";
                        ws.Cells["CR3"].Value = "Sen5";

                    }


                    // units for all the column
                    ws.Cells["A4"].Value = "mm-dd-yyyy";
                    ws.Cells["B4"].Value = "hh:mm:ss";
                    ws.Cells["C4"].Value = "hh:mm";
                    ws.Cells["D4"].Value = "ident";
                    ws.Cells["E4"].Value = "degrees";
                    ws.Cells["F4"].Value = "degrees";
                    ws.Cells["G4"].Value = "ft Baro";
                    ws.Cells["H4"].Value = "inch";
                    ws.Cells["I4"].Value = "ft msl";
                    ws.Cells["J4"].Value = "deg C";
                    ws.Cells["K4"].Value = "kt";

                    ws.Cells["L4"].Value = "kt";
                    ws.Cells["M4"].Value = "fpm";
                    ws.Cells["N4"].Value = "deg";
                    ws.Cells["O4"].Value = "deg";
                    ws.Cells["P4"].Value = "G";
                    ws.Cells["Q4"].Value = "G";
                    ws.Cells["R4"].Value = "deg";
                    ws.Cells["S4"].Value = "deg";
                    ws.Cells["T4"].Value = "volts";
                    ws.Cells["U4"].Value = "volts";
                    ws.Cells["V4"].Value = "amps";
                    ws.Cells["W4"].Value = "amps";
                    ws.Cells["X4"].Value = "gals";
                    ws.Cells["Y4"].Value = "gals";
                    ws.Cells["Z4"].Value = "gph";
                    ws.Cells["AA4"].Value = "deg F";
                    ws.Cells["AB4"].Value = "psi";
                    ws.Cells["AC4"].Value = "Hg";
                    ws.Cells["AD4"].Value = "rpm";
                    ws.Cells["AE4"].Value = "deg F";
                    ws.Cells["AF4"].Value = "deg F";
                    ws.Cells["AG4"].Value = "deg F";
                    ws.Cells["AH4"].Value = "deg F";
                    ws.Cells["AI4"].Value = "deg F";
                    ws.Cells["AJ4"].Value = "deg F";
                    ws.Cells["AK4"].Value = "deg F";
                    ws.Cells["AL4"].Value = "deg F";
                    ws.Cells["AM4"].Value = "deg F";
                    ws.Cells["AN4"].Value = "deg F";
                    ws.Cells["AO4"].Value = "deg F";
                    ws.Cells["AP4"].Value = "deg F";
                    ws.Cells["AQ4"].Value = "deg F";
                    ws.Cells["AR4"].Value = "ft wgs";
                    ws.Cells["AS4"].Value = "kt";
                    ws.Cells["AT4"].Value = "enum";
                    ws.Cells["AU4"].Value = "deg";
                    ws.Cells["AV4"].Value = "MHz";
                    ws.Cells["AW4"].Value = "MHz";
                    ws.Cells["AX4"].Value = "MHz";
                    ws.Cells["AY4"].Value = "MHz";
                    ws.Cells["AZ4"].Value = "fsd";
                    ws.Cells["BA4"].Value = "fsd";
                    ws.Cells["BB4"].Value = "kt";
                    ws.Cells["BC4"].Value = "deg";
                    ws.Cells["BD4"].Value = "nm";
                    ws.Cells["BE4"].Value = "deg";
                    ws.Cells["BF4"].Value = "deg";
                    ws.Cells["BG4"].Value = "bool";
                    ws.Cells["BH4"].Value = "enum";
                    ws.Cells["BI4"].Value = "enum";
                    ws.Cells["BJ4"].Value = "deg";
                    ws.Cells["BK4"].Value = "deg";
                    ws.Cells["BL4"].Value = "fpm";
                    ws.Cells["BM4"].Value = "enum";
                    ws.Cells["BN4"].Value = "mt";
                    ws.Cells["BO4"].Value = "mt";
                    ws.Cells["BP4"].Value = "mt";
                    ws.Cells["BQ4"].Value = "mt";
                    ws.Cells["BR4"].Value = "mt";
                    ws.Cells["BS4"].Value = "degrees";

                    ws.Cells["BT4"].Value = "";
                    ws.Cells["BU4"].Value = "";
                    ws.Cells["BV4"].Value = "";
                    ws.Cells["BW4"].Value = "";
                    ws.Cells["BX4"].Value = "";
                    ws.Cells["BY4"].Value = "";
                    ws.Cells["BZ4"].Value = "";
                    ws.Cells["CA4"].Value = "";
                    ws.Cells["CB4"].Value = "";
                    ws.Cells["CC4"].Value = "";
                    ws.Cells["CD4"].Value = "";
                    ws.Cells["CE4"].Value = "";
                    ws.Cells["CF4"].Value = "";
                    if (engineCommandType.Contains("Garmin"))
                    {
                        ws.Cells["CG4"].Value = "gallon";
                        ws.Cells["CH4"].Value = "hours";
                        ws.Cells["CI4"].Value = "hours";
                        ws.Cells["CJ4"].Value = "1 % of Travel";
                        ws.Cells["CK4"].Value = "&nbsp;";
                        ws.Cells["CL4"].Value = "degree";
                        ws.Cells["CM4"].Value = "&nbsp;";
                        ws.Cells["CN4"].Value = "deg";
                        ws.Cells["CO4"].Value = "&nbsp;";
                        ws.Cells["CP4"].Value = "PSI";
                        ws.Cells["CQ4"].Value = "&nbsp;";
                        ws.Cells["CR4"].Value = "deg";
                        ws.Cells["CS4"].Value = "&nbsp;";
                        ws.Cells["CT4"].Value = "&nbsp;";
                        ws.Cells["CU4"].Value = "1 % of Travel";
                        ws.Cells["CV4"].Value = "&nbsp;";
                        ws.Cells["CW4"].Value = "1 % of Travel";
                        ws.Cells["CX4"].Value = "&nbsp;";
                        ws.Cells["CY4"].Value = "gallon";
                        ws.Cells["CZ4"].Value = "&nbsp;";
                        ws.Cells["DA4"].Value = "gallon";
                        ws.Cells["DB4"].Value = "&nbsp;";
                        ws.Cells["DC4"].Value = "&nbsp;";
                        ws.Cells["DD4"].Value = "&nbsp;";
                        ws.Cells["DE4"].Value = "&nbsp;";
                        ws.Cells["DF4"].Value = "&nbsp;";

                    }
                    else if (engineCommandType.Contains("ULP"))
                    {
                        ws.Cells["CG4"].Value = "&nbsp;";
                        ws.Cells["CH4"].Value = "&nbsp;";
                        ws.Cells["CI4"].Value = "percent";
                        ws.Cells["CJ4"].Value = "hPa";
                        ws.Cells["CK4"].Value = "deg F";
                        ws.Cells["CL4"].Value = "deg F";
                        ws.Cells["CM4"].Value = "volts";
                        ws.Cells["CN4"].Value = "&nbsp;";
                        ws.Cells["CO4"].Value = "&nbsp;";
                        ws.Cells["CP4"].Value = "&nbsp;";
                        ws.Cells["CQ4"].Value = "&nbsp;";
                        ws.Cells["CR4"].Value = "&nbsp;";
                    }

                    #endregion HeadersAndUnits

                    try
                    {
                        //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                        ws.Cells["A5"].LoadFromCollection(jpi);
                    }
                    catch (Exception e)
                    {
                        logger.Fatal("Exception while Saving the excel file on the local system for uploading this file on DropBox ", e);
                    }

                    pck.Save();

                    return true;
                }// using
            }
            catch (Exception e)
            {
                logger.Fatal("Exception ", e);
                return false;
            }
        }



        public string ValidateStringValue(string value)
        {
            string response = "0";
            try
            {
                response = string.IsNullOrEmpty(value) ? "0" : value;
            }
            catch (Exception ex)
            {
                response = "0";
            }
            return response;
        }
    }
}
