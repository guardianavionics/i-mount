﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Dropbox.Api;
using NLog;
//using OAuthProtocol;
using Nemiro.OAuth;
using System.Configuration;
using System.IO;
using Dropbox.Api.Files;
using System.Threading.Tasks;

namespace UploadFileOnDropBoxServiceStaging
{

    class DropboxFileUpload
    {
        public Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Redirects to dropbox account.
        /// </summary>
        /// <returns></returns>
        //private static OAuthProtocol.OAuthToken GetAccessToken()
        //{
        //    string consumerKey = ConfigurationManager.AppSettings["DropboxConsumerKey"].Trim();
        //    string consumerSecret = ConfigurationManager.AppSettings["DropboxConsumerSecret"].Trim();
        //    string authenticationTime = "30"; //ConfigurationReader.DropboxAuthenticationTimeSeconds;

        //    int authTime = 1;
        //    int.TryParse(authenticationTime, out authTime);

        //    var oauth = new OAuthProtocol.OAuth();
        //    var requestToken = oauth.GetRequestToken(new Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret);
        //    var authorizeUri = oauth.GetAuthorizeUri(new Uri(DropboxRestApi.AuthorizeBaseUri), requestToken);


        //    Process.Start(authorizeUri.AbsoluteUri);
        //    Thread.Sleep(authTime * 1000); // Leave some time for the authorization step to complete

        //    return oauth.GetAccessToken(new Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret, requestToken);
        //}

        /// <summary>
        /// Redirect user to its dropbox account and gets the access token and access secret
        /// </summary>
        /// <returns>access token and access secret</returns>
        //public string[] GetAccessToken1()
        //{
        //    try
        //    {
        //        var accessToken = GetAccessToken();
        //        return accessToken != null ? new string[] { accessToken.Token, accessToken.Secret } : new string[] { };
        //    }
        //    catch (Exception e)
        //    {
        //        var a = e.Data;
        //        return new string[] { };
        //    }
        //}

        /// <summary>
        /// Upload File in dropbox.
        /// </summary>
        /// <param name="token">User token</param>
        /// <param name="tokenSecret">User token secret</param>
        /// <param name="filePath">Path where file is saved on server</param>
        /// <param name="fileName">Name of file</param>
        /// <returns>Message for success of faliure</returns>
        //public string UploadFileDropbox(string token = "kt4my1pghxu69o9", string tokenSecret = "A7F29F31-1783-4841-B7B5-CEE8E6EDB676", string filePath = "", string fileName = "")        
        public async Task<string> UploadFileDropbox(string tokenAccess, string filePath, string fileName)
        {
            string consumerKey = ConfigurationManager.AppSettings["DropboxConsumerKey"].Trim();
            string consumerSecret = ConfigurationManager.AppSettings["DropboxConsumerSecret"].Trim();
            try
            {
                //var accessToken = new OAuthProtocol.OAuthToken(token, tokenSecret);
                //var api = new DropboxApi(consumerKey, consumerSecret, accessToken);
                //var file = api.UploadFile("dropbox/Guardian Avionics/Flight Data", fileName, @filePath);

                using (var dbx = new DropboxClient(tokenAccess))
                {
                    using (FileStream stream = System.IO.File.Open(filePath, FileMode.Open))
                    {
                        var response = await dbx.Files.UploadAsync("/Guardian Avionics/Flight Data/" + fileName, WriteMode.Overwrite.Instance, body: stream);
                    }
                };
                return Boolean.TrueString;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, "From dropbox");
                logger.Fatal("Exception From dropbox", e);
            }
            return string.Empty;
        }
    }
}
