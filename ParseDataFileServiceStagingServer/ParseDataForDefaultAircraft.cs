﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using GA.DataLayer;
using NLog;
using System.Configuration;
using NLog.Internal;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using GA.CommonForParseDataFile;

namespace ParseDataFileService
{
    public class ParseDataForDefaultAircraft
    {
        bool isNewFlight = true;
        int rowCount = 0;
        List<DataLogWithoutUnit> objDatalogList;
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        public DataTable dt_AirframeDataLog;
        bool isFinished = false;

        public void ParseData(string filePath, int aircarftId, int profileId, int parseDataFileId, long uniqueId)
        {

            var context = new GuardianAvionicsEntities();
            int pilotLogid = 0;
            int flightTimeInPilotLog = 0;  //calculate in second
            objDatalogList = new List<DataLogWithoutUnit>();
            SqlParameter[] param = new SqlParameter[1];

            dt_AirframeDataLog = new DataTable();

            dt_AirframeDataLog.Columns.Add("Id", typeof(int));
            dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
            dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
            dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));

            try
            {

                foreach (var line in System.IO.File.ReadLines(filePath))
                {
                    rowCount++;
                    string[] arrColumns = line.Split(',');
                    if (arrColumns.Count() == 1)
                    {
                        isFinished = (arrColumns[0].ToString().ToLower() == "end");
                    }
                    else
                    {
                        objDatalogList.Add(new DataLogWithoutUnit
                        {
                            Date = Convert.ToDateTime((rowCount == 1) ? (arrColumns[0].Substring(arrColumns[0].Length - 19, 19)) : arrColumns[0]),
                            Latitude = arrColumns[1],
                            Longitude = arrColumns[2],
                            Speed = arrColumns[3],
                            Altitude = arrColumns[4],
                            Yaw = arrColumns[5],
                            Pitch = arrColumns[6],
                            Roll = arrColumns[7],
                            SNO = rowCount
                        });
                    }

                }

                if (rowCount == 0)
                {
                    UpdateParseDataFileStatus(parseDataFileId, 1, "No record found in the file");
                    return;
                }
                var totalSecondInAPIData = (int)(objDatalogList.OrderByDescending(o => o.SNO).FirstOrDefault().Date - objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date).TotalSeconds;


                var pilotLogs = context.PilotLogs.FirstOrDefault(p => p.AeroUnitMasterId == null && p.UniqeId == uniqueId && p.AircraftId == aircarftId && p.FlightDataType == (int)Common.FlightDataType.StoredData);
                var pilotLogToBeInsert = context.PilotLogs.Create();
                if (pilotLogs == null)
                {

                    //Now check that the live data is present or not , if present then delete
                    var pilotlogLiveData = context.PilotLogs.FirstOrDefault(p => p.AeroUnitMasterId == null && p.UniqeId == uniqueId && p.AircraftId == aircarftId && p.FlightDataType == (int)Common.FlightDataType.LiveData);
                    if (pilotlogLiveData != null)
                    {

                        param = new SqlParameter[2];
                        param[0] = new SqlParameter();
                        param[0].ParameterName = "@pilotLogId";
                        param[0].SqlDbType = SqlDbType.Int;
                        param[0].Value = pilotlogLiveData.Id;


                        param[1] = new SqlParameter();
                        param[1].ParameterName = "@aircraftId";
                        param[1].SqlDbType = SqlDbType.Int;
                        param[1].Value = pilotlogLiveData.AircraftId;

                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spDeleteLiveDataFlight", param));
                    }


                    pilotLogToBeInsert.AircraftId = aircarftId;
                    pilotLogToBeInsert.PilotId = profileId;
                    pilotLogToBeInsert.LastUpdated = DateTime.UtcNow;
                    //  pilotLogToBeInsert.Date = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;
                    pilotLogToBeInsert.Date = DateTime.UtcNow;
                    pilotLogToBeInsert.Deleted = false;
                    pilotLogToBeInsert.Actual = "00:00:00";
                    pilotLogToBeInsert.CrossCountry = "00:00:00";


                    TimeSpan time = TimeSpan.FromSeconds(totalSecondInAPIData);
                    pilotLogToBeInsert.DayPIC = time.ToString(@"hh\:mm\:ss");
                    pilotLogToBeInsert.Hood = "00:00:00";
                    pilotLogToBeInsert.NightPIC = "00:00:00";
                    pilotLogToBeInsert.Remark = "";
                    pilotLogToBeInsert.Route = "";
                    pilotLogToBeInsert.Sim = "00:00:00";
                    pilotLogToBeInsert.Finished = isFinished;
                    pilotLogToBeInsert.IFRAppchs = "";
                    pilotLogToBeInsert.IsSavedOnDropbox = false;
                    pilotLogToBeInsert.UniqeId = uniqueId;
                    pilotLogToBeInsert.CoPilotId = null;
                    pilotLogToBeInsert.FlightId = 1;
                    pilotLogToBeInsert.AeroUnitMasterId = null;
                    pilotLogToBeInsert.IsEmailSent = false;
                    pilotLogToBeInsert.CommandRecFrom = null;
                    pilotLogToBeInsert.DropBoxErrorMsg = null;
                    pilotLogToBeInsert.FlightDataType = (int)Common.FlightDataType.StoredData;
                    context.PilotLogs.Add(pilotLogToBeInsert);
                    context.SaveChanges();
                    pilotLogid = pilotLogToBeInsert.Id;
                }
                else
                {
                    pilotLogid = pilotLogs.Id;
                    flightTimeInPilotLog = (int)TimeSpan.Parse(pilotLogs.DayPIC).TotalSeconds;
                    int totalFlightTimeInSec = totalSecondInAPIData + flightTimeInPilotLog;
                    TimeSpan time = TimeSpan.FromSeconds(totalFlightTimeInSec);

                    pilotLogs.DayPIC = time.ToString(@"hh\:mm\:ss");
                    pilotLogs.PilotId = profileId;
                    pilotLogs.LastUpdated = DateTime.UtcNow;
                    pilotLogs.Finished = isFinished;

                    context.SaveChanges();

                    ////Check that the new data is continue with the last flight
                    //SqlParameter[] param = new SqlParameter[4];
                    //param[0] = new SqlParameter();
                    //param[0].ParameterName = "@aircraftId";
                    //param[0].SqlDbType = SqlDbType.Int;
                    //param[0].Value = aircarftId;

                    //param[1] = new SqlParameter();
                    //param[1].ParameterName = "@pilotId";
                    //param[1].SqlDbType = SqlDbType.Int;
                    //param[1].Value = profileId;

                    //param[2] = new SqlParameter();
                    //param[2].ParameterName = "@startDateInDataFile";
                    //param[2].SqlDbType = SqlDbType.DateTime;
                    //param[2].Value = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;

                    //param[3] = new SqlParameter();
                    //param[3].ParameterName = "@EndDateInDataFile";
                    //param[3].SqlDbType = SqlDbType.DateTime;
                    //param[3].Value = objDatalogList.OrderByDescending(o => o.SNO).FirstOrDefault().Date;


                    //DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetStatusToInsertDefaultLog", param);

                    //if (ds.Tables.Count > 0)
                    //{
                    //    if (ds.Tables[0].Rows.Count > 0)
                    //    {
                    //        string flagStatus = ds.Tables[0].Rows[0][0].ToString();
                    //        int pilotLogId = Convert.ToInt32(ds.Tables[0].Rows[0][1]);
                    //        dt_AirframeDataLog = new DataTable();

                    //        dt_AirframeDataLog.Columns.Add("Id", typeof(int));
                    //        dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
                    //        dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
                    //        dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));

                    //        if (flagStatus == "1")
                    //        {
                    //            //Create New Flight
                    //            int? flightId = pilotLogs.FlightId;

                    //            var pilotLogToBeInsert = context.PilotLogs.Create();
                    //            pilotLogToBeInsert.AircraftId = aircarftId;
                    //            pilotLogToBeInsert.PilotId = profileId;
                    //            pilotLogToBeInsert.LastUpdated = DateTime.UtcNow;
                    //            pilotLogToBeInsert.Date = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;
                    //            pilotLogToBeInsert.Deleted = false;
                    //            pilotLogToBeInsert.Actual = "00:00:00";
                    //            pilotLogToBeInsert.CrossCountry = "00:00:00";

                    //            var datetimediff = (int)(objDatalogList.OrderByDescending(o => o.SNO).FirstOrDefault().Date - objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date).TotalMinutes;

                    //            pilotLogToBeInsert.DayPIC = new GA.CommonForParseDataFile.Common().ConvertMinToHHMM(datetimediff);
                    //            pilotLogToBeInsert.Hood = "00:00:00";
                    //            pilotLogToBeInsert.NightPIC = "00:00:00";
                    //            pilotLogToBeInsert.Remark = "";
                    //            pilotLogToBeInsert.Route = "";
                    //            pilotLogToBeInsert.Sim = "00:00:00";
                    //            pilotLogToBeInsert.Finished = isFinished;
                    //            pilotLogToBeInsert.IFRAppchs = "";
                    //            pilotLogToBeInsert.IsSavedOnDropbox = false;
                    //            pilotLogToBeInsert.CoPilotId = null;
                    //            pilotLogToBeInsert.FlightId = flightId + 1;
                    //            pilotLogToBeInsert.AeroUnitMasterId = null;
                    //            pilotLogToBeInsert.IsEmailSent = false;
                    //            pilotLogToBeInsert.CommandRecFrom = null;
                    //            pilotLogToBeInsert.DropBoxErrorMsg = null;
                    //            context.PilotLogs.Add(pilotLogToBeInsert);
                    //            context.SaveChanges();

                    //            foreach (var datalog in objDatalogList)
                    //            {
                    //                DataRow dr = dt_AirframeDataLog.NewRow();
                    //                dr[0] = 0;
                    //                dr[1] = pilotLogToBeInsert.Id;
                    //                dr[2] = datalog.Date;
                    //                dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                    //                dt_AirframeDataLog.Rows.Add(dr);
                    //            }

                    //            param = new SqlParameter[3];
                    //            param[0] = new SqlParameter();
                    //            param[0].ParameterName = "@flag";
                    //            param[0].SqlDbType = SqlDbType.Int;
                    //            param[0].Value = 1;

                    //            param[1] = new SqlParameter();
                    //            param[1].ParameterName = "@aircraftId";
                    //            param[1].SqlDbType = SqlDbType.Int;
                    //            param[1].Value = aircarftId;


                    //            param[2] = new SqlParameter();
                    //            param[2].ParameterName = "tbAirframeDatalog";
                    //            param[2].SqlDbType = SqlDbType.Structured;
                    //            param[2].Value = dt_AirframeDataLog;

                    //            if (dt_AirframeDataLog.Rows.Count > 0)
                    //            {
                    //                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                    //            }

                    //        }
                    //        else if (flagStatus == "2")
                    //        {
                    //            //Append Data to the existing flight

                    //            var pilotLogToUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == pilotLogId);
                    //            pilotLogToUpdate.LastUpdated = DateTime.UtcNow;
                    //            pilotLogToUpdate.DayPIC = new GA.CommonForParseDataFile.Common().ConvertMinToHHMM(Convert.ToDouble(ds.Tables[0].Rows[0]["TotalFlightTime"]));
                    //            pilotLogToUpdate.Finished = isFinished;
                    //            context.SaveChanges();

                    //            foreach (var datalog in objDatalogList)
                    //            {
                    //                DataRow dr = dt_AirframeDataLog.NewRow();
                    //                dr[0] = 0;
                    //                dr[1] = pilotLogId;
                    //                dr[2] = datalog.Date;
                    //                dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                    //                dt_AirframeDataLog.Rows.Add(dr);
                    //            }

                    //            param = new SqlParameter[3];
                    //            param[0] = new SqlParameter();
                    //            param[0].ParameterName = "@flag";
                    //            param[0].SqlDbType = SqlDbType.Int;
                    //            param[0].Value = 1;

                    //            param[1] = new SqlParameter();
                    //            param[1].ParameterName = "@aircraftId";
                    //            param[1].SqlDbType = SqlDbType.Int;
                    //            param[1].Value = aircarftId;


                    //            param[2] = new SqlParameter();
                    //            param[2].ParameterName = "tbAirframeDatalog";
                    //            param[2].SqlDbType = SqlDbType.Structured;
                    //            param[2].Value = dt_AirframeDataLog;

                    //            if (dt_AirframeDataLog.Rows.Count > 0)
                    //            {
                    //                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                    //            }
                    //        }

                    //        UpdateParseDataFileStatus(parseDataFileId, 1, null);
                    //    }
                    //    else
                    //    {
                    //        UpdateParseDataFileStatus(parseDataFileId, 1, "Not handle the case for previous flights");
                    //    }
                    //}
                    //else
                    //{
                    //    UpdateParseDataFileStatus(parseDataFileId, 1, "Not handle the case for previous flights");
                    //}

                }


                foreach (var datalog in objDatalogList)
                {
                    DataRow dr = dt_AirframeDataLog.NewRow();
                    dr[0] = 0;
                    dr[1] = pilotLogid;
                    dr[2] = datalog.Date;
                    dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                    dt_AirframeDataLog.Rows.Add(dr);
                }

                param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@pilotLogId";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = pilotLogid;


                param[1] = new SqlParameter();
                param[1].ParameterName = "@aircraftId";
                param[1].SqlDbType = SqlDbType.Int;
                param[1].Value = aircarftId;


                param[2] = new SqlParameter();
                param[2].ParameterName = "tbAirframeDatalog";
                param[2].SqlDbType = SqlDbType.Structured;
                param[2].Value = dt_AirframeDataLog;

                if (dt_AirframeDataLog.Rows.Count > 0)
                {
                    int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                }

                UpdateParseDataFileStatus(parseDataFileId, 1, null);
            }
            catch (Exception ex)
            {
                UpdateParseDataFileStatus(parseDataFileId, 2, ex.Message);
            }

            //Here we get all the data from the file
            //Now check that the data is for new flight or previous flight
            //There are four possible cases
            //1. Flight data is for new flight
            //2. continue data to the last flight


        }


        public void UpdateParseDataFileStatus(int id, int status, string remark)
        {
            var context = new GuardianAvionicsEntities();
            var obj = context.ParseDataFiles.FirstOrDefault(f => f.Id == id);

            obj.StatusId = status;
            obj.Remark = remark;
            context.SaveChanges();
        }
    }
}
