﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using GA.DataLayer;
using NLog;
using System.Configuration;
using NLog.Internal;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Threading;
using ParseDataFileService;

namespace ParseDataFileServiceStagingServer
{
    
    class SetQueusForParsing
    {

        List<Thread> threadList = new List<Thread>();

        public void SetQueusForParsingDataFiles()
        {
            var context = new GuardianAvionicsEntities();
            var AIrcraftIdList = context.ParseDataFiles.Where(p => p.StatusId == 3 && p.AircraftId != null).OrderBy(o => o.Id).Select(s => s.AircraftId).Distinct().Take(5).ToList();
            


            if (AIrcraftIdList.Count > 0)
            {


                foreach (var aircraftId in AIrcraftIdList)
                {
                
                    ParseDataFileTemp.aircraftIdList.Add(aircraftId);
                    Thread th = new Thread(delegate()
                    {
                       CallMethod(aircraftId);
                    });
                    th.IsBackground = true;
                    th.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[1].Value);
                    //Thread th1 = new Thread(delegate()
                    //{
                    //    CallMethod(AIrcraftIdList[1]);
                    //});
                    //th1.IsBackground = true;
                    //th1.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[2].Value);
                    //Thread th2 = new Thread(delegate()
                    //{
                    //    CallMethod(AIrcraftIdList[2]);
                    //});
                    //th2.IsBackground = true;
                    //th2.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[3].Value);
                    //Thread th3 = new Thread(delegate()
                    //{
                    //    CallMethod(AIrcraftIdList[3]);
                    //});
                    //th3.IsBackground = true;
                    //th3.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[4].Value);
                    //Thread th4 = new Thread(delegate()
                    //{
                    //    CallMethod(AIrcraftIdList[4]);
                    //});
                    //th4.IsBackground = true;
                    //th4.Start();
                }

                    //Thread thread = new Thread(() => CallMethod(AIrcraftIdList[0].Value));
                    //thread.IsBackground = true;
                    //thread.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[1].Value);
                    //Thread thread2 = new Thread(() => CallMethod(AIrcraftIdList[1].Value));
                    //thread2.IsBackground = true;
                    //thread2.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[2].Value);
                    //Thread thread3 = new Thread(() => CallMethod(AIrcraftIdList[2].Value));
                    //thread3.IsBackground = true;
                    //thread3.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[3].Value);
                    //Thread thread4 = new Thread(() => CallMethod(AIrcraftIdList[3].Value));
                    //thread4.IsBackground = true;
                    //thread4.Start();

                    //ParseDataFileTemp.aircraftIdList.Add(AIrcraftIdList[4].Value);
                    //Thread thread5 = new Thread(() => CallMethod(AIrcraftIdList[4].Value));
                    //thread5.IsBackground = true;
                    //thread5.Start();

               
            }
        }


        public void CallMethod(int? aircraftId)
        {
            
            new ParseDataFileTemp().ParseDataFiles(aircraftId);
        }
    }

}


