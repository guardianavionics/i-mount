﻿using System.ServiceProcess;
using System.Text;
using System;
using System.Threading;

using NLog;
using ParseDataFileService;

namespace ParseDataFileServiceStagingServer
{
    public partial class Service1 : ServiceBase
    {
        // Timer timer = new Timer();
        private Timer parseDataTimer = null;
        private readonly int TimerInterval = 60000;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            //int time = 1;
            //time = time == 0 ? 60 : time;
            //// in milliseconds , default is 100 milli seconds
            //timer.Interval = 1000 * 60 * time; // 1 min * time
            //// ExceptionHandler.ReportError(new Exception("Time Interval " + timer.Interval.ToString()));
            //timer.Enabled = true;

            int startin = 60 - DateTime.Now.Second;
            int dueTime = startin * 1000;
            SetProcessingTimer(dueTime);
        }

        protected override void OnStop()
        {
        }

        public void SetProcessingTimer(int dueTime)
        {
            // m_Logger.LogDebug("==============SetProcessingTimer====================");
            // Create the delegate that invokes methods for the timer.
            TimerCallback timerDelegate = new TimerCallback(ProcessItems);

            // Create a timer that signals the delegate to invoke 
            // extracting, its a one click timer
            parseDataTimer = new Timer(timerDelegate, null, dueTime, TimerInterval);
        }

        private void StopProcessingTimer()
        {
            // m_Logger.LogDebug("==============StopProcessingTimer====================");
            parseDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void StartProcessingTimer(int timerInterval)
        {
            // m_Logger.LogDebug("==============StartProcessingTimer====================");
            parseDataTimer.Change(timerInterval, timerInterval);
        }

        public void ProcessItems(Object stateInfo)
        {
            try
            {
                System.Diagnostics.Debugger.Launch();
                //timer.Stop();
                StopProcessingTimer();
                //str.Append("Parse Data File Service Started at " + DateTime.Now + Environment.NewLine);
                //str.Append("ParseDataFileTemp.aircraftIdList.Count= " + ParseDataFileTemp.aircraftIdList.Count);
                //if (ParseDataFileTemp.aircraftIdList.Count == 0)
                //{
                //    SetQueusForParsing obj = new SetQueusForParsing();
                //    obj.SetQueusForParsingDataFiles();
                //}
                //logger.Fatal("Parse Data File parsing Started");
                GA.CommonForParseDataFile.Common objCommon = new GA.CommonForParseDataFile.Common();
                objCommon.SetLiveDataFlightStatus();
                ParseDataFile parseDataFile = new ParseDataFile();
                parseDataFile.ParseDataFiles();

                //ParseDataFileTemp parseDataFile = new ParseDataFileTemp();
                //parseDataFile.ParseDataFiles();

                // timer.Start();
                StartProcessingTimer(TimerInterval);
            }
            catch (Exception ex)
            {
                logger.Fatal("Exception : Service Catch Block " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                SetProcessingTimer(TimerInterval);
            }
        }

    }
}
