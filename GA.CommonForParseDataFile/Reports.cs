﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataLayer;

namespace GA.CommonForParseDataFile
{
    public class Reports
    {
        public StringBuilder GenerateReport()
        {
            StringBuilder strBuilder = new StringBuilder();
            try
            {
                
                int dayOfWeek = (int)DateTime.Now.DayOfWeek;
                if (dayOfWeek == 3) //Check for Monday
                {
                   
                    DateTime dateFrom = DateTime.Now.AddDays(-7).Date;
                    DateTime dateFromMonthly = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime dateFromYearly = new DateTime(DateTime.Now.Year, 1, 1);
                    DateTime dateTo = DateTime.Now.Date;
                    string totalMinFlownWeekly = "";
                    string totalMinFlownMonthly = "";
                    string totalMinFlownYearly = "";
                    int hoursFlown = 0;
                    int customerRegisteredInWeek = 0;
                    int customerRegisteredInMonth = 0;
                    int customerRegisteredInYear = 0;
                    int activeAircraftInWeek = 0;
                    int activeAircraftInMonth = 0;
                    int activeAircraftInYear = 0;


                    var context = new GuardianAvionicsEntities();
                    var profileList = context.Profiles.Where(w => !w.Deleted && w.DateCreated >= dateFromYearly && w.DateCreated < dateTo).ToList();

                    if (profileList != null)
                    {
                        customerRegisteredInYear = profileList.Count;

                        var objTemp = profileList.Where(w => !w.Deleted && w.DateCreated >= dateFromMonthly && w.DateCreated < dateTo).ToList();
                        if (objTemp != null)
                        {
                            customerRegisteredInMonth = objTemp.Count;
                        }
                        objTemp = profileList.Where(w => !w.Deleted && w.DateCreated >= dateFrom && w.DateCreated < dateTo).ToList();
                        if (objTemp != null)
                        {
                            customerRegisteredInWeek = objTemp.Count;
                        }
                        
                    }

                    TimeSpan time = TimeSpan.FromMinutes(0);
                    var pilotLogList = context.PilotLogs.Where(p => p.FlightDataType == (int)Common.FlightDataType.StoredData && p.Finished && p.Date>= dateFromYearly && p.Date < dateTo).ToList();
                    if (pilotLogList != null)
                    {
                        pilotLogList.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                        time = TimeSpan.FromMinutes(hoursFlown);
                        totalMinFlownYearly = time.ToString(@"hh\:mm\:ss");

                        activeAircraftInYear = pilotLogList.Select(s => s.AircraftId).Distinct().Count();

                         var objTemp = pilotLogList.Where(p => p.FlightDataType == (int)Common.FlightDataType.StoredData && p.Finished && p.Date >= dateFromMonthly && p.Date < dateTo).ToList();
                        if (objTemp != null)
                        {
                            hoursFlown = 0;
                            objTemp.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                            time = TimeSpan.FromMinutes(hoursFlown);
                            totalMinFlownMonthly = time.ToString(@"hh\:mm\:ss");
                            activeAircraftInMonth = objTemp.Select(s => s.AircraftId).Distinct().Count();
                        }
                        objTemp = pilotLogList.Where(p => p.FlightDataType == (int)Common.FlightDataType.StoredData && p.Finished && p.Date >= dateFrom && p.Date < dateTo).ToList();
                        if (objTemp != null)
                        {
                            hoursFlown = 0;
                            objTemp.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                            time = TimeSpan.FromMinutes(hoursFlown);
                            totalMinFlownWeekly = time.ToString(@"hh\:mm\:ss");
                            activeAircraftInWeek = objTemp.Select(s => s.AircraftId).Distinct().Count();
                        }
                    }

                    strBuilder.Append("<tr bgcolor=\"#c8e2eb\">");
                    strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"></td>");
                    strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Weekly</strong><br>" + dateFrom.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                    strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Monthly</strong><br>" + dateFromMonthly.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                    strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Monthly</strong><br>" + dateFromYearly.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                    strBuilder.Append("</tr>");

                    strBuilder.Append("<tr>");
                    strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Customer Registered</strong></td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">"+ customerRegisteredInWeek + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + customerRegisteredInMonth + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + customerRegisteredInYear + "</td>");
                    strBuilder.Append("</tr>");

                    strBuilder.Append("<tr bgcolor=\"#c8e2eb\">");
                    strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Total hours flown</strong></td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownWeekly + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownMonthly + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownYearly + "</td>");
                    strBuilder.Append("</tr>");

                    strBuilder.Append("<tr>");
                    strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Active Aircrafts</strong></td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInWeek + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInMonth + "</td>");
                    strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInYear + "</td>");
                    strBuilder.Append("</tr>");
                }
            }
            catch (Exception ex)
            {
                return new StringBuilder();
            }
            return strBuilder;
        }

       


    }
}
