﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ApplicationBlocks.Data;
using GA.DataLayer;


namespace GA.CommonForParseDataFile
{
    public class Common
    {
        string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];

        public string ConvertMinToHHMM(double totalMinutes)
        {
            string minute = "0";
            string hours = "0";

            if (totalMinutes < 60)
            {
                minute = Convert.ToString(totalMinutes);
            }
            else
            {
                minute = Convert.ToString(totalMinutes % 60);
            }

            hours = Convert.ToString(Convert.ToInt32((Int32)totalMinutes / 60));
            if (Convert.ToDouble(hours) < 1)
            {
                hours = "00";
            }
            else if (Convert.ToDouble(hours) < 10)
            {
                hours = "0" + hours;
            }

            if (Convert.ToDouble(minute) < 10)
            {
                minute = "0" + minute;
            }

            //return (hours + ":" + minute + ":00");
            return (hours + ":" + minute + ":00");
        }


        public string GetFlightRoute(double sourceLatitude, double sourceLogitude, double destinationLatitude, double destinationLonitude)
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter();
            param[0].ParameterName = "@sourceLatitude";
            param[0].SqlDbType = SqlDbType.Float;
            param[0].Value = sourceLatitude;

            param[1] = new SqlParameter();
            param[1].ParameterName = "@sourceLogitude";
            param[1].SqlDbType = SqlDbType.Float;
            param[1].Value = sourceLogitude;

            param[2] = new SqlParameter();
            param[2].ParameterName = "@destinationLatitude";
            param[2].SqlDbType = SqlDbType.Float;
            param[2].Value = destinationLatitude;

            param[3] = new SqlParameter();
            param[3].ParameterName = "@destinationLonitude";
            param[3].SqlDbType = SqlDbType.Float;
            param[3].Value = destinationLonitude;

            string flightRoute = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spGetFlightRoute", param));

            return flightRoute;
        }


        public enum FlightDataType
        {
            LiveData = 1,
            StoredData = 2
        }

        public void SetLiveDataFlightStatus()
        {
            try
            {
                var context = new GuardianAvionicsEntities();


                int liveDataDiscardTime = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LiveDataDiscardTime"]);
                var currDate = DateTime.UtcNow.AddMinutes(liveDataDiscardTime * -1);
                var logList = context.PilotLogs.Where(p => p.FlightDataType == 1 && p.Finished &&  p.LastUpdated < currDate && p.LiveDataMaxSpeed < 50).ToList();


                foreach (var log in logList)
                {

                    //Delete live data for the flight
                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "@pilotLogId";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = log.Id;


                    param[1] = new SqlParameter();
                    param[1].ParameterName = "@aircraftId";
                    param[1].SqlDbType = SqlDbType.Int;
                    param[1].Value = log.AircraftId;

                    int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spDeleteLiveDataFlight", param));
                }


                int setFinishedIn = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LiveDataFlightFinishIn"]);
                currDate = DateTime.UtcNow.AddMinutes(setFinishedIn * -1);


                logList = context.PilotLogs.Where(p => p.FlightDataType == 1 && !p.Finished && p.LastUpdated < currDate).ToList();

                logList.ForEach(f => f.Finished = true);
                context.SaveChanges();
            }
            catch (Exception ex)
            { 
            
            }

        }

    }
}
