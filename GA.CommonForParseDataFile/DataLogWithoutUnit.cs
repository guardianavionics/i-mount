﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.CommonForParseDataFile
{
  public class DataLogWithoutUnit
    {
      public int SNO { get; set; }

      public DateTime Date { get; set; }

      public string DateString { get; set; }



      public string Latitude { get; set; }

      public string Longitude { get; set; }

      public string Speed { get; set; }

      public string Altitude { get; set; }

      public string Yaw { get; set; }

      public string Roll { get; set; }

      public string Pitch { get; set; }

      public string Time { get; set; }

      public string AircraftNNumber { get; set; }

      public string FlightNumber { get; set; }

      public int Id { get; set; }

      public string IsSecData { get; set; }
    }
}
