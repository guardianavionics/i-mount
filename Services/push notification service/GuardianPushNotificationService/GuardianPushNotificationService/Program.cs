﻿using System.ServiceProcess;

namespace GuardianPushNotificationService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] 
            { 
                new PushNotificationService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
