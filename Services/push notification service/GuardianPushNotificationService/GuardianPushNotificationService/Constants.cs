﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuardianPushNotificationService
{
    public class Constants
    {
            public const string ImagePathKey = "ImagePath";

            public const string LogPathKey = "LogPath";

            public const string AltLogPathKey = "AltLogPath";

            //public const string ImageServerPathKey = "ImageServerPath";

            public const string SchedulingTime = "SchedulingTime";
            // push notification

            //public const string IPhonePNServerUrl = "IPhonePNServerUrl";

            //public const string IPhonePort = "IPhonePort";

            //public const string IPhoneCerficatePath = "IPhoneCerficatePath";

            public const string CertificatePath = "CertificatePath";

            public const string IsDistribution = "IsDistribution";

    }

}
