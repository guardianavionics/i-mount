﻿using System;
using System.ServiceProcess;
using System.Timers;
using BL_AL;

namespace GuardianPushNotificationService
{
    public partial class PushNotificationService : ServiceBase
    {
        Timer timer = new Timer();

        public PushNotificationService()
        {
           // System.Diagnostics.Debugger.Launch();
            InitializeComponent();

            if (!System.Diagnostics.EventLog.SourceExists("GuardianServicePush"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "GuardianServicePush", "GuardainPushLog");
            }
            eventLog1.Source = "GuardianServicePush";
            eventLog1.Log = "GuardainPushLog";
        }

        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            eventLog1.WriteEntry("In OnStart");
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            int time = Convert.ToInt32(ConfigurationReader.SchedulingTime);
            time = time == 0 ? 60 : time;
            // in milliseconds , default is 100 milli seconds
            timer.Interval = 1000 * 60 * time; // 1 min * time
            ExceptionHandler.ReportError(new Exception("Time Interval " + timer.Interval.ToString()));
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In onStop.");
        }

        /// <summary>
        /// Performs Fuction when time of timer elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            eventLog1.WriteEntry("In timer_Elapsed.");

            ExceptionHandler.ReportError(new Exception("Push Notification service timer_Elapsed"));

            try
            {
             //  System.Diagnostics.Debugger.Launch();
                PushNotificationMessage obj = new PushNotificationMessage();
                obj.SendPushNotification();
                AircraftRegistrationData regData = new AircraftRegistrationData();
                regData.InsertRegistrationData();
            }
            catch (Exception e1)
            {
                ExceptionHandler.ReportError(e1);
            }

        } // timer_Elapsed

    }
}