﻿namespace BL_AL
{
    // Id , profile Id , Text = rating with license #licenseRefNo is expiring in x days , Identity = 0.             
    public class mlrList
    {
        public int id { get; set; }

        public int profileId { get; set; }

        public int identity { get; set; }

        public string text { get; set; }

        public string tokenId { get; set; }
    }
}
