﻿using System;
using System.Configuration;


namespace BL_AL
{


    /// <summary>
    /// Configuration Reader Class used to read values from web.config file of given key
    /// </summary>
    public class ConfigurationReader
    {
        /// <summary>
        /// Returns a trimmed string for the specified AppSettings key
        /// </summary>
        /// <param name="key">The App/Web key</param>
        /// <returns>value of the key</returns>
        public static string AppSetting(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key].Trim();
            }
            catch (Exception e)
            {
                ExceptionHandler.ReportError(e);
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Error logging path.
        /// </summary>
        /// <value>Path where Error is saved</value>
        public static string LogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.LogPathKey)))
                {
                    return AppSetting(Constants.LogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Alt Image Path value
        /// </summary>
        /// <value>Path where Alt Image is saved</value>
        public static string AltLogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AltLogPathKey)))
                {
                    return AppSetting(Constants.AltLogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get interval for service
        /// </summary>
        /// <value>time interval of service</value>
        public static string SchedulingTime
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.SchedulingTime)))
                {
                    return AppSetting(Constants.SchedulingTime);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Certifiate path for service
        /// </summary>
        /// <value>time interval of service</value>
        public static string CertificatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.CertificatePath)))
                {
                    return AppSetting(Constants.CertificatePath);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }



        /// <summary>
        /// Gets the web.config Key used to get ZipFile Web URL
        /// </summary>
        /// <value>time interval of service</value>
        public static string ZipFileURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ZipFileWebUrl)))
                {
                    return AppSetting(Constants.ZipFileWebUrl);
                }

                 
                return string.Empty;
            }
        }



        /// <summary>
        /// Gets the web.config Key used to get ZipFile path
        /// </summary>
        /// <value>time interval of service</value>
        public static string ZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ZipFilePath)))
                {
                    return AppSetting(Constants.ZipFilePath);
                }


                return string.Empty;
            }
        }




        /// <summary>
        /// Gets the web.config Key used to get ZipFile path
        /// </summary>
        /// <value>time interval of service</value>
        public static string UnZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.UnZipFilePath)))
                {
                    return AppSetting(Constants.UnZipFilePath);
                }


                return string.Empty;
            }
        }


        public static string IsPushNotificationForDistributionCertificate
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsPushNotificationForDistributionCertificate)))
                {
                    return AppSetting(Constants.IsPushNotificationForDistributionCertificate);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

    }
}