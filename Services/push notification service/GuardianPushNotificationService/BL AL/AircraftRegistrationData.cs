﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EntityFramework.BulkInsert.Extensions;
using System.Text;
using System.IO.Compression;
using System.Data.OleDb;
using System.Net;
using System.IO;
using System.Data;

namespace BL_AL
{
    public class AircraftRegistrationData
    {
        
        string source = ConfigurationReader.ZipFileURL;
        String ZipFilePath = ConfigurationReader.ZipFilePath;
        string UnZipFilePath = ConfigurationReader.UnZipFilePath;

        public void InsertRegistrationData()
        {
            ExceptionHandler.ReportError(new Exception("Start Extraction"), "---Debug-----");

            string month = "0" + DateTime.UtcNow.Month.ToString();
            month = month.Substring(month.Length - 2);

            string year = DateTime.UtcNow.Year.ToString();

            string fileNameWithExt = "AR" + month + year + ".zip";
            string fileNameWithoutExt = "AR" + month + year;


            if (System.IO.Directory.Exists(ZipFilePath + fileNameWithExt))
            {
                ExceptionHandler.ReportError(new Exception(fileNameWithExt + " Already Exists"), "---Debug-----");
                return;        //folder exists means we have already download the Zip file
            }
            else
            {
                ExceptionHandler.ReportError(new Exception(fileNameWithExt + " Folder Not Exists"), "---Debug-----");
            }

            ExceptionHandler.ReportError(new Exception("Continue"), "---Debug-----");
            //if (System.IO.Directory.Exists(UnZipFilePath + fileNameWithoutExt))
            //    System.IO.Directory.Delete(UnZipFilePath + fileNameWithoutExt, true);
            ////Delete the files from UnZip And ZipFiles Directory)
            //System.IO.File.Delete(ZipFilePath + fileNameWithExt);


            using (WebClient webClient = new WebClient())
            {
                ExceptionHandler.ReportError(new Exception("Start Zip Downloading"), "---Debug-----");
                webClient.DownloadFile(source + fileNameWithExt, ZipFilePath + fileNameWithExt);
                ExceptionHandler.ReportError(new Exception("Download Zip Successfully"), "---Debug-----");
            }

            //ZipFile.CreateFromDirectory(startPath, zipPath, System.IO.Compression.CompressionLevel.Fastest , true);

            bool isFileAvailable = true;
            try
            {
                ExceptionHandler.ReportError(new Exception("Start UnZip"), "---Debug-----");
                ZipFile.ExtractToDirectory(ZipFilePath + fileNameWithExt, UnZipFilePath + fileNameWithoutExt);
                ExceptionHandler.ReportError(new Exception("File UnZip Successfullyn"), "---Debug-----");
            }
            catch (Exception ex)
            {
                // If the file is not present on server then the code throws exception 
                // Now delete the zip file that is recently created.

                isFileAvailable = false;
            }

            if (!isFileAvailable)
            {
                System.IO.File.Delete(ZipFilePath + fileNameWithExt);
            }
            else
            {
                //Copy Schema.ini file to the newly Created Direciory
                System.IO.File.Copy(ZipFilePath + "Schema.ini", UnZipFilePath + fileNameWithoutExt + "\\Schema.ini");
                ExceptionHandler.ReportError(new Exception("Converting File Formate Extraction"), "---Debug-----");
                //Convert File Formate
                convertFileFormate(UnZipFilePath + "\\" + fileNameWithoutExt + "\\" + "ACFTREF.txt", UnZipFilePath + "\\" + fileNameWithoutExt + "\\" + "ACFTREFNew.txt");
                convertFileFormate(UnZipFilePath + "\\" + fileNameWithoutExt + "\\" + "MASTER.txt", UnZipFilePath + "\\" + fileNameWithoutExt + "\\" + "MASTERNew.txt");
                ExceptionHandler.ReportError(new Exception("File Formate Conversion Completed"), "---Debug-----");
                setAircraftData(UnZipFilePath + fileNameWithoutExt);

            }
            setAircraftData(UnZipFilePath + fileNameWithoutExt);

        }




        public void setAircraftData(string path)
        {


            ExceptionHandler.ReportError(new Exception("Start Inserting Data into Database for Master File"), "---Debug-----");
            string fileName = "";
            string dirName = path;
            try
            {
                // MASTERANSI mast
                fileName = "MASTERNew.txt";
                DataTable dt;
                using (OleDbConnection cn =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + fileName, cn))
                    {
                        dt = new DataTable("MASTER");
                        adapter.Fill(dt);
                    }
                }

                var airRef = dt.AsEnumerable()
                    .Select(o => new AircraftMasterFile
                    {
                        NNumber = Convert.ToString(o.Field<string>(0)),
                        SERIALNUMBER = Convert.ToString(o.Field<string>(1)),
                        MFR_MDL_CODE = Convert.ToString(o.Field<string>(2)),
                        ManufactureYear = Convert.ToString(o.Field<string>(4))

                    }).ToList();

                var options = new BulkInsertOptions
                {
                    NotifyAfter = 1000,
                };

                var context = new GuardianAvionicsEntities();
                ExceptionHandler.ReportError(new Exception("Deleting Data From the Tables"), "---Debug-----");
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [AircraftMasterFile]");
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [AircraftReferenceFile]");
                ExceptionHandler.ReportError(new Exception("Data Deleted Successfully"), "---Debug-----");
                context.BulkInsert(airRef);

                context.SaveChanges();

                context.Dispose();
                ExceptionHandler.ReportError(new Exception(" Inserting Data into Database for Master File Completed"), "---Debug-----");
            }
            catch (Exception e)
            {
                ExceptionHandler.ReportError(e);
            }

            try
            {
                ExceptionHandler.ReportError(new Exception("Start Inserting Data into Database for Aircrafr Reference File"), "---Debug-----");
                fileName = "ACFTREFNew.txt";
                DataTable dt;
                using (OleDbConnection cn1 =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn1.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter1 = new OleDbDataAdapter("SELECT * FROM " + fileName, cn1))
                    {
                        dt = new DataTable("MASTER");
                        adapter1.Fill(dt);
                    }
                }

                var airRef1 = dt.AsEnumerable()
                    .Select(o => new AircraftReferenceFile
                    {
                        MFR_MDL_CODE = o.Field<string>(0),
                        ManufacturerName = o.Field<string>(1),
                        ModelName = o.Field<string>(2),
                        NumberOfEngine = o.Field<string>(7),

                    }).ToList();

                var context = new GuardianAvionicsEntities();
                context.BulkInsert(airRef1);
                context.SaveChanges();
                context.Dispose();
                ExceptionHandler.ReportError(new Exception(" Inserting Data into Database for Aircraft Reference Completed"), "---Debug-----");

            }
            catch (Exception e)
            {

            }
        }


        public void convertFileFormate(string sourceFileName, string destinationFileName)
        {
            using (var sourceFile = new StreamReader(sourceFileName))
            {

                var destinationFile = new StreamWriter(
                    string.Format(destinationFileName), false, Encoding.GetEncoding(1250));

                try
                {
                    var lineCounter = 0;

                    string line;
                    while ((line = sourceFile.ReadLine()) != null)
                    {
                        line.Replace("\"", "");
                        destinationFile.WriteLine(line);
                        lineCounter++;
                    }
                }
                finally
                {
                    destinationFile.Dispose();
                }
            }
        }

    }
}