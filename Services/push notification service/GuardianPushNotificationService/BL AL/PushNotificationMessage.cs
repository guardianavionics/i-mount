﻿using System;
using System.Collections.Generic;
using System.Linq;
using PushSharp;
using PushSharp.Core;
using PushSharp.Apple;

namespace BL_AL
{
    public class PushNotificationMessage
    {
        public void SendPushNotification()
        {
            try
            {

                ExceptionHandler.ReportError(new Exception("In time elapsed"), "=============================================================================================================Push Notification service Logs");

                using (var context = new GuardianAvionicsEntities())
                {
                    ExceptionHandler.ReportError(new Exception(context.Configuration.ToString()));
                    //ExceptionHandler.ReportError(new Exception("created context."), "Push Notification service Logs");

                    /*
                     *  1 Find all 
                     *      a. rating which are going to expire in 3 days , not deleted , there license not deleted , there profile not deleted also license are not expired. 
                     *              we need Id , profile Id , Text = rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0.             
                     * 
                     *      b. license which are going to expire in 3 days , not deleted , there profile not deleted.
                     *              we need Id , profile Id and Text = license with reference Number #licenseRefNo is expiring in x days , Identity = 1.
                     *  
                     *      c. medical certificates which are going to expire in 3 days , not deleted , there profile not deleted.
                     *              we need Id = 0 , profileId , Text = medical certificate with reference Number #CertificateRefNo is expiring in x days , Identity = 2.
                     *   
                     *  2 Remove all the ratings for which license are included
                     *      
                     *  3 Add all the three list , Group them according to profile Id
                     *  
                     *  4 
                     *   pnList = new list<abcd>();
                     *   For each group check 
                     *      a. Is there any record in push table
                     *          a1. If no record then continue
                     *          a1. If record are there , is there any record which is valid , have token Id which is not black 
                     *              a11. If no then continue
                     *              a12. If yes then
                     *                  for each profile record
                     *                      pnList.add ( current record , profile record)
                     * 
                     * 
                     * 
                     * 5 add pnList in Push que.
                     * 
                     * 
                     * 
                     */

                    //rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0

                    // rating for which profile, license is not deleted , validUntil is not null , will expire in 3 days
                    var ExpiredRatings = context.Ratings.Where
                        (
                        rt => !rt.Deleted
                        && rt.ValidUntil != null
                        && !rt.License.Deleted
                        && !rt.License.Profile.Deleted
                        && rt.ValidUntil >= System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                        && rt.ValidUntil <= System.Data.Entity.DbFunctions.AddDays(System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow), 3)
                        && rt.License.ValidUntil > System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                        )
                        .Select(s =>
                            new mlrList
                            {
                                id = s.LicenseId,
                                identity = 0,
                                profileId = s.License.ProfileId,
                                text = "Rating for license with reference Number "
                                        + s.License.ReferenceNumber
                                        + " is expiring on "
                                        + System.Data.Entity.DbFunctions.TruncateTime(s.ValidUntil)
                                        + " ."
                            })
                        .ToList();
                    ExceptionHandler.ReportError(new Exception("ExpiredRating count =" + ExpiredRatings.Count()));

                    //foreach (var ef in ExpiredRatings)
                    //{
                    //    ExceptionHandler.ReportError(new Exception(ef.id.ToString() + "=ratings Id , profile Id = " + ef.profileId.ToString()), "Push Notification ExpiredRatings");
                    //}


                    // license with reference Number #licenseRefNo is expiring in x days , Identity = 1.

                    var ExpiredLicense = context.Licenses.Where
                        (
                            l => !l.Profile.Deleted
                            && !l.Deleted
                                //&& l.ValidUntil != null
                            && l.ValidUntil >= System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                            && l.ValidUntil <= System.Data.Entity.DbFunctions.AddDays(System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow), 3)
                        )
                        .Select(s =>
                            new mlrList
                            {
                                id = s.Id,
                                identity = 1,
                                profileId = s.ProfileId,
                                text = "License with reference Number "
                                        + s.ReferenceNumber
                                        + " is expiring on "
                                        + System.Data.Entity.DbFunctions.TruncateTime(s.ValidUntil)
                                        + " ."
                            })
                        .ToList();
                    ExceptionHandler.ReportError(new Exception("ExpiredLicense count =" + ExpiredLicense.Count()));
                    //foreach (var eg in ExpiredLicense)
                    //{
                    //    ExceptionHandler.ReportError(new Exception(eg.id.ToString() + "=License Id , profile Id = " + eg.profileId.ToString()), "Push Notification ExpiredLicense");
                    //}


                    // remove ratings for which license are expiring (are in ExpiredLicense list).
                    // a.id is rating's license id

                    ExpiredRatings = ExpiredRatings.Where(a => ExpiredLicense.All(el => el.id != a.id)).ToList();

                    //foreach (var eh in ExpiredRatings)
                    //{
                    //    ExceptionHandler.ReportError(new Exception(eh.id.ToString() + "=selected ratings Id , profile Id = " + eh.profileId.ToString()), "Push Notification ExpiredRatings after removing ratings for which ExpiredLicense if exist");
                    //}


                    //medical certificate with reference Number #CertificateRefNo is expiring in x days

                    var ExpiredM = context.MedicalCertificates.Where
                        (
                            m => !m.Deleted
                                && !m.Profile.Deleted
                                && m.ValidUntil != null
                                && m.ValidUntil >= System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                                && m.ValidUntil <= System.Data.Entity.DbFunctions.AddDays(System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow), 3)
                        )
                        .Select(s =>
                            new mlrList
                            {
                                id = s.Id,
                                identity = 2,
                                profileId = s.ProfileId,
                                text = "Medical certificate with reference Number "
                                        + s.ReferenceNumber
                                        + " is expiring on "
                                        + System.Data.Entity.DbFunctions.TruncateTime(s.ValidUntil)
                                        + " ."
                            })
                        .ToList();
                    ExceptionHandler.ReportError(new Exception("ExpiredM count =" + ExpiredM.Count()));
                    //foreach (var ei in ExpiredM)
                    //{
                    //    ExceptionHandler.ReportError(new Exception(ei.id.ToString() + "= medical certificate Id , profile Id = " + ei.profileId.ToString()), "Push Notification ExpiredM");
                    //}


                    ExceptionHandler.ReportError(new Exception("===================================================push notification queue=============================================================="), "Push Notification queue entries");


                    // combine all the list into one
                    var combinedList = new List<mlrList>();

                    combinedList.AddRange(ExpiredRatings);
                    combinedList.AddRange(ExpiredLicense);
                    combinedList.AddRange(ExpiredM);

                    // group by profile id
                    //var groupList = combinedList.GroupBy(a => a.profileId);
                    ExceptionHandler.ReportError(new Exception( "D"));
                    var groupList = from cl in combinedList
                                    group cl by cl.profileId into grp
                                    orderby grp.Key
                                    select grp;

                    // final output list
                    var completeList = new List<mlrList>();

                    foreach (var group in groupList)
                    {

                        //if rateGroup is empty then continue
                        // if it has any element then group.first() will not we null
                        if (!group.Any())
                            continue;

                        var profileId = group.First().profileId;
                        var push = context.PushNotifications.Where(pr => pr.ProfileId == profileId).ToList();

                        //ExceptionHandler.ReportError(new Exception("Profile Id of group" + profileId.ToString()), "Push Notification service Logs");

                        var pushList = new List<PushNotification>();
                        //if profile is empty or push notification is not enabled or tokeinId is empty then continue

                        if (push.Any())
                            pushList.AddRange(push.Where(p => p != default(PushNotification) && p.IsValid && !string.IsNullOrEmpty(p.TokenId)));

                        if (pushList.Any())
                        {
                            foreach (var element in group)
                            {
                                completeList.AddRange(pushList.Select(p => CreateMLRListObject(element, p.TokenId)));
                                //pushList.ForEach(a => combinedList.Add(CreateMLRListObject(element, a.TokenId)));
                            }

                            //foreach (var element in group)
                            //{
                            //    //ExceptionHandler.ReportError(new Exception("Element Profile Id" + element.profileId.ToString()), "Push Notification service Logs");
                            //    //ExceptionHandler.ReportError(new Exception("Element Id" + element.id.ToString()), "Push Notification service Logs");
                            //    //ExceptionHandler.ReportError(new Exception("Element Identity" + element.identity.ToString()), "Push Notification service Logs");
                            //    foreach (var p in pushList)
                            //    {
                            //        // add token to mlrList and add it to combinedList
                            //        completeList.Add(CreateMLRListObject(element, p.TokenId));
                            //    }
                            //}


                        }
                        ExceptionHandler.ReportError(new Exception("CompleteList Count =" + completeList.Count()));

                        // send push notification for all the elements of this group
                        if (completeList.Any())
                            QueueMessages(completeList);

                        completeList = new List<mlrList>();
                        //combinedList.Clear();
                        //pushList.Clear();
                    }
                }// using

                ExceptionHandler.ReportError(new Exception("=============================================Response from apple server===================================================="));

            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(ex);
            }

        }

        /// <summary>
        /// adds token to mlrList object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="token"></param>
        /// <returns>mlrList object</returns>
        public mlrList CreateMLRListObject(mlrList obj, string token)
        {
            // we are checking token before sending so it cant be null
            return new mlrList
            {
                tokenId = token,
                text = obj.text.Replace(" 00:00:00.0000000", ""),
                profileId = obj.profileId,
                identity = obj.identity,
                id = obj.id,
            };
        }

        // sends push notifications
        private void QueueMessages(IEnumerable<mlrList> combinedList)
        {
            ExceptionHandler.ReportError(new Exception("F"));
            //create the puchbroker object
            var push = new PushBroker();

            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;


            // send push notification

            //-------------------------
            // APPLE NOTIFICATIONS
            //-------------------------
            //Configure and start Apple APNS
            // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to
            //generate one for connecting to Sandbox, and one for connecting to Production.  You must
            // use the right one, to match the provisioning profile you build your
            //   app with!
            try
            {
                var path = ConfigurationReader.CertificatePath;
                bool isPushNotificationForDistributionCertificate = Convert.ToBoolean(ConfigurationReader.IsPushNotificationForDistributionCertificate);
                var appleCert = System.IO.File.ReadAllBytes(path);

                //IMPORTANT: If you are using a Development provisioning Profile, you must use
                // the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                // 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                //Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                //'true')
                push.RegisterAppleService(new ApplePushChannelSettings(isPushNotificationForDistributionCertificate, appleCert, ""));
                //Extension method
                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                // generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, 
                // and the device token is passed back to you
                
                foreach (var msg in combinedList)
                {
                    if (!String.IsNullOrEmpty(msg.tokenId) && !String.IsNullOrEmpty(msg.text))
                        push.QueueNotification(new AppleNotification()
                                                    .ForDeviceToken(msg.tokenId)//the recipient device id
                                                    .WithAlert(msg.text)//the message
                                                    .WithBadge(0)
                                                    .WithSound("sound.caf")
                                                    );

                    ExceptionHandler.ReportError(new Exception(
                        "Element profile Id = " + msg.profileId.ToString() + 
                        " Element Id = " + msg.id + 
                        " identity = " + msg.identity + 
                        " device token = " + msg.tokenId +
                        " msg = " + msg.text
                        ), "Push Notification Elements to which msg sent");

                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(ex);
            }
        }

        // Events for sending push notification

        //Currently it will raise only for android devices
        static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId,
            INotification notification)
        {
            //Do something here
            ExceptionHandler.ReportError(new Exception("DeviceSubscriptionChanged Error Details"),
                " Device Token = " + ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken +
                " EnqueuedTimestamp.ToShortDateString = " + notification.EnqueuedTimestamp.ToShortDateString() +
                " EnqueuedTimestamp.ToShortTimeString = " + notification.EnqueuedTimestamp.ToShortTimeString()
                );
        }

        //this even raised when a notification is successfully sent
        static void NotificationSent(object sender, INotification notification)
        {
            //Do something here
            ExceptionHandler.ReportError(new Exception(
                " Device Token = " + ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken
                + " msg = " + ((PushSharp.Apple.AppleNotification)(notification)).Payload.Alert.Body
                + " date " + notification.EnqueuedTimestamp.ToShortDateString()
                )
                , " Push Notification service Notification Sent ");
        }

        //this is raised when a notification is failed due to some reason
        static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            //Do something here

            //notificationFailureException.

            //ExceptionHandler.ReportError(notificationFailureException, "Push Notification service exception NotificationFailed");

            //ExceptionHandler.ReportError(new Exception(((PushSharp.Apple.NotificationFailureException)(notificationFailureException).ErrorStatusCode).ToString()));

            //ExceptionHandler.ReportError(new Exception("ErrorStatusDescription"), ((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).ErrorStatusDescription.ToString());

            //ExceptionHandler.ReportError(new Exception("EnqueuedTimestamp.ToShortDateString"), ((PushSharp.Core.Notification)(((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).Notification)).EnqueuedTimestamp.ToShortDateString());

            //ExceptionHandler.ReportError(new Exception("EnqueuedTimestamp.ToShortTimeString"), ((PushSharp.Core.Notification)(((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).Notification)).EnqueuedTimestamp.ToShortTimeString());

            //ExceptionHandler.ReportError(new Exception("DeviceToken"), ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken.ToString());

            ExceptionHandler.ReportError(new Exception("Notification Failed ======================================================================== "),
                " Device Token = " + ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken +
                " ErrorStatusDescription = " + ((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).ErrorStatusDescription +
                " EnqueuedTimestamp.ToShortDateString = " + ((PushSharp.Core.Notification)(((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).Notification)).EnqueuedTimestamp.ToShortDateString() +
                " EnqueuedTimestamp.ToShortTimeString = " + ((PushSharp.Core.Notification)(((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).Notification)).EnqueuedTimestamp.ToShortTimeString() +
                " msg = " + ((PushSharp.Apple.NotificationFailureException)(notificationFailureException)).Notification.Payload.Alert.Body
                );

            //ExceptionHandler.ReportError(new Exception("Error Details"),
            //    " Device Token = " + ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken +
            //    " EnqueuedTimestamp.ToShortDateString = " + notification.EnqueuedTimestamp.ToShortDateString() +
            //    " EnqueuedTimestamp.ToShortTimeString = " + notification.EnqueuedTimestamp.ToShortTimeString()
            //    );

            //ExceptionHandler.ReportError(new Exception("EnqueuedTimestamp.ToShortDateString"), notification.EnqueuedTimestamp.ToShortDateString());

            //ExceptionHandler.ReportError(new Exception("EnqueuedTimestamp.ToShortTimeString"),notification.EnqueuedTimestamp.ToShortTimeString());

            //ExceptionHandler.ReportError(new Exception("DeviceToken"), ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken);

            //ExceptionHandler.ReportError(notificationFailureException, "Push Notification service exception NotificationFailed");

        }

        //this is fired when there is exception is raised by the channel
        static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
            ExceptionHandler.ReportError(exception, "Push Notification service Channel Exception");

        }

        //this is fired when there is exception is raised by the service
        static void ServiceException(object sender, Exception exception)
        {
            //Do something here
            ExceptionHandler.ReportError(exception, "Push Notification service exception");
        }

        //this is raised when the particular device subscription is expired
        static void DeviceSubscriptionExpired(object sender,
        string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
            ExceptionHandler.ReportError(new Exception("DeviceSubscriptionExpired Error Details"),
            " Device Token = " + ((PushSharp.Apple.AppleNotification)(notification)).DeviceToken +
            " EnqueuedTimestamp.ToShortDateString = " + notification.EnqueuedTimestamp.ToShortDateString() +
            " EnqueuedTimestamp.ToShortTimeString = " + notification.EnqueuedTimestamp.ToShortTimeString()
            );

        }

        //this is raised when the channel is destroyed
        static void ChannelDestroyed(object sender)
        {
            //Do something here
            ExceptionHandler.ReportError(new Exception(), " Push Notification service ChannelDestroyed ");
        }

        //this is raised when the channel is created
        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
            ExceptionHandler.ReportError(new Exception(), " Push Notification service ChannelCreated ");
        }

    }
}


#region commented code

//try
//{

//    using (var context = new GuardianAvionicsEntities())
//    {
//        /*
//         *  1 Find all 
//         *      a. rating which are going to expire in 3 days , not deleted , there license not deleted , there profile not deleted also license are not expired. 
//         *              we need Id , profile Id , Text = rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0.             
//         * 
//         *      b. license which are going to expire in 3 days , not deleted , there profile not deleted.
//         *              we need Id , profile Id and Text = license with reference Number #licenseRefNo is expiring in x days , Identity = 1.
//         *  
//         *      c. medical certificates which are going to expire in 3 days , not deleted , there profile not deleted.
//         *              we need Id = 0 , profileId , Text = medical certificate with reference Number #CertificateRefNo is expiring in x days , Identity = 2.
//         *   
//         *  2 Remove all the ratings for which license are included
//         *      
//         *  3 Add all the three list , Group them according to profile Id
//         *  
//         *  4 
//         *   pnList = new list<abcd>();
//         *   For each group check 
//         *      a. Is there any record in push table
//         *          a1. If no record then continue
//         *          a1. If record are there , is there any record which is valid , have token Id which is not black 
//         *              a11. If no then continue
//         *              a12. If yes then
//         *                  for each profile record
//         *                      pnList.add ( current record , profile record)
//         * 
//         * 
//         * 
//         * 5 add pnList in Push que.
//         * 
//         * 
//         * 
//         */


//        //rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0

//        // rating for which profile, license is not deleted , validUntil is not null , will expire in 3 days
//        var ExpiredRatings = context.Ratings.Where
//            (
//            rt => rt.ValidUntil != null
//            && !rt.License.Deleted
//            && !rt.License.Profile.Deleted
//            && rt.ValidUntil > DateTime.UtcNow
//            && rt.ValidUntil < System.Data.Entity.DbFunctions.AddDays(rt.ValidUntil, 3)
//            && rt.License.ValidUntil > DateTime.UtcNow
//            )
//            .Select(s =>
//                new mlrList
//                {
//                    id = s.Id,
//                    identity = 0,
//                    profileId = s.License.ProfileId,
//                    text = "Rating for license with reference Number "
//                            + s.License.ReferenceNumber
//                            + " is expiring on "
//                            + s.ValidUntil
//                            + " ."
//                })
//            .ToList();

//        // license with reference Number #licenseRefNo is expiring in x days , Identity = 1.

//        var ExpiredLicense = context.Licenses.Where
//            (
//                l => !l.Profile.Deleted
//                && !l.Deleted
//                && l.ValidUntil != null
//                && l.ValidUntil > DateTime.UtcNow
//                && l.ValidUntil < System.Data.Entity.DbFunctions.AddDays(DateTime.UtcNow, 3)
//            )
//            .Select(s =>
//                new mlrList
//                {
//                    id = s.Id,
//                    identity = 1,
//                    profileId = s.ProfileId,
//                    text = "License with reference Number "
//                            + s.ReferenceNumber
//                            + " is expiring on "
//                            + s.ValidUntil
//                            + " ."
//                })
//            .ToList();

//        // remove ratings for which license are expiring (are in ExpiredLicense list).
//        ExpiredRatings = ExpiredRatings.Where(a => !ExpiredLicense.Any(el => el.id != a.id)).ToList();

//        //medical certificate with reference Number #CertificateRefNo is expiring in x days

//        var ExpiredM = context.MedicalCertificates.Where
//            (
//                m => !m.Deleted
//                    && !m.Profile.Deleted
//                    && m.ValidUntil != null
//                    && m.ValidUntil > DateTime.UtcNow
//                    && m.ValidUntil < System.Data.Entity.DbFunctions.AddDays(DateTime.UtcNow, 3)
//            )
//            .Select(s =>
//                new mlrList
//                {
//                    id = s.Id,
//                    identity = 2,
//                    profileId = s.ProfileId,
//                    text = "Medical certificate with reference Number "
//                            + s.ReferenceNumber
//                            + " is expiring on "
//                            + s.ValidUntil
//                            + " ."
//                })
//            .ToList();

//        // combine all the list into one
//        var combinedList = new List<mlrList>();

//        combinedList.AddRange(ExpiredRatings);
//        combinedList.AddRange(ExpiredLicense);
//        combinedList.AddRange(ExpiredM);

//        // group by profile id
//        var groupList = combinedList.GroupBy(a => a.profileId);

//        // final output list
//        var completeList = new List<mlrList>();

//        foreach (var group in groupList)
//        {
//            //if rateGroup is empty then continue
//            // if it has any element then group.first() will not we null
//            if (!group.Any())
//                continue;

//            var profileId = group.First().profileId;
//            var push = context.PushNotifications.Where(pr => pr.ProfileId == profileId).ToList();

//            var pushList = new List<PushNotification>();
//            //if profile is empty or push notification is not enabled or tokeinId is empty then continue
//            foreach (var p in push)
//            {
//                if (p == default(PushNotification) || !p.IsValid || !string.IsNullOrEmpty(p.TokenId))
//                    continue;
//                pushList.Add(p);
//            }

//            if (pushList.Any())
//            {
//                // fore each element send push notification to all the profiles associated with that profile.
//                foreach (var element in group)
//                {
//                    foreach (var p in pushList)
//                    {
//                        // add token to mlrList and add it to combinedList
//                        combinedList.Add(CreateMLRListObject(element, p.TokenId));
//                    }
//                }
//            }
//        }

//    }// using



//    // send push notification
//}
//catch (Exception e)
//{ 

//}

#endregion commented code
