﻿namespace BL_AL
{
    public class Constants
    {
        public const string LogPathKey = "LogPath";

        public const string AltLogPathKey = "AltLogPath";

        public const string SchedulingTime = "SchedulingTime";

        public const string CertificatePath = "CertificatePath";

        public const string ZipFileWebUrl = "ZipFileWebUrl";

        public const string ZipFilePath = "ZipFilePath";

        public const string UnZipFilePath = "UnZipFilePath";

        public const string IsPushNotificationForDistributionCertificate = "IsPushNotificationForDistributionCertificate";
    }
}