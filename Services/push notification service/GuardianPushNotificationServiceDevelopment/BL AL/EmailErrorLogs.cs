﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Data.Entity;
using System.Net.Mime;
using System.IO;
using System.Web.UI;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;


namespace BL_AL
{
    public class EmailErrorLogs
    {
        public void SendErrorLogs()
        {
            var cidLogoImage = Guid.NewGuid().ToString();
            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\emailErrorLog.html";
            ExceptionHandler.ReportError(new Exception("pathHtmlFile = " + pathHtmlFile), "");
            StringBuilder str = new StringBuilder();
            str.Append("");
            string bgColour = "";
            int i = 0;
            var context = new GuardianAvionicsEntities();

            DateTime prevoiusDate = DateTime.Now.AddDays(-1).Date;
            DateTime nextDate = DateTime.Now.Date;
            var errorLogList = context.ErrorLogs.Where(p => p.ErrorDate >= prevoiusDate && p.ErrorDate < nextDate).ToList();
            if (errorLogList.Count > 0)
            {
                foreach (var errorLog in errorLogList)
                {
                    if (i % 2 == 1)
                    {
                        bgColour = "";
                    }
                    else
                    {
                        bgColour = "bgcolor=\"#c8e2eb\"";
                    }
                    str.Append("<tr " + bgColour + "><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"><strong>" + errorLog.ModuleName + "</strong></td> <td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + errorLog.ErrorSource + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + errorLog.ErrorDetails + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + errorLog.RequestParameters + "</td></tr>");
                }
            }

            string strDate = prevoiusDate.ToShortDateString();
           
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string h1 = string.Format(htmlBody,
                str.ToString(),
                cidLogoImage,
                strDate

                );


            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            LinkedResource inline = new LinkedResource(ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png",
                  contentType: new ContentType("image/png"));
            inline.ContentId = cidLogoImage;//Guid.NewGuid().ToString();
            inline.ContentType.Name = "Logo";

            avHtml.LinkedResources.Add(inline);


            ExceptionHandler.ReportError(new Exception("Image Path = " + ConfigurationManager.AppSettings["ImagePath"] + Environment.NewLine + " Mail From  = " + ConfigurationManager.AppSettings["MailFrom"] + Environment.NewLine + "Mail To = " + ConfigurationManager.AppSettings["MailFrom"]), "");
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);

            mail.To.Add("atul.agrawal@ideavate.com,dhawal.dawar@ideavate.com");

            mail.Subject = "Error Log Report - " + DateTime.Now.AddDays(-1).ToShortDateString();
            mail.Body = h1;

            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailUserName"], System.Configuration.ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);

        }

    }
}