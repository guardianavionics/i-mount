﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace BL_AL
{
    public class Misc
    {
        public double CalculateLatitudeAndLongitude(double degree, double minute, double seconds, string strDirection)
        {
            int latsign = 1;


            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (seconds / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }


        public bool RemoteFileExists(string url)
        {
            bool exist = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create("https://nfdc.faa.gov/webContent/28DaySub/2017-05-25/APT.zip");
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    exist = response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch(Exception ex)
            {

            }
            return exist;
        }
    }
}