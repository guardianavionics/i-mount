//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BL_AL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ParseDataFile
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public Nullable<int> AircraftId { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string Remark { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<int> ProfileId { get; set; }
        public Nullable<long> UniqueId { get; set; }
    
        public virtual AircraftProfile AircraftProfile { get; set; }
    }
}
