﻿namespace BL_AL
{
    public class Constants
    {
        public const string LogPathKey = "LogPath";

        public const string AltLogPathKey = "AltLogPath";

        public const string SchedulingTime = "SchedulingTime";

        public const string CertificatePath = "CertificatePath";

        public const string ZipFileWebUrl = "ZipFileWebUrl";

        public const string ZipFilePath = "ZipFilePath";

        public const string UnZipFilePath = "UnZipFilePath";

        public const string IsPushNotificationForDistributionCertificate = "IsPushNotificationForDistributionCertificate";

        public const string APTFilePath = "APTFilePath";

        public const string NAVFilePath = "NAVFilePath";

        public const string AmazonAccessKey = "AmazonAccessKey";

        public const string AmazonSecretKey = "AmazonSecretKey";

        public const string S3Bucket = "S3Bucket";

        public const string BucketDocPath = "BucketDocPath";

        public const string S3BucketEnv = "S3BucketEnv";

        public const string BucketMapFilePath = "BucketMapFilePath";

        public const string s3BucketImages = "s3BucketImages";

        public const string s3BuckeyURL = "s3BuckeyURL";

        public const string BucketIPassVideo = "BucketIPassVideo";

        public const string BucketIPassImage = "BucketIPassImage";

        public const string BucketIPassPDF = "BucketIPassPDF";

        public const string BucketKmlFile = "BucketKmlFile";

        public const string BucketPlateJsonFolder = "BucketPlateJsonFolder";

        public const string BucketPlatePNGAndTxtFolder = "BucketPlatePNGAndTxtFolder";

        public const string BucketAudioFilePath = "BucketAudioFilePath";

        public const string BucketHardwareFirmwareFilePath = "BucketHardwareFirmwareFilePath";

        public const string s3BucketDefaultImages = "s3BucketDefaultImages";

        public const string ExportFilePath = "ExportFilePath";

        public const string DayOfWeekReport = "DayOfWeekReport";

        public const string ServiceScheduleHour = "ServiceScheduleHour";

        public const string ServiceScheduleMinutes = "ServiceScheduleMinutes";

    }
}