//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BL_AL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AirframeDatalog
    {
        public int Id { get; set; }
        public int PilotLogId { get; set; }
        public Nullable<System.DateTime> GPRMCDate { get; set; }
        public string DataLog { get; set; }
    
        public virtual PilotLog PilotLog { get; set; }
    }
}
