﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace BL_AL
{
    public class ExportExcel
    {
        public void ExportToCSV(System.Data.DataTable dt, string strFilePath)
        {
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
            }
            //evtLog.WriteEntry("filepath + name : " + strFilePath + fileName);
            StreamWriter sw = new StreamWriter(strFilePath, false);
            // Write the headers.
            int iColCount = dt.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write("\"" + dt.Columns[i] + "\"");
                if (i < iColCount - 1) sw.Write(",");
            }

            sw.Write(sw.NewLine);

            // Write rows.
            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write("\"" + dr[i].ToString() + "\"");
                    }
                    if (i < iColCount - 1) sw.Write(",");
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();

        }
    }
}