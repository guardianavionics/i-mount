﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BL_AL.Model
{
    public class DataIndex
    {
        public int StartIndex { get; set; }

        public int Length { get; set; }

        public string ColumnName { get; set; }
    }
}