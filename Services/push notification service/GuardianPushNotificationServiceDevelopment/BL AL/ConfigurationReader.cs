﻿using System;
using System.Configuration;

namespace BL_AL
{


    /// <summary>
    /// Configuration Reader Class used to read values from web.config file of given key
    /// </summary>
    public class ConfigurationReader
    {
        /// <summary>
        /// Returns a trimmed string for the specified AppSettings key
        /// </summary>
        /// <param name="key">The App/Web key</param>
        /// <returns>value of the key</returns>
        public static string AppSetting(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key].Trim();
            }
            catch (Exception e)
            {
                ExceptionHandler.ReportError(e);
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Error logging path.
        /// </summary>
        /// <value>Path where Error is saved</value>
        public static string LogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.LogPathKey)))
                {
                    return AppSetting(Constants.LogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Alt Image Path value
        /// </summary>
        /// <value>Path where Alt Image is saved</value>
        public static string AltLogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AltLogPathKey)))
                {
                    return AppSetting(Constants.AltLogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get interval for service
        /// </summary>
        /// <value>time interval of service</value>
        public static string SchedulingTime
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.SchedulingTime)))
                {
                    return AppSetting(Constants.SchedulingTime);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Certifiate path for service
        /// </summary>
        /// <value>time interval of service</value>
        public static string CertificatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.CertificatePath)))
                {
                    return AppSetting(Constants.CertificatePath);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }



        /// <summary>
        /// Gets the web.config Key used to get ZipFile Web URL
        /// </summary>
        /// <value>time interval of service</value>
        public static string ZipFileURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ZipFileWebUrl)))
                {
                    return AppSetting(Constants.ZipFileWebUrl);
                }

                 
                return string.Empty;
            }
        }



        /// <summary>
        /// Gets the web.config Key used to get ZipFile path
        /// </summary>
        /// <value>time interval of service</value>
        public static string ZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ZipFilePath)))
                {
                    return AppSetting(Constants.ZipFilePath);
                }


                return string.Empty;
            }
        }




        /// <summary>
        /// Gets the web.config Key used to get ZipFile path
        /// </summary>
        /// <value>time interval of service</value>
        public static string UnZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.UnZipFilePath)))
                {
                    return AppSetting(Constants.UnZipFilePath);
                }


                return string.Empty;
            }
        }


        public static string IsPushNotificationForDistributionCertificate
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsPushNotificationForDistributionCertificate)))
                {
                    return AppSetting(Constants.IsPushNotificationForDistributionCertificate);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }


        public static string AmazonAccessKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AmazonAccessKey)))
                {
                    return AppSetting(Constants.AmazonAccessKey);
                }
                return string.Empty;
            }
        }


        public static string AmazonSecretKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AmazonSecretKey)))
                {
                    return AppSetting(Constants.AmazonSecretKey);
                }
                return string.Empty;
            }
        }

        public static string S3Bucket
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.S3Bucket)))
                {
                    return AppSetting(Constants.S3Bucket);
                }
                return string.Empty;
            }
        }

        public static string BucketDocPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketDocPath)))
                {
                    return AppSetting(Constants.BucketDocPath);
                }
                return string.Empty;
            }
        }

        public static string S3BucketEnv
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.S3BucketEnv)))
                {
                    return AppSetting(Constants.S3BucketEnv);
                }
                return string.Empty;
            }
        }

        public static string BucketMapFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketMapFilePath)))
                {
                    return AppSetting(Constants.BucketMapFilePath);
                }
                return string.Empty;
            }
        }


        public static string s3BucketDefaultImages
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketDefaultImages)))
                {
                    return AppSetting(Constants.s3BucketDefaultImages);
                }
                return string.Empty;
            }
        }

        public static string s3BucketImages
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketImages)))
                {
                    return AppSetting(Constants.s3BucketImages);
                }
                return string.Empty;
            }
        }

        public static string s3BucketURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BuckeyURL)))
                {
                    return AppSetting(Constants.s3BuckeyURL);
                }
                return string.Empty;
            }
        }




        public static string BucketIPassVideo
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassVideo)))
                {
                    return AppSetting(Constants.BucketIPassVideo);
                }
                return string.Empty;
            }
        }
        public static string BucketIPassImage
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassImage)))
                {
                    return AppSetting(Constants.BucketIPassImage);
                }
                return string.Empty;
            }
        }
        public static string BucketIPassPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassPDF)))
                {
                    return AppSetting(Constants.BucketIPassPDF);
                }
                return string.Empty;
            }
        }

        public static string BucketKmlFile
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketKmlFile)))
                {
                    return AppSetting(Constants.BucketKmlFile);
                }
                return string.Empty;
            }
        }

        public static string BucketPlateJsonFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketPlateJsonFolder)))
                {
                    return AppSetting(Constants.BucketPlateJsonFolder);
                }
                return string.Empty;
            }
        }


        public static string BucketPlatePNGAndTxtFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketPlatePNGAndTxtFolder)))
                {
                    return AppSetting(Constants.BucketPlatePNGAndTxtFolder);
                }
                return string.Empty;
            }
        }

        public static string BucketAudioFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketAudioFilePath)))
                {
                    return AppSetting(Constants.BucketAudioFilePath);
                }
                return string.Empty;
            }
        }
        public static string BucketHardwareFirmwareFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketHardwareFirmwareFilePath)))
                {
                    return AppSetting(Constants.BucketHardwareFirmwareFilePath);
                }
                return string.Empty;
            }
        }

        public static string ExportFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ExportFilePath)))
                {
                    return AppSetting(Constants.ExportFilePath);
                }
                return string.Empty;
            }
        }


        public static string DayOfWeekReport
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.DayOfWeekReport)))
                {
                    return AppSetting(Constants.DayOfWeekReport);
                }
                return string.Empty;
            }
        }

        public static string ServiceScheduleHour
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ServiceScheduleHour)))
                {
                    return AppSetting(Constants.ServiceScheduleHour);
                }
                return string.Empty;
            }
        }

        public static string ServiceScheduleMinutes
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ServiceScheduleMinutes)))
                {
                    return AppSetting(Constants.ServiceScheduleMinutes);
                }
                return string.Empty;
            }
        }

        public static string APTFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.APTFilePath)))
                {
                    return AppSetting(Constants.APTFilePath);
                }
                return string.Empty;
            }
        }

        public static string NAVFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.NAVFilePath)))
                {
                    return AppSetting(Constants.NAVFilePath);
                }
                return string.Empty;
            }
        }

    }
}