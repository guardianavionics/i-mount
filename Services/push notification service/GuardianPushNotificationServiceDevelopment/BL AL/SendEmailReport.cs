﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Data.Entity;
using System.Net.Mime;
using System.IO;
using System.Web.UI;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace BL_AL
{
    public class SendEmailReport
    {
        public void EmailReport()
        {
            ExceptionHandler.ReportError(new Exception("EmailReport Method Call as"), "");
            var context = new GuardianAvionicsEntities();

            var cidLogoImage = Guid.NewGuid().ToString();


            DateTime prevoiusDate = DateTime.Now.AddDays(-1).Date;
            DateTime nextDate = DateTime.Now.Date;

            var pilotlog = context.PilotLogs.Include(x => x.AircraftProfile).Where(p => p.Date >= prevoiusDate && p.Date < nextDate).ToList().Select(s => new { AircraftId = s.AircraftId, DayPIC = ConvertHHMMToMinutes(s.DayPIC), Date = s.Date, AircraftRegNo = s.AircraftProfile.Registration, SerialNo = s.AircraftProfile.AircraftSerialNo });


            var reportData = pilotlog.GroupBy(a => new { a.AircraftId, a.AircraftRegNo, a.SerialNo })
                .Select(a => new { daypic = ConvertMinToHHMM(a.Sum(b => (b.DayPIC))), AircraftRegNo = a.Key.AircraftRegNo, SerialNo = a.Key.SerialNo })
                                    .ToList();

            if (reportData.Count == 0)
            {
                return;
            }
            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\email.html";
            ExceptionHandler.ReportError(new Exception("pathHtmlFile = " + pathHtmlFile), "");
            StringBuilder str = new StringBuilder();
            str.Append("");
            int i = 0;
            string bgColour = "";
            foreach (var data in reportData)
            {
                if (i % 2 == 1)
                {
                    bgColour = "";
                }
                else
                {
                    bgColour = "bgcolor=\"#c8e2eb\"";
                }
                str.Append("<tr " + bgColour + "><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"><strong>" + data.AircraftRegNo + "</strong></td> <td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.SerialNo + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.daypic + "</td></tr>");
            }

           
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string h1 = string.Format(htmlBody,
                str.ToString(),
                cidLogoImage,
                DateTime.Now.AddDays(-1).ToShortDateString(),
                "Daily"
                );


            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            LinkedResource inline = new LinkedResource(ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png",
                  contentType: new ContentType("image/png"));
            inline.ContentId = cidLogoImage;//Guid.NewGuid().ToString();
            inline.ContentType.Name = "Logo";

            avHtml.LinkedResources.Add(inline);


            ExceptionHandler.ReportError(new Exception("Image Path = " + ConfigurationManager.AppSettings["ImagePath"] + Environment.NewLine + " Mail From  = " + ConfigurationManager.AppSettings["MailFrom"] + Environment.NewLine + "Mail To = " + ConfigurationManager.AppSettings["MailFrom"]), "");
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);

            mail.To.Add(ConfigurationManager.AppSettings["MailTo"]);
            mail.CC.Add(ConfigurationManager.AppSettings["MailToAlternate"]);


            mail.Subject = "Daily Flight Report - " + DateTime.Now.AddDays(-1).ToShortDateString();
            mail.Body = h1;

            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailUserName"], System.Configuration.ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);


            DateTime dt = DateTime.Now;
            if (dt.DayOfWeek.ToString() == "Monday")
            {
                WeeklyReportSendToAllAircraftOwner();
                EmailReportWeekly();
            }
        }

        public void EmailReportWeekly()
        {

            ExceptionHandler.ReportError(new Exception("EmailWeeklyReport Method Call"), "");

            var context = new GuardianAvionicsEntities();

            var cidLogoImage = Guid.NewGuid().ToString();

            DateTime prevoiusDate = DateTime.Now.AddDays(-7).Date;
            DateTime nextDate = DateTime.Now.Date;

            var pilotlog = context.PilotLogs.Include(x => x.AircraftProfile).Where(p => p.Date >= prevoiusDate && p.Date < nextDate).ToList().Select(s => new { AircraftId = s.AircraftId, DayPIC = ConvertHHMMToMinutes(s.DayPIC), Date = s.Date, AircraftRegNo = s.AircraftProfile.Registration, SerialNo = s.AircraftProfile.AircraftSerialNo });


            var reportData = pilotlog.GroupBy(a => new { a.AircraftId, a.AircraftRegNo, a.SerialNo })
                .Select(a => new { daypic = ConvertMinToHHMM(a.Sum(b => (b.DayPIC))), AircraftRegNo = a.Key.AircraftRegNo, SerialNo = a.Key.SerialNo })
                                    .ToList();

            if (reportData.Count == 0)
            {
                return;
            }
            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\email.html";
            ExceptionHandler.ReportError(new Exception("pathHtmlFile = " + pathHtmlFile), "");
            StringBuilder str = new StringBuilder();
            str.Append("");
            int i = 0;
            string bgColour = "";
            foreach (var data in reportData)
            {
                if (i % 2 == 1)
                {
                    bgColour = "";
                }
                else
                {
                    bgColour = "bgcolor=\"#c8e2eb\"";
                }
                str.Append("<tr " + bgColour + "><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"><strong>" + data.AircraftRegNo + "</strong></td> <td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.SerialNo + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.daypic + "</td></tr>");
            }

          
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string h1 = string.Format(htmlBody,
                str.ToString(),
                cidLogoImage,
                ("from " + DateTime.Now.AddDays(-7).ToShortDateString() + " to " + DateTime.Now.AddDays(-1).ToShortDateString()),
                "Weekly"
                );


            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            LinkedResource inline = new LinkedResource(ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png",
                  contentType: new ContentType("image/png"));
            inline.ContentId = cidLogoImage;//Guid.NewGuid().ToString();
            inline.ContentType.Name = "Logo";

            avHtml.LinkedResources.Add(inline);


            ExceptionHandler.ReportError(new Exception("Image Path = " + ConfigurationManager.AppSettings["ImagePath"] + Environment.NewLine + " Mail From  = " + ConfigurationManager.AppSettings["MailFrom"] + Environment.NewLine + "Mail To = " + ConfigurationManager.AppSettings["MailFrom"]), "");
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);

            mail.To.Add(ConfigurationManager.AppSettings["MailTo"]);
            mail.CC.Add(ConfigurationManager.AppSettings["MailToAlternate"]);


            mail.Subject = "Weekly Flight Report - [ From " + DateTime.Now.AddDays(-7).ToShortDateString() + " To " + DateTime.Now.AddDays(-7).ToShortDateString() + " ]";
            mail.Body = h1;

            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailUserName"], System.Configuration.ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);
        }

        public class TempPilotLog
        {
            public int AircraftId { get; set; }
            public double TotalHoursFlown { get; set; }
            public DateTime Date { get; set; }
            public string AircraftRegNo { get; set; }
            public string SerialNo { get; set; }
            public string OwnerEmailId { get; set; }
            public int PilotLogId { get; set; }
            public double TotalGallons { get; set; }
            public double TotalMiles { get; set; }
        }

        public class WeeklyReport
        {
            public string TotalHoursFlown { get; set; }
            public double Totalgallons { get; set; }
            public double TotalMiles { get; set; }
            public string AircraftRegNo { get; set; }
            public string SerialNo { get; set; }
            public string OwnerEmailId { get; set; }
        }

        public void WeeklyReportSendToAllAircraftOwner()
        {
            var context = new GuardianAvionicsEntities();

            DateTime prevoiusDate = DateTime.Now.AddDays(-7).Date;
            DateTime nextDate = DateTime.Now.Date;
            var dataLogList = new List<DataLogListing>();
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            double gallon = 0.0;
            double prevLatitude = 0.0;
            double prevLongitude = 0.0;
            double totalMiles = 0.0;

            var pilotlog = context.PilotLogs.Include(x => x.AircraftProfile).Where(p => p.Date >= prevoiusDate && p.Date < nextDate && p.AircraftProfile.OwnerProfileId != null).ToList().Select(s => new TempPilotLog { AircraftId = s.AircraftId, TotalHoursFlown = ConvertHHMMToMinutes(s.DayPIC), Date = s.Date, AircraftRegNo = s.AircraftProfile.Registration, SerialNo = s.AircraftProfile.AircraftSerialNo, OwnerEmailId = s.AircraftProfile.Profile.EmailId, PilotLogId = s.Id, TotalGallons = 0.0, TotalMiles = 0.0 }).ToList();

            foreach (var obj in pilotlog)
            {
                var airframedatalog = context.AirframeDatalogs.Where(a => a.PilotLogId == obj.PilotLogId).Select(s => s.DataLog).ToList();
                if (airframedatalog.Count > 0)
                {
                    dataLogList.Clear();
                    foreach (var airframedata in airframedatalog)
                    {
                        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                    }
                    var dataLog = dataLogList.FirstOrDefault();
                    if (dataLog != null)
                    {
                        prevLatitude = Convert.ToDouble(dataLog.Latitude);
                        prevLongitude = Convert.ToDouble(dataLog.Longitude);
                    }
                    foreach (var objDataLoglist in dataLogList.Where(w => w.FF != null && w.FF != ""))
                    {
                        gallon += (Convert.ToDouble(string.IsNullOrEmpty(objDataLoglist.FF) ? 0.0 : Convert.ToDouble(objDataLoglist.FF)) / 3600);
                        totalMiles += distance(prevLatitude, prevLongitude, Convert.ToDouble(objDataLoglist.Latitude), Convert.ToDouble(objDataLoglist.Latitude), 'M');

                        prevLatitude = Convert.ToDouble(objDataLoglist.Latitude);
                        prevLongitude = Convert.ToDouble(objDataLoglist.Longitude);
                    }


                    obj.TotalGallons = Math.Round(gallon, 4); ;
                    obj.TotalMiles = Math.Round(totalMiles, 4);

                    gallon = 0.0;
                    totalMiles = 0.0;
                    prevLatitude = 0.0;
                    prevLongitude = 0.0;

                }
                else
                {
                    continue;
                }
            }

            var WeeklyReportData = pilotlog.GroupBy(a => new { a.AircraftId, a.AircraftRegNo, a.SerialNo, a.OwnerEmailId })
                .Select(a => new WeeklyReport { TotalHoursFlown = ConvertMinToOneTenthOFTime(a.Sum(b => (b.TotalHoursFlown))), Totalgallons = a.Sum(s => s.TotalGallons), TotalMiles = a.Sum(s => s.TotalMiles), AircraftRegNo = a.Key.AircraftRegNo, SerialNo = a.Key.SerialNo, OwnerEmailId = a.Key.OwnerEmailId })
                                    .ToList();

            var ownersId = WeeklyReportData.Select(s => s.OwnerEmailId).Distinct().ToList();

            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Content\\PDFReports");

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch(Exception ex)
                {
                
                }
            }
            foreach (var obj in ownersId)
            {
                //if (System.IO.File.Exists(System.Windows.Forms.Application.StartupPath + "\\Content\\WeeklyReport.pdf"))
                //{
                //    System.IO.File.Delete(System.Windows.Forms.Application.StartupPath + "\\Content\\WeeklyReport.pdf");
                //}
                SendEmailForWeeklyReport(WeeklyReportData.Where(w => w.OwnerEmailId == obj).ToList(), obj);
            }
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));
        }

        public string ConvertMinToHHMM(double totalMinutes)
        {
            string minute = "0";
            string hours = "0";

            if (totalMinutes < 60)
            {
                minute = Convert.ToString(totalMinutes);
            }
            else
            {

                minute = Convert.ToString(totalMinutes % 60);
            }

            hours = Convert.ToString(Convert.ToInt32((Int32)totalMinutes / 60));
            if (Convert.ToDouble(hours) < 1)
            {
                hours = "00";
            }
            else if (Convert.ToDouble(hours) < 10)
            {
                hours = "0" + hours;
            }

            if (Convert.ToDouble(minute) < 10)
            {
                minute = "0" + minute;
            }


            //return (hours + ":" + minute + ":00");
            return (hours + ":" + minute);
        }



        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::                                                                         :::
        //:::  This routine calculates the distance between two points (given the     :::
        //:::  latitude/longitude of those points). It is being used to calculate     :::
        //:::  the distance between two locations using GeoDataSource(TM) products    :::
        //:::                                                                         :::
        //:::  Definitions:                                                           :::
        //:::    South latitudes are negative, east longitudes are positive           :::
        //:::                                                                         :::
        //:::  Passed to function:                                                    :::
        //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
        //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
        //:::    unit = the unit you desire for results                               :::
        //:::           where: 'M' is statute miles (default)                         :::
        //:::                  'K' is kilometers                                      :::
        //:::                  'N' is nautical miles                                  :::
        //:::                                                                         :::
        //:::  Worldwide cities and other features databases with latitude longitude  :::
        //:::  are available at http://www.geodatasource.com                          :::
        //:::                                                                         :::
        //:::  For enquiries, please contact sales@geodatasource.com                  :::
        //:::                                                                         :::
        //:::  Official Web site: http://www.geodatasource.com                        :::
        //:::                                                                         :::
        //:::           GeoDataSource.com (C) All Rights Reserved 2015                :::
        //:::                                                                         :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        private double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        public static string ConvertMinToOneTenthOFTime(double min)
        {
            double response = (min / 60);
            response = Math.Round(Convert.ToDouble(response), 1, MidpointRounding.ToEven); // Rounds to even
            return response.ToString().Split('.').Length > 1 ? response.ToString() : (response.ToString() + ".0");
        }

        public void SendEmailForWeeklyReport(List<WeeklyReport> aircraftList, string OwnerEmailId)
        {
            var context = new GuardianAvionicsEntities();

            var profile = context.Profiles.FirstOrDefault(f => f.EmailId == OwnerEmailId);

            var ownername = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;

            var cidLogoImage = Guid.NewGuid().ToString();

            DateTime prevoiusDate = DateTime.Now.AddDays(-7).Date;
            DateTime nextDate = DateTime.Now.Date;

            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\WeeklyReportForOwner.html";
            ExceptionHandler.ReportError(new Exception("pathHtmlFile = " + pathHtmlFile), "");
            StringBuilder str = new StringBuilder();
            str.Append("");
            int i = 0;
            string bgColour = "";
            foreach (var data in aircraftList)
            {
                if (i % 2 == 1)
                {
                    bgColour = "";
                }
                else
                {
                    bgColour = "bgcolor=\"#c8e2eb\"";
                }
                str.Append("<tr " + bgColour + "><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"><strong>" + data.AircraftRegNo + "</strong></td> <td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.SerialNo + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.TotalHoursFlown + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.Totalgallons + "</td><td valign=\"top\" style=\"border-bottom:1px solid #ddd;\"> " + data.TotalMiles + "</td></tr>");
            }

           
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string h2 = "<table border=\"0\" align=\"center\" ><tr><td align=\"center\" ><table  border=\"0\" align=\"center\"  style=\"font-size:10px; font-family: Arial, Helvetica, sans-serif\"><tr><td>&nbsp;</td></tr><tr><td style=\"font-size:10px;\">&nbsp;</td></tr><tr><td bgcolor=\"#FFFFFF\"><table  border=\"0\" align=\"center\"><tr><td colspan=\"2\"><h1 style=\"font-size:inherit;\">Dear "+ownername+",</h1><p>The Pilot FMS app has recorded a completed flight summary for the date from "+ DateTime.Now.AddDays(-7).ToShortDateString() + " to " + DateTime.Now.AddDays(-1).ToShortDateString() + " and flight information is listed below.</p></td></tr><tr><td colspan=\"2\" style=\"border-bottom:3px solid #3e8ca7;\"><h2 style=\"color: #155165; font-style:italic; font-size: 18px; margin:0 0 10px;\">Weekly Flight Summary Details</h2> <table  width=\"400\" border=\"0\" style=\"border: 1px solid #ddd; border-bottom:0;font-size:10px;margin-top:15px;\"><tr bgcolor=\"#c8e2eb\"><td valign=\"top\" width=\"20%\" style=\"border-bottom:1px solid #ddd;\"><strong>Registration No.</strong></td>                                                <td valign=\"top\" width=\"20%\" style=\"border-bottom:1px solid #ddd;\">Serial No</td><td valign=\"top\" width=\"20%\" style=\"border-bottom:1px solid #ddd;\">Total Hours Flown</td><td valign=\"top\" width=\"20%\" style=\"border-bottom:1px solid #ddd;\">Total Gallons</td><td valign=\"top\" width=\"20%\" style=\"border-bottom:1px solid #ddd;\">Total Miles</td></tr>"+str.ToString()+"</table><p>View all your flight details at <a href=\"https://www.guardianavionicsdata.com/\" title=\"Guardian Avionics\" style=\"color:#00118c; font-style:italic; font-weight:bold; text-decoration:none;\" target=\"_blank\">www.guardianavionicsdata.com</a> and please do not forget to close your flight plan</p>                                        <p>We hope you enjoyed your flight.</p>                                        <p style=\"padding-bottom:15px;\">Guardian Avionics Flight Team</p>                                    </td>                                </tr>                                <!---------flight details section ends--------->                                <tr>                                    <td colspan=\"2\" style=\" border-bottom:2px solid #d2d2d2;\">                                        <h2 style=\"color: #155165; font-style:italic; font-size: inherit; margin:0;padding-top:20px;\">NEED HELP?</h2>                                        <p>Manage your account online <a href=\"https://www.guardianavionicsdata.com/\" title=\"Guardian Avionics\" style=\"color:#00118c; font-style:italic; font-weight:bold; text-decoration:none;\" target=\"_blank\">www.guardianavionicsdata.com</a></p>                                        <p>Email Support <a href=\"mailto:support@guardianavionicsdata.com\" title=\"Mail to Guardian Avionics\" style=\"color:#00118c;text-decoration:none;\" target=\"_blank\">Support@guardianavionicsdata.com</a></p>                                    </td>                                </tr>                                <tr>                                    <td colspan=\"2\" align=\"center\" style=\"padding-top:50px;\">                                      <!--  <a href=\"#\" target=\"_blank\" title=\"Login to Facebook\" style=\"margin-right:5px;\"><img src=\"socialicon_fb.jpg\" alt=\"Login to Facebook\" width=\"65\" height=\"23\" /></a>                                        <a href=\"#\" target=\"_blank\" title=\"Login to Twitter\" style=\"margin-left:5px;\"><img src=\"socialicon_twitter.jpg\" alt=\"Login to Twitter\" width=\"28\" height=\"29\" /></a>-->                                        <p style=\"color:#999999; padding:0; margin:0; font-size:12px; font-weight:bold; padding-bottom:30px;\">GuardianAvionics.com &#169; 2014 | All Right Reserved.</p>                                    </td>                                </tr>                            </table>                        </td>                    </tr>                </table>            </td>        </tr>    </table>";
            string h1 = string.Format(htmlBody,
               ownername, // str.ToString(),
                cidLogoImage,
                ("from " + DateTime.Now.AddDays(-7).ToShortDateString() + " to " + DateTime.Now.AddDays(-1).ToShortDateString()),
                "Weekly"
                );

           
            ////////////////////////////////////////////Create PDF File
            System.IO.StringWriter stringWrite = new StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            StringReader reader = new StringReader(h2);
            iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4, 20, 20, 20, 20);

            string imageURL = ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png";
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
            //Resize image depend upon your need
            jpg.ScaleToFit(140f, 120f);
            //Give space before image
            jpg.SpacingBefore = 10f;
            //Give some space after the image
            jpg.SpacingAfter = 1f;
            jpg.Alignment = Element.ALIGN_LEFT;

           string fileName = "WeeklyReport"+ DateTime.Now.Month + "-" + DateTime.Now.Day + "-"+ DateTime.Now.Year + "-ID=" + profile.Id +".pdf";

            HTMLWorker parser = new HTMLWorker(doc);
            // PdfWriter.GetInstance(doc, new FileStream(System.Windows.Forms.Application.StartupPath + "\\Content\\WeeklyReport.pdf", FileMode.Create));
            using (FileStream fs = new FileStream(ConfigurationManager.AppSettings["PDFFilePath"] + "PDFReports\\" + fileName, FileMode.Create, FileAccess.ReadWrite))
            {
                PdfWriter.GetInstance(doc, fs);

                doc.Open();
                doc.Add(jpg);
                parser.Parse(reader);
                doc.Close();
            }
            parser.Close();
            htmlWrite.Close();
            stringWrite.Close();


            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            LinkedResource inline = new LinkedResource(ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png",
                  contentType: new ContentType("image/png"));
            inline.ContentId = cidLogoImage;//Guid.NewGuid().ToString();
            inline.ContentType.Name = "Logo";

            avHtml.LinkedResources.Add(inline);


            ExceptionHandler.ReportError(new Exception("Image Path = " + ConfigurationManager.AppSettings["ImagePath"] + Environment.NewLine + " Mail From  = " + ConfigurationManager.AppSettings["MailFrom"] + Environment.NewLine + "Mail To = " + ConfigurationManager.AppSettings["MailFrom"]), "");
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);

            mail.To.Add(OwnerEmailId);
            mail.CC.Add(ConfigurationManager.AppSettings["MailToAlternate"]);


            mail.Subject = "Weekly Flight Report - [ From " + DateTime.Now.AddDays(-7).ToShortDateString() + " To " + DateTime.Now.AddDays(-7).ToShortDateString() + " ]";
            mail.Body = h1;

            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            mail.Attachments.Add(new Attachment(System.Windows.Forms.Application.StartupPath + "\\Content\\PDFReports\\" + fileName));
            smpt.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailUserName"], System.Configuration.ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);

        }


    }
}