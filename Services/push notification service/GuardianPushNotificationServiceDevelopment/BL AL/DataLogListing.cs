﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
namespace BL_AL
{
     [DataContract]
    public class DataLogListing
    {

        [Display(Name = "Lcl Date")]
        public string Date { get; set; }

        [Display(Name = "Lcl Time")]
        public string Time { get; set; }


        public long TickTime { get; set; }

        //[Display(Name ="UTCOfst")]
        //public String utcOfst {get;set;}

        //[Display(Name ="AltB")]
        //public String AltB {get;set;}

        //[Display(Name ="BaroA")]
        //public String BaroA{get;set;}

        //[Display(Name ="AltMSL")]
        //public String AltMSL{get;set;}

        //[Display(Name ="GndSpd")]
        //public String GndSpd{get;set;}

        [Display(Name = "VSpd")]
        public String VSpd { get; set; }

        [Display(Name = "Pitch")]
        public String Pitch { get; set; }

        [Display(Name = "Roll")]
        public String Roll { get; set; }

        [Display(Name = "Yaw")]
        public string Yaw { get; set; }

        //[Display(Name ="LatAc")]
        //public String LatAc{get;set;}

        //[Display(Name ="NormAc")]
        //public String NormAc{get;set;}

        //[Display(Name ="HDG")]
        //public String HDG{get;set;}

        //[Display(Name ="TRK")]
        //public String TRK{get;set;}

        //[Display(Name ="TAS")]
        //public String TAS{get;set;}

        //[Display(Name ="HSIS")]
        //public String HSIS{get;set;}

        //[Display(Name ="CRS")]
        //public String CRS{get;set;}

        //[Display(Name ="NAV1")]
        //public String NAV1{get;set;}

        //[Display(Name ="NAV2")]
        //public String NAV2{get;set;}

        //[Display(Name ="COM1")]
        //public String COM1{get;set;}

        //[Display(Name ="COM2")]
        //public String COM2{get;set;}

        //[Display(Name ="HCDI")]
        //public String HCDI{get;set;}

        //[Display(Name ="VCDI")]
        //public String VCDI{get;set;}

        //[Display(Name ="WndSpd")]
        //public String WndSpd{get;set;}

        //[Display(Name ="WndDr")]
        //public String WndDr{get;set;}

        //[Display(Name ="WptDst")]
        //public String WptDst{get;set;}

        //[Display(Name ="WptBrg")]
        //public String WptBrg{get;set;}

        //[Display(Name ="MagVar")]
        //public String MagVar{get;set;}

        //[Display(Name ="AfcsOn")]
        //public String AfcsOn{get;set;}

        //[Display(Name ="RollM")]
        //public String RollM{get;set;}

        //[Display(Name ="PitchM")]
        //public String PitchM{get;set;}

        //[Display(Name ="RollC")]
        //public String RollC{get;set;}

        //[Display(Name ="VSpdG")]
        //public String VSpdG{get;set;}

        //[Display(Name ="PichC")]
        //public String PichC {get;set;}

        //[Display(Name ="GPSfix")]
        //public String GPSfix{get;set;}

        //[Display(Name ="HAL")]
        //public String HAL{get;set;}

        //[Display(Name ="VAL")]
        //public String VAL{get;set;}

        //[Display(Name ="HPLwas")]
        //public String HPLwas{get;set;}

        //[Display(Name ="HPLfd")]
        //public String HPLfd{get;set;}

        //[Display(Name ="VPLwas")]
        //public String VPLwas{get;set;}

        //[Display(Name ="Volt2")]
        //public String Volt2{get;set;}

        //[Display(Name ="AMP2")]
        //public String AMP2 { get; set; }

        [Display(Name = "Latitude")]
        public string Latitude { get; set; }

        [Display(Name = "Longitude")]
        public string Longitude { get; set; }

        //[Display(Name = "WayPoint")]
        //public string WayPoint { get; set; }

        //[Display(Name = "AltGPS")]
        //public string GpsAltitude { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "AircraftNNumber")]
        public string AircraftNNumber { get; set; }

        [Display(Name = "Serial Number")]
        public string UnitSerialNumber { get; set; }

        //[Display(Name = "IAS")]
        //public string IAS { get; set; }

        //[Display(Name = "Volt")]
        //public string Volt1 { get; set; }

        //[Display(Name = "AMP1")]
        //public string AMP1 { get; set; }

        [Display(Name = "FQtyL")]
        public string FQL { get; set; }

        [Display(Name = "FQR")]
        public string FQR { get; set; }

        [Display(Name = "FUEL-F")]
        public string FF { get; set; }

        [Display(Name = "OIL-P")]
        public string OILP { get; set; }

        [Display(Name = "E1 MAP")]
        public string MAP { get; set; }

        [Display(Name = "E1 RPM")]
        public string RPM { get; set; }

        [Display(Name = "E1 CHT1")]
        public string Cht1 { get; set; }

        [Display(Name = "E1 CHT2")]
        public string Cht2 { get; set; }

        [Display(Name = "E1 CHT3")]
        public string Cht3 { get; set; }

        [Display(Name = "E1 CHT4")]
        public string Cht4 { get; set; }

        [Display(Name = "E1 CHT5")]
        public string Cht5 { get; set; }

        [Display(Name = "E1 CHT6")]
        public string Cht6 { get; set; }

        [Display(Name = "E1 EGT1")]
        public string Egt1 { get; set; }

        [Display(Name = "E1 EGT2")]
        public string Egt2 { get; set; }

        [Display(Name = "E1 EGT3")]
        public string Egt3 { get; set; }

        [Display(Name = "E1 EGT4")]
        public string Egt4 { get; set; }

        [Display(Name = "E1 EGT5")]
        public string Egt5 { get; set; }

        [Display(Name = "E1 EGT6")]
        public string Egt6 { get; set; }

        [Display(Name = "E1 TIT")]
        public string Tit { get; set; }

        [Display(Name = "OAT-C")]
        public string Oat { get; set; }


        [Display(Name = "Flight Detail")]
        public string Text { get; set; }

        public int pilotLogId { get; set; }

        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; }

        //[Display(Name="Unit Serial Number")]
        //public string UnitSerialNumber { get; set; }



        [Display(Name = "AMP")]
        public string AMP { get; set; }

        [Display(Name = "AMP2")]
        public string AMP2 { get; set; }

        //[Display(Name = "BAT")]
        //public string BAT { get; set; }

        [Display(Name = "CDT")]
        public string CDT { get; set; }

        [Display(Name = "CLD")]
        public string CLD { get; set; }

        [Display(Name = "END")]
        public string END { get; set; }

        [Display(Name = "FP")]
        public string FP { get; set; }

        [Display(Name = "HP")]
        public string HP { get; set; }

        [Display(Name = "IAT")]
        public string IAT { get; set; }

        [Display(Name = "MPG")]
        public string MPG { get; set; }

        [Display(Name = "WP REM")]
        public string REM { get; set; }

        [Display(Name = "WP REQ")]
        public string REQ { get; set; }

        [Display(Name = "RES")]
        public string RES { get; set; }

        [Display(Name = "USD")]
        public string USD { get; set; }

        [Display(Name = "H:M")]
        public string HM { get; set; }

        [Display(Name = "ALTITUDE")]
        public string Altitude { get; set; }

        [Display(Name = "SPEED")]
        public string Speed { get; set; }

        [Display(Name = "Oil-T")]
        public string OILT { get; set; }

        [Display(Name = "OP")]
        public string OP { get; set; }

        [Display(Name = "OT")]
        public string OT { get; set; }

        [Display(Name = "TIT-R")]
        public string TIT2 { get; set; }

        [Display(Name = "TIT-L")]
        public string TIT1 { get; set; }

        [Display(Name = "FUEL-F")]
        public string FUELF { get; set; }

        [Display(Name = "LAT")]
        public string LAT { get; set; }

        [Display(Name = "LNG")]
        public string LNG { get; set; }

        [Display(Name = "ALT")]
        public string ALT { get; set; }

        [Display(Name = "SPD")]
        public string SPD { get; set; }

        [Display(Name = "VOLTS")]
        public string VOLTS { get; set; }

        [Display(Name = "VOLTS2")]
        public string VOLTS2 { get; set; }

        [Display(Name = "GPH")]
        public string GPH { get; set; }

        [Display(Name = "ECON")]
        public string ECON { get; set; }

        [Display(Name = "SerialNo")]
        public string AircraftSerialNo { get; set; }

        [Display(Name = "Model")]
        public string AircraftModelNo { get; set; }

        [Display(Name = "CalculatedFuelRemaining")]
        public string CalculatedFuelRemaining { get; set; }

        [Display(Name = "TotalAircraftTime")]
        public string TotalAircraftTime { get; set; }

        [Display(Name = "EngineTime")]
        public string EngineTime { get; set; }

        [Display(Name = "ElevatorTrimPosition")]
        public string ElevatorTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator")]
        public string UnitsIndicator { get; set; }

        [Display(Name = "FlapPosition")]
        public string FlapPosition { get; set; }

        [Display(Name = "UnitsIndicator2")]
        public string UnitsIndicator2 { get; set; }

        [Display(Name = "CarbTemp")]
        public string CarbTemp { get; set; }

        [Display(Name = "UnitsIndicator3")]
        public string UnitsIndicator3 { get; set; }

        [Display(Name = "CoolantPressure")]
        public string CoolantPressure { get; set; }

        [Display(Name = "UnitsIndicator4")]
        public string UnitsIndicator4 { get; set; }

        [Display(Name = "CoolantTemperature")]
        public string CoolantTemperature { get; set; }

        [Display(Name = "UnitsIndicator5")]
        public string UnitsIndicator5 { get; set; }

        [Display(Name = "UnitsIndicator6")]
        public string UnitsIndicator6 { get; set; }

        [Display(Name = "AileronTrimPosition")]
        public string AileronTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator7")]
        public string UnitsIndicator7 { get; set; }

        [Display(Name = "RudderTrimPosition")]
        public string RubberTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator8")]
        public string UnitsIndicator8 { get; set; }

        [Display(Name = "FuelQty3")]
        public string FuelQty3 { get; set; }

        [Display(Name = "UnitsIndicator9")]
        public string UnitsIndicator9 { get; set; }

        [Display(Name = "FuelQty4")]
        public string FuelQty4 { get; set; }

        [Display(Name = "UnitsIndicator10")]
        public string UnitsIndicator10 { get; set; }

        [Display(Name = "DiscreteInput1")]
        public string DiscreteInput1 { get; set; }

        [Display(Name = "DiscreteInput2")]
        public string DiscreteInput2 { get; set; }

        [Display(Name = "DiscreteInput3")]
        public string DiscreteInput3 { get; set; }

        [Display(Name = "DiscreteInput4")]
        public string DiscreteInput4 { get; set; }

        [Display(Name = "CRLF")]
        public string CRLF { get; set; }

        [Display(Name = "IgnStatus")]
        public string IgnStatus { get; set; }

        [Display(Name = "SensorStatus")]
        public string SensorStatus { get; set; }

        [Display(Name = "ThrottlePosition")]
        public string ThrottlePosition { get; set; }

        [Display(Name = "Baro")]
        public string Baro { get; set; }

        [Display(Name = "Airtemp")]
        public string Airtemp { get; set; }

        [Display(Name = "EcuTemp")]
        public string EcuTemp { get; set; }

        [Display(Name = "Batteryvoltage")]
        public string Batteryvoltage { get; set; }

        [Display(Name = "Sen1")]
        public string Sen1 { get; set; }

        [Display(Name = "Sen2")]
        public string Sen2 { get; set; }

        [Display(Name = "Sen3")]
        public string Sen3 { get; set; }

        [Display(Name = "Sen4")]
        public string Sen4 { get; set; }

        [Display(Name = "Sen5")]
        public string Sen5 { get; set; }
    }
}