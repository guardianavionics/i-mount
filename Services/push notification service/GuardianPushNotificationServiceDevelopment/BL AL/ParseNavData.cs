﻿using BL_AL.Model;
using Ionic.Zip;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;

namespace BL_AL
{
    public class ParseNavData
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
       public DateTime nextDate;
        public void CheckFileForDownload()
        {
            if (System.IO.File.Exists(ConfigurationReader.NAVFilePath + "FAANAV.zip"))
            {
                System.IO.File.Delete(ConfigurationReader.NAVFilePath + "FAANAV.zip");
            }
            Array.ForEach(Directory.GetFiles(ConfigurationReader.NAVFilePath + @"\UnZip\"),
          delegate (string path) { File.Delete(path); });

            var context = new GuardianAvionicsEntities();
            Misc objMisc = new Misc();
            var aptFileDownloadDetails = context.APTFileDownloadDetails.OrderByDescending(o => o.Id).FirstOrDefault(f=>f.FileType == "Nav");
            if (aptFileDownloadDetails == null)
            {
                //Check the file exist for the current month
                //File URL  https://nfdc.faa.gov/webContent/28DaySub/28DaySubscription_Effective_2017-04-27.zip

                DateTime currDate = new DateTime(2017, 5, 25);

                nextDate = currDate;
                string fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + currDate.ToString("yyyy-MM-dd") + "/NAV.zip";

                //Now check the file exist or not
                if (objMisc.RemoteFileExists(fileURL))
                {
                    DownloadFile(fileURL);
                }
                //else
                //{
                //    currDate = currDate.AddMonths(-1);
                //    fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + currDate.ToString("yyyy-MM-dd") + "/NAV.zip";
                //    //Now again check if the file exist or not
                //    if (objMisc.RemoteFileExists(fileURL))
                //    {
                //        DownloadFile(fileURL);
                //    }
                //}
            }
            else {
                //Now check that the file is available for the current data or not
                  nextDate = aptFileDownloadDetails.SubScriptionDate.AddDays(28);
                if (nextDate <= DateTime.Now)
                {
                    string fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + nextDate.ToString("yyyy-MM-dd") + "/NAV.zip";

                    //Now check the file exist or not
                    if (objMisc.RemoteFileExists(fileURL))
                    {
                        DownloadFile(fileURL);
                    }
                }
            }
        }

        public void DownloadFile(string fileURL)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.DownloadFile(new Uri(fileURL), ConfigurationReader.NAVFilePath + "FAANAV.zip");
            using (var zip = ZipFile.Read(ConfigurationReader.NAVFilePath + "FAANAV.zip"))
            {
                zip.ExtractProgress += zip_ExtractProgress;
                zip.ExtractAll(ConfigurationReader.NAVFilePath + "UnZip");
            }
        }

        public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                ParseAirportData();
            }
        }

        public void ParseAirportData()
        {
            List<DataIndex> listNav1 = new List<DataIndex>();
            listNav1.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav1.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });  //NAVAID FACILITY TYPE
            listNav1.Add(new DataIndex { ColumnName = "OfficalNavAID", StartIndex = 28, Length = 4 }); //OFFICIAL NAVAID FACILITY IDENTIFIER
            listNav1.Add(new DataIndex { ColumnName = "EffectiveDate", StartIndex = 32, Length = 10 });  //EFFECTIVE DATE; THIS DATE COINCIDES WITH THE 56 - DAY CHARTING AND PUBLICATION CYCLE EFFECTIVE DATE.
            listNav1.Add(new DataIndex { ColumnName = "Name", StartIndex = 42, Length = 30 });  //NAME OF NAVAID.(EX: WASHINGTON)
            listNav1.Add(new DataIndex { ColumnName = "City", StartIndex = 72, Length = 40 });  //CITY ASSOCIATED WITH THE NAVAID.
            listNav1.Add(new DataIndex { ColumnName = "State", StartIndex = 112, Length = 30 });  //TATE NAME WHERE ASSOCIATED CITY IS LOCATED (MAY NOT BE SAME STATE WHERE NAVAID IS LOCATED) (EX: DC)
            listNav1.Add(new DataIndex { ColumnName = "StatePOCode", StartIndex = 142, Length = 2 }); // STATE POST OFFICE CODE WHERE ASSOCIATED CITY IS LOCATED. (MAY NOT BE SAME STATE WHERE NAVAID IS LOCATED)(EX: DC)
            listNav1.Add(new DataIndex { ColumnName = "Country", StartIndex = 177, Length = 2 });
            listNav1.Add(new DataIndex { ColumnName = "Owner", StartIndex = 179, Length = 50 });  //NAVAID OWNER NAME  (EX: U.S. NAVY)
            listNav1.Add(new DataIndex { ColumnName = "Operator", StartIndex = 229, Length = 50 });  //NAVAID OPERATOR NAME (EX: U.S. NAVY)
            listNav1.Add(new DataIndex { ColumnName = "CommonSystemUsage", StartIndex = 279, Length = 1 });  //COMMON SYSTEM USAGE (Y OR N) DEFINES HOW THE NAVAID IS USED.
            listNav1.Add(new DataIndex { ColumnName = "PublicUse", StartIndex = 280, Length = 1 });  //NAVAID PUBLIC USE (Y OR N) DEFINES BY WHOM THE NAVAID IS USED
            listNav1.Add(new DataIndex { ColumnName = "NavAIDClass", StartIndex = 281, Length = 11 });  //CLASS OF NAVAID.
            listNav1.Add(new DataIndex { ColumnName = "HoursOfOperation", StartIndex = 292, Length = 11 });  //HOURS OF OPERATION OF NAVAID
            listNav1.Add(new DataIndex { ColumnName = "HighAltARTCCIdentifier", StartIndex = 303, Length = 4 });  //IDENTIFIER OF ARTCC WITH HIGH ALTITUDE BOUNDARY THAT THE NAVAID FALLS WITHIN.
            listNav1.Add(new DataIndex { ColumnName = "HighAltARTCCName", StartIndex = 307, Length = 30 });  //NAME OF ARTCC WITH HIGH ALTITUDE BOUNDARY THAT THE NAVAID FALLS WITHIN.
            listNav1.Add(new DataIndex { ColumnName = "LowAltARTCCIdentifier", StartIndex = 337, Length = 4 });  //IDENTIFIER OF ARTCC WITH LOW ALTITUDE BOUNDARY THAT THE NAVAID FALLS WITHIN.
            listNav1.Add(new DataIndex { ColumnName = "LowAltARTCCName", StartIndex = 341, Length = 30 });  //NAME OF ARTCC WITH LOW ALTITUDE BOUNDARY THAT THE NAVAID FALLS WITHIN.
            listNav1.Add(new DataIndex { ColumnName = "Latitude", StartIndex = 371, Length = 14 });  //NAVAID LATITUDE (FORMATTED)
            listNav1.Add(new DataIndex { ColumnName = "Longitude", StartIndex = 396, Length = 14 });  //NAVAID LONGITUDE (FORMATTED)
            listNav1.Add(new DataIndex { ColumnName = "LatLogSurveyAccuracyCode", StartIndex = 421, Length = 1 });  //LATITUDE/LONGITUDE SURVERY ACCURACY (CODE)
            listNav1.Add(new DataIndex { ColumnName = "LatitudeOfTacanPortionOfVortac", StartIndex = 422, Length = 14 });  //LATITUDE OF TACAN PORTION OF VORTAC WHEN TACAN IS NOT SITED WITH VOR(FORMATTED)
            listNav1.Add(new DataIndex { ColumnName = "LongitudeOfTacanPortionOfVortac", StartIndex = 447, Length = 14 });  //LONGITUDE OF TACAN PORTION OF VORTAC WHEN TACAN IS NOT SITED WITH VOR(FORMATTED)
            listNav1.Add(new DataIndex { ColumnName = "ElevationInTenthOfFoot", StartIndex = 472, Length = 7 });  //ELEVATION IN TENTH OF A FOOT (MSL)
            listNav1.Add(new DataIndex { ColumnName = "MagneticVariationDegree", StartIndex = 479, Length = 5 });  //MAGNETIC VARIATION DEGREES   (00-99)FOLLOWED BY MAGNETIC VARIATION DIRECTION(E, W)
            listNav1.Add(new DataIndex { ColumnName = "MagneticVariationEPOCHYear", StartIndex = 484, Length = 4 });  //MAGNETIC VARIATION EPOCH YEAR  (00-99)
            listNav1.Add(new DataIndex { ColumnName = "SimultaneousVoiceFeature", StartIndex = 488, Length = 3 });  //SIMULTANEOUS VOICE FEATURE (Y,N, OR NULL)
            listNav1.Add(new DataIndex { ColumnName = "PowerOutput", StartIndex = 491, Length = 4 });  //POWER OUTPUT (IN WATTS)
            listNav1.Add(new DataIndex { ColumnName = "AutoVoiceIdentificationFeature", StartIndex = 495, Length = 3 });  //AUTOMATIC VOICE IDENTIFICATION FEATURE (Y, N, OR NULL)
            listNav1.Add(new DataIndex { ColumnName = "MonitoringCategory", StartIndex = 498, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "RadioVoiceCall", StartIndex = 499, Length = 30 });  //RADIO VOICE CALL (NAME) (EX: WASHINGTON RADIO)
            listNav1.Add(new DataIndex { ColumnName = "TransmitOnChannel", StartIndex = 529, Length = 4 });  //CHANNEL (TACAN)  NAVAID TRANSMITS ON (EX : 051X)
            listNav1.Add(new DataIndex { ColumnName = "TransmitOnFrequency", StartIndex = 533, Length = 6 });  //FREQUENCY THE NAVAID TRANSMITS ON (EXCEPT TACAN) (EX:  110.60 298)
            listNav1.Add(new DataIndex { ColumnName = "RadioBeaconIdentifier", StartIndex = 539, Length = 24 });
            listNav1.Add(new DataIndex { ColumnName = "FanMarkerType", StartIndex = 563, Length = 10 });  //FAN MARKER TYPE (BONE OR ELLIPTICAL)
            listNav1.Add(new DataIndex { ColumnName = "FanMarkerAxis", StartIndex = 573, Length = 3 });  //TRUE BEARING OF MAJOR AXIS OF FAN MARKER
            listNav1.Add(new DataIndex { ColumnName = "ProtectedFrequencyAltitude", StartIndex = 576, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "LowAltitudeFacility", StartIndex = 577, Length = 3 });  //LOW ALTITUDE FACILITY USED IN HIGH STRUCTURE (Y, N, OR NULL)
            listNav1.Add(new DataIndex { ColumnName = "ZMarkerAvailable", StartIndex = 580, Length = 3 });  //NAVAID Z MARKER AVAILABLE (Y, N, OR NULL)
            listNav1.Add(new DataIndex { ColumnName = "TWEBHours", StartIndex = 583, Length = 9 });
            listNav1.Add(new DataIndex { ColumnName = "TWEBPhoneNo", StartIndex = 592, Length = 20 });
            listNav1.Add(new DataIndex { ColumnName = "AssociatedFSSId", StartIndex = 612, Length = 4 }); //ASSOCIATED / CONTROLLING FSS(IDENT)
            listNav1.Add(new DataIndex { ColumnName = "AssociatedFSSName", StartIndex = 616, Length = 30 });
            listNav1.Add(new DataIndex { ColumnName = "HoursOfOperationFSS", StartIndex = 646, Length = 100 });
            listNav1.Add(new DataIndex { ColumnName = "NotamAccountabilityCode", StartIndex = 746, Length = 4 });
            listNav1.Add(new DataIndex { ColumnName = "QuadrantIdentification", StartIndex = 750, Length = 16 });
            listNav1.Add(new DataIndex { ColumnName = "NavAidStatus", StartIndex = 766, Length = 30 });
            listNav1.Add(new DataIndex { ColumnName = "PitchFlag", StartIndex = 796, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "CatchFlag", StartIndex = 797, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "ATCAAFlag", StartIndex = 798, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "RestrictionFlag", StartIndex = 799, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "HiwasFlag", StartIndex = 800, Length = 1 });
            listNav1.Add(new DataIndex { ColumnName = "TWEBRestriction", StartIndex = 801, Length = 1 });


            List<DataIndex> listNav2 = new List<DataIndex>();
            listNav2.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav2.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });  //NAVAID FACILITY TYPE
            listNav2.Add(new DataIndex { ColumnName = "Remark", StartIndex = 28, Length = 600 });
            listNav2.Add(new DataIndex { ColumnName = "Filler", StartIndex = 628, Length = 174 });

            List<DataIndex> listNav3 = new List<DataIndex>();
            listNav3.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav3.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });
            listNav3.Add(new DataIndex { ColumnName = "Name", StartIndex = 28, Length = 36 });
            listNav3.Add(new DataIndex { ColumnName = "SpaceForFixes", StartIndex = 64, Length = 720 });

            List<DataIndex> listNav4 = new List<DataIndex>();
            listNav4.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav4.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });
            listNav4.Add(new DataIndex { ColumnName = "Name", StartIndex = 28, Length = 80 });
            listNav4.Add(new DataIndex { ColumnName = "Pattern", StartIndex = 108, Length = 3 });
            listNav4.Add(new DataIndex { ColumnName = "SpaceForHoldingPattern", StartIndex = 111, Length = 664 });

            List<DataIndex> listNav5 = new List<DataIndex>();
            listNav5.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav5.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });
            listNav5.Add(new DataIndex { ColumnName = "Name", StartIndex = 28, Length = 30 });
            listNav5.Add(new DataIndex { ColumnName = "SpaceForFanMarker", StartIndex = 58, Length = 690 });

            List<DataIndex> listNav6 = new List<DataIndex>();
            listNav6.Add(new DataIndex { ColumnName = "NavAID", StartIndex = 4, Length = 4 }); //NAVAID FACILITY IDENTIFIER
            listNav6.Add(new DataIndex { ColumnName = "NavAIDType", StartIndex = 8, Length = 20 });
            listNav6.Add(new DataIndex { ColumnName = "AirOrGroundCode", StartIndex = 28, Length = 2 });
            listNav6.Add(new DataIndex { ColumnName = "BearingOfCheckpoint", StartIndex = 30, Length = 3 });
            listNav6.Add(new DataIndex { ColumnName = "Altitude", StartIndex = 33, Length = 5 });
            listNav6.Add(new DataIndex { ColumnName = "AirportId", StartIndex = 38, Length = 4 });
            listNav6.Add(new DataIndex { ColumnName = "State", StartIndex = 42, Length = 2 });
            listNav6.Add(new DataIndex { ColumnName = "NarrativeDescriptionAir", StartIndex = 44, Length = 75 });
            listNav6.Add(new DataIndex { ColumnName = "NarrativeDescriptionGround", StartIndex = 119, Length = 75 });


            DataTable dtNav1 = new DataTable("Nav1");
            DataTable dtNav2 = new DataTable("Nav2");
            DataTable dtNav3 = new DataTable("Nav3");
            DataTable dtNav4 = new DataTable("Nav4");
            DataTable dtNav5 = new DataTable("Nav5");
            DataTable dtNav6 = new DataTable("Nav6");


            dtNav1.Columns.Add("NavAID", typeof(string));           //0
            dtNav1.Columns.Add("NavAIDType", typeof(string));       //1
            dtNav1.Columns.Add("OfficalNavAID", typeof(string));    //2
            dtNav1.Columns.Add("EffectiveDate", typeof(DateTime));  //3
            dtNav1.Columns[3].AllowDBNull = true;
            dtNav1.Columns.Add("Name", typeof(string));             //4
            dtNav1.Columns.Add("City", typeof(string));             //5
            dtNav1.Columns.Add("State", typeof(string));            //6
            dtNav1.Columns.Add("StatePOCode", typeof(string));      //7
            dtNav1.Columns.Add("Country", typeof(string));          //8
            dtNav1.Columns.Add("Owner", typeof(string));            //9
            dtNav1.Columns.Add("Operator", typeof(string));         //10
            dtNav1.Columns.Add("CommonSystemUsage", typeof(bool));  //11
            dtNav1.Columns[11].AllowDBNull = true;
            dtNav1.Columns.Add("PublicUse", typeof(bool));          //12
            dtNav1.Columns[12].AllowDBNull = true;
            dtNav1.Columns.Add("NavAIDClass", typeof(string));      //13
            dtNav1.Columns.Add("HoursOfOperation", typeof(string));    //14

            dtNav1.Columns.Add("HighAltARTCCIdentifier", typeof(string));   //15
            dtNav1.Columns.Add("HighAltARTCCName", typeof(string));         //16
            dtNav1.Columns.Add("LowAltARTCCIdentifier", typeof(string));    //17
            dtNav1.Columns.Add("LowAltARTCCName", typeof(string));          //18
            dtNav1.Columns.Add("Latitude", typeof(decimal));                //19
            dtNav1.Columns[19].AllowDBNull = true;
            dtNav1.Columns.Add("Longitude", typeof(decimal));               //20
            dtNav1.Columns[20].AllowDBNull = true;
            dtNav1.Columns.Add("LatLogSurveyAccuracyCode", typeof(string)); //21
            dtNav1.Columns.Add("LatitudeOfTacanPortionOfVortac", typeof(string));   //22
            dtNav1.Columns.Add("LongitudeOfTacanPortionOfVortac", typeof(string));  //23
            dtNav1.Columns.Add("ElevationInTenthOfFoot", typeof(decimal));          //24
            dtNav1.Columns[24].AllowDBNull = true;
            dtNav1.Columns.Add("MagneticVariationDegree", typeof(string));          //25
            dtNav1.Columns.Add("MagneticVariationEPOCHYear", typeof(int));          //26
            dtNav1.Columns[26].AllowDBNull = true;
            dtNav1.Columns.Add("SimultaneousVoiceFeature", typeof(bool));           //27
            dtNav1.Columns[27].AllowDBNull = true;
            dtNav1.Columns.Add("PowerOutput", typeof(int));                         //28
            dtNav1.Columns[28].AllowDBNull = true;
            dtNav1.Columns.Add("AutoVoiceIdentificationFeature", typeof(bool));     //29
            dtNav1.Columns[29].AllowDBNull = true;
            dtNav1.Columns.Add("MonitoringCategory", typeof(int));                  //30
            dtNav1.Columns[30].AllowDBNull = true;
            dtNav1.Columns.Add("RadioVoiceCall", typeof(string));                   //31
            dtNav1.Columns.Add("TransmitOnChannel", typeof(string));                //32
            dtNav1.Columns.Add("TransmitOnFrequency", typeof(decimal));             //33
            dtNav1.Columns[33].AllowDBNull = true;
            dtNav1.Columns.Add("RadioBeaconIdentifier", typeof(string));            //34
            dtNav1.Columns.Add("FanMarkerType", typeof(string));                    //35
            dtNav1.Columns.Add("FanMarkerAxis", typeof(int));                       //36
            dtNav1.Columns[36].AllowDBNull = true;
            dtNav1.Columns.Add("ProtectedFrequencyAltitude", typeof(string));       //37
            dtNav1.Columns.Add("LowAltitudeFacility", typeof(bool));                //38
            dtNav1.Columns[38].AllowDBNull = true;
            dtNav1.Columns.Add("ZMarkerAvailable", typeof(bool));                   //39
            dtNav1.Columns[39].AllowDBNull = true;
            dtNav1.Columns.Add("TWEBHours", typeof(string));                        //40
            dtNav1.Columns.Add("TWEBPhoneNo", typeof(string));                      //41
            dtNav1.Columns.Add("AssociatedFSSId", typeof(string));                  //42
            dtNav1.Columns.Add("AssociatedFSSName", typeof(string));                //43
            dtNav1.Columns.Add("HoursOfOperationFSS", typeof(string));              //44
            dtNav1.Columns.Add("NotamAccountabilityCode", typeof(string));          //45
            dtNav1.Columns.Add("QuadrantIdentification", typeof(string));           //46
            dtNav1.Columns.Add("NavAidStatus", typeof(string));                     //47
            dtNav1.Columns.Add("PitchFlag", typeof(bool));                          //48 
            dtNav1.Columns[48].AllowDBNull = true;
            dtNav1.Columns.Add("CatchFlag", typeof(bool));                          //49 
            dtNav1.Columns[49].AllowDBNull = true;
            dtNav1.Columns.Add("ATCAAFlag", typeof(bool));                          //50 
            dtNav1.Columns[50].AllowDBNull = true;
            dtNav1.Columns.Add("RestrictionFlag", typeof(bool));                    //51 
            dtNav1.Columns[51].AllowDBNull = true;
            dtNav1.Columns.Add("HiwasFlag", typeof(bool));                          //52 
            dtNav1.Columns[52].AllowDBNull = true;
            dtNav1.Columns.Add("TWEBRestriction", typeof(bool));                    //53 
            dtNav1.Columns[53].AllowDBNull = true;

            dtNav2.Columns.Add("NavAID", typeof(string));  //0 
            dtNav2.Columns.Add("NavAIDType", typeof(string));  //0 
            dtNav2.Columns.Add("Remark", typeof(string));  //0 
            dtNav2.Columns.Add("Filler", typeof(string));  //0 

            dtNav3.Columns.Add("NavAID", typeof(string));  //0 
            dtNav3.Columns.Add("NavAIDType", typeof(string));  //0 
            dtNav3.Columns.Add("Name", typeof(string));  //0 
            dtNav3.Columns.Add("SpaceForFixes", typeof(string));  //0 

            dtNav4.Columns.Add("NavAID", typeof(string));
            dtNav4.Columns.Add("NavAIDType", typeof(string));
            dtNav4.Columns.Add("Name", typeof(string));
            dtNav4.Columns.Add("Pattern", typeof(string));
            dtNav4.Columns.Add("SpaceForHoldingPattern", typeof(string));

            dtNav5.Columns.Add("NavAID", typeof(string));
            dtNav5.Columns.Add("NavAIDType", typeof(string));
            dtNav5.Columns.Add("Name", typeof(string));
            dtNav5.Columns.Add("SpaceForFanMarker", typeof(string));


            dtNav6.Columns.Add("NavAID", typeof(string));
            dtNav6.Columns.Add("NavAIDType", typeof(string));
            dtNav6.Columns.Add("AirOrGroundCode", typeof(string));
            dtNav6.Columns.Add("BearingOfCheckpoint", typeof(string));
            dtNav6.Columns.Add("Altitude", typeof(string));
            dtNav6.Columns.Add("AirportId", typeof(string));
            dtNav6.Columns.Add("State", typeof(string));
            dtNav6.Columns.Add("NarrativeDescriptionAir", typeof(string));
            dtNav6.Columns.Add("NarrativeDescriptionGround", typeof(string));

            try
            {
                DataRow dr;
                string strValue = "";
                string[] arr = { "", "", "" };
                string direction = "";
                foreach (var line in System.IO.File.ReadLines(ConfigurationReader.NAVFilePath + @"UnZip\NAV.txt"))
                {
                    switch (line.Substring(0, 4))
                    {
                        case "NAV1":
                            {

                                DataRow drNav1 = dtNav1.NewRow();
                                foreach (var li in listNav1)
                                {

                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        //Datetime
                                        case "EffectiveDate":
                                            {
                                                drNav1[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDateTime(strValue);
                                                break;
                                            }
                                        //Boolean
                                        case "CommonSystemUsage":
                                        case "PublicUse":
                                        case "SimultaneousVoiceFeature":
                                        case "AutoVoiceIdentificationFeature":
                                        case "LowAltitudeFacility":
                                        case "ZMarkerAvailable":
                                        case "PitchFlag":
                                        case "CatchFlag":
                                        case "ATCAAFlag":
                                        case "RestrictionFlag":
                                        case "HiwasFlag":
                                        case "TWEBRestriction":
                                            {
                                                drNav1[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)((strValue == "Y") ? true : false);
                                                break;
                                            }
                                        //Integer
                                        //case "HoursOfOperation":  //String also occurs in some record like "See remark"
                                        case "MagneticVariationEPOCHYear":
                                        case "PowerOutput":
                                        case "MonitoringCategory":
                                        case "FanMarkerAxis":
                                            //case "TWEBHours":  // hrs string occurs for some record
                                            {
                                                drNav1[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        //Decimal
                                        case "ElevationInTenthOfFoot":
                                        case "TransmitOnFrequency":
                                            {
                                                drNav1[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(strValue), 3);
                                                break;
                                            }
                                        //Lat Log Calculation
                                        case "Latitude":
                                        case "Longitude":
                                            {
                                                if (string.IsNullOrEmpty(strValue))
                                                {
                                                    drNav1[li.ColumnName] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    arr = strValue.Split('-');
                                                    direction = arr[2].Substring(arr[2].Length - 1);
                                                    arr[2] = arr[2].Remove(arr[2].Length - 1);
                                                    drNav1[li.ColumnName] = (new Misc().CalculateLatitudeAndLongitude(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), direction)).ToString();
                                                }
                                                break;
                                            }
                                        default:
                                            {
                                                drNav1[li.ColumnName] = strValue;
                                                break;
                                            }
                                    }
                                }
                                dtNav1.Rows.Add(drNav1);
                                break;
                            }
                        case "NAV2":
                            {
                                dr = dtNav2.NewRow();
                                foreach (var li in listNav2)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    dr[li.ColumnName] = strValue;
                                }
                                dtNav2.Rows.Add(dr);
                                break;
                            }
                        case "NAV3":
                            {
                                dr = dtNav3.NewRow();
                                foreach (var li in listNav3)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    dr[li.ColumnName] = strValue;
                                }
                                dtNav3.Rows.Add(dr);
                                break;
                            }
                        case "NAV4":
                            {
                                dr = dtNav4.NewRow();
                                foreach (var li in listNav4)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    dr[li.ColumnName] = strValue;
                                }
                                dtNav4.Rows.Add(dr);
                                break;
                            }
                        case "NAV5":
                            {
                                dr = dtNav5.NewRow();
                                foreach (var li in listNav5)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    dr[li.ColumnName] = strValue;
                                }
                                dtNav5.Rows.Add(dr);
                                break;
                            }
                        case "NAV6":
                            {
                                dr = dtNav6.NewRow();
                                foreach (var li in listNav6)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    dr[li.ColumnName] = strValue;
                                }
                                dtNav6.Rows.Add(dr);
                                break;
                            }
                    }
                }

                dtNav1.Rows.Cast<DataRow>().Where(r => r.ItemArray[6].ToString() == "").ToList().ForEach(r => r.Delete());
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav1";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav1;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav1Data", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav2";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav2;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav2Data", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav3";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav3;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav3Data", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav4";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav4;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav4Data", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav5";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav5;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav5Data", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbNav6";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtNav6;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNav6Data", param);

                var context = new GuardianAvionicsEntities();
                var aptFileDownloadDetails = context.APTFileDownloadDetails.Create();
                aptFileDownloadDetails.CreateDate = DateTime.Now;
                aptFileDownloadDetails.FileName = nextDate.ToString("yyyy-MM-dd") + "/NAV.zip";
                aptFileDownloadDetails.SubScriptionDate = nextDate;
                aptFileDownloadDetails.FileType = "NAV";
                context.APTFileDownloadDetails.Add(aptFileDownloadDetails);
                context.SaveChanges();

                

            }
            catch (Exception ex)
            {

            }
        }
    }
}