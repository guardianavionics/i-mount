﻿using BL_AL.Model;
using Ionic.Zip;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;


namespace BL_AL
{
    public class ParseFAAAirportData
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        public DateTime nextDate;
        public void CheckFileForDownload()
        {
            if (System.IO.File.Exists(ConfigurationReader.APTFilePath + "FAAAPT.zip"))
            {
                System.IO.File.Delete(ConfigurationReader.APTFilePath + "FAAAPT.zip");
            }
            Array.ForEach(Directory.GetFiles(ConfigurationReader.APTFilePath + @"\UnZip\"),
          delegate (string path) { File.Delete(path); });

            Misc objMisc = new Misc();
            var context = new GuardianAvionicsEntities();
            var aptFileDownloadDetails = context.APTFileDownloadDetails.OrderByDescending(o => o.Id).FirstOrDefault(f => f.FileType == "APT");
            if (aptFileDownloadDetails == null)
            {
                //Check the file exist for the current month
                DateTime currDate = new DateTime(2017, 5, 25);
                nextDate = currDate;
                string fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + currDate.ToString("yyyy-MM-dd") + "/APT.zip";
                //Now check the file exist or not
                if (objMisc.RemoteFileExists(fileURL))
                {
                    DownloadFile(fileURL);
                }
                //else
                //{
                //    currDate = currDate.AddMonths(-1);
                //    fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + currDate.ToString("yyyy-MM-dd") + "/APT.zip";
                //    //Now again check if the file exist or not
                //    if (objMisc.RemoteFileExists(fileURL))
                //    {
                //        DownloadFile(fileURL);
                //    }
                //}
            }
            else
            {
                //Now check that the file is available for the current data or not
                nextDate = aptFileDownloadDetails.SubScriptionDate.AddDays(28);
                if (nextDate <= DateTime.Now)
                {
                    string fileURL = "https://nfdc.faa.gov/webContent/28DaySub/" + nextDate.ToString("yyyy-MM-dd") + "/APT.zip";

                    //Now check the file exist or not
                    if (objMisc.RemoteFileExists(fileURL))
                    {
                        DownloadFile(fileURL);
                    }
                }

            }
        }

        public void DownloadFile(string fileURL)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.DownloadFile(new Uri(fileURL), ConfigurationReader.APTFilePath + "FAAAPT.zip");
            using (var zip = ZipFile.Read(ConfigurationReader.APTFilePath + "FAAAPT.zip"))
            {
                zip.ExtractProgress += zip_ExtractProgress;
                zip.ExtractAll(ConfigurationReader.APTFilePath + "UnZip");
            }
        }

        public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                ParseAirportData();
            }
        }

        public void ParseAirportData()
        {
            List<DataIndex> listAPT = new List<DataIndex>();
            //listAPT.Add(new DataIndex {ColumnName = "", StartIndex = 0, Length = 3 }); //RECORD TYPE INDICATOR
            listAPT.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 }); //LANDING FACILITY SITE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "Type", StartIndex = 14, Length = 13 }); //LANDING FACILITY TYPE
            listAPT.Add(new DataIndex { ColumnName = "apt_ident", StartIndex = 27, Length = 4 });  //LOCATION IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "EffectiveDate", StartIndex = 31, Length = 10 }); //INFORMATION EFFECTIVE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "Region", StartIndex = 41, Length = 3 });  //FAA REGION CODE
            listAPT.Add(new DataIndex { ColumnName = "DistrictOffice", StartIndex = 44, Length = 4 });  //FAA DISTRICT OR FIELD OFFICE CODE
            listAPT.Add(new DataIndex { ColumnName = "State", StartIndex = 48, Length = 2 }); //ASSOCIATED STATE POST OFFICE CODE
            listAPT.Add(new DataIndex { ColumnName = "StateName", StartIndex = 50, Length = 20 });  //ASSOCIATED STATE NAME
            listAPT.Add(new DataIndex { ColumnName = "County", StartIndex = 70, Length = 21 }); //ASSOCIATED COUNTY (OR PARISH) NAME
            listAPT.Add(new DataIndex { ColumnName = "CountyState", StartIndex = 91, Length = 2 });  //ASSOCIATED COUNTY'S STATE (POST OFFICE CODE)
            listAPT.Add(new DataIndex { ColumnName = "City", StartIndex = 93, Length = 40 }); //ASSOCIATED CITY NAME
            listAPT.Add(new DataIndex { ColumnName = "AirportName", StartIndex = 133, Length = 50 });  //OFFICIAL FACILITY NAME
            listAPT.Add(new DataIndex { ColumnName = "Ownership", StartIndex = 183, Length = 2 });  //AIRPORT OWNERSHIP TYPE
            listAPT.Add(new DataIndex { ColumnName = "Use", StartIndex = 185, Length = 2 });  //FACILITY USE
            listAPT.Add(new DataIndex { ColumnName = "Owner", StartIndex = 187, Length = 35 });  //FACILITY OWNER'S NAME
            listAPT.Add(new DataIndex { ColumnName = "OwnerAddress", StartIndex = 222, Length = 72 });  //OWNER'S ADDRESS
            listAPT.Add(new DataIndex { ColumnName = "OwnerCSZ", StartIndex = 294, Length = 45 });  //OWNER'S CITY, STATE AND ZIP CODE
            listAPT.Add(new DataIndex { ColumnName = "OwnerPhone", StartIndex = 339, Length = 16 });  //OWNER'S PHONE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "Manager", StartIndex = 355, Length = 35 });  //FACILITY MANAGER'S NAME
            listAPT.Add(new DataIndex { ColumnName = "ManagerAddress", StartIndex = 390, Length = 72 });  //MANAGER'S ADDRESS
            listAPT.Add(new DataIndex { ColumnName = "ManagerCSZ", StartIndex = 462, Length = 45 });  //MANAGER'S CITY, STATE AND ZIP CODE
            listAPT.Add(new DataIndex { ColumnName = "ManagerPhone", StartIndex = 507, Length = 16 });  //MANAGER'S PHONE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "X", StartIndex = 523, Length = 15 }); //ARPLatitude  //AIRPORT REFERENCE POINT LATITUDE (FORMATTED)
            listAPT.Add(new DataIndex { ColumnName = "ARPLatitudeS", StartIndex = 538, Length = 12 });  //AIRPORT REFERENCE POINT LATITUDE (SECONDS)
            listAPT.Add(new DataIndex { ColumnName = "Y", StartIndex = 550, Length = 15 }); //ARPLongitude  //AIRPORT REFERENCE POINT LONGITUDE (FORMATTED)
            listAPT.Add(new DataIndex { ColumnName = "ARPLongitudeS", StartIndex = 565, Length = 12 });  //AIRPORT REFERENCE POINT LONGITUDE (SECONDS)
            listAPT.Add(new DataIndex { ColumnName = "ARPMethod", StartIndex = 577, Length = 1 });  //AIRPORT REFERENCE POINT DETERMINATION METHOD
            listAPT.Add(new DataIndex { ColumnName = "ARPElevation", StartIndex = 578, Length = 7 });  //AIRPORT ELEVATION  (NEAREST TENTH OF A FOOT MSL)
            listAPT.Add(new DataIndex { ColumnName = "ARPElevationMethod", StartIndex = 585, Length = 1 });  //AIRPORT ELEVATION DETERMINATION METHOD
            listAPT.Add(new DataIndex { ColumnName = "MagneticVariation", StartIndex = 586, Length = 3 });  //MAGNETIC VARIATION AND DIRECTION
            listAPT.Add(new DataIndex { ColumnName = "MagneticVariationYear", StartIndex = 589, Length = 4 });  //MAGNETIC VARIATION EPOCH YEAR
            listAPT.Add(new DataIndex { ColumnName = "TrafficPatternAltitude", StartIndex = 593, Length = 4 });  //TRAFFIC PATTERN ALTITUDE  (WHOLE FEET AGL)
            listAPT.Add(new DataIndex { ColumnName = "ChartName", StartIndex = 597, Length = 30 });  //AERONAUTICAL SECTIONAL CHART ON WHICH FACILITY
            listAPT.Add(new DataIndex { ColumnName = "DistanceFromCBD", StartIndex = 627, Length = 2 });  //DISTANCE FROM CENTRAL BUSINESS DISTRICT OF
            listAPT.Add(new DataIndex { ColumnName = "DirectionFromCBD", StartIndex = 629, Length = 3 });  //DIRECTION OF AIRPORT FROM CENTRAL BUSINESS
            listAPT.Add(new DataIndex { ColumnName = "LandAreaCoveredByAirport", StartIndex = 632, Length = 5 });  //LAND AREA COVERED BY AIRPORT (ACRES)
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCID", StartIndex = 637, Length = 4 });  //BOUNDARY ARTCC IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCComputerID", StartIndex = 641, Length = 3 });  //BOUNDARY ARTCC (FAA) COMPUTER IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCName", StartIndex = 644, Length = 30 });  //BOUNDARY ARTCC NAME
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCID", StartIndex = 674, Length = 4 });  //RESPONSIBLE ARTCC IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCComputerID", StartIndex = 678, Length = 3 });  //RESPONSIBLE ARTCC (FAA) COMPUTER IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCName", StartIndex = 681, Length = 30 });  //RESPONSIBLE ARTCC NAME
            listAPT.Add(new DataIndex { ColumnName = "TieInFSS", StartIndex = 711, Length = 1 });  //TIE-IN FSS PHYSICALLY LOCATED ON FACILITY
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSID", StartIndex = 712, Length = 4 });  //TIE-IN FLIGHT SERVICE STATION (FSS) IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSName", StartIndex = 716, Length = 30 });  //TIE-IN FSS NAME
            listAPT.Add(new DataIndex { ColumnName = "AirportToFSSPhoneNumber", StartIndex = 746, Length = 16 });  //LOCAL PHONE NUMBER FROM AIRPORT TO FSS
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSTollFreeNumber", StartIndex = 762, Length = 16 });  //TOLL FREE PHONE NUMBER FROM AIRPORT TO FSS FOR PILOT BRIEFING SERVICES
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSID", StartIndex = 778, Length = 4 }); //ALTERNATE FSS IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSName", StartIndex = 782, Length = 30 });  //ALTERNATE FSS NAME
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSTollFreeNumber", StartIndex = 812, Length = 16 });  //TOLL FREE PHONE NUMBER FROM AIRPORT TO ALTERNATE FSS FOR PILOT BRIEFING SERVICES
            listAPT.Add(new DataIndex { ColumnName = "NOTAMFacilityID", StartIndex = 828, Length = 4 });  //IDENTIFIER OF THE FACILITY RESPONSIBLE FOR ISSUING NOTICES TO AIRMEN (NOTAMS)AND WEATHER INFORMATION FOR THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "NOTAMService", StartIndex = 832, Length = 1 });  //AVAILABILITY OF NOTAM 'D' SERVICE AT AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "ActivationDate", StartIndex = 833, Length = 7 });  //AIRPORT ACTIVATION DATE (MM/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportStatusCode", StartIndex = 840, Length = 2 });  // AIRPORT STATUS CODE
            listAPT.Add(new DataIndex { ColumnName = "CertificationTypeDate", StartIndex = 842, Length = 15 });  //AIRPORT ARFF CERTIFICATION TYPE AND DATE
            listAPT.Add(new DataIndex { ColumnName = "FederalAgreements", StartIndex = 857, Length = 7 });  //NPIAS/FEDERAL AGREEMENTS CODE
            listAPT.Add(new DataIndex { ColumnName = "AirspaceDetermination", StartIndex = 864, Length = 13 });  //AIRPORT AIRSPACE ANALYSIS DETERMINATION
            listAPT.Add(new DataIndex { ColumnName = "CustomsAirportOfEntry", StartIndex = 877, Length = 1 });  //FACILITY HAS BEEN DESIGNATED BY THE U.S. TREASURY AS AN INTERNATIONAL AIRPORT OF ENTRY FOR CUSTOMS
            listAPT.Add(new DataIndex { ColumnName = "CustomsLandingRights", StartIndex = 878, Length = 1 });  //FACILITY HAS BEEN DESIGNATED BY THE U.S. TREASURY AS A CUSTOMS LANDING RIGHTS AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MilitaryJointUse", StartIndex = 879, Length = 1 });  //FACILITY HAS MILITARY/CIVIL JOINT USE AGREEMENT THAT ALLOWS CIVIL OPERATIONS AT A MILITARY AIRPORT OR MILITARY OPERATIONS AT A CIVIL AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MilitaryLandingRights", StartIndex = 880, Length = 1 });  //AIRPORT HAS ENTERED INTO AN AGREEMENT THAT GRANTS LANDING RIGHTS TO THE MILITARY
            listAPT.Add(new DataIndex { ColumnName = "InspectionMethod", StartIndex = 881, Length = 2 });  //AIRPORT INSPECTION METHOD
            listAPT.Add(new DataIndex { ColumnName = "InspectionGroup", StartIndex = 883, Length = 1 });  //AGENCY/GROUP PERFORMING PHYSICAL INSPECTION
            listAPT.Add(new DataIndex { ColumnName = "LastInspectionDate", StartIndex = 884, Length = 8 });  //LAST PHYSICAL INSPECTION DATE (MMDDYYYY)   *************************************************
            listAPT.Add(new DataIndex { ColumnName = "LastOwnerInformationDate", StartIndex = 892, Length = 8 });  //LAST DATE INFORMATION REQUEST WAS COMPLETED BY FACILITY OWNER OR MANAGER(MMDDYYYY)  *******************************
            listAPT.Add(new DataIndex { ColumnName = "FuelTypes", StartIndex = 900, Length = 40 });  //FUEL TYPES AVAILABLE FOR PUBLIC USE AT THE AIRPORT.THERE CAN BE UP TO 8 OCCURENCES OF A FIXED 5 CHARACTER FIELD.
            listAPT.Add(new DataIndex { ColumnName = "AirframeRepair", StartIndex = 940, Length = 5 });  //AIRFRAME REPAIR SERVICE AVAILABILITY/TYPE
            listAPT.Add(new DataIndex { ColumnName = "PowerPlantRepair", StartIndex = 945, Length = 5 });  //POWER PLANT (ENGINE) REPAIR AVAILABILITY/TYPE
            listAPT.Add(new DataIndex { ColumnName = "BottledOxygenType", StartIndex = 950, Length = 8 });  // TYPE OF BOTTLED OXYGEN AVAILABLE (VALUE REPRESENTS HIGH AND/ OR LOW PRESSURE REPLACEMENT BOTTLE)
            listAPT.Add(new DataIndex { ColumnName = "BulkOxygenType", StartIndex = 958, Length = 8 });  //TYPE OF BULK OXYGEN AVAILABLE (VALUE REPRESENTS HIGH AND/ OR LOW PRESSURE CYLINDERS)
            listAPT.Add(new DataIndex { ColumnName = "LightingSchedule", StartIndex = 966, Length = 7 });  //AIRPORT LIGHTING SCHEDULE
            listAPT.Add(new DataIndex { ColumnName = "BeaconSchedule", StartIndex = 973, Length = 7 });  //BEACON LIGHTING SCHEDULE
            listAPT.Add(new DataIndex { ColumnName = "ATCT", StartIndex = 980, Length = 1 });  //AIR TRAFFIC CONTROL TOWER LOCATED ON AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "UNICOMFrequencies", StartIndex = 981, Length = 7 });  //UNICOM FREQUENCY AVAILABLE AT THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "CTAFFrequency", StartIndex = 988, Length = 7 });  //COMMON TRAFFIC ADVISORY FREQUENCY (CTAF)
            listAPT.Add(new DataIndex { ColumnName = "SegmentedCircle", StartIndex = 995, Length = 4 });  //SEGMENTED CIRCLE AIRPORT MARKER SYSTEM ON THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "BeaconColor", StartIndex = 999, Length = 3 });  //LENS COLOR OF OPERABLE BEACON LOCATED ON THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "NonCommercialLandingFee", StartIndex = 1002, Length = 1 });  //LANDING FEE CHARGED TO NON-COMMERCIAL USERS OF AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MedicalUse", StartIndex = 1003, Length = 1 });  //A "Y" IN THIS FIELD INDICATES THAT THE LANDING FACILITY IS USED FOR MEDICAL PURPOSES
            listAPT.Add(new DataIndex { ColumnName = "SingleEngineGA", StartIndex = 1004, Length = 3 });  //SINGLE ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "MultiEngineGA", StartIndex = 1007, Length = 3 });  //MULTI ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "JetEngineGA", StartIndex = 1010, Length = 3 });  //JET ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "HelicoptersGA", StartIndex = 1013, Length = 3 });  //GENERAL AVIATION HELICOPTER
            listAPT.Add(new DataIndex { ColumnName = "GlidersOperational", StartIndex = 1016, Length = 3 });  //OPERATIONAL GLIDERS
            listAPT.Add(new DataIndex { ColumnName = "MilitaryOperational", StartIndex = 1019, Length = 3 });  //OPERATIONAL MILITARY AIRCRAFT (INCLUDING HELICOPTERS)
            listAPT.Add(new DataIndex { ColumnName = "Ultralights", StartIndex = 1022, Length = 3 });  //ULTRALIGHT AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "OperationsCommercial", StartIndex = 1025, Length = 6 });  //COMMERCIAL SERVICES
            listAPT.Add(new DataIndex { ColumnName = "OperationsCommuter", StartIndex = 1031, Length = 6 });  //COMMUTER SERVICES
            listAPT.Add(new DataIndex { ColumnName = "OperationsAirTaxi", StartIndex = 1037, Length = 6 });  //AIR TAXI
            listAPT.Add(new DataIndex { ColumnName = "OperationsGALocal", StartIndex = 1043, Length = 6 });  //GENERAL AVIATION LOCAL OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsGAItin", StartIndex = 1049, Length = 6 });  //GENERAL AVIATION ITINERANT OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsMilitary", StartIndex = 1055, Length = 6 });  //MILITARY AIRCRAFT OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsDate", StartIndex = 1061, Length = 10 });  //12-MONTH ENDING DATE ON WHICH ANNUAL OPERATIONS DATA IN ABOVE SIX FIELDS IS BASED (MM / DD / YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportPositionSource", StartIndex = 1071, Length = 16 });  //AIRPORT POSITION SOURCE
            listAPT.Add(new DataIndex { ColumnName = "AirportPositionSourceDate", StartIndex = 1087, Length = 10 });  //AIRPORT POSITION SOURCE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportElevationSource", StartIndex = 1097, Length = 16 });  //AIRPORT ELEVATION SOURCE
            listAPT.Add(new DataIndex { ColumnName = "AirportElevationSourceDate", StartIndex = 1113, Length = 10 });  //AIRPORT ELEVATION SOURCE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "ContractFuelAvailable", StartIndex = 1123, Length = 1 });  //CONTRACT FUEL AVAILABLE
            listAPT.Add(new DataIndex { ColumnName = "TransientStorage", StartIndex = 1124, Length = 12 });  //TRANSIENT STORAGE FACILITIES
            listAPT.Add(new DataIndex { ColumnName = "OtherServices", StartIndex = 1136, Length = 71 });  //OTHER AIRPORT SERVICES AVAILABLE
            listAPT.Add(new DataIndex { ColumnName = "WindIndicator", StartIndex = 1207, Length = 3 });  //WIND INDICATOR
            listAPT.Add(new DataIndex { ColumnName = "icao_ident", StartIndex = 1210, Length = 7 });  //ICAO IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "AirportRecordFiller", StartIndex = 1217, Length = 312 });  //AIRPORT RECORD FILLER (BLANK)



            List<DataIndex> listATT = new List<DataIndex>();
            listATT.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listATT.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //LANDING FACILITY STATE POST OFFICE CODE
            listATT.Add(new DataIndex { ColumnName = "SequenceNumber", StartIndex = 16, Length = 2 });  //ATTENDANCE SCHEDULE SEQUENCE NUMBER
            listATT.Add(new DataIndex { ColumnName = "AttendanceSchedule", StartIndex = 18, Length = 108 });  //AIRPORT ATTENDANCE SCHEDULE(WHEN MINIMUM SERVICES ARE AVAILABLE


            List<DataIndex> listRWY = new List<DataIndex>();
            //listRWY.Add(new DataIndex { ColumnName="", StartIndex = 0, Length = 3 });  //RECORD TYPE INDICATOR
            listRWY.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listRWY.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //RUNWAY STATE POST OFFICE CODE
            listRWY.Add(new DataIndex { ColumnName = "RunwayID", StartIndex = 16, Length = 7 });  //RUNWAY IDENTIFICATION
            listRWY.Add(new DataIndex { ColumnName = "RunwayLength", StartIndex = 23, Length = 5 });  //PHYSICAL RUNWAY LENGTH (NEAREST FOOT
            listRWY.Add(new DataIndex { ColumnName = "RunwayWidth", StartIndex = 28, Length = 4 });  //PHYSICAL RUNWAY WIDTH (NEAREST FOOT
            listRWY.Add(new DataIndex { ColumnName = "RunwaySurfaceTypeCondition", StartIndex = 32, Length = 12 });  //RUNWAY SURFACE TYPE AND CONDITION
            listRWY.Add(new DataIndex { ColumnName = "RunwaySurfaceTreatment", StartIndex = 44, Length = 5 });  //RUNWAY SURFACE TREATMENT
            listRWY.Add(new DataIndex { ColumnName = "PavementClass", StartIndex = 49, Length = 11 });  //PAVEMENT CLASSIFICATION NUMBER (PCN
            listRWY.Add(new DataIndex { ColumnName = "EdgeLightsIntensity", StartIndex = 60, Length = 5 });  //RUNWAY LIGHTS EDGE INTENSITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndID", StartIndex = 65, Length = 3 });  //BASE END IDENTIFIER
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTrueAlignment", StartIndex = 68, Length = 3 });  //RUNWAY END TRUE ALIGNMENT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndILSType", StartIndex = 71, Length = 10 });  //INSTRUMENT LANDING SYSTEM (ILS) TYPE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRightTrafficPattern", StartIndex = 81, Length = 1 });  //RIGHT HAND TRAFFIC PATTERN FOR LANDING AIRCRAFT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndMarkingsType", StartIndex = 82, Length = 5 });  //RUNWAY MARKINGS  (TYPE)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndMarkingsCondition", StartIndex = 87, Length = 1 });  //RUNWAY MARKINGS  (CONDITION)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLatitude", StartIndex = 88, Length = 15 });  //LATITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLatitudeS", StartIndex = 103, Length = 12 });  //LATITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLongitude", StartIndex = 115, Length = 15 });  //LONGITUDE OF PHYSICAL RUNWAY END (FORMATTED
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLongitudeS", StartIndex = 130, Length = 12 });  //LONGITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalElevation", StartIndex = 142, Length = 7 });  //ELEVATION (FEET MSL) AT PHYSICAL RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "BaseEndCrossingHeight", StartIndex = 149, Length = 3 });  //THRESHOLD CROSSING HEIGHT (FEET AGL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGlidePathAngle", StartIndex = 152, Length = 4 });  //VISUAL GLIDE PATH ANGLE (HUNDREDTHS OF DEGREES)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLatitude", StartIndex = 156, Length = 15 });  //LATITUDE  AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLatitudeS", StartIndex = 171, Length = 12 });  //LATITUDE  AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLongitude", StartIndex = 183, Length = 15 });  //LONGITUDE AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLongitudeS", StartIndex = 198, Length = 12 });  //LONGITUDE AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedElevation", StartIndex = 210, Length = 7 });  //ELEVATION AT DISPLACED THRESHOLD (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLength", StartIndex = 217, Length = 4 });  //DISPLACED THRESHOLD - LENGTH IN FEET FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTDZElevation", StartIndex = 221, Length = 7 });  //ELEVATION AT TOUCHDOWN ZONE (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndVASI", StartIndex = 228, Length = 5 });  //VISUAL GLIDE SLOPE INDICATORS
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRVR", StartIndex = 233, Length = 3 });  // RUNWAY VISUAL RANGE EQUIPMENT (RVR)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRVV", StartIndex = 236, Length = 1 });  //RUNWAY VISIBILITY VALUE EQUIPMENT (RVV)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndALS", StartIndex = 237, Length = 8 });  //APPROACH LIGHT SYSTEM
            listRWY.Add(new DataIndex { ColumnName = "BaseEndREIL", StartIndex = 245, Length = 1 });  //RUNWAY END IDENTIFIER LIGHTS (REIL) AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndCenterlineLights", StartIndex = 246, Length = 1 });  //RUNWAY CENTERLINE LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownLights", StartIndex = 247, Length = 1 });  //RUNWAY END TOUCHDOWN LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectDescription", StartIndex = 248, Length = 11 });  //CONTROLLING OBJECT DESCRIPTION
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectMarkLight", StartIndex = 259, Length = 4 });  //CONTROLLING OBJECT MARKED/LIGHTED
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPart77Category", StartIndex = 263, Length = 5 });  //FAA CFR PART 77 (OBJECTS AFFECTING NAVIGABLE AIRSPACE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectClearSlope", StartIndex = 268, Length = 2 });  //CONTROLLING OBJECT CLEARANCE SLOPE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectHeight", StartIndex = 270, Length = 5 });  //CONTROLLING OBJECT HEIGHT ABOVE RUNWAY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectDistance", StartIndex = 275, Length = 5 });  //CONTROLLING OBJECT DISTANCE FROM RUNWAY 
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectOffset", StartIndex = 280, Length = 7 });  //CONTROLLING OBJECT CENTERLINE OFFSET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndID", StartIndex = 287, Length = 3 });  //RECIPROCAL END IDENTIFIER
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTrueAlignment", StartIndex = 290, Length = 3 });  //RUNWAY END TRUE ALIGNMENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndILSType", StartIndex = 293, Length = 10 });  //INSTRUMENT LANDING SYSTEM (ILS) TYPE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRightTrafficPattern", StartIndex = 303, Length = 1 });  //RIGHT HAND TRAFFIC PATTERN FOR LANDING AIRCRAFT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndMarkingsType", StartIndex = 304, Length = 5 });  //RUNWAY MARKINGS  (TYPE)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndMarkingsCondition", StartIndex = 309, Length = 1 });  //RUNWAY MARKINGS  (CONDITION)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLatitude", StartIndex = 310, Length = 15 });  //LATITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLatitudeS", StartIndex = 325, Length = 12 });  //LATITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLongitude", StartIndex = 337, Length = 15 });  //LONGITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLongitudeS", StartIndex = 352, Length = 12 });  //LONGITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalElevation", StartIndex = 364, Length = 7 });  //ELEVATION (FEET MSL) AT PHYSICAL RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndCrossingHeight", StartIndex = 371, Length = 3 });  //THRESHOLD CROSSING HEIGHT (FEET AGL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGlidePathAngle", StartIndex = 374, Length = 4 });  //VISUAL GLIDE PATH ANGLE (HUNDREDTHS OF DEGREES)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLatitude", StartIndex = 378, Length = 15 });  //LATITUDE  AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLatitudeS", StartIndex = 393, Length = 12 });  //LATITUDE  AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLongitude", StartIndex = 405, Length = 15 });  //LONGITUDE AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLongitudeS", StartIndex = 420, Length = 12 });  //LONGITUDE AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedElevation", StartIndex = 432, Length = 7 });  //ELEVATION AT DISPLACED THRESHOLD (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLength", StartIndex = 439, Length = 4 });  //DISPLACED THRESHOLD - LENGTH IN FEET FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTDZElevation", StartIndex = 443, Length = 7 });  //ELEVATION AT TOUCHDOWN ZONE (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndVASI", StartIndex = 450, Length = 5 });  //APPROACH SLOPE INDICATOR EQUIPMENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRVR", StartIndex = 455, Length = 3 });  //RUNWAY VISUAL RANGE EQUIPMENT (RVR)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRVV", StartIndex = 458, Length = 1 });  //RUNWAY VISIBILITY VALUE EQUIPMENT (RVV)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndALS", StartIndex = 459, Length = 8 });  //APPROACH LIGHT SYSTEM
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndREIL", StartIndex = 467, Length = 1 });  //RUNWAY END IDENTIFIER LIGHTS (REIL) AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndCenterlineLights", StartIndex = 468, Length = 1 });  //RUNWAY CENTERLINE LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownLights", StartIndex = 469, Length = 1 });  //RUNWAY END TOUCHDOWN LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectDescription", StartIndex = 470, Length = 11 });  //CONTROLLING OBJECT DESCRIPTION
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectMarkLight", StartIndex = 481, Length = 4 });  //CONTROLLING OBJECT MARKED/LIGHTED
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPart77Category", StartIndex = 485, Length = 5 });  //FAA CFR PART 77 (OBJECTS AFFECTING NAVIGABLE AIRSPACE)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectClearSlope", StartIndex = 490, Length = 2 });  //CONTROLLING OBJECT CLEARANCE SLOPE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectHeight", StartIndex = 492, Length = 5 }); //CONTROLLING OBJECT HEIGHT ABOVE RUNWAY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectDistance", StartIndex = 497, Length = 5 });  //CONTROLLING OBJECT DISTANCE FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectOffset", StartIndex = 502, Length = 7 });  //CONTROLLING OBJECT CENTERLINE OFFSET
            listRWY.Add(new DataIndex { ColumnName = "RunwayLengthSource", StartIndex = 509, Length = 16 });  //RUNWAY LENGTH SOURCE
            listRWY.Add(new DataIndex { ColumnName = "RunwayLengthSourceDate", StartIndex = 525, Length = 10 });  //RUNWAY LENGTH SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacitySW", StartIndex = 535, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Single wheel
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDW", StartIndex = 541, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Dual wheel
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDT", StartIndex = 547, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Two dual wheels
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDDT", StartIndex = 553, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Two dual wheels
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGradient", StartIndex = 559, Length = 5 });  //RUNWAY END GRADIENT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGradientDirection", StartIndex = 564, Length = 4 });  //RUNWAY END GRADIENT DIRECTION (UP OR DOWN)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPositionSource", StartIndex = 568, Length = 16 });  //RUNWAY END POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPositionSourceDate", StartIndex = 584, Length = 10 });  //RUNWAY END POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndElevationSource", StartIndex = 594, Length = 16 });  //RUNWAY END ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndElevationSourceDate", StartIndex = 610, Length = 10 });  //RUNWAY END ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdPositionSource", StartIndex = 620, Length = 16 });  //DISPLACED THESHOLD POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdPositionSourceDate", StartIndex = 636, Length = 10 });  // DISPLACED THESHOLD POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdElevationSource", StartIndex = 646, Length = 16 });  //DISPLACED THESHOLD ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdElevationSourceDate", StartIndex = 662, Length = 10 });  //DISPLACED THESHOLD ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownZoneElevationSource", StartIndex = 672, Length = 16 });  //TOUCHDOWN ZONE ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownZoneElevationSourceDate", StartIndex = 688, Length = 10 });  // TOUCHDOWN ZONE ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTakeOffRunAvailableTORA", StartIndex = 698, Length = 5 });  //TAKEOFF RUN AVAILABLE (TORA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTakeOffDistanceAvailableTODA", StartIndex = 703, Length = 5 });  //TAKEOFF DISTANCE AVAILABLE (TODA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndAcltStopDistanceAvailableASDA", StartIndex = 708, Length = 5 });  //ACLT STOP DISTANCE AVAILABLE (ASDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLDA", StartIndex = 713, Length = 5 });  //LANDING DISTANCE AVAILABLE (LDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSO", StartIndex = 718, Length = 5 });  //AVAILABLE LANDING DISTANCE FOR LAND AND HOLD SHORT OPERATIONS(LAHSO)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOIntRwyID", StartIndex = 723, Length = 7 });  //ID OF INTERSECTING RUNWAY DEFINING HOLD SHORT POINT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOIntEntityDesc", StartIndex = 730, Length = 40 });  //DESCRIPTION OF ENTITY DEFINING HOLD SHORT POINT 
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLatitude", StartIndex = 770, Length = 15 });  //LATITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLatitudeS", StartIndex = 785, Length = 12 });  //LATITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLongitude", StartIndex = 797, Length = 15 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLongitudeS", StartIndex = 812, Length = 12 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOCoordSrc", StartIndex = 824, Length = 16 });  //LAHSO HOLD SHORT POINT LAT/LONG SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOCoordSrcDate", StartIndex = 840, Length = 10 });  //HOLD SHORT POINT LAT/LONG SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGradient", StartIndex = 850, Length = 5 });  //RUNWAY END GRADIENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGradientDirection", StartIndex = 855, Length = 4 });  //RUNWAY END GRADIENT DIRECTION (UP OR DOWN)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPositionSource", StartIndex = 859, Length = 16 });  //RUNWAY END POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPositionSourceDate", StartIndex = 875, Length = 10 });  //RUNWAY END POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndElevationSource", StartIndex = 885, Length = 16 });  //RUNWAY END ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndElevationSourceDate", StartIndex = 901, Length = 10 });  //RUNWAY END ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdPositionSource", StartIndex = 911, Length = 16 });  //DISPLACED THESHOLD POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdPositionSourceDate", StartIndex = 927, Length = 10 });  //DISPLACED THESHOLD POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdElevationSource", StartIndex = 937, Length = 16 });  //DISPLACED THESHOLD ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdElevationSourceDate", StartIndex = 953, Length = 10 });  //DISPLACED THESHOLD ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownZoneElevationSource", StartIndex = 963, Length = 16 });  //TOUCHDOWN ZONE ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownZoneElevationSourceDate", StartIndex = 979, Length = 10 });  //TOUCHDOWN ZONE ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTakeOffRunAvailableTORA", StartIndex = 989, Length = 5 });  //TAKEOFF RUN AVAILABLE (TORA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTakeOffDistanceAvailableTODA", StartIndex = 994, Length = 5 });  //TAKEOFF DISTANCE AVAILABLE (TODA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndAcltStopDistanceAvailableASDA", StartIndex = 999, Length = 5 });  //ACLT STOP DISTANCE AVAILABLE (ASDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLDA", StartIndex = 1004, Length = 5 });  //LANDING DISTANCE AVAILABLE (LDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSO", StartIndex = 1009, Length = 5 });  //AVAILABLE LANDING DISTANCE FOR LAND AND HOLD SHORT OPERATIONS(LAHSO)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOIntRwyID", StartIndex = 1014, Length = 7 });  //ID OF INTERSECTING RUNWAY DEFINING HOLD SHORT POINT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOIntEntityDesc", StartIndex = 1021, Length = 40 }); //DESCRIPTION OF ENTITY DEFINING HOLD SHORT POINT 
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude", StartIndex = 1061, Length = 15 });  //LATITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitudeS", StartIndex = 1076, Length = 12 });  //LATITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude", StartIndex = 1088, Length = 15 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitudeS", StartIndex = 1103, Length = 12 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrc", StartIndex = 1115, Length = 16 });   //LAHSO HOLD SHORT POINT LAT/LONG SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate", StartIndex = 1131, Length = 10 });  //HOLD SHORT POINT LAT/LONG SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "RunwayRecordFiller", StartIndex = 1141, Length = 388 });  //RUNWAY RECORD FILLER (BLANK)


            List<DataIndex> listRMK = new List<DataIndex>();
            listRMK.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listRMK.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //LANDING FACILITY STATE POST OFFICE CODE
            listRMK.Add(new DataIndex { ColumnName = "RemarkElementName", StartIndex = 16, Length = 13 });  //REMARK ELEMENT NAME
            listRMK.Add(new DataIndex { ColumnName = "Remarks", StartIndex = 29, Length = 1500 });  //REMARK TEXT



            DataTable dtFacilities = new DataTable("Facilities");
            DataTable dtAttendence = new DataTable("Attendence");
            DataTable dtRunway = new DataTable("Runway");
            DataTable dtRemark = new DataTable("Remark");


            dtFacilities.Columns.Add("SiteNumber", typeof(string));  //0
            dtFacilities.Columns.Add("Type", typeof(string));  //1
            dtFacilities.Columns.Add("apt_ident", typeof(string)); //2
            dtFacilities.Columns.Add("EffectiveDate", typeof(DateTime)); //3
            dtFacilities.Columns[3].AllowDBNull = true;
            dtFacilities.Columns.Add("Region", typeof(string));  //4
            dtFacilities.Columns.Add("DistrictOffice", typeof(string));  //5
            dtFacilities.Columns.Add("State", typeof(string));  //6
            dtFacilities.Columns.Add("StateName", typeof(string)); //7
            dtFacilities.Columns.Add("County", typeof(string));  //8
            dtFacilities.Columns.Add("CountyState", typeof(string));  //9
            dtFacilities.Columns.Add("City", typeof(string));  //10
            dtFacilities.Columns.Add("AirportName", typeof(string));  //11
            dtFacilities.Columns.Add("Ownership", typeof(string));  //12
            dtFacilities.Columns.Add("Use", typeof(string)); //13
            dtFacilities.Columns.Add("Owner", typeof(string));  //14
            dtFacilities.Columns.Add("OwnerAddress", typeof(string));  //15
            dtFacilities.Columns.Add("OwnerCSZ", typeof(string));  //16
            dtFacilities.Columns.Add("OwnerPhone", typeof(string));  //17
            dtFacilities.Columns.Add("Manager", typeof(string));  //18
            dtFacilities.Columns.Add("ManagerAddress", typeof(string));  //19
            dtFacilities.Columns.Add("ManagerCSZ", typeof(string));  //20
            dtFacilities.Columns.Add("ManagerPhone", typeof(string));  //21
            dtFacilities.Columns.Add("X", typeof(string));  //22   //ARPLatitude
            dtFacilities.Columns.Add("ARPLatitudeS", typeof(string));  //23
            dtFacilities.Columns.Add("Y", typeof(string));  //24   //ARPLongitude
            dtFacilities.Columns.Add("ARPLongitudeS", typeof(string));  //25
            dtFacilities.Columns.Add("ARPMethod", typeof(string));  //26
            dtFacilities.Columns.Add("ARPElevation", typeof(decimal));  //27
            dtFacilities.Columns[27].AllowDBNull = true;
            dtFacilities.Columns.Add("ARPElevationMethod", typeof(string));  //28
            dtFacilities.Columns.Add("MagneticVariation", typeof(string));  //29
            dtFacilities.Columns.Add("MagneticVariationYear", typeof(int));  //30
            dtFacilities.Columns[30].AllowDBNull = true;
            dtFacilities.Columns.Add("TrafficPatternAltitude", typeof(int));  //31
            dtFacilities.Columns[31].AllowDBNull = true;
            dtFacilities.Columns.Add("ChartName", typeof(string));  //32
            dtFacilities.Columns.Add("DistanceFromCBD", typeof(int));  //33
            dtFacilities.Columns[33].AllowDBNull = true;
            dtFacilities.Columns.Add("DirectionFromCBD", typeof(string));  //34
            dtFacilities.Columns.Add("LandAreaCoveredByAirport", typeof(int)); //35
            dtFacilities.Columns[35].AllowDBNull = true;
            dtFacilities.Columns.Add("BoundaryARTCCID", typeof(string));  //36
            dtFacilities.Columns.Add("BoundaryARTCCComputerID", typeof(string));  //37
            dtFacilities.Columns.Add("BoundaryARTCCName", typeof(string));  //38
            dtFacilities.Columns.Add("ResponsibleARTCCID", typeof(string));  //39
            dtFacilities.Columns.Add("ResponsibleARTCCComputerID", typeof(string));  //40
            dtFacilities.Columns.Add("ResponsibleARTCCName", typeof(string));  //41
            dtFacilities.Columns.Add("TieInFSS", typeof(bool));  //42
            dtFacilities.Columns[42].AllowDBNull = true;
            dtFacilities.Columns.Add("TieInFSSID", typeof(string));  //43
            dtFacilities.Columns.Add("TieInFSSName", typeof(string));  //44
            dtFacilities.Columns.Add("AirportToFSSPhoneNumber", typeof(string));  //45
            dtFacilities.Columns.Add("TieInFSSTollFreeNumber", typeof(string));  //46
            dtFacilities.Columns.Add("AlternateFSSID", typeof(string));  //47
            dtFacilities.Columns.Add("AlternateFSSName", typeof(string));  //48
            dtFacilities.Columns.Add("AlternateFSSTollFreeNumber", typeof(string));  //49
            dtFacilities.Columns.Add("NOTAMFacilityID", typeof(string));  //50
            dtFacilities.Columns.Add("NOTAMService", typeof(bool));  //51
            dtFacilities.Columns[51].AllowDBNull = true;
            dtFacilities.Columns.Add("ActivationDate", typeof(string));  //52
            dtFacilities.Columns.Add("AirportStatusCode", typeof(string));  //53
            dtFacilities.Columns.Add("CertificationTypeDate", typeof(string));  //54
            dtFacilities.Columns.Add("FederalAgreements", typeof(string));  //55
            dtFacilities.Columns.Add("AirspaceDetermination", typeof(string));  //56
            dtFacilities.Columns.Add("CustomsAirportOfEntry", typeof(bool));  //57
            dtFacilities.Columns[57].AllowDBNull = true;
            dtFacilities.Columns.Add("CustomsLandingRights", typeof(bool));  //58
            dtFacilities.Columns[58].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryJointUse", typeof(bool));  //59
            dtFacilities.Columns[59].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryLandingRights", typeof(bool));  //60
            dtFacilities.Columns[60].AllowDBNull = true;
            dtFacilities.Columns.Add("InspectionMethod", typeof(string));  //61
            dtFacilities.Columns.Add("InspectionGroup", typeof(string));  //62
            dtFacilities.Columns.Add("LastInspectionDate", typeof(DateTime));  //63     MMDDYYYY ************************************
            dtFacilities.Columns[63].AllowDBNull = true;
            dtFacilities.Columns.Add("LastOwnerInformationDate", typeof(DateTime));  //64   MMDDYYYY ************************************
            dtFacilities.Columns[64].AllowDBNull = true;
            dtFacilities.Columns.Add("FuelTypes", typeof(string));  //65
            dtFacilities.Columns.Add("AirframeRepair", typeof(string));  //66
            dtFacilities.Columns.Add("PowerPlantRepair", typeof(string));  //67
            dtFacilities.Columns.Add("BottledOxygenType", typeof(string));  //68
            dtFacilities.Columns.Add("BulkOxygenType", typeof(string));  //69
            dtFacilities.Columns.Add("LightingSchedule", typeof(string));  //70
            dtFacilities.Columns.Add("BeaconSchedule", typeof(string));  //71
            dtFacilities.Columns.Add("ATCT", typeof(bool));  //72
            dtFacilities.Columns[72].AllowDBNull = true;
            dtFacilities.Columns.Add("UNICOMFrequencies", typeof(decimal)); //73
            dtFacilities.Columns[73].AllowDBNull = true;
            dtFacilities.Columns.Add("CTAFFrequency", typeof(decimal));  //74
            dtFacilities.Columns[74].AllowDBNull = true;
            dtFacilities.Columns.Add("SegmentedCircle", typeof(string));  //75
            dtFacilities.Columns.Add("BeaconColor", typeof(string));  //76
            dtFacilities.Columns.Add("NonCommercialLandingFee", typeof(bool));   //77
            dtFacilities.Columns[77].AllowDBNull = true;
            dtFacilities.Columns.Add("MedicalUse", typeof(bool));  //78
            dtFacilities.Columns[78].AllowDBNull = true;
            dtFacilities.Columns.Add("SingleEngineGA", typeof(int));  //79
            dtFacilities.Columns[79].AllowDBNull = true;
            dtFacilities.Columns.Add("MultiEngineGA", typeof(int));  //80
            dtFacilities.Columns[80].AllowDBNull = true;
            dtFacilities.Columns.Add("JetEngineGA", typeof(int));  //81
            dtFacilities.Columns[81].AllowDBNull = true;
            dtFacilities.Columns.Add("HelicoptersGA", typeof(int));  //82
            dtFacilities.Columns[82].AllowDBNull = true;
            dtFacilities.Columns.Add("GlidersOperational", typeof(int));  //83
            dtFacilities.Columns[83].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryOperational", typeof(int));  //84
            dtFacilities.Columns[84].AllowDBNull = true;
            dtFacilities.Columns.Add("Ultralights", typeof(int));  //85
            dtFacilities.Columns[85].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsCommercial", typeof(int));  //86
            dtFacilities.Columns[86].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsCommuter", typeof(string));  //87
            dtFacilities.Columns.Add("OperationsAirTaxi", typeof(int));  //88
            dtFacilities.Columns[88].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsGALocal", typeof(int));  //89
            dtFacilities.Columns[89].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsGAItin", typeof(int));  //90
            dtFacilities.Columns[90].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsMilitary", typeof(int));  //91
            dtFacilities.Columns[91].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsDate", typeof(DateTime));  //92   
            dtFacilities.Columns[92].AllowDBNull = true;
            dtFacilities.Columns.Add("AirportPositionSource", typeof(string));  //93
            dtFacilities.Columns.Add("AirportPositionSourceDate", typeof(DateTime));  //94
            dtFacilities.Columns[94].AllowDBNull = true;
            dtFacilities.Columns.Add("AirportElevationSource", typeof(string));  //95
            dtFacilities.Columns.Add("AirportElevationSourceDate", typeof(DateTime));  //96
            dtFacilities.Columns[96].AllowDBNull = true;
            dtFacilities.Columns.Add("ContractFuelAvailable", typeof(bool));  //97
            dtFacilities.Columns[97].AllowDBNull = true;
            dtFacilities.Columns.Add("TransientStorage", typeof(string));  //98
            dtFacilities.Columns.Add("OtherServices", typeof(string));  //99
            dtFacilities.Columns.Add("WindIndicator", typeof(string));  //100
            dtFacilities.Columns.Add("icao_ident", typeof(string));  //101
            dtFacilities.Columns.Add("AirportRecordFiller", typeof(string));  //102
            dtFacilities.Columns.Add("CityId", typeof(int));  //103
            dtFacilities.Columns[103].AllowDBNull = true;

            ////////////////////////////Attendence////////////////////
            dtAttendence.Columns.Add("SiteNumber", typeof(string));  //0
            dtAttendence.Columns.Add("State", typeof(string));  //1
            dtAttendence.Columns.Add("SequenceNumber", typeof(int));  //2
            dtFacilities.Columns[2].AllowDBNull = true;
            dtAttendence.Columns.Add("AttendanceSchedule", typeof(string));  //3


            ////////////////////////////Runway////////////////////
            dtRunway.Columns.Add("SiteNumber", typeof(string));  //0
            dtRunway.Columns.Add("State", typeof(string));       //1
            dtRunway.Columns.Add("RunwayID", typeof(string));    //2
            dtRunway.Columns.Add("RunwayLength", typeof(int));   //3
            dtRunway.Columns[3].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWidth", typeof(int));    //4
            dtRunway.Columns[4].AllowDBNull = true;
            dtRunway.Columns.Add("RunwaySurfaceTypeCondition", typeof(string));  //5
            dtRunway.Columns.Add("RunwaySurfaceTreatment", typeof(string));      //6
            dtRunway.Columns.Add("PavementClass", typeof(string));               //7
            dtRunway.Columns.Add("EdgeLightsIntensity", typeof(string));         //8
            dtRunway.Columns.Add("BaseEndID", typeof(string));                   //9
            dtRunway.Columns.Add("BaseEndTrueAlignment", typeof(int));           //10
            dtRunway.Columns[10].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndILSType", typeof(string));              //11
            dtRunway.Columns.Add("BaseEndRightTrafficPattern", typeof(bool));    //12
            dtRunway.Columns[12].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndMarkingsType", typeof(string));         //13
            dtRunway.Columns.Add("BaseEndMarkingsCondition", typeof(string));    //14
            dtRunway.Columns.Add("BaseEndPhysicalLatitude", typeof(decimal));     //15
            dtRunway.Columns[15].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndPhysicalLatitudeS", typeof(string));    //16
            dtRunway.Columns.Add("BaseEndPhysicalLongitude", typeof(decimal));    //17
            dtRunway.Columns[17].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndPhysicalLongitudeS", typeof(string));   //18
            dtRunway.Columns.Add("BaseEndPhysicalElevation", typeof(decimal));   //19
            dtRunway.Columns[19].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndCrossingHeight", typeof(int));          //20
            dtRunway.Columns[20].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGlidePathAngle", typeof(decimal));      //21
            dtRunway.Columns[21].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLatitude", typeof(decimal));    //22
            dtRunway.Columns[22].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLatitudeS", typeof(string));   //23
            dtRunway.Columns.Add("BaseEndDisplacedLongitude", typeof(decimal));   //24
            dtRunway.Columns[24].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLongitudeS", typeof(string));  //25
            dtRunway.Columns.Add("BaseEndDisplacedElevation", typeof(decimal));  //26
            dtRunway.Columns[26].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLength", typeof(int));         //27
            dtRunway.Columns[27].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTDZElevation", typeof(decimal));        //28
            dtRunway.Columns[28].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndVASI", typeof(string));                 //29
            dtRunway.Columns.Add("BaseEndRVR", typeof(string));                  //30
            dtRunway.Columns.Add("BaseEndRVV", typeof(bool));                    //31
            dtRunway.Columns[31].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndALS", typeof(string));                  //32 
            dtRunway.Columns.Add("BaseEndREIL", typeof(bool));                   //33
            dtRunway.Columns[33].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndCenterlineLights", typeof(bool));       //34
            dtRunway.Columns[34].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTouchdownLights", typeof(bool));        //35
            dtRunway.Columns[35].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectDescription", typeof(string));    //36
            dtRunway.Columns.Add("BaseEndObjectMarkLight", typeof(string));      //37
            dtRunway.Columns.Add("BaseEndPart77Category", typeof(string));       //38
            dtRunway.Columns.Add("BaseEndObjectClearSlope", typeof(int));        //39 
            dtRunway.Columns[39].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectHeight", typeof(int));            //40
            dtRunway.Columns[40].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectDistance", typeof(int));          //41
            dtRunway.Columns[41].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectOffset", typeof(string));         //42  
            dtRunway.Columns.Add("ReciprocalEndID", typeof(string));             //43
            dtRunway.Columns.Add("ReciprocalEndTrueAlignment", typeof(int));     //44
            dtRunway.Columns[44].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndILSType", typeof(string));        //45
            dtRunway.Columns.Add("ReciprocalEndRightTrafficPattern", typeof(bool));  //46
            dtRunway.Columns[46].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndMarkingsType", typeof(string));       //47
            dtRunway.Columns.Add("ReciprocalEndMarkingsCondition", typeof(string));  //48
            dtRunway.Columns.Add("ReciprocalEndPhysicalLatitude", typeof(decimal));   //49
            dtRunway.Columns[49].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndPhysicalLatitudeS", typeof(string));  //50
            dtRunway.Columns.Add("ReciprocalEndPhysicalLongitude", typeof(decimal));  //51
            dtRunway.Columns[51].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndPhysicalLongitudeS", typeof(string)); //52 
            dtRunway.Columns.Add("ReciprocalEndPhysicalElevation", typeof(decimal)); //53
            dtRunway.Columns[53].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndCrossingHeight", typeof(int));        //54
            dtRunway.Columns[54].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGlidePathAngle", typeof(decimal));        //55
            dtRunway.Columns[55].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLatitude", typeof(decimal));  //56
            dtRunway.Columns[56].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLatitudeS", typeof(string)); //57
            dtRunway.Columns.Add("ReciprocalEndDisplacedLongitude", typeof(decimal)); //58
            dtRunway.Columns[58].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLongitudeS", typeof(string));//59
            dtRunway.Columns.Add("ReciprocalEndDisplacedElevation", typeof(decimal));//60
            dtRunway.Columns[60].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLength", typeof(int));       //61
            dtRunway.Columns[61].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTDZElevation", typeof(decimal));      //62
            dtRunway.Columns[62].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndVASI", typeof(string));               //63 
            dtRunway.Columns.Add("ReciprocalEndRVR", typeof(string));                //64
            dtRunway.Columns.Add("ReciprocalEndRVV", typeof(bool));                  //65
            dtRunway.Columns[65].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndALS", typeof(string));                //66
            dtRunway.Columns.Add("ReciprocalEndREIL", typeof(bool));                 //67
            dtRunway.Columns[67].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndCenterlineLights", typeof(bool));    //68
            dtRunway.Columns[68].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTouchdownLights", typeof(bool));     //69
            dtRunway.Columns[69].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectDescription", typeof(string)); //70
            dtRunway.Columns.Add("ReciprocalEndObjectMarkLight", typeof(string));   //71
            dtRunway.Columns.Add("ReciprocalEndPart77Category", typeof(string));    //72
            dtRunway.Columns.Add("ReciprocalEndObjectClearSlope", typeof(int));     //73
            dtRunway.Columns[73].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectHeight", typeof(int));         //74
            dtRunway.Columns[74].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectDistance", typeof(int));       //75
            dtRunway.Columns[75].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectOffset", typeof(string));      //76
            dtRunway.Columns.Add("RunwayLengthSource", typeof(string));             //77
            dtRunway.Columns.Add("RunwayLengthSourceDate", typeof(DateTime));       //78
            dtRunway.Columns[78].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacitySW", typeof(decimal)); //79
            dtRunway.Columns[79].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDW", typeof(decimal)); //80
            dtRunway.Columns[80].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDT", typeof(decimal)); //81
            dtRunway.Columns[81].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDDT", typeof(decimal));    //82
            dtRunway.Columns[82].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGradient", typeof(decimal));               //83
            dtRunway.Columns[83].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGradientDirection", typeof(string));       //84
            dtRunway.Columns.Add("BaseEndPositionSource", typeof(string));          //85
            dtRunway.Columns.Add("BaseEndPositionSourceDate", typeof(DateTime));    //86
            dtRunway.Columns[86].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndElevationSource", typeof(string));         //87
            dtRunway.Columns.Add("BaseEndElevationSourceDate", typeof(DateTime));   //88
            dtRunway.Columns[88].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedThresholdPositionSource", typeof(string)); //89
            dtRunway.Columns.Add("BaseEndDisplacedThresholdPositionSourceDate", typeof(DateTime));  //90
            dtRunway.Columns[90].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedThresholdElevationSource", typeof(string));       //91
            dtRunway.Columns.Add("BaseEndDisplacedThresholdElevationSourceDate", typeof(DateTime)); //92
            dtRunway.Columns[92].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTouchdownZoneElevationSource", typeof(string));            //93
            dtRunway.Columns.Add("BaseEndTouchdownZoneElevationSourceDate", typeof(DateTime));      //94
            dtRunway.Columns[94].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTakeOffRunAvailableTORA", typeof(int));                    //95
            dtRunway.Columns[95].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTakeOffDistanceAvailableTODA", typeof(int));               //96
            dtRunway.Columns[96].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndAcltStopDistanceAvailableASDA", typeof(int));              //97
            dtRunway.Columns[97].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLDA", typeof(int));                //98
            dtRunway.Columns[98].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSO", typeof(int));              //99
            dtRunway.Columns[99].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOIntRwyID", typeof(string));   //100
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOIntEntityDesc", typeof(string)); //101
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLatitude", typeof(decimal)); //102
            dtRunway.Columns[102].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLatitudeS", typeof(string)); //103
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLongitude", typeof(decimal)); //104
            dtRunway.Columns[104].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLongitudeS", typeof(string)); //105
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOCoordSrc", typeof(string));        //106 
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOCoordSrcDate", typeof(DateTime));  //107
            dtRunway.Columns[107].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGradient", typeof(decimal));        //108
            dtRunway.Columns[108].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGradientDirection", typeof(string));  //109
            dtRunway.Columns.Add("ReciprocalEndPositionSource", typeof(string));  //110
            dtRunway.Columns.Add("ReciprocalEndPositionSourceDate", typeof(DateTime)); //111
            dtRunway.Columns[111].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndElevationSource", typeof(string));  //112
            dtRunway.Columns.Add("ReciprocalEndElevationSourceDate", typeof(DateTime));  //113
            dtRunway.Columns[113].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdPositionSource", typeof(string)); //114
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdPositionSourceDate", typeof(DateTime));  //115
            dtRunway.Columns[115].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdElevationSource", typeof(string));  //116
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdElevationSourceDate", typeof(DateTime)); //117
            dtRunway.Columns[117].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTouchdownZoneElevationSource", typeof(string));  //118
            dtRunway.Columns.Add("ReciprocalEndTouchdownZoneElevationSourceDate", typeof(DateTime)); //119
            dtRunway.Columns[119].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTakeOffRunAvailableTORA", typeof(int));  //120
            dtRunway.Columns[120].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTakeOffDistanceAvailableTODA", typeof(int)); //121
            dtRunway.Columns[121].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndAcltStopDistanceAvailableASDA", typeof(int));  //122
            dtRunway.Columns[122].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLDA", typeof(int));  //123
            dtRunway.Columns[123].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSO", typeof(int)); //124
            dtRunway.Columns[124].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOIntRwyID", typeof(string)); //125
            dtRunway.Columns[125].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOIntEntityDesc", typeof(string));  //126
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude", typeof(decimal));  //127
            dtRunway.Columns[127].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitudeS", typeof(string));  //128
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude", typeof(decimal));  //129
            dtRunway.Columns[129].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitudeS", typeof(string));  //130
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOCoordSrc", typeof(string));         //131
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate", typeof(DateTime));  //132
            dtRunway.Columns[132].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayRecordFiller", typeof(string));  //133
            dtRunway.Columns.Add("StateId", typeof(int));  //134
            dtRunway.Columns[134].AllowDBNull = true;



            ////////////////////////////Remark////////////////////
            dtRemark.Columns.Add("SiteNumber", typeof(string));  //0
            dtRemark.Columns.Add("State", typeof(string));  //1
            dtRemark.Columns.Add("RemarkElementName", typeof(string));  //2
            dtRemark.Columns.Add("Remarks", typeof(string));  //3
            try
            {
                DataRow dr;
                string strValue = "";
                string[] arr = { "", "", "" };
                string direction = "";
                foreach (var line in System.IO.File.ReadLines(ConfigurationReader.APTFilePath + @"UnZip\APT.txt"))
                {
                    
                    switch (line.Substring(0, 3))
                    {
                        case "APT":
                            {
                                dr = dtFacilities.NewRow();
                                foreach (var li in listAPT)
                                {

                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        case "EffectiveDate":
                                        case "OperationsDate":
                                        case "AirportPositionSourceDate":
                                        case "AirportElevationSourceDate":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDateTime(strValue);
                                                break;
                                            }
                                        //Decimal values
                                        case "ARPElevation":
                                        case "UNICOMFrequencies":
                                        case "CTAFFrequency":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDecimal(strValue);
                                                break;
                                            }
                                        // Int values
                                        case "MagneticVariationYear":
                                        case "TrafficPatternAltitude":
                                        case "DistanceFromCBD":
                                        case "LandAreaCoveredByAirport":
                                        case "SingleEngineGA":
                                        case "MultiEngineGA":
                                        case "JetEngineGA":
                                        case "HelicoptersGA":
                                        case "GlidersOperational":
                                        case "MilitaryOperational":
                                        case "Ultralights":
                                        case "OperationsCommercial":
                                        case "OperationsAirTaxi":
                                        case "OperationsGALocal":
                                        case "OperationsGAItin":
                                        case "OperationsMilitary":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        // Bool values
                                        case "TieInFSS":
                                        case "NOTAMService":
                                        case "CustomsAirportOfEntry":
                                        case "CustomsLandingRights":
                                        case "MilitaryJointUse":
                                        case "MilitaryLandingRights":
                                        case "ATCT":
                                        case "NonCommercialLandingFee":
                                        case "MedicalUse":
                                        case "ContractFuelAvailable":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)((strValue == "Y") ? true : false);
                                                break;
                                            }
                                        case "LastInspectionDate":
                                        case "LastOwnerInformationDate":
                                            {
                                                if (string.IsNullOrEmpty(strValue))
                                                {
                                                    dr[li.ColumnName] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    if (strValue.Length == 7)
                                                    {
                                                        dr[li.ColumnName] = new DateTime(Convert.ToInt16(strValue.Substring(3, 4)), Convert.ToInt16(strValue.Substring(0, 1)), Convert.ToInt16(strValue.Substring(1, 2)));
                                                    }
                                                    else
                                                    {
                                                        dr[li.ColumnName] = new DateTime(Convert.ToInt16(strValue.Substring(4, 4)), Convert.ToInt16(strValue.Substring(0, 2)), Convert.ToInt16(strValue.Substring(2, 2)));
                                                    }
                                                }
                                                break;
                                            }
                                        case "X":
                                        case "Y":
                                            {
                                                arr = strValue.Split('-');
                                                direction = arr[2].Substring(arr[2].Length - 1);
                                                arr[2] = arr[2].Remove(arr[2].Length - 1);
                                                dr[li.ColumnName] = (new Misc().CalculateLatitudeAndLongitude(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), direction)).ToString();
                                                break;
                                            }
                                        case "StateName":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? "" : Regex.Replace(strValue, "DIST. OF COLUMBIA", "Washington, DC", RegexOptions.IgnoreCase);
                                                break;
                                            }
                                        default:
                                            {
                                                dr[li.ColumnName] = strValue;
                                                break;
                                            }
                                    }
                                }
                                dr["CityId"] = DBNull.Value;
                                if (dr["StateName"].ToString() != "")
                                {
                                    dtFacilities.Rows.Add(dr);

                                }

                                break;
                            }
                        case "RWY":
                            {
                                DataRow drRWY;
                                drRWY = dtRunway.NewRow();
                                foreach (var li in listRWY)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        //Decimal Values
                                        case "BaseEndPhysicalElevation":
                                        case "BaseEndGlidePathAngle":
                                        case "BaseEndDisplacedElevation":
                                        case "BaseEndTDZElevation":
                                        case "ReciprocalEndPhysicalElevation":
                                        case "ReciprocalEndGlidePathAngle":
                                        case "ReciprocalEndDisplacedElevation":
                                        case "ReciprocalEndTDZElevation":
                                        case "RunwayWeightBerringCapacitySW":
                                        case "RunwayWeightBerringCapacityDW":
                                        case "RunwayWeightBerringCapacityDT":
                                        case "RunwayWeightBerringCapacityDDT":
                                        case "BaseEndGradient":
                                        case "ReciprocalEndGradient":

                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(strValue), 3);
                                                break;
                                            }
                                        //Integer Values
                                        case "RunwayLength":
                                        case "RunwayWidth":
                                        case "BaseEndTrueAlignment":
                                        case "BaseEndCrossingHeight":
                                        case "BaseEndDisplacedLength":
                                        case "BaseEndObjectClearSlope":
                                        case "BaseEndObjectHeight":
                                        case "BaseEndObjectDistance":
                                        case "ReciprocalEndTrueAlignment":
                                        case "ReciprocalEndCrossingHeight":
                                        case "ReciprocalEndDisplacedLength":
                                        case "ReciprocalEndObjectClearSlope":
                                        case "ReciprocalEndObjectHeight":
                                        case "ReciprocalEndObjectDistance":
                                        case "BaseEndTakeOffRunAvailableTORA":
                                        case "BaseEndTakeOffDistanceAvailableTODA":
                                        case "BaseEndAcltStopDistanceAvailableASDA":
                                        case "BaseEndLandingDistanceAvailableLDA":
                                        case "BaseEndLandingDistanceAvailableLAHSO":
                                        case "ReciprocalEndTakeOffRunAvailableTORA":
                                        case "ReciprocalEndTakeOffDistanceAvailableTODA":
                                        case "ReciprocalEndAcltStopDistanceAvailableASDA":
                                        case "ReciprocalEndLandingDistanceAvailableLDA":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSO":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        //Boolean values
                                        case "BaseEndRightTrafficPattern":
                                        case "BaseEndRVV":
                                        case "BaseEndREIL":
                                        case "BaseEndCenterlineLights":
                                        case "BaseEndTouchdownLights":
                                        case "ReciprocalEndRightTrafficPattern":
                                        case "ReciprocalEndRVV":
                                        case "ReciprocalEndREIL":
                                        case "ReciprocalEndCenterlineLights":
                                        case "ReciprocalEndTouchdownLights":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)((strValue == "Y") ? true : false);
                                                break;
                                            }
                                        //Lat long calculation
                                        case "BaseEndPhysicalLatitude":
                                        case "BaseEndPhysicalLongitude":
                                        case "BaseEndDisplacedLatitude":
                                        case "BaseEndDisplacedLongitude":
                                        case "ReciprocalEndPhysicalLatitude":
                                        case "ReciprocalEndPhysicalLongitude":
                                        case "ReciprocalEndDisplacedLatitude":
                                        case "ReciprocalEndDisplacedLongitude":
                                        case "BaseEndLandingDistanceAvailableLAHSOHldPtLatitude":
                                        case "BaseEndLandingDistanceAvailableLAHSOHldPtLongitude":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude":
                                            {
                                                if (string.IsNullOrEmpty(strValue))
                                                {
                                                    drRWY[li.ColumnName] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    arr = strValue.Split('-');
                                                    direction = arr[2].Substring(arr[2].Length - 1);
                                                    arr[2] = arr[2].Remove(arr[2].Length - 1);
                                                    drRWY[li.ColumnName] = (new Misc().CalculateLatitudeAndLongitude(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), direction)).ToString();
                                                }
                                                break;
                                            }
                                        //Datetime
                                        case "RunwayLengthSourceDate":
                                        case "BaseEndPositionSourceDate":
                                        case "BaseEndElevationSourceDate":
                                        case "BaseEndDisplacedThresholdPositionSourceDate":
                                        case "BaseEndDisplacedThresholdElevationSourceDate":
                                        case "BaseEndTouchdownZoneElevationSourceDate":
                                        case "BaseEndLandingDistanceAvailableLAHSOCoordSrcDate":
                                        case "ReciprocalEndPositionSourceDate":
                                        case "ReciprocalEndElevationSourceDate":
                                        case "ReciprocalEndDisplacedThresholdPositionSourceDate":
                                        case "ReciprocalEndDisplacedThresholdElevationSourceDate":
                                        case "ReciprocalEndTouchdownZoneElevationSourceDate":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDateTime(strValue);
                                                break;

                                            }
                                        default:
                                            {
                                                drRWY[li.ColumnName] = strValue;
                                                break;
                                            }

                                    }
                                    drRWY["StateId"] = DBNull.Value;
                                }
                                dtRunway.Rows.Add(drRWY);
                                break;
                            }
                        case "ATT":
                            {
                                DataRow drATT;
                                drATT = dtAttendence.NewRow();
                                foreach (var li in listATT)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        case "SequenceNumber":
                                            {
                                                drATT[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        default:
                                            {
                                                drATT[li.ColumnName] = strValue;
                                                break;
                                            }
                                    }
                                }
                                dtAttendence.Rows.Add(drATT);
                                break;
                            }
                        case "RMK":
                            {
                                DataRow drRMK;
                                drRMK = dtRemark.NewRow();
                                foreach (var li in listRMK)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    drRMK[li.ColumnName] = strValue;
                                }
                                dtRemark.Rows.Add(drRMK);
                                break;
                            }
                    }
                }

                //Delete all records which have the blank state name or code
                dtFacilities.Rows.Cast<DataRow>().Where(r => r.ItemArray[7].ToString() == "").ToList().ForEach(r => r.Delete());
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbFacility";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtFacilities;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportData", param);

                //Delete all records which have the blank state name or code
                dtRunway.Rows.Cast<DataRow>().Where(r => r.ItemArray[7].ToString() == "").ToList().ForEach(r => r.Delete());
                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbRunway";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtRunway;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetRunwayData", param);

                dtAttendence.Rows.Cast<DataRow>().Where(r => r.ItemArray[1].ToString() == "").ToList().ForEach(r => r.Delete());
                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbAirportAttendance";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtAttendence;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportAttendanceData", param);

                dtRemark.Rows.Cast<DataRow>().Where(r => r.ItemArray[1].ToString() == "").ToList().ForEach(r => r.Delete());
                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbAirportRemark";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtRemark;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportRemarkData", param);

                var context = new GuardianAvionicsEntities();
                var aptFileDownloadDetails = context.APTFileDownloadDetails.Create();
                aptFileDownloadDetails.CreateDate = DateTime.Now;
                aptFileDownloadDetails.FileName = nextDate.ToString("yyyy-MM-dd") + "/APT.zip";
                aptFileDownloadDetails.SubScriptionDate = nextDate;
                aptFileDownloadDetails.FileType = "APT";
                context.APTFileDownloadDetails.Add(aptFileDownloadDetails);
                context.SaveChanges();

                
            }
            catch (Exception ex)
            {

            }
        }
    }
}