﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BL_AL
{
    public static class Enumerations
    {

        //5044  is the available value

        /// <summary>
        /// Returns the StringValue from an Enumeration
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>The string</returns>
        public static string GetStringValue(this Enum value)
        {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValue), false) as StringValue[];

            // Return the first if there was a match.
            return attribs != null && attribs.Length > 0 ? attribs[0].Value : null;
        }



    

        public enum UserType
        {
            [StringValue("135")]
            Admin = 1,

            [StringValue("5005")]
            User = 2,

            [StringValue("5006")]
            Manufacturer = 3,

            [StringValue("5037")]
            Maintenance = 4,

            [StringValue("136")]
            NotAdmin = 5
        }

      
    }
}