﻿using System;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using BL_AL;

namespace GuardianPushNotificationService
{
    public partial class PushNotificationService : ServiceBase
    {
        System.Timers.Timer _timer;
        DateTime _scheduleTime;
        public PushNotificationService()
        {
            InitializeComponent();
            _timer = new System.Timers.Timer();
            //_scheduleTime = DateTime.Today.AddHours(9).AddMinutes(55);// Schedule to run once a day at 3:30 a.m.

            _scheduleTime = DateTime.Today.AddHours(09).AddMinutes(40);// Schedule to run once a day at 3:30 a.m.
        }


        protected override void OnStart(string[] args)
        {
            // For first time, set amount of seconds between current time and schedule time
            _timer.Enabled = true;
            _timer.Interval = _scheduleTime.Subtract(DateTime.Now).TotalSeconds * 1000;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
        }

        protected void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // 2. If tick for the first time, reset next run to every 24 hours
            if (_timer.Interval != 24 * 60 * 60 * 1000)
            {
                _timer.Interval = 24 * 60 * 60 * 1000;
            }
            // 1. Process Schedule Task
            // ----------------------------------
            // Add code to Process your task here
            // ----------------------------------

            StringBuilder str = new StringBuilder();
            try
            {
                System.Diagnostics.Debugger.Launch();
               
                ExceptionHandler.ReportError(new Exception("File upload on dropbox Service Started at " + DateTime.Now + Environment.NewLine), "");
                SendEmailReport emailReport = new SendEmailReport();
                emailReport.EmailReport();
                //PushNotificationMessage obj = new PushNotificationMessage();
                //obj.SendPushNotification();
                AircraftRegistrationData regData = new AircraftRegistrationData();
                regData.InsertRegistrationData();
                EmailErrorLogs objErrorLog = new EmailErrorLogs();
                objErrorLog.SendErrorLogs();
                ExceptionHandler.ReportError(new Exception("File upload on dropbox Service End at " + DateTime.Now + Environment.NewLine), "");
               
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(new Exception("File upload on dropbox Service Catch Block " + ex.Message + Environment.NewLine), "");
            }
        }
    }
}
