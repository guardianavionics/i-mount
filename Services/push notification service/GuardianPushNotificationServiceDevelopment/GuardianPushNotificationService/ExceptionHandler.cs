﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;

namespace GA.Common
{
    /// <summary>
    /// Class having the methods to create the log files for error/exception occuring services.
    /// </summary>
    public class ExceptionHandler
    {
        #region Members

        /// <summary>
        /// The Name of the Log File
        /// </summary>
        private string logFile;

        #endregion Members

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the exception has been writen to the log file.
        /// </summary>
        /// <value>The error logged.</value>
        public static bool ErrorLogged { get; set; }

        /// <summary>
        /// Gets Contains the LogsPath value from the config file.
        /// </summary>
        /// <value>The log path.</value>
        public string LogPath
        {
            get
            {
                return ConfigurationReader.LogPathKey;
            }
        }

        /// <summary>
        /// Gets or sets the LogFile value from the config file. The log file is combined with the date "MM-dd-yyyy.txt" string.
        /// </summary>
        /// <value>The log file.</value>
        protected string LogFile
        {
            get
            {
                // get the file path where logs are to be written
                this.logFile = Path.Combine(ConfigurationReader.LogPathKey, DateTime.Today.ToString("MM-dd-yyyy") + ".txt");
                return this.logFile;
            }
            set
            {
                this.logFile = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Writes an error to the error log file.
        /// </summary>
        /// <param name="err">An Exception object</param>
        public static void ReportError(Exception err)
        {
            var exceptionHandler = new ExceptionHandler();
            exceptionHandler.WriteError(err);
        }

        /// <summary>
        /// This overload allows to add a specific message when an exception occurs.
        /// </summary>
        /// <param name="err">Exception object</param>
        /// <param name="explicitMessage">The explicit Message</param>
        public static void ReportError(Exception err, string explicitMessage)
        {
            var exceptionHandler = new ExceptionHandler();
            exceptionHandler.WriteError(err, explicitMessage);
        }

        /// <summary>
        /// Checks if the directory exists and grant the user permissions
        /// </summary>
        /// <param name="directoryName">The directory name</param>
        public void ValidateDir(string directoryName)
        {
            // check if the directory where flogs are to be written exists or not.
            if (!DirExists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }

            // Giving permission for writing/reading/exploring
            var filePermissions = new FileIOPermission(FileIOPermissionAccess.AllAccess, directoryName);
            filePermissions.AllLocalFiles = FileIOPermissionAccess.AllAccess;
            filePermissions.Demand();
        }

        /// <summary>
        /// Writes an exception in an given alternative file.
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <param name="path">The alternative file path</param>
        protected static void WriteInAlternativeFile(Exception ex, string path)
        {
            TextWriter tw = new StreamWriter(path, true);
            tw.WriteLine(DateTime.Now.ToString());
            tw.WriteLine("-------------------------");
            tw.WriteLine(ex.Message);
            tw.WriteLine(ex.InnerException);
            tw.WriteLine(ex.StackTrace);
            tw.WriteLine(ex.Source);
            tw.WriteLine("=============");
            tw.Close();
        }

        /// <summary>
        /// Determines whether the specified directory name exists.
        /// </summary>
        /// <param name="dirName">[sDirName] - name of directory to check for</param>
        /// <returns>True if the directory exists, False otherwise</returns>
        public static bool DirExists(string dirName)
        {
            try
            {
                return Directory.Exists(dirName);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Dumps the exception to LogFile
        /// </summary>
        /// <param name="err">The Exception object.</param>
        private void WriteError(Exception err)
        {
            try
            {
                // validate the path
                ValidateDir(LogPath);

                var tab = new StringBuilder();

                // get the file stream and write the error details
                using (var fs = new FileStream(this.LogFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite, 1024))
                {
                    ErrorLogged = true;
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("=====");

                        while (null != err)
                        {
                            sw.WriteLine(string.Format("{0}{1} : {2} : {3}", tab, DateTime.Now, err.Source, err.Message));

                            if (!string.IsNullOrEmpty(err.HelpLink))
                            {
                                sw.WriteLine(err.HelpLink);
                            }

                            foreach (object key in err.Data.Keys)
                            {
                                if (null != err.Data[key])
                                {
                                    sw.WriteLine(string.Format("{0}          {1}={2}", tab, key, err.Data[key]));
                                }
                                else
                                {
                                    sw.WriteLine(string.Format("{0}          {1}=<null>", tab, key));
                                }
                            }

                            sw.WriteLine(err.StackTrace);

                            if (!string.IsNullOrEmpty(err.Source))
                            {
                                sw.WriteLine(err.Source);
                            }

                            err = err.InnerException;
                            tab.Append("          ");
                            sw.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // var fileName = new StringBuilder(Path.Combine(Environment.CurrentDirectory, "GuardianAvionics" + System.Threading.Thread.CurrentThread.GetHashCode()));

                var fileName = new StringBuilder(ConfigurationReader.AltLogPathKey + "\\galogs");
                fileName.Append("-" + DateTime.Now.Date.ToString("MM-dd-yyyy") + ".txt");
                WriteInAlternativeFile(ex, fileName.ToString());
                WriteInAlternativeFile(err, fileName.ToString());
            }
        }

        /// <summary>
        /// Overload to dumps the exception and an explicitMessage to LogFile
        /// </summary>
        /// <param name="err">The Exception object.</param>
        /// <param name="explicitMessage">The explicit message.</param>
        private void WriteError(Exception err, string explicitMessage)
        {
            try
            {
                // validate the path
                this.ValidateDir(this.LogPath);

                var tab = new StringBuilder();

                // get the file stream and write the error details
                using (var fs = new FileStream(this.LogFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite, 1024))
                {
                    ErrorLogged = true;
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("=====");
                        sw.WriteLine(explicitMessage);
                        while (null != err)
                        {
                            sw.WriteLine(string.Format("{0}{1} : {2} : {3}", tab, DateTime.Now, err.Source, err.Message));

                            if (!string.IsNullOrEmpty(err.HelpLink))
                            {
                                sw.WriteLine(err.HelpLink);
                            }

                            foreach (object key in err.Data.Keys)
                            {
                                if (null != err.Data[key])
                                {
                                    sw.WriteLine(string.Format("{0}          {1}={2}", tab, key, err.Data[key]));
                                }
                                else
                                {
                                    sw.WriteLine(string.Format("{0}          {1}=<null>", tab, key));
                                }
                            }

                            sw.WriteLine(err.StackTrace);

                            if (!string.IsNullOrEmpty(err.Source))
                            {
                                sw.WriteLine(err.Source);
                            }

                            err = err.InnerException;
                            tab.Append("          ");
                            sw.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var fileName = new StringBuilder(ConfigurationReader.AltLogPathKey + "\\galogs");

                //var fileName = new StringBuilder(Path.Combine(Environment.CurrentDirectory, "MobilePaymentServices" + System.Threading.Thread.CurrentThread.GetHashCode()));
                fileName.Append("-" + DateTime.Now.Date.ToString("MM-dd-yyyy") + ".txt");
                WriteInAlternativeFile(ex, fileName.ToString());
                WriteInAlternativeFile(err, fileName.ToString());
            }
        }

        #endregion Methods
    }
}
