﻿using System.ServiceProcess;
using System.Text;
using System;
//using System.Timers;
using NLog;
using System.Threading;

namespace ParseDataFileServiceLive
{
    public partial class ParseDataFileServiceLive : ServiceBase
    {
        // Timer timer = new Timer();
        private Timer parseDataTimer = null;
        private readonly int TimerInterval = 60000;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public ParseDataFileServiceLive()
        {
            InitializeComponent();
            //OnStart(new string[0]);
        }

        protected override void OnStart(string[] args)
        {
            //timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            //int time = 1;
            //time = time == 0 ? 60 : time;
            //// in milliseconds , default is 100 milli seconds
            //timer.Interval = 1000 * 60 * time; // 1 min * time
            //// ExceptionHandler.ReportError(new Exception("Time Interval " + timer.Interval.ToString()));
            //timer.Enabled = true;

            int startin = 60 - DateTime.Now.Second;
            int dueTime = startin * 1000;
            SetProcessingTimer(dueTime);
        }

        protected override void OnStop()
        {
        }

        public void SetProcessingTimer(int dueTime)
        {
            // m_Logger.LogDebug("==============SetProcessingTimer====================");
            // Create the delegate that invokes methods for the timer.
            TimerCallback timerDelegate = new TimerCallback(ProcessItems);

            // Create a timer that signals the delegate to invoke 
            // extracting, its a one click timer
            parseDataTimer = new Timer(timerDelegate, null, dueTime, TimerInterval);
        }

        private void StopProcessingTimer()
        {
            // m_Logger.LogDebug("==============StopProcessingTimer====================");
            parseDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void StartProcessingTimer(int timerInterval)
        {
            // m_Logger.LogDebug("==============StartProcessingTimer====================");
            parseDataTimer.Change(timerInterval, timerInterval);
        }

        public void ProcessItems(Object stateInfo)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                System.Diagnostics.Debugger.Launch();
                //timer.Stop();
                StopProcessingTimer();
                str.Append("Parse Data File Service Started at " + DateTime.Now + Environment.NewLine);
                GA.CommonForParseDataFile.Common objCommon = new GA.CommonForParseDataFile.Common();
                objCommon.SetLiveDataFlightStatus();
                ParseDataFile parseDataFile = new ParseDataFile();
                parseDataFile.ParseDataFiles();
                str.Append("Parse Data File parsing Complete");
                logger.Fatal(str.ToString());
                str.Clear();
                //  logger.Fatal("End Service");

                // timer.Start();
                StartProcessingTimer(TimerInterval);
            }
            catch (Exception ex)
            {
                logger.Fatal("Exception : Service Catch Block " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                SetProcessingTimer(TimerInterval);
            }
        }

        //void timer_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    try
        //    {
        //        System.Diagnostics.Debugger.Launch();
        //        timer.Stop();
        //        // logger.Fatal("Start Service");
        //        ParseDataFile parseDataFile = new ParseDataFile();
        //        parseDataFile.ParseDataFiles();
        //        //  logger.Fatal("End Service");
        //        timer.Start();
        //    }
        //    catch (Exception e1)
        //    {
        //        timer.Start();
        //        //logger.Fatal("Exception : " + e1.Message);
        //    }
        //}
    }
}
