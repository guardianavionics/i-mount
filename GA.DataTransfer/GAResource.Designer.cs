﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataTransfer {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class GAResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal GAResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GA.DataTransfer.GAResource", typeof(GAResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street address  is required..
        /// </summary>
        public static string AddressNotFound {
            get {
                return ResourceManager.GetString("AddressNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error occoured while deleting aircraft..
        /// </summary>
        public static string AircraftDeletionFailureMessage {
            get {
                return ResourceManager.GetString("AircraftDeletionFailureMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aircraft deleted successfully..
        /// </summary>
        public static string AircraftDeletionSuccessMessage {
            get {
                return ResourceManager.GetString("AircraftDeletionSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aircraft Profile.
        /// </summary>
        public static string AircraftProfilePageName {
            get {
                return ResourceManager.GetString("AircraftProfilePageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aero Units.
        /// </summary>
        public static string AreoUnitListingPageName {
            get {
                return ResourceManager.GetString("AreoUnitListingPageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aviation Chart.
        /// </summary>
        public static string AviationChart {
            get {
                return ResourceManager.GetString("AviationChart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City name should have aplhabets only.
        /// </summary>
        public static string CityNameOnlyAlphabetsMessage {
            get {
                return ResourceManager.GetString("CityNameOnlyAlphabetsMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City is required..
        /// </summary>
        public static string CityNotFound {
            get {
                return ResourceManager.GetString("CityNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company name is required..
        /// </summary>
        public static string CompanyNameNoFound {
            get {
                return ResourceManager.GetString("CompanyNameNoFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configuration Settings.
        /// </summary>
        public static string ConfigurationSetting {
            get {
                return ResourceManager.GetString("ConfigurationSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password and confirmation password do not match..
        /// </summary>
        public static string ConfirmPasswordDoNotMatchPassword {
            get {
                return ResourceManager.GetString("ConfirmPasswordDoNotMatchPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Password must have minimum length of 6 and a maximum length of 20..
        /// </summary>
        public static string ConfirmPasswordLengthMessage {
            get {
                return ResourceManager.GetString("ConfirmPasswordLengthMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm password is required..
        /// </summary>
        public static string ConfirmPasswordNotFoundMessage {
            get {
                return ResourceManager.GetString("ConfirmPasswordNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airframe Datalog.
        /// </summary>
        public static string DatalogPageName {
            get {
                return ResourceManager.GetString("DatalogPageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of birth is required..
        /// </summary>
        public static string DateOfBirthNotFound {
            get {
                return ResourceManager.GetString("DateOfBirthNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to delete Document..
        /// </summary>
        public static string DocumentDeletionFailedMessage {
            get {
                return ResourceManager.GetString("DocumentDeletionFailedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document deleted successfully..
        /// </summary>
        public static string DocumentDeletionSuccessMessage {
            get {
                return ResourceManager.GetString("DocumentDeletionSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flight Bag.
        /// </summary>
        public static string Documents {
            get {
                return ResourceManager.GetString("Documents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to cp88j54o9i1sx6n.
        /// </summary>
        public static string DropBoxAppkey {
            get {
                return ResourceManager.GetString("DropBoxAppkey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ifx8f01dkuiqjm4.
        /// </summary>
        public static string DropBoxAppSecret {
            get {
                return ResourceManager.GetString("DropBoxAppSecret", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email address already taken. Please use a different email address..
        /// </summary>
        public static string DuplicateEmailIdMessage {
            get {
                return ResourceManager.GetString("DuplicateEmailIdMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Id do not exist..
        /// </summary>
        public static string EmailIdDoNotExistMessage {
            get {
                return ResourceManager.GetString("EmailIdDoNotExistMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Id is incorrect..
        /// </summary>
        public static string EmailIdInWrongFormat {
            get {
                return ResourceManager.GetString("EmailIdInWrongFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Id is required..
        /// </summary>
        public static string EmailIdNotFound {
            get {
                return ResourceManager.GetString("EmailIdNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flight {0} - {1} {2} {3} {4}.xlsx.
        /// </summary>
        public static string ExcelSheetNameForFlight {
            get {
                return ResourceManager.GetString("ExcelSheetNameForFlight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flight on {0} {1} {2}.xlsx.
        /// </summary>
        public static string ExcelSheetNameForMultipleFlight {
            get {
                return ResourceManager.GetString("ExcelSheetNameForMultipleFlight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An Exception Occurred.
        /// </summary>
        public static string ExceptionMessage {
            get {
                return ResourceManager.GetString("ExceptionMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FlightPath{0}.fdr.
        /// </summary>
        public static string FDRFileName {
            get {
                return ResourceManager.GetString("FDRFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File can not be downloaded..
        /// </summary>
        public static string FileCanNotBeDownloadedMessage {
            get {
                return ResourceManager.GetString("FileCanNotBeDownloadedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First name is required..
        /// </summary>
        public static string FirstNameNotFound {
            get {
                return ResourceManager.GetString("FirstNameNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid first name..
        /// </summary>
        public static string FirstNameOnlyAlphabetsMessage {
            get {
                return ResourceManager.GetString("FirstNameOnlyAlphabetsMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Required field cannot be empty..
        /// </summary>
        public static string FirstNameRequired {
            get {
                return ResourceManager.GetString("FirstNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Graph and Maps.
        /// </summary>
        public static string GraphPageName {
            get {
                return ResourceManager.GetString("GraphPageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hardware Firmware Update.
        /// </summary>
        public static string HardwareFirmwareUpdate {
            get {
                return ResourceManager.GetString("HardwareFirmwareUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The email or password you entered is incorrect..
        /// </summary>
        public static string InValidCreadential {
            get {
                return ResourceManager.GetString("InValidCreadential", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IPassenger.
        /// </summary>
        public static string IPassenger {
            get {
                return ResourceManager.GetString("IPassenger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://maps.google.com/maps?q={0}.
        /// </summary>
        public static string KMLFileGoogleEarthUrl {
            get {
                return ResourceManager.GetString("KMLFileGoogleEarthUrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FlightPath{0}.kml.
        /// </summary>
        public static string KMLFileName {
            get {
                return ResourceManager.GetString("KMLFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last name is required..
        /// </summary>
        public static string LastNameNotFound {
            get {
                return ResourceManager.GetString("LastNameNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid last name..
        /// </summary>
        public static string LastNameOnlyAlphabetsMessage {
            get {
                return ResourceManager.GetString("LastNameOnlyAlphabetsMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage User.
        /// </summary>
        public static string ManageUser {
            get {
                return ResourceManager.GetString("ManageUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to Delete Map File..
        /// </summary>
        public static string MapFileDeletionFailureMesage {
            get {
                return ResourceManager.GetString("MapFileDeletionFailureMesage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Map file deleted successfully..
        /// </summary>
        public static string MapFileDeletionSuccessMessage {
            get {
                return ResourceManager.GetString("MapFileDeletionSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password is incorrect..
        /// </summary>
        public static string PasswordIncorrect {
            get {
                return ResourceManager.GetString("PasswordIncorrect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password must be a have minimum length of 6 and a maximum length of 50..
        /// </summary>
        public static string PasswordLengthMessage {
            get {
                return ResourceManager.GetString("PasswordLengthMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password is required..
        /// </summary>
        public static string PasswordNotFound {
            get {
                return ResourceManager.GetString("PasswordNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password is sent to your email Id..
        /// </summary>
        public static string PasswordSentToEmailIdMessage {
            get {
                return ResourceManager.GetString("PasswordSentToEmailIdMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone Number is required..
        /// </summary>
        public static string PhoneNumberNotFound {
            get {
                return ResourceManager.GetString("PhoneNumberNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone number is not valid..
        /// </summary>
        public static string PhoneNumberNotValidMessage {
            get {
                return ResourceManager.GetString("PhoneNumberNotValidMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pilot Logs.
        /// </summary>
        public static string PilotLogPageName {
            get {
                return ResourceManager.GetString("PilotLogPageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summary.
        /// </summary>
        public static string PilotSummary {
            get {
                return ResourceManager.GetString("PilotSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preferences and Setup.
        /// </summary>
        public static string PreferencePageName {
            get {
                return ResourceManager.GetString("PreferencePageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security answer is required..
        /// </summary>
        public static string SecurityAnswerNotFoundMessage {
            get {
                return ResourceManager.GetString("SecurityAnswerNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Session has expired..
        /// </summary>
        public static string SessionExpiredMessage {
            get {
                return ResourceManager.GetString("SessionExpiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Showing complete engine data..
        /// </summary>
        public static string ShowingCompleteEngineDataMessage {
            get {
                return ResourceManager.GetString("ShowingCompleteEngineDataMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State name should have aplhabets only.
        /// </summary>
        public static string StateNameOnlyAlphabetsMessage {
            get {
                return ResourceManager.GetString("StateNameOnlyAlphabetsMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State is required..
        /// </summary>
        public static string StateNotFound {
            get {
                return ResourceManager.GetString("StateNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Incorrect time format..
        /// </summary>
        public static string TimeFormatMessage {
            get {
                return ResourceManager.GetString("TimeFormatMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User account has been blocked..
        /// </summary>
        public static string UserBlockedMessage {
            get {
                return ResourceManager.GetString("UserBlockedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pilot Profile.
        /// </summary>
        public static string UserProfilePageName {
            get {
                return ResourceManager.GetString("UserProfilePageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip Code is required..
        /// </summary>
        public static string ZipCodeNotFound {
            get {
                return ResourceManager.GetString("ZipCodeNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip code should have only numbers.
        /// </summary>
        public static string ZipCodeOnlyNumbersMessage {
            get {
                return ResourceManager.GetString("ZipCodeOnlyNumbersMessage", resourceCulture);
            }
        }
    }
}
