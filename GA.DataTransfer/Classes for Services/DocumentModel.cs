﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class DocumentModel
    {
        [DataMember(Name = "sno")]
        public int SNO { get; set; }
        
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [IgnoreDataMember]
        public bool IsForAll { get; set; }

        [DataMember(Name = "canDeleteDocument")]
        public bool CanDeleteDocument { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "fileSizeInKB")]
        public int FileSizeInKB { get; set; }

        [DataMember(Name = "iconURL")]
        public string IconURL { get; set; }

        [DataMember(Name = "mineType")]
        public string MineType { get; set; }


    }
}
