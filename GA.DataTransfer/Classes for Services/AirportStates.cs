﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class AirportStates
    {
        [DataMember(Name = "stateName")]
        public string StateName { get; set; }
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "shortName")]
        public string ShortName { get; set; }
        [DataMember(Name = "state_Code_Id")]
        public Nullable<int> State_Code_Id { get; set; }
        [DataMember(Name = "cityList")]
        public List<AirportCitys> CityList { get; set; }


        [DataMember(Name ="navaidList")]
        public List<AirportNavaid> NavaidList { get; set; }
    }
}
