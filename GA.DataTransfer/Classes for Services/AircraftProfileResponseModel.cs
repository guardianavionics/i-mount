﻿using System.Linq;
using System.Runtime.Remoting.Contexts;
using GA.Common;
using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GA.DataTransfer.Classes_for_Services;
using System.Xml.Linq;
using System.IO;

namespace GA.DataTransfer
{
    [DataContract]
    public class AircraftProfileResponseModel : GeneralResponse
    {
        [DataMember(Name = "aircraftList")]
        public List<AircraftModel> AircraftList { get; set; }
                
        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }

        [DataMember(Name = "deletedAircraft")]
        public List<int> DAircraftList { get; set; }

        /// <summary>
        /// Converts AircraftProfile object into AircraftModel object
        /// </summary>
        /// <param name="aircraft">AircraftProfile object</param>
        /// <returns>AircraftModel object</returns>
        public static AircraftModel Create(AircraftProfile aircraft, DateTime? lastUpdateDate)
        {
            var context = new GuardianAvionicsEntities();
            var objAircraftProfileUpdateModel = new AircraftModel();
            objAircraftProfileUpdateModel.LastTransactionId = aircraft.LastTransactionId;
            objAircraftProfileUpdateModel.AircraftId = aircraft.Id;
            objAircraftProfileUpdateModel.UniqueId = aircraft.UniqueId ?? default(long);


            objAircraftProfileUpdateModel.Power = (aircraft.Power == null) ? 0 : (double)aircraft.Power;
            objAircraftProfileUpdateModel.RPM = (aircraft.RPM == null) ? 0 : (double)aircraft.RPM;
            objAircraftProfileUpdateModel.MP = (aircraft.MP == null) ? 0 : (double)aircraft.MP;
            objAircraftProfileUpdateModel.Altitude = (aircraft.Altitude == null) ? 0 : (double)aircraft.Altitude;
            objAircraftProfileUpdateModel.FuelBurnCruise = (aircraft.FuelBurnCruise == null) ? 0 : (double)aircraft.FuelBurnCruise;
            objAircraftProfileUpdateModel.TASCruise = (aircraft.TASCruise == null) ? 0 : (double)aircraft.TASCruise;
            objAircraftProfileUpdateModel.RateOfClimb = (aircraft.RateOfClimb == null) ? 0 : (double)aircraft.RateOfClimb;
            objAircraftProfileUpdateModel.TASClimb = (aircraft.TASClimb == null) ? 0 : (double)aircraft.TASClimb;
            objAircraftProfileUpdateModel.RateOfDescent = (aircraft.RateOfDescent == null) ? 0 : (double)aircraft.RateOfDescent;
            objAircraftProfileUpdateModel.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == null) ? 0 : (double)aircraft.FuelBurnRateOfDescent;
            objAircraftProfileUpdateModel.TASDescent = (aircraft.TASDescent == null) ? 0 : (double)aircraft.TASDescent;




            objAircraftProfileUpdateModel.Registration = aircraft.Registration ?? string.Empty;
            objAircraftProfileUpdateModel.Capacity = aircraft.Capacity ?? string.Empty;
            objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel == null ? string.Empty : aircraft.TaxiFuel.ToString(); //ChangeDataType
            objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel == null ? string.Empty : aircraft.AdditionalFuel.ToString();//ChangeDataType
            objAircraftProfileUpdateModel.FuelUnit = aircraft.FuelUnit ?? string.Empty;
            objAircraftProfileUpdateModel.WeightUnit = aircraft.WeightUnit ?? string.Empty;
            objAircraftProfileUpdateModel.SpeedUnit = aircraft.SpeedUnit ?? string.Empty;
            objAircraftProfileUpdateModel.VerticalSpeed = aircraft.VerticalSpeed ?? string.Empty;

            objAircraftProfileUpdateModel.IsAmericanAircraft = (aircraft.IsAmericaAircraft != null) && (bool)aircraft.IsAmericaAircraft;
            objAircraftProfileUpdateModel.Color = aircraft.Color ?? string.Empty;
            objAircraftProfileUpdateModel.FuelBurnRateClimb = (aircraft.FuelBurnRateClimb == null) ? 0 : (double)aircraft.FuelBurnRateClimb;
            objAircraftProfileUpdateModel.HomeBase = aircraft.HomeBase ?? string.Empty;
            objAircraftProfileUpdateModel.Make = aircraft.Make;
            objAircraftProfileUpdateModel.Model = aircraft.Model;
            objAircraftProfileUpdateModel.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
            objAircraftProfileUpdateModel.OtheAircraftrModel = aircraft.OtheAircraftrModel;
            objAircraftProfileUpdateModel.ManufacturerName = (aircraft.Make != null) ? aircraft.AircraftManufacturer.Name : string.Empty;
            objAircraftProfileUpdateModel.ModelName = (aircraft.Model != null) ? aircraft.AircraftModelList.ModelName : string.Empty;
            objAircraftProfileUpdateModel.AircraftType = aircraft.AircraftType ?? string.Empty;
            objAircraftProfileUpdateModel.AircraftSerialNumber = aircraft.AircraftSerialNo ?? string.Empty;
            objAircraftProfileUpdateModel.AircraftYear = aircraft.AircraftYear ?? default(int);
            objAircraftProfileUpdateModel.CurrentHobbsTime = aircraft.HobbsTime ?? default(double);
            objAircraftProfileUpdateModel.HobbsTimeOffset = aircraft.HobbsTimeOffset ?? default(double);
            objAircraftProfileUpdateModel.CurrentTachTime = aircraft.TachTime ?? default(double);
            objAircraftProfileUpdateModel.TachTimeOffset = aircraft.TachTimeOffset ?? default(double);
            objAircraftProfileUpdateModel.EngineMFGType = aircraft.EngineMFGType;
            objAircraftProfileUpdateModel.OtherEngineManufactureType = aircraft.OtherEngineManufacturerType;
            objAircraftProfileUpdateModel.EngineTBO = aircraft.EngineTBO ?? default(double);
            objAircraftProfileUpdateModel.Engine1LastMOHDate = (aircraft.Engine1LastMOHDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Engine1LastMOHDate);

            objAircraftProfileUpdateModel.CurrentEngineTime1 = aircraft.CurrentEngineTime ?? 0;
            objAircraftProfileUpdateModel.EngineTimeOffset1 = aircraft.EngineTimeOffset ?? 0;
            objAircraftProfileUpdateModel.PropMFG = aircraft.PropMFG;
            objAircraftProfileUpdateModel.OtherPropManufactureType = aircraft.OtherPropManufacturer;
            objAircraftProfileUpdateModel.Prop1TBO = (aircraft.Prop1TBO == null) ? default(int) : (int)aircraft.Prop1TBO;
            objAircraftProfileUpdateModel.Prop1OHDueDate = (aircraft.Prop1OHDueDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Prop1OHDueDate);

            objAircraftProfileUpdateModel.CurrentPropTime1 = aircraft.Prop1Time ?? 0;
            objAircraftProfileUpdateModel.PropTimeOffset1 = aircraft.Prop1TimeOffset ?? 0;
            objAircraftProfileUpdateModel.Engine2LastMOHDate = (aircraft.Engine2LastMOHDate == null)
                ? string.Empty
                : Misc.GetStringOnlyDate(aircraft.Engine2LastMOHDate);

            objAircraftProfileUpdateModel.CurrentEngineTime2 = aircraft.Engine2Time ?? 0;
            objAircraftProfileUpdateModel.EngineTimeOffset2 = (aircraft.Engine2TimeOffset ?? 0);


            objAircraftProfileUpdateModel.Prop2OHDueDate = (aircraft.Prop2OHDueDate == null)
                ? string.Empty
                : Misc.GetStringOnlyDate(aircraft.Prop2OHDueDate);

            objAircraftProfileUpdateModel.CurrentPropTime2 = (aircraft.Prop2Time ?? 0);
            objAircraftProfileUpdateModel.PropTimeOffset2 = (aircraft.Prop2Timeoffset ?? 0);


            var aeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Deleted);
            objAircraftProfileUpdateModel.IsAeroUnitBlocked = (aeroUnit == null) ? true : aeroUnit.Blocked;

            objAircraftProfileUpdateModel.AeroUnitNo = aircraft.AeroUnitNo ?? string.Empty;
            objAircraftProfileUpdateModel.ChargeBy = aircraft.ChargeBy;


            // converting Engine Type to codes.
            if (aircraft.EngineType == Enumerations.EngineType.MultipleEngine.ToString())
                objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.MultipleEngine.GetStringValue());
            else if (aircraft.EngineType == Enumerations.EngineType.SingleEngine.ToString())
                objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.SingleEngine.GetStringValue());
            else
                objAircraftProfileUpdateModel.EngineType = 0;

            // send aircraft profile image
            if (!String.IsNullOrEmpty(aircraft.ImageUrl))
            {
                objAircraftProfileUpdateModel.ImageName = ConfigurationReader.s3BucketURL +   ConfigurationReader.s3BucketImages + aircraft.ImageUrl;
            }
            else
            {
                objAircraftProfileUpdateModel.ImageName = aircraft.ImageUrl ?? string.Empty;
            }

            objAircraftProfileUpdateModel.Comm = aircraft.Comm;
            objAircraftProfileUpdateModel.Transponder = aircraft.Transponder;
            objAircraftProfileUpdateModel.EngineMonitor = aircraft.EngineMonitor;

            var documentList = context.Documents.Where(d => !d.Deleted && d.AircraftId == aircraft.Id).Select(d => d.Url).ToList();
            objAircraftProfileUpdateModel.AircraftComponentMakeAndModels =
            context.AircraftComponentMakeAndModels.Where(a => a.AircraftId == aircraft.Id).Select(s => new AircraftComponentMakeAndModelApp()
            {
                aircraftComponentId = s.AircraftComponentId,
                componentManufacturerId = s.ComponentManufacturerId,
                componentModelId = s.ComponentModelId,
                isShowInFlightBag = documentList.Contains(s.AircraftComponentModel.UserMannual),
                modelSerialNo = s.ComponentModelFreetext
            }).ToList();


            //var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.Id);

            //var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraft.Id && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
            //objAircraftProfileUpdateModel.ShareDataWithManufacturer = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
            //if (mappingAircraftManufacturerAndUsers != null)
            //{
            //    List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
            //    objAircraftProfileUpdateModel.ShareDataWithManufacturer.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
            //}
            //objAircraftProfileUpdateModel.ShareDataWithManufacturer = objAircraftProfileUpdateModel.ShareDataWithManufacturer.OrderBy(o => o.ManufacturerId).ToList();


            var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.Id);

            var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraft.Id && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
            List<ManufacturerAuthForDataLog> manufacturerAuthForDataLog = new List<ManufacturerAuthForDataLog>();
            manufacturerAuthForDataLog = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
            if (mappingAircraftManufacturerAndUsers != null)
            {
                List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                manufacturerAuthForDataLog.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
            }
            var manuIds = manufacturerAuthForDataLog.Where(w => w.ISAuthenticateForDatalog).OrderBy(o => o.ManufacturerId).Select(s => s.ManufacturerId).ToList();
            objAircraftProfileUpdateModel.ShareDataWithManufacturer = string.Join(",", manuIds);


            objAircraftProfileUpdateModel.OwnerEmailId =
                context.Profiles.FirstOrDefault(p => p.Id == aircraft.OwnerProfileId).EmailId;

            if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml"))
            {

                XDocument loaded = XDocument.Load(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml");
                objAircraftProfileUpdateModel.EngineSettings = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + loaded.ToString().Replace("[]", "");
            }
            else
            {
                objAircraftProfileUpdateModel.EngineSettings = "";
            }
            AircraftProfileResponseModel getval = new AircraftProfileResponseModel();
            var objFuelQuantity = getval.getfueldetail(aircraft.Id);
            objAircraftProfileUpdateModel.FuelQuantityModels = objFuelQuantity;

            return objAircraftProfileUpdateModel;
        }


        public List<AircraftModel> CreateList(List<AircraftProfile> aircraftList)
        {
            List<AircraftModel> aircraftModelList = new List<AircraftModel>();
            var context = new GuardianAvionicsEntities();

            foreach (var aircraft in aircraftList)
            {
                var objAircraftProfileUpdateModel = new AircraftModel();
                objAircraftProfileUpdateModel.LastTransactionId = aircraft.LastTransactionId;
                objAircraftProfileUpdateModel.Registration = aircraft.Registration ?? string.Empty;
                objAircraftProfileUpdateModel.Capacity = aircraft.Capacity ?? string.Empty;
                //objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel ?? string.Empty;
                objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel == null ? string.Empty : aircraft.TaxiFuel.ToString(); //ChangeDataType
                //objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel ?? string.Empty;
                objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel == null ? string.Empty : aircraft.AdditionalFuel.ToString();//ChangeDataType
                objAircraftProfileUpdateModel.FuelUnit = aircraft.FuelUnit ?? string.Empty;
                objAircraftProfileUpdateModel.WeightUnit = aircraft.WeightUnit ?? string.Empty;
                objAircraftProfileUpdateModel.SpeedUnit = aircraft.SpeedUnit ?? string.Empty;
                objAircraftProfileUpdateModel.VerticalSpeed = aircraft.VerticalSpeed ?? string.Empty;
                objAircraftProfileUpdateModel.Power = (aircraft.Power == null) ? 0 : (double)aircraft.Power;
                objAircraftProfileUpdateModel.RPM = (aircraft.RPM == null) ? 0 : (double)aircraft.RPM;
                objAircraftProfileUpdateModel.MP = (aircraft.MP == null) ? 0 : (double)aircraft.MP;
                objAircraftProfileUpdateModel.Altitude = (aircraft.Altitude == null) ? 0 : (double)aircraft.Altitude;
                objAircraftProfileUpdateModel.FuelBurnCruise = (aircraft.FuelBurnCruise == null) ? 0 : (double)aircraft.FuelBurnCruise;
                objAircraftProfileUpdateModel.TASCruise = (aircraft.TASCruise == null) ? 0 : (double)aircraft.TASCruise;
                objAircraftProfileUpdateModel.RateOfClimb = (aircraft.RateOfClimb == null) ? 0 : (double)aircraft.RateOfClimb;
                objAircraftProfileUpdateModel.TASClimb = (aircraft.TASClimb == null) ? 0 : (double)aircraft.TASClimb;
                objAircraftProfileUpdateModel.RateOfDescent = (aircraft.RateOfDescent == null) ? 0 : (double)aircraft.RateOfDescent;
                objAircraftProfileUpdateModel.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == null) ? 0 : (double)aircraft.FuelBurnRateOfDescent;
                objAircraftProfileUpdateModel.TASDescent = (aircraft.TASDescent == null) ? 0 : (double)aircraft.TASDescent;
                objAircraftProfileUpdateModel.IsAmericanAircraft = (aircraft.IsAmericaAircraft != null) && (bool)aircraft.IsAmericaAircraft;
                objAircraftProfileUpdateModel.AircraftId = aircraft.Id;
                objAircraftProfileUpdateModel.Color = aircraft.Color ?? string.Empty;
                objAircraftProfileUpdateModel.FuelBurnRateClimb = (aircraft.FuelBurnRateClimb == null) ? 0 : (double)aircraft.FuelBurnRateClimb;
                objAircraftProfileUpdateModel.HomeBase = aircraft.HomeBase ?? string.Empty;
                objAircraftProfileUpdateModel.Make = aircraft.Make;
                objAircraftProfileUpdateModel.Model = aircraft.Model;
                objAircraftProfileUpdateModel.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                objAircraftProfileUpdateModel.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                objAircraftProfileUpdateModel.ManufacturerName = (aircraft.Make != null) ? aircraft.AircraftManufacturer.Name : string.Empty;
                objAircraftProfileUpdateModel.ModelName = (aircraft.Model != null) ? aircraft.AircraftModelList.ModelName : string.Empty;
                objAircraftProfileUpdateModel.AircraftType = aircraft.AircraftType ?? string.Empty;
                objAircraftProfileUpdateModel.AircraftSerialNumber = aircraft.AircraftSerialNo ?? string.Empty;
                objAircraftProfileUpdateModel.AircraftYear = aircraft.AircraftYear ?? default(int);
                objAircraftProfileUpdateModel.CurrentHobbsTime = aircraft.HobbsTime ?? default(double);
                objAircraftProfileUpdateModel.HobbsTimeOffset = aircraft.HobbsTimeOffset ?? default(double);
                objAircraftProfileUpdateModel.CurrentTachTime = aircraft.TachTime ?? default(double);
                objAircraftProfileUpdateModel.TachTimeOffset = aircraft.TachTimeOffset ?? default(double);
                objAircraftProfileUpdateModel.EngineMFGType = aircraft.EngineMFGType;
                objAircraftProfileUpdateModel.OtherEngineManufactureType = aircraft.OtherEngineManufacturerType;
                objAircraftProfileUpdateModel.EngineTBO = aircraft.EngineTBO ?? default(double);
                objAircraftProfileUpdateModel.Engine1LastMOHDate = (aircraft.Engine1LastMOHDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Engine1LastMOHDate);

                objAircraftProfileUpdateModel.CurrentEngineTime1 = aircraft.CurrentEngineTime ?? 0;
                objAircraftProfileUpdateModel.EngineTimeOffset1 = aircraft.EngineTimeOffset ?? 0;
                objAircraftProfileUpdateModel.PropMFG = aircraft.PropMFG;
                objAircraftProfileUpdateModel.OtherPropManufactureType = aircraft.OtherPropManufacturer;
                objAircraftProfileUpdateModel.Prop1TBO = (aircraft.Prop1TBO == null) ? default(int) : (int)aircraft.Prop1TBO;
                objAircraftProfileUpdateModel.Prop1OHDueDate = (aircraft.Prop1OHDueDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Prop1OHDueDate);

                objAircraftProfileUpdateModel.CurrentPropTime1 = aircraft.Prop1Time ?? 0;
                objAircraftProfileUpdateModel.PropTimeOffset1 = aircraft.Prop1TimeOffset ?? 0;
                objAircraftProfileUpdateModel.Engine2LastMOHDate = (aircraft.Engine2LastMOHDate == null)
                    ? string.Empty
                    : Misc.GetStringOnlyDate(aircraft.Engine2LastMOHDate);

                objAircraftProfileUpdateModel.CurrentEngineTime2 = aircraft.Engine2Time ?? 0;
                objAircraftProfileUpdateModel.EngineTimeOffset2 = (aircraft.Engine2TimeOffset ?? 0);


                objAircraftProfileUpdateModel.Prop2OHDueDate = (aircraft.Prop2OHDueDate == null)
                    ? string.Empty
                    : Misc.GetStringOnlyDate(aircraft.Prop2OHDueDate);

                objAircraftProfileUpdateModel.CurrentPropTime2 = (aircraft.Prop2Time ?? 0);
                objAircraftProfileUpdateModel.PropTimeOffset2 = (aircraft.Prop2Timeoffset ?? 0);


                var aeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Deleted);
                objAircraftProfileUpdateModel.IsAeroUnitBlocked = (aeroUnit == null) ? true : aeroUnit.Blocked;

                objAircraftProfileUpdateModel.AeroUnitNo = aircraft.AeroUnitNo ?? string.Empty;
                objAircraftProfileUpdateModel.ChargeBy = aircraft.ChargeBy;


                // converting Engine Type to codes.
                if (aircraft.EngineType == Enumerations.EngineType.MultipleEngine.ToString())
                    objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.MultipleEngine.GetStringValue());
                else if (aircraft.EngineType == Enumerations.EngineType.SingleEngine.ToString())
                    objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.SingleEngine.GetStringValue());
                else
                    objAircraftProfileUpdateModel.EngineType = 0;

                // send aircraft profile image
                if (!String.IsNullOrEmpty(aircraft.ImageUrl))
                {
                    objAircraftProfileUpdateModel.ImageName = ConfigurationReader.s3BucketURL +  ConfigurationReader.s3BucketImages + aircraft.ImageUrl;
                }
                else
                {
                    objAircraftProfileUpdateModel.ImageName = aircraft.ImageUrl ?? string.Empty;
                }


                objAircraftProfileUpdateModel.UniqueId = aircraft.UniqueId ?? default(long);

                objAircraftProfileUpdateModel.Comm = aircraft.Comm;
                objAircraftProfileUpdateModel.Transponder = aircraft.Transponder;
                objAircraftProfileUpdateModel.EngineMonitor = aircraft.EngineMonitor;

                var documentList = context.Documents.Where(d => !d.Deleted && d.AircraftId == aircraft.Id).Select(d => d.Url).ToList();
                objAircraftProfileUpdateModel.AircraftComponentMakeAndModels =
                context.AircraftComponentMakeAndModels.Where(a => a.AircraftId == aircraft.Id).Select(s => new AircraftComponentMakeAndModelApp()
                {
                    aircraftComponentId = s.AircraftComponentId,
                    componentManufacturerId = s.ComponentManufacturerId,
                    componentModelId = s.ComponentModelId,
                    isShowInFlightBag = documentList.Contains(s.AircraftComponentModel.UserMannual),
                    modelSerialNo = s.ComponentModelFreetext
                }).ToList();





                var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.Id);

                var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraft.Id && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
                List<ManufacturerAuthForDataLog> manufacturerAuthForDataLog = new List<ManufacturerAuthForDataLog>();
                manufacturerAuthForDataLog = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
                if (mappingAircraftManufacturerAndUsers != null)
                {
                    List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                    manufacturerAuthForDataLog.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
                }
                var manuIds = manufacturerAuthForDataLog.Where(w => w.ISAuthenticateForDatalog).OrderBy(o => o.ManufacturerId).Select(s => s.ManufacturerId).ToList();
                objAircraftProfileUpdateModel.ShareDataWithManufacturer = string.Join(",", manuIds);


                objAircraftProfileUpdateModel.OwnerEmailId =
                    context.Profiles.FirstOrDefault(p => p.Id == aircraft.OwnerProfileId).EmailId;

                if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml"))
                {
                    XDocument loaded = XDocument.Load(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml");
                    objAircraftProfileUpdateModel.EngineSettings = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + loaded.ToString().Replace("[]", ""); ;
                }
                else
                {
                    objAircraftProfileUpdateModel.EngineSettings = "";
                }
                aircraftModelList.Add(objAircraftProfileUpdateModel);

            }

            return aircraftModelList;
        }


        /// <summary>
        /// Returns AircraftProfileResponseModel Object with error code set as exception
        /// </summary>
        /// <returns>AircraftProfileResponseModel Object</returns>
        public AircraftProfileResponseModel Exception()
        {
            return new AircraftProfileResponseModel
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue(),
            };
        }

        public FuelQuantityModel getfueldetail(int aircraftId)
        {
            try
            {
                int SNO = aircraftId;
                FuelQuantityModel model = new FuelQuantityModel();
                using (var context = new GuardianAvionicsEntities())
                {

                    var list = (from FD in context.FuelDetails
                                join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                where FD.aircraftId == SNO
                                select new { FD, FL }).ToList();
                    if (list.Count > 0)
                    {
                        List<FuelQuantityModel.Frequencies> mfListright = new List<FuelQuantityModel.Frequencies>();
                        FuelQuantityModel.Right a = new FuelQuantityModel.Right();
                        List<FuelQuantityModel.Frequencies> mfListleft = new List<FuelQuantityModel.Frequencies>();
                        for (var i = 0; i < list.Count; i++)
                        {
                            if (list[i].FD.fuelsideType.ToLower() == "right")
                            {
                                a.fuelValue = Convert.ToDouble(list[i].FD.fuelValue);
                                FuelQuantityModel.Frequencies mf = new FuelQuantityModel.Frequencies();
                                mf.frequencyAtLevel = Convert.ToInt16(list[i].FL.frequencyAtLevel);
                                mf.gallonLevel = Convert.ToDouble(list[i].FL.gallonLevel);
                                mf.frequency = Convert.ToDouble(list[i].FL.frequency);
                                mfListright.Add(mf);
                                a.frequencies = mfListright;
                            }
                            model.aircraftId = Convert.ToInt32(list[i].FD.aircraftId);
                        }
                        FuelQuantityModel.Left ab = new FuelQuantityModel.Left();
                        for (var i = 0; i < list.Count; i++)
                        {
                            if (list[i].FD.fuelsideType.ToLower() == "left")
                            {
                                ab.fuelValue = Convert.ToDouble(list[i].FD.fuelValue);

                                FuelQuantityModel.Frequencies mf = new FuelQuantityModel.Frequencies();
                                mf.frequencyAtLevel = Convert.ToInt16(list[i].FL.frequencyAtLevel);
                                mf.gallonLevel = Convert.ToDouble(list[i].FL.gallonLevel);
                                mf.frequency = Convert.ToDouble(list[i].FL.frequency);
                                mfListleft.Add(mf);
                                ab.frequencies = mfListleft;
                            }
                            model.aircraftId = Convert.ToInt32(list[i].FD.aircraftId);
                        }
                        model.right = a;
                        model.left = ab;
                    }
                    else
                    {
                        model = null;
                    }
                }

                return model;
            }
            catch (Exception e)
            {
                string aircraftLogFileName = GetAircraftRegAndSerialNo(1);
                string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                if (File.Exists(aircraftLogPath))
                {
                    using (StreamWriter sw = File.AppendText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - GetAircraftDetails)");
                        //sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - getAircraftDetails)");
                        sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(1));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                return new FuelQuantityModel().Exception();
            }
        }

        public string GetAircraftRegAndSerialNo(int aircraftId)
        {
            var msg = string.Empty;
            var context = new GuardianAvionicsEntities();
            var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);
            if (aircraft != null)
            {
                msg = aircraft.Registration.Replace(" ", "_") + "-" + aircraft.AircraftSerialNo;
            }
            return msg;
        }
    }
}
