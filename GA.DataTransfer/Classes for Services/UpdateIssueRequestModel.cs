﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
  public  class UpdateIssueRequestModel
    {
        [DataMember(Name = "issueId")]
        public int IssueId { get; set; }


        [DataMember(Name = "closedBy")]
        public int ClosedBy { get; set; }
    }
}
