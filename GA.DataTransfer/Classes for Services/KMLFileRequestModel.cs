﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class KMLFileRequestModel
    {
        [DataMember(Name = "flightId")]
        public int FlightId { get; set; }
    }
}
