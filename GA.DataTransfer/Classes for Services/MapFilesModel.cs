﻿using System;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class MapFilesModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string NameOfMapFile { get; set; }

        [DataMember(Name = "url")]
        public string UrlForDown { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "fileSize")]
        public int? FileSize { get; set; }

    }
}
