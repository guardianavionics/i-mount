﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
      [DataContract]
  public  class GetNotificationRequest
    {
          [DataMember(Name = "lastUpdateDate")]
          public string LastUpdateDate { get; set; }
    }
}
