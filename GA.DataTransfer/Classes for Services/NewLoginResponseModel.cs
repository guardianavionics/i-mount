﻿using GA.Common;
using GA.DataTransfer.Classes_for_Services;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class NewLoginResponseModel : GeneralResponse
    {
        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        [DataMember(Name = "medicalCertificateList")]
        public List<MedicalCertificateModel> MedicalCertificateList { get; set; }

        [DataMember(Name = "licensesList")]
        public List<LicensesModel> LicensesList { get; set; }

        [DataMember(Name = "countryCode")]
        public string CountryCode { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "streetAddress")]
        public string StreetAddress { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [DataMember(Name = "imageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

        
 

        [DataMember(Name = "deletedMedicalCertificates")]
        public int[] DeletedMedicalCertificate { get; set; }

        [DataMember(Name = "deletedLicences")]
        public int[] DeletedLicences { get; set; }

        [DataMember(Name = "deletedRatings")]
        public int[] DeletedRatings { get; set; }

        //[DataMember(Name = "dropDownListData")]
        //public ManufacturereModelListResponse DropDownListData { get; set; }

        [DataMember(Name = "socialLoginAccountType")] // 1 for Facebook, 2 for Gmail
        public int SocialLoginAccountType { get; set; }

        [DataMember(Name = "socialLoginAccountId")]
        public string SocialLoginAccountId { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name = "isNewVersionAvaliable")]
        public bool isNewVersionAvaliable { get; set; }
    }
}
