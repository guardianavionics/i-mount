﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    
    public class RequestOTPVeriftcationModel
    {
        [DataMember(Name = "EmailId")]
        public string emailId { get; set; }
        [DataMember(Name = "PhoneNumber")]
        public string phoneNumber { get; set; }
    }
}
