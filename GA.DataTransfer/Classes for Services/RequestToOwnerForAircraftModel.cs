﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
   public class RequestToOwnerForAircraftModel
    {
        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

      
    }
}
