﻿using GA.Common;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class RegistrationResponseModel : GeneralResponse
    {
        #region properties

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        #endregion

        /// <summary>
        /// Creates response message for Response of registaration service.
        /// </summary>
        /// <param name="aircraftId">Unique Id of aircraft table</param>
        /// <param name="profileId">Unique Id of profile table</param>
        /// <param name="returnCode">Response message</param>
        /// <returns></returns>
        public RegistrationResponseModel Create(int profileId, Enumerations.RegistrationReturnCodes returnCode)//int aircraftId , long aircraftUniqueId, 
        {
            return new RegistrationResponseModel()
            {
                ProfileId = profileId,
                ResponseCode = ((int)returnCode).ToString(),
                ResponseMessage = returnCode.GetStringValue()
            };
        }
    }
}
