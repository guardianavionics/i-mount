﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class GeneralResponse
    {
        [DataMember(Name = "responseCode")]
        public string ResponseCode { get; set; }

        [DataMember(Name = "responseMessage")]
        public string ResponseMessage { get; set; }
    }
}
