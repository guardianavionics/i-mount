﻿using System.Runtime.Serialization;
using System.Collections.Generic;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SquawkListRequest
    {
        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }
    }
}
