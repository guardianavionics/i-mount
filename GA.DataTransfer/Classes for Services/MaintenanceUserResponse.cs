﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class MaintenanceUserResponse : GeneralResponse
    {

        [DataMember(Name = "maintenanceUserList")]
        public List<MaintenanceUserModel> MaintenanceUserList { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }
    }
}
