﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using GA.Common;

namespace GA.DataTransfer
{
     [DataContract]
    public class PassengerMapFileResponse :GeneralResponse
    {
        [DataMember(Name = "passengerMapFileList")]
        public List<PassengerProMapFileModel> PassengerMapFileList { get; set; }

        [DataMember(Name = "lastUpdatedDateTime")]
        public string LastUpdatedDateTime { get; set; }

        [DataMember(Name = "Region")]
        public List<ListItems> RegionsList{ get; set; }
       
        public static PassengerMapFileResponse Exception()
        {
            return new PassengerMapFileResponse
            {
                ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
            };
        }
    }
}
