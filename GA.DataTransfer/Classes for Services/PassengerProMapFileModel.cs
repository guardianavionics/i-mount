﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
   public  class PassengerProMapFileModel
    {
        [DataMember(Name = "FileName")]
        public string FileName { get; set; }

        [DataMember(Name = "RegionName")]
        public string Region { get; set; }

        [DataMember(Name = "RegionId")]
        public int RegionId { get; set; }

        [DataMember(Name = "ZoomLevel")]
        public int ZoomLevel { get; set; }

        [DataMember(Name = "FileSize")]
        public int FileSizeInMB { get; set; }

        [DataMember(Name = "URL")]
        public string URl { get; set; }

        [DataMember(Name = "Title")]
        public string Title { get; set; }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

    }
}
