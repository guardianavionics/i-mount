﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public  class AirportForFreqAndNavaid
    {
        [DataMember(Name = "airportName")]
        public string AirportName { get; set; }

        [DataMember(Name = "military")]
        public string Military { get; set; }

        [DataMember(Name = "apt_ident")]
        public string apt_ident { get; set; }

        [DataMember(Name = "icao_ident")]
        public string icao_ident { get; set; }

        [DataMember(Name = "airport_name_Id")]
        public Nullable<int> airport_name_Id { get; set; }

        [DataMember(Name = "city_Name_Id")]
        public Nullable<int> city_name_Id { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "x")]
        public Nullable<double> X { get; set; }

        [DataMember(Name = "y")]
        public Nullable<double> Y { get; set; }

        [DataMember(Name = "altitude")]
        public Nullable<int> Altittude { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "cityId")]
        public Nullable<int> CityId { get; set; }

        [DataMember(Name = "featureId")]
        public string FeatureId { get; set; }

        [DataMember(Name = "frequencyList")]
        public List<AptFrequency> FrequencyList { get; set; }

        [DataMember(Name = "runwayList")]
        public List<AptRunway> RunwayList { get; set; }

    }
}
