﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class CategoryModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "categoryName")]
        public string CategoryName { get; set; }
        [DataMember(Name = "aircraftId")]
        public Nullable<int> AircraftId { get; set; }
        [DataMember(Name = "createdDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember(Name = "updatedDate")]
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        [DataMember(Name = "itemList")]
        public List<CategoryItemModel> ItemList { get; set; }
        [DataMember(Name = "isActive")]
        public Nullable<bool> IsActive { get; set; }
        [DataMember(Name = "tabColor")]
        public string TabColor { get; set; }
        [DataMember(Name = "priority")]
        public int? Priority { get; set; }
        [DataMember(Name = "categoryType")]
        public int? CategoryType { get; set; }
        //[DataMember(Name = "iOSCategoryId")]
        //public int? IOSCategoryId { get; set; }
    }
    [DataContract]
    public class CategoryItemModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "categoryItemName")]
        public string CategoryItemName { get; set; }
        [DataMember(Name = "createdBy")]
        public Nullable<int> CreatedBy { get; set; }
        [DataMember(Name = "createdDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember(Name = "updatedDate")]
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        [DataMember(Name = "isActive")]
        public Nullable<bool> IsActive { get; set; }
        [DataMember(Name = "rowIndex")]
        public Nullable<int> RowIndex { get; set; }
        [DataMember(Name = "condition")]
        public string Condition { get; set; }
        //[DataMember(Name = "iOSCategoryItemId")]
        //public Nullable<int> IOSCategoryItemId { get; set; }

    }
}
