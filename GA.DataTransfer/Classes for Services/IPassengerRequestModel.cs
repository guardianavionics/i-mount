﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class IPassengerRequestModel
    {
        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }
    }
}
