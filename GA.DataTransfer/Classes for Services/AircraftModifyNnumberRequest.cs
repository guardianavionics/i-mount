﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using GA.DataLayer;

namespace GA.DataTransfer
{
    [DataContract]
    public class AircraftModifyNnumberRequest
    {
        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "registration")]
        public string Registration { get; set; }

        [DataMember(Name = "make", IsRequired = false, EmitDefaultValue = true)]
        public int? Make { get; set; }//*123

        [DataMember(Name = "model", IsRequired = false, EmitDefaultValue = true)]
        public int? Model { get; set; }//*123

        [DataMember(Name = "color", IsRequired = false, EmitDefaultValue = true)]
        public string Color { get; set; }

        [DataMember(Name = "homeBase", IsRequired = false, EmitDefaultValue = true)]
        public string HomeBase { get; set; }

        [DataMember(Name = "capacity", IsRequired = false, EmitDefaultValue = true)]
        public string Capacity { get; set; }

        [DataMember(Name = "power", IsRequired = false, EmitDefaultValue = true)]
        public double Power { get; set; }

        [DataMember(Name = "rpm", IsRequired = false, EmitDefaultValue = true)]
        public double RPM { get; set; }

        [DataMember(Name = "mp", IsRequired = false, EmitDefaultValue = true)]
        public double MP { get; set; }

        [DataMember(Name = "altitude", IsRequired = false, EmitDefaultValue = true)]
        public double Altitude { get; set; }

        [DataMember(Name = "taxiFuel", IsRequired = false, EmitDefaultValue = true)]
        public string TaxiFuel { get; set; }

        [DataMember(Name = "additionalFuel", IsRequired = false, EmitDefaultValue = true)]
        public string AdditionalFuel { get; set; }

        [DataMember(Name = "fuelUnit", IsRequired = false, EmitDefaultValue = true)]
        public string FuelUnit { get; set; }

        [DataMember(Name = "weightUnit", IsRequired = false, EmitDefaultValue = true)]
        public string WeightUnit { get; set; }

        [DataMember(Name = "speedUnit", IsRequired = false, EmitDefaultValue = true)]
        public string SpeedUnit { get; set; }

        [DataMember(Name = "verticalSpeed", IsRequired = false, EmitDefaultValue = true)]
        public string VerticalSpeed { get; set; }

       

        [DataMember(Name = "fuelBurnCruise", IsRequired = false, EmitDefaultValue = true)]
        public double FuelBurnCruise { get; set; }

        [DataMember(Name = "tasCruise", IsRequired = false, EmitDefaultValue = true)]
        public double TASCruise { get; set; }

        [DataMember(Name = "rateOfClimb", IsRequired = false, EmitDefaultValue = true)]
        public double RateOfClimb { get; set; }

        [DataMember(Name = "fuelBurnRateClimb", IsRequired = false, EmitDefaultValue = true)]
        public double FuelBurnRateClimb { get; set; }

        [DataMember(Name = "tasClimb", IsRequired = false, EmitDefaultValue = true)]
        public double TASClimb { get; set; }

        [DataMember(Name = "rateOfDescent", IsRequired = false, EmitDefaultValue = true)]
        public double RateOfDescent { get; set; }

        [DataMember(Name = "fuelBurnRateOfDescent", IsRequired = false, EmitDefaultValue = true)]
        public double FuelBurnRateOfDescent { get; set; }

        [DataMember(Name = "tasDescent", IsRequired = false, EmitDefaultValue = true)]
        public double TASDescent { get; set; }

        [DataMember(Name = "isAmericanAircraft", IsRequired = false, EmitDefaultValue = true)]
        public bool IsAmericanAircraft { get; set; }

        [DataMember(Name = "imageName", IsRequired = false, EmitDefaultValue = true)]
        public string ImageName { get; set; }

        [DataMember(Name = "imageOption", IsRequired = false, EmitDefaultValue = true)]
        public string ImageOption { get; set; }

        [DataMember(Name = "profileId", IsRequired = false, EmitDefaultValue = true)]
        public int ProfileId { get; set; }

        [DataMember(Name = "engineType", IsRequired = false, EmitDefaultValue = true)]
        public int EngineType { get; set; }

        [DataMember(Name = "areoUnitNumber", IsRequired = false, EmitDefaultValue = true)]
        public string AeroUnitNo { get; set; }

        [DataMember(Name = "aircraftType", IsRequired = false, EmitDefaultValue = true)]
        public string AircraftType { get; set; }

        [DataMember(Name = "aircraftSerialNumber", IsRequired = false, EmitDefaultValue = true)]
        public string AircraftSerialNumber { get; set; }

        [DataMember(Name = "aircraftYear")]
        public int? AircraftYear { get; set; }

        [DataMember(Name = "currentHobbsTime", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentHobbsTime { get; set; }

        [DataMember(Name = "hobbsTimeOffset", IsRequired = false, EmitDefaultValue = true)]
        public double HobbsTimeOffset { get; set; }

        [DataMember(Name = "currentTachTime", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentTachTime { get; set; }

        [DataMember(Name = "tachTimeOffset", IsRequired = false, EmitDefaultValue = true)]
        public double TachTimeOffset { get; set; }

        [DataMember(Name = "engineMFGType", IsRequired = false, EmitDefaultValue = true)]
        public int? EngineMFGType { get; set; }

        [DataMember(Name = "engineTBO", IsRequired = false, EmitDefaultValue = true)]
        public double EngineTBO { get; set; }

        [DataMember(Name = "engine1LastMOHDate", IsRequired = false, EmitDefaultValue = true)]
        public string Engine1LastMOHDate { get; set; }

        [DataMember(Name = "currentEngineTime1", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentEngineTime1 { get; set; }


        [DataMember(Name = "engineTimeOffset1", IsRequired = false, EmitDefaultValue = true)]
        public double EngineTimeOffset1 { get; set; }

        [DataMember(Name = "propMFG", IsRequired = false, EmitDefaultValue = true)]
        public int? PropMFG { get; set; }

        [DataMember(Name = "prop1TBO", IsRequired = false, EmitDefaultValue = true)]
        public int Prop1TBO { get; set; }

        [DataMember(Name = "prop1OHDueDate", IsRequired = false, EmitDefaultValue = true)]
        public string Prop1OHDueDate { get; set; }

        [DataMember(Name = "currentPropTime1", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentPropTime1 { get; set; }

        [DataMember(Name = "propTimeOffset1", IsRequired = false, EmitDefaultValue = true)]
        public double PropTimeOffset1 { get; set; }

        [DataMember(Name = "engine2LastMOHDate", IsRequired = false, EmitDefaultValue = true)]
        public string Engine2LastMOHDate { get; set; }

        [DataMember(Name = "currentEngineTime2", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentEngineTime2 { get; set; }

        [DataMember(Name = "engineTimeOffset2", IsRequired = false, EmitDefaultValue = true)]
        public double EngineTimeOffset2 { get; set; }

        [DataMember(Name = "prop2OHDueDate", IsRequired = false, EmitDefaultValue = true)]
        public string Prop2OHDueDate { get; set; }

        [DataMember(Name = "currentPropTime2", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentPropTime2 { get; set; }

        [DataMember(Name = "propTimeOffset2", IsRequired = false, EmitDefaultValue = true)]
        public double PropTimeOffset2 { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

        [DataMember(Name = "comm")]
        public bool Comm { get; set; }

        [DataMember(Name = "engineMonitor")]
        public bool EngineMonitor { get; set; }

        [DataMember(Name = "transponder")]
        public bool Transponder { get; set; }

        [DataMember(Name = "ispilot")]
        public bool IsPilot { get; set; }

        [DataMember(Name = "otherManufacturer")]
        public string OtherAircraftManufacturer { get; set; }

        [DataMember(Name = "otherModel")]
        public string OtheAircraftrModel { get; set; }

        [DataMember(Name = "componentManufacturerModelMapping")]
        public List<AircraftComponentMakeAndModelApp> AircraftComponentMakeAndModels { get; set; }

        [DataMember(Name = "ownerEmailId")]
        public string OwnerEmailId { get; set; }

        [DataMember(Name = "otherEngineManufactureType")]
        public string OtherEngineManufactureType { get; set; }

        [DataMember(Name = "otherPropManufactureType")]
        public string OtherPropManufactureType { get; set; }

        [DataMember(Name = "chargeBy")]
        public int? ChargeBy { get; set; }

        [DataMember(Name = "manufacturersMappedWithAircraft")]
        public string ManufacturersMappedWithAircraft { get; set; }

        [DataMember(Name = "engineSettings")]
        public string EngineSettings { get; set; }


        [DataMember(Name = "isUpdateAvailableForGeneral")]
        public bool IsUpdateAvailableForGeneral { get; set; }

        [DataMember(Name = "isUpdateAvailableForPerformance")]
        public bool IsUpdateAvailableForPerformance { get; set; }

        [DataMember(Name = "isUpdateAvailableForEngine")]
        public bool IsUpdateAvailableForEngine { get; set; }

        [DataMember(Name = "isRegisterWithoutUnit")]
        public bool IsRegisterWithoutUnit { get; set; }

    }
}
