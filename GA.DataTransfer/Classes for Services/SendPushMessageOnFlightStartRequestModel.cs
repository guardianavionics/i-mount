﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SendPushMessageOnFlightStartRequestModel
    {
        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "deviceTokenId")]
        public string DeviceTokenId { get; set; }

        [DataMember(Name = "startTime")]
        public string StartTime { get; set; }
    }
}
