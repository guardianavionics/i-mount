﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
  public  class GenerateIssueResponseModel : GeneralResponse
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

    }
}
