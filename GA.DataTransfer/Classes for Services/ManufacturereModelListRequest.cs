﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{

    [DataContract]
    public class ManufacturereModelListRequest
    {
        [DataMember(Name = "lastUpdatedDateTime", IsRequired = false, EmitDefaultValue = true)]
        public string LastUpdatedDateTime { get; set; }

    }
}
