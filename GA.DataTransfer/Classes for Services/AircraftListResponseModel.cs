﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class AircraftListResponseModel : GeneralResponse
    {
        [DataMember(Name = "aircraftList")]
        public List<AircraftProfileUpdateModel> AircraftList { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }
    }
}
