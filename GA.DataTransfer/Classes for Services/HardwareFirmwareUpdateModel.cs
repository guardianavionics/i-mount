﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
   public  class HardwareFirmwareUpdateModel
    {

        [DataMember(Name = "Version")]
        public string Version { get; set; }

        [DataMember(Name = "VersionSummary")]
        public string VersionSummary { get; set; }

        [DataMember(Name = "FileUrl")]
        public string FileUrl { get; set; }

        [DataMember(Name = "FileName")]
        public string FileName { get; set; }

        [DataMember(Name = "CreateDate")]
        public System.DateTime   CreateDate { get; set; }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "isLatest")]
        public bool? IsLatest { get; set; }

    }
}
