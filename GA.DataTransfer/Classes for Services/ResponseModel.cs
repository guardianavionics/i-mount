﻿using System.Runtime.Serialization;
namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
   public class ResponseModel
    {
        [DataMember(Name = "responseCode")]
        public string ResponseCode { get; set; }

        [DataMember(Name = "responseMessage")]
        public string ResponseMessage { get; set; }

        [DataMember(Name = "response")]
        public string Response { get; set; }
    }
}
