﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
   public  class ChartDetailsRequestModel
    {
        [DataMember(Name = "lastUpdateDateTime")]
        public System.DateTime? LastUpdateDateTime { get; set; }

        [DataMember(Name = "lastChangedDataTimeForPlate")]
        public System.DateTime? LastChangedDataTimeForPlate { get; set; }

        [DataMember(Name = "stateIds")]
        public int[] StateIds { get; set; }
    }
}
