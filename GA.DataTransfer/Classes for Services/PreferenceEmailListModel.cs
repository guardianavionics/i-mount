﻿using System;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class PreferenceEmailListModel
    {
        [DataMember(Name = "emailId")]
        public string Email { get; set; }

        [DataMember(Name = "isenabled")]
        public bool IsEnabled { get; set; }
    }
}
