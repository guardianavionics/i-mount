﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class NotificationModel
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        //[DataMember(Name = "requestFrom")]
        //public int? RequestFrom { get; set; }

        //[DataMember(Name = "requestTo")]
        //public int? RequestTo { get; set; }

        [DataMember(Name = "type")]
        public int Type { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "createdDate")]
        public string CreatedDate { get; set; }

        [DataMember(Name = "aircraftId")]
        public int? AircraftId { get; set; }

      
    }
}
