﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class AeroUnitNumberRequestModel
    {
        [DataMember(Name = "FMS")]
        public string FMS { get; set; }
    }
}
