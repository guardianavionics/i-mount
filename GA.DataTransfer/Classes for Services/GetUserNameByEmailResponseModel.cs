﻿using  System.Runtime.Serialization;

namespace GA.DataTransfer
{
   public class GetUserNameByEmailResponseModel : GeneralResponse
    {
       [DataMember(Name = "FirstName")]
       public string FirstName { get; set; }

       [DataMember(Name = "LastName")]
       public string LastName { get; set; }
    }
}
