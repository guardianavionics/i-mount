﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
   public class ErrorLogModel
    {
        public int Id { get; set; }
        public string EncryptRequest { get; set; }
        public string JsonRequest { get; set; }
        public string Response { get; set; }
        public int RequestId { get; set; }
        public string Module { get; set; }
        public string MethodName { get; set; }
        public string ServerName { get; set; }
        public DateTime Date { get; set; }
        public bool IsResolved { get; set; }
        public string Remark { get; set; }
        public string EncryptedResponse { get; set; }
        public string JsonResponse { get; set; }

    }
}
