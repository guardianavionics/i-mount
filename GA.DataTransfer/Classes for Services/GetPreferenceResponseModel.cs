﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataTransfer.Classes_for_Services;

namespace GA.DataTransfer
{
    [DataContract]
    public class GetPreferenceResponseModel
    {
        [DataMember(Name = "distanceUnit")]
        public string DistanceUnit { get; set; }

        [DataMember(Name = "speedUnit")]
        public string SpeedUnit { get; set; }

        [DataMember(Name = "verticalSpeedUnit")]
        public string VerticalSpeedUnit { get; set; }

        [DataMember(Name = "fuelUnit")]
        public string FuelUnit { get; set; }

        [DataMember(Name = "weightUnit")]
        public string WeightUnit { get; set; }

        [DataMember(Name = "defaultAirportIdentifier")]
        public string DefaultAirportIdentifier { get; set; }

        [DataMember(Name = "responseCode")]
        public string ResponseCode { get; set; }

        [DataMember(Name = "responseMessage")]
        public string ResponseMessage { get; set; }

        [DataMember(Name = "isDropboxSync")]
        public bool IsDropboxSync { get; set; }

        //[DataMember(Name = "token")]
        //public string Token { get; set; }

        //[DataMember(Name = "tokenSecret")]
        //public string TokenSecret { get; set; }

        [DataMember(Name = "emailList")]
        public List<PreferenceEmailListModel> EmailList { get; set; }


        [DataMember(Name = "showCabin")]
        public bool ShowCabin { get; set; }

        [DataMember(Name = "showPitch")]
        public bool ShowPitch { get; set; }

        [DataMember(Name = "audioRecording")]
        public bool AudioRecording { get; set; }

        [DataMember(Name = "switchToEnginForAlarm")]
        public bool SwitchToEnginForAlarm { get; set; }

        [DataMember(Name = "displayAltitude")]
        public int DisplayAltitude { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        [DataMember(Name = "isUpdateAvailableForPreference")]
        public bool IsUpdateAvailableForPreference { get; set; }

        [DataMember(Name = "isUpdateAvailableForEmailList")]
        public bool IsUpdateAvailableForEmailList { get; set; }

        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

    }
}
