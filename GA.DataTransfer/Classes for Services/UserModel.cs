﻿using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace GA.DataTransfer
{
    [DataContract]
    public class UserModel
    {
        #region properties

        [Required]
        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [Required]
        [DataMember(Name = "password")]
        public string Password { get; set; }

        [Required]
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [Required]
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [Required]
        [DataMember(Name = "streetAddress")]
        public string StreetAddress { get; set; }

        [Required]
        [DataMember(Name = "city")]
        public string City { get; set; }

        [Required]
        [DataMember(Name = "state")]
        public string State { get; set; }

        [Required]
        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        
        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [Required]
        [DataMember(Name = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [Required]
        [DataMember(Name = "securityQuestion")]
        public int SecurityQuestionId { get; set; }

        [Required]
        [DataMember(Name = "securityAnswer")]
        public string SecurityAnswer { get; set; }

        
        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }


        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name = "isRegisterbyAdmin")]
        public bool IsRegisterByAdmin { get; set; }

        [DataMember(Name = "manufacturerId")]
        public int? ManufacturerId { get; set; }

        [DataMember(Name = "countryCode")]
        public string CountryCode { get; set; }
        //[DataMember(Name = "aircraftTailNo")]
        //public string AircraftTailNo { get; set; }


        //[DataMember(Name = "engineSetting")]
        //public string EngineSetting { get; set; }

        //[DataMember(Name = "uniqueId")]
        //public long UniqueId { get; set; }

        #endregion properties
    }
}
