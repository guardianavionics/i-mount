﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
   public class PlateList : GeneralResponse
    {
        [DataMember(Name = "StateList")]
        public List<PlateStates> PlateStateList { get; set; }

        [DataMember(Name = "totalPlateSizeInMB")]
        public decimal TotalPlateSizeInMB { get; set; }
    }
}
