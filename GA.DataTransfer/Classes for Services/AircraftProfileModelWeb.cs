﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using GA.DataLayer;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;

namespace GA.DataTransfer
{
    public class AircraftProfileModelWeb
    {
        [Required]
        [IsUnique]
        public string Registration { get; set; }
        public int? Make { get; set; }
        public int? Model { get; set; }
        public string Color { get; set; }
        public string HomeBase { get; set; }

        [RegularExpression(@"^\d*\.?\d*$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string Capacity { get; set; }

        public double Power { get; set; }
        public double RPM { get; set; }
        public double MP { get; set; }
        public double Altitude { get; set; }

        [RegularExpression(@"^\d*\.?\d*$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [Display(Name = "Taxi Fuel")]
        public string TaxiFuel { get; set; }

        [Display(Name = "Additional Fuel")]
        [RegularExpression(@"^\d*\.?\d*$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string AdditionalFuel { get; set; }

        [Display(Name = "Fuel Unit")]
        public string FuelUnit { get; set; }

        [Display(Name = "Weight Unit")]
        public string WeightUnit { get; set; }

        [Display(Name = "Speed Unit")]
        public string SpeedUnit { get; set; }

        [Display(Name = "Vertical Speed")]
        public string VerticalSpeed { get; set; }

        [Display(Name = "Fuel Burn Rate Cruise")]
        public double FuelBurnCruise { get; set; }

        [Display(Name = "TAS Cruise")]
        public double TASCruise { get; set; }

        [Display(Name = "Rate Of Climb")]
        public double RateOfClimb { get; set; }

        [Display(Name = "Fuel Burn Rate Climb")]
        public double FuelBurnRateClimb { get; set; }

        [Display(Name = "TAS Climb")]
        public double TASClimb { get; set; }

        [Display(Name = "Rate Of Descent")]
        public double RateOfDescent { get; set; }

        [Display(Name = "Fuel Burn Rate Of Descent")]
        public double FuelBurnRateOfDescent { get; set; }

        [Display(Name = "TAS Descent")]
        public double TASDescent { get; set; }

        [Display(Name = "Is American Aircraft")]
        public bool IsAmericanAircraft { get; set; }

        [Display(Name = "FMS Serial number")]
        [Required( ErrorMessage = "FMS serial number is required")]
        public string AreoUnitNo { get; set; }

        [Display(Name = "Engine Type")]
        public bool IsSingleEngine { get; set; }

        //[Display(Name = "Aircraft Type")]
        //public string AircraftType { get; set; }

     

        [Display(Name = "Aircraft Serial Number")]
        public string AircraftSerialNumber { get; set; }

        [Display(Name = "Aircraft Year")]
        public int? AircraftYear { get; set; }

        [Display(Name = "Current Hobbs Time")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CurrentHobbsTime { get; set; }

        [Display(Name = "Hobbs Time Offset")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string HobbsTimeOffset { get; set; }

        [Display(Name = "Total Hobbs Time")]
        public string TotalHobbsTime { get; set; }

        [Display(Name = "Current Tach Time")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CurrentTachTime { get; set; }

        [Display(Name = "Tach Time Offset")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string TachTimeOffset { get; set; }

        [Display(Name = "Total Tach Time")]
        public string TotalTechTime { get; set; }

        [Display(Name = "Engine MFG Type")]
        public int? EngineMFGType { get; set; }

        [Display(Name = "Engine TBO")]
        public double EngineTBO { get; set; }

        [Display(Name = "Engine # 1 Last MOH Date")]
        public string Engine1LastMOHDate { get; set; }

        [Display(Name = "Current Engine Time 1")]
        public string CurrentEngineTime1 { get; set; }

        [Display(Name = "Engine Time Offset 1")]
        public string EngineTimeOffset1 { get; set; }

        [Display(Name = "Total Engine Time 1")]
        public string TotalEngineTime1 { get; set; }

        [Display(Name = "Prop MFG")]
        public int? PropMFG { get; set; }

        [Display(Name = "Prop # 1 TBO")]
        public int Prop1TBO { get; set; }

        [Display(Name = "prop # 1 OH DueDate")]
        public string Prop1OHDueDate { get; set; }

        [Display(Name = "Current Prop Time 1")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CurrentPropTime1 { get; set; }

        [Display(Name = "Prop Time Offset 1")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string PropTimeOffset1 { get; set; }

        [Display(Name = "Total Prop Time 1")]
        public string TotalPropTime1 { get; set; }

        [Display(Name = "Engine # 2 Last MOH Date")]
        public string Engine2LastMOHDate { get; set; }

        [Display(Name = "Current Engine Time 2")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CurrentEngineTime2 { get; set; }

        [Display(Name = "Engine Time Offset 2")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string EngineTimeOffset2 { get; set; }

        [Display(Name = "Total Engine Time 2")]
        public string TotalEngineTime2 { get; set; }

        [Display(Name = "Prop # 2 OH DueDate")]
        public string Prop2OHDueDate { get; set; }

        [Display(Name = "Current Prop Time 2")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CurrentPropTime2 { get; set; }

        [Display(Name = "Prop Time Offset 2")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceName = "InvalidInput", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string PropTimeOffset2 { get; set; }

        [Display(Name = "Total Prop Time 2")]
        public string TotalPropTime2 { get; set; }

        public int AircraftId { get; set; }

        public int SpeedUnitId { get; set; }

        public int VerticalSpeedUnitId { get; set; }

        public int WeightUnitId { get; set; }

        public int FuelUnitId { get; set; }

        public List<ListItems> SpeedList { get; set; }

        public List<ListItems> VerticalSpeedList { get; set; }

        public List<ListItems> FuelList { get; set; }

        public List<ListItems> WeightList { get; set; }

        public HttpPostedFileBase AircraftImage { get; set; }

        public string AircraftImagePath { get; set; }

        public List<ListItems> Manufacturer { get; set; }

        public List<ListItems> AircraftModel { get; set; }

        [Display(Name = "Number Of Engines")]
        public int NumberOfEngine { get; set; }


        [Display(Name = "Comm")]
        public bool Comm { get; set; }

        [Display(Name = "Engine Monitor")]
        public bool EngineMonitor { get; set; }

        [Display(Name = "Transponder")]
        public bool Transponder { get; set; }

        [Display(Name = "OtherManufacturer")]
        public string OtherAircraftManufacturer { get; set; }

        [Display(Name = "OtherModel")]
        public string OtheAircraftrModel { get; set; }

        #region Components

        [Display(Name = "CommId")]
        public int CommId { get; set; }

        [Display(Name = "CommManufacturerId")]
        public int? CommManufacturerId { get; set; }

        [Display(Name = "CommModelId")]
        public int? CommModelId { get; set; }

        [Display(Name = "TransponderId")]
        public int TransponderId { get; set; }

        [Display(Name = "TransponderManufacturerId")]
        public int? TransponderManufacturerId { get; set; }

        [Display(Name = "TransponderModelId")]
        public int? TransponderModelId { get; set; }

        [Display(Name = "EngineMonitorId")]
        public int EngineMonitorId { get; set; }

        [Display(Name = "EngineMonitorManufacturerId")]
        public int? EngineMonitorManufacturerId { get; set; }

        [Display(Name = "EngineMonitorModelId")]
        public int? EngineMonitorModelId { get; set; }

        public List<ListItems> CommManufacturerList { get; set; }

        public List<ListItems> TransponderManufacturerList { get; set; }

        public List<ListItems> EngineMonitorManufacturerList { get; set; }

        public List<ListItems> CommModelList { get; set; }

        public List<ListItems> TransponderModelList { get; set; }

        public List<ListItems> EngineMonitorModelList { get; set; }

        public string commFileName { get; set; }

        public string TransponderFileName { get; set; }

        public string EngineMonitorFileNAme { get; set; }

        public List<ListItems> EngineMFGTypeList { get; set; }

        public List<ListItems> PropellerManufacturrList { get; set; }

        public List<AircraftComponentMakeAndModelWeb> AircraftComponentMakeAndModels { get; set; }

        public List<AircraftComponentManufacturerModel> AircraftComponentManufacturerList { get; set; }

        public List<AircraftComponentModel> ComponentModelList { get; set; }

        public List<ManufacturerAuthForDataLog> manufacturerAuthForDataLogList { get; set; }

        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                                 ErrorMessageResourceName = "InvalidEmailId", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Email Id")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string OwnerEmailId { get; set; }

        public bool IsOwner { get; set; }

        public string OtherEngineManufactureType { get; set; }

        public string OtherPropManufactureType { get; set; }

        public bool Part { get; set; }

        public int ChargeBy { get; set; }

        public List<AircraftSalesDetailWeb> aircraftSalesDetailList { get; set; }

        public IPassengerMediaDetails iPassengerMediaDetails { get; set; }

        public string UpdateModuleName { get; set; }

        //public List<MaintenanceUserModel> maintenanceUserList { get; set; }

        //public List<IssueModel> issueList { get; set; }

        public MaintenanceModelWeb Maintenance { get; set; }

        #endregion Components

    }

    public class AircraftSalesDetailWeb
    {
        public int Id { get; set; }
        public string SalesDetailtype { get; set; }
        public decimal Price { get; set; }
        public string GLAccountNumber { get; set; }
        public string TId { get; set; }
    }


    public class AircraftComponentMakeAndModelWeb
    {
        public int Id { get; set; }
        public int AircraftId { get; set; }
        public int? AircraftComponentId { get; set; }
        public string AircraftComponentName { get; set; }
        public int? ComponentManufacturerId { get; set; }
        public string ComponentManufacturerName { get; set; }
        public int? ComponentModelId { get; set; }
        public string ComponentModelName { get; set; }
        public string ComponentModelFileName { get; set; }
        public Boolean IsShowInFlightBag { get; set; }
        public string ModelFreetext { get; set; }
        public Boolean IsShownForManufacturerUser { get; set; }

        public Boolean IsVisibleOnMFDScreen { get; set; }
    }

    public class AircraftComponentMakeAndModelApp
    {
        public int? aircraftComponentId { get; set; }
        public int? componentManufacturerId { get; set; }
        public int? componentModelId { get; set; }
        //public string ComponentModelFileName { get; set; }
        public Boolean isShowInFlightBag { get; set; }
        public string modelSerialNo { get; set; }

        public Boolean isVisibleOnMFDScreen { get; set; }
    }

    public class AircraftComponentManufacturerModel
    {
        public int AircraftComponentManufacturerId { get; set; }
        public string AircraftComponentManufacturerName { get; set; }
        public int AircraftComponentId { get; set; }
        public string AircraftComponentName { get; set; }
    }

    public class ManufacturerAuthForDataLog
    {
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public bool ISAuthenticateForDatalog { get; set; }
    }
}
