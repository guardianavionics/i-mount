﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class MedicalCertificateModel
    {
        [DataMember(Name = "referenceNumber")]
        public string ReferenceNumber { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "validUntil")]
        public string ValidUntil { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

        [DataMember(Name = "className")]
        public string ClassName { get; set; }

        [DataMember(Name = "classId")]
        public int? ClassId { get; set; }
    }
}