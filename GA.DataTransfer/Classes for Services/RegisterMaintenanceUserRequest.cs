﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class RegisterMaintenanceUserRequest
    {
        //[DataMember(Name = "firstName")]
        //public string FirstName { get; set; }

        //[DataMember(Name = "lastName")]
        //public string LastName { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        //[DataMember(Name = "mobileNo")]
        //public string MobileNo { get; set; }

        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }
    }
}
