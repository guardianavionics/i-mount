﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
       [DataContract]
   public class UpdateNotificationStatusResponse : GeneralResponse
    {
           [DataMember(Name = "lastUpdateDate")]
           public string LastUpdateDate { get; set; } 
    }
}
