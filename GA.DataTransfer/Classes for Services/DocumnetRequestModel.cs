﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class DocumnetRequestModel
    {
        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }
    }
}
