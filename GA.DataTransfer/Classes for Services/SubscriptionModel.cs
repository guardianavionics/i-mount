﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class SubscriptionModel
    {
        [DataMember(Name ="id")]
        public int Id { get; set; }
        [DataMember(Name = "planName")]
        public string PlanName { get; set; }
        [DataMember(Name = "detail")]
        public string Detail { get; set; }
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        [DataMember(Name = "productList")]
        public List<SubscriptionDetailsModel> ProductList { get; set; }
        [DataMember(Name = "featureIdList")]
        public List<int> FeatureIdList { get; set; }

    }
}
