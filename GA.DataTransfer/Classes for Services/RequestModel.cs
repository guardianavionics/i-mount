﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class RequestModel
    {

        [DataMember(Name = "requestId")]
        public int requestId { get; set; }

        [DataMember(Name = "request")]
        public string request { get; set; }
    }
}
