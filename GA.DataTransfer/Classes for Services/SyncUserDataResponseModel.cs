﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SyncUserDataResponseModel
    {
        [DataMember(Name = "aircraftProfileResponse")]
        public AircraftProfileResponseModel AircraftProfileResponse { get; set; }

        [DataMember(Name = "userProfileResponse")]
        public ProfileUpdateResponseModel UserProfileResponse { get; set; }


        [DataMember(Name = "logBookResponse")]
        public LogBookResponseModel LogBookResponse { get; set; }

         [DataMember(Name = "preferenceResponse")]
        public GetPreferenceResponseModel PreferenceResponse { get; set; }

         [DataMember(Name = "notificationResponse")]
         public NotificationResponse NotificationResponse { get; set; }


         [DataMember(Name = "maintenanceUserResponse")]
         public MaintenanceUserResponse MaintenanceUserResponse { get; set; }

         [DataMember(Name = "squawkResponse")]
         public SquawkListResponse SquawkResponse { get; set; }

        [DataMember(Name = "dropDownListResponse")]
        public ManufacturereModelListResponse DropDownListResponse { get; set; }


    }
}
