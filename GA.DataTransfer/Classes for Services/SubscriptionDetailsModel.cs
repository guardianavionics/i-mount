﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class SubscriptionDetailsModel
    {
        [DataMember(Name ="id")]
        public int Id { get; set; }
       
        [DataMember(Name = "amount")]
        public decimal Amount { get; set; }
        [DataMember(Name = "subscriptionId")]
        public int SubscriptionId { get; set; }
        [DataMember(Name = "trialFrequency")]
        public string TrialFrequency { get; set; }
        [DataMember(Name = "regularFrequency")]
        public string RegularFrequency { get; set; }
        [DataMember(Name = "inAppProductId")]
        public string InAppProductId { get; set; }
        [DataMember(Name = "paypalBId")]
        public string PaypalBId { get; set; }


    }
}
