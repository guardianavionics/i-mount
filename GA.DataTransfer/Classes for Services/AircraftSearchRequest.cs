﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    public class AircraftSearchRequest
    {
        //[DataMember(Name = "aircraftId")]
        //public int AircraftId { get; set; }

        //[DataMember(Name = "profileId")]
        //public int ProfileId { get; set; }

        //[DataMember(Name = "uniqueId")]
        //public long UniqueId { get; set; }

        [DataMember(Name = "registration")]
        public string Registration { get; set; }

    }
}
