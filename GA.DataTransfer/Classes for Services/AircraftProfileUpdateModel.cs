﻿using GA.DataTransfer.Classes_for_Services;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class AircraftProfileUpdateModel
    {
        [DataMember(Name = "deletedAircraft")]
        public List<int> DAircraftList { get; set; }

        [DataMember(Name = "aircraftList")]
        public List<AircraftModel> AircraftList { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

    }
}