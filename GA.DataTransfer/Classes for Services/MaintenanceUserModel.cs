﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class MaintenanceUserModel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "isRegister")]
        public bool IsRegister { get; set; }

        [DataMember(Name = "linkedAircrafts")]
        public int[] LinkedAircrafts { get; set; }

    }
}
