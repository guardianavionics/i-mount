﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;
namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class PlateCitys
    {
        [DataMember(Name = "cityName")]
        public string CityName { get; set; }

        [DataMember(Name = "volume")]
        public string Volume { get; set; }
        
        [DataMember(Name = "city_Name_Id")]
        public int City_Name_Id { get; set; }

        [DataMember(Name = "state_Code_Id")]
        public Nullable<int> State_Code_Id { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "stateId")]
        public Nullable<int> StateId { get; set; }

        [DataMember(Name = "airportList")]
        public List<PlateAirPorts> AirportList { get; set; }
    }
}
