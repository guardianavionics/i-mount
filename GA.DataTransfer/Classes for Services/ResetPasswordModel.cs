﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class ResetPasswordModel
    {
        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }
    }
}
