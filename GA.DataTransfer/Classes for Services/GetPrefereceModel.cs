﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class GetPrefereceModel
    {
        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "lastUpdatedDate")]
        public string LastUpdatedDate { get; set; }
    }
}
