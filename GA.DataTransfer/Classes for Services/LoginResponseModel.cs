﻿using GA.Common;
using GA.DataTransfer.Classes_for_Services;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class LoginResponseModel : GeneralResponse
    {
        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        [DataMember(Name = "medicalCertificateList")]
        public List<MedicalCertificateModel> MedicalCertificateList { get; set; }

        [DataMember(Name = "licensesList")]
        public List<LicensesModel> LicensesList { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "streetAddress")]
        public string StreetAddress { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "countryCode")]
        public string CountryCode { get; set; }

        [DataMember(Name = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [DataMember(Name = "imageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

        // aircraft list

        //[DataMember(Name = "aircraftList")]
        //public List<AircraftModel> AircraftList { get; set; }

        //// preference

        //[DataMember(Name = "distanceUnit")]
        //public string DistanceUnit { get; set; }

        //[DataMember(Name = "speedUnit")]
        //public string SpeedUnit { get; set; }

        //[DataMember(Name = "verticalSpeedUnit")]
        //public string VerticalSpeedUnit { get; set; }

        //[DataMember(Name = "fuelUnit")]
        //public string FuelUnit { get; set; }

        //[DataMember(Name = "weightUnit")]
        //public string WeightUnit { get; set; }

        //[DataMember(Name = "defaultAirportIdentifier")]
        //public string DefaultAirportIdentifier { get; set; }


        [DataMember(Name = "deletedMedicalCertificates")]
        public int[] DeletedMedicalCertificate { get; set; }

        [DataMember(Name = "deletedLicences")]
        public int[] DeletedLicences { get; set; }

        [DataMember(Name = "deletedRatings")]
        public int[] DeletedRatings { get; set; }

        //[DataMember(Name = "pilotLogList")]
        //public List<LogBookModel> pilotLogList { get; set; }

        //[DataMember(Name = "dropDownListData")]
        //public ManufacturereModelListResponse DropDownListData { get; set; }

        [DataMember(Name = "socialLoginAccountType")] // 1 for Facebook, 2 for Gmail
        public int SocialLoginAccountType { get; set; }

        [DataMember(Name = "socialLoginAccountId")]
        public string SocialLoginAccountId { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name = "twoStepAuthentication")]
        public bool? TwoStepAuthentication { get; set; }

        //[DataMember(Name = "logBookResponse")]
        //public LogBookResponseModel LogBookResponse { get; set; }

        public LoginResponseModel Create(GeneralResponse objGeneralResponse, LoginResponseModel objLoginResponseModel = null)
        {
            if (objLoginResponseModel != null)
            {
                return new LoginResponseModel
                {
                    ProfileId = objLoginResponseModel.ProfileId,
                    LastUpdateDate = objLoginResponseModel.LastUpdateDate,
                    MedicalCertificateList = objLoginResponseModel.MedicalCertificateList,
                    LicensesList = objLoginResponseModel.LicensesList,

                    ResponseCode = objLoginResponseModel.ResponseCode,
                    ResponseMessage = objLoginResponseModel.ResponseMessage
                };
            }
            else
            {
                return new LoginResponseModel
                {
                    ResponseCode = objGeneralResponse.ResponseCode,
                    ResponseMessage = objGeneralResponse.ResponseMessage
                };
            }
        }

        public GeneralResponse CreateGenerlResponse(Enumerations.LoginReturnCodes returnCode)
        {
            return new GeneralResponse
            {
                ResponseCode = ((int)returnCode).ToString(),
                ResponseMessage = returnCode.GetStringValue()
            };
        }
    }
}
