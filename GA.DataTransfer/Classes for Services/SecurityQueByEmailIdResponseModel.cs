﻿using System.Runtime.Serialization;
 

namespace GA.DataTransfer
{
    public class SecurityQueByEmailIdResponseModel :GeneralResponse
    {
        [DataMember(Name = "securityQuestion")]
        public string SecurityQuestion { get; set; }

        [DataMember(Name = "securityAnswer")]
        public string SecurityAnswer { get; set; }
    }
}
