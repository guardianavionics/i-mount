﻿namespace GA.DataTransfer.Classes_for_Services
{
    public class DropboxParameterisedThread
    {
        public int Id { get; set; }

        public int ProfileId { get; set; }

        public System.Security.Principal.WindowsIdentity identity { get; set; }

    }
}
