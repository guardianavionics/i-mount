﻿using GA.DataTransfer.Classes_for_Web;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class IPassengerResponseModel :GeneralResponse
    {
        [DataMember(Name = "videoList")]
        public List<MediaDetails> VideoList { get; set; }

        [DataMember(Name = "imageList")]
        public List<MediaDetails> ImageList { get; set; }

        [DataMember(Name = "pdfList")]
        public List<MediaDetails> PDFList { get; set; }

        [DataMember(Name = "warningList")]
        public List<MediaDetails> WarningList { get; set; }

        [DataMember(Name = "instructionList")]
        public List<MediaDetails> InstructionList { get; set; }

      
    }
}
