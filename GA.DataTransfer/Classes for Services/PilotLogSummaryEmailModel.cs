﻿namespace GA.DataTransfer.Classes_for_Services
{
    public class PilotLogSummaryEmailModel
    {
        public string PilotName { get; set; }

        public string PilotImage { get; set; }
        public string AircraftImage { get; set; }

        public string Nnumber { get; set; }
        public string FlightCompletedOn { get; set; }
        public string LengthOfFlight { get; set; }
        
        public string StartTachTime { get; set; }
        public string EndTachTime { get; set; }

        public string StartHobbsTime { get; set; }
        public string EndHobbsTime { get; set; }

        public string EstimatedFuelUsed { get; set; }

        public string PilotEmail { get; set; }

    }
}
