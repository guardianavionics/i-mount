﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace GA.DataTransfer
{
    [DataContract]
    public class ManufacturereModelListResponse : GeneralResponse
    {
        [DataMember(Name = "aircraftManufacturerAndModelList")]
        public List<AircraftManufacturerAndModel> AircraftManufacturerAndModelList { get; set; }

        [DataMember(Name = "propeller")]
        public List<ListItems> Propeller { get; set; }

        [DataMember(Name = "ratingType")]
        public List<ListItems> RatingType { get; set; }

        [DataMember(Name = "medicalClass")]
        public List<ListItems> MedicalClass { get; set; }

        [DataMember(Name = "engine")]
        public List<ListItems> Engine { get; set; }

        [DataMember(Name = "componentManufacturerModelList")]
        public List<ComponentManufacturerModel> ComponentManufacturerModelList { get; set; }

        [DataMember(Name = "deletedComponentModelList")]
        public List<ListItems> DeletedComponentModelList { get; set; }

    }

   
    public class AircraftManufacturerAndModel
    {
        public ListItems aircraftManufacturerNameAndId { get; set; }

        public List<ListItems> aircraftModelList { get; set; }
    }

    public class ManufacturerAndModel
    {
        public ListItems manufacturerNameAndId { get; set; }

        public bool isUserAvailable { get; set; }

        public List<ComponentModel> componentModelList { get; set; }
    }

    public class ComponentManufacturerModel
    {
        public ListItems componentNameAndId { get; set; }

        public List<ManufacturerAndModel> manufacturerAndModelList { get; set; }
    }

      
    public class ComponentModel
    {
        public string name { get; set; }

        public int id { get; set; }

        public string fileName { get; set; }

        public string fileUrl { get; set; }
    }

}
