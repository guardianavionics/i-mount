﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class AirportFreqAndNavaidResponseModel
    {
        [DataMember(Name = "StateList")]
        public List<AirportStates> StateList { get; set; }
    }
}
