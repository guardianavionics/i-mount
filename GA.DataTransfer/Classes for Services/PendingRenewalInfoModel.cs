﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class PendingRenewalInfoModel
    {
        [DataMember(Name ="id")]
        public int Id { get; set; }
        [DataMember(Name = "expiration_intent")]
        public int Expiration_Intent { get; set; }
        [DataMember(Name = "original_transaction_id")]
        public string Original_Transaction_Id { get; set; }
        [DataMember(Name ="is_in_billing_retry_period")]
        public int Is_In_Billing_Retry_Period { get; set; }
        [DataMember(Name = "auto_renew_status")]
        public int Auto_Renew_Status { get; set; }
        [DataMember(Name = "auto_renew_product_id")]
        public string Auto_Renew_Product_Id { get; set; }
        [DataMember(Name = "product_id")]
        public string Product_Id { get; set; }

    }
}
