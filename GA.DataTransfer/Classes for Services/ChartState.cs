﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class ChartState
    {
        [DataMember(Name = "stateName")]
        public string StateName { get; set; }

        [DataMember(Name = "stateId")]
        public int StateId { get; set; }

       [DataMember(Name = "chartDetailsList")]
       public List<ChartDetailModel> ChartDetailsList { get; set; }

       [DataMember(Name = "plateMaxExpiryDate")]
       public string PlateMaxExpiryDate { get; set; }

        [DataMember(Name = "plateTotalFileSizeInMB")]
        public decimal? PlateTotalFileSizeInMB { get; set; }

        [DataMember(Name = "plateFilePath")]
        public string PlateFilePath { get; set; }

        [DataMember(Name = "shortName")]
        public string ShortName { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "countryId")]
        public int CountryId { get; set; }
    }
}
