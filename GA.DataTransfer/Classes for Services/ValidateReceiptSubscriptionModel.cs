﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using GA.DataLayer;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class ValidateReceiptSubscriptionModel
    {
        [DataMember(Name = "subscriptionId")]
        public int SubscriptionId { get; set; }

        [DataMember(Name = "productId")]
        public string ProductId { get; set; }

        [DataMember(Name = "planName")]
        public string PlanName { get; set; }

        [DataMember(Name = "detail")]
        public string Detail { get; set; }

        [DataMember(Name = "expirationDate")]
        public string ExpirationDate { get; set; }

        [DataMember(Name = "featureList")]
        public List<FeatureMaster> FeatureList { get; set; }

        [DataMember(Name = "autoRenewStatus")]
        public bool AutoRenewStatus { get; set; }

        [DataMember(Name = "purchaseDate")]
        public string PurchaseDate { get; set; }

        [DataMember(Name = "inAppStatusCode")]
        public string InAppStatusCode { get; set; }

        [DataMember(Name = "statusCode")]
        public string StatusCode { get; set; }

        [DataMember(Name = "statusMessage")]
        public string StatusMessage { get; set; }

        [DataMember(Name ="source")]
        public int Source { get; set; }

        [DataMember(Name = "isTrialPeriod")]
        public bool IsTrialPeriod { get; set; }
    }
}
