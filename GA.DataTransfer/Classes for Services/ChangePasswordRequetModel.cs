﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
    public class ChangePasswordRequetModel
    {
         [DataMember(Name = "emailId")]
         public string EmailId { get; set; }

         [DataMember(Name = "password")]
         public string Password { get; set; }

         [DataMember(Name = "newPassword")]
         public string NewPassword { get; set; }

         [DataMember(Name = "isQueAndAnsVerified")]
         public bool isQueAndAnsVerified { get; set; }
    }
}
