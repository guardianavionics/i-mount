﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class LogBookRequestModel
    {
        [DataMember(Name = "logBookList", IsRequired = false, EmitDefaultValue = true)]
        public List<LogBookModel> LogBookList { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

     

    }
}
