﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class ChartDetailsResponseModel : GeneralResponse
    {
        [DataMember(Name = "chartStateList")]
        public List<ChartState> ChartStateList { get; set; }

        [DataMember(Name = "airportNavAndFreqUpdateDate")]
        public AirportNavAndFreqStatusApp AirportNavAndFreqUpdateDate { get; set; }

        [DataMember(Name = "plateList")]
        public PlateList PlateList { get; set; }
    }
}
