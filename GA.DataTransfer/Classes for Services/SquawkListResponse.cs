﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SquawkListResponse : GeneralResponse
    {
        [DataMember(Name = "squawkList")]
        public List<IssueModel> SquawkList { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }
    }
}
