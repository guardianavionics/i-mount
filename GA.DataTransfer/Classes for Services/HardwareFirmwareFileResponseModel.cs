﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using GA.Common;


namespace GA.DataTransfer
{
      [DataContract]
   public class HardwareFirmwareFileResponseModel : GeneralResponse
    {

          [DataMember(Name = "HardwareFirmwareUpdateModelList")]
          public List<HardwareFirmwareUpdateModel> HardwareFirmwareUpdateModelList { get; set; }
    }
}
