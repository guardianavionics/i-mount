﻿using System.IO;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
    public class TestFileUpload
    {

        [DataMember(Name = "file")]
        public Stream file { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
        
    }


}
