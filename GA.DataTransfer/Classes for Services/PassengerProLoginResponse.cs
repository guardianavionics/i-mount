﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class PassengerProLoginResponse : GeneralResponse
    {
        [DataMember(Name = "aircraftAndIpassengerList")]
        public List<AircraftAndIpassengerDetailModel> AircraftAndIpassengerList { get; set; }

        [DataMember(Name = "userName")]
        public string UserName{ get; set; }
    }
}
