﻿using GA.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class LogBookResponseModel:GeneralResponse
    {
        [DataMember(Name = "logBookList")]
        public List<LogBookModelWithAircraftDetail> LogBookList { get; set; }

        [DataMember(Name = "lastUpdateDate")]
        public string LastUpdateDate { get; set; }

        //[DataMember(Name = "clearAllData")]
        //public bool ClearAllData { get; set; }


        //[DataMember(Name = "isUpdateAvailableForLogbook")]
        //public bool IsUpdateAvailableForLogbook { get; set; }


        public LogBookResponseModel Exception()
        {
            return new LogBookResponseModel
            {
                ResponseCode = ((int)Enumerations.Logbook.Exception).ToString(),
                ResponseMessage = Enumerations.Logbook.Exception.GetStringValue(),
            };
        }

    }
}
