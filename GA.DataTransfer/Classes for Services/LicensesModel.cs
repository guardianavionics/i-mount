﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class LicensesModel
    {
        [DataMember(Name = "ratingsList")]
        public List<RatingModel> Ratingslist { get; set; }

        [DataMember(Name = "referenceNumber")]
        public string ReferenceNumber { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "validUntil")]
        public string ValidUntil { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

    }
}
