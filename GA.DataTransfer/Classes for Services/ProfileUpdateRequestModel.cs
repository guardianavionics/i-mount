﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class ProfileUpdateRequestModel
    {
        [DataMember(Name = "profileId")]
        public string ProfileId { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }

        [DataMember(Name = "streetAddress")]
        public string StreetAddress { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public int? ZipCode { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "medicalCertificateList")]
        public List<MedicalCertificateModel> MedicalCertificateList { get; set; }

        [DataMember(Name = "licensesList")]
        public List<LicensesModel> LicensesList { get; set; }

        [DataMember(Name = "deletedMedicalCertificates")]
        public int[] DeletedMedicalCertificate { get; set; }

        [DataMember(Name = "deletedLicences")]
        public int[] DeletedLicences { get; set; }

        [DataMember(Name = "deletedRatings")]
        public int[] DeletedRatings { get; set; }

        [DataMember(Name = "imageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "imageOption")]
        public string ImageOption { get; set; }

        [DataMember(Name = "oldTokenId", IsRequired = false, EmitDefaultValue = true)]
        public string OldTokenId { get; set; }

        [DataMember(Name = "newTokenId", IsRequired = false, EmitDefaultValue = true)]
        public string NewTokenId { get; set; }

        [DataMember(Name = "isPushNotificationOn" , IsRequired = false , EmitDefaultValue = true)]
        public string IsPushNotificationOn { get; set; }

        [DataMember(Name = "isUpdateAvailableForProfile")]
        public bool IsUpdateAvailableForProfile { get; set; }

        //[DataMember(Name = "isUpdateAvailableForMedicalCertificate")]
        //public bool IsUpdateAvailableForMedicalCertificate { get; set; }

        //[DataMember(Name = "isUpdateAvailableForLicenseRating")]
        //public bool IsUpdateAvailableForLicenseRating { get; set; }

    }
}
