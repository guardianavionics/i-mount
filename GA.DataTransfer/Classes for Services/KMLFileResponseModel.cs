﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
  public  class KMLFileResponseModel : GeneralResponse
    {
         [DataMember(Name = "url")]
         public string URL { get; set; }
    }
}
