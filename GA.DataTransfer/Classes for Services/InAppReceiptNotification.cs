﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
  public  class InAppReceiptNotification
    {
        public string environment { get; set; }

        public string notification_type { get; set; }

        public string password { get; set; }

        public string original_transaction_id { get; set; }

        public string cancellation_date { get; set; }

        public InaapReceiptModel latest_receipt_info { get; set; }

        public InaapReceiptModel latest_expired_receipt_info { get; set; }

        public string auto_renew_status { get; set; }

        public string expiration_intent { get; set; }

        
    }
}
