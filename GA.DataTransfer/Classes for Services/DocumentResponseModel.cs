﻿using GA.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class DocumentResponseModel: GeneralResponse
    {
        public DocumentResponseModel Exception()
        {
            return new DocumentResponseModel 
                        { 
                            ResponseCode = ((int)Enumerations.Documents.Exception).ToString(),
                            ResponseMessage = Enumerations.Documents.Exception.GetStringValue(),
                        };
        }

        [DataMember(Name = "listOfDocuments")]
        public List<DocumentModelForApp> ListOfDocuments { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

    }
}
