﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class UsAirportResponseModel : GeneralResponse
    {
       [DataMember(Name = "lastUpdateDateTime")]
       public string LastUpdateDateTime { get; set; }

       [DataMember(Name = "fileURL")]
       public string FileURL { get; set; }

        [DataMember(Name = "startDate")]
        public string StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public string EndDate { get; set; }

        [DataMember(Name = "modifyDate")]
        public string ModifyDate { get; set; }

        [DataMember(Name = "fileSizeMB")]
        public decimal? FileSizeInMB { get; set; }
    }
}
