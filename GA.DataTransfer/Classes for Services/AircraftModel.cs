﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class AircraftModel
    {

        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "isAmericanAircraft", IsRequired = false, EmitDefaultValue = true)]
        public bool IsAmericanAircraft { get; set; }

        [DataMember(Name = "registration")]
        public string Registration { get; set; }

        [DataMember(Name = "make")]
        public int? Make { get; set; }//*123

        [DataMember(Name = "model")]
        public int? Model { get; set; }//*123

        [DataMember(Name = "color")]
        public string Color { get; set; }

        [DataMember(Name = "homeBase")]
        public string HomeBase { get; set; }

        [DataMember(Name = "capacity")]
        public string Capacity { get; set; }

        [DataMember(Name = "taxiFuel")]
        public string TaxiFuel { get; set; }

        [DataMember(Name = "additionalFuel")]
        public string AdditionalFuel { get; set; }

        [DataMember(Name = "fuelUnit")]
        public string FuelUnit { get; set; }

        [DataMember(Name = "weightUnit")]
        public string WeightUnit { get; set; }

        [DataMember(Name = "speedUnit")]
        public string SpeedUnit { get; set; }

        [DataMember(Name = "verticalSpeed")]
        public string VerticalSpeed { get; set; }

        [DataMember(Name = "power")]
        public double Power { get; set; }

        [DataMember(Name = "rpm")]
        public double RPM { get; set; }

        [DataMember(Name = "mp")]
        public double MP { get; set; }

        [DataMember(Name = "altitude")]
        public double Altitude { get; set; }

        [DataMember(Name = "fuelBurnCruise")]
        public double FuelBurnCruise { get; set; }

        [DataMember(Name = "tasCruise")]
        public double TASCruise { get; set; }

        [DataMember(Name = "rateOfClimb")]
        public double RateOfClimb { get; set; }

        [DataMember(Name = "fuelBurnRateClimb")]
        public double FuelBurnRateClimb { get; set; }

        [DataMember(Name = "tasClimb")]
        public double TASClimb { get; set; }

        [DataMember(Name = "rateOfDescent")]
        public double RateOfDescent { get; set; }

        [DataMember(Name = "fuelBurnRateOfDescent")]
        public double FuelBurnRateOfDescent { get; set; }

        [DataMember(Name = "tasDescent")]
        public double TASDescent { get; set; }

        [DataMember(Name = "imageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "imageOption")]
        public string ImageOption { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "engineType")]
        public int EngineType { get; set; }

        [DataMember(Name = "areoUnitNumber")]
        public string AeroUnitNo { get; set; }



        [DataMember(Name = "aircraftType")]
        public string AircraftType { get; set; }

        [DataMember(Name = "aircraftSerialNumber")]
        public string AircraftSerialNumber { get; set; }

        [DataMember(Name = "aircraftYear")]
        public int? AircraftYear { get; set; }

        [DataMember(Name = "currentHobbsTime")]
        public double CurrentHobbsTime { get; set; }

        [DataMember(Name = "hobbsTimeOffset")]
        public double HobbsTimeOffset { get; set; }

        [DataMember(Name = "currentTachTime")]
        public double CurrentTachTime { get; set; }

        [DataMember(Name = "tachTimeOffset")]
        public double TachTimeOffset { get; set; }

        [DataMember(Name = "engineMFGType")]
        public int? EngineMFGType { get; set; }

        [DataMember(Name = "engineTBO")]
        public double EngineTBO { get; set; }

        [DataMember(Name = "engine1LastMOHDate")]
        public string Engine1LastMOHDate { get; set; }

        [DataMember(Name = "currentEngineTime1", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentEngineTime1 { get; set; }

        [DataMember(Name = "engineTimeOffset1", IsRequired = false, EmitDefaultValue = true)]
        public double EngineTimeOffset1 { get; set; }

        [DataMember(Name = "propMFG")]
        public int? PropMFG { get; set; }

        [DataMember(Name = "prop1TBO")]
        public int Prop1TBO { get; set; }

        [DataMember(Name = "prop1OHDueDate")]
        public string Prop1OHDueDate { get; set; }

        [DataMember(Name = "currentPropTime1")]
        public double CurrentPropTime1 { get; set; }

        [DataMember(Name = "propTimeOffset1", IsRequired = false, EmitDefaultValue = true)]
        public double PropTimeOffset1 { get; set; }

        [DataMember(Name = "engine2LastMOHDate")]
        public string Engine2LastMOHDate { get; set; }

        [DataMember(Name = "currentEngineTime2", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentEngineTime2 { get; set; }

        [DataMember(Name = "engineTimeOffset2", IsRequired = false, EmitDefaultValue = true)]
        public double EngineTimeOffset2 { get; set; }

        [DataMember(Name = "prop2OHDueDate")]
        public string Prop2OHDueDate { get; set; }

        [DataMember(Name = "currentPropTime2", IsRequired = false, EmitDefaultValue = true)]
        public double CurrentPropTime2 { get; set; }

        [DataMember(Name = "propTimeOffset2", IsRequired = false, EmitDefaultValue = true)]
        public double PropTimeOffset2 { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

        [DataMember(Name = "comm")]
        public bool Comm { get; set; }

        [DataMember(Name = "transponder")]
        public bool Transponder { get; set; }

        [DataMember(Name = "engineMonitor")]
        public bool EngineMonitor { get; set; }

        [DataMember(Name = "ispilot")]
        public bool IsPilot { get; set; }

        [DataMember(Name = "manufactureName")]
        public string ManufacturerName { get; set; }

        [DataMember(Name = "modelName")]
        public string ModelName { get; set; }

        [DataMember(Name = "otherManufacturer")]
        public string OtherAircraftManufacturer { get; set; }

        [DataMember(Name = "otherModel")]
        public string OtheAircraftrModel { get; set; }

        [DataMember(Name = "ownerEmailId")]
        public string OwnerEmailId { get; set; }

        [DataMember(Name = "componentManufacturerModelMapping")]
        public List<AircraftComponentMakeAndModelApp> AircraftComponentMakeAndModels { get; set; }

        [DataMember(Name = "fuelQuantityModels")]
        public FuelQuantityModel FuelQuantityModels { get; set; }
        //public List<FuelQuantityModel> FuelQuantityModels { get; set; } //gp

        [DataMember(Name = "lastTransactionId")]
        public string LastTransactionId { get; set; }

        [DataMember(Name = "isAeroUnitBlocked")]
        public bool IsAeroUnitBlocked { get; set; }

        [DataMember(Name = "otherEngineManufactureType")]
        public string OtherEngineManufactureType { get; set; }

        [DataMember(Name = "otherPropManufactureType")]
        public string OtherPropManufactureType { get; set; }

        [DataMember(Name = "chargeBy")]
        public int? ChargeBy { get; set; }

        [DataMember(Name = "manufacturersMappedWithAircraft")]
        public string ManufacturersMappedWithAircraft { get; set; }

        //[DataMember(Name = "ShareDataWithManufacturer")]
        //public List<ManufacturerAuthForDataLog> ShareDataWithManufacturer { get; set; }

        [DataMember(Name = "shareDataWithManufacturer")]
        public string ShareDataWithManufacturer { get; set; }

        [DataMember(Name = "engineSettings")]
        public string EngineSettings { get; set; }

        [DataMember(Name = "installedEngine")]
        public string InstalledEngine { get; set; }

        [DataMember(Name = "iPassengerList")]

        public PassengerProIpassegerDetails IPassengerList { get; set; }

        [DataMember(Name = "isUpdateAvailableForGeneral")]
        public bool IsUpdateAvailableForGeneral { get; set; }

        [DataMember(Name = "isUpdateAvailableForPerformance")]
        public bool IsUpdateAvailableForPerformance { get; set; }

        [DataMember(Name = "isUpdateAvailableForEngine")]
        public bool IsUpdateAvailableForEngine { get; set; }

        [DataMember(Name = "isActiveForUser")]
        public bool IsActiveForUser { get; set; }

        [DataMember(Name = "isRegisterWithoutUnit")]
        public bool IsRegisterWithoutUnit { get; set; }

        [DataMember(Name = "isDefaultAircraft")]
        public bool IsDefaultAircraft { get; set; }

        [DataMember(Name ="missingFlightData")]

        public List<missingFightData> missingFightData { get; set; }

        [DataMember(Name = "missingFlight")]
        public string MissingFlight { get; set; }

        [DataMember(Name = "categoryList")]
        public List<CategoryModel> CategoryList { get; set; }
    }

    public class missingFightData
    {
        public int? flightId { get; set; }
        public bool? isMissingData { get; set; }
    }
}
