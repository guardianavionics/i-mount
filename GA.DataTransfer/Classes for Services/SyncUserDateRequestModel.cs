﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SyncUserDataModel
    {
        #region properties

        [DataMember(Name = "syncUserProfile")]
        public ProfileUpdateRequestModel SyncUserProfile { get; set; }

        [DataMember(Name = "syncAircraftProfile")]
        public AircraftProfileUpdateModel SyncAircraftProfile { get; set; }

        [DataMember(Name = "syncPreference")]
        public UpdatePreferenceModel SyncPreference { get; set; }

        [DataMember(Name = "syncLogBook")]
        public LogBookRequestModel syncLogBook { get; set; }

        [DataMember(Name = "syncNotification")]
        public GetNotificationRequest SyncNotification { get; set; }

        [DataMember(Name = "syncMaintenanceUser")]
        public MaintenanceUserRequest SyncMaintenanceUser { get; set; }


        [DataMember(Name = "syncSquawkList")]
        public SquawkListRequest SyncSquawkList { get; set; }

        [DataMember(Name = "syncDropdownListData")]
        public ManufacturereModelListRequest SyncDropdownListData { get; set; }

        #endregion
    }
}
