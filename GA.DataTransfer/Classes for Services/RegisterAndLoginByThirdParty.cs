﻿using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;


namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
    public class RegisterAndLoginByThirdParty
    {

        #region properties

        [Required]
        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        //[DataMember(Name = "password")]
        //public string Password { get; set; }

        [Required]
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [Required]
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [Required]
        [DataMember(Name = "socialLoginAccountId")]
        public string SocialLoginAccountId { get; set; }

        [DataMember(Name = "newTokenId")]
        public string NewTokenId { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }

        [DataMember(Name = "socialLoginAccountType")] // 1 for Facebook, 2 for Gmail
        public int SocialLoginAccountType { get; set; }

        [DataMember(Name = "streetAddress")]
        public string StreetAddress { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "zipCode")]
        public int? ZipCode { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        

        [DataMember(Name = "dateOfBirth")]
        public System.DateTime ? DateOfBirth { get; set; }

       
        //[DataMember(Name = "securityQuestion")]
        //public int SecurityQuestionId { get; set; }

        //[DataMember(Name = "securityAnswer")]
        //public string SecurityAnswer { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

               #endregion properties
    }
}
