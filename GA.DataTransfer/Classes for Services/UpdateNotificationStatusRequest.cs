﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class UpdateNotificationStatusRequest
    {
        [DataMember(Name = "notificationId")]
        public int NotificationId { get; set; }

        [DataMember(Name = "statusId")]
        public int StatusId { get; set; }

    }
}
