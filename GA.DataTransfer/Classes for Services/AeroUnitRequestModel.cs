﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class AeroUnitRequestModel
    {
        [DataMember(Name = "FMS")]
        public string FMS { get; set; }

        [DataMember(Name = "isPilot")]
        public bool IsPilot { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

    }
}
