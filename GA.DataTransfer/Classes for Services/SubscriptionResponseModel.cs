﻿using GA.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SubscriptionResponseModel : GeneralResponse
    {
        ///// <summary>
        ///// sets values for properties when exception occours
        ///// </summary>
        ///// <returns></returns>
        //public SubscriptionResponseModel Exception()
        //{
        //    return new SubscriptionResponseModel 
        //    {
        //        ResponseCode = Enumerations.RegistrationReturnCodes.Exception.GetStringValue(),
        //        ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.ToString(),
        //    };
        //}

        [DataMember(Name = "subscriptionList")]
        public List<SubscriptionModel> SubscriptionList { get; set; }

        [DataMember(Name = "featureList")]
        public List<FeatureMasterModel> FeatureList { get; set; }
    }
}
