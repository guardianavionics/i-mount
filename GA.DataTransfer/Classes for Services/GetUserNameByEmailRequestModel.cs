﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
   public class GetUserNameByEmailRequestModel 
    {
       [DataMember(Name = "emailId")]
       public string EmailId { get; set; }
    }
}
