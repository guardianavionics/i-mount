﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class PlatesRequestModel
    {
        [DataMember(Name = "arrStateId")]
       public int[] arrStateId { get; set; } 
    }
}
