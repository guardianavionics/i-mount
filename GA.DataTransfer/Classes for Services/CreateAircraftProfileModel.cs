﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class CreateAircraftProfileModel
    {
        [DataMember(Name = "AircraftTag")]
        public string aircraftTag { get; set; }

        [DataMember(Name = "aircrafProfile")]
        public AircraftProfileUpdateModel AircraftProfile { get; set; }
    }
}
