﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
   public class OTPVerificationcode:GeneralResponse
    {
        [DataMember(Name = "sentOTP")]
        public string senTOTP { get; set; }
    }
}
