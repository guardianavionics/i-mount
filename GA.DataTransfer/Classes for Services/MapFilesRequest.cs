﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class MapFilesRequest
    {
        [DataMember(Name = "lastUpdatedDateTime", IsRequired = false, EmitDefaultValue = true)]
        public string LastUpdatedDateTime { get; set; }
    }
}
