﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class LoginModel
    {
        #region properties

        [RegularExpression(@"^\s*(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*\s*$", ErrorMessageResourceName = "InvalidEmailId"
                            , ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [Display(Name = "Email Id")]
        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [Display(Name = "Password")]
        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }

        [DataMember(Name = "newTokenId", IsRequired = false, EmitDefaultValue = true)]
        public string NewTokenId { get; set; }

        

        #endregion
    }
    public class LoginOTPModel
    {
        #region properties

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [Display(Name = "OTP")]
        [DataMember(Name = "OTP")]
        public string OTP { get; set; }

        #endregion
    }
}
