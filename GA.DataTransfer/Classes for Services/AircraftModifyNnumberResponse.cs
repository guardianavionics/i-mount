﻿using System.Linq;
using GA.Common;
using GA.DataLayer;
using System;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.IO;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class AircraftModifyNnumberResponse : GeneralResponse
    {
        public static AircraftModel Create(AircraftProfile aircraft)
        {

            var objAircraftProfileUpdateModel = new AircraftModel();
            var context = new GuardianAvionicsEntities();
            objAircraftProfileUpdateModel.Registration = aircraft.Registration ?? string.Empty;
            objAircraftProfileUpdateModel.Capacity = aircraft.Capacity ?? string.Empty;
            //objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel ?? string.Empty;
            objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel == null ? string.Empty : aircraft.TaxiFuel.ToString(); //ChangeDataType
            //objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel ?? string.Empty;
            objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel == null ? string.Empty : aircraft.AdditionalFuel.ToString(); //ChangeDataType
            objAircraftProfileUpdateModel.FuelUnit = aircraft.FuelUnit ?? string.Empty;
            objAircraftProfileUpdateModel.WeightUnit = aircraft.WeightUnit ?? string.Empty;
            objAircraftProfileUpdateModel.SpeedUnit = aircraft.SpeedUnit ?? string.Empty;
            objAircraftProfileUpdateModel.VerticalSpeed = aircraft.VerticalSpeed ?? string.Empty;
            objAircraftProfileUpdateModel.Power = (aircraft.Power == null) ? 0 : (double)aircraft.Power;
            objAircraftProfileUpdateModel.RPM = (aircraft.RPM == null) ? 0 : (double)aircraft.RPM;
            objAircraftProfileUpdateModel.MP = (aircraft.MP == null) ? 0 : (double)aircraft.MP;
            objAircraftProfileUpdateModel.Altitude = (aircraft.Altitude == null) ? 0 : (double)aircraft.Altitude;
            objAircraftProfileUpdateModel.FuelBurnCruise = (aircraft.FuelBurnCruise == null) ? 0 : (double)aircraft.FuelBurnCruise;
            objAircraftProfileUpdateModel.TASCruise = (aircraft.TASCruise == null) ? 0 : (double)aircraft.TASCruise;
            objAircraftProfileUpdateModel.RateOfClimb = (aircraft.RateOfClimb == null) ? 0 : (double)aircraft.RateOfClimb;
            objAircraftProfileUpdateModel.TASClimb = (aircraft.TASClimb == null) ? 0 : (double)aircraft.TASClimb;
            objAircraftProfileUpdateModel.RateOfDescent = (aircraft.RateOfDescent == null) ? 0 : (double)aircraft.RateOfDescent;
            objAircraftProfileUpdateModel.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == null) ? 0 : (double)aircraft.FuelBurnRateOfDescent;
            objAircraftProfileUpdateModel.TASDescent = (aircraft.TASDescent == null) ? 0 : (double)aircraft.TASDescent;
            objAircraftProfileUpdateModel.IsAmericanAircraft = (aircraft.IsAmericaAircraft != null) && (bool)aircraft.IsAmericaAircraft;
            objAircraftProfileUpdateModel.AircraftId = aircraft.Id;
            objAircraftProfileUpdateModel.Color = aircraft.Color ?? string.Empty;
            objAircraftProfileUpdateModel.FuelBurnRateClimb = (aircraft.FuelBurnRateClimb == null) ? 0 : (double)aircraft.FuelBurnRateClimb;
            objAircraftProfileUpdateModel.HomeBase = aircraft.HomeBase ?? string.Empty;
            objAircraftProfileUpdateModel.Make = aircraft.Make;//*123
            objAircraftProfileUpdateModel.Model = aircraft.Model;//*123
            objAircraftProfileUpdateModel.ModelName = aircraft.AircraftModelList.ModelName;
           // objAircraftProfileUpdateModel.ManufacturerName = aircraft.AircraftModelList.AircraftManufacturer.Name;
            objAircraftProfileUpdateModel.AircraftType = aircraft.AircraftType ?? string.Empty;
            objAircraftProfileUpdateModel.AircraftSerialNumber = aircraft.AircraftSerialNo ?? string.Empty;
            objAircraftProfileUpdateModel.AircraftYear = aircraft.AircraftYear ?? default(int);
            objAircraftProfileUpdateModel.CurrentHobbsTime = aircraft.HobbsTime ?? default(double);
            objAircraftProfileUpdateModel.HobbsTimeOffset = aircraft.HobbsTimeOffset ?? default(double);
            objAircraftProfileUpdateModel.CurrentTachTime = aircraft.TachTime ?? default(double);
            objAircraftProfileUpdateModel.TachTimeOffset = aircraft.TachTimeOffset ?? default(double);
            objAircraftProfileUpdateModel.EngineMFGType = aircraft.EngineMFGType;
            objAircraftProfileUpdateModel.EngineTBO = aircraft.EngineTBO ?? default(double);
            objAircraftProfileUpdateModel.Engine1LastMOHDate = (aircraft.Engine1LastMOHDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Engine1LastMOHDate);

            objAircraftProfileUpdateModel.CurrentEngineTime1 = aircraft.CurrentEngineTime ?? 0 ;
            objAircraftProfileUpdateModel.PropMFG = aircraft.PropMFG ;

            objAircraftProfileUpdateModel.Prop1TBO = (aircraft.Prop1TBO == null) ? default(int) : (int)aircraft.Prop1TBO;
            objAircraftProfileUpdateModel.Prop1OHDueDate = (aircraft.Prop1OHDueDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Prop1OHDueDate);

            objAircraftProfileUpdateModel.CurrentPropTime1 = (aircraft.Prop1Time == null) ? default(double) : (double)aircraft.Prop1Time;
            objAircraftProfileUpdateModel.Engine2LastMOHDate = (aircraft.Engine2LastMOHDate == null)
                ? string.Empty
                : Misc.GetStringOnlyDate(aircraft.Engine2LastMOHDate);

            objAircraftProfileUpdateModel.CurrentEngineTime2 = aircraft.Engine2Time ??0 ;

            objAircraftProfileUpdateModel.Prop2OHDueDate = (aircraft.Prop2OHDueDate == null)
                ? string.Empty
                : Misc.GetStringOnlyDate(aircraft.Prop2OHDueDate);

            objAircraftProfileUpdateModel.CurrentPropTime2 = aircraft.Prop2Time ?? 0;



            objAircraftProfileUpdateModel.AeroUnitNo = aircraft.AeroUnitNo ?? string.Empty;

            // converting Engine Type to codes.
            if (aircraft.EngineType == Enumerations.EngineType.MultipleEngine.ToString())
                objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.MultipleEngine.GetStringValue());
            else if (aircraft.EngineType == Enumerations.EngineType.SingleEngine.ToString())
                objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.SingleEngine.GetStringValue());
            else
                objAircraftProfileUpdateModel.EngineType = 0;

            // send aircraft profile image
            if (!String.IsNullOrEmpty(aircraft.ImageUrl))
            {
                string path = ConfigurationReader.ImageServerPathKey;
                if (!string.IsNullOrEmpty(path))
                    objAircraftProfileUpdateModel.ImageName = path + aircraft.ImageUrl;
                else
                    objAircraftProfileUpdateModel.ImageName = string.Empty;
            }
            else
            {
                objAircraftProfileUpdateModel.ImageName = aircraft.ImageUrl ?? string.Empty;
            }

            var aeroUnitMaster = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Deleted);
            objAircraftProfileUpdateModel.IsAeroUnitBlocked = (aeroUnitMaster == null) ? true : aeroUnitMaster.Blocked;

            objAircraftProfileUpdateModel.UniqueId = aircraft.UniqueId ?? default(long);
            objAircraftProfileUpdateModel.Comm = aircraft.Comm;
            objAircraftProfileUpdateModel.Transponder = aircraft.Transponder;
            objAircraftProfileUpdateModel.EngineMonitor = aircraft.EngineMonitor;
           
                if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id + ".xml"))
                {
                    XDocument loaded = XDocument.Load(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml");
                    objAircraftProfileUpdateModel.EngineSettings = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + loaded.ToString().Replace("[]", ""); 
                }
             
            return objAircraftProfileUpdateModel;
        }

        public AircraftModifyNnumberResponse Exception()
        {
            return new AircraftModifyNnumberResponse
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue(),
            };
        }

        public AircraftModifyNnumberResponse DuplicateNNumber()
        {
            return new AircraftModifyNnumberResponse
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.NNumberIsNotUnique).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.NNumberIsNotUnique.GetStringValue(),
            };
        }


        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "imageName")]
        public string ImageName { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }

    }
}
