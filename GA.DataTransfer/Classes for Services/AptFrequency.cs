﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class AptFrequency
    {
        [DataMember(Name = "featureId")]
        public string FeatureId { get; set; }

        [DataMember(Name = "frequency")]
        public string Frequency { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }
}
