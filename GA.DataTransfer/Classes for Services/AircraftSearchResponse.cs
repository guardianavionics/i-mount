﻿using System.Runtime.Serialization;
using GA.Common;

namespace GA.DataTransfer.Classes_for_Services
{
    public class AircraftSearchResponse : GeneralResponse
    {
        [DataMember(Name = "registration")]
        public string Registration { get; set; }

        [DataMember(Name = "make")]
        public int? Make { get; set; }//*123

        [DataMember(Name = "model")]
        public int? Model { get; set; }//*123

        [DataMember(Name = "manufacturerName")]
        public string ManufacturerName { get; set; }//*123

        [DataMember(Name = "modelName")]
        public string ModelName { get; set; }//*123

        [DataMember(Name = "aircraftSerialNumber")]
        public string AircraftSerialNumber { get; set; }

        [DataMember(Name = "aircraftYear")]
        public int AircraftYear { get; set; }

        //[DataMember(Name = "Engine Type")]
        //public bool IsSingleEngine { get; set; }

        [DataMember(Name = "numbersOfEngine")]
        public int NumbersOfEngine { get; set; }

        public AircraftSearchResponse Exception()
        {
            return new AircraftSearchResponse 
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue(),
            };
        }

    }

}
