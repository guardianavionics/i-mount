﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public class ChartDetailModel
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "country")]
        public string Country{ get; set; }

        [DataMember(Name = "countryId")]
        public int CountryId { get; set; }

        //[DataMember(Name = "state")]
        //public string State { get; set; }

        //[DataMember(Name = "stateId")]
        //public int StateId { get; set; }

        [DataMember(Name = "chartType")]
        public string ChartType { get; set; }

        [DataMember(Name = "chartTypeId")]
        public int ChartTypeId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "shortName")]
        public string ShortName { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "version")]
        public string Version { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "format")]
        public string Format { get; set; }

        [DataMember(Name = "expDate")]
        public string ExpDate { get; set; }

        [DataMember(Name = "expiration")]
        public string Expiration { get; set; }

        [DataMember(Name = "provider")]
        public string Provider { get; set; }

        [DataMember(Name = "shortProvider")]
        public string shortProvider { get; set; }

        [DataMember(Name = "copyright")]
        public string Copyright { get; set; }

        [DataMember(Name = "createDate")]
        public string CreateDate { get; set; }

        [DataMember(Name = "shortChartType")]
        public string ShortChartType { get; set; }

        [DataMember(Name = "minZoom")]
        public int? MinZoom { get; set; }

        [DataMember(Name = "maxZoom")]
        public int? MaxZoom { get; set; }

        [DataMember(Name = "bounds")]
        public string Bounds { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "fileSize")]
        public decimal? FileSize { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
