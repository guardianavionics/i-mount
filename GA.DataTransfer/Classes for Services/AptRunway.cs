﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
   public class AptRunway
    {
        [DataMember(Name = "featureId")]
        public string FeatureId { get; set; }

        [DataMember(Name = "length")]
        public Nullable<int> Length { get; set; }

        [DataMember(Name = "width")]
        public Nullable<int> Width { get; set; }

        [DataMember(Name = "BEID")]
        public string BEID { get; set; }

        [DataMember(Name = "REID")]
        public string REID { get; set; }

        [DataMember(Name = "surface")]
        public Nullable<int> Surface { get; set; }
    }
}
