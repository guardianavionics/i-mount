﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class AircraftAndIpassengerDetailModel
    {
        [DataMember(Name = "tailNumber")]
        public string TailNumber { get; set; }

        [DataMember(Name = "aircraftImagePath")]
        public string AircraftImagePath { get; set; }

        [DataMember(Name = "iPassengerList")]
        public PassengerProIpassegerDetails IPassengerList { get; set; }
    }
}
