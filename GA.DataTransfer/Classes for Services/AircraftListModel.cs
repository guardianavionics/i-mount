﻿using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class AircraftListModel
    {
        [DataMember(Name = "profileId")]
        public string ProfileId { get; set; }

        [DataMember(Name = "lastUpdateDateTime")]
        public string LastUpdateDateTime { get; set; }
    }
}
