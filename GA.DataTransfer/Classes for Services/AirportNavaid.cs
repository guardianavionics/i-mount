﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
   public class AirportNavaid
    {
        [DataMember(Name = "uniqueID")]
        public string UniqueID { get; set; }

        [DataMember(Name = "featureID")]
        public string FeatureID { get; set; }

        [DataMember(Name = "type")]
        public Nullable<int> Type { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "freq")]
        public string Freq { get; set; }

        [DataMember(Name = "navRange")]
        public string Nav_Range { get; set; }

        [DataMember(Name = "y")]
        public Nullable<decimal> Y { get; set; }

        [DataMember(Name = "x")]
        public Nullable<decimal> X { get; set; }

        [DataMember(Name = "altitude")]
        public string Altitude { get; set; }

        [DataMember(Name = "arptICAO")]
        public string ARPT_ICAO { get; set; }

        [DataMember(Name = "magVar")]
        public string Mag_Var { get; set; }

        


    }
}
