﻿using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class LiveDataRequestModel
    {
        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "engineType")]
        public string EngineType { get; set; }

        [DataMember(Name = "flightData")]
        public string FlightData { get; set; }

        [DataMember(Name = "pilotId")]
        public int? PilotId { get; set; }

        [DataMember(Name = "coPilotId")]
        public int? CoPilotId { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

    }
}
