﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
   public class UpdateLogBookForPilotAndCopilot
    {
        [DataMember(Name = "logBookModel", IsRequired = false, EmitDefaultValue = true)]
       public LogBookModel logBookModel { get; set; }

        //[DataMember(Name = "lastUpdateDate")]
        //public string LastUpdateDate { get; set; }

        //[DataMember(Name = "profileId")]
        //public int ProfileId { get; set; }
    }
}
