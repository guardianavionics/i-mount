﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class SubscriptionRequestModel
    {
        [DataMember(Name="profileId")]
        public int ProfileId { get; set; }

        [DataMember(Name = "transactionReceipt")]
        public string TransactionReceipt { get; set; }

        [DataMember(Name = "productIdentifier")]
        public string ProductIdentifier { get; set; }

    }
}
