﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class PassengerProIpassegerDetails
    {
        [DataMember(Name = "videoList")]
        public List<FileDetails> VideoList { get; set; }
        [DataMember(Name = "imageList")]
        public List<FileDetails> ImageList { get; set; }
        [DataMember(Name = "pdfList")]
        public List<FileDetails> PDFList { get; set; }
        [DataMember(Name = "warningList")]
        public List<DocumentDetails> WarningList { get; set; }
        [DataMember(Name = "instructionList")]
        public List<DocumentDetails> InstructionList { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FileDetails
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "filePath")]
        public string FilePath { get; set; }
        [DataMember(Name = "title")]
        public string Title { get; set; }
        [DataMember(Name = "thumbnailImagePath")]
        public string ThumbnailImagePath { get; set; }

        [DataMember(Name = "fileSizeInMB")]
        public decimal? FileSizeInMB { get; set; }

    }

    [DataContract]
    [Serializable]
    public class DocumentDetails
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

}
