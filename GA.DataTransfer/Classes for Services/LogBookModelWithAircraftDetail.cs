﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class LogBookModelWithAircraftDetail
    {

        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        [DataMember(Name = "logBookId")]
        public int Id { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }

        [Display(Name = "Actual")]
        [DataMember(Name = "actual", EmitDefaultValue = true, IsRequired = false)]
        public string Actual { get; set; }


        [Display(Name = "CoPilot")]
        [DataMember(Name = "coPilot")]
        public string CoPilot { get; set; }

        [Display(Name = "Cross Country")]
        [DataMember(Name = "crossCountry")]
        public string CrossCountry { get; set; }

        [Display(Name = "Date")]
        [DataMember(Name = "date")]
        public string Date { get; set; }

        [Display(Name = "Day PIC")]
        [DataMember(Name = "dayPIC")]
        public string DayPIC { get; set; }

        [Display(Name = "Hood")]
        [DataMember(Name = "hood")]
        public string Hood { get; set; }

        [Display(Name = "Night PIC")]
        [DataMember(Name = "nightPIC")]
        public string NightPIC { get; set; }

        [Display(Name = "Remark")]
        [DataMember(Name = "remark")]
        public string Remark { get; set; }

        [Display(Name = "Route")]
        [DataMember(Name = "route")]
        public string Route { get; set; }

        [Display(Name = "Sim")]
        [DataMember(Name = "sim")]
        public string Sim { get; set; }

        //[Display(Name = "Finished")]
        //[DataMember(Name = "finished")]
        //public bool Finished { get; set; }

        [Display(Name = "IFR Appchs")]
        [DataMember(Name = "iFRAppchs")]
        public string IFRAppchs { get; set; }

        //[DataMember(Name = "aircraft")]
        //public AircraftModel Airacraft { get; set; }

        //[DataMember(Name = "uploadOnDropbox")]
        //public bool UploadOnDropbox { get; set; }

        [DataMember(Name = "uniqueId")]
        public long UniqueId { get; set; }

        [DataMember(Name = "pilotEmailId")]
        public string PilotEmailId { get; set; }

        [DataMember(Name = "coPilotEmailId")]
        public string CoPilotEmailId { get; set; }

        [DataMember(Name = "flightId")]
        public int FlightId { get; set; }

    }
}
