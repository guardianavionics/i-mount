﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
   public class AirportNavAndFreqStatusApp
    {
        [DataMember(Name = "startDate")]
        public string StartDate { get; set; }

        [DataMember(Name = "endDate")]
        public string EndDate { get; set; }

        [DataMember(Name = "modifyDate")]
        public string ModifyDate { get; set; }

        [DataMember(Name = "fileURL")]
        public string FileURL { get; set; }

        [DataMember(Name = "fileSizeMB")]
        public decimal? FileSizeInMB { get; set; }
    }
}
