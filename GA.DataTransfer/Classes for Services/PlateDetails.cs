﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GA.DataLayer;
using System;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class PlateDetails
    {
        [DataMember(Name = "airport_Name_Id")]
        public int? Airport_Name_Id { get; set; }

        [DataMember(Name = "airportId")]
        public int? AirportId { get; set; }

        [DataMember(Name = "chart_Code")]
        public string Chart_Code { get; set; }

        [DataMember(Name = "chart_Name")]
        public string Chart_Name { get; set; }

        [DataMember(Name = "chartSeq")]
        public string ChartSeq { get; set; }

        [DataMember(Name = "civil")]
        public string Civil { get; set; }

        [DataMember(Name = "copter")]
        public string Copter { get; set; }

        [DataMember(Name = "draw_Bottom")]
        public string Draw_Bottom { get; set; }

        [DataMember(Name = "draw_Left")]
        public string Draw_Left { get; set; }

        [DataMember(Name = "draw_Right")]
        public string Draw_Right { get; set; }

        [DataMember(Name = "draw_Top")]
        public string Draw_Top { get; set; }

        [DataMember(Name = "excludeAreas")]
        public string ExcludeAreas { get; set; }

        [DataMember(Name = "faanfd15")]
        public string Faanfd15 { get; set; }

        [DataMember(Name = "faanfd18")]
        public string Faanfd18 { get; set; }

        [DataMember(Name = "file_Name")]
        public string File_Name { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "invalid")]
        public bool Invalid { get; set; }

        [DataMember(Name = "lastChanged")]
        public string LastChanged { get; set; }

        [DataMember(Name = "lat1")]
        public string Lat1 { get; set; }

        [DataMember(Name = "lat2")]
        public string Lat2 { get; set; }

        [DataMember(Name = "long1")]
        public string Long1 { get; set; }

        [DataMember(Name = "long2")]
        public string Long2 { get; set; }

        [DataMember(Name = "orientation")]
        public string Orientation { get; set; }

        [DataMember(Name = "userAction")]
        public string UserAction { get; set; }

        [DataMember(Name = "validWithinRadius")]
        public string ValidWithinRadius { get; set; }

        [DataMember(Name = "validWithinX")]
        public string ValidWithinX { get; set; }

        [DataMember(Name = "validWithinY")]
        public string ValidWithinY { get; set; }

        [DataMember(Name = "x1")]
        public string X1 { get; set; }

        [DataMember(Name = "x2")]
        public string X2 { get; set; }

        [DataMember(Name = "y1")]
        public string Y1 { get; set; }

        [DataMember(Name = "y2")]
        public string Y2 { get; set; }

        [DataMember(Name = "fileSize")]
        public double FileSize { get; set; }
    }
}
