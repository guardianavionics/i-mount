﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class ReassignIssueRequestModel
    {
        [DataMember(Name = "issueId")]
        public int IssueId { get; set; }

        [DataMember(Name = "assigner")]
        public int Assigner { get; set; }

        [DataMember(Name = "assignee")]
        public int Assignee { get; set; }

    }
}
