﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
   public  class MaintenanceUserRequest
    {
       [DataMember(Name = "lastUpdateDate")]
       public string LastUpdateDate { get; set; }
    }
}
