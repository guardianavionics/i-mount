﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
   public  class InappReceiptResponseModel
    {
        [DataMember(Name = "latest_receipt_info")]
        public List<InaapReceiptModel> Latest_Receipt_Info { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "pending_renewal_info")]
        public List<PendingRenewalInfoModel> Pending_Renewal_Info { get; set; }
    }
}
