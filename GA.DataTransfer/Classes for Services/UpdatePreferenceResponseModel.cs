﻿using System.Runtime.Serialization;
using GA.Common;
using GA.DataTransfer.Classes_for_Services;

namespace GA.DataTransfer
{
    [DataContract]
    public class UpdatePreferenceResponseModel : GeneralResponse
    {
        /// <summary>
        /// Returns response with Exception 
        /// </summary>
        /// <returns>UpdatePreferenceResponseModel object with response code set as exception</returns>
        public UpdatePreferenceResponseModel Exception()
        {
            return new UpdatePreferenceResponseModel
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue()
            };
        }
    }
}
