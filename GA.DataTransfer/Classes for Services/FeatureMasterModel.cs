﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
  public  class FeatureMasterModel
    {
        [DataMember(Name ="id")]
        public int Id { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "keyCode")]
        public string KeyCode { get; set; }
        [DataMember(Name = "isEnable")]
        public bool IsEnable { get; set; }
        [DataMember(Name = "groupType")]
        public string GroupType { get; set; }
        [DataMember(Name = "sno")]
        public int? SNO { get; set; }
    }
}
