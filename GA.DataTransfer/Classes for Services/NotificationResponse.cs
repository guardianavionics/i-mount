﻿using System.Runtime.Serialization;
using System.Collections.Generic;



namespace GA.DataTransfer.Classes_for_Services
{
     [DataContract]
  public   class NotificationResponse : GeneralResponse
    {
         [DataMember(Name = "notificationList")]
         public List<NotificationModel> NotificationList { get; set; }


         [DataMember(Name = "lastUpdateDate")]
         public string LastUpdateDate { get; set; } 

    }
}
