﻿using System;
using System.Runtime.Serialization;


namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class IssueModel
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "assignerId")]
        public int AssignerId { get; set; }

        [DataMember(Name = "assignerName")]
        public string AssignerName { get; set; }

        [DataMember(Name = "assigneeId")]
        public Nullable<int> AssigneeId { get; set; }

        [DataMember(Name = "assigneeName")]
        public string AssigneeName { get; set; }

        [DataMember(Name = "statusId")]
        public int StatusId { get; set; }

        //[DataMember(Name = "status")]
        //public string Status { get; set; }

        [DataMember(Name = "closedById")]
        public Nullable<int> ClosedById { get; set; }

        [DataMember(Name = "closedByName")]
        public string ClosedByName { get; set; }

        [DataMember(Name = "createDate")]
        public string CreateDate { get; set; }

        [DataMember(Name = "updateDate")]
        public string UpdateDate { get; set; }

        [DataMember(Name = "aircraftId")]
        public Nullable<int> AircraftId { get; set; }


        [DataMember(Name = "priorityId")]
        public Nullable<int> PriorityId { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "registrationNo")]
        public string RegistrationNo { get; set; }

        [DataMember(Name = "isOwner")]
        public bool IsOwner { get; set; }

    }
}
