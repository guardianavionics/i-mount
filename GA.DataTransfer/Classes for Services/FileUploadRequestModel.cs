﻿using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    public class FileUploadRequestModel
    {
        [DataMember(Name="dataAsString")]
        public string DataAsString { get; set; }

        [DataMember(Name = "pilotLogId")]
        public int PilotLogId { get; set; }

        [DataMember(Name = "isLastDataForFlight")]
        public bool IsLastDataForFlight { get; set; }

        [DataMember(Name = "aircraftId")]
        public int AircraftId { get; set; }

        //[DataMember(Name = "isLastDataForFlight1")]
        //public string IsLastDataForFlight1 { get; set; }

        //[DataMember(Name = "isLastDataForFlight2")]
        //public string IsLastDataForFlight2 { get; set; }
    }
}
