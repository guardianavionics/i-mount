﻿using GA.Common;
using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Services
{
    [DataContract]
    [Serializable]
    public class FuelQuantityModel: GeneralResponse
    {
        [DataMember(Name = "aircraftId")]
        public int aircraftId { get; set;}
        [DataMember(Name = "right")]
        public Right right { get; set; }
        [DataMember(Name = "left")]
        public Left left { get; set; }

        public FuelQuantityModel Exception()
        {
            return new FuelQuantityModel
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue(),
            };
        }

        public class Right { 
            public double fuelValue { get; set; }
            public List<Frequencies> frequencies { get; set; }
        }
        public class Left
        {
            public double fuelValue { get; set; }
            public List<Frequencies> frequencies { get; set; }
        }
        public class Frequencies
        {
            [DataMember(Name = "frequencyAtLevel")]
            public int frequencyAtLevel { get; set; }
            [DataMember(Name = "frequency")]
            public double frequency { get; set; }
            [DataMember(Name = "gallonLevel")]
            public double gallonLevel { get; set; }
        }
    }
}
