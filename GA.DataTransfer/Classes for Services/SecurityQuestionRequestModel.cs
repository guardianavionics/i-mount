﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
   
    public class SecurityQuestionRequestModel
    {
        [DataMember(Name = "SecurityQuestionList")]
        public List<ListItems> SecurityQuestionList { get; set; }

    }
}
