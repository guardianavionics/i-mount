﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using GA.Common;

namespace GA.DataTransfer
{
    [DataContract]
    public class MapFilesResponse : GeneralResponse
    {
        [DataMember(Name = "mapFilesList")]
        public List<MapFilesModel> MapFilesList { get; set; }

        [DataMember(Name = "lastUpdatedDateTime")]
        public string LastUpdatedDateTime { get; set; }

        public static MapFilesResponse Exception()
        {
            return new MapFilesResponse
            {
                ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
            };
        }
    }
}
