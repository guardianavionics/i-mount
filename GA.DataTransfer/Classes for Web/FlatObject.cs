﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace GA.DataTransfer.Classes_for_Web
{
   
    public class FlatObject
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public string label { get; set; }
        
    }

    public class RecursiveObject
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public string label { get; set; }
        public List<RecursiveObject> children { get; set; }
    }
}
