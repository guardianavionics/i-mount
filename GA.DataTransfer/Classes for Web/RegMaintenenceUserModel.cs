﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
 

namespace GA.DataTransfer.Classes_for_Web
{
   public class RegMaintenenceUserModel
    {

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "FirstNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "First Name")]
        [Required(ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "FirstNameNotFound")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "LastNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Last Name")]
        [Required(ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "LastNameNotFound")]
        public string LastName { get; set; }

        

        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                                  ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Email Id")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessageResourceName = "EmailIdNotFound", ErrorMessageResourceType = typeof(GAResource))]
        public string EmailId { get; set; }


        
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [StringLength(50, MinimumLength = 6, ErrorMessageResourceName = "PasswordLimit", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        public string Password { get; set; }


        [Compare("Password", ErrorMessageResourceName = "ConfirmPasswordNotMatch", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Confirm Password")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string ConfirmPassword { get; set; }


        public int ProfileId { get; set; }

        public string uniqueId { get; set; }

    }
}
