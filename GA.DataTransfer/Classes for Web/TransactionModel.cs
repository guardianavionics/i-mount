﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class TransactionModel
    {
        public string SubProfileId { get; set; }

        public string ReceiptId { get; set; }

        public string TransactionId { get; set; }

        public string PayerId { get; set; }

        public string ReceiverId { get; set; }

        public string PayerFirstName { get; set; }

        public string PayerLastName { get; set; }

        public string PayerEmailId { get; set; }

       public string TransactionType { get; set; }

        public string PaymentStatus { get; set; }

        public decimal? PaymentGross { get; set; }

        public string Currency { get; set; }

        public DateTime? Date { get; set; }

        public string ItemName { get; set; }

        public int SNO { get; set; }


    }
}
