﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class PartIndexModel
    {
        public int SNO { get; set; }
        public string PartIndex { get; set; }
        public string Description { get; set; }
        public string LastAddedPartNumber { get; set; }
        public string Id { get; set; }
    }
}
