﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GA.DataTransfer
{
    public class UserModelWeb
    {
        //[RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
        //                                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
        //                                          ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]

        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
        //                                  ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]
        //[Email(ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]


        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                                  ErrorMessageResourceName = "InvalidEmailId", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Email Id")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string EmailId { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        [StringLength(50, MinimumLength = 6, ErrorMessageResourceName = "PasswordLimit", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        public string Password { get; set; }
        
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessageResourceName = "ConfirmPasswordNotMatch", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Confirm Password")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        //[StringLength(20, MinimumLength = 6, ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "abcd")]
        public string ConfirmPassword { get; set; }

        public List<SelectListItem> Countries { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "InvalidFirstName", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "First Name")]
        [Required(ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages), ErrorMessageResourceName = "Required")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessageResourceName = "InvalidLastName", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Last Name")]
        [Required(ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages), ErrorMessageResourceName = "Required")]
        public string LastName { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessageResourceName = "InvalidCityName", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "City")]
        public string City { get; set; }

        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessageResourceName = "InvalidStateName", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "State")]
        public string State { get; set; }

        [RegularExpression(@"^[0-9]{4,8}$", ErrorMessageResourceName = "InvalidZipCode", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.UserProfile.Messages))]
        [Display(Name = "Zip Code")]
        [MaxLength(8)]
        public string ZipCode { get; set; }

        [RegularExpression(@"(\+[0-9]+|[0-9]+)", ErrorMessageResourceName = "PhoneNumberNotValidMessage" , ErrorMessageResourceType= typeof(GAResource))]
        [Display(Name = "Phone Number")]
        //[Required(ErrorMessageResourceName = "PhoneNumberNotFound" , ErrorMessageResourceType= typeof(GAResource))]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please select country code")]
        public string CountryCode { get; set; }



        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date,ErrorMessage ="Invalid date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]//[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]//"{0:dd/MM/yyyy}"
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "Security Question")]
        public string SecurityQuestion { get; set; }

        [Display(Name = "Security Answer")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string SecurityAnswer { get; set; }

        [Display(Name = "Company Name")]
       // [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GA.Common.ResourcrFiles.Common.Messages))]
        public string CompanyName { get; set; }

        public int DDLid { get; set; }

        public List<ListItems> SecurityQuestionsList { get; set; }

        public bool IsRegisterByAdmin { get; set; }

         [Display(Name = "Comp. Manufacturer")]
        public List<ListItems> ComponentManufacturerList { get; set; }

         public int CompManufacturerId { get; set; }
         public string OtherCompManufacturer { get; set; }

        

         public string ViewName { get; set; } // set the view name from which the admin call the register page like from Config Setting or from Manufacturer user list

    }
}
