﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataLayer;
namespace GA.DataTransfer.Classes_for_Web
{
    public class GetConstants : GeneralResponse
    {
        public List<ListItems> Propeller { get; set; }

        public List<ListItems> RatingType { get; set; }

        public List<ListItems> MedicalClass { get; set; }

        public List<ListItems> Engine { get; set; }

        public List<ListItems> Document { get; set; }

        public List<ListItems> AircraftManufacturer { get; set; }

        public List<ListItems> AircraftModel { get; set; }

        public List<ListItems> ComponentManufacturer { get; set; }

        public List<ComponentModel_ConfigSetting> ComponentModel { get; set; }

        public List<ListItems> ComponentList { get; set; }

        public List<ComponentManufacturerAndUser> ComponentManufacturerAndUserList { get; set; } 
        }

    public class ComponentModel_ConfigSetting
    {
        public int Id { get; set; }
        public string componentName { get; set; }
        public int CompnentId { get; set; }
        public string ManufacturerName { get; set; }
        public int ComponentManufacturerId { get; set; }
        public string ModelName { get; set; }
        public string FileName { get; set; }
    }

    public class ComponentManufacturerAndUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public GA.DataLayer.MappingAircraftManufacturerAndUser mappingAircraftManufacturerAndUsers { get; set; }
        public string EmailId { get; set; }
    }
}
