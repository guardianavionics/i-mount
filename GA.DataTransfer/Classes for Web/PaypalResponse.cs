﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class PaypalResponse
    {
        public double GrossTotal { get; set; }
       
        public string PaymentStatus { get; set; }
        public string PayerFirstName { get; set; }
        public double PaymentFee { get; set; }
        public string BusinessEmail { get; set; }
        public string PayerEmail { get; set; }
        public string PayerId { get; set; }
        public double TxToken { get; set; }
        public string PayerLastName { get; set; }
        public string RecieverEmail { get; set; }
        public string ReceiverId { get; set; }
        public string ItemName { get; set; }
        public string Currency { get; set; }
        public string TransactionId { get; set; }
        public string ReceiptId { get; set; }
        public string SubProfileId { get; set; }
        public string Custom { get; set; }
        public string PaymentDate { get; set; }
        public int ProfileId { get; set; }

        public string PayerAddressCountry { get; set; }
        public string PayerAddressState { get; set; }
        public string PayerAddressCity { get; set; }
        public string PayerAddressStreet { get; set; }
        public string PayerAddressZip { get; set; }
        public string TransactionType { get; set; }
        public string PaymentType { get; set; }
        public int SubscriptionItemId { get; set; }



    }
}
