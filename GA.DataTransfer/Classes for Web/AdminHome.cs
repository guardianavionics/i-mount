﻿using System.Collections.Generic;

namespace GA.DataTransfer.Classes_for_Web
{
    public class AdminHome
    {
        public List<UserInfo> UserList { get; set; }

        public List<UserInfo> ManufacturerUserList { get; set; }

        public List<UserInfo> InActiveUserList { get; set; }
    }
}
