﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GA.DataTransfer.Classes_for_Web
{
    public class JPIDataExcelSheets
    {
        [Display(Name = "FlightNumber")]
        public string FlightNumber { get; set; }

        [Display(Name = "RegistrationNumber")]
        public string RegistrationNumber { get; set; }


        [Display(Name = "Lcl Date")]
        public string Date { get; set; }

        [Display(Name = "Lcl Time")]
        public string Time { get; set; }

        [Display(Name = "UTCOfst")]
        public String utcOfst { get; set; }

        [Display(Name = "WayPoint")]
        public string WayPoint { get; set; }

        [Display(Name = "Latitude")]
        public double Latitude { get; set; }

        [Display(Name = "Longitude")]
        public double Longitude { get; set; }

        [Display(Name = "AltB")]
        public String AltB { get; set; }

        [Display(Name = "BaroA")]
        public String BaroA { get; set; }

        [Display(Name = "AltMSL")]
        public String AltMSL { get; set; }

        [Display(Name = "OAT")]
        public string Oat { get; set; }

        [Display(Name = "IAS")]
        public String IAS { get; set; }

        [Display(Name = "GndSpd")]
        public String GndSpd { get; set; }

        [Display(Name = "VSpd")]
        public String VSpd { get; set; }

        [Display(Name = "Pitch")]
        public String Pitch { get; set; }

        [Display(Name = "Roll")]
        public String Roll { get; set; }

        [Display(Name = "LatAc")]
        public String LatAc { get; set; }

        [Display(Name = "NormAc")]
        public String NormAc { get; set; }

        [Display(Name = "HDG")]
        public String HDG { get; set; }

        [Display(Name = "TRK")]
        public String TRK { get; set; }

        [Display(Name = "Volt")]
        public string Volt1 { get; set; }

        [Display(Name = "Volt2")]
        public String Volt2 { get; set; }

        [Display(Name = "AMP1")]
        public String AMP1 { get; set; }

        [Display(Name = "AMP2")]
        public String AMP2 { get; set; }

        
        [Display(Name = "FQtyL")]
        public String FQL { get; set; }

        [Display(Name = "FQtyR")]
        public String FQR { get; set; }
        
        [Display(Name = "E1 FFlow")]
        public string FF { get; set; }
        
        [Display(Name = "E1 OilP")]
        public string OilP { get; set; }
        
        [Display(Name = "E1 OilT")]
        public string   OilT { get; set; }

        [Display(Name = "E1 MAP")]
        public string MAP { get; set; }

        [Display(Name = "E1 RPM")]
        public string RPM { get; set; }

        [Display(Name = "E1 CHT1")]
        public string Cht1 { get; set; }

        [Display(Name = "E1 CHT2")]
        public string Cht2 { get; set; }

        [Display(Name = "E1 CHT3")]
        public string Cht3 { get; set; }

        [Display(Name = "E1 CHT4")]
        public string Cht4 { get; set; }

        [Display(Name = "E1 CHT5")]
        public string Cht5 { get; set; }

        [Display(Name = "E1 CHT6")]
        public string Cht6 { get; set; }

        [Display(Name = "E1 EGT1")]
        public string Egt1 { get; set; }

        [Display(Name = "E1 EGT2")]
        public string Egt2 { get; set; }

        [Display(Name = "E1 EGT3")]
        public string Egt3 { get; set; }

        [Display(Name = "E1 EGT4")]
        public string Egt4 { get; set; }

        [Display(Name = "E1 EGT5")]
        public string Egt5 { get; set; }

        [Display(Name = "E1 EGT6")]
        public string Egt6 { get; set; }

        [Display(Name = "E1 TIT")]
        public string Tit { get; set; }

        [Display(Name = "AltGPS")]
        public string GpsAltitude { get; set; }

        [Display(Name = "TAS")]
        public String TAS { get; set; }

        [Display(Name = "HSIS")]
        public String HSIS { get; set; }

        [Display(Name = "CRS")]
        public String CRS { get; set; }

        [Display(Name = "NAV1")]
        public String NAV1 { get; set; }

        [Display(Name = "NAV2")]
        public String NAV2 { get; set; }

        [Display(Name = "COM1")]
        public String COM1 { get; set; }

        [Display(Name = "COM2")]
        public String COM2 { get; set; }

        [Display(Name = "HCDI")]
        public String HCDI { get; set; }

        [Display(Name = "VCDI")]
        public String VCDI { get; set; }

        [Display(Name = "WndSpd")]
        public String WndSpd { get; set; }

        [Display(Name = "WndDr")]
        public String WndDr { get; set; }

        [Display(Name = "WptDst")]
        public String WptDst { get; set; }

        [Display(Name = "WptBrg")]
        public String WptBrg { get; set; }

        [Display(Name = "MagVar")]
        public String MagVar { get; set; }

        [Display(Name = "AfcsOn")]
        public String AfcsOn { get; set; }

        [Display(Name = "RollM")]
        public String RollM { get; set; }

        [Display(Name = "PitchM")]
        public String PitchM { get; set; }

        [Display(Name = "RollC")]
        public String RollC { get; set; }

        [Display(Name = "PitchC")]
        public String PichC { get; set; }

        [Display(Name = "VSpdG")]
        public String VSpdG { get; set; }

        [Display(Name = "GPSfix")]
        public String GPSfix { get; set; }

        [Display(Name = "HAL")]
        public String HAL { get; set; }

        [Display(Name = "VAL")]
        public String VAL { get; set; }

        [Display(Name = "HPLwas")]
        public String HPLwas { get; set; }

        [Display(Name = "HPLfd")]
        public String HPLfd { get; set; }

        [Display(Name = "VPLwas")]
        public String VPLwas { get; set; }

        [Display(Name = "YAW")]
        public String Yaw { get; set; }

        [Display(Name = "CDT")]
        public String CDT { get; set; }

        [Display(Name = "CLD")]
        public String CLD { get; set; }

        [Display(Name = "END")]
        public String END { get; set; }

        [Display(Name = "FP")]
        public String FP { get; set; }

        

        [Display(Name = "IAT")]
        public String IAT { get; set; }

        [Display(Name = "MPG")]
        public String MPG { get; set; }

        [Display(Name = "REM")]
        public String REM { get; set; }

        [Display(Name = "REQ")]
        public String REQ { get; set; }

        [Display(Name = "RES")]
        public String RES { get; set; }

        [Display(Name = "USD")]
        public String USD { get; set; }

      

        [Display(Name = "CalculatedFuelRemaining")]
        public string CalculatedFuelRemaining { get; set; }

        [Display(Name = "TotalAircraftTime")]
        public string TotalAircraftTime { get; set; }

        [Display(Name = "EngineTime")]
        public string EngineTime { get; set; }

        [Display(Name = "ElevatorTrimPosition")]
        public string ElevatorTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator")]
        public string UnitsIndicator { get; set; }

        [Display(Name = "FlapPosition")]
        public string FlapPosition { get; set; }

        [Display(Name = "UnitsIndicator2")]
        public string UnitsIndicator2 { get; set; }

        [Display(Name = "CarbTemp")]
        public string CarbTemp { get; set; }

        [Display(Name = "UnitsIndicator3")]
        public string UnitsIndicator3 { get; set; }

        [Display(Name = "CoolantPressure")]
        public string CoolantPressure { get; set; }

        [Display(Name = "UnitsIndicator4")]
        public string UnitsIndicator4 { get; set; }

        [Display(Name = "CoolantTemperature")]
        public string CoolantTemperature { get; set; }

        [Display(Name = "UnitsIndicator5")]
        public string UnitsIndicator5 { get; set; }

        [Display(Name = "UnitsIndicator6")]
        public string UnitsIndicator6 { get; set; }

        [Display(Name = "AileronTrimPosition")]
        public string AileronTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator7")]
        public string UnitsIndicator7 { get; set; }

        [Display(Name = "RudderTrimPosition")]
        public string RubberTrimPosition { get; set; }

        [Display(Name = "UnitsIndicator8")]
        public string UnitsIndicator8 { get; set; }

        [Display(Name = "FuelQty3")]
        public string FuelQty3 { get; set; }

        [Display(Name = "UnitsIndicator9")]
        public string UnitsIndicator9 { get; set; }

        [Display(Name = "FuelQty4")]
        public string FuelQty4 { get; set; }

        [Display(Name = "UnitsIndicator10")]
        public string UnitsIndicator10 { get; set; }

        [Display(Name = "DiscreteInput1")]
        public string DiscreteInput1 { get; set; }

        [Display(Name = "DiscreteInput2")]
        public string DiscreteInput2 { get; set; }

        [Display(Name = "DiscreteInput3")]
        public string DiscreteInput3 { get; set; }

        [Display(Name = "DiscreteInput4")]
        public string DiscreteInput4 { get; set; }

        [Display(Name = "CRLF")]
        public string CRLF { get; set; }


        [Display(Name = "IgnStatus")]
        public string IgnStatus { get; set; }

        [Display(Name = "SensorStatus")]
        public string SensorStatus { get; set; }

        [Display(Name = "ThrottlePosition")]
        public string ThrottlePosition { get; set; }

        [Display(Name = "Baro")]
        public string Baro { get; set; }

        [Display(Name = "Airtemp")]
        public string Airtemp { get; set; }

        [Display(Name = "EcuTemp")]
        public string EcuTemp { get; set; }

        [Display(Name = "Batteryvoltage")]
        public string Batteryvoltage { get; set; }

        [Display(Name = "Sen1")]
        public string Sen1 { get; set; }

        [Display(Name = "Sen2")]
        public string Sen2 { get; set; }

        [Display(Name = "Sen3")]
        public string Sen3 { get; set; }

        [Display(Name = "Sen4")]
        public string Sen4 { get; set; }

        [Display(Name = "Sen5")]
        public string Sen5 { get; set; }

        [Display(Name = "HP")]
        public String HP { get; set; }

        public string UnitSerialNumber { get; set; }

        public string AccountNumber { get; set; }
       
    }
}
