﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class UserSubscription
    {
        public string SubscriptionName { get; set; }

        public string SubscriptionDetails { get; set; }

        public string UserName { get; set; }

        public string emailId { get; set; }

        public string SubProfileId { get; set; }

        public string Status { get; set; }

        public string ProfileId { get; set; }

        public int SNO { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PurchaseFrom { get; set; }

        public string SubId { get; set; }

        public string PaypalButtonId { get; set; }

        public bool IsExpired { get; set; }
    }
}
