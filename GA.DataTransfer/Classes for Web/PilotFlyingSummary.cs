﻿using System.ComponentModel.DataAnnotations;


namespace GA.DataTransfer
{
    public class PilotFlyingSummary
    {
        [RegularExpression("^[0-9]{1,}.[0-9]$" , ErrorMessage="Invalid Time Format")]
        public string Night { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public string Day { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public string CrossCountry { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public string Actual { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public string Sim { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public string Hood {get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public int Id { get; set; }

         [RegularExpression("^[0-9]{1,}.[0-9]$", ErrorMessage = "Invalid Time Format")]
        public int ProfileId { get; set; }
    }
}
