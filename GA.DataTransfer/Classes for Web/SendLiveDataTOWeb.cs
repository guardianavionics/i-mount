﻿using GA.CommonForParseDataFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class SendLiveDataTOWeb
    {
        public dynamic jpiData { get; set; }

        public bool IsJPI { get; set; }

        public bool IsGarmin { get; set; }

        public bool IsULP { get; set; }

        public int lastRecordFetchId { get; set; }

        public bool IsDataAvailable { get; set; }

        public string FlightNo { get; set; }

        public int CylinderCount { get; set; }
        public string STALL { get; set; }
        public string VMC { get; set; }
        public string VNE { get; set; }
        public string VX { get; set; }
        public string VY { get; set; }

        public bool IsDataAvailableForFlight { get; set; }

        public int flightTimeInterval { get; set; }

        public bool isFlightFinished { get; set; }
        public bool calibrationRight { get; set; }
        public bool calibrationLeft { get; set; }

        public List<LatLong> latLongList { get; set; }

        public string route { get; set; }

        public string wayPoint { get; set; }

        public bool isAircraftWithoutUnit { get; set; }

        public List<MFDSettings> MFDList { get; set; }

        public int[] PLHeader { get; set; }
    }
}
