﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
    public class AssemblyKitAndPartModel
    {
        public int Id { get; set; }
        public string AssemblyPN { get; set; }
        public string Nomenclature { get; set; }
        public List<PartNumberModel> PartList { get; set; }
    }
}
