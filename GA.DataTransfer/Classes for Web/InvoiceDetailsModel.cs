﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class InvoiceDetailsModel
    {
        public InvoiceModel InvoiceModel { get; set; }

        public List<UnitModel> UnitModelList { get; set; }
    }
}
