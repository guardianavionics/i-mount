﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class InappReceiptModel
    {
        public int Id { get; set; }
        public string TransactionID { get; set; }
        public System.DateTime PurchaseDate { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public string InappOriginalTransactionId { get; set; }
        public decimal Amount { get; set; }
        public string ProductId { get; set; }
        public int SNO { get; set; }
    }
}
