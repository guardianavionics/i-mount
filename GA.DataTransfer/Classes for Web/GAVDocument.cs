﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class GAVDocument
    {
        public List<GavDocsModel> documentList { get; set; }
        public List<GavDocsListItem> parentFolderList { get; set; }
    }
}
