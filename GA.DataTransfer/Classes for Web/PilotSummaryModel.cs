﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GA.DataTransfer.Classes_for_Web
{
    public class PilotSummaryModel
    {
        [Display(Name = "Last Flight On : ")]
        public string lastFlight { get; set; }

        [Display(Name = "Pilot Name")]
        public string pilotName { get; set; }

        [Display(Name = "Hours Flown Last 90 Days")]
        public string filghtFlownLast90Days { get; set; }

        [Display(Name = "Day Landing Last 90 Days")]
        public string dayLandingLast90Days { get; set; }

        [Display(Name = "Night Landing Last 90 Days")]
        public int nightLandingLast90Days { get; set; }

        [Display(Name = "Total Flight Time")]
        public string totalFlightTime { get; set; }

        [Display(Name = "BFR Valid Until")]
        public string bfrValidUnitl { get; set; }

        [Display(Name = "Medical Valid Until")]
        public string medicalValidUntil { get; set; }

        public string pilotImage { get; set; }

        public List<AircraftSummary> aircraftList { get; set; }

        [Display(Name = "Reference Number")]
        public string referenceNumber { get; set; }

        [Display(Name = "License Type")]
        public string type { get; set; }


        [Display(Name = "Medical Type")]
        public string MedicalType { get; set; }

    }
}
