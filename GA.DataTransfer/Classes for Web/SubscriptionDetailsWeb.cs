﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class SubscriptionDetailsModel
    {
        public string SubscriptionName { get; set; }

        public string SubscriptionDetails { get; set; }

        public bool IsSubscriptionPurchased { get; set; }

        public string SubscriptionStatus { get; set; }

        public string StartDate { get; set; }

        public string RenewOn { get; set; }

        public string SubProdileId { get; set; }

        public int UserSubMapID { get; set; }

        public string PurchasedFrom { get; set; }

    }
}
