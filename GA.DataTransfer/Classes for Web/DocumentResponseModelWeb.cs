﻿using GA.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Web
{
    [DataContract]
    public class DocumentResponseModelWeb : GeneralResponse
    {
        public DocumentResponseModelWeb Exception()
        {
            return new DocumentResponseModelWeb
            {
                ResponseCode = ((int)Enumerations.Logbook.Exception).ToString(),
                ResponseMessage = Enumerations.Logbook.Exception.GetStringValue(),
            };
        }

        [DataMember(Name = "listOfDocuments")]
        public List<DocumentModelWeb> ListOfDocuments { get; set; }

        [DataMember(Name = "profileId")]
        public int ProfileId { get; set; }
    }
}
