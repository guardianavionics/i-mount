﻿using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
    public class SortPilotLog
    {
        public string date { get; set; }
        public PilotLog log { get; set; }
        public string time { get; set; }
    }
}
