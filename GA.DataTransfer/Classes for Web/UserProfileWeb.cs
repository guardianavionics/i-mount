﻿using GA.DataTransfer.Classes_for_Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
//using DataAnnotationsExtensions;

namespace GA.DataTransfer
{
    public class UserProfileWeb
    {
        public List<MedicalCertificateModelWeb> MedicalCertificateList { get; set; }

        public List<LicensesModelWeb> LicensesList { get; set; }

        public List<RatingModelWeb> RatingsList { get; set; }



         [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                                  ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Email Id")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessageResourceName = "EmailIdNotFound", ErrorMessageResourceType = typeof(GAResource))]
        public string EmailId { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "FirstNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "First Name")]
        [Required(ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "FirstNameNotFound")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "LastNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Last Name")]
        [Required(ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "LastNameNotFound")]
        public string LastName { get; set; }

        [RegularExpression(@"(\+[0-9]+|[0-9]+)", ErrorMessageResourceName = "PhoneNumberNotValidMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Phone Number")]
        [StringLength(12, MinimumLength = 10)]
        //[Required(ErrorMessage = "Phone number cannot be left blank")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please select country code")]
        public string CountryCode { get; set; }

        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]//[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]//"{0:dd/MM/yyyy}"
        public string DateOfBirth { get; set; }

        [Display(Name = "Street Address")]
       // [Required(ErrorMessage = "Required")]
        public string StreetAddress { get; set; }

        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessageResourceName = "CityNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "City")]
        //[Required(ErrorMessage = "Required")]
        public string City { get; set; }

        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessageResourceName = "StateNameOnlyAlphabetsMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "State")]
        //[Required(ErrorMessage = "Required")]
        public string State { get; set; }

        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessageResourceName = "ZipCodeOnlyNumbersMessage", ErrorMessageResourceType = typeof(GAResource))]
        [Display(Name = "Zip Code")]
        //[StringLength(8, MinimumLength = 4)]
        public string ZipCode { get; set; }

        [Display(Name = "Company Name")]
       // [Required(ErrorMessageResourceName = "CompanyNameNoFound", ErrorMessageResourceType = typeof(GAResource))]
        public string CompanyName { get; set; }

        public MedicalCertificateModel medicalCertificate { get; set; }

        public LicensesModel licenses { get; set; }

        public RatingModel ratings { get; set; }

        public HttpPostedFileBase UserProfileImage { get; set; }

        public List<ListItems> MedicalCertificateClassList { get; set; }

        public int? ClasssId { get; set; }

        public string ProfileImagePath { get; set; }

        public string MedicalValidUntil { get; set; }

        public string BfrValidUnitl { get; set; }

        public string FilghtFlownLast90Days { get; set; }

        public string MedicalType { get; set; }

        public string Type { get; set; }

        public string TotalFlightTime { get; set; }

        public SubscriptionDetailsModel SubscriptionDetails { get; set; }

        public List<TransactionModel> TransactionModelList { get; set; }

        public List<InappReceiptModel> InappReceiptModelList { get; set; }

        public List<SubscriptionViewModel> SubViewModelList { get; set; }

        public List<SelectListItem> Countries { get; set; }

        public bool IsSubscriptionVisible { get; set; }
        public bool TwoFactorAuthentication { get; set; }
        
        public int Id { get; set; }
    }
}
