﻿using System.ComponentModel.DataAnnotations;


namespace GA.DataTransfer.Classes_for_Web
{
    public class UserInfo
    {
        public int ProfileId { get; set; }

        //[Display(Name = "User Name")]
        //public string UserName { get; set; }

        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Display(Name = "Options")]
        public bool IsBlock { get; set; }

        [Display(Name = "Email Id")]
        public string EmailId { get; set; }

        [Display(Name = "Manufacturer")]
        public string ComponentManufacturer { get; set; }

        [Display(Name = "TId")]
        public string TId { get; set; }

    }
}
