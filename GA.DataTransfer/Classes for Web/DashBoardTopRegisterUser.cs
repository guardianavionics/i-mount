﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class DashBoardTopRegisterUser
    {
       public int ProfileId { get; set; }

       public string Name { get; set; }

       public string EmailId { get; set; }

       public string CompanyName { get; set; }

       public string ImagePath { get; set; }
    }
}
