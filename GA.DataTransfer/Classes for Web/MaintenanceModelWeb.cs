﻿using GA.DataTransfer.Classes_for_Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class MaintenanceModelWeb
    {
        public List<MaintenanceUserModel> MaintenanceUserList { get; set; }

        public SquawkListModelWeb SquawkListModel { get; set; }
    }
}
