﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class EngDocListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public int SNO { get; set; }
    }
}
