﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace GA.DataTransfer.Classes_for_Web
{
   public class LatLong
    {

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Yaw { get; set; }
    }
}
