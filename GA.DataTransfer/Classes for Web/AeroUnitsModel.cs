﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer
{
    public class AeroUnitsModel
    {
        public List<UnlinkedAreoUnit> UnlinkedAreoUnits { get; set; }
        public List<LinkedAeroUnit> LinkedAeroUnits { get; set; }
        public List<LinkedAeroUnit> PreviouslyLinkedAeroUnits { get; set; }
    }

    public class UnlinkedAreoUnit
    {
        public string Date { get; set; }
        public string UnitSerialNumber { get; set; }
    }

    public class LinkedAeroUnit
    {
        public int Id { get; set; }
        public string AeroUnitNumber { get; set; }
        public int? AircraftId { get; set; }
        public string AircraftTelNumber { get; set; }
        public string AircraftSerialNo { get; set; }
        public string DateCreated { get; set; }
        public string ReplacedReason { get; set; }
        public string ReplacedWithUnitNumber { get; set; }
        public string Comment { get; set; }
        public DateTime? ReplacedDate { get; set; }
    }
}
