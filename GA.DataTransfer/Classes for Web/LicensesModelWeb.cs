﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class LicensesModelWeb
    {
         
        public List<RatingModelWeb> Ratingslist { get; set; }

     
        public string ReferenceNumber { get; set; }

       
        public string TId { get; set; }

      
        public string Type { get; set; }

      
        public string ValidUntil { get; set; }

       
        public long UniqueId { get; set; }
    }
}
