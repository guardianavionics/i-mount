﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public  class UnitModel : GeneralResponse
    {
        public int SNO { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string PurchaseOrderId { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Vendor { get; set; }
        public int? SerialNumber { get; set; }
        public string UnitStatus { get; set; }
        public int StatusId { get; set; }
        public string SoftwareLevel { get; set; }
        public string UnitId { get; set; }
        public string ReceivedBy { get; set; }
        public int Id { get; set; }
        public int UnitCount { get; set; }
        public int? BoardSerialNumber { get; set; }
        public bool IsSoftwareLevelApplicable { get; set; }
        public string ReasonForSalesReturn { get; set; }
        public string ModelNo { get; set; }
        public string InvoiceNumber { get; set; }
        public int RMAStatusId { get; set; }
        public string OldOrNewUnit { get; set; }

        public string NewPartNumberAssigned { get; set; }
        public string NewModelNumberAssigned { get; set; }
        public DateTime InvoiceDate { get; set;  }
    }
}
