﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class PartNumberModel
    {
        public int SNO { get; set; }
        public string PartNumber { get; set; }
        public int Id { get; set; }
        public string PartNumberId { get; set; }
        public string PartName { get; set; }
        public string Description { get; set; }
        public bool IsSoftwareLevelApplicable { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SalesPrice { get; set; }
        public int RecommendQuantity { get; set; }
        public string Comment { get; set; }
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public string ModelNumber { get; set; }
        public int TotalUnitsAvailableForSale { get; set; }
        public string ImageName { get; set; }
        public int Quantity { get; set; }

        public bool IsSerialNumberRequired { get; set; }

        public int? MasterPartId { get; set; }

        public string MasterPartNumber { get; set; }

        public bool IsAliasCreated { get; set; }

        public bool IsMasterPart { get; set; }

        public List<string> AliasList { get; set; }
        public string Alias { get; set; }
        public bool IsInActive { get; set; }
        public bool CanSold { get; set; }

    }
}
