﻿namespace GA.DataTransfer.Classes_for_Web
{
    public class DocumentModelWeb
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public string LastUpdateDate { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string FileSize { get; set; }

        public string IconUrl { get; set; }

        public bool IsForAll { get; set; }

        public bool CanDeleteDocument { get; set; }

        public int SNO { get; set; }

        public string MineType { get; set; }

        public string TId { get; set; }

    }
}
