﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class InventoryCustomerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string CustomerTypeId { get; set; }
        public string CustomerType { get; set; }
        public string EncryptId { get; set; }

        public int Id { get; set; }
        public string CompanyName { get; set; }

        public int SNO { get; set; }
 


    }
}
