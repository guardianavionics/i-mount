﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class UnitDetailedSheetModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public int? SerialNumber { get; set; }
        public int StatusId { get; set; }
        public string SoftwareLevel { get; set; }
        public string RecStatus { get; set; }
        public DateTime RecDate { get; set; }
        public string AvailableForSaleStatus { get; set; }
        public DateTime? AvailableForSaleDate { get; set; }
        public string SoldStatus { get; set; }
        public DateTime? SoldDate { get; set; }
        public int UnitCount { get; set; }
        public int RecomendedQty { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal Margin { get; set; }

    }
}
