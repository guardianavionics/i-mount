﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class GavDocsModel
    {
        public string Id { get; set; }
        public string DocumentName { get; set; }
        public bool IsFolder { get; set; }
        public string DocUrl { get; set; }
     
        public int SNO { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }

        public bool IsRootFolder { get; set; }

    }
}
