﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class PurchaseOrderModel
    {
        public int SNO { get; set; }
        public int CreatedBy { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public bool IsSoftwareLevelApplicable { get; set; }
        public string SoftwareLevel { get; set; }
        public string VendorName { get; set; }
        public string ReceivedBy { get; set; }
        public int Quantity { get; set; }
        public int StartSerialNo { get; set; }
        public int EndSerialNo { get; set; }
        public int? StartingBoardSerialNo { get; set; }
        public int? EndBoardSerialNo { get; set; }
        public int Id { get; set; }
        public string PurchaseOrderId { get; set; }
        public string PartNumberId { get; set; }
        public string VendorId { get; set; }
        public string ReceiverId { get; set; }
        public Nullable<System.DateTime> ManufactureDate { get; set; }
        public string CreateByName { get; set; }
        public System.DateTime CreateDate { get; set; }

        public string CreatedOn { get; set; }
        public List<BoardSerialAndSW> BoardNoList { get; set; }
        public bool IsEditBoardSerialNoAtReceive { get; set; }

        public bool IsSerialNumberRequired { get; set; }
    }


   
}
