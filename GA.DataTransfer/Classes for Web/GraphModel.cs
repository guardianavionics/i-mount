﻿using System.Collections.Generic;

namespace GA.DataTransfer.Classes_for_Web
{
    public class GraphModel
    {
        public List<ListItems> AircraftNNumberList { get; set; }
        public int AircraftId { get; set; }
       // public List<ListItems> flightList { get; set; }
        public List<AircraftDetail> flightList { get; set; }
        public List<DataLogListing> dataLogList { get; set; }
    }
}
