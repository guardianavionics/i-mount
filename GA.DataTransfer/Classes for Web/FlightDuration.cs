﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class FlightDuration
    {
       public int logId { get; set; }

       public string Duration { get; set; }

       public bool IsFinished { get; set; }

       public bool IsDeleted { get; set; }


    }
}
