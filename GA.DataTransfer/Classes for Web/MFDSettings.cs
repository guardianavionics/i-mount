﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace GA.DataTransfer.Classes_for_Web
{
   public class MFDSettings
    {
       public string identifier { get; set; }

       public string max { get; set; }

       public string min { get; set; }

       public List<seperator> seperatorList { get; set; }
    }


   public class seperator
   {
       public string max { get; set; }

       public string min { get; set; }

       public string color { get; set; }
   }
}
