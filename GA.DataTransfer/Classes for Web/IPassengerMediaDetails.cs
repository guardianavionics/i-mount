﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
    public class IPassengerMediaDetails : GeneralResponse
    {
        public List<MediaDetails> VideoList { get; set; }

        public List<MediaDetails> ImageList { get; set; }

        public List<MediaDetails> PDFList { get; set; }

        public List<MediaDetails> WarningList { get; set; }

        public List<MediaDetails> InstructionList { get; set; }

        
    }


    public class MediaDetails
    {
        public string TId { get; set; }
        public int Id { get; set; }

        public string FilePath { get; set; }

        public string Title { get; set; }

        public string ThumbnailImagePath { get; set; }

        public bool IsForAll { get; set; }
    }
}
