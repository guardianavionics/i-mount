﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
    public class DataIndex
    {
        public int StartIndex { get; set; }

        public int Length { get; set; }

        public string ColumnName { get; set; }
    }
}
