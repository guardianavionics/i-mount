﻿using GA.DataTransfer.Classes_for_Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class SubDetailTableModel
    {
       
        public int Id { get; set; }
       
        public string SubItemName { get; set; }
       
        public string SubDetails { get; set; }
        
        public bool Status { get; set; }
        
        public List<GA.DataTransfer.Classes_for_Services.SubscriptionDetailsModel> SubscriptionDetailList { get; set; }
        
        public List<FeatureMasterModel> FeatureIdList { get; set; }

        public List<FeatureMasterModel> SubFeatureList { get; set; }
    }
}
