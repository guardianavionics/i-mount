﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class RMAStatusMOdel
    {
        public int SNO { get; set; }
        public string Status { get; set; }

        public int StatusId { get; set; }
        public string Date { get; set; }
        public string EncryptId { get; set; }
        public string Receiver { get; set; }

        public int? UnitStatusId { get; set; }

        public string UnitStatus { get; set; }

    }
}
