﻿using System.ComponentModel.DataAnnotations;

namespace GA.DataTransfer.Classes_for_Web
{
    public class AircraftSummary
    {
        [Display(Name = "Aircraft Type")]
        public string aircraftType { get; set; }

        [Display(Name="Aircrft N-number")]
        public string aircraftNNumber { get; set; }

        [Display(Name = "Total tach time")]
        public double totalTachTime { get; set; }

        public string aircraftImage { get; set; }

        public int AircraftId { get; set; }

        public string TId { get; set; }

        public int SNO { get; set; }

        public string UnitSerialNo { get; set; }

        public int? OwnerProfileId { get; set; }


    }
}
