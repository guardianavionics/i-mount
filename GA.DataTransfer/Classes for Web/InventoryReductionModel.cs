﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class InventoryReductionModel
    {
        public int Id { get; set; }
        public int SNO { get; set; }
        public string PartNo { get; set; }
        public int Quantity { get; set; }
        public string UsedOn { get; set; }
        public string UsedBy { get; set; }
        public int UsedById { get; set; }
        public string Comment { get; set; }
        public string encId { get; set; }

        public DateTime UsedOnDatetime { get; set; }
    }
}
