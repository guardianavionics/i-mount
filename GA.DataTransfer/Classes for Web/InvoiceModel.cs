﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class InvoiceModel
    {
        public int SNO { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }

        public string InvoiceCreatedOn { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }

        public string  CustomerId { get; set; }
        public int ItemCount { get; set; }
        public string InvoiceId { get; set; }
    }
}
