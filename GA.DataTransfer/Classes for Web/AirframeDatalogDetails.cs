﻿ using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    class AirframeDatalogDetails
    {
        [Display(Name = "pilotName")]
        public string PilotName { get; set; }

        [Display(Name = "coPilotName")]
        public string CoPilotName { get; set; }

        [Display(Name = "latitude")]
        public string Latitude { get; set; }

        [Display(Name = "longitude")]
        public string Longitude { get; set; }

        [Display(Name = "speed")]
        public string Speed { get; set; }

        [Display(Name = "amp")]
        public string AMP { get; set; }

        [Display(Name = "bat")]
        public string BAT { get; set; }

        [Display(Name = "cdt")]
        public string CDT { get; set; }

        [Display(Name = "cht1")]
        public string CHT1 { get; set; }

        [Display(Name = "cht2")]
        public string CHT2 { get; set; }

        [Display(Name = "cht3")]
        public string CHT3 { get; set; }

        [Display(Name = "cht4")]
        public string CHT4 { get; set; }

        [Display(Name = "cht5")]
        public string CHT5 { get; set; }

        [Display(Name = "cht6")]
        public string CHT6 { get; set; }

        [Display(Name = "cld")]
        public string CLD { get; set; }

        [Display(Name = "egt1")]
        public string EGT1 { get; set; }

        [Display(Name = "egt2")]
        public string EGT2 { get; set; }

        [Display(Name = "egt3")]
        public string EGT3 { get; set; }

        [Display(Name = "egt4")]
        public string EGT4 { get; set; }

        [Display(Name = "egt5")]
        public string EGT5 { get; set; }

        [Display(Name = "egt6")]
        public string EGT6 { get; set; }

        [Display(Name = "end")]
        public string END { get; set; }

        [Display(Name = "ff")]
        public string FF { get; set; }

        [Display(Name = "fp")]
        public string FP { get; set; }

        [Display(Name = "fql")]
        public string FQL { get; set; }

        [Display(Name = "fqr")]
        public string FQR { get; set; }

        [Display(Name = "hp")]
        public string HP { get; set; }

        [Display(Name = "iat")]
        public string IAT { get; set; }

        [Display(Name = "map")]
        public string MAP { get; set; }

        [Display(Name = "mpg")]
        public string MPG { get; set; }

        [Display(Name = "oat")]
        public string OAT { get; set; }

        [Display(Name = "rem")]
        public string REM { get; set; }

        [Display(Name = "req")]
        public string REQ { get; set; }

        [Display(Name = "res")]
        public string RES { get; set; }

        [Display(Name = "rpm")]
        public string RPM { get; set; }

        [Display(Name = "tit")]
        public string TIT { get; set; }

        [Display(Name = "usd")]
        public string USD { get; set; }

        [Display(Name = "h:m")]
        public string HM { get; set; }

        [Display(Name = "altitude")]
        public string Altitude { get; set; }


    }
}
