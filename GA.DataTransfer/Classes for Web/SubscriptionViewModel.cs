﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class SubscriptionViewModel
    {
        public UserSubscription Subscription { get; set; }

        public List<TransactionModel> ReceiptList { get; set; }

        public List<InappReceiptModel> InAppReceiptList { get; set; }
    }
}
