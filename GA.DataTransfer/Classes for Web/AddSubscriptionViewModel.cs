﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataLayer;


namespace GA.DataTransfer.Classes_for_Web
{
   public class AddSubscriptionViewModel
    {
        public int  Id { get; set; }

        public int Sno { get; set; }

        public string Name { get; set; }

        public string Details { get; set; }

        public decimal AmountMonthly { get; set; }

        public decimal AmountYearly { get; set; }

        public int PaypalItemIdMonthly { get; set; }

        public int PaypalItemIdYearly { get; set; }

        public string PaypalButtonIdMonthly { get; set; }

        public string PaypalButtonIdYearly { get; set; }

        public string InappProductIdMonthly { get; set; }

        public string InappProductIdYearly { get; set; }

        public string TrialFrequency { get; set; }
        
        //Weekly or yearly
        public string RegularFrequency { get; set; }

        public List<Classes_for_Services.FeatureMasterModel> FeatureList { get; set; }



    }
}
