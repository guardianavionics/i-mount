﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 
using GA.DataLayer;
using GA.DataTransfer.Classes_for_Web;
using System.Runtime.Serialization;
namespace GA.DataTransfer
{

    public class VerticalSC
    {
        public int VId { get; set; }
        public string Name { get; set; }
    }



    public class ListItems
    {
        public string text { get; set; }

        public int value { get; set; }
    }


    public class TestValues
    {
        //public TestValues()
        //{
        //    //   this.Selected = false;
        //}

        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class PreferenceModelWeb
    {
        //public PreferenceModelWeb()
        //{
        //}

        public string ProfileId { get; set; }

        public int DistanceUnitId { get; set; }

        public int SpeedUnitId { get; set; }

        public int VerticalSpeedUnitId { get; set; }

        public int WeightUnitId { get; set; }

        public int FuelUnitId { get; set; }

        public string DefaultAirportIdentifier { get; set; }

        public List<ListItems> Distance { get; set; }

        public List<ListItems> Speed { get; set; }

        public List<ListItems> VerticalSpeed { get; set; }

        public List<ListItems> Fuel { get; set; }

        public List<ListItems> Weight { get; set; }

        public bool ShowCabin { get; set; }

        public bool ShowPitch { get; set; }

        public bool AudioRecording { get; set; }
        public bool SwitchToEnginForAlarm { get; set; }

        public int DisplayAltitude { get; set; }

        public int TotalRows { get; set; }

        public IEnumerable<UserEmailWeb> userEmail { get; set; }

        public int PageSize { get; set; }

        public List<ListItems> aircraftList { get; set; }

        public PilotFlyingSummary FlyingSummaryOld { get; set; }

        public PilotFlyingSummary FlyingSummaryNew { get; set; }

        public PilotFlyingSummary FlyingSummaryTotal { get; set; }



        [Required]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceName = "PasswordNotFound", ErrorMessageResourceType = typeof(GAResource))]
        [StringLength(20, MinimumLength = 6, ErrorMessageResourceName = "PasswordLengthMessage", ErrorMessageResourceType = typeof(GAResource))]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessageResourceName = "ConfirmPasswordDoNotMatchPassword", ErrorMessageResourceType = typeof(GAResource))]
        [Required(ErrorMessageResourceName = "ConfirmPasswordNotFoundMessage", ErrorMessageResourceType = typeof(GAResource))]
        public string ConfirmPassword { get; set; }

    }
}
