﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using  GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
namespace GA.DataTransfer
{
     [DataContract]
    public class AviationChartModel
    {
         [DataMember(Name = "passengerMapFileList")]
         public List<PassengerProMapFileModel> PassengerMapFileList { get; set; }

         [DataMember(Name = "MapFileList")]
         public List<MapFile> MapFileList { get; set; }

         public List<ChartListByStateModel> chartListByStateModel { get; set; }
    }


}
