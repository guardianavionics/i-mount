﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public class RMAModel
    {
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string SerialNo { get; set; }
        public string Description { get; set; }
        public string ModelNo { get; set; }
        public string Discrepancy { get; set; }
        public string DiscrepancyId { get; set; }
        public string Status { get; set; }
        public string StatusId { get; set; }
        public string Receiver { get; set; }
        public string ReceiverId { get; set; }
        public string Resolution { get; set; }
        public string ResolutionId { get; set; }
        public string TrackingNo { get; set; }
        public DateTime CreateDate { get; set; }
        public string Comments { get; set; }
        public string CreatedDate { get; set; }
        public int Id { get; set;}
        public string tId { get; set; }
        public string CompanyName { get; set; }
        public string EmailId { get; set; }
        public string PhoneNo { get; set; }
        public int UnitCount { get; set; }
        public List<UnitModel> UnitModelList { get; set; }

    }


   
}
