﻿using GA.DataTransfer.Classes_for_Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class SquawkListModelWeb
    {
       public List<ListItems> UserList { get; set; }

       public List<ListItems> aircraftList { get; set; }

       public List<IssueModel> IssueList { get; set; }

       public int aircraftIdForIssue { get; set; }

       public int userIdForIssue { get; set; }

       public bool IsOwner { get; set; }

       public string UserType { get; set; }



       
    }
}
