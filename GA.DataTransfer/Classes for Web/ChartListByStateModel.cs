﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataTransfer.Classes_for_Services;

namespace GA.DataTransfer.Classes_for_Web
{
    public class ChartListByStateModel
    {
        public string StateName { get; set; }
        public List<ChartDetailModel> ChartDetailModelList { get; set; }
    }
}
