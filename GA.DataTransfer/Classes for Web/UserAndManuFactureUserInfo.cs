﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class UserAndManuFactureUserInfo
    {
        public List<UserInfo> UserList { get; set; }

        public List<UserInfo> ManufactureruserList { get; set; }
    }
}
