﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class UserEmailWeb
    {
        public int id { get; set; }
        public string EmailId { get; set; }
        public int ProfileId { get; set; }
        public bool IsEnabled { get; set; }
        public string TempId { get; set; }
    }
}
