﻿using System.Runtime.Serialization;



namespace GA.DataTransfer.Classes_for_Web
{
    public class DocumentModel
    {
        [DataMember(Name = "registration")]
        public string Registration { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public bool Deleted { get; set; }
        public int ProfileId { get; set; }
        public bool IsForAll { get; set; }
        public Nullable<int> AircraftId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public Nullable<int> FileSize { get; set; }
    }
}
