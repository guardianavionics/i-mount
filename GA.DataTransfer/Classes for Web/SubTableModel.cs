﻿using GA.DataTransfer.Classes_for_Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public class SubTableModel
    {
        public bool IsCurrSubPurchaseByInappAndActive { get; set; }

        //If the sub is active then set the value for CurrentSubId else set to Zero
        public string CurrentPaymentDefinationId { get; set; }
        
        public List<SubDetailTableModel> SubscriptionList { get; set; }

       
        public List<FeatureMasterModel> FeatureList { get; set; }
    }
}
