﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using GA.DataLayer;

namespace GA.DataTransfer.Classes_for_Web
{
    public static class SortExtension
    {
        public static IOrderedEnumerable<TSource> OrderByWithDirection<TSource, TKey>
            (this IEnumerable<TSource> source,
             Func<TSource, TKey> keySelector,
             bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

        public static IOrderedQueryable<TSource> OrderByWithDirection<TSource, TKey>
            (this IQueryable<TSource> source,
             Expression<Func<TSource, TKey>> keySelector,
             bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

    }

    public class ModelServices : IDisposable
    {
        private readonly GuardianAvionicsEntities entities = new GuardianAvionicsEntities();

        public string[] SaveCustomer(string emailId, int profileId, bool isEnabled)
        {
            string[] msg = new string[2];
            try
            {
                string result = validateEmail(emailId, profileId);
              
                msg = result.Split(',');
                if (msg[0] != "Success")
                    return msg;

                UserEmail email = new UserEmail();
                email.EmailId = emailId;
                email.ProfileId = profileId;
                email.IsEnabled = isEnabled;

                entities.UserEmails.Add(email);

                var profile = entities.Profiles.FirstOrDefault(p => p.Id == profileId);
                if (profile != null)
                {
                    profile.LastUpdated = DateTime.UtcNow;
                }

                var preference = entities.Preferences.FirstOrDefault(p => p.ProfileId == profileId);
                if (preference != null)
                {
                    preference.LastUpdateDateForEmailList = DateTime.UtcNow;
                }

                entities.SaveChanges();
                msg[0] = "Record Save Successfully";
                msg[1] = email.id.ToString();
                return msg;


            }
            catch
            {
                msg[0] = "Error!!!";
                msg[1] = "0";
                return msg;
            }
        }


        public string validateEmail(string emailId, int profileId)
        {
            string pattern = null;
            pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            if (!Regex.IsMatch(emailId, pattern))
            {
                return "Please Enter Valid Email ID,0";
            }
            else
            {
                int count = (from e in entities.UserEmails
                             where e.EmailId == emailId && e.ProfileId == profileId
                             select e).ToList().Count();

                if (count > 0)
                {
                    return "EmailId already exists,0";
                }
            }
            return "Success,0";
        }
        public string UpdateCustomer(int id, string emailId, int profileId, bool isEnabled)
        {
            try
            {
                string pattern = null;
                pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

                if (!Regex.IsMatch(emailId, pattern))
                {
                    return GA.Common.ResourcrFiles.UserProfile.Messages.InvalidEmailId.ToString();
                }

                int count = (from e in entities.UserEmails
                             where e.EmailId == emailId && e.ProfileId == profileId && e.id != id
                             select e).ToList().Count();

                if (count > 0)
                {
                    return GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdAlreadyExist.ToString();
                }
                var email = (from tbl in entities.UserEmails
                             where tbl.id == id
                             select tbl).FirstOrDefault();
                email.EmailId = emailId;
                email.ProfileId = profileId;
                email.IsEnabled = isEnabled;

                var profile = entities.Profiles.FirstOrDefault(p => p.Id == profileId);
                if (profile != null)
                {
                    profile.LastUpdated = DateTime.UtcNow;
                }

                var preference = entities.Preferences.FirstOrDefault(p => p.ProfileId == profileId);
                if (preference != null)
                {
                    preference.LastUpdateDateForEmailList = DateTime.UtcNow;
                }

                entities.SaveChanges();
                return GA.Common.ResourcrFiles.Common.Messages.UpdateRecord.ToString();
            }
            catch
            {
                return GA.Common.ResourcrFiles.Common.Messages.ErrorWhileProcessing.ToString();
            }
        }

        public bool DeleteCustomer(int id)
        {
            try
            {
                var email = (from tbl in entities.UserEmails
                             where tbl.id == id
                             select tbl).FirstOrDefault();

                var profile = entities.Profiles.FirstOrDefault(p => p.Id == email.ProfileId);
                if (profile != null)
                {
                    profile.LastUpdated = DateTime.UtcNow;
                }

                var preference = entities.Preferences.FirstOrDefault(p => p.ProfileId == email.ProfileId);
                if (preference != null)
                {
                    preference.LastUpdateDateForEmailList = DateTime.UtcNow;
                }
                entities.UserEmails.Remove(email);
                entities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //For Custom Paging
        public IEnumerable<UserEmail> GetCustomerPage( int profileId)
        {
           
                return entities.UserEmails.Where(p => p.ProfileId == profileId).ToList();
        }
        public int CountCustomer()
        {
            return entities.UserEmails.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

    }

    public class PagedCustomerModel
    {
        public int ProfileId { get; set; }
        public int TotalRows { get; set; }
        public IEnumerable<UserEmail> userEmail { get; set; }
        public int PageSize { get; set; }
    }
}