﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GA.DataTransfer
{
    [DataContract]
    public class DataLogListing
    {
        [Display(Name = "Lcl Date")]
        public string Date { get; set; }

        [Display(Name = "Lcl Time")]
        public string Time { get; set; }


        public long TickTime { get; set; }

        //[Display(Name ="UTCOfst")]
        //public String utcOfst {get;set;}

        //[Display(Name ="AltB")]
        //public String AltB {get;set;}

        //[Display(Name ="BaroA")]
        //public String BaroA{get;set;}

        //[Display(Name ="AltMSL")]
        //public String AltMSL{get;set;}

        //[Display(Name ="GndSpd")]
        //public String GndSpd{get;set;}

        //gunjan
        [Display(Name = "FPosition")]
        public string FPosition { get; set; }

        [Display(Name = "FQLeft")]
        public string FQleft { get; set; }


        [Display(Name = "FQtyRight")]
        public string FQRight { get; set; }

        //gunjan

        [Display(Name = "VSpd")]
        public String VSpd { get; set; }

        [Display(Name = "VSpdColor")]
        public String VSpdColor { get; set; }

        [Display(Name = "Pitch")]
        public String Pitch { get; set; }

        [Display(Name = "PitchColor")]
        public String PitchColor { get; set; }

        [Display(Name = "Roll")]
        public String Roll { get; set; }

        [Display(Name = "RollColor")]
        public String RollColor { get; set; }

        [Display(Name = "Yaw")]
        public string Yaw { get; set; }

        [Display(Name = "YawColor")]
        public string YawColor { get; set; }

        //[Display(Name ="LatAc")]
        //public String LatAc{get;set;}

        //[Display(Name ="NormAc")]
        //public String NormAc{get;set;}

        //[Display(Name ="HDG")]
        //public String HDG{get;set;}

        //[Display(Name ="TRK")]
        //public String TRK{get;set;}

        //[Display(Name ="TAS")]
        //public String TAS{get;set;}

        //[Display(Name ="HSIS")]
        //public String HSIS{get;set;}

        //[Display(Name ="CRS")]
        //public String CRS{get;set;}

        //[Display(Name ="NAV1")]
        //public String NAV1{get;set;}

        //[Display(Name ="NAV2")]
        //public String NAV2{get;set;}

        //[Display(Name ="COM1")]
        //public String COM1{get;set;}

        //[Display(Name ="COM2")]
        //public String COM2{get;set;}

        //[Display(Name ="HCDI")]
        //public String HCDI{get;set;}

        //[Display(Name ="VCDI")]
        //public String VCDI{get;set;}

        //[Display(Name ="WndSpd")]
        //public String WndSpd{get;set;}

        //[Display(Name ="WndDr")]
        //public String WndDr{get;set;}

        //[Display(Name ="WptDst")]
        //public String WptDst{get;set;}

        //[Display(Name ="WptBrg")]
        //public String WptBrg{get;set;}

        //[Display(Name ="MagVar")]
        //public String MagVar{get;set;}

        //[Display(Name ="AfcsOn")]
        //public String AfcsOn{get;set;}

        //[Display(Name ="RollM")]
        //public String RollM{get;set;}

        //[Display(Name ="PitchM")]
        //public String PitchM{get;set;}

        //[Display(Name ="RollC")]
        //public String RollC{get;set;}

        //[Display(Name ="VSpdG")]
        //public String VSpdG{get;set;}

        //[Display(Name ="PichC")]
        //public String PichC {get;set;}

        //[Display(Name ="GPSfix")]
        //public String GPSfix{get;set;}

        //[Display(Name ="HAL")]
        //public String HAL{get;set;}

        //[Display(Name ="VAL")]
        //public String VAL{get;set;}

        //[Display(Name ="HPLwas")]
        //public String HPLwas{get;set;}

        //[Display(Name ="HPLfd")]
        //public String HPLfd{get;set;}

        //[Display(Name ="VPLwas")]
        //public String VPLwas{get;set;}

        //[Display(Name ="Volt2")]
        //public String Volt2{get;set;}

        //[Display(Name ="AMP2")]
        //public String AMP2 { get; set; }

        [Display(Name = "Latitude")]
        public string Latitude { get; set; }

        [Display(Name = "LatitudeColor")]
        public string LatitudeColor { get; set; }

        [Display(Name = "Longitude")]
        public string Longitude { get; set; }

        [Display(Name = "LongitudeColor")]
        public string LongitudeColor { get; set; }

        //[Display(Name = "WayPoint")]
        //public string WayPoint { get; set; }

        //[Display(Name = "AltGPS")]
        //public string GpsAltitude { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "AircraftNNumber")]
        public string AircraftNNumber { get; set; }

        [Display(Name = "Serial Number")]
        public string UnitSerialNumber { get; set; }

        //[Display(Name = "IAS")]
        //public string IAS { get; set; }

        //[Display(Name = "Volt")]
        //public string Volt1 { get; set; }

        //[Display(Name = "AMP1")]
        //public string AMP1 { get; set; }

        [Display(Name = "FQtyL")]
        public string FQL { get; set; }

        [Display(Name = "FQtyLColor")]
        public string FQLColor { get; set; }

        [Display(Name = "FQR")]
        public string FQR { get; set; }


        [Display(Name = "FQRColor")]
        public string FQRColor { get; set; }

        [Display(Name = "FUEL-F")]
        public string FF { get; set; }

        [Display(Name = "FUEL-FColor")]
        public string FFColor { get; set; }

        [Display(Name = "OIL-P")]
        public string OILP { get; set; }

        [Display(Name = "OIL-PColor")]
        public string OILPColor { get; set; }

        [Display(Name = "E1 MAP")]
        public string MAP { get; set; }

        [Display(Name = "E1 MAPColor")]
        public string MAPColor { get; set; }


        [Display(Name = "E1 RPM")]
        public string RPM { get; set; }

        [Display(Name = "E1 RPMColor")]
        public string RPMColor { get; set; }

        [Display(Name = "E1 CHT1")]
        public string Cht1 { get; set; }

        [Display(Name = "E1 CHT1Color")]
        public string Cht1Color { get; set; }

        [Display(Name = "E1 CHT2")]
        public string Cht2 { get; set; }

        [Display(Name = "E1 CHT2Color")]
        public string Cht2Color { get; set; }

        [Display(Name = "E1 CHT3")]
        public string Cht3 { get; set; }

        [Display(Name = "E1 CHT3Color")]
        public string Cht3Color { get; set; }

        [Display(Name = "E1 CHT4")]
        public string Cht4 { get; set; }

        [Display(Name = "E1 CHT4Color")]
        public string Cht4Color { get; set; }

        [Display(Name = "E1 CHT5")]
        public string Cht5 { get; set; }

        [Display(Name = "E1 CHT5Color")]
        public string Cht5Color { get; set; }

        [Display(Name = "E1 CHT6")]
        public string Cht6 { get; set; }

        [Display(Name = "E1 CHT6Color")]
        public string Cht6Color { get; set; }

        [Display(Name = "E1 EGT1")]
        public string Egt1 { get; set; }

        [Display(Name = "E1 EGT1Color")]
        public string Egt1Color { get; set; }

        [Display(Name = "E1 EGT2")]
        public string Egt2 { get; set; }

        [Display(Name = "E1 EGT2Color")]
        public string Egt2Color { get; set; }

        [Display(Name = "E1 EGT3")]
        public string Egt3 { get; set; }

        [Display(Name = "E1 EGT3Color")]
        public string Egt3Color { get; set; }

        [Display(Name = "E1 EGT4")]
        public string Egt4 { get; set; }

        [Display(Name = "E1 EGT4Color")]
        public string Egt4Color { get; set; }

        [Display(Name = "E1 EGT5")]
        public string Egt5 { get; set; }

        [Display(Name = "E1 EGT5Color")]
        public string Egt5Color { get; set; }

        [Display(Name = "E1 EGT6")]
        public string Egt6 { get; set; }

        [Display(Name = "E1 EGT6Color")]
        public string Egt6Color { get; set; }

        [Display(Name = "E1 TIT")]
        public string Tit { get; set; }

        [Display(Name = "E1 TITColor")]
        public string TitColor { get; set; }

        [Display(Name = "OAT-C")]
        public string Oat { get; set; }

        [Display(Name = "OAT-C-IsVisible")]
        public bool Oat_IsVisible { get; set; }

        [Display(Name = "OAT-CColor")]
        public string OatColor { get; set; }

        [Display(Name = "Flight Detail")]
        public string Text { get; set; }

        public int pilotLogId { get; set; }

        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; }

        //[Display(Name="Unit Serial Number")]
        //public string UnitSerialNumber { get; set; }



        [Display(Name = "AMP")]
        public string AMP { get; set; }

        [Display(Name = "AMPColor")]
        public string AMPColor { get; set; }

        [Display(Name = "AMP2")]
        public string AMP2 { get; set; }

        [Display(Name = "AMP2Color")]
        public string AMP2Color { get; set; }

        //[Display(Name = "BAT")]
        //public string BAT { get; set; }

        [Display(Name = "CDT")]
        public string CDT { get; set; }

        [Display(Name = "CDTColor")]
        public string CDTColor { get; set; }

        [Display(Name = "CLD")]
        public string CLD { get; set; }

        [Display(Name = "CLDColor")]
        public string CLDColor { get; set; }

        [Display(Name = "END")]
        public string END { get; set; }

        [Display(Name = "ENDColor")]
        public string ENDColor { get; set; }

        [Display(Name = "FP")]
        public string FP { get; set; }

        [Display(Name = "FPColor")]
        public string FPColor { get; set; }

        [Display(Name = "HP")]
        public string HP { get; set; }

        [Display(Name = "HPColor")]
        public string HPColor { get; set; }

        [Display(Name = "IAT")]
        public string IAT { get; set; }

        [Display(Name = "IATColor")]
        public string IATColor { get; set; }

        [Display(Name = "MPG")]
        public string MPG { get; set; }

        [Display(Name = "MPGColor")]
        public string MPGColor { get; set; }

        [Display(Name = "WP REM")]
        public string REM { get; set; }

        [Display(Name = "WP REMColor")]
        public string REMColor { get; set; }

        [Display(Name = "WP REQ")]
        public string REQ { get; set; }

        [Display(Name = "WP REQColor")]
        public string REQColor { get; set; }

        [Display(Name = "RES")]
        public string RES { get; set; }

        [Display(Name = "RESColor")]
        public string RESColor { get; set; }

        [Display(Name = "USD")]
        public string USD { get; set; }

        [Display(Name = "USDColor")]
        public string USDColor { get; set; }

        [Display(Name = "H:M")]
        public string HM { get; set; }

        [Display(Name = "H:MColor")]
        public string HMColor { get; set; }

        [Display(Name = "ALTITUDE")]
        public string Altitude { get; set; }

        [Display(Name = "ALTITUDEColor")]
        public string AltitudeColor { get; set; }

        [Display(Name = "SPEED")]
        public string Speed { get; set; }

        [Display(Name = "SPEEDColor")]
        public string SpeedColor { get; set; }

        [Display(Name = "Oil-T")]
        public string OILT { get; set; }

        [Display(Name = "Oil-TColor")]
        public string OILTColor { get; set; }

        [Display(Name = "OP")]
        public string OP { get; set; }

        [Display(Name = "OPColor")]
        public string OPColor { get; set; }

        [Display(Name = "OT")]
        public string OT { get; set; }

        [Display(Name = "OTColor")]
        public string OTColor { get; set; }

        [Display(Name = "TIT-R")]
        public string TIT2 { get; set; }

        [Display(Name = "TIT-RColor")]
        public string TIT2Color { get; set; }

        [Display(Name = "TIT-L")]
        public string TIT1 { get; set; }

        [Display(Name = "TIT-LColor")]
        public string TIT1Color { get; set; }

        [Display(Name = "FUEL-F")]
        public string FUELF { get; set; }

        [Display(Name = "FUEL-FColor")]
        public string FUELFColor { get; set; }

        [Display(Name = "LAT")]
        public string LAT { get; set; }

        [Display(Name = "LATColor")]
        public string LATColor { get; set; }

        [Display(Name = "LNG")]
        public string LNG { get; set; }

        [Display(Name = "LNGColor")]
        public string LNGColor { get; set; }

        [Display(Name = "ALT")]
        public string ALT { get; set; }

        [Display(Name = "ALTColor")]
        public string ALTColor { get; set; }

        [Display(Name = "SPD")]
        public string SPD { get; set; }

        [Display(Name = "SPDColor")]
        public string SPDColor { get; set; }

        [Display(Name = "VOLTS")]
        public string VOLTS { get; set; }

        [Display(Name = "VOLTSColor")]
        public string VOLTSColor { get; set; }

        [Display(Name = "VOLTS2")]
        public string VOLTS2 { get; set; }

        [Display(Name = "VOLTS2Color")]
        public string VOLTS2Color { get; set; }

        [Display(Name = "GPH")]
        public string GPH { get; set; }

        [Display(Name = "GPHColor")]
        public string GPHColor { get; set; }

        [Display(Name = "ECON")]
        public string ECON { get; set; }

        [Display(Name = "ECONColor")]
        public string ECONColor { get; set; }

        [Display(Name = "SerialNo")]
        public string AircraftSerialNo { get; set; }

        [Display(Name = "Model")]
        public string AircraftModelNo { get; set; }

        [Display(Name = "CalculatedFuelRemaining")]
        public string CalculatedFuelRemaining { get; set; }

        [Display(Name = "CalculatedFuelRemainingColor")]
        public string CalculatedFuelRemainingColor { get; set; }

        [Display(Name = "TotalAircraftTime")]
        public string TotalAircraftTime { get; set; }

        [Display(Name = "TotalAircraftTimeColor")]
        public string TotalAircraftTimeColor { get; set; }

        [Display(Name = "EngineTime")]
        public string EngineTime { get; set; }

        [Display(Name = "EngineTimeColor")]
        public string EngineTimeColor { get; set; }

        [Display(Name = "ElevatorTrimPosition")]
        public string ElevatorTrimPosition { get; set; }

        [Display(Name = "ElevatorTrimPositionColor")]
        public string ElevatorTrimPositionColor { get; set; }

        [Display(Name = "UnitsIndicator")]
        public string UnitsIndicator { get; set; }

        [Display(Name = "UnitsIndicatorColor")]
        public string UnitsIndicatorColor { get; set; }

        [Display(Name = "FlapPosition")]
        public string FlapPosition { get; set; }

        [Display(Name = "FlapPositionColor")]
        public string FlapPositionColor { get; set; }

        [Display(Name = "UnitsIndicator2")]
        public string UnitsIndicator2 { get; set; }

        [Display(Name = "UnitsIndicator2Color")]
        public string UnitsIndicator2Color { get; set; }

        [Display(Name = "CarbTemp")]
        public string CarbTemp { get; set; }

        [Display(Name = "CarbTempColor")]
        public string CarbTempColor { get; set; }

        [Display(Name = "UnitsIndicator3")]
        public string UnitsIndicator3 { get; set; }

        [Display(Name = "UnitsIndicator3Color")]
        public string UnitsIndicator3Color { get; set; }

        [Display(Name = "CoolantPressure")]
        public string CoolantPressure { get; set; }

        [Display(Name = "CoolantPressureColor")]
        public string CoolantPressureColor { get; set; }

        [Display(Name = "UnitsIndicator4")]
        public string UnitsIndicator4 { get; set; }

        [Display(Name = "UnitsIndicator4Color")]
        public string UnitsIndicator4Color { get; set; }

        [Display(Name = "CoolantTemperature")]
        public string CoolantTemperature { get; set; }

        [Display(Name = "CoolantTemperatureColor")]
        public string CoolantTemperatureColor { get; set; }

        [Display(Name = "UnitsIndicator5")]
        public string UnitsIndicator5 { get; set; }

        [Display(Name = "UnitsIndicator5Color")]
        public string UnitsIndicator5Color { get; set; }

        [Display(Name = "UnitsIndicator6")]
        public string UnitsIndicator6 { get; set; }

        [Display(Name = "UnitsIndicator6Color")]
        public string UnitsIndicator6Color { get; set; }

        [Display(Name = "AileronTrimPosition")]
        public string AileronTrimPosition { get; set; }

        [Display(Name = "AileronTrimPositionColor")]
        public string AileronTrimPositionColor { get; set; }

        [Display(Name = "UnitsIndicator7")]
        public string UnitsIndicator7 { get; set; }

        [Display(Name = "UnitsIndicator7Color")]
        public string UnitsIndicator7Color { get; set; }

        [Display(Name = "RudderTrimPosition")]
        public string RubberTrimPosition { get; set; }

        [Display(Name = "RudderTrimPositionColor")]
        public string RubberTrimPositionColor { get; set; }

        [Display(Name = "UnitsIndicator8")]
        public string UnitsIndicator8 { get; set; }

        [Display(Name = "UnitsIndicator8Color")]
        public string UnitsIndicator8Color { get; set; }

        [Display(Name = "FuelQty3")]
        public string FuelQty3 { get; set; }

        [Display(Name = "FuelQty3Color")]
        public string FuelQty3Color { get; set; }

        [Display(Name = "UnitsIndicator9")]
        public string UnitsIndicator9 { get; set; }

        [Display(Name = "UnitsIndicator9Color")]
        public string UnitsIndicator9Color { get; set; }

        [Display(Name = "FuelQty4")]
        public string FuelQty4 { get; set; }

        [Display(Name = "FuelQty4Color")]
        public string FuelQty4Color { get; set; }

        [Display(Name = "UnitsIndicator10")]
        public string UnitsIndicator10 { get; set; }

        [Display(Name = "UnitsIndicator10Color")]
        public string UnitsIndicator10Color { get; set; }

        [Display(Name = "DiscreteInput1")]
        public string DiscreteInput1 { get; set; }

        [Display(Name = "DiscreteInput1Color")]
        public string DiscreteInput1Color { get; set; }

        [Display(Name = "DiscreteInput2")]
        public string DiscreteInput2 { get; set; }

        [Display(Name = "DiscreteInput2Color")]
        public string DiscreteInput2color { get; set; }

        [Display(Name = "DiscreteInput3")]
        public string DiscreteInput3 { get; set; }

        [Display(Name = "DiscreteInput3Color")]
        public string DiscreteInput3Color { get; set; }

        [Display(Name = "DiscreteInput4")]
        public string DiscreteInput4 { get; set; }

        [Display(Name = "DiscreteInput4Color")]
        public string DiscreteInput4Color { get; set; }

        [Display(Name = "CRLF")]
        public string CRLF { get; set; }

        [Display(Name = "CRLFColor")]
        public string CRLFColor { get; set; }

        [Display(Name = "IgnStatus")]
        public string IgnStatus { get; set; }

        [Display(Name = "IgnStatusColor")]
        public string IgnStatusColor { get; set; }

        [Display(Name = "SensorStatus")]
        public string SensorStatus { get; set; }

        [Display(Name = "SensorStatusColor")]
        public string SensorStatusColor { get; set; }

        [Display(Name = "ThrottlePosition")]
        public string ThrottlePosition { get; set; }

        [Display(Name = "ThrottlePositionColor")]
        public string ThrottlePositionColor { get; set; }

        [Display(Name = "Baro")]
        public string Baro { get; set; }

        [Display(Name = "BaroColor")]
        public string BaroColor { get; set; }

        [Display(Name = "Airtemp")]
        public string Airtemp { get; set; }

        [Display(Name = "AirtempColor")]
        public string AirtempColor { get; set; }

        [Display(Name = "EcuTemp")]
        public string EcuTemp { get; set; }

        [Display(Name = "EcuTempColor")]
        public string EcuTempColor { get; set; }

        [Display(Name = "Batteryvoltage")]
        public string Batteryvoltage { get; set; }

        [Display(Name = "BatteryvoltageColor")]
        public string BatteryvoltageColor { get; set; }

        [Display(Name = "Sen1")]
        public string Sen1 { get; set; }

        [Display(Name = "Sen1Color")]
        public string Sen1Color { get; set; }

        [Display(Name = "Sen2")]
        public string Sen2 { get; set; }

        [Display(Name = "Sen2Color")]
        public string Sen2Color { get; set; }

        [Display(Name = "Sen3")]
        public string Sen3 { get; set; }

        [Display(Name = "Sen3Color")]
        public string Sen3Color { get; set; }

        [Display(Name = "Sen4")]
        public string Sen4 { get; set; }

        [Display(Name = "Sen4Color")]
        public string Sen4Color { get; set; }

        [Display(Name = "Sen5")]
        public string Sen5 { get; set; }

        [Display(Name = "Sen5Color")]
        public string Sen5Color { get; set; }

        [Display(Name = "FlightNumber")]
        public string FlightNumber { get; set; }

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Id")]
        public string TId { get; set; }

        [Display(Name = "IsSecData")]
        public string IsSecData { get; set; }

        [Display(Name = "HB")]
        public string HB { get; set; }

        [Display(Name = "ComActive")]
        public string ComActive { get; set; }
        [Display(Name = "ComStby")]
        public string ComStby { get; set; }
        [Display(Name = "NavActive")]
        public string NavActive { get; set; }

        [Display(Name = "NavStby")]
        public string NavStby { get; set; }

        [Display(Name = "LA")]
        public string LA { get; set; }
        [Display(Name = "VA")]
        public string VA { get; set; }
        [Display(Name = "ALTM")]
        public string ALTM { get; set; }
        [Display(Name = "VSI")]
        public string VSI { get; set; }
    }
}