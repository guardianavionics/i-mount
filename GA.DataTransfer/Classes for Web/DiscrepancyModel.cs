﻿ 

namespace GA.DataTransfer.Classes_for_Web
{
  public  class DiscrepancyModel
    {
        public int SNO { get; set; }
        public string DiscrepancyDetail { get; set; }
        public int Id { get; set; }
        public string EncryptId { get; set; }
    }
}
