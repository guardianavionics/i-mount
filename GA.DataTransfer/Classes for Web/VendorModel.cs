﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
   public  class VendorModel
    {
        public int SNO { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public string EncryptId { get; set; }
        public string ContactName { get; set; }
        public string ContactLastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Comments { get; set; }
        public string Website { get; set; }
        public string WebUrlPrefix { get; set; }



    }
}
