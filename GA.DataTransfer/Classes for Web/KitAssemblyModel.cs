﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GA.DataTransfer.Classes_for_Web
{
   public class KitAssemblyModel
    {
        public int Id { get; set; }
        public int PartId { get; set; }
        public string PartName { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public decimal SuggestedSalesPrice { get; set; }
        public List<PartNumberModel> PartList { get; set; }
        public HttpPostedFile File { get; set; }

        public string Comment { get; set; }
    }
}
