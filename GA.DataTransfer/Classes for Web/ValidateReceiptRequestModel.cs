﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace GA.DataTransfer.Classes_for_Web
{
    [DataContract]
   public class ValidateReceiptRequestModel
    {
        [DataMember(Name = "receiptdata")]
        public string ReceiptData { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
