﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer.Classes_for_Web
{
    public class QuickBloxTokenResponse
    {
        public GenerateToken Session { get; set; }
    }

    public class GenerateToken
    {       
        public string Token { get; set; }
    }
}
