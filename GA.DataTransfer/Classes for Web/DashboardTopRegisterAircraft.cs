﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
  public  class DashboardTopRegisterAircraft
    {
        public int AircraftId { get; set; }

        public string TailNo { get; set; }

        public string SerialNo { get; set; }

        public string OwnerName { get; set; }

        public string ImagePath { get; set; }
    }
}
