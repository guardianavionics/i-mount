﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
    public class DashboardModel
    {

        public int TotalUserRegistered { get; set; }

        public int TotalAircraftRegistered { get; set; }

        public int TotalFlights { get; set; }

        public int TotalUnits { get; set; }

        public int TotalManufacturerUser { get; set; }

        public string LastRegisterUserName { get; set; }

        public string LastRegisterUserEmailId { get; set; }

        public string LastRegisterUserCompanyName { get; set; }

        public string LastRegisterAircraftTailNo { get; set; }

        public string LastRegisterAircraftMake { get; set; }

        public string LastRegisterAircraftModel { get; set; }

        public string LastFlightByAircraft { get; set; }

        public string LastFlightByPilot { get; set; }

        public string LastFlightDuration { get; set; }

        public int TotalLinkedUnit { get; set; }

        public int TotalUnLinkedUnit { get; set; }

        public string LastUnitLinked { get; set; }

        public string LastRegisterManuUserName { get; set; }

        public string LastRegisterManuUserEmailId { get; set; }

        public string LastRegisterManuUserCompanyName { get; set; }

        public List<DashBoardTopRegisterUser> TopRegisterUserList { get; set; }

        public List<DashboardTopRegisterAircraft> TopRegisterAircraftList { get; set; }

        public List<DashBoardLastFlights> LastFlightsList { get; set; }

    }
}
