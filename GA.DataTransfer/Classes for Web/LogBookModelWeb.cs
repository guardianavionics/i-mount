﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using PagedList;
//using PagedList.Mvc;

namespace GA.DataTransfer
{
    public class LogBookModelWeb
    {
        public int SNO { get; set; }

        [Display(Name = "Flight Detail")]
        public string Text { get; set; }

        public int Id { get; set; }

        public string  TId { get; set; }
       

        public int? ProfileId { get; set; }

        //[Editable(false)]
        [Required]
        [Display(Name = "Actual")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        public string Actual { get; set; }

        [Display(Name = "CoPilot")]
        public string CoPilot { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [Display(Name = "Cross Country")]
        public string CrossCountry { get; set; }

        [Required]
        [Display(Name = "Date")]
        public string Date { get; set; }

        //[RegularExpression(@"^([0-9][0-9][0-9]:[0-5][0-9]:[0-5][0-9]|[0-9][0-9]:[0-5][0-9]:[0-5][0-9])",
        //    ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [Required]
        [Display(Name = "Day")]
        public string DayPIC { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [Display(Name = "Hood")]
        public string Hood { get; set; }

        //[RegularExpression(@"^([1-9][0-9][0-9]:[0-5][0-9]|[1-9][0-9]:[0-5][0-9]|[0-9]:[0-5][0-9]|[1-9][0-9]|[1-9])", ErrorMessage = "Format of Time is incorrect.")]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [Display(Name = "Night")]
        public string NightPIC { get; set; }

        [Display(Name = "Remark")]
        public string Remark { get; set; }

       
        [Display(Name = "Route")]
        public string Route { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+(\.[0-9])?$", ErrorMessageResourceType = typeof(GAResource), ErrorMessageResourceName = "TimeFormatMessage")]
        [Display(Name = "Sim")]
        public string Sim { get; set; }

        [Display(Name = "Finished")]
        public bool Finished { get; set; }

        public string IFRAppchs { get; set; }

        [Display(Name = "Model")]
        public string AircraftModel { get; set; }

        [Display(Name = "Tail Number")]
        public string AircraftRegistration { get; set; }

        // route , from and to 
        [Display(Name = "From")]
        public string DepartureFrom { get; set; }

        [Display(Name = "To")]
        public string ArivalTo { get; set; }

        [Display(Name = "Play Audio")]
        public string AudioFileName { get; set; }

        //public int? SearchResults1Page { get; set; }
        //public int? SearchResults2Page { get; set; }
        //public IPagedList<string> SearchResults1 { get; set; }
        //public IPagedList<string> SearchResults2 { get; set; }

        [Display(Name = "IFR Appchs")]
        public int IfrAppchs1 { get; set; }

        public List<ListItems> IfrAppchsList { get; set; }

        public bool IsDataPresentForFlight { get; set; }

        //public int orderIndex { get; set; }

        [Display(Name = "Time")]
        public string Time { get; set; }

        [Display(Name = "CoPilotId")]
        public int? CoPilotId { get; set; }

        [Display(Name = "CoPilotEmailId")]
        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                             @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                             ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]
        public string CoPilotEmailId { get; set; }

        [Display(Name = "PilotEmailId")]
        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                             @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$",
                                             ErrorMessageResourceName = "EmailIdInWrongFormat", ErrorMessageResourceType = typeof(GAResource))]
        public string PilotEmailId { get; set; }

        public int FlightId { get; set; }

        public string TotalFlightDuration { get; set; }

        public string FlightDataType { get; set; }

        public long? UniqeId { get; set; }

        public int AircraftId { get; set; }

    }
}
