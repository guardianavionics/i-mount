﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA.DataTransfer.Classes_for_Web
{
   public  class DashBoardLastFlights
    {
        public int LogId { get; set; }

        public string AircraftTailNo { get; set; }

        public string PilotName { get; set; }

        public string Duration { get; set; }

        public string ImagePath { get; set; }

    }
}
