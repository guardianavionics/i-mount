﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer
{
    [DataContract]
    public  class AirframeDataLogRequestModel
    {
        
            [DataMember(Name = "aircraftId")]
            public int AircraftId { get; set; }

            [DataMember(Name = "flightId")]
            public int FlightId  { get; set; }

            [DataMember(Name = "pilotId")]
            public int PilotId { get; set; }

            [DataMember(Name = "pilotLogId")]
            public int PilotLogId { get; set; }
        
    }
}
