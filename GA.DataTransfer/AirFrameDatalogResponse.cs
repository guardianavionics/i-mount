﻿using GA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GA.DataTransfer
{
    [DataContract]
    public class AirFrameDatalogResponse:GeneralResponse
    {
        [DataMember(Name = "airFrameDataLogs")]
        public List<AirFrameLogValue> AirFrameDataLogs { get; set; }
    }
    public class AirFrameLogValue
    {
        [DataMember(Name = "latitude")]
        public double Latitude { get; set; }

        [DataMember(Name = "longitude")]
        public double Longitude { get; set; }

        [DataMember(Name = "speed")]
        public double Speed { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "altitude")]
        public double Altitude { get; set; }

        [DataMember(Name = "heading")]
        public double Heading { get; set; }
    }
}
