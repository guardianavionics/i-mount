﻿using GA.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization;
using TestApp.Models;
using System.Net;
using System.IO;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace TestApp.Controllers
{
  
    [RoutePrefix("api/Home")]
    public class HomeController : System.Web.Http.ApiController
    {
       
        [HttpPost]
        [Route("InappCallback")]
        public HttpResponseMessage InappCallback([System.Web.Http.FromBody] dynamic inappResponse )
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            string inappResponseStr = Newtonsoft.Json.JsonConvert.SerializeObject(inappResponse);
            ExceptionHandler.ReportError(new Exception(), "InappReceiptNotification API call Live");
            ExceptionHandler.ReportError(new Exception(), inappResponseStr);
            string URL = "http://test.guardianavionicsdata.com/Home/InAppNotificationCallback";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = inappResponseStr.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(inappResponseStr);
            }
            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = responseReader.ReadToEnd();

                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            return httpResponse;
        }


        [HttpPost]
        [Route("InappCallbackLive")]
        public HttpResponseMessage InappCallbackLive([System.Web.Http.FromBody] dynamic inappResponse)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            string inappResponseStr = Newtonsoft.Json.JsonConvert.SerializeObject(inappResponse);
            ExceptionHandler.ReportError(new Exception(), "InappReceiptNotification API call Live");
            ExceptionHandler.ReportError(new Exception(), inappResponseStr);
            string URL = "https://guardianavionicsdata.com/Home/InAppNotificationCallback";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = inappResponseStr.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(inappResponseStr);
            }
            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = responseReader.ReadToEnd();

                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            return httpResponse;
        }


    }
}
