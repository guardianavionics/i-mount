﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;


namespace TestApp.Controllers
{
    public class LogActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            Log(actionExecutedContext.ActionContext.RequestContext.RouteData);

            base.OnActionExecuted(actionExecutedContext);
        }

        private void Log(System.Web.Http.Routing.IHttpRouteData httpRouteData)
        {
            var controllerName = "Home";
            var actionName = "Index";
            var message = String.Format("controller:{0}, action:{1}", controllerName, actionName);

            Debug.WriteLine(message, "Action Filter Log");
        }
    }
}