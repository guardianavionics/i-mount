﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace TestApp.Models
{
    [DataContract]
    public class ReceiptInfo
    {
        public int Id { get; set; }
        [DataMember(Name = "quantity")]
        public Nullable<int> Quantity { get; set; }

        [DataMember(Name = "product_id")]
        public string Product_Id { get; set; }

        [DataMember(Name = "transaction_id")]
        public string Transaction_Id { get; set; }

        [DataMember(Name = "original_transaction_id")]
        public string Original_Transaction_Id { get; set; }

        [DataMember(Name = "purchase_date")]
        public string Purchase_Date { get; set; }

        [DataMember(Name = "original_purchase_date")]
        public string Original_Purchase_Date { get; set; }

        [DataMember(Name = "expires_date")]
        public string Expires_Date { get; set; }

        [DataMember(Name = "web_order_line_item_id")]
        public string Web_Order_Line_Item_Id { get; set; }

        [DataMember(Name = "is_trial_period")]
        public Nullable<bool> Is_Trial_Period { get; set; }
    }
}