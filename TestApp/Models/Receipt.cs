﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace TestApp.Models
{
    public class Receipt
    {
        public string environment { get; set; }

        public string notification_type { get; set; }

        public string password { get; set; }

        public string original_transaction_id { get; set; }

        public string cancellation_date { get; set; }

        public ReceiptInfo latest_receipt_info { get; set; }

        public string auto_renew_status   { get; set; }

        public string expiration_intent { get; set; }
    }
}