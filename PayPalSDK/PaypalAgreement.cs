﻿using GA.Common;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayPalSDK
{
    public class PaypalAgreement
    {

        public void CancelAgreement(string subProfileId, string remark)
        {
            try
            {
                var apiContext = ConfigSetting.GetAPIContext();
                Agreement.Cancel(apiContext, subProfileId, new AgreementStateDescriptor { note = remark });
            }
            catch (Exception ex)
            {
            }
        }

        public bool ReActivateAgreement(string subProfileId, string remark)
        {
            try
            {
                var apiContext = ConfigSetting.GetAPIContext();
                Agreement.ReActivate(apiContext, subProfileId, new AgreementStateDescriptor { note = remark });
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public void RefundPAypal(string subProfileId,string transactionId, string refundAmount )
        {
            ExceptionHandler.ReportError(new Exception(), "RefundPAypal call");
            refundAmount = Convert.ToString(Math.Round(Convert.ToDecimal(refundAmount), 2));
            ExceptionHandler.ReportError(new Exception(), "refundAmount = " + refundAmount);
            try
            {
                var refund = new Refund()
                {
                    amount = new Amount()
                    {
                        currency = "USD",
                        total = refundAmount // "5.00"
                    }
                };
                var apiContext = ConfigSetting.GetAPIContext();
                Sale aa = new Sale
                {
                    id = transactionId // "0U928361HU1278724"
                };
                aa.billing_agreement_id = subProfileId;  // "I-LYDGTYW1P5J0";

                
                var response = aa.Refund(apiContext, refund);
                ExceptionHandler.ReportError(new Exception(), "response = " + response.description);
                ExceptionHandler.ReportError(new Exception(), "refund success ");
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(new Exception(), "exception in refund");

            }
        }

        public bool SuspendProfile(string subProfileId)
        {
            try
            {
                var apiContext = ConfigSetting.GetAPIContext();
                Agreement.Suspend(apiContext, subProfileId, new AgreementStateDescriptor { note="Auto renew off" });
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
