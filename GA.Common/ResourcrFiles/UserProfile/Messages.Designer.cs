﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.Common.ResourcrFiles.UserProfile {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Messages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Messages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GA.Common.ResourcrFiles.UserProfile.Messages", typeof(Messages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please login by Google account. Your Id is already registered with google login..
        /// </summary>
        public static string AccountAlreadyRegisterWithGoogle {
            get {
                return ResourceManager.GetString("AccountAlreadyRegisterWithGoogle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please login by Guardian Avionics account. Your Id is already registered in Guardian Avionics..
        /// </summary>
        public static string AccountAlreadyRegisterWithGuardian {
            get {
                return ResourceManager.GetString("AccountAlreadyRegisterWithGuardian", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email added successfully.
        /// </summary>
        public static string AddEmail {
            get {
                return ResourceManager.GetString("AddEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to License added successfully.
        /// </summary>
        public static string AddLicense {
            get {
                return ResourceManager.GetString("AddLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maintenance user added successfully.
        /// </summary>
        public static string AddMaintenanceUserSuccess {
            get {
                return ResourceManager.GetString("AddMaintenanceUserSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medical certificate added successfully.
        /// </summary>
        public static string AddMedicalCertificate {
            get {
                return ResourceManager.GetString("AddMedicalCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating added successfully.
        /// </summary>
        public static string AddRating {
            get {
                return ResourceManager.GetString("AddRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User is blocked.
        /// </summary>
        public static string BlockedUser {
            get {
                return ResourceManager.GetString("BlockedUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete the Email ID?.
        /// </summary>
        public static string ComfirmDeleteEmail {
            get {
                return ResourceManager.GetString("ComfirmDeleteEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Component manufacturer ((name)) already exists..
        /// </summary>
        public static string ComponentManufacturerAlreadyExist {
            get {
                return ResourceManager.GetString("ComponentManufacturerAlreadyExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manufacturer name cannot be empty..
        /// </summary>
        public static string ComponentManufacturerNameRequired {
            get {
                return ResourceManager.GetString("ComponentManufacturerNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete license?.
        /// </summary>
        public static string ConfirmDeleteLicense {
            get {
                return ResourceManager.GetString("ConfirmDeleteLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete medical certificate?.
        /// </summary>
        public static string ConfirmDeleteMedicalCertificate {
            get {
                return ResourceManager.GetString("ConfirmDeleteMedicalCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to delete rating?.
        /// </summary>
        public static string ConfirmDeleteRating {
            get {
                return ResourceManager.GetString("ConfirmDeleteRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password and confirm password does not match..
        /// </summary>
        public static string ConfirmPasswordNotMatch {
            get {
                return ResourceManager.GetString("ConfirmPasswordNotMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email deleted successfully.
        /// </summary>
        public static string DeleteEmail {
            get {
                return ResourceManager.GetString("DeleteEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to License deleted successfully.
        /// </summary>
        public static string DeleteLicense {
            get {
                return ResourceManager.GetString("DeleteLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medical certificate deleted successfully.
        /// </summary>
        public static string DeleteMedicalCertificate {
            get {
                return ResourceManager.GetString("DeleteMedicalCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating deleted successfully.
        /// </summary>
        public static string DeleteRating {
            get {
                return ResourceManager.GetString("DeleteRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email id has already been registered as pilot..
        /// </summary>
        public static string DupluicateEmailIdMaintenanceUser {
            get {
                return ResourceManager.GetString("DupluicateEmailIdMaintenanceUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email id already exists..
        /// </summary>
        public static string EmailIdAlreadyExist {
            get {
                return ResourceManager.GetString("EmailIdAlreadyExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email id not exists..
        /// </summary>
        public static string EmailIdNotExist {
            get {
                return ResourceManager.GetString("EmailIdNotExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maxiumum 50 characters allowed!.
        /// </summary>
        public static string FirstNameMaxCharLimit {
            get {
                return ResourceManager.GetString("FirstNameMaxCharLimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flying summary details updated successfully.
        /// </summary>
        public static string FlyingSummaryUpdate {
            get {
                return ResourceManager.GetString("FlyingSummaryUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security answer is incorrect..
        /// </summary>
        public static string IncorrectSecurityAnswer {
            get {
                return ResourceManager.GetString("IncorrectSecurityAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid city name..
        /// </summary>
        public static string InvalidCityName {
            get {
                return ResourceManager.GetString("InvalidCityName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid username or password.
        /// </summary>
        public static string InvalidCredentials {
            get {
                return ResourceManager.GetString("InvalidCredentials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid EmailId.
        /// </summary>
        public static string InvalidEmailId {
            get {
                return ResourceManager.GetString("InvalidEmailId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid first name..
        /// </summary>
        public static string InvalidFirstName {
            get {
                return ResourceManager.GetString("InvalidFirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid last name..
        /// </summary>
        public static string InvalidLastName {
            get {
                return ResourceManager.GetString("InvalidLastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid state name..
        /// </summary>
        public static string InvalidStateName {
            get {
                return ResourceManager.GetString("InvalidStateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid zip code..
        /// </summary>
        public static string InvalidZipCode {
            get {
                return ResourceManager.GetString("InvalidZipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maxiumum 50 characters allowed!.
        /// </summary>
        public static string LastNameMaxCharLimit {
            get {
                return ResourceManager.GetString("LastNameMaxCharLimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to License expiration date cannot be less than current date.
        /// </summary>
        public static string LicenseDateValidate {
            get {
                return ResourceManager.GetString("LicenseDateValidate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occurs while login.
        /// </summary>
        public static string LoginException {
            get {
                return ResourceManager.GetString("LoginException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maintenance user already registered with this aircraft. .
        /// </summary>
        public static string MaintenanceUserAlreadyRegisteredWithAircraft {
            get {
                return ResourceManager.GetString("MaintenanceUserAlreadyRegisteredWithAircraft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medical certificate expiration date cannot be less than current date.
        /// </summary>
        public static string MedicalCertificateDateValidate {
            get {
                return ResourceManager.GetString("MedicalCertificateDateValidate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password must be between 6 to 50 characters..
        /// </summary>
        public static string PasswordLimit {
            get {
                return ResourceManager.GetString("PasswordLimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password changed successfully.
        /// </summary>
        public static string PasswordUpdateSuccess {
            get {
                return ResourceManager.GetString("PasswordUpdateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone number must be between 10-15 characters..
        /// </summary>
        public static string PhoneNumberCharLimit {
            get {
                return ResourceManager.GetString("PhoneNumberCharLimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating expiration date cannot be less than current date.
        /// </summary>
        public static string RatingDateValidation {
            get {
                return ResourceManager.GetString("RatingDateValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password reset request has been recieved for the account with username ((USEREMAILID)). To set a new pasword, please visit at ((RESETPASSWORDURL)).
        /// </summary>
        public static string ResetPasswordEmailData {
            get {
                return ResourceManager.GetString("ResetPasswordEmailData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email updated successfully.
        /// </summary>
        public static string UpdateEmail {
            get {
                return ResourceManager.GetString("UpdateEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to License updated successfully.
        /// </summary>
        public static string UpdateLicense {
            get {
                return ResourceManager.GetString("UpdateLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Medical certificate updated successfully.
        /// </summary>
        public static string UpdateMedicalCertificate {
            get {
                return ResourceManager.GetString("UpdateMedicalCertificate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating updated successfully.
        /// </summary>
        public static string UpdateRating {
            get {
                return ResourceManager.GetString("UpdateRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ZipCode must be between 4 to 8 characters..
        /// </summary>
        public static string ZipCodeCharLimit {
            get {
                return ResourceManager.GetString("ZipCodeCharLimit", resourceCulture);
            }
        }
    }
}
