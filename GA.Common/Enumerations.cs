﻿using System;
using System.Reflection;

namespace GA.Common
{

    public static class Enumerations
    {

        //5045  is the available value

        /// <summary>
        /// Returns the StringValue from an Enumeration
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>The string</returns>
        public static string GetStringValue(this Enum value)
        {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValue), false) as StringValue[];

            // Return the first if there was a match.
            return attribs != null && attribs.Length > 0 ? attribs[0].Value : null;
        }



        public enum RegistrationReturnCodes
        {
            [StringValue("Success")]
            Success = 0,

            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("User name cannot null or empty")]
            UserNameNotFound = 1,

            [StringValue("Password name cannot null or empty")]
            PasswordNotFound = 2,

            [StringValue("First name name cannot null or empty")]
            FirstNameNotFound = 3,

            [StringValue("Last name cannot null or empty")]
            LastNameNotFound = 4,

            [StringValue("Phone no. cannot null or empty")]
            PhoneNumberNotFound = 5,

            [StringValue("Security question id cannot null or empty")]
            SecurityQuestionNotFound = 6,

            [StringValue("Security answer cannot null or empty")]
            SecurityAnswerNotFound = 7,

            [StringValue("Email id already exist")]
            DuplicateEmailId = 8,

            [StringValue("Email id in format")]
            EmailIdInWrongFormat = 9,

            [StringValue("Short password length")]
            PasswordShortInLength = 10,

            [StringValue("Email Id Not exist")]
            EmailIdNotFound = 11,

            [StringValue("Depricate Current Version")]
            DepricateCurrentVersion = 12,

            [StringValue("5007")]
            ManufacturerAlreadyLinkedWithOtherUser = 13,

            [StringValue("5011")]
            AircraftTailNoNotFound = 14,

            [StringValue("MaintenanceUserAlreadyMappedWithSameAircraft")]
            MaintenanceUserAlreadyMappedWithSameAircraft = 15

        }

        public enum LoginReturnCodes
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid email id")]
            EmailIdDoNotExist = 1,

            [StringValue("Invalid password")]
            PasswordDoNotExist = 2,

            [StringValue("User is blocked")]
            UserBlocked = 3,

            //[StringValue("Already register with guardian")]
            //AlreadyRegisterWithGuardian = 4,

            //[StringValue("Third party id cannot null or empty")]
            //ThirdPartyIdCannotNullOrEmpty = 5,

            [StringValue("EmailId cannot null or empty")]
            EmailIdCannotNullOrEmpty = 4,

            [StringValue("Password cannot null or empty")]
            PasswordCannotNullOrEmpty = 5,

            //[StringValue("ThirdParty account type cannot null or empty")]
            //ThirdPartyAccountTypeCannotNullOrEmpty = 8,

            //[StringValue("Incorrect value Of third party account type")]
            //IncorrectValueOfThirdPartyAccountType = 9,

            [StringValue("Already register with FaceBook account")]
            AlreadyRegisterWithFaceBookAccount = 6,

            [StringValue("Already register with Gmail account")]
            AlreadyRegisterWithGmailAccount = 7,

            [StringValue("Admin cannot login")]
            AdminCannotLogin = 8,

            [StringValue("Depricate Current Version")]
            DepricateCurrentVersion = 9,

            [StringValue("AdminUser")]
            AdminUser = 10,

            [StringValue("Invalid username or password")]
            InvalidUserNameOrPassword = 11,

            [StringValue("New password cannot be null or empty")]
            NewPasswordCannotNullOrEmpty = 12,

            [StringValue("Maintenance user cannot login from the app")]
            MaintenanceUserCannotLoginFromApp = 13,

           [StringValue("Already register with Apple account")]
            AlreadyRegisterWithAppleAccount = 7,

        }


        public enum ThirdPartyLogin
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Third party id cannot null or empty")]
            ThirdPartyIdCannotNullOrEmpty = 1,

            [StringValue("EmailId cannot null or empty")]
            EmailIdCannotNullOrEmpty = 2,

            [StringValue("First name name cannot null or empty")]
            FirstNameNotFound = 3,

            [StringValue("Last name cannot null or empty")]
            LastNameNotFound = 4,

            [StringValue("ThirdParty account type cannot null or empty")]
            ThirdPartyAccountTypeCannotNullOrEmpty = 5,

            [StringValue("Incorrect value Of third party account type")]
            IncorrectValueOfThirdPartyAccountType = 6,

            [StringValue("Already register with guardian")]
            AlreadyRegisterWithGuardian = 7,

            [StringValue("Already register with FaceBook account")]
            AlreadyRegisterWithFaceBookAccount = 8,

            [StringValue("Already register with Gmail account")]
            AlreadyRegisterWithGmailAccount = 9,

            [StringValue("User is blocked")]
            UserBlocked = 10,

            [StringValue("Admin cannot login")]
            AdminCannotLogin = 11,

            [StringValue("Already register with Apple account")]
            AlreadyRegisterWithAppleAccount = 12


        }

        public enum UserProfileUpdateCodes
        {
            [StringValue("9999")]
            Exception = 9999,


            [StringValue("0")]
            Success = 0,

            [StringValue("Profile id not found")]
            ProfileIdNotFound = 1,

            [StringValue("EmailId already exist with other profile")]
            EmailIdAlreadyExistWithOtherProfile = 2,

            [StringValue("EmailId cannot null or empty")]
            EmailIdCannotNullOrEmpty = 3,

            [StringValue("User is blocked")]
            UserBlocked = 4
        }


        public enum UsAirportFile
        {
            [StringValue("Success")]
            Success = 0,

            [StringValue("Exception")]
            Exception = 9999


        }

        public enum KMLFileCodes
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid flight id")]
            InvalidFlightId = 1
        }

        public enum Status
        {
            [StringValue("1")]
            Success = 1,

            [StringValue("1")]
            Failure = 2,

            [StringValue("1")]
            Pending = 3,
        }

        public enum NotificationCodes
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid notification id")]
            InvalidNotificationId = 1,

            [StringValue("Invalid status id")]
            InvalidStatusId = 2

        }


        public enum AircraftCodes
        {

            [StringValue("9999")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid profile id")]
            InvalidProfileId = 1,

            [StringValue("Profile Id not found")]
            ProfileIdNotFound = 2,

            [StringValue("Aircraft id not found")]
            AircraftIdNotFound = 3,

            [StringValue("Aero unit already link with another aircraft")]
            AeroUnitAlreadyLinkWithAnotherAircraft = 4,

            [StringValue("Owner emailId not found")]
            OwnerEmailIdNotFound = 5,

            [StringValue("Tail No. already exist")]
            TailNoAlreadyExist = 6,

            [StringValue("User register success and tail no. already exist")]
            UserRegisterSuccessAndTailNoAlreadyExist = 7,

            [StringValue("User register success and exception in aircraft registration")]
            UserRegisterSuccessAndExceptionInAircraftRegistration = 8,

            [StringValue("Owner profile id not found")]
            OwnerProfileIdNotFound = 9,

            [StringValue("Tail no already exist for register aircraft without unit")]
            TailNoAlreadyExistForRegisterAircraftWithoutUnit = 10,

            [StringValue("Owner emailId is already registered with tail no.")]
            OwnerEmailIdIsAlreadyRegisteredWithTailNo = 11,

            [StringValue("Maintenance user already mapped with same aircraft")]
            MaintenanceUserAlreadyMappedWithSameAircraft = 12,

            [StringValue("Cannot create aircraft for current subscription")]
            CannotCreateAircraftForCurrentSubscription = 13,

            [StringValue("Email id cannot be null or empty")]
            EmailIdCannotNullOrEmpty = 14,

            [StringValue("Invalid unique id")]
            InvalidUniqueId = 15,

            [StringValue("Aero unit number not exist")]
            AeroUnitNumberNotExist = 16,

            [StringValue("Owner email id not exist")]
            EmailIdDoNotExist = 17,

            [StringValue("NNumber is not unique")]
            NNumberIsNotUnique = 18
        }

        public enum UpdatePreferenceCode
        {
            [StringValue("117")]
            DistanceNotFound = 0,

            [StringValue("118")]
            SpeedNotFound = 1,

            [StringValue("119")]
            VerticalSpeedNotFound = 2,

            [StringValue("120")]
            FuelNotFound = 3,

            [StringValue("121")]
            WeightNotFound = 4,

            [StringValue("0")]
            Success = 5,

            [StringValue("9999")]
            Exception = 6,

            [StringValue("137")]
            EmailCountCrossMaxLimit = 7,

            [StringValue("138")]
            InsertDistinctEmailOnly = 8,

            [StringValue("139")]
            InvalidEmailId = 9
        }

        public enum SendPushMessageAfterFlightStart
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Aircraft id not exist")]
            AircraftIdNotExist = 1,

            [StringValue("Prodile Id not exist")]
            ProfileIdNotExist = 2,

            [StringValue("Device token id not available")]
            DeviceTokenIdNotAvailable = 3
        }

        public enum GetPreference
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Profile id not found")]
            ProfileIdNotFound = 1

        }

        public enum ResetPassword
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Email id not found")]
            EmailIdNotFound = 1,

            [StringValue("Profile do not exist")]
            ProfileDoNotExist = 2

        }

        public enum Logbook
        {

            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid profile id")]
            InvalidProfileId = 1,

            [StringValue("LoogBook tag and id not found")]
            LoogBookTagAndIdNotFound = 2,

            [StringValue("Profile id not found")]
            ProfileIdNotFound = 3,

            [StringValue("Invalid log id")]
            InvalidLogId = 4,

            [StringValue("Pilot emailId not found")]
            PilotEmailIdNotFound = 5,

            [StringValue("CoPilot emailId not found")]
            CoPilotEmailIdNotFound = 6,

            [StringValue("Both pilot and copilot emailId not found")]
            BothPilotAndCoPilotEmailIdNotFound = 7,

            [StringValue("Pilot and copilot emailId cannot be same")]
            PilotAndCoPilotEmailIdCannotBeSame = 8,

            [StringValue("Invalid Time format for Actual")]
            InvalidTimeFormatForActual = 9,

            [StringValue("Invalid Time format for DayPIC")]
            InvalidTimeFormatForDayPIC = 10,

            [StringValue("Invalid Time format for CrossCountry")]
            InvalidTimeFormatForCrossCountry = 11,

            [StringValue("Invalid Time format for Hood")]
            InvalidTimeFormatForHood = 12,

            [StringValue("Invalid Time format for NightPIC")]
            InvalidTimeFormatForNightPIC = 13,

            [StringValue("Invalid Time format for Sim")]
            InvalidTimeFormatForSim = 14,

            [StringValue("pilot email id not exist")]
            PilotEmailIdNotExist = 15,

            [StringValue("copilot email id not exist")]
            CoPilotEmailIdNotExist = 16

        }

        public enum AircraftSearch
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Nnumber do not exist")]
            NNumberDoNotExist = 1,

            [StringValue("NNumber is not unique")]
            NNumberIsNotUnique = 2,

            [StringValue("Registration no. cannot be null or empty.")]
            RegistrationNoCannotNullOrEmpty = 3
        }

        public enum Documents
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid profile id")]
            InvalidProfileId = 1
        }


        public enum Subscription
        {
            [StringValue("0")]
            Success = 0,

            [StringValue("9999")]
            Exception = 1,

            //[StringValue("131")]
            //TransactionReceiptEmpty = 2,

        }

        public enum ValidateReceipt
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Subscription with original transaction id already mapped with other user.")]
            OriginalTransactionIdAlreadyMapWithOtherUser = 1,

            [StringValue("Inapp Status Invalid.")]
            InappStatusInvalid = 2,

            [StringValue("Subscription auto renewal is false.")]
            SubscriptionAutoRenewalIsFalse = 3,

            [StringValue("Exception occurs while receiving response from apple itunes.")]
            ExceptionFromItunes = 4,

            [StringValue("No Subscription available for this user.")]
            NoSubscriptionAvailableForThisUSer = 5,
        }


        // enums for All the Units
        public enum FuelUnit
        {
            [StringValue("GAL")]
            GAL = 0,

            [StringValue("LIT")]
            LIT = 1

        }

        public enum WeightUnit
        {
            [StringValue("LBS")]
            LBS = 0,

            [StringValue("KG")]
            KG = 1
        }

        public enum SpeedUnit
        {
            [StringValue("KTS")]
            KTS = 0,

            [StringValue("MPH")]
            MPH = 1,

            [StringValue("KM/H")]
            KMH = 2
        }

        public enum VerticalSpeedUnit
        {
            [StringValue("FT/MIN")]
            FTMIN = 0,

            [StringValue("M/S")]
            MS = 1
        }

        public enum Distance
        {

            [StringValue("SM")]
            SM = 0,

            [StringValue("NM")]
            NM = 1,

            [StringValue("KM")]
            KM = 2
        }

        public enum ImageOptions
        {
            [StringValue("0")]
            DeleteImage = 0,

            [StringValue("1")]
            SaveImage = 1,

            [StringValue("2")]
            DoNothing = 2,

        }

        public enum EngineType
        {
            [StringValue("1")]
            SingleEngine = 1,

            [StringValue("2")]
            MultipleEngine = 2,
        }

        public enum UpdatePilotForFlight
        {
            [StringValue("0")]
            Success = 0,

            [StringValue("502")]
            InvalidPilotEmailId = 1,

            [StringValue("503")]
            InvalidCoPilotEmailId = 2,

            [StringValue("504")]
            InvalidPilotLogId = 3,

            [StringValue("505")]
            PilotAndCoPilotEmailIdCannotBeSame = 4
        }

        public enum UserType
        {
            [StringValue("135")]
            Admin = 1,

            [StringValue("5005")]
            User = 2,

            [StringValue("5006")]
            Manufacturer = 3,

            [StringValue("5037")]
            Maintenance = 4,

            //[StringValue("136")]
            //NotAdmin = 5

            [StringValue("")]
            InventoryAdmin = 5,

            [StringValue("")]
            InventorySubAdmin = 6,

            [StringValue("")]
            InventoryProduction = 7,

            [StringValue("")]
            InventorySales = 8,

            [StringValue("")]
            DemoUser = 9
        }

        public enum FMSSearch
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("False")]
            False = 1,

            [StringValue("True")]
            True = 2,

            [StringValue("Aero Unit no. cannot be null or empty.")]
            AeroUnitNoNotFound = 3,

            [StringValue("Not register with aircraft.")]
            NotRegisterWithAircraft = 4,

            [StringValue("Aero unit number not exist.")]
            AeroUnitNumberNotExist = 5,

            [StringValue("Pilot already link with aero unit.")]
            PilotAlreadyLinkWithAeroUnit = 6,

            [StringValue("Cannot link with deleted pilot.")]
            CannotLinkWithDeletedPilot = 7,

            [StringValue("Aero unit linked with aircraft.")]
            AeroUnitLinkedWithAircraft = 8,

            [StringValue("Profile Id not found.")]
            ProfileIdNotFound = 9,

            [StringValue("Aero unit already registered.")]
            FMSAlreadyRegistered = 10
        }

        public enum LiveData
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid pilot id")]
            InvalidPilotId = 1,

            [StringValue("Invalid copilot Id")]
            InvalidCoPilotId = 2,

            //[StringValue("Invalid engine type")]
            //InvalidEngineType = 3,

            [StringValue("Invalid unique id")]
            InvalidUniqueId = 3,

            [StringValue("Aircraft id not found")]
            AircraftIdNotFound = 4

        }

        public enum GetDataFile
        {

            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Aircraft Id not received in header.")]
            AircraftIdNotFound = 1,

            [StringValue("Profile Id not received in header.")]
            ProfileIdNotFound = 2,

            [StringValue("Unique Id not received in header.")]
            UniqueIdNotFound = 3,
        }

        public enum GetAudioFile
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Error while saving the audio file, Parser = Failed.")]
            ParserFailed = 1
        }


        public enum FlightDataType
        {
            [StringValue("5026")]
            LiveData = 1,

            [StringValue("5027")]
            StoredData = 2
        }


        public enum IssueStatus
        {
            Open = 1,
            InProgress = 2,
            Closed = 3
        }

        public enum IssuePriority
        {
            [StringValue("5038")]
            Low = 0,

            [StringValue("5039")]
            High = 1,

            [StringValue("5040")]
            Highest = 2
        }

        public enum enumHeader
        {
            Date = 1,
            Time = 2,
            Latitude = 3,
            Longitude = 4,
            Speed = 5,
            Altitude = 6,
            Yaw = 7,
            Pitch = 8,
            Roll = 9,
            MAP = 10,
            RPM = 11,
            EGT1 = 12,
            EGT2 = 13,
            EGT3 = 14,
            EGT4 = 15,
            EGT5 = 16,
            EGT6 = 17,
            EGT7 = 18,
            EGT8 = 19,
            EGT9 = 20,
            CHT1 = 21,
            CHT2 = 22,
            CHT3 = 23,
            CHT4 = 24,
            CHT5 = 25,
            CHT6 = 26,
            CHT7 = 27,
            CHT8 = 28,
            CHT9 = 29,
            TIT1 = 30,
            TIT2 = 31,
            VOLTS = 32,
            VOLTS2 = 33,
            OILT = 34,
            OILP = 35,
            FP = 36,
            AMP = 37,
            AMP2 = 38,
            FF = 39,
            FQL = 40,
            FQR = 41,
            CoolantTemperature = 42,
            CoolantPressure = 43,
            Airtemp = 44,
            REM = 45,
            FuelQty3 = 46,
            FuelQty4 = 47,
            CDT = 48,
            CarbTemp = 49,
            RubberTrimPosition = 50,
            AileronTrimPosition = 51,
            FlapPosition = 52,
            ElevatorTrimPosition = 53,
            IgnStatus = 54,
            SensorStatus = 55,
            Sen1 = 56,
            Sen2 = 57,
            Sen3 = 58,
            Sen4 = 59,
            Sen5 = 60,
            ThrottlePosition = 61,
            Baro = 62,
            EcuTemp = 63,
            CLD = 64, //Dec No, Sign No
            REQ = 65, //Dec Yes, Sign No
            END = 66, //Dec No, Sign No
            HP = 67, //Dec No, Sign No
            IAT = 68, //Dec No, Sign No
            MPG = 69, //Dec Yes, Sign No
            Oat = 70, //Dec No, Sign No
            RES = 71, //Dec Yes, Sign No
            USD = 72, //Dec Yes, Sign No
            HM = 73,
            LAT = 74, //Dec Yes, Sign No
            LNG = 75, //Dec Yes, Sign No
            ALT = 76, //Dec No, Sign No
            SPD = 77 //Dec Yes, Sign No
        }

        public enum enumHeaderDuplicate
        {
            Date = 1,
            Time = 2,
            Latitude = 3,
            Longitude = 4,
            Speed = 5,
            Altitude = 6,
            Yaw = 7,
            Pitch = 8,
            Roll = 9,
            ALTM = 10,
            VA = 11,
            LA = 12,
            MAP = 13,
            RPM = 14,
            EGT1 = 15,
            EGT2 = 16,
            EGT3 = 17,
            EGT4 = 18,
            EGT5 = 19,
            EGT6 = 20,
            EGT7 = 21,
            EGT8 = 22,
            EGT9 = 23,
            CHT1 = 24,
            CHT2 = 25,
            CHT3 = 26,
            CHT4 = 27,
            CHT5 = 28,
            CHT6 = 29,
            CHT7 = 30,
            CHT8 = 31,
            CHT9 = 32,
            TIT1 = 33,
            TIT2 = 34,
            VOLTS = 35,
            VOLTS2 = 36,
            OILT = 37,
            OILP = 38,
            FP = 39,
            AMP = 40,
            AMP2 = 41,
            FF = 42,
            FQL = 43,
            FQR = 44,
            CoolantTemperature = 45,
            CoolantPressure = 46,
            Airtemp = 47,
            REM = 48,
            FuelQty3 = 49,
            FuelQty4 = 50,
            CDT = 51,
            CarbTemp = 52,
            RubberTrimPosition = 53,
            AileronTrimPosition = 54,
            FlapPosition = 55,
            ElevatorTrimPosition = 56,
            IgnStatus = 57,
            SensorStatus = 58,
            Sen1 = 59,
            Sen2 = 60,
            Sen3 = 61,
            Sen4 = 62,
            Sen5 = 63,
            ThrottlePosition = 64,
            Baro = 65,
            EcuTemp = 66,
            CLD = 67, //Dec No, Sign No
            REQ = 68, //Dec Yes, Sign No
            END = 69, //Dec No, Sign No
            HP = 70, //Dec No, Sign No
            IAT = 71, //Dec No, Sign No
            MPG = 72, //Dec Yes, Sign No
            Oat = 73, //Dec No, Sign No
            RES = 74, //Dec Yes, Sign No
            USD = 75, //Dec Yes, Sign No
            HM = 76,
            LAT = 77, //Dec Yes, Sign No
            LNG = 78, //Dec Yes, Sign No
            ALT = 79, //Dec No, Sign No
            SPD = 80, //Dec Yes, Sign No
            VSI = 81
        }

        public enum PaypalStatus
        {
            [StringValue("Active")]
            Active = 1,

            [StringValue("Cancelled")]
            Cancelled = 2,

            [StringValue("Suspended")]
            Suspended = 3,

            [StringValue("Completed")]
            Completed = 4
        }

        //public enum SubscriptionSource
        //{
        //    InappPurchase = 1,
        //    Paypal = 2
        //}

        public enum SubscriptionSourceEnum
        {
            Paypal = 1,
            InappPurchase = 2,
            Free = 3
        }

        public enum AircraftIssue
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Title cannot be null or empty.")]
            TitleNotFound = 1,

            [StringValue("Invalid assigner id")]
            InvalidAssignerId = 2,

            [StringValue("Invalid assignee id")]
            InvalidAssigneeId = 3,

            [StringValue("Invalid aircraft id")]
            InvalidAircraftId = 4,

            [StringValue("Invalid issue id")]
            InvalidIssueId = 5,

            [StringValue("Close by user id not found.")]
            CloseByUserIdNotFound = 6,

            [StringValue("User must be maintenance user or owner.")]
            UserMustBeMaintenanceUserOrOwner = 7,

            [StringValue("Comment canNot be empty.")]
            CommentCanNotBeEmpty = 8,

            [StringValue("Invalid profile id")]
            InvalidProfileId = 9
        }

        public enum MaintenanceUser
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Email Id Not exist.")]
            EmailIdNotFound = 1,

            [StringValue("Email id in format.")]
            EmailIdInWrongFormat = 2,

            [StringValue("Aircraft id not found.")]
            AircraftIdNotFound = 3,

            [StringValue("Email id already exist.")]
            DuplicateEmailId = 4,

            [StringValue("Maintenance user already mapped with same aircraft.")]
            MaintenanceUserAlreadyMappedWithSameAircraft = 5,
        }


        public enum PartNumber
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Part number already exist.")]
            PartNoAlreadyExist = 1,

            [StringValue("Part number does not exist.")]
            PartNoNotExist = 2,

            [StringValue("Cannot change the \"Can Sold\" status of part number from {0} to {1} because {2} entry exist for this part number")]
            CannotChangeCanSoldStatus = 3
        }

        public enum InventoryReduction
        {
            [StringValue("Exception occurs while processing the request.")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Reduction quantity cannot greater than available quantity.")]
            InvalidReductionQuantity = 1,

            [StringValue("Invalid request to delete inventory reduction record.")]
            InvalidDeleteRequest = 2,

            [StringValue("Invalid request to edit inventory reduction record.")]
            InvalidEditRequest = 3
        }

        public enum Customer
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Customer already exist.")]
            CustomerAlreadyExist = 1,

            [StringValue("Invalid request to delete customer.")]
            InvalidRequestToDelete = 2,

            [StringValue("Invalid request to edit customer.")]
            InvalidRequestToEdit = 3,

            [StringValue("Cannot delete this customer type as some of the invoices are created with this customer.")]
            CannotDeleteCustomer_InUse = 4,

            [StringValue("Cannot delete this customer as the customer is linked with RMA record.")]
            CannotDeleteCustomer_RMALinked = 5

        }

        public enum PurchaseOrder
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request to edit purchase order.")]
            InvalidRequest = 1,

            [StringValue("Cannot edit part number as some unit's status has been changed from Received to others.")]
            CannotEditPartNo = 2,

            [StringValue("Serial no already exist.")]
            SerialNoAlreadyExist = 3,

            [StringValue("Invalid request to delete purchase order.")]
            InvalidRequestToDelete = 4,

            [StringValue("Cannot delete purchase order as some of the unit's has been sold.")]
            CannotDeletePurchaseOrder = 5,

            [StringValue("Purchase order number already exist.")]
            PONumberAlreadyExist = 6
        }

        public enum Invoice
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request to delete invoice.")]
            InvalidDeleteRequest = 1,

            [StringValue("Units with serial number {0} already sold.")]
            SerialNumberAlreadySold = 2,

            [StringValue("Invalid request to update invoice.")]
            InvalidUpdateRequest = 3,

            [StringValue("Units with serial number {0} belongs to inactive part number.")]
            UnitBelongToInActivePartNo = 4,

            [StringValue("Invoice number does not exist.")]
            InvoiceNoNotExist = 5,

        }

        public enum Unit
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request to edit unit details.")]
            InvalidRequest = 1,

            [StringValue("Serial number already assigned to another unit.")]
            SerialNumberAlreadyAssign = 2,

            [StringValue("Invoice number already exist.")]
            InvoiceNumberAlreadyExist = 3,

            [StringValue("Invalid request to delete the unit.")]
            InvalidDeleteRequest = 4,

            [StringValue("Units can only be deleted if it is in production.")]
            CannotDeleteUnitWhichareNotInproduction = 5,

            [StringValue("Serial Number does not exist.")]
            SerialNoNotExist = 6,



            [StringValue("Invalid serial number.")]
            InvalidSerialNumber = 7,

            [StringValue("Unit with serial number {0} already sold.")]
            UnitAlreadySold = 8,

            [StringValue("Unit serial numbers already exist within a range from {0} to {1}. ")]
            SerialNoAlreadyExistWithinRange = 9,

            [StringValue("Board serial numbers already exist within a range from {0} to {1}. ")]
            BoardSerialNoAlreadyExistWithinRange = 10,

            [StringValue("Unit can be sold only if its status is \"Available for Sale\". The unit with serial number {0} has the status as \"{1}\"")]
            UnitEligiblityForSale = 11,

            [StringValue("Cannot delete this unit as it's status changes from {0} to {1}.")]
            CannotDeleteUnitWithChangedStatus = 12,

            [StringValue("unit with serial number {0} belongs to inactive part number.")]
            UnitBelongsToInActivePart = 13,

            [StringValue("Only Sold units can be added in the RMA")]
            OnlySoldUnitsCanAddInRMA = 14,

            [StringValue("Cannot create RMA for this unit, the unit will be used for either engineering or manufacturing purpose.")]
            CannotCreateRMAForNonSalableUnit = 15,

            [StringValue("This unit is added manually and liknked with RMA ")]
            UnitAddedManually = 16,

            [StringValue("Part Number does not exist.")]
            PartNumbernotExist = 17,

            [StringValue("No any Purchase order exist.")]
            PoNotExist = 18,
        }

        //public enum UnitStatus
        //{
        //    [StringValue("In Production")]
        //    InProduction = 1,

        //    [StringValue("Assembled")]
        //    Assembled = 2,

        //    [StringValue("QC Passed")]
        //    QCPassed = 3,

        //    [StringValue("QC Failed")]
        //    QCFailed = 4,

        //    [StringValue("Sold")]
        //    Sold = 5
        //}

        public enum UnitStatus
        {
            [StringValue("Received")]
            Received = 1,

            [StringValue("Available for Sale")]
            AvailableForSale = 2,

            [StringValue("Sold")]
            Sold = 3,

            [StringValue("Tradshow Demo")]
            TradshowDemo = 4,

            [StringValue("Destroy")]
            Destroy = 5,

            [StringValue("Sales Return")]
            SalesReturn = 6,

            [StringValue("Used for Production")]
            UsedForProduction = 7
        }

        public enum Vendor
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request to edit vendor details.")]
            InvalidRequest = 1,

            [StringValue("Vendor name already exist.")]
            VendorAlreadyExist = 2,

            [StringValue("Invalid request to delete vendor.")]
            InvalidDeleteRequest = 3,

            [StringValue("Cannot delete this vendor as this vendor belongs to some part number.")]
            CannotDeletVendor_InUse = 4,

            [StringValue("Cannot delete this vendor as this vendor belongs to some receive order.")]
            CannotDeletVendor_InUse_RecOrder = 5


        }

        public enum Receiver
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request to edit receiver details.")]
            InvalidRequest = 1,

            [StringValue("Receiver name already exist.")]
            ReceiverAlreadyExist = 2,

            [StringValue("Invalid request to delete receiver.")]
            InvalidDeleteRequest = 3,

            [StringValue("Cannot delete the receiver as this receiver belongs to some receive order.")]
            ReceiverBelongsToPO = 4
        }

        public enum EngineeringDocEnum
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request !!!")]
            InvalidRequest = 1,

            [StringValue("File/Folder name already exist.")]
            FileFolderAlreadyExist = 2
        }

        public enum KitAssemblyEnum
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Kit Assembly already exist with the part number {0}.")]
            KitAssemblyAlreadyExist = 1,

            [StringValue("Invalid request to delete kit assembly.")]
            InValidDeleteRequest = 2
        }

        public enum HardwareFirmwareEnum
        {
            [StringValue("Error occurs while processing the request!!!")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Invalid request.")]
            InvalidRequest = 1
        }


        public enum Discrepancy
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Discrepancy already exist.")]
            DiscrepancyAlreadyExist = 1,

            [StringValue("Invalid request to delete record.")]
            InvalidRequestToDelete = 2,

            [StringValue("Invalid request to edit record.")]
            InvalidRequestToEdit = 3,

            [StringValue("Cannot delete this discrepancy as some of the RMA record are linked with this discrepancy.")]
            CannotDeleteDiscrepancy_InUse = 4,

            [StringValue("Cannot delete this discrepancy as this is linked with RMA record.")]
            CannotDeleteDiscrepancy_RMALinked = 5

        }

        public enum Resolution
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("Resolution already exist.")]
            ResolutionAlreadyExist = 1,

            [StringValue("Invalid request to delete record.")]
            InvalidRequestToDelete = 2,

            [StringValue("Invalid request to edit record.")]
            InvalidRequestToEdit = 3,

            [StringValue("Cannot delete this Resolution as some of the RMA record are linked with this Resolution.")]
            CannotDeleteResolution_InUse = 4,

            [StringValue("Cannot delete this Resolution as this is linked with RMA record.")]
            CannotDeleteResolution_RMALinked = 5

        }

        public enum EnumRMAStatus
        {
            [StringValue("Exception")]
            Exception = 9999,

            [StringValue("Success")]
            Success = 0,

            [StringValue("RMA Status already exist.")]
            RMAStatusAlreadyExist = 1,

            [StringValue("Invalid request to delete record.")]
            InvalidRequestToDelete = 2,

            [StringValue("Invalid request to edit record.")]
            InvalidRequestToEdit = 3,

            [StringValue("Cannot delete this RMA Status as some of the RMA record are linked with this status.")]
            CannotDeleteRMAStatus_InUse = 4,

            [StringValue("Cannot delete this RMA Status as this is linked with RMA record.")]
            CannotDeleteRMAStatus_RMALinked = 5,

            [StringValue("Invalid invoice number")]
            InvalidInvoiceNumber = 6

        }

    }
}
