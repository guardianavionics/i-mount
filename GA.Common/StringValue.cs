﻿namespace GA.Common
{
    /// <summary>
    /// Class used to get the value in "StringValue" attribute added above the enumeration items.
    /// </summary>
    public class StringValue : System.Attribute
    {
        private string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringValue" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public StringValue(string value)
        {
            _value = value;
        }

        /// <summary>
        /// Gets the string value in "StringValue" attribute added above the enumeration items
        /// </summary>
        /// <value>The value.</value>
        public string Value
        {
            get { return _value; }
        }
    }
}
