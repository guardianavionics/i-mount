﻿using GA.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
//using MobilePaymentServer.Common;
//using MobilePaymentServer.Common.DataTypes;
//using MobilePaymentServer.Core;
//using Slps.ProtectionAttributes;

namespace GA.Common
{
    public class IphonePushNotification
    {

        /// <summary>
        /// Send Push notification to iOS Device //Added by Nitin Chotwani (ideavate solutions)
        /// </summary>
        /// <param name="serverUrl">The URL for iPhone Push Notification Server</param>
        /// <param name="serverPort">The port for iPhone Push Notification Server.</param>
        /// <param name="certificatePath">The certificate path for iPhone.</param>
        /// <param name="title">Title of the notification</param>
        /// <param name="tokenID">The token id provided by APNS to iPhone device.</param>
        /// <param name="alertMsg">The alert message for push notification.</param>
        /// <param name="phoneId">The phone id of the iPhone device.</param>
        /// <param name="soundFile">Sound file name</param>
        /// <param name="surveyId">Survey Id</param>
        /// <returns></returns>
        public bool PushToiPhone(string tokenID, string message, string notificationType = "")
        {
            try
            {
                //IPhonePNServerUrl
                
                String hostname = ConfigurationReader.IPhonePNServerUrl;
                int serverPort = 2195;

                string path = ConfigurationReader.CertificatePath;
                
                X509Certificate2 clientCertificate = new X509Certificate2(path, "", X509KeyStorageFlags.MachineKeySet);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, serverPort);

                SslStream sslStream = new SslStream(client.GetStream(),
                                                    false,
                                                    new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                                    null);

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (Exception e)
                {
                    string exce = e.Message.ToString() + "-------" + e.StackTrace.ToString() +
                                  "-------" + e.InnerException.ToString();
                    client.Close();
                    return false;
                }

                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0); //------- The command
                writer.Write((byte)0); //------- The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //------- The deviceId length (big-endian second byte)               
                String deviceID = tokenID;
                writer.Write(HexStringToByteArray(deviceID.ToUpper()));

                String payload = "{\"aps\":{\"alert\":\"" + message + "\",\"badge\":1,\"sound\":\"sound.caf\",\"notificationType\" : \""+notificationType+"\"}}";

                // writer.Write((byte)0);
                // writer.Write((byte)payload.Length);

                //  byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                //  writer.Write(b1);
                //  writer.Flush();

                writer.Write((byte)0);
                writer.Write((byte)payload.Length);

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                sslStream.Write(array);
                sslStream.Flush();

                client.Close();
                writer.Close();
            }
            catch (Exception ex)
            {
                //Logger.LogException(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validates the server certificate.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="certificate">The certificate.</param>
        /// <param name="chain">The chain.</param>
        /// <param name="sslPolicyErrors">The SSL policy errors.</param>
        /// <returns>true if certificate is valid else false</returns>
        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Convert the string to byte array.
        /// </summary>
        /// <param name="stringToConvert">The string to convert.</param>
        /// <returns>the byte array</returns>
        public static byte[] HexStringToByteArray(String stringToConvert)
        {
            stringToConvert = stringToConvert.Replace(" ", "");

            byte[] buffer = new byte[stringToConvert.Length / 2];

            for (int i = 0; i < stringToConvert.Length; i += 2)
            {
                buffer[i / 2] = Convert.ToByte(stringToConvert.Substring(i, 2), 16);
            }

            return buffer;
        }


    }
}
