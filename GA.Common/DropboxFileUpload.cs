﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Web.Mvc;
using Dropbox.Api;
using OAuthProtocol;
//using NLog;
namespace GA.Common
{
    public class DropboxFileUpload
    {
       // private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Redirects to dropbox account.
        /// </summary>
        /// <returns></returns>
        private static OAuthToken GetAccessToken()
        {
            string consumerKey = ConfigurationReader.DropboxConsumerKey;
            string consumerSecret = ConfigurationReader.DropboxConsumerSecret;
            string authenticationTime = "30"; //ConfigurationReader.DropboxAuthenticationTimeSeconds;

            int authTime = 1;
            int.TryParse(authenticationTime, out authTime);

            var oauth = new OAuth();
            var requestToken = oauth.GetRequestToken(new Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret);
            var authorizeUri = oauth.GetAuthorizeUri(new Uri(DropboxRestApi.AuthorizeBaseUri), requestToken);


            Process.Start(authorizeUri.AbsoluteUri);
            Thread.Sleep(authTime * 1000); // Leave some time for the authorization step to complete

            return oauth.GetAccessToken(new Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret, requestToken);
        }

        /// <summary>
        /// Redirect user to its dropbox account and gets the access token and access secret
        /// </summary>
        /// <returns>access token and access secret</returns>
        public string[] GetAccessToken1()
        {
            try
            {
                var accessToken = GetAccessToken();
                return accessToken != null ? new string[] { accessToken.Token, accessToken.Secret } : new string[] { };
            }
            catch (Exception e)
            {
                var a = e.Data;
                return new string[] { };
            }
        }

        /// <summary>
        /// Upload File in dropbox.
        /// </summary>
        /// <param name="token">User token</param>
        /// <param name="tokenSecret">User token secret</param>
        /// <param name="filePath">Path where file is saved on server</param>
        /// <param name="fileName">Name of file</param>
        /// <returns>Message for success of faliure</returns>
        //public string UploadFileDropbox(string token = "kt4my1pghxu69o9", string tokenSecret = "A7F29F31-1783-4841-B7B5-CEE8E6EDB676", string filePath = "", string fileName = "")        
        public string UploadFileDropbox(string token, string tokenSecret, string filePath, string fileName)
        {
            string consumerKey = ConfigurationReader.DropboxConsumerKey;
            string consumerSecret = ConfigurationReader.DropboxConsumerSecret;

            if (String.IsNullOrEmpty(token) || string.IsNullOrEmpty(tokenSecret))
            {
                //logger.Fatal("Exception dropbox file upload", new Exception("Invalid token and token secret"));
                return "Invalid token and token secret";
            }
            try
            {
                var accessToken = new OAuthToken(token, tokenSecret);

                var api = new DropboxApi(consumerKey, consumerSecret, accessToken);

                var file = api.UploadFile("dropbox/Apps/GuardianAvionicsData", fileName, @filePath);

                return Boolean.TrueString;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, "From dropbox");
                //logger.Fatal("Exception From dropbox", e);
            }

            //ExceptionHandler.ReportError(new Exception("Exception file not uploaded on dropbox"), "token = " + token + "token secret = " + tokenSecret + "file name and path " + fileName + " " + filePath);
            //logger.Fatal("Exception " + "token = " + token + "token secret = " + tokenSecret + "file name and path " + fileName + " " + filePath, new Exception("Exception file not uploaded on dropbox"));
            return string.Empty;
        }
    }
}
