﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GA.Common
{
   public class Encryption
    {
       public string Encrypt(string dataToEncrypt, string encryptionKey)
        {
            if (dataToEncrypt == null || dataToEncrypt.Length <= 0)
                throw new ArgumentNullException("data");
            if (encryptionKey == null || encryptionKey.Length <= 0)
                throw new ArgumentNullException("key");
            try
            {
                //Generate a Key based on a Password and HMACSHA1 pseudo-random number generator
                //Salt must be at least 8 bytes long
                //Use an iteration count of at least 1000
                // Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt), 10000);

                //Create AES algorithm
                byte[] encrypted;

                using (AesManaged aes = new AesManaged())
                {
                    // Defaults
                    // CipherMode = CBC
                    // Padding = PKCS7

                    aes.KeySize = 128;
                    aes.BlockSize = 128;

                    //Key derived from byte array with 32 pseudo-random key bytes
                    aes.Key = Encoding.UTF8.GetBytes(encryptionKey);
                    //IV derived from byte array with 16 pseudo-random key bytes
                    // aes.IV = Encoding.UTF8.GetBytes(encryptionIV); 
                    byte[] iv = new byte[16];
                    aes.IV = iv;

                    byte[] data = Encoding.UTF8.GetBytes(dataToEncrypt);
                    ICryptoTransform encryptor = aes.CreateEncryptor();
                    encrypted = encryptor.TransformFinalBlock(data, 0, data.Length);
                }
                return Convert.ToBase64String(encrypted);
            }
            finally
            {
            }
        }


       public string Decrypt(string cipherText, string encryptionKey)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (encryptionKey == null || encryptionKey.Length <= 0)
                throw new ArgumentNullException("key");

            byte[] decrypted;
            using (AesManaged aes = new AesManaged())
            {
                // Defaults
                // CipherMode = CBC
                // Padding = PKCS7

                aes.KeySize = 128;
                aes.BlockSize = 128;

                aes.Key = Encoding.UTF8.GetBytes(encryptionKey);
                byte[] iv = new byte[16];
                aes.IV = iv;

                var encryptedBytes = Convert.FromBase64String(cipherText);
                // byte[] cipherText = Encoding.UTF8.GetBytes(dataToDecrypt);

                ICryptoTransform decryptor = aes.CreateDecryptor();
                decrypted = decryptor.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            }
            return Encoding.UTF8.GetString(decrypted, 0, decrypted.Length);
        }


       public Stream DecryptTemp(byte[] encryptedBytes, string encryptionKey)
       {
           if (encryptedBytes == null || encryptedBytes.Length <= 0)
               throw new ArgumentNullException("cipherText");
           if (encryptionKey == null || encryptionKey.Length <= 0)
               throw new ArgumentNullException("key");
             MemoryStream mStream = new MemoryStream();
           byte[] decrypted;
           using (AesManaged aes = new AesManaged())
           {
               // Defaults
               // CipherMode = CBC
               // Padding = PKCS7

               aes.KeySize = 128;
               aes.BlockSize = 128;

               aes.Key = Encoding.UTF8.GetBytes(encryptionKey);
               byte[] iv = new byte[16];
               aes.IV = iv;

             
               // byte[] cipherText = Encoding.UTF8.GetBytes(dataToDecrypt);

               ICryptoTransform decryptor = aes.CreateDecryptor();
               decrypted = decryptor.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
             
               mStream.Write(decrypted, 0, decrypted.Length);
           }
           return mStream;
       }


      


       public Stream GenerateStreamFromString(string s)
       {
           return new MemoryStream(Encoding.UTF8.GetBytes(s));
       }


   


    }
}
