﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
//using NLog;
using System.Text.RegularExpressions;

using Amazon.S3;



using Amazon.Runtime;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;

namespace GA.Common
{
    /// <summary>
    /// Contains common functions which can be used in any other project.
    /// </summary>
    public class Misc
    {
       // private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        // format of date is "G", CultureInfo.CreateSpecificCulture("es-ES")

        /// <summary>
        /// Converts string date into DateTime object.
        /// </summary>
        /// <param name="date"> date is in "es-ES" culture</param>
        /// <returns>nullable DateTime object</returns>
        public static DateTime? GetDate(string date)
        {
            // last update date will never be string.Empty
            if (string.IsNullOrEmpty(date))
            {
                return null;
            }

            DateTime dateObj;

            if (DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("es-ES"), DateTimeStyles.None, out dateObj))
            {
                if (dateObj < (DateTime)SqlDateTime.MinValue)
                {
                    dateObj = (DateTime)SqlDateTime.MinValue;
                }

                return dateObj;
            }

            return (DateTime)SqlDateTime.MinValue;
        }


        public static DateTime? GetDateUS(string date)
        {
            var dateObj = new DateTime();

            if (DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("es-US"), DateTimeStyles.None, out dateObj))
            {
                if (dateObj < (DateTime)SqlDateTime.MinValue)
                {
                    dateObj = (DateTime)SqlDateTime.MinValue;
                }

                return dateObj;
            }

            // last update date will never be string.Empty
            if (string.IsNullOrEmpty(date))
            {
                return null;
            }

            return (DateTime)SqlDateTime.MinValue;
        }


        /// <summary>
        /// Converts Date Object into String which includes time.
        /// </summary>
        /// <param name="objDate">Nullable Date object</param>
        /// <returns>Date in String datatype , in culture "es-ES"</returns>
        public static string GetStringOfDate(DateTime? objDate)
        {
            if (objDate != null)
            {
                DateTime obj = (System.DateTime)objDate;
                return obj.ToString("G", CultureInfo.CreateSpecificCulture("es-ES"));
            }

            return string.Empty;
            //                return DateTime.UtcNow.ToString("G", CultureInfo.CreateSpecificCulture("es-ES"));
        }

        /// <summary>
        /// Converts Date Object into String which excludes time.format es-US
        /// </summary>
        /// <param name="objDate">Nullable Date object</param>
        /// <returns>Date in String datatype , in culture "es-ES" without time</returns>
        public static string GetStringOnlyDate(DateTime? objDate)
        {
            if (objDate != null)
            {
                var obj = (DateTime)objDate;
                string[] test = obj.Date.ToString("G", CultureInfo.CreateSpecificCulture("es-ES")).Split(' ');
                return test[0];

            }

            return string.Empty;

        }


        public static string GetStringOnlyDateUS(DateTime? objDate)
        {
            if (objDate != null)
            {
                var obj = (DateTime)objDate;
                string[] test = obj.Date.ToString("G", CultureInfo.CreateSpecificCulture("es-US")).Split(' ');
                return test[0];

            }

            return string.Empty;

        }

        /// <summary>
        /// Converts Date Object into String in format MM-dd-yyyy, which excludes time.
        /// </summary>
        /// <param name="objDate">Nullable Date object</param>
        /// <returns>Date in String datatype , in format "MM-dd-yyyy" without time</returns>
        public static string GetStringOnlyDateFormat2(DateTime? objDate)
        {
            if (objDate != null)
            {
                DateTime obj = (System.DateTime)objDate;
                return obj.Date.ToString("MM/dd/yyyy");
                //string[] test = obj.Date.ToString("MM-dd-yyyy");
                //return test[0];
            }

            return string.Empty;
        }

        public static string dateToMMDDYYYY(string date)
        {
            if (string.IsNullOrEmpty(date))
                return date;
            string[] arr = date.Split('/');
            string temp = arr[0];
            arr[0] = arr[1];
            arr[1] = temp;
            return string.Join("/", arr);
        }

        //public static string GetTimeFromDate(DateTime objDate)
        //{
        //    var time = new TimeSpan(objDate.Hour, objDate.Minute, objDate.Second).ToString();
        //}

        /// <summary>
        /// Sends email to specified email address using gmail.
        /// </summary>
        /// <param name="emailId">Email Receiver</param>
        /// <param name="MessageBody">Message Body</param>
        /// <param name="MessageSubject">Message Subject</param>
        public static void SendEmail(string emailId, string MessageBody, string MessageSubject = "Passwrod of {0} site")
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(ConfigurationReader.EmailId);
                mail.To.Add(emailId);
                mail.Subject = "Change Password Request";
                mail.Body = MessageBody;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(ConfigurationReader.HostName, Convert.ToInt32(ConfigurationReader.PortNo));
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationReader.EmailId, ConfigurationReader.EmailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationReader.EnableSSL);
                smtp.Send(mail);

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception", e);
                throw;
            }

        }


        public static void SendEmailCommon(string message, string Subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(ConfigurationReader.EmailId);
                mail.To.Add("atul.agrawal@ideavate.com");
                mail.Subject = Subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient(ConfigurationReader.HostName, Convert.ToInt32(ConfigurationReader.PortNo));
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationReader.EmailId, ConfigurationReader.EmailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationReader.EnableSSL);
                smtp.Send(mail);
            }
            catch(Exception ex)
            {

            }
        }

        public string ConvertMinToHHMM(double totalMinutes)
        {
            string minute = "0";
            string hours = "0";

            if (totalMinutes < 60)
            {
                minute = Convert.ToString(totalMinutes);
            }
            else
            {
                minute = Convert.ToString(totalMinutes % 60);
            }

            hours = Convert.ToString(Convert.ToInt32((int)totalMinutes / 60));
            if (Convert.ToDouble(hours) < 1)
            {
                hours = "00";
            }
            else if (Convert.ToDouble(hours) < 10)
            {
                hours = "0" + hours;
            }

            if (Convert.ToDouble(minute) < 10)
            {
                minute = "0" + minute;
            }


            return (hours + ":" + minute);

        }

        public string addMinuteAndConvertToHHMM(double minute1, double minute2)
        {
            return ConvertMinToHHMM(minute1 + minute2);
            //return ((minute1 + minute2) / 60).ToString() + ":" + ((minute1 + minute2) % 60).ToString() ;
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));
        }

        public static int convertHHMMTTTOSecond(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 3600) + (Convert.ToInt32(arr[1]) * 60) + Convert.ToInt32(arr[2]));
        }

        public string addTime(string time1, string time2)
        {

            if (string.IsNullOrEmpty(time1))
            {
                time1 = "00:00:00";
            }
            if (string.IsNullOrEmpty(time2))
            {
                time2 = "00:00:00";
            }


            string[] arr1 = time1.Split(':');
            string[] arr2 = time2.Split(':');

            int[] t1 = { Convert.ToInt32(arr1[0]), Convert.ToInt32(arr1[1]), Convert.ToInt32(arr1[2]) };
            int[] t2 = { Convert.ToInt32(arr2[0]), Convert.ToInt32(arr2[1]), Convert.ToInt32(arr2[2]) };

            int sec = 0, min = 0, hrs = 0;

            if ((t1[2] + t2[2]) >= 60)
            {
                sec = ((t1[2] + t2[2]) % 60);
                min = min + 1;
            }
            else
            {
                sec = (t1[2] + t2[2]);
            }

            if ((t1[1] + t2[1] + min) >= 60)
            {
                min = ((t1[1] + t2[1] + min) % 60);
                hrs = hrs + 1;
            }
            else
            {
                min = (t1[1] + t2[1] + min);
            }

            hrs = hrs + t1[0] + t2[0];

            return ((hrs < 10) ? "0" + Convert.ToString(hrs) : Convert.ToString(hrs)) + ":" + ((min < 10) ? "0" + Convert.ToString(min) : Convert.ToString(min)) + ":" + ((sec < 10) ? "0" + Convert.ToString(sec) : Convert.ToString(sec));

        }

        public string GetHoursFromDateTime(DateTime? objDate)
        {
            try
            {
                if (objDate.HasValue)
                {
                    var hrs = objDate.Value.Hour;
                    var min = objDate.Value.Minute;

                    return hrs + ":" + min;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return string.Empty;
        }


        /// <summary>
        /// gets the time difference from 1 jan 1970 to given date
        /// </summary>
        /// <param name="date">if its null then takes date as datetime.utcnow</param>
        /// <returns></returns>
        public static long GetMilliSecondsFrom01011970(DateTime? date)
        {
            // if date is null take date as current utc date
            date = date ?? DateTime.UtcNow;

            var a = ((DateTime)date).Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)Convert.ToUInt64(a.TotalMilliseconds);
        }

        public static string RandomString(int Size)
        {
            Random random = new Random();
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, Size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }
        public string RandomNumericString(int Size)
        {
            Random random = new Random();
            string input = "012345678901234567890123456789";
            var chars = Enumerable.Range(0, Size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }

        public static string convertOneTenthTimeToHHMMSS(string time)
        {
            string[] arr = time.Split('.');
            string response = "";
            response = (Convert.ToInt16(arr[0]) < 10) ? ("0" + arr[0]) : arr[0];

            if (arr.Length >= 2)
            {
                response = response + ":" + (((Convert.ToInt16(arr[1]) * 6) < 10) ? ("0" + (Convert.ToInt16(arr[1]) * 6) + ":00") : (Convert.ToInt16(arr[1]) * 6).ToString() + ":00");
            }
            else
            {
                response = response + "00:00";
            }

            return response;
        }

        public static string ConvertMinToOneTenthOFTime(double min)
        {
            double response = (min / 60);
            response = Math.Round(Convert.ToDouble(response), 1, MidpointRounding.ToEven); // Rounds to even
            return response.ToString().Split('.').Length > 1 ? response.ToString() : (response.ToString() + ".0");
        }


        public static double ConvertOneTenthOfTimeToMinutes(string time)
        {
            time = string.IsNullOrEmpty(time) ? "0" : time;
            string[] arr = time.Split('.');
            double response = 0;

            response = (Convert.ToDouble(arr[0]) * 60);

            if (arr.Length > 1)
            {
                response = response + (Convert.ToDouble(arr[1]) * 6);
            }
            return response;
        }


        public string Encrypt(string clearText)
        {
            string EncryptionKey = "JHDDLHUDH24564DJHKJDA";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                System.Security.Cryptography.Rfc2898DeriveBytes pdb = new System.Security.Cryptography.Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "JHDDLHUDH24564DJHKJDA";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                System.Security.Cryptography.Rfc2898DeriveBytes pdb = new System.Security.Cryptography.Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public string addTimeInOneTenthFormat(string time1, string time2)
        {
            string[] arrTime1 = time1.Split('.');
            string[] arrTime2 = time2.Split('.');

            int hrs = 0;
            int min = 0;

            if (Convert.ToInt16(arrTime1[1]) + Convert.ToInt16(arrTime2[1]) >= 10)
            {
                min = (Convert.ToInt16(arrTime1[1]) + Convert.ToInt16(arrTime2[1])) - 10;
                hrs = Convert.ToInt16(arrTime1[0]) + Convert.ToInt16(arrTime2[0]) + 1;
            }
            else
            {
                min = (Convert.ToInt16(arrTime1[1]) + Convert.ToInt16(arrTime2[1]));
                hrs = Convert.ToInt16(arrTime1[0]) + Convert.ToInt16(arrTime2[0]);
            }

            return hrs.ToString() + "." + min.ToString();
        }

        public double CalculateLatitudeAndLongitude(double degree, double minute, string strDirection)
        {
            int latsign = 1;

            double second = 0.0;
            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (second / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }


        //Overloaded  CalculateLatitudeAndLongitude
        public double CalculateLatitudeAndLongitude(double degree, double minute, double seconds, string strDirection)
        {
            int latsign = 1;


            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (seconds / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }

        public void test()
        {

            double aa = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(20.000510), "N");
            double b = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(32.000405), "W");
            double c = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(20.000894), "N");
            double d = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(32.000249), "W");
            double e = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(21.000277), "N");
            double f = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(32.000094), "W");
            double g = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(21.000660), "N");
            double h = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(31.000939), "W");
            double i = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(22.000044), "N");
            double j = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(31.000784), "W");
            double k = CalculateLatitudeAndLongitude(Convert.ToDouble(46), Convert.ToDouble(22.000427), "N");
            double l = CalculateLatitudeAndLongitude(Convert.ToDouble(121), Convert.ToDouble(31.000629), "W");

        }

        public static int IndexOf(byte[] searchWithin, byte[] serachFor, int startIndex)
        {
            int index = 0;
            int startPos = Array.IndexOf(searchWithin, serachFor[0], startIndex);

            if (startPos != -1)
            {
                while ((startPos + index) < searchWithin.Length)
                {
                    if (searchWithin[startPos + index] == serachFor[index])
                    {
                        index++;
                        if (index == serachFor.Length)
                        {
                            return startPos;
                        }
                    }
                    else
                    {
                        startPos = Array.IndexOf<byte>(searchWithin, serachFor[0], startPos + index);
                        if (startPos == -1)
                        {
                            return -1;
                        }
                        index = 0;
                    }
                }
            }

            return -1;
        }

        public static byte[] ToByteArray(Stream stream)
        {
            byte[] buffer = new byte[32768];
            using (MemoryStream ms = new MemoryStream())
            {
                while (true)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return ms.ToArray();
                    ms.Write(buffer, 0, read);
                }
            }
        }


        public static DateTime FormatedDate(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(0, 2));
                dd = (dd > 31 || dd < 1) ? 1 : dd;

                var mm = Convert.ToInt32(date.Substring(2, 2));
                mm = (mm > 12 || mm < 1) ? 1 : mm;

                var yy = 0;
                if (date.Length == 6)
                {
                    yy = Convert.ToInt32(date.Substring(4, 2));
                    yy = (yy > 7999 || yy < 1) ? 1 : yy;
                    yy += 2000;
                }
                else
                {
                    yy = Convert.ToInt32(date.Substring(4, 4));
                }


                var h = Convert.ToInt32(time.Substring(0, 2));
                h = (h > 23 || h < 0) ? 1 : h;

                var m = Convert.ToInt32(time.Substring(2, 2));
                m = (m > 59 || m < 0) ? 1 : m;

                var s = Convert.ToInt32(time.Substring(4, 2));
                s = (s > 59 || s < 1) ? 1 : s;

                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }

        public static DateTime FormatedDateFromMMDDYYLive(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(2, 2));


                var mm = Convert.ToInt32(date.Substring(0, 2));



                var yy = Convert.ToInt32(date.Substring(4, 4));


                var h = Convert.ToInt32(time.Substring(0, 2));


                var m = Convert.ToInt32(time.Substring(2, 2));


                var s = Convert.ToInt32(time.Substring(4, 2));


                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }


        public static DateTime FormatedDateFromMMDDYY(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(3, 2));


                var mm = Convert.ToInt32(date.Substring(0, 2));



                var yy = Convert.ToInt32(date.Substring(6, 4));


                var h = Convert.ToInt32(time.Substring(0, 2));


                var m = Convert.ToInt32(time.Substring(3, 2));


                var s = Convert.ToInt32(time.Substring(6, 2));


                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }

        public static bool validateEmailId(string emailid)
        {

            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(emailid);
            if (match.Success)
                return true;
            else
                return false;
        }

        public Stream GenerateStreamFromString(string s)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(s));
        }

        public bool SaveFileToS3Bucket(string uniqueName, Stream stream, string folderName)
        {
            try
            {
                var filePath = "";
                filePath = GetS3FolderName(folderName) + uniqueName;
                AmazonS3 client;
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
                {
                    PutObjectRequest putRequest = new PutObjectRequest();
                    putRequest.WithBucketName(ConfigurationReader.S3Bucket)
                    .WithCannedACL(S3CannedACL.PublicRead)
                    .WithKey(filePath).InputStream = stream;
                    S3Response s3Response = client.PutObject(putRequest);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteFileFromS3Bucket(string uniqueName, string folderName)
        {
            try
            {
                var filePath = "";
                filePath = GetS3FolderName(folderName) + uniqueName;
                AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey);
                DeleteObjectRequest request = new DeleteObjectRequest();
                request.BucketName = ConfigurationReader.S3Bucket;
                request.Key = filePath;   //"folder/folder/file.pdf";
                s3Client.DeleteObject(request);
                s3Client.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public void UploadFile(string localFilePath, string fileName, string S3folderName)
        {
            try
            {
                var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey);
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    FilePath = localFilePath,
                    BucketName = ConfigurationReader.S3Bucket,
                    CannedACL = S3CannedACL.PublicRead,
                    Key = GetS3FolderName(S3folderName) + fileName
                };
                var fileTransferUtility = new TransferUtility(client);
                fileTransferUtility.Upload(uploadRequest);
            }
            catch (Exception ex)
            {
                //logger.Info("exception while upload the file on s3 bucket");
            }
        }

        public void UploadFolderToS3Bucket(string directoryPath, string S3folderName)
        {
            var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey);

            var uploadRequest = new Amazon.S3.Transfer.TransferUtilityUploadDirectoryRequest
            {

                Directory = directoryPath,
                BucketName = ConfigurationReader.S3Bucket,
                CannedACL = S3CannedACL.PublicRead,
                KeyPrefix = GetS3FolderName(S3folderName),
                SearchOption = SearchOption.AllDirectories
            };

            var fileTransferUtility = new Amazon.S3.Transfer.TransferUtility(client);
            fileTransferUtility.UploadDirectory(uploadRequest);
        }

        public bool IsFileExistOnS3(string folderName, string fileName)
        {

            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
            {
                Stream rs = new System.IO.MemoryStream();
                GetObjectRequest getObjectRequest = new GetObjectRequest();
                getObjectRequest.BucketName = ConfigurationReader.S3Bucket;
                getObjectRequest.Key = GetS3FolderName(folderName) + fileName;

                try
                {
                    using (var getObjectResponse = client.GetObject(getObjectRequest))
                    {
                        //return getObjectResponse.ResponseStream;
                        //below 3 lines of code is to save the s3 bucket file to the local path
                        //// getObjectResponse.ResponseStream.CopyTo(rs);
                        //using (var fileStream = new FileStream(@"D:\Hello.txt", FileMode.Create, FileAccess.Write))
                        //{
                        //    getObjectResponse.ResponseStream.CopyTo(fileStream);
                        //}
                        ////System.Drawing.Image img = System.Drawing.Image.FromStream(rs);
                        ////img.Save("D:\\ttt.png", ImageFormat.Png);
                    }

                }
                catch (Exception ex)
                {
                    //Error occurs when the file is not exist on the server
                    return false;
                }
                return true;
            }
        }

        public void GetAllFiles()
        {
            try
            {
                using (var _client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
                {
                    ListObjectsRequest request = new ListObjectsRequest();
                    request.BucketName = ConfigurationReader.S3Bucket; //Amazon Bucket Name
                    request.Prefix = ConfigurationReader.S3BucketEnv  + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/";  //Amazon S3 Folder path           

                    ListObjectsResponse response = _client.ListObjects(request);//_clien

                }
            }
            catch (Exception ex)
            {

            }
        }

        public decimal? GetFileSizeFromS3BucketFile(string folderName, string fileName)
        {
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
            {
                Stream rs = new System.IO.MemoryStream();
                GetObjectRequest getObjectRequest = new GetObjectRequest();
                getObjectRequest.BucketName = ConfigurationReader.S3Bucket;
                getObjectRequest.Key = GetS3FolderName(folderName) + fileName;

                try
                {
                    using (var getObjectResponse = client.GetObject(getObjectRequest))
                    {
                       return decimal.Round((((decimal)getObjectResponse.ContentLength / 1024) / 1024), 3);
                    }
                }
                catch (Exception ex)
                {
                 
                }
            }
            return null;
        }


        public Stream ReadFileFromS3(string folderName, string fileName, out bool isFileExist)
        {
            isFileExist = true;
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
            {
                Stream rs = new System.IO.MemoryStream();
                GetObjectRequest getObjectRequest = new GetObjectRequest();
                getObjectRequest.BucketName = ConfigurationReader.S3Bucket;
                getObjectRequest.Key = GetS3FolderName(folderName) + fileName;

                try
                {
                    using (var getObjectResponse = client.GetObject(getObjectRequest))
                    {

                        return getObjectResponse.ResponseStream;
                        //below 3 lines of code is to save the s3 bucket file to the local path
                        //// getObjectResponse.ResponseStream.CopyTo(rs);
                        //using (var fileStream = new FileStream(@"D:\Hello.txt", FileMode.Create, FileAccess.Write))
                        //{
                        //    getObjectResponse.ResponseStream.CopyTo(fileStream);
                        //}
                        ////System.Drawing.Image img = System.Drawing.Image.FromStream(rs);
                        ////img.Save("D:\\ttt.png", ImageFormat.Png);
                    }

                }
                catch (Exception ex)
                {
                    //Error occurs when the file is not exist on the server
                    isFileExist = false;
                }
                return rs;
            }
        }

        public string GetS3FolderName(string folderName)
        {
            string path = "";
            switch (folderName)
            {
                case "Doc":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketDocPath;
                    break;
                case "MapFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketMapFilePath;
                    break;
                case "Image":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages;
                    break;
                case "IPassVideo":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassVideo;
                    break;
                case "IPassImage":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassImage;
                    break;
                case "IPassPDF":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassPDF;
                    break;
                case "PlateJson":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlateJsonFolder;
                    break;
                case "PlatePNGAndTxt":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlatePNGAndTxtFolder;
                    break;
                case "AudioFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketAudioFilePath;
                    break;
                case "HardWareFirmware":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketHardwareFirmwareFilePath;
                    break;
                case "DefaultImages":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages;
                    break;
                case "KmlFiles":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketKmlFile;
                    break;
                case "FdrFiles":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketFdrFile;
                    break;
                case "JsonMisc":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketJsonMiscPath;
                    break;
                case "PlatePNG":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/";
                    break;
                case "GoogleMapSnapshopPath":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketGoogleMapSnapshot;
                    break;
                case "EngineeringDocs":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketEngineeringDocs;
                    break;
                case "KitAssembly":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.KitAssembly;
                    break;
                case "PlatePNGForAllState":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlatePNGForAllState;
                    break;
                case "GAVDocs":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketGAVDocs;
                    break;
            }
            return path;
        }
    }
}
