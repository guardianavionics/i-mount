﻿namespace GA.Common
{
    public class Constants
    {
        // keys for documents
        public const string DocumentPath = "DocumentPath";

        public const string DocumentServerPath = "DocumentServerPath";


        // general keys
        public const string ImagePathKey = "ImagePath";

        public const string LogPathKey = "LogPath";

        public const string AltLogPathKey = "AltLogPath";

        public const string ImageServerPathKey = "ImageServerPath";

        public const string EmailIdForSendingEmail = "EmailIdForSendingEmail";

        public const string EmailPasswordForSendingEmail = "EmailPasswordForSendingEmail";

        public const string Host = "Host";

        public const string Port = "Port";

        public const string EmailTo = "EmailTo";

        public const string EnableSSL = "EnableSSL";

        // for dropbox
        public const string DropboxExcelSheetPath = "DropboxExcelSheetPath";

        public const string DropboxConsumerKey = "DropboxConsumerKey";

        public const string DropboxConsumerSecret = "DropboxConsumerSecret";

        public const string DropboxCallBackUrl = "DropboxCallBackUrl";

        public const string IsPushNotificationForDistributionCertificate = "IsPushNotificationForDistributionCertificate";

      

        //public const string DropboxRestApiBaseUri = "";

        //public const string DropboxRestApiAuthorizeBaseUri = "";

        //public const string 

        //    public const string


        // fuel unit constants
        public const double LiterToGallon = 0.264172052637296;

        public const double GalonToLiter = 1 / LiterToGallon;

        // vertical speed unit constants
        public const double FtminToMs = 0.01;

        public const double MsToFtmin = 1 / FtminToMs;

        // speed unit constants
        public const double MphToKts = 0.868976;

        public const double KtsToMph = 1 / MphToKts;

        public const double KmhToKts = 0.539957;

        public const double KtsToKmh = 1 / KmhToKts;

        public const double KmhToMph = 0.621371;

        public const double MphToKmh = 1 / KmhToMph;

        public const int RoundOffDigit = 2;


        //public const int PageSizeJpiTable = 5;

        public const int PageSizeTable = 10;


        public const string Save = "Save";

        public const string Edit = "Edit";

        public const string Create = "Create";


        public const string JpiAircraft = "Aircraft";

        public const string JpiFlight = "Flight";

        public const string JpiDateTime = "DateTime";

        public const string JpiAll = "All";

        public const string JpiMobile = "Mobile";

        public const string JpiLastFlight = "DefaiultLastFlight";

        public const string JpiMultipleFlight = "MultipleFlights";



        //public const string regularexpressionemail = @"\"^(?(\"\")(\"\".+?\"\"@)|(([0-9a-za-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-za-z])@))" +
        //                                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-za-z][-\w]*[0-9a-za-z]\.)+[a-za-z]{2,6}))$";

        //    @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +

        //    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))    



        // push notification

        public const string IPhoneCerficatePath = "IPhoneCerficatePath";


        // kml file for map

        public const string KmlFilePath = "KmlFilePath";

        public const string FDRFilePath = "FDRFilePath";

        public const string KmlFileServerPath = "KmlFileServerPath";
        public const string FDRFileServerPath = "FDRFileServerPath";

        // constants for file extentions 

        public const string Pdf = ".pdf";

        public const string xls = ".xls";

        public const string xlsx = ".xlsx";

        public const string doc = ".doc";

        public const string docx = ".docx";

        public const string ppt = ".ppt";

        public const string pptx = ".pptx";

        public const string rtf = ".rft";

        public const string key = ".key";

        public const string number = ".number";

        public const string pages = ".pages";

        public const string jpg = ".jpg";

        public const string jpeg = ".jpeg";

        public const string png = ".png";

        public const string txt = ".txt";



        public const string ContentTypeForXlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public const string ContextTypeForKml = "application/vnd.google-earth.kml+xml";


        public const string AircraftDefaultImage = "../Images/GA/profile_pic.png";

        public const string UserProfileDefaultImage = "../Images/GA/User_Profile.jpg";

        public const string Comma = ",";

        public const string SessionTimeOutConstant = "_Logon_";

        public const string AdminSessionTimeOutConstant = "_Logon_Admin";

        public const string NotApplicable = "Not available";

        public const string Expired = "Expired";

        public const string DayPICDefaultValue = "00:00:00";

        public const string KmlFileDemo = "http://kmlscribe.googlepages.com/SamplesInMaps.kml";


        public const string ImageTypePng = "image/png";

        public const string ServerUrl = "ServerUrl";

        public const string ServerUrlToResetPassword = "ServerUrlToResetPassword";

        // map file path
        public const string MapFile = "MapFile";

        public const string HardwareFirmwareFilePath = "HardwareFirmwareFilePath";

        public const string DepricateCurrentVersion = "DepricateCurrentVersion";

        public const string CertificatePath = "CertificatePath";

        public const string IPhonePNServerUrl = "IPhonePNServerUrl";

        // Components

        public const string Comm = "Comm";
        public const string Transponder = "Transponder";
        public const string EngineMonitor = "EngineMonitor";

        public const string DataFilePath = "DataFilePath";

        public const string AudioFilePath = "AudioFilePath";

        public const string NoOfRowsForAirframeDataLog = "NoOfRowsForAirframeDataLog";

        public const string NoOfRowsForFlightList = "NoOfRowsForFlightList";

        public const string FacebookAppId = "FacebookAppId";

        public const string UnZipFilePath = "UnZipFilePath";

        public const string GmailClientId = "GmailClientId";

        public const string GmailClientSecret = "GmailClientSecret";

        public const string EngineConfigXMLFilePath = "EngineConfigXMLFilePath";

        public const string GlobalEncryptionKey = "zRmz90oH15p9nc1p";

        public const string IsLoggerEnabled = "IsLoggerEnabled";

        public const string IPassengerMediaPath = "IPassengerMediaPath";

        public const string ChartFilePath = "ChartFilePath";
        public const string ChartPlatesPath = "ChartPlatesPath";

        public const string SqliteFilePath = "SqliteFilePath";

        public const string SqliteFilePathForPlates = "SqliteFilePathForPlates";

        public const string PlateFilePath = "PlateFilePath";

        public const string AircraftLogFilePath = "AircraftLogFilePath";

        public const string IsNewVersionAvailable = "IsNewVersionAvailable";

        public const string GoogleAnalyticsId = "GoogleAnalyticsId";

        public const string AmazonAccessKey = "AmazonAccessKey";

        public const string AmazonSecretKey = "AmazonSecretKey";

        public const string S3Bucket = "S3Bucket";

        public const string BucketDocPath = "BucketDocPath";

        public const string S3BucketEnv = "S3BucketEnv";

        public const string BucketMapFilePath = "BucketMapFilePath";

        public const string s3BucketImages = "s3BucketImages";

        public const string s3BuckeyURL = "s3BuckeyURL";

        public const string BucketIPassVideo = "BucketIPassVideo";

        public const string BucketIPassImage = "BucketIPassImage";

        public const string BucketIPassPDF = "BucketIPassPDF";

        public const string BucketKmlFile = "BucketKmlFile";

        public const string BucketFdrFile = "BucketFdrFile";

        public const string BucketPlateJsonFolder = "BucketPlateJsonFolder";

        public const string BucketPlatePNGAndTxtFolder = "BucketPlatePNGAndTxtFolder";

        public const string BucketAudioFilePath = "BucketAudioFilePath";

        public const string BucketHardwareFirmwareFilePath = "BucketHardwareFirmwareFilePath";

        public const string s3BucketDefaultImages = "s3BucketDefaultImages";

        public const string WebSocketURL = "WebSocketURL";

        public const string ExportFilePath = "ExportFilePath";

        public const string BucketJsonMiscPath = "BucketJsonMiscPath";

        public const string GoogleMapSnapshopPath = "GoogleMapSnapshopPath";

        public const string s3BucketGoogleMapSnapshot = "s3BucketGoogleMapSnapshot";


        public const string paypalEmailTemplatePath = "paypalEmailTemplatePath";

        public const string InappEmailTemplatePath = "InappEmailTemplatePath";

        public const string FreeSubscriptionFrequency = "FreeSubscriptionFrequency";

        public const string FreeSubscriptionFrequencyInterval = "FreeSubscriptionFrequencyInterval";

        public const string urlSubmitPayment = "urlSubmitPayment";

        public const string IsPaypalSandbox = "IsPaypalSandbox";

        public const string Token = "Token";

        public const string PaypalMode = "PaypalMode";

        public const string InAppURL = "InAppURL";

        public const string apiUsername = "apiUsername";

        public const string apiPassword = "apiPassword";

        public const string apiSignature = "apiSignature";

        public const string IsSubscriptionEnable = "IsSubscriptionEnable";

        public const string ServerName = "ServerName";

        public const string s3BucketEngineeringDocs = "s3BucketEngineeringDocs";

        public const string PartZipFilePath = "PartZipFilePath";

        public const string EmailTemplatePath = "EmailTemplatePath";

        public const string KitAssembly = "KitAssembly";

        public const string BucketPlatePNGForAllState = "BucketPlatePNGForAllState";

        public const string RMAStatusReportSchedulrTime = "RMAStatusReportSchedulrTime";

        public const string s3BucketGAVDocs = "s3BucketGAVDocs";

        public const string TwiloAccountSid = "TwiloAccountSid";
        public const string TwiloAuthToken = "TwiloAuthToken";
        public const string TwiloFromuserNumber = "TwiloFromuserNumber";
    }
}
