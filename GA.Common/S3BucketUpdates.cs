﻿using Amazon.S3;

using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GA.Common
{
   public class S3BucketUpdates
    {
        public bool SaveFileToS3BucketEngDocs(string uniqueName, Stream stream, string folderPath)
        {
            try
            {
                var filePath = "";
                filePath = new Misc().GetS3FolderName("EngineeringDocs") + folderPath + "/" + uniqueName;
                AmazonS3 client;
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
                {
                    PutObjectRequest putRequest = new PutObjectRequest();
                    putRequest.WithBucketName(ConfigurationReader.S3Bucket)
                    .WithCannedACL(S3CannedACL.PublicRead)
                    .WithKey(filePath).InputStream = stream;
                    S3Response s3Response = client.PutObject(putRequest);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SaveFileToS3BucketKitAssembly(string uniqueName, Stream stream)
        {
            try
            {
                var filePath = "";
                filePath = new Misc().GetS3FolderName("KitAssembly") +  uniqueName;
                AmazonS3 client;
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
                {
                    PutObjectRequest putRequest = new PutObjectRequest();
                    putRequest.WithBucketName(ConfigurationReader.S3Bucket)
                    .WithCannedACL(S3CannedACL.PublicRead)
                    .WithKey(filePath).InputStream = stream;
                    S3Response s3Response = client.PutObject(putRequest);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public void CreateFolderOns3(string folderName, string folderPath)
        //{
        //    var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey);

        //    PutObjectRequest folderRequest = new PutObjectRequest();

        //    folderRequest.BucketName = ConfigurationReader.S3Bucket;
        //    string folderKey = new Misc().GetS3FolderName("EngineeringDocs") + folderPath + folderName + "/";
        //    folderRequest.Key = folderKey;
        //    folderRequest.InputStream = new MemoryStream(new byte[0]);
        //    PutObjectResponse folderResponse = client.PutObject(folderRequest);
        //}

        public void RenameFile()
        {
            AmazonS3Client s3 = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey);
            
            CopyObjectRequest copyRequest = new CopyObjectRequest()
                  .WithSourceBucket("guardianavionics")
                  .WithSourceKey("GuardianTest/EngineeringDocs/A.txt")
                  .WithDestinationBucket("guardianavionics")
                  .WithDestinationKey("GuardianTest/EngineeringDocs/AAA.txt")
                  .WithCannedACL(S3CannedACL.PublicRead);
            s3.CopyObject(copyRequest);

            //Delete the original
            //DeleteObjectRequest deleteRequest = new DeleteObjectRequest()
            //       .WithBucketName("SourceBucket")
            //       .WithKey("SourceKey");
            //s3.DeleteObject(deleteRequest);
        }


        #region GAVDocs
        public bool SaveFileToS3BucketGAVDocs(string uniqueName, Stream stream, string folderPath)
        {
            try
            {
                var filePath = "";
                filePath = new Misc().GetS3FolderName("GAVDocs") + folderPath + "/" + uniqueName;
                AmazonS3 client;
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
                {
                    PutObjectRequest putRequest = new PutObjectRequest();
                    putRequest.WithBucketName(ConfigurationReader.S3Bucket)
                    .WithCannedACL(S3CannedACL.PublicRead)
                    .WithKey(filePath).InputStream = stream;
                    S3Response s3Response = client.PutObject(putRequest);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion GAVDocs


    }
}
