﻿using System;
using System.Configuration;


namespace GA.Common
{
    /// <summary>
    /// Configuration Reader Class used to read values from web.config file of given key
    /// </summary>
    public class ConfigurationReader
    {

        /// <summary>
        /// Returns a trimmed string for the specified AppSettings key
        /// </summary>
        /// <param name="key">The App/Web key</param>
        /// <returns>value of the key</returns>
        public static string AppSetting(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key].Trim();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, " key not found = " + key);

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Image Path value.
        /// Path where image is to be saved on server.
        /// </summary>
        /// <value>Path where Image is saved</value>
        public static string ImagePathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ImagePathKey)))
                {
                    return AppSetting(Constants.ImagePathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Image Server Path value.
        /// Path where is hosted on server.
        /// </summary>
        /// <value>Path where Image is saved</value>
        public static string ImageServerPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ImageServerPathKey)))
                {
                    return AppSetting(Constants.ImageServerPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Error logging path.
        /// </summary>
        /// <value>Path where Error is saved</value>
        public static string LogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.LogPathKey)))
                {
                    return AppSetting(Constants.LogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config Key used to get Alt Image Path value
        /// </summary>
        /// <value>Path where Alt Image is saved</value>
        public static string AltLogPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AltLogPathKey)))
                {
                    return AppSetting(Constants.AltLogPathKey);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key usede to get the path to save the dropbox Excel sheets
        /// </summary>
        public static string DropboxExcelSheetPath
        {
            get
            {
                if (!string.IsNullOrEmpty(Constants.DropboxExcelSheetPath))
                {
                    return AppSetting(Constants.DropboxExcelSheetPath);
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key usede to get the Consumer key for dropbox application for Guardaian avionics project
        /// </summary>
        public static string DropboxConsumerKey
        {
            get
            {
                if (!string.IsNullOrEmpty(Constants.DropboxConsumerKey))
                {
                    return AppSetting(Constants.DropboxConsumerKey);
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key usede to get the Consumer Secret for dropbox application for Guardaian avionics project
        /// </summary>
        public static string DropboxConsumerSecret
        {
            get
            {
                return !string.IsNullOrEmpty(Constants.DropboxConsumerSecret) ? AppSetting(Constants.DropboxConsumerSecret) : string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key used to get the emial id for sending email.
        /// </summary>
        public static string EmailId
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EmailIdForSendingEmail)))
                {
                    return AppSetting(Constants.EmailIdForSendingEmail);
                }

                return string.Empty;
            }
        }

        public static string EmailTo
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EmailTo)))
                {
                    return AppSetting(Constants.EmailTo);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key used to get the password of email Id , used for sending email
        /// </summary>
        public static string EmailPassword
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EmailPasswordForSendingEmail)))
                {
                    return AppSetting(Constants.EmailPasswordForSendingEmail);
                }

                return string.Empty;
            }
        }


        /// <summary>
        /// Gets the web.config key used to get the HostName , used for sending email
        /// </summary>
        public static string HostName
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.Host)))
                {
                    return AppSetting(Constants.Host);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key used to get the Port No. , used for sending email
        /// </summary>
        public static string PortNo
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.Port)))
                {
                    return AppSetting(Constants.Port);
                }

                return string.Empty;
            }
        }

        public static string EnableSSL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EnableSSL)))
                {
                    return AppSetting(Constants.EnableSSL);
                }

                return string.Empty;
            }
        }


        /// <summary>
        /// Gets the web.config key used to get the Kml file path of server
        /// </summary>
        public static string KmlFilePath
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.KmlFilePath)) ? AppSetting(Constants.KmlFilePath) : string.Empty;
            }
        }
        public static string FdrFilePath
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.FDRFilePath)) ? AppSetting(Constants.FDRFilePath) : string.Empty;
            }
        }
        

        /// <summary>
        /// Gets the web.config key used to get the KmlFileServerPath for downloading kml file
        /// </summary>
        public static string KmlFileServerPath
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.KmlFileServerPath)) ? AppSetting(Constants.KmlFileServerPath) : string.Empty;
            }
        }
        public static string FDRFileServerPath
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.FDRFileServerPath)) ? AppSetting(Constants.KmlFileServerPath) : string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key used to get the IPhone Cerficate Path for sending push notification
        /// </summary>
        public static string IPhoneCerficatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IPhoneCerficatePath)))
                {
                    return AppSetting(Constants.IPhoneCerficatePath);
                }

                return string.Empty;
            }
        }


        /// <summary>
        /// Gets the web.config key used to get the url of document
        /// </summary>
        public static string DocumentPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.DocumentPath)))
                {
                    return AppSetting(Constants.DocumentPath);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the web.config key used to get the server path key of document
        /// </summary>
        public static string DocumentServerPathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.DocumentServerPath)))
                {
                    return AppSetting(Constants.DocumentServerPath);
                }

                return string.Empty;
            }
        }

        public static string HardwareFirmwarePathKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.HardwareFirmwareFilePath)))
                {
                    return AppSetting(Constants.HardwareFirmwareFilePath);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Get the web.config key used to get the callBackUrl for dropbox
        /// </summary>
        public static string DropboxCallBackUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.DropboxCallBackUrl)))
                {
                    return AppSetting(Constants.DropboxCallBackUrl);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Get the web.config key used to get the server url
        /// </summary>
        public static string ServerUrl
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.ServerUrl)) ? AppSetting(Constants.ServerUrl) : string.Empty;
            }
        }

        public static string ServerUrlToResetPassword
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.ServerUrlToResetPassword)) ? AppSetting(Constants.ServerUrlToResetPassword) : string.Empty;
            }
        }

        /// <summary>
        /// Get the web.config key used to get the map files server url
        /// </summary>
        public static string MapFileUrl
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.MapFile))
                    ? AppSetting(Constants.MapFile)
                    : string.Empty;
            }
        }

        /// <summary>
        /// Get the web.config key used to get the map files server url
        /// </summary>
        public static string DepricateCurrentVersion
        {
            get
            {
                return !string.IsNullOrEmpty(AppSetting(Constants.DepricateCurrentVersion))
                    ? AppSetting(Constants.DepricateCurrentVersion)
                    : string.Empty;
            }
        }

        public static string CertificatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.CertificatePath)))
                {
                    return AppSetting(Constants.CertificatePath);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        public static string IPhonePNServerUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IPhonePNServerUrl)))
                {
                    return AppSetting(Constants.IPhonePNServerUrl);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        public static string Comm
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.Comm)))
                {
                    return AppSetting(Constants.Comm);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }


        public static string Transponder
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.Transponder)))
                {
                    return AppSetting(Constants.Transponder);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        public static string EngineMonitor
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EngineMonitor)))
                {
                    return AppSetting(Constants.EngineMonitor);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }


        public static string NoOfRowsForAirframeDataLog
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.NoOfRowsForAirframeDataLog)))
                {
                    return AppSetting(Constants.NoOfRowsForAirframeDataLog);
                }
                return string.Empty;
            }
        }

        public static string NoOfRowsForFlightList
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.NoOfRowsForFlightList)))
                {
                    return AppSetting(Constants.NoOfRowsForFlightList);
                }
                return string.Empty;
            }
        }

        public static string IsPushNotificationForDistributionCertificate
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsPushNotificationForDistributionCertificate)))
                {
                    return AppSetting(Constants.IsPushNotificationForDistributionCertificate);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        public static string DataFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.DataFilePath)))
                {
                    return AppSetting(Constants.DataFilePath);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }

        public static string AudioFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AudioFilePath)))
                {
                    return AppSetting(Constants.AudioFilePath);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }


        public static string FacebookAppId
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.FacebookAppId)))
                {
                    return AppSetting(Constants.FacebookAppId);
                }

                //if we do not get path then return default path which should be the structure of folder where
                //service is hosted.
                return string.Empty;
            }
        }


        /// <summary>
        /// Gets the web.config Key used to get ZipFile path
        /// </summary>
        /// <value>time interval of service</value>
        public static string UnZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.UnZipFilePath)))
                {
                    return AppSetting(Constants.UnZipFilePath);
                }


                return string.Empty;
            }
        }

        public static string GmailClientId
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.GmailClientId)))
                {
                    return AppSetting(Constants.GmailClientId);
                }
                return string.Empty;
            }
        }

        public static string GmailClientSecret
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.GmailClientSecret)))
                {
                    return AppSetting(Constants.GmailClientSecret);
                }
                return string.Empty;
            }
        }

        public static string EngineConfigXMLFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EngineConfigXMLFilePath)))
                {
                    return AppSetting(Constants.EngineConfigXMLFilePath);
                }
                return string.Empty;
            }
        }

        public static string IsLoggerEnabled
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsLoggerEnabled)))
                {
                    return AppSetting(Constants.IsLoggerEnabled);
                }
                return string.Empty;
            }
        }

        public static string IPassengerMediaPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IPassengerMediaPath)))
                {
                    return AppSetting(Constants.IPassengerMediaPath);
                }
                return string.Empty;
            }
        }


        public static string ChartFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ChartFilePath)))
                {
                    return AppSetting(Constants.ChartFilePath);
                }
                return string.Empty;
            }
        }
        public static string ChartPlatesPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ChartPlatesPath)))
                {
                    return AppSetting(Constants.ChartPlatesPath);
                }
                return string.Empty;
            }
        }

        public static string SqliteFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.SqliteFilePath)))
                {
                    return AppSetting(Constants.SqliteFilePath);
                }
                return string.Empty;
            }
        }

        public static string SqliteFilePathForPlates
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.SqliteFilePathForPlates)))
                {
                    return AppSetting(Constants.SqliteFilePathForPlates);
                }
                return string.Empty;
            }
        }

        public static string PlateFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.PlateFilePath)))
                {
                    return AppSetting(Constants.PlateFilePath);
                }
                return string.Empty;
            }
        }

        public static string AircraftLogFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AircraftLogFilePath)))
                {
                    return AppSetting(Constants.AircraftLogFilePath);
                }
                return string.Empty;
            }
        }

        public static string IsNewVersionAvailable
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsNewVersionAvailable)))
                {
                    return AppSetting(Constants.IsNewVersionAvailable);
                }
                return string.Empty;
            }
        }

        public static string GoogleAnalyticsId
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.GoogleAnalyticsId)))
                {
                    return AppSetting(Constants.GoogleAnalyticsId);
                }
                return string.Empty;
            }
        }


        public static string AmazonAccessKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AmazonAccessKey)))
                {
                    return AppSetting(Constants.AmazonAccessKey);
                }
                return string.Empty;
            }
        }


        public static string AmazonSecretKey
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.AmazonSecretKey)))
                {
                    return AppSetting(Constants.AmazonSecretKey);
                }
                return string.Empty;
            }
        }

        public static string S3Bucket
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.S3Bucket)))
                {
                    return AppSetting(Constants.S3Bucket);
                }
                return string.Empty;
            }
        }

        public static string BucketDocPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketDocPath)))
                {
                    return AppSetting(Constants.BucketDocPath);
                }
                return string.Empty;
            }
        }

        public static string S3BucketEnv
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.S3BucketEnv)))
                {
                    return AppSetting(Constants.S3BucketEnv);
                }
                return string.Empty;
            }
        }

        public static string BucketMapFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketMapFilePath)))
                {
                    return AppSetting(Constants.BucketMapFilePath);
                }
                return string.Empty;
            }
        }


        public static string s3BucketDefaultImages
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketDefaultImages)))
                {
                    return AppSetting(Constants.s3BucketDefaultImages);
                }
                return string.Empty;
            }
        }

        public static string s3BucketImages
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketImages)))
                {
                    return AppSetting(Constants.s3BucketImages);
                }
                return string.Empty;
            }
        }

        public static string s3BucketURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BuckeyURL)))
                {
                    return AppSetting(Constants.s3BuckeyURL);
                }
                return string.Empty;
            }
        }
        //twilo//
        public static string TwiloAccountSid
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.TwiloAccountSid)))
                {
                    return AppSetting(Constants.TwiloAccountSid);
                }
                return string.Empty;
            }
        }
        public static string TwiloAuthToken
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.TwiloAuthToken)))
                {
                    return AppSetting(Constants.TwiloAuthToken);
                }
                return string.Empty;
            }
        }
        public static string TwiloFromuserNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.TwiloFromuserNumber)))
                {
                    return AppSetting(Constants.TwiloFromuserNumber);
                }
                return string.Empty;
            }
        }



        public static string BucketIPassVideo
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassVideo)))
                {
                    return AppSetting(Constants.BucketIPassVideo);
                }
                return string.Empty;
            }
        }
        public static string BucketIPassImage
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassImage)))
                {
                    return AppSetting(Constants.BucketIPassImage);
                }
                return string.Empty;
            }
        }
        public static string BucketIPassPDF
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketIPassPDF)))
                {
                    return AppSetting(Constants.BucketIPassPDF);
                }
                return string.Empty;
            }
        }

        public static string BucketKmlFile
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketKmlFile)))
                {
                    return AppSetting(Constants.BucketKmlFile);
                }
                return string.Empty;
            }
        }
        public static string BucketFdrFile
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketFdrFile)))
                {
                    return AppSetting(Constants.BucketFdrFile);
                }
                return string.Empty;
            }
        }



        public static string BucketPlateJsonFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketPlateJsonFolder)))
                {
                    return AppSetting(Constants.BucketPlateJsonFolder);
                }
                return string.Empty;
            }
        }


        public static string BucketPlatePNGAndTxtFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketPlatePNGAndTxtFolder)))
                {
                    return AppSetting(Constants.BucketPlatePNGAndTxtFolder);
                }
                return string.Empty;
            }
        }

        public static string BucketAudioFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketAudioFilePath)))
                {
                    return AppSetting(Constants.BucketAudioFilePath);
                }
                return string.Empty;
            }
        }
        public static string BucketHardwareFirmwareFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketHardwareFirmwareFilePath)))
                {
                    return AppSetting(Constants.BucketHardwareFirmwareFilePath);
                }
                return string.Empty;
            }
        }
        public static string WebSocketURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.WebSocketURL)))
                {
                    return AppSetting(Constants.WebSocketURL);
                }
                return string.Empty;
            }
        }

        public static string ExportFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ExportFilePath)))
                {
                    return AppSetting(Constants.ExportFilePath);
                }
                return string.Empty;
            }
        }


        public static string BucketJsonMiscPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketJsonMiscPath)))
                {
                    return AppSetting(Constants.BucketJsonMiscPath);
                }
                return string.Empty;
            }
        }

        public static string GoogleMapSnapshopPath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.GoogleMapSnapshopPath)))
                {
                    return AppSetting(Constants.GoogleMapSnapshopPath);
                }
                return string.Empty;
            }
        }

        public static string s3BucketGoogleMapSnapshot
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketGoogleMapSnapshot)))
                {
                    return AppSetting(Constants.s3BucketGoogleMapSnapshot);
                }
                return string.Empty;
            }
        }
        public static string s3BucketEngineeringDocs
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketEngineeringDocs)))
                {
                    return AppSetting(Constants.s3BucketEngineeringDocs);
                }
                return string.Empty;
            }
        }

        public static string s3BucketGAVDocs
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.s3BucketGAVDocs)))
                {
                    return AppSetting(Constants.s3BucketGAVDocs);
                }
                return string.Empty;
            }
        }


        public static string paypalEmailTemplatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.paypalEmailTemplatePath)))
                {
                    return AppSetting(Constants.paypalEmailTemplatePath);
                }
                return string.Empty;
            }
        }

        public static string InappEmailTemplatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.InappEmailTemplatePath)))
                {
                    return AppSetting(Constants.InappEmailTemplatePath);
                }
                return string.Empty;
            }
        }

        public static string FreeSubscriptionFrequency
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.FreeSubscriptionFrequency)))
                {
                    return AppSetting(Constants.FreeSubscriptionFrequency);
                }
                return string.Empty;
            }
        }

        public static string FreeSubscriptionFrequencyInterval
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.FreeSubscriptionFrequencyInterval)))
                {
                    return AppSetting(Constants.FreeSubscriptionFrequencyInterval);
                }
                return string.Empty;
            }
        }

        public static string urlSubmitPayment
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.urlSubmitPayment)))
                {
                    return AppSetting(Constants.urlSubmitPayment);
                }
                return string.Empty;
            }
        }
        public static string IsPaypalSandbox
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsPaypalSandbox)))
                {
                    return AppSetting(Constants.IsPaypalSandbox);
                }
                return string.Empty;
            }
        }

        public static string Token
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.Token)))
                {
                    return AppSetting(Constants.Token);
                }
                return string.Empty;
            }
        }

        public static string PaypalMode
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.PaypalMode)))
                {
                    return AppSetting(Constants.PaypalMode);
                }
                return string.Empty;
            }
        }

        public static string InAppURL
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.InAppURL)))
                {
                    return AppSetting(Constants.InAppURL);
                }
                return string.Empty;
            }
        }

        public static string apiUsername
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.apiUsername)))
                {
                    return AppSetting(Constants.apiUsername);
                }
                return string.Empty;
            }
        }


        public static string apiPassword
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.apiPassword)))
                {
                    return AppSetting(Constants.apiPassword);
                }
                return string.Empty;
            }
        }

        public static string apiSignature
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.apiSignature)))
                {
                    return AppSetting(Constants.apiSignature);
                }
                return string.Empty;
            }
        }

        public static string IsSubscriptionEnable
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.IsSubscriptionEnable)))
                {
                    return AppSetting(Constants.IsSubscriptionEnable);
                }
                return string.Empty;
            }
        }

        public static string ServerName
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.ServerName)))
                {
                    return AppSetting(Constants.ServerName);
                }
                return string.Empty;
            }
        }

        public static string PartZipFilePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.PartZipFilePath)))
                {
                    return AppSetting(Constants.PartZipFilePath);
                }
                return string.Empty;
            }
        }

        public static string EmailTemplatePath
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.EmailTemplatePath)))
                {
                    return AppSetting(Constants.EmailTemplatePath);
                }
                return string.Empty;
            }
        }
        public static string KitAssembly
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.KitAssembly)))
                {
                    return AppSetting(Constants.KitAssembly);
                }
                return string.Empty;
            }
        }

        public static string BucketPlatePNGForAllState
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.BucketPlatePNGForAllState)))
                {
                    return AppSetting(Constants.BucketPlatePNGForAllState);
                }
                return string.Empty;
            }
        }

        public static string RMAStatusReportSchedulrTime
        {
            get
            {
                if (!string.IsNullOrEmpty(AppSetting(Constants.RMAStatusReportSchedulrTime)))
                {
                    return AppSetting(Constants.RMAStatusReportSchedulrTime);
                }
                return string.Empty;
            }
        }

    }

}
