﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using GA.Common;
//using NLog;

namespace GuardianAvionicsWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        protected void Session_End(object sender, EventArgs e)
        {

            //GA.Common.ExceptionHandler.ReportError(new Exception("Session end => " + DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.ToShortTimeString()));

            // event is raised when a session is abandoned or expires

            //Response.RedirectToRoute(new RouteValueDictionary {
            //            { "Controller", "Home" },
            //            { "Action", "UserProfile" }
            //    });


            // Response.Redirect("/Home/Register");


            //var Result = new RedirectToRouteResult(
            //        new RouteValueDictionary {
            //            { "Controller", "Home" },
            //            { "Action", "TimeoutRedirect" }
            //    });

            //Result.ExecuteResult(

            //RedirectResult
            //    RedirectToRouteResult
        }

        protected void Session_Start(object sender, EventArgs e)
        {

            //GA.Common.ExceptionHandler.ReportError(new Exception("Session start => " + DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.ToShortTimeString()));

            //Response.RedirectToRoute("Home/UserProfile");

            //Response.RedirectToRoute(new RouteValueDictionary {
            //            { "Controller", "Home" },
            //            { "Action", "UserProfile" }
            //    });





            //Response.RedirectToRoute("Home/UserProfile");



//            routes.MapRoute(
//    name: "Default",
//    url: "{controller}/{action}/{id}",
//    defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
//);




            // event is raised each time a new session is created     
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //var logger = LogManager.GetCurrentClassLogger();
            //logger.Trace("Exception" + Newtonsoft.Json.JsonConvert.SerializeObject(sender) +
            //             Newtonsoft.Json.JsonConvert.SerializeObject(e));

            //ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(sender)
            //    + Newtonsoft.Json.JsonConvert.SerializeObject(e));
        }

    }
}