using GA.DataTransfer.Classes_for_Services;
using Mvc.Mailer;

namespace GuardianAvionicsWeb.Mailers
{ 
    public interface IUserMailer
    {
			MvcMailMessage Welcome();
			MvcMailMessage PasswordReset();

            MvcMailMessage PilotLogSummary(PilotLogSummaryEmailModel modlel);

	}
}