using System.Collections.Generic;
using System.Net.Mail;
using GA.DataTransfer.Classes_for_Services;
using Mvc.Mailer;

namespace GuardianAvionicsWeb.Mailers
{
    public class UserMailer : MailerBase, IUserMailer
    {
        public UserMailer()
        {
            MasterName = "_Layout";
        }

        public virtual MvcMailMessage Welcome()
        {
            //ViewBag.Data = someObject;
            return Populate(x =>
            {
                x.Subject = "Welcome";
                x.ViewName = "Welcome";
                x.To.Add("Prabal.Khajanchi@ideavate.com");
            });
        }

        public virtual MvcMailMessage PasswordReset()
        {
            //ViewBag.Data = someObject;
            return Populate(x =>
            {
                x.Subject = "PasswordReset";
                x.ViewName = "PasswordReset";
                x.To.Add("Prabal.Khajanchi@ideavate.com");
            });
        }

        public virtual MvcMailMessage PilotLogSummary( PilotLogSummaryEmailModel model)
        {
            ViewData.Model = model;

            var resources = new Dictionary<string, string>();
            resources["logo"] = System.Web.HttpContext.Current.Server.MapPath("~/Image/GA/logo.png");// "~/Image/GA/logo.png";

            var a = Populate(z =>
            {
                z.Subject = "End of flight notification from the Pilot EFB App";
                z.ViewName = "PilotSummaryEmail";
                z.To.Add("Prabal.Khajanchi@ideavate.com");
                z.From = new MailAddress("Prabal.Khajanchi@ideavate.com");
                z.LinkedResources = resources;
            });

            return a;
        }
    }
}