﻿using GA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GuardianAvionicsWeb.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class InventoryFilter : ActionFilterAttribute
    {
        public string Role { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext.Session["ProfileId"] == null || httpContext.Session["UserType"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                             new RouteValueDictionary
                                 {
                                    {"Controller", "Home"},
                                    {"Action", "Login"} 
                                 });
            }
            string[] arrRoles = Role.Split(',');

           
            if (!arrRoles.Contains(httpContext.Session["UserType"]))
                 {
                filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                                {
                                    {"Controller", "Home"},
                                    {"Action", "Login"}
                                });
            }

        }
    }
}