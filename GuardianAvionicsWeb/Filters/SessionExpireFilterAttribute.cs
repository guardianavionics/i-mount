﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace GuardianAvionicsWeb.Controllers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            var url = ctx.Request.Url;
            if (ctx.Session["UserType"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                          new RouteValueDictionary
                              {
                                    {"Controller", "Home"},
                                    {"Action", "Logout"} 
                                    
                              });
                base.OnActionExecuting(filterContext);
            }

            // If the browser session or authentication session has expired...
            if (ctx.Session["ProfileId"] == null && ctx.Session["AdminProfileId"] == null)       // || !filterContext.HttpContext.Request.IsAuthenticated)
            {
                // Session has expired

                // check for cookies
                HttpCookie profileId = ctx.Request.Cookies["ProfileId"];
                HttpCookie userName = ctx.Request.Cookies["UserName"];

                if (profileId != null && userName != null && profileId.Value != null && userName.Value != null)
                {
                    // value of session variables exists in cookies
                    // user has selected  KEEP ME SIGN  in option
                    // SET SESSION

                    // ReSharper disable once PossibleNullReferenceException
                    if (profileId.Value != "1")
                    {
                        // normal user

                        ctx.Session["UserLoggedIn"] = true;
                        // ReSharper disable once PossibleNullReferenceException
                        ctx.Session["UserName"] = userName.Value;
                        // ReSharper disable once PossibleNullReferenceException
                        ctx.Session["ProfileId"] = profileId.Value;
                    }
                    else
                    {
                        // admin user
                        
                        // save data in session variable for admin.
                        ctx.Session["UserLoggedIn"] = true;
                        // ReSharper disable once PossibleNullReferenceException
                        ctx.Session["AdminName"] = userName.Value;
                        ctx.Session["AdminProfileId"] = profileId.Value;

                    }
                }
                else
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        // For AJAX requests, we're overriding the returned JSON result with a simple string,
                        // indicating to the calling JavaScript code that a redirect should be performed.

                        filterContext.Result = new JsonResult { Data = GA.Common.Constants.SessionTimeOutConstant };

                        var redirectToUrl = filterContext.HttpContext.Request.Url;
                        filterContext.Result = new JavaScriptResult() { Script = "window.location.replace('"+GA.Common.Constants.ServerUrl+"');" };

                        // "_Logon_"
                    }
                    else
                    {
                        // For round-trip posts, we're forcing a redirect to Home/TimeoutRedirect/, which
                        // simply displays a temporary 5 second notification that they have timed out, and
                        // will, in turn, redirect to the logon page.
                        
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                                {
                                    {"Controller", "Home"},
                                    {"Action", "Login"},
                                    {"id", "1"}
                                });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class LocsAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // If the browser session has expired...
            if (ctx.Session["UserName"] == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, we're overriding the returned JSON result with a simple string,
                    // indicating to the calling JavaScript code that a redirect should be performed.
                    filterContext.Result = new JsonResult { Data = "Your Session has expired .Login again." };
                }
                else
                {
                    // For round-trip posts, we're forcing a redirect to Home/TimeoutRedirect/, which
                    // simply displays a temporary 5 second notification that they have timed out, and
                    // will, in turn, redirect to the logon page.
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                        { "Controller", "Home" },
                        { "Action", "TimeoutRedirect" }
                });
                }
            }
            else if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                // Otherwise the reason we got here was because the user didn't have access rights to the
                // operation, and a 403 should be returned.
                filterContext.Result = new HttpStatusCodeResult(403);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }


  
}