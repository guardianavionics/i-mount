'use strict';

function Login() {
    this.isLoginPageRendered = false;
    this.isLogin = false;
}

Login.prototype.init = function(){
    var self = this;

    return new Promise(function(resolve, reject) {
        var user = localStorage.getItem('user');
        if(user && !app.user){
            var savedUser = JSON.parse(user);
            app.room = savedUser.tag_list;
            //self.login(savedUser)
            //    .then(function(){
            //        resolve(true);
            //    }).catch(function(error){
            //    reject(error);
            //    });
            loginModule.isLogin = false;
            app.isDashboardLoaded = false;
             
            localStorage.removeItem('user');
             
            QB.chat.disconnect();
            QB.destroySession();
            resolve(false);
            location.reload();
             
        } else {
            resolve(false);
        }
    });
};

Login.prototype.login = function (user) {    
    var self = this;
    return new Promise(function(resolve, reject) {
        if(self.isLoginPageRendered){
            document.forms.loginForm.login_submit.innerText = 'loading...';
        } else {
            self.renderLoadingPage();
        }
        QB.createSession(function(csErr, csRes) {            
            var userRequiredParams = {
            'login': user.login,
			'id': user.id,
            'password': '12345678',
			'email': user.email,
            'full_name': user.full_name,
            'tag_list': user.tag_list
            };
            if (csErr) {
                loginError(csErr);
            } else {
                app.token = csRes.token;
                QB.login(userRequiredParams, function(loginErr, loginUser){
                    if(loginErr) {
					 
                        /** Login failed, trying to create account */
                        QB.users.create(userRequiredParams, function (createErr, createUser) {
						 
                            if (createErr) {
								 
                                loginError(createErr);
                            } else {                                
                                SetUserQuickBloxId(createUser.id);
                                //var newid = createUser.id
                               
								//alert('Create User Success now login ' +JSON.stringify(userRequiredParams)  );
                                QB.login(userRequiredParams, function (reloginErr, reloginUser) {
                                    if (reloginErr) {
										 
                                        loginError(reloginErr);
                                    } else {
										 
                                        loginSuccess(reloginUser);
                                    }
                                });
                            }
                        });
                    } else {                        
                        getAllUserToLink(loginUser.id);
						 
                        /** Update info */
                        if(loginUser.user_tags !== user.tag_list || loginUser.full_name !== user.full_name) {
                            QB.users.update(loginUser.id, {
                                'full_name': user.full_name,
                                'tag_list': user.tag_list
                            }, function(updateError, updateUser) {
                                if(updateError) {
                                    loginError(updateError);
                                } else {
                                    loginSuccess(updateUser);
                                }
                            });
                        } else {
                            loginSuccess(loginUser);
                        }
                    }
                });
            }
        });

        function loginSuccess(userData){
            app.user = userModule.addToCache(userData);
            app.user.user_tags = userData.user_tags;
            QB.chat.connect({ userId: app.user.id, password: user.password }, function (err, roster) {
                if (err) {
                    document.querySelector('.j-login__button').innerText = 'Login';
                    console.error(err);
                    reject(err);
                } else {
                    self.isLogin = true;
                    resolve();

                    var unreadParams = { };
                    QB.chat.message.unreadCount(unreadParams, function (err, res) {
                        debugger;
                        if (err) {
                            console.log(err)
                            callback(err, null)
                        } else {
                            //alert('res.total = ' + res.total);
                            //alert(res.total);
                            //callback(null, res.total)
                            if (res.total != "0") {
                                $("#spnLiveChatUnreadMsgCount").addClass("messagecounter");
                                $("#spnLiveChatUnreadMsgCount").text(res.total);
                            } else {
                                $("#spnLiveChatUnreadMsgCount").removeClass("messagecounter");
                                $("#spnLiveChatUnreadMsgCount").text("");
                            }
                        }
                    })
                }
            });
        }

        function loginError(error){
            self.renderLoginPage();
            console.error(error);
           
            reject(error);
        }
    }); 
};

Login.prototype.renderLoginPage = function(){
    helpers.clearView(app.page);

    app.page.innerHTML = helpers.fillTemplate('tpl_login', {
        version: QB.version + ':' + QB.buildNumber
    });
    this.isLoginPageRendered = true;
    this.setListeners();
};

Login.prototype.renderLoadingPage = function(){
    helpers.clearView(app.page);
    app.page.innerHTML = helpers.fillTemplate('tpl_loading');
};

Login.prototype.setListeners = function(){
    var self = this,
        loginForm = document.forms.loginForm,
        formInputs = [loginForm.id, loginForm.password, loginForm.email, loginForm.full_name],
        loginBtn = loginForm.login_submit;

    loginForm.addEventListener('submit', function(e){
        e.preventDefault();
        
        if(loginForm.hasAttribute('disabled')){
            return false;
        } else {
            loginForm.setAttribute('disabled', true);
        }

        var id = loginForm.id.value.trim(),
            password = '12345678',
            email = loginForm.email.value.trim(),
            full_name = loginForm.full_name.value.trim();
        
        var user = {
            login: id,
            id: id,
            password: password,
			email: email,
            full_name: full_name,
            tag_list: "Guardian,Avionics"
        };

        localStorage.setItem('user', JSON.stringify(user));

        self.login(user).then(function(){
            router.navigate('/dashboard');
        }).catch(function(error){
            alert('lOGIN ERROR\n open console to get more info');
            loginBtn.removeAttribute('disabled');
            console.error(error);
            loginForm.login_submit.innerText = 'LOGIN';
        });
    });

    // add event listeners for each input;
    _.each(formInputs, function(i){
        i.addEventListener('focus', function(e){
            var elem = e.currentTarget,
                container = elem.parentElement;

            if (!container.classList.contains('filled')) {
                container.classList.add('filled');
            }
        });

        i.addEventListener('focusout', function(e){
            var elem = e.currentTarget,
                container = elem.parentElement;

            if (!elem.value.length && container.classList.contains('filled')) {
                container.classList.remove('filled');
            }
        });

        i.addEventListener('input', function () {
            loginBtn.removeAttribute('disabled');
            //var userName = loginForm.userName.value.trim(),
            //    userGroup = loginForm.userGroup.value.trim();
            //if(userName.length >=3 && userGroup.length >= 3){
            //    loginBtn.removeAttribute('disabled');
            //} else {
            //    loginBtn.setAttribute('disabled', true);
            //}
        })
    });
};

var loginModule = new Login();