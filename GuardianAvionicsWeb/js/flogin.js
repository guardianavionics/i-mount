/*
 * flogin.js
 * stupidly simple facebook login as a jQuery plugin
 *
 * Gary Rafferty - 2013
 */
(function($) {
  $.fn.facebook_login = function(options) {
    var defaults = {
      endpoint: '/sessions/new',
      permissions: 'email',
      onSuccess: function(data) { console.log([200,'OK']); },
      onError: function(data) { console.log([500,'Error']); }
    };

    var settings = $.extend({}, defaults, options);

    if(settings.appId === 'undefined') {
      console.log('You must set the appId');
      return false;
    }

    (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
      js.src = "http://connect.facebook.net/en_US/all.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
      FB.init({
        appId: settings.appId,
        status: true,
        xfbml: true
      });
    };

    this.bind('click', function() {
      FB.login(function(r) {
        var response;
        if(response = r.authResponse) {
          var user_id = response.userID;
          var token   = response.accessToken;

          FB.api('/me?access_token=' + token, function (user) {
              console.log('Successful login for: ' + user.name);
              //document.getElementById('status').innerHTML =
              //  'Thanks for logging in, ' + response.name + '!';
              $.post("/Home/verifyFBLogin", {
                  Email: user.email,
                  FirstName: user.first_name,
                  LastName: user.last_name,
                  Id: user.id,
                  ThirdPartyAccountType: 1
              },
              function (data) {

                  if (data == "Admin") {
                      window.location.href = '/Admin/AdminHome';
                  }
                  else if (data == "User") {
                      window.location.href = '/Home/PilotSummaryDetails';
                  }
                  else if (data == "UserBlocked") {
                      HideLoader();
                      alert('User is blocked. Please contact administrator');
                      FB.logout(function (response) {
                          // user is now logged out
                      });
                  }
                  else if (data == "AlreadyRegisterWithGuardian") {
                      HideLoader();
                      alert('Please login by Guardian Avionics account. Your Id is already registered in Guardian Avionics.');
                      FB.logout(function (response) {
                          // user is now logged out
                      });
                  }
              })
          });
        } else {
          settings.onError();
        }
      }, {scope: settings.permissions});

      return false; 
    });
  }
})(jQuery);

