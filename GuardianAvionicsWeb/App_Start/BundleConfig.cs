﻿using System.Web;
using System.Web.Optimization;

namespace GuardianAvionicsWeb
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/JqueryJs").Include(
                      "~/js/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                 "~/vendors/datatables.net/js/jquery.dataTables.js",
                 "~/vendors/datatables.net-bs/js/dataTables.bootstrap.js"));

            

            bundles.Add(new ScriptBundle("~/bundles/bootstrapJs").Include(
                    "~/vendors/bootstrap/dist/js/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2Full").Include(
                     "~/vendors/select2/dist/js/select2.full.js"));

            bundles.Add(new ScriptBundle("~/bundles/Inventory").Include(
                "~/js/jquery.js",
                "~/vendors/bootstrap/dist/js/bootstrap.js",
                "~/js/bootbox.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));


            bundles.Add(new StyleBundle("~/bundles/layoutMaster").Include(
                    "~/vendors/bootstrap/dist/css/bootstrap.css",
                    "~/content/fonts/css/customfont.css",
                    "~/Content/fonts/css/font-awesome.css",
                    "~/vendors/datatables.net-bs/css/dataTables.bootstrap.css",
                    "~/content/css/green.css",
                    "~/vendors/select2/dist/css/select2.css",
                    "~/content/css/custom.css",
                    "~/content/css/style.css"));
            //bundles.Add(new ScriptBundle("~/bundles/pilotLogs").Include());

         
        }
    }
}