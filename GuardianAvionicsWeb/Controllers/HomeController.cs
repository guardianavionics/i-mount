﻿using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Web;
using System.Web.Services.Description;
using GA.ApplicationLayer;
using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GA.DataTransfer.Classes_for_Services;
//using GuardianAvionicsWeb.Mailers;
using PagedList;
using Dropbox.Api;
using OAuthProtocol;
using System.Linq;
using System.Timers;
using System.Text;
using DropNet;
//using NLog;
using ASPSnippets.GoogleAPI;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data.SqlTypes;
using PayPal;
using System.Collections.Specialized;
using System.Web.Configuration;
using Ionic.Zip;
using System.Threading.Tasks;
using Newtonsoft.Json;
using HighchartsExportClient;
using System.Net.Http;
using System.Net.Http.Headers;
using PayPalSDK;
using Dropbox.Api.Files;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Linq;
using System.Net.Mail;
using System.Web.UI;
using System.IO.Compression;
using System.Data.SQLite;
using System.Xml;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Clients;

namespace GuardianAvionicsWeb.Controllers
{

    //[SessionExpireFilterAttribute] this attribute cant be applied on 
    //login action method as it will struck in infinite loop , so we cant apply to complete controller
    /// <summary>
    /// Contains all the functions related to user.
    /// </summary>
    /// 
    public static class MyGlobalVariables
    {
        public static string MyGlobalString { get; set; }
    }
    [HandleError(View = "Error")]
    public class HomeController : Controller
    {
        public HomeController()
        {
            DropNetRT.DropNetClient _client = new DropNetRT.DropNetClient(ConfigurationReader.DropboxConsumerKey, ConfigurationReader.DropboxConsumerSecret);
        }


        private SQLiteConnection sqlite;

        //Test Message check for branch
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        // private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public StringBuilder strIPN = new StringBuilder();
        ModelServices mobjModel = new ModelServices();
        private string paypalResponse = "";

        public ActionResult DeletePlates()
        {
            var context = new GuardianAvionicsEntities();

            var objState = context.States.Select(s => s.Id).ToList();
            PlatesResponseModel objPaltesResponse = new PlatesResponseModel();


            foreach (int stateId in objState)
            {
                objPaltesResponse.StateList = context.States.Where(s => s.Id == stateId && s.State_Code_Id != null).Select(s => new PlateStates
                {
                    Id = s.Id,
                    ShortName = s.ShortName,
                    State_Code_Id = s.State_Code_Id,
                    StateName = s.StateName,
                    CityList = s.Cities.Where(wc => wc.City_Name_Id != null && wc.State_Code_Id != null).Select(c => new PlateCitys
                    {
                        City_Name_Id = c.City_Name_Id ?? 0,
                        CityName = c.CityName,
                        Id = c.Id,
                        State_Code_Id = c.State_Code_Id,
                        StateId = c.StateId,
                        Volume = c.Volume,
                        AirportList = c.Airports.Where(wa => wa.airport_name_Id != null && wa.city_name_Id != null).Select(a => new PlateAirPorts
                        {
                            airport_name_Id = a.airport_name_Id,
                            AirportName = a.AirportName,
                            apt_ident = a.apt_ident,
                            city_name_Id = a.city_name_Id,
                            CityId = a.CityId,
                            Country = a.Country,
                            icao_ident = a.icao_ident,
                            Id = a.Id,
                            Military = a.Military,
                            X = a.X,
                            Y = a.Y,
                            PlateList = a.Plates.Where(wp => wp.Airport_Name_Id != null).Select(p => new PlateDetails
                            {
                                Airport_Name_Id = p.Airport_Name_Id,
                                AirportId = p.AirportId,
                                Chart_Code = p.Chart_Code,
                                Chart_Name = p.Chart_Name,
                                ChartSeq = p.ChartSeq,
                                Civil = p.Civil,
                                Copter = p.Copter,
                                Draw_Bottom = p.Draw_Bottom,
                                Draw_Left = p.Draw_Left,
                                Draw_Right = p.Draw_Right,
                                Draw_Top = p.Draw_Top,
                                ExcludeAreas = p.ExcludeAreas,
                                Faanfd15 = p.Faanfd15,
                                Faanfd18 = p.Faanfd18,
                                //File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
                                File_Name = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/" + p.File_Name,
                                Id = p.Id,
                                Invalid = p.Invalid == "true" ? true : false,
                                LastChanged = p.LastChanged,
                                Lat1 = p.Lat1,
                                Lat2 = p.Lat2,
                                Long1 = p.Long1,
                                Long2 = p.Long2,
                                Orientation = p.Orientation,
                                UserAction = p.UserAction,
                                ValidWithinRadius = p.ValidWithinRadius,
                                ValidWithinX = p.ValidWithinX,
                                ValidWithinY = p.ValidWithinY,
                                X1 = p.X1,
                                X2 = p.X2,
                                Y1 = p.Y1,
                                Y2 = p.Y2,
                                FileSize = p.FileSize == null ? (long)0.0 : Math.Round(((long)p.FileSize / 1024f) / 1024f, 6)
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

                System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + @"JsonFiles\" + stateId.ToString() + ".txt", Newtonsoft.Json.JsonConvert.SerializeObject(objPaltesResponse));
            }

            return View();
        }



        public async Task<string> GetImageLinkFromOptions_Works(List<int> seriesData, List<string> categoryData, string graphHead)
        {
            var client = new HighchartsClient("http://export.highcharts.com/");
            var options = new
            {
                xAxis = new
                {
                    categories = categoryData.ToArray()
                },
                series = new[]
                {
                    new { data = seriesData.ToArray(), name = graphHead } //new[] { 10, 20, 30 } 
                }
            };

            var res = await client.GetChartImageLinkFromOptionsAsync(JsonConvert.SerializeObject(options));

            return res;
        }

        public bool setsession(String sessionval)
        {
            try
            {
                MyGlobalVariables.MyGlobalString = sessionval.ToString();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string Getsession()
        {
            string getTemp = "";
            try
            {
                getTemp = MyGlobalVariables.MyGlobalString;
                return getTemp;
            }
            catch (Exception ex)
            {
                return getTemp;
            }
        }

        [HttpPost]
        public ActionResult InAppNotificationCallback(InAppReceiptNotification obj)
        {
            ExceptionHandler.ReportError(new Exception(), "InAppNotification Callback Method");
            ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(obj));
            var context = new GuardianAvionicsEntities();
            if (obj.auto_renew_status == "false")
            {
                if (obj.latest_expired_receipt_info != null)
                {
                    var inappReceipt = context.InappReceipts.FirstOrDefault(f => f.TransactionID == obj.latest_expired_receipt_info.Transaction_Id);
                    if (inappReceipt != null)
                    {
                        var mapSubAndUserId = inappReceipt.MapSubscriptionAndUSer.Id;
                        var pendingRenInfo = context.PendingRenewalInfoes.FirstOrDefault(f => f.MapSubAndUserId == mapSubAndUserId);
                        if (pendingRenInfo != null)
                        {
                            //To send email when sub auto renewal is false and current status in DB is true
                            if (pendingRenInfo.AutoRenewStatus == true)
                            {
                                Task.Factory.StartNew(() => { new PaypalPaymentHelper().SendEmail(new InappReceipt(), pendingRenInfo.MapSubAndUserId ?? 0, pendingRenInfo.MapSubscriptionAndUSer.ProfileId, Enumerations.PaypalStatus.Cancelled.ToString()); });
                            }
                            pendingRenInfo.AutoRenewStatus = false;
                            pendingRenInfo.ExpirationIntent = 1;
                        }
                    }
                }
                context.SaveChanges();
            }
            else if (obj.notification_type != "CANCEL")
            {
                var MapSubscriptionAndUSers = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.InappOriginalTransactionId == obj.latest_receipt_info.Original_Transaction_Id && f.InAppProductId == obj.latest_receipt_info.Product_Id);
                if (MapSubscriptionAndUSers != null)
                {
                    if (context.InappReceipts.FirstOrDefault(f => f.MapSubAndUserId == MapSubscriptionAndUSers.Id && f.TransactionID == obj.latest_receipt_info.Transaction_Id) == null)
                    {
                        InappReceipt newReceipt = new InappReceipt
                        {
                            Quantity = obj.latest_receipt_info.Quantity,
                            TransactionID = obj.latest_receipt_info.Transaction_Id,
                            PurchaseDate = Convert.ToDateTime(obj.latest_receipt_info.Purchase_Date.Replace(" Etc/GMT", "")),
                            ExpireDate = (new DateTime(1970, 1, 1, 0, 0, 0)).AddMilliseconds(Convert.ToInt64(obj.latest_receipt_info.Expires_Date)),
                            WebOrderLineItemId = obj.latest_receipt_info.Web_Order_Line_Item_Id,
                            IsTrialPeriod = obj.latest_receipt_info.Is_Trial_Period,
                            MapSubAndUserId = MapSubscriptionAndUSers.Id
                        };
                        context.InappReceipts.Add(newReceipt);
                        context.SaveChanges();
                        Task.Factory.StartNew(() => { new PaypalPaymentHelper().SendEmail(newReceipt, MapSubscriptionAndUSers.Id, MapSubscriptionAndUSers.ProfileId, Enumerations.PaypalStatus.Active.ToString()); });
                    }
                }
            }
            return View();
        }

        public void SetSession()
        {
            Session["UserType"] = null;
        }

        public ActionResult Index()
        {

            string aaa = new Misc().Decrypt("FpvKrS7b1iV5grzUb7yE+A==");
            if (Request.Cookies["UserName"] != null && Request.Cookies["Details"] != null)
            {
                string emailId = Request.Cookies["UserName"].Value;
                string password = Request.Cookies["Details"].Value;
                try
                {
                    emailId = new Misc().Decrypt(emailId);
                    password = new Misc().Decrypt(password);
                    LoginModel objModel = new LoginModel();
                    objModel.EmailId = emailId;
                    objModel.Password = password;
                    var responseLogin = new LoginHelper().IsFaceBookUser(objModel);
                    if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.AdminUser).ToString())
                    {
                        // Admin logged in
                        // save data in session variable.
                        Session["AdminName"] = responseLogin.FirstName;
                        Session["UserName"] = responseLogin.FirstName;
                        Session["AdminProfileId"] = responseLogin.ProfileId;
                        Session["UserType"] = responseLogin.UserType;
                        ViewBag.HasError = Boolean.FalseString;
                        Session["ImageUrl"] = responseLogin.ImageName;
                        return RedirectToAction("Dashboard", "Admin");
                    }
                    else if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
                    {
                        Session["UserLoggedIn"] = true;
                        Session["UserName"] = responseLogin.FirstName;
                        Session["ProfileId"] = responseLogin.ProfileId;
                        Session["UserType"] = responseLogin.UserType;
                        Session["ImageUrl"] = responseLogin.ImageName;
                        TempData["url"] = "NewLog";
                        // no error found . set error flag as false
                        ViewBag.HasError = Boolean.FalseString;

                        if (responseLogin.UserType == Enumerations.UserType.Manufacturer.ToString())
                        {
                            return RedirectToAction("ListAllJPIData", "Data");
                        }
                        else if (responseLogin.UserType == Enumerations.UserType.Maintenance.ToString())
                        {
                            return RedirectToAction("SquawkList", "Aircraft");
                        }
                        else
                        {
                            return RedirectToAction("PilotSummary", "Home");
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    return View();
                }
            }

            // new Misc().UploadFile(ConfigurationReader.ChartFilePath + "Hello.txt", "HelloNoACL.txt", "JsonMisc");
            // GenerateScript(1, 100006, "testpilotlogscriptideavate@ideavate.com");
            // CreatePilotLogForScript("sTail 100006", "testpilotlogscriptideavate@ideavate.com", "");
            return View();
        }

        /// <summary>
        /// This method is created to generate Aircraft Logs
        /// </summary>
        /// <param name="noOfUnits"></param>
        /// <param name="startingUnitNo"></param>
        public void GenerateScript(int noOfUnits, int startingUnitNo, string emailId)
        {
            for (int i = 0; i < noOfUnits; i++)
            {
                string response = CreateAeroUnit(startingUnitNo.ToString());
                if (response == "Success")
                {
                    // Create Aircraft
                    response = CreateAircraftForScript(startingUnitNo.ToString(), emailId);
                    if (response == "Success")
                    {
                        //Create Flight
                        CreatePilotLogForScript("sTail " + startingUnitNo.ToString(), "testpilotlogscriptideavate@ideavate.com", "");
                    }
                    else
                    {
                        //Error
                    }
                }
                else
                {
                    //Error
                }
                startingUnitNo = startingUnitNo + 1;
            }
        }

        public string CreateAeroUnit(string unitNo)
        {
            var context = new GuardianAvionicsEntities();

            var objUnit = context.AeroUnitMasters.FirstOrDefault(f => f.AeroUnitNo == unitNo);
            if (objUnit != null)
            {
                return "Unit Already Exist";
            }

            AeroUnitMaster objAeroUnit = (new AeroUnitMaster
            {
                AeroUnitNo = unitNo,
                AircraftId = null,
                Blocked = false,
                CreateDate = DateTime.UtcNow,
                Deleted = false
            });
            context.AeroUnitMasters.Add(objAeroUnit);
            context.SaveChanges();
            return "Success";
        }

        public string CreateAircraftForScript(string unitNo, string emailId)
        {
            AircraftModifyNnumberRequest aircraft = new AircraftModifyNnumberRequest();
            aircraft.AeroUnitNo = unitNo; ///////////
            aircraft.UniqueId = Convert.ToInt64(new Misc().RandomNumericString(13));////
            aircraft.OwnerEmailId = emailId; ////
            aircraft.IsAmericanAircraft = true;
            aircraft.Registration = "sTail " + unitNo;
            aircraft.Make = null;
            aircraft.Model = null;
            aircraft.Color = null;
            aircraft.HomeBase = null;
            aircraft.Capacity = "53.10";
            aircraft.TaxiFuel = "1.32";
            aircraft.AdditionalFuel = "1.32";
            aircraft.FuelUnit = "GAL";
            aircraft.WeightUnit = "LBS";
            aircraft.SpeedUnit = "KTS";
            aircraft.VerticalSpeed = "FT/MIN";
            aircraft.Power = 65.0;
            aircraft.RPM = 2400.0;
            aircraft.MP = 24.0;
            aircraft.Altitude = 2500.0;
            aircraft.FuelBurnCruise = 34.0;
            aircraft.TASCruise = 115.0;
            aircraft.RateOfClimb = 650.0;
            aircraft.FuelBurnRateClimb = 60.0;
            aircraft.TASClimb = 73.0;
            aircraft.RateOfDescent = 500.0;
            aircraft.FuelBurnRateOfDescent = 5.0;
            aircraft.TASDescent = 125.0;
            aircraft.ImageName = null;
            aircraft.ImageOption = null;
            aircraft.ProfileId = 0;
            aircraft.EngineType = 1;
            aircraft.AircraftType = null;
            aircraft.AircraftSerialNumber = null;
            aircraft.AircraftYear = null;
            aircraft.CurrentHobbsTime = 0.0;
            aircraft.HobbsTimeOffset = 0.0;
            aircraft.CurrentTachTime = 0.0;
            aircraft.TachTimeOffset = 0.0;
            aircraft.EngineMFGType = null;
            aircraft.EngineTBO = 0.0;
            aircraft.Engine1LastMOHDate = "";
            aircraft.CurrentEngineTime1 = 0.0;
            aircraft.EngineTimeOffset1 = 0.0;
            aircraft.PropMFG = null;
            aircraft.Prop1TBO = 0;
            aircraft.Prop1OHDueDate = "";
            aircraft.CurrentPropTime1 = 0.0;
            aircraft.PropTimeOffset1 = 0.0;
            aircraft.Engine2LastMOHDate = "";
            aircraft.CurrentEngineTime2 = 0.0;
            aircraft.EngineTimeOffset2 = 0.0;
            aircraft.Prop2OHDueDate = "";
            aircraft.CurrentPropTime2 = 0.0;
            aircraft.PropTimeOffset2 = 0.0;
            aircraft.Comm = false;
            aircraft.EngineMonitor = false;
            aircraft.Transponder = false;
            aircraft.IsPilot = false;
            aircraft.OtherAircraftManufacturer = "";
            aircraft.OtheAircraftrModel = "";
            aircraft.AircraftComponentMakeAndModels = null;
            aircraft.OtherEngineManufactureType = "";
            aircraft.OtherPropManufactureType = "";
            aircraft.ChargeBy = null;
            aircraft.ManufacturersMappedWithAircraft = "";
            aircraft.EngineSettings = "";
            aircraft.IsUpdateAvailableForGeneral = true;
            aircraft.IsUpdateAvailableForPerformance = true;
            aircraft.IsUpdateAvailableForEngine = true;
            aircraft.IsRegisterWithoutUnit = false;

            var response = new AircraftHelper().ModifyAircraftNNumber(aircraft);
            return response.ResponseMessage;

        }

        public string CreatePilotLogForScript(string tailNo, string pilotEmailId, string coPilotEmailID)
        {
            var context = new GuardianAvionicsEntities();
            List<byte> list = new List<byte>();
            var aircraftProfile = context.AircraftProfiles.FirstOrDefault(f => f.Registration == tailNo);
            int aId = aircraftProfile.Id;
            int noOfRecord = 7200;
            byte[] buffer = BitConverter.GetBytes((Int64)aId);
            list.AddRange(buffer);
            string message = "";
            Int16 messageLength = 0;

            string pilotId = "000000";
            string coPilotId = "999999";
            string pilotName = "noname";
            string coPilotName = "noname";

            //Now check pilot name and id
            if (string.IsNullOrEmpty(pilotEmailId))
            {
                return "Invalid pilot email Id";
            }
            var profile = context.Profiles.FirstOrDefault(f => f.EmailId == pilotEmailId);
            if (profile == null)
            {
                return "Invalid pilot email Id";
            }
            pilotId = (pilotId + profile.Id.ToString());
            pilotId = pilotId.Substring(pilotId.Length - 6);

            pilotName = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;

            if (!string.IsNullOrEmpty(coPilotEmailID))
            {
                profile = context.Profiles.FirstOrDefault(f => f.EmailId == coPilotEmailID);
                if (profile != null)
                {
                    coPilotId = ("000000" + profile.Id.ToString());
                    coPilotId = coPilotId.Substring(coPilotId.Length - 6);
                }
                coPilotName = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;
            }

            int transactionid = Convert.ToInt32(aircraftProfile.LastTransactionId) + 1;
            string flightId = "000001";
            var pilotLog = context.PilotLogs.Where(p => p.AircraftId == aId && p.FlightDataType == (int)GA.CommonForParseDataFile.Common.FlightDataType.StoredData).OrderByDescending(p => p.FlightId).FirstOrDefault();
            if (pilotLog != null)
            {
                flightId = "000000" + ((pilotLog.FlightId ?? 0) + 1).ToString();
            }
            flightId = flightId.Substring(flightId.Length - 6);

            list.Add((byte)5);
            //Add TransactionId
            list.AddRange(BitConverter.GetBytes(transactionid));
            //Add Message Length
            message = "$PGAVF,NOT ASSIGNED," + flightId + "," + pilotId + "," + pilotName + "," + coPilotId + "," + coPilotName + "*35";
            messageLength = Convert.ToInt16(Encoding.ASCII.GetBytes(message).Length + 11);

            list.Add(((byte)0));
            list.Add(((byte)messageLength));
            //list.AddRange(BitConverter.GetBytes(messageLength));

            //Add Message Type
            list.Add((byte)1);

            //Add Message
            list.AddRange(Encoding.ASCII.GetBytes(message));
            list.Add((byte)('\n'));
            list.Add((byte)('\r'));
            list.Add((byte)(0));
            list.Add((byte)(0));
            list.Add((byte)(0));
            DateTime dt = DateTime.Now.AddHours(-2);
            string date = String.Format("{0:ddMMyyyy}", DateTime.Now);
            for (int i = 0; i < noOfRecord; i++)
            {
                /*=================GPRMC command  Start=========================*/
                //Add Status
                list.Add((byte)5);
                //Add TransactionId
                transactionid = transactionid + 1;
                list.AddRange(BitConverter.GetBytes(transactionid));
                //Add Message Length
                message = "$GPRMC," + dt.ToString("hhmmss") + ",A,3307.320000,N,11717.940000,W,00053,346," + date + ",000.0,E,A*2E";
                messageLength = Convert.ToInt16(Encoding.ASCII.GetBytes(message).Length + 11);
                dt = dt.AddSeconds(1);
                list.Add(((byte)0));
                list.Add(((byte)messageLength));
                //list.AddRange(BitConverter.GetBytes(messageLength));

                //Add Message Type
                list.Add((byte)1);

                //Add Message
                list.AddRange(Encoding.ASCII.GetBytes(message));
                list.Add((byte)('\n'));
                list.Add((byte)('\r'));
                list.Add((byte)(0));
                list.Add((byte)(0));
                list.Add((byte)(0));
                /*=================GPRMC command  End=========================*/


                /*=================GPGGA command  Start=========================*/
                //Add Status
                list.Add((byte)5);
                //Add TransactionId
                transactionid = transactionid + 1;
                list.AddRange(BitConverter.GetBytes(transactionid));
                //Add Message Length
                message = "$GPGGA,122333.00,3307.330000,N,11717.940000,W,8,12,0.0,3049.56,M,50.0,M,,*75";
                messageLength = Convert.ToInt16(Encoding.ASCII.GetBytes(message).Length + 11);
                list.Add(((byte)0));
                list.Add(((byte)messageLength));
                //list.AddRange(BitConverter.GetBytes(messageLength));

                //Add Message Type
                list.Add((byte)1);

                //Add Message
                list.AddRange(Encoding.ASCII.GetBytes(message));
                list.Add((byte)('\n'));
                list.Add((byte)('\r'));
                list.Add((byte)(0));
                list.Add((byte)(0));
                list.Add((byte)(0));
                /*=================GPGGA command  End=========================*/

                /*=================Garmin =3 command  start=========================*/
                //Add Status
                list.Add((byte)5);
                //Add TransactionId
                transactionid = transactionid + 1;
                list.AddRange(BitConverter.GetBytes(transactionid));
                //Add Message Length
                message = "=3101583674066+0870951____156030___072176159000129144+0140510105811________________+174+842+180+813+176+807+184+826________+0036T+0000T__________________+0125A+0071T+0050T______________________________________________C0";
                messageLength = Convert.ToInt16(Encoding.ASCII.GetBytes(message).Length + 11);
                list.Add(((byte)0));
                list.Add(((byte)messageLength));
                //list.AddRange(BitConverter.GetBytes(messageLength));

                //Add Message Type
                list.Add((byte)1);

                //Add Message
                list.AddRange(Encoding.ASCII.GetBytes(message));
                list.Add((byte)('\n'));
                list.Add((byte)('\r'));
                list.Add((byte)(0));
                list.Add((byte)(0));
                list.Add((byte)(0));
                /*=================Garmin =3 command  End=========================*/
            }

            /*=================PGAVF END command  Start=========================*/
            list.Add((byte)5);
            //Add TransactionId
            transactionid = transactionid + 1;
            list.AddRange(BitConverter.GetBytes(transactionid));
            //Add Message Length
            message = "$PGAVF,END*25";
            messageLength = Convert.ToInt16(Encoding.ASCII.GetBytes(message).Length + 11);
            list.Add(((byte)0));
            list.Add(((byte)messageLength));
            //Add Message Type
            list.Add((byte)1);
            //Add Message
            list.AddRange(Encoding.ASCII.GetBytes(message));
            list.Add((byte)('\n'));
            list.Add((byte)('\r'));
            list.Add((byte)(0));
            list.Add((byte)(0));
            list.Add((byte)(0));
            /*=================PGAVF END command  End=========================*/

            string fileName = DateTime.Now.Ticks.ToString() + ".data";
            ByteArrayToFile(fileName, list.ToArray());
            //Now Insert data into the database
            var parseDataFile = context.ParseDataFiles.Create();
            parseDataFile.FileName = fileName;
            parseDataFile.AircraftId = aId;
            parseDataFile.StatusId = 3;
            parseDataFile.CreateDate = DateTime.UtcNow;
            parseDataFile.ProfileId = Convert.ToInt32(pilotId);
            parseDataFile.UniqueId = 0;
            context.ParseDataFiles.Add(parseDataFile);
            context.SaveChanges();
            return "";
        }

        public bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            fileName = "C:\\inetpub\\wwwroot\\GuardianAvionics\\TestEnv\\ParseDataFileService\\DataFiles\\" + fileName;
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        public void CreateFileForDefaultAircraft()
        {
            var context = new GuardianAvionicsEntities();
            var aircraftList = context.AircraftProfiles.Where(a => !a.Deleted && !string.IsNullOrEmpty(a.AeroUnitNo)).ToList();
            foreach (var objAircraft in aircraftList)
            {

            }

            DateTime dt = DateTime.Now;
            StringBuilder strbuilder = new StringBuilder();
            for (int i = 0; i < 3600; i++)
            {
                strbuilder.Append(String.Format("{0:dd/MM/yyyy HH:mm:ss}", dt) + ",11.869911,-78.570602,442,40206,241.523438,8.626539,-15.319576");
                strbuilder.Append(Environment.NewLine);
                dt = dt.AddSeconds(1);
            }
            strbuilder.Append("end");
            string fileName = "D:\\Projects\\Guardian\\Publish\\ParseDataFileService\\DataFiles\\ABC.data";
            try
            {
                System.IO.File.WriteAllText(fileName, strbuilder.ToString());
            }
            catch (Exception ex)
            {

            }
        }

        public void ParseAirportData()
        {
            List<DataIndex> listAPT = new List<DataIndex>();
            //listAPT.Add(new DataIndex {ColumnName = "", StartIndex = 0, Length = 3 }); //RECORD TYPE INDICATOR
            listAPT.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 }); //LANDING FACILITY SITE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "Type", StartIndex = 14, Length = 13 }); //LANDING FACILITY TYPE
            listAPT.Add(new DataIndex { ColumnName = "apt_ident", StartIndex = 27, Length = 4 });  //LOCATION IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "EffectiveDate", StartIndex = 31, Length = 10 }); //INFORMATION EFFECTIVE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "Region", StartIndex = 41, Length = 3 });  //FAA REGION CODE
            listAPT.Add(new DataIndex { ColumnName = "DistrictOffice", StartIndex = 44, Length = 4 });  //FAA DISTRICT OR FIELD OFFICE CODE
            listAPT.Add(new DataIndex { ColumnName = "State", StartIndex = 48, Length = 2 }); //ASSOCIATED STATE POST OFFICE CODE
            listAPT.Add(new DataIndex { ColumnName = "StateName", StartIndex = 50, Length = 20 });  //ASSOCIATED STATE NAME
            listAPT.Add(new DataIndex { ColumnName = "County", StartIndex = 70, Length = 21 }); //ASSOCIATED COUNTY (OR PARISH) NAME
            listAPT.Add(new DataIndex { ColumnName = "CountyState", StartIndex = 91, Length = 2 });  //ASSOCIATED COUNTY'S STATE (POST OFFICE CODE)
            listAPT.Add(new DataIndex { ColumnName = "City", StartIndex = 93, Length = 40 }); //ASSOCIATED CITY NAME
            listAPT.Add(new DataIndex { ColumnName = "AirportName", StartIndex = 133, Length = 50 });  //OFFICIAL FACILITY NAME
            listAPT.Add(new DataIndex { ColumnName = "Ownership", StartIndex = 183, Length = 2 });  //AIRPORT OWNERSHIP TYPE
            listAPT.Add(new DataIndex { ColumnName = "Use", StartIndex = 185, Length = 2 });  //FACILITY USE
            listAPT.Add(new DataIndex { ColumnName = "Owner", StartIndex = 187, Length = 35 });  //FACILITY OWNER'S NAME
            listAPT.Add(new DataIndex { ColumnName = "OwnerAddress", StartIndex = 222, Length = 72 });  //OWNER'S ADDRESS
            listAPT.Add(new DataIndex { ColumnName = "OwnerCSZ", StartIndex = 294, Length = 45 });  //OWNER'S CITY, STATE AND ZIP CODE
            listAPT.Add(new DataIndex { ColumnName = "OwnerPhone", StartIndex = 339, Length = 16 });  //OWNER'S PHONE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "Manager", StartIndex = 355, Length = 35 });  //FACILITY MANAGER'S NAME
            listAPT.Add(new DataIndex { ColumnName = "ManagerAddress", StartIndex = 390, Length = 72 });  //MANAGER'S ADDRESS
            listAPT.Add(new DataIndex { ColumnName = "ManagerCSZ", StartIndex = 462, Length = 45 });  //MANAGER'S CITY, STATE AND ZIP CODE
            listAPT.Add(new DataIndex { ColumnName = "ManagerPhone", StartIndex = 507, Length = 16 });  //MANAGER'S PHONE NUMBER
            listAPT.Add(new DataIndex { ColumnName = "X", StartIndex = 523, Length = 15 }); //ARPLatitude  //AIRPORT REFERENCE POINT LATITUDE (FORMATTED)
            listAPT.Add(new DataIndex { ColumnName = "ARPLatitudeS", StartIndex = 538, Length = 12 });  //AIRPORT REFERENCE POINT LATITUDE (SECONDS)
            listAPT.Add(new DataIndex { ColumnName = "Y", StartIndex = 550, Length = 15 }); //ARPLongitude  //AIRPORT REFERENCE POINT LONGITUDE (FORMATTED)
            listAPT.Add(new DataIndex { ColumnName = "ARPLongitudeS", StartIndex = 565, Length = 12 });  //AIRPORT REFERENCE POINT LONGITUDE (SECONDS)
            listAPT.Add(new DataIndex { ColumnName = "ARPMethod", StartIndex = 577, Length = 1 });  //AIRPORT REFERENCE POINT DETERMINATION METHOD
            listAPT.Add(new DataIndex { ColumnName = "ARPElevation", StartIndex = 578, Length = 7 });  //AIRPORT ELEVATION  (NEAREST TENTH OF A FOOT MSL)
            listAPT.Add(new DataIndex { ColumnName = "ARPElevationMethod", StartIndex = 585, Length = 1 });  //AIRPORT ELEVATION DETERMINATION METHOD
            listAPT.Add(new DataIndex { ColumnName = "MagneticVariation", StartIndex = 586, Length = 3 });  //MAGNETIC VARIATION AND DIRECTION
            listAPT.Add(new DataIndex { ColumnName = "MagneticVariationYear", StartIndex = 589, Length = 4 });  //MAGNETIC VARIATION EPOCH YEAR
            listAPT.Add(new DataIndex { ColumnName = "TrafficPatternAltitude", StartIndex = 593, Length = 4 });  //TRAFFIC PATTERN ALTITUDE  (WHOLE FEET AGL)
            listAPT.Add(new DataIndex { ColumnName = "ChartName", StartIndex = 597, Length = 30 });  //AERONAUTICAL SECTIONAL CHART ON WHICH FACILITY
            listAPT.Add(new DataIndex { ColumnName = "DistanceFromCBD", StartIndex = 627, Length = 2 });  //DISTANCE FROM CENTRAL BUSINESS DISTRICT OF
            listAPT.Add(new DataIndex { ColumnName = "DirectionFromCBD", StartIndex = 629, Length = 3 });  //DIRECTION OF AIRPORT FROM CENTRAL BUSINESS
            listAPT.Add(new DataIndex { ColumnName = "LandAreaCoveredByAirport", StartIndex = 632, Length = 5 });  //LAND AREA COVERED BY AIRPORT (ACRES)
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCID", StartIndex = 637, Length = 4 });  //BOUNDARY ARTCC IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCComputerID", StartIndex = 641, Length = 3 });  //BOUNDARY ARTCC (FAA) COMPUTER IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "BoundaryARTCCName", StartIndex = 644, Length = 30 });  //BOUNDARY ARTCC NAME
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCID", StartIndex = 674, Length = 4 });  //RESPONSIBLE ARTCC IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCComputerID", StartIndex = 678, Length = 3 });  //RESPONSIBLE ARTCC (FAA) COMPUTER IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "ResponsibleARTCCName", StartIndex = 681, Length = 30 });  //RESPONSIBLE ARTCC NAME
            listAPT.Add(new DataIndex { ColumnName = "TieInFSS", StartIndex = 711, Length = 1 });  //TIE-IN FSS PHYSICALLY LOCATED ON FACILITY
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSID", StartIndex = 712, Length = 4 });  //TIE-IN FLIGHT SERVICE STATION (FSS) IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSName", StartIndex = 716, Length = 30 });  //TIE-IN FSS NAME
            listAPT.Add(new DataIndex { ColumnName = "AirportToFSSPhoneNumber", StartIndex = 746, Length = 16 });  //LOCAL PHONE NUMBER FROM AIRPORT TO FSS
            listAPT.Add(new DataIndex { ColumnName = "TieInFSSTollFreeNumber", StartIndex = 762, Length = 16 });  //TOLL FREE PHONE NUMBER FROM AIRPORT TO FSS FOR PILOT BRIEFING SERVICES
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSID", StartIndex = 778, Length = 4 }); //ALTERNATE FSS IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSName", StartIndex = 782, Length = 30 });  //ALTERNATE FSS NAME
            listAPT.Add(new DataIndex { ColumnName = "AlternateFSSTollFreeNumber", StartIndex = 812, Length = 16 });  //TOLL FREE PHONE NUMBER FROM AIRPORT TO ALTERNATE FSS FOR PILOT BRIEFING SERVICES
            listAPT.Add(new DataIndex { ColumnName = "NOTAMFacilityID", StartIndex = 828, Length = 4 });  //IDENTIFIER OF THE FACILITY RESPONSIBLE FOR ISSUING NOTICES TO AIRMEN (NOTAMS)AND WEATHER INFORMATION FOR THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "NOTAMService", StartIndex = 832, Length = 1 });  //AVAILABILITY OF NOTAM 'D' SERVICE AT AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "ActivationDate", StartIndex = 833, Length = 7 });  //AIRPORT ACTIVATION DATE (MM/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportStatusCode", StartIndex = 840, Length = 2 });  // AIRPORT STATUS CODE
            listAPT.Add(new DataIndex { ColumnName = "CertificationTypeDate", StartIndex = 842, Length = 15 });  //AIRPORT ARFF CERTIFICATION TYPE AND DATE
            listAPT.Add(new DataIndex { ColumnName = "FederalAgreements", StartIndex = 857, Length = 7 });  //NPIAS/FEDERAL AGREEMENTS CODE
            listAPT.Add(new DataIndex { ColumnName = "AirspaceDetermination", StartIndex = 864, Length = 13 });  //AIRPORT AIRSPACE ANALYSIS DETERMINATION
            listAPT.Add(new DataIndex { ColumnName = "CustomsAirportOfEntry", StartIndex = 877, Length = 1 });  //FACILITY HAS BEEN DESIGNATED BY THE U.S. TREASURY AS AN INTERNATIONAL AIRPORT OF ENTRY FOR CUSTOMS
            listAPT.Add(new DataIndex { ColumnName = "CustomsLandingRights", StartIndex = 878, Length = 1 });  //FACILITY HAS BEEN DESIGNATED BY THE U.S. TREASURY AS A CUSTOMS LANDING RIGHTS AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MilitaryJointUse", StartIndex = 879, Length = 1 });  //FACILITY HAS MILITARY/CIVIL JOINT USE AGREEMENT THAT ALLOWS CIVIL OPERATIONS AT A MILITARY AIRPORT OR MILITARY OPERATIONS AT A CIVIL AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MilitaryLandingRights", StartIndex = 880, Length = 1 });  //AIRPORT HAS ENTERED INTO AN AGREEMENT THAT GRANTS LANDING RIGHTS TO THE MILITARY
            listAPT.Add(new DataIndex { ColumnName = "InspectionMethod", StartIndex = 881, Length = 2 });  //AIRPORT INSPECTION METHOD
            listAPT.Add(new DataIndex { ColumnName = "InspectionGroup", StartIndex = 883, Length = 1 });  //AGENCY/GROUP PERFORMING PHYSICAL INSPECTION
            listAPT.Add(new DataIndex { ColumnName = "LastInspectionDate", StartIndex = 884, Length = 8 });  //LAST PHYSICAL INSPECTION DATE (MMDDYYYY)   *************************************************
            listAPT.Add(new DataIndex { ColumnName = "LastOwnerInformationDate", StartIndex = 892, Length = 8 });  //LAST DATE INFORMATION REQUEST WAS COMPLETED BY FACILITY OWNER OR MANAGER(MMDDYYYY)  *******************************
            listAPT.Add(new DataIndex { ColumnName = "FuelTypes", StartIndex = 900, Length = 40 });  //FUEL TYPES AVAILABLE FOR PUBLIC USE AT THE AIRPORT.THERE CAN BE UP TO 8 OCCURENCES OF A FIXED 5 CHARACTER FIELD.
            listAPT.Add(new DataIndex { ColumnName = "AirframeRepair", StartIndex = 940, Length = 5 });  //AIRFRAME REPAIR SERVICE AVAILABILITY/TYPE
            listAPT.Add(new DataIndex { ColumnName = "PowerPlantRepair", StartIndex = 945, Length = 5 });  //POWER PLANT (ENGINE) REPAIR AVAILABILITY/TYPE
            listAPT.Add(new DataIndex { ColumnName = "BottledOxygenType", StartIndex = 950, Length = 8 });  // TYPE OF BOTTLED OXYGEN AVAILABLE (VALUE REPRESENTS HIGH AND/ OR LOW PRESSURE REPLACEMENT BOTTLE)
            listAPT.Add(new DataIndex { ColumnName = "BulkOxygenType", StartIndex = 958, Length = 8 });  //TYPE OF BULK OXYGEN AVAILABLE (VALUE REPRESENTS HIGH AND/ OR LOW PRESSURE CYLINDERS)
            listAPT.Add(new DataIndex { ColumnName = "LightingSchedule", StartIndex = 966, Length = 7 });  //AIRPORT LIGHTING SCHEDULE
            listAPT.Add(new DataIndex { ColumnName = "BeaconSchedule", StartIndex = 973, Length = 7 });  //BEACON LIGHTING SCHEDULE
            listAPT.Add(new DataIndex { ColumnName = "ATCT", StartIndex = 980, Length = 1 });  //AIR TRAFFIC CONTROL TOWER LOCATED ON AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "UNICOMFrequencies", StartIndex = 981, Length = 7 });  //UNICOM FREQUENCY AVAILABLE AT THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "CTAFFrequency", StartIndex = 988, Length = 7 });  //COMMON TRAFFIC ADVISORY FREQUENCY (CTAF)
            listAPT.Add(new DataIndex { ColumnName = "SegmentedCircle", StartIndex = 995, Length = 4 });  //SEGMENTED CIRCLE AIRPORT MARKER SYSTEM ON THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "BeaconColor", StartIndex = 999, Length = 3 });  //LENS COLOR OF OPERABLE BEACON LOCATED ON THE AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "NonCommercialLandingFee", StartIndex = 1002, Length = 1 });  //LANDING FEE CHARGED TO NON-COMMERCIAL USERS OF AIRPORT
            listAPT.Add(new DataIndex { ColumnName = "MedicalUse", StartIndex = 1003, Length = 1 });  //A "Y" IN THIS FIELD INDICATES THAT THE LANDING FACILITY IS USED FOR MEDICAL PURPOSES
            listAPT.Add(new DataIndex { ColumnName = "SingleEngineGA", StartIndex = 1004, Length = 3 });  //SINGLE ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "MultiEngineGA", StartIndex = 1007, Length = 3 });  //MULTI ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "JetEngineGA", StartIndex = 1010, Length = 3 });  //JET ENGINE GENERAL AVIATION AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "HelicoptersGA", StartIndex = 1013, Length = 3 });  //GENERAL AVIATION HELICOPTER
            listAPT.Add(new DataIndex { ColumnName = "GlidersOperational", StartIndex = 1016, Length = 3 });  //OPERATIONAL GLIDERS
            listAPT.Add(new DataIndex { ColumnName = "MilitaryOperational", StartIndex = 1019, Length = 3 });  //OPERATIONAL MILITARY AIRCRAFT (INCLUDING HELICOPTERS)
            listAPT.Add(new DataIndex { ColumnName = "Ultralights", StartIndex = 1022, Length = 3 });  //ULTRALIGHT AIRCRAFT
            listAPT.Add(new DataIndex { ColumnName = "OperationsCommercial", StartIndex = 1025, Length = 6 });  //COMMERCIAL SERVICES
            listAPT.Add(new DataIndex { ColumnName = "OperationsCommuter", StartIndex = 1031, Length = 6 });  //COMMUTER SERVICES
            listAPT.Add(new DataIndex { ColumnName = "OperationsAirTaxi", StartIndex = 1037, Length = 6 });  //AIR TAXI
            listAPT.Add(new DataIndex { ColumnName = "OperationsGALocal", StartIndex = 1043, Length = 6 });  //GENERAL AVIATION LOCAL OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsGAItin", StartIndex = 1049, Length = 6 });  //GENERAL AVIATION ITINERANT OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsMilitary", StartIndex = 1055, Length = 6 });  //MILITARY AIRCRAFT OPERATIONS
            listAPT.Add(new DataIndex { ColumnName = "OperationsDate", StartIndex = 1061, Length = 10 });  //12-MONTH ENDING DATE ON WHICH ANNUAL OPERATIONS DATA IN ABOVE SIX FIELDS IS BASED (MM / DD / YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportPositionSource", StartIndex = 1071, Length = 16 });  //AIRPORT POSITION SOURCE
            listAPT.Add(new DataIndex { ColumnName = "AirportPositionSourceDate", StartIndex = 1087, Length = 10 });  //AIRPORT POSITION SOURCE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "AirportElevationSource", StartIndex = 1097, Length = 16 });  //AIRPORT ELEVATION SOURCE
            listAPT.Add(new DataIndex { ColumnName = "AirportElevationSourceDate", StartIndex = 1113, Length = 10 });  //AIRPORT ELEVATION SOURCE DATE (MM/DD/YYYY)
            listAPT.Add(new DataIndex { ColumnName = "ContractFuelAvailable", StartIndex = 1123, Length = 1 });  //CONTRACT FUEL AVAILABLE
            listAPT.Add(new DataIndex { ColumnName = "TransientStorage", StartIndex = 1124, Length = 12 });  //TRANSIENT STORAGE FACILITIES
            listAPT.Add(new DataIndex { ColumnName = "OtherServices", StartIndex = 1136, Length = 71 });  //OTHER AIRPORT SERVICES AVAILABLE
            listAPT.Add(new DataIndex { ColumnName = "WindIndicator", StartIndex = 1207, Length = 3 });  //WIND INDICATOR
            listAPT.Add(new DataIndex { ColumnName = "icao_ident", StartIndex = 1210, Length = 7 });  //ICAO IDENTIFIER
            listAPT.Add(new DataIndex { ColumnName = "AirportRecordFiller", StartIndex = 1217, Length = 312 });  //AIRPORT RECORD FILLER (BLANK)



            List<DataIndex> listATT = new List<DataIndex>();
            listATT.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listATT.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //LANDING FACILITY STATE POST OFFICE CODE
            listATT.Add(new DataIndex { ColumnName = "SequenceNumber", StartIndex = 16, Length = 2 });  //ATTENDANCE SCHEDULE SEQUENCE NUMBER
            listATT.Add(new DataIndex { ColumnName = "AttendanceSchedule", StartIndex = 18, Length = 108 });  //AIRPORT ATTENDANCE SCHEDULE(WHEN MINIMUM SERVICES ARE AVAILABLE


            List<DataIndex> listRWY = new List<DataIndex>();
            //listRWY.Add(new DataIndex { ColumnName="", StartIndex = 0, Length = 3 });  //RECORD TYPE INDICATOR
            listRWY.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listRWY.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //RUNWAY STATE POST OFFICE CODE
            listRWY.Add(new DataIndex { ColumnName = "RunwayID", StartIndex = 16, Length = 7 });  //RUNWAY IDENTIFICATION
            listRWY.Add(new DataIndex { ColumnName = "RunwayLength", StartIndex = 23, Length = 5 });  //PHYSICAL RUNWAY LENGTH (NEAREST FOOT
            listRWY.Add(new DataIndex { ColumnName = "RunwayWidth", StartIndex = 28, Length = 4 });  //PHYSICAL RUNWAY WIDTH (NEAREST FOOT
            listRWY.Add(new DataIndex { ColumnName = "RunwaySurfaceTypeCondition", StartIndex = 32, Length = 12 });  //RUNWAY SURFACE TYPE AND CONDITION
            listRWY.Add(new DataIndex { ColumnName = "RunwaySurfaceTreatment", StartIndex = 44, Length = 5 });  //RUNWAY SURFACE TREATMENT
            listRWY.Add(new DataIndex { ColumnName = "PavementClass", StartIndex = 49, Length = 11 });  //PAVEMENT CLASSIFICATION NUMBER (PCN
            listRWY.Add(new DataIndex { ColumnName = "EdgeLightsIntensity", StartIndex = 60, Length = 5 });  //RUNWAY LIGHTS EDGE INTENSITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndID", StartIndex = 65, Length = 3 });  //BASE END IDENTIFIER
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTrueAlignment", StartIndex = 68, Length = 3 });  //RUNWAY END TRUE ALIGNMENT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndILSType", StartIndex = 71, Length = 10 });  //INSTRUMENT LANDING SYSTEM (ILS) TYPE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRightTrafficPattern", StartIndex = 81, Length = 1 });  //RIGHT HAND TRAFFIC PATTERN FOR LANDING AIRCRAFT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndMarkingsType", StartIndex = 82, Length = 5 });  //RUNWAY MARKINGS  (TYPE)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndMarkingsCondition", StartIndex = 87, Length = 1 });  //RUNWAY MARKINGS  (CONDITION)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLatitude", StartIndex = 88, Length = 15 });  //LATITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLatitudeS", StartIndex = 103, Length = 12 });  //LATITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLongitude", StartIndex = 115, Length = 15 });  //LONGITUDE OF PHYSICAL RUNWAY END (FORMATTED
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalLongitudeS", StartIndex = 130, Length = 12 });  //LONGITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPhysicalElevation", StartIndex = 142, Length = 7 });  //ELEVATION (FEET MSL) AT PHYSICAL RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "BaseEndCrossingHeight", StartIndex = 149, Length = 3 });  //THRESHOLD CROSSING HEIGHT (FEET AGL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGlidePathAngle", StartIndex = 152, Length = 4 });  //VISUAL GLIDE PATH ANGLE (HUNDREDTHS OF DEGREES)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLatitude", StartIndex = 156, Length = 15 });  //LATITUDE  AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLatitudeS", StartIndex = 171, Length = 12 });  //LATITUDE  AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLongitude", StartIndex = 183, Length = 15 });  //LONGITUDE AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLongitudeS", StartIndex = 198, Length = 12 });  //LONGITUDE AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedElevation", StartIndex = 210, Length = 7 });  //ELEVATION AT DISPLACED THRESHOLD (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedLength", StartIndex = 217, Length = 4 });  //DISPLACED THRESHOLD - LENGTH IN FEET FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTDZElevation", StartIndex = 221, Length = 7 });  //ELEVATION AT TOUCHDOWN ZONE (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndVASI", StartIndex = 228, Length = 5 });  //VISUAL GLIDE SLOPE INDICATORS
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRVR", StartIndex = 233, Length = 3 });  // RUNWAY VISUAL RANGE EQUIPMENT (RVR)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndRVV", StartIndex = 236, Length = 1 });  //RUNWAY VISIBILITY VALUE EQUIPMENT (RVV)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndALS", StartIndex = 237, Length = 8 });  //APPROACH LIGHT SYSTEM
            listRWY.Add(new DataIndex { ColumnName = "BaseEndREIL", StartIndex = 245, Length = 1 });  //RUNWAY END IDENTIFIER LIGHTS (REIL) AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndCenterlineLights", StartIndex = 246, Length = 1 });  //RUNWAY CENTERLINE LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownLights", StartIndex = 247, Length = 1 });  //RUNWAY END TOUCHDOWN LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectDescription", StartIndex = 248, Length = 11 });  //CONTROLLING OBJECT DESCRIPTION
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectMarkLight", StartIndex = 259, Length = 4 });  //CONTROLLING OBJECT MARKED/LIGHTED
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPart77Category", StartIndex = 263, Length = 5 });  //FAA CFR PART 77 (OBJECTS AFFECTING NAVIGABLE AIRSPACE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectClearSlope", StartIndex = 268, Length = 2 });  //CONTROLLING OBJECT CLEARANCE SLOPE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectHeight", StartIndex = 270, Length = 5 });  //CONTROLLING OBJECT HEIGHT ABOVE RUNWAY
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectDistance", StartIndex = 275, Length = 5 });  //CONTROLLING OBJECT DISTANCE FROM RUNWAY 
            listRWY.Add(new DataIndex { ColumnName = "BaseEndObjectOffset", StartIndex = 280, Length = 7 });  //CONTROLLING OBJECT CENTERLINE OFFSET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndID", StartIndex = 287, Length = 3 });  //RECIPROCAL END IDENTIFIER
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTrueAlignment", StartIndex = 290, Length = 3 });  //RUNWAY END TRUE ALIGNMENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndILSType", StartIndex = 293, Length = 10 });  //INSTRUMENT LANDING SYSTEM (ILS) TYPE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRightTrafficPattern", StartIndex = 303, Length = 1 });  //RIGHT HAND TRAFFIC PATTERN FOR LANDING AIRCRAFT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndMarkingsType", StartIndex = 304, Length = 5 });  //RUNWAY MARKINGS  (TYPE)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndMarkingsCondition", StartIndex = 309, Length = 1 });  //RUNWAY MARKINGS  (CONDITION)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLatitude", StartIndex = 310, Length = 15 });  //LATITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLatitudeS", StartIndex = 325, Length = 12 });  //LATITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLongitude", StartIndex = 337, Length = 15 });  //LONGITUDE OF PHYSICAL RUNWAY END (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalLongitudeS", StartIndex = 352, Length = 12 });  //LONGITUDE OF PHYSICAL RUNWAY END (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPhysicalElevation", StartIndex = 364, Length = 7 });  //ELEVATION (FEET MSL) AT PHYSICAL RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndCrossingHeight", StartIndex = 371, Length = 3 });  //THRESHOLD CROSSING HEIGHT (FEET AGL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGlidePathAngle", StartIndex = 374, Length = 4 });  //VISUAL GLIDE PATH ANGLE (HUNDREDTHS OF DEGREES)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLatitude", StartIndex = 378, Length = 15 });  //LATITUDE  AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLatitudeS", StartIndex = 393, Length = 12 });  //LATITUDE  AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLongitude", StartIndex = 405, Length = 15 });  //LONGITUDE AT DISPLACED THRESHOLD (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLongitudeS", StartIndex = 420, Length = 12 });  //LONGITUDE AT DISPLACED THRESHOLD (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedElevation", StartIndex = 432, Length = 7 });  //ELEVATION AT DISPLACED THRESHOLD (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedLength", StartIndex = 439, Length = 4 });  //DISPLACED THRESHOLD - LENGTH IN FEET FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTDZElevation", StartIndex = 443, Length = 7 });  //ELEVATION AT TOUCHDOWN ZONE (FEET MSL)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndVASI", StartIndex = 450, Length = 5 });  //APPROACH SLOPE INDICATOR EQUIPMENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRVR", StartIndex = 455, Length = 3 });  //RUNWAY VISUAL RANGE EQUIPMENT (RVR)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndRVV", StartIndex = 458, Length = 1 });  //RUNWAY VISIBILITY VALUE EQUIPMENT (RVV)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndALS", StartIndex = 459, Length = 8 });  //APPROACH LIGHT SYSTEM
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndREIL", StartIndex = 467, Length = 1 });  //RUNWAY END IDENTIFIER LIGHTS (REIL) AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndCenterlineLights", StartIndex = 468, Length = 1 });  //RUNWAY CENTERLINE LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownLights", StartIndex = 469, Length = 1 });  //RUNWAY END TOUCHDOWN LIGHTS AVAILABILITY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectDescription", StartIndex = 470, Length = 11 });  //CONTROLLING OBJECT DESCRIPTION
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectMarkLight", StartIndex = 481, Length = 4 });  //CONTROLLING OBJECT MARKED/LIGHTED
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPart77Category", StartIndex = 485, Length = 5 });  //FAA CFR PART 77 (OBJECTS AFFECTING NAVIGABLE AIRSPACE)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectClearSlope", StartIndex = 490, Length = 2 });  //CONTROLLING OBJECT CLEARANCE SLOPE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectHeight", StartIndex = 492, Length = 5 }); //CONTROLLING OBJECT HEIGHT ABOVE RUNWAY
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectDistance", StartIndex = 497, Length = 5 });  //CONTROLLING OBJECT DISTANCE FROM RUNWAY END
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndObjectOffset", StartIndex = 502, Length = 7 });  //CONTROLLING OBJECT CENTERLINE OFFSET
            listRWY.Add(new DataIndex { ColumnName = "RunwayLengthSource", StartIndex = 509, Length = 16 });  //RUNWAY LENGTH SOURCE
            listRWY.Add(new DataIndex { ColumnName = "RunwayLengthSourceDate", StartIndex = 525, Length = 10 });  //RUNWAY LENGTH SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacitySW", StartIndex = 535, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Single wheel
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDW", StartIndex = 541, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Dual wheel
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDT", StartIndex = 547, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Two dual wheels
            listRWY.Add(new DataIndex { ColumnName = "RunwayWeightBerringCapacityDDT", StartIndex = 553, Length = 6 });  //RUNWAY WEIGHT-BEARING CAPACITY FOR Two dual wheels
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGradient", StartIndex = 559, Length = 5 });  //RUNWAY END GRADIENT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndGradientDirection", StartIndex = 564, Length = 4 });  //RUNWAY END GRADIENT DIRECTION (UP OR DOWN)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPositionSource", StartIndex = 568, Length = 16 });  //RUNWAY END POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndPositionSourceDate", StartIndex = 584, Length = 10 });  //RUNWAY END POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndElevationSource", StartIndex = 594, Length = 16 });  //RUNWAY END ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndElevationSourceDate", StartIndex = 610, Length = 10 });  //RUNWAY END ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdPositionSource", StartIndex = 620, Length = 16 });  //DISPLACED THESHOLD POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdPositionSourceDate", StartIndex = 636, Length = 10 });  // DISPLACED THESHOLD POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdElevationSource", StartIndex = 646, Length = 16 });  //DISPLACED THESHOLD ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndDisplacedThresholdElevationSourceDate", StartIndex = 662, Length = 10 });  //DISPLACED THESHOLD ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownZoneElevationSource", StartIndex = 672, Length = 16 });  //TOUCHDOWN ZONE ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTouchdownZoneElevationSourceDate", StartIndex = 688, Length = 10 });  // TOUCHDOWN ZONE ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTakeOffRunAvailableTORA", StartIndex = 698, Length = 5 });  //TAKEOFF RUN AVAILABLE (TORA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndTakeOffDistanceAvailableTODA", StartIndex = 703, Length = 5 });  //TAKEOFF DISTANCE AVAILABLE (TODA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndAcltStopDistanceAvailableASDA", StartIndex = 708, Length = 5 });  //ACLT STOP DISTANCE AVAILABLE (ASDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLDA", StartIndex = 713, Length = 5 });  //LANDING DISTANCE AVAILABLE (LDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSO", StartIndex = 718, Length = 5 });  //AVAILABLE LANDING DISTANCE FOR LAND AND HOLD SHORT OPERATIONS(LAHSO)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOIntRwyID", StartIndex = 723, Length = 7 });  //ID OF INTERSECTING RUNWAY DEFINING HOLD SHORT POINT
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOIntEntityDesc", StartIndex = 730, Length = 40 });  //DESCRIPTION OF ENTITY DEFINING HOLD SHORT POINT 
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLatitude", StartIndex = 770, Length = 15 });  //LATITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLatitudeS", StartIndex = 785, Length = 12 });  //LATITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLongitude", StartIndex = 797, Length = 15 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOHldPtLongitudeS", StartIndex = 812, Length = 12 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOCoordSrc", StartIndex = 824, Length = 16 });  //LAHSO HOLD SHORT POINT LAT/LONG SOURCE
            listRWY.Add(new DataIndex { ColumnName = "BaseEndLandingDistanceAvailableLAHSOCoordSrcDate", StartIndex = 840, Length = 10 });  //HOLD SHORT POINT LAT/LONG SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGradient", StartIndex = 850, Length = 5 });  //RUNWAY END GRADIENT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndGradientDirection", StartIndex = 855, Length = 4 });  //RUNWAY END GRADIENT DIRECTION (UP OR DOWN)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPositionSource", StartIndex = 859, Length = 16 });  //RUNWAY END POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndPositionSourceDate", StartIndex = 875, Length = 10 });  //RUNWAY END POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndElevationSource", StartIndex = 885, Length = 16 });  //RUNWAY END ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndElevationSourceDate", StartIndex = 901, Length = 10 });  //RUNWAY END ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdPositionSource", StartIndex = 911, Length = 16 });  //DISPLACED THESHOLD POSITION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdPositionSourceDate", StartIndex = 927, Length = 10 });  //DISPLACED THESHOLD POSITION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdElevationSource", StartIndex = 937, Length = 16 });  //DISPLACED THESHOLD ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndDisplacedThresholdElevationSourceDate", StartIndex = 953, Length = 10 });  //DISPLACED THESHOLD ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownZoneElevationSource", StartIndex = 963, Length = 16 });  //TOUCHDOWN ZONE ELEVATION SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTouchdownZoneElevationSourceDate", StartIndex = 979, Length = 10 });  //TOUCHDOWN ZONE ELEVATION SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTakeOffRunAvailableTORA", StartIndex = 989, Length = 5 });  //TAKEOFF RUN AVAILABLE (TORA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndTakeOffDistanceAvailableTODA", StartIndex = 994, Length = 5 });  //TAKEOFF DISTANCE AVAILABLE (TODA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndAcltStopDistanceAvailableASDA", StartIndex = 999, Length = 5 });  //ACLT STOP DISTANCE AVAILABLE (ASDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLDA", StartIndex = 1004, Length = 5 });  //LANDING DISTANCE AVAILABLE (LDA), IN FEET
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSO", StartIndex = 1009, Length = 5 });  //AVAILABLE LANDING DISTANCE FOR LAND AND HOLD SHORT OPERATIONS(LAHSO)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOIntRwyID", StartIndex = 1014, Length = 7 });  //ID OF INTERSECTING RUNWAY DEFINING HOLD SHORT POINT
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOIntEntityDesc", StartIndex = 1021, Length = 40 }); //DESCRIPTION OF ENTITY DEFINING HOLD SHORT POINT 
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude", StartIndex = 1061, Length = 15 });  //LATITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitudeS", StartIndex = 1076, Length = 12 });  //LATITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude", StartIndex = 1088, Length = 15 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (FORMATTED)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitudeS", StartIndex = 1103, Length = 12 });  //LONGITUDE OF LAHSO HOLD SHORT POINT (SECONDS)
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrc", StartIndex = 1115, Length = 16 });   //LAHSO HOLD SHORT POINT LAT/LONG SOURCE
            listRWY.Add(new DataIndex { ColumnName = "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate", StartIndex = 1131, Length = 10 });  //HOLD SHORT POINT LAT/LONG SOURCE DATE (MM/DD/YYYY)
            listRWY.Add(new DataIndex { ColumnName = "RunwayRecordFiller", StartIndex = 1141, Length = 388 });  //RUNWAY RECORD FILLER (BLANK)


            List<DataIndex> listRMK = new List<DataIndex>();
            listRMK.Add(new DataIndex { ColumnName = "SiteNumber", StartIndex = 3, Length = 11 });  //LANDING FACILITY SITE NUMBER
            listRMK.Add(new DataIndex { ColumnName = "State", StartIndex = 14, Length = 2 });  //LANDING FACILITY STATE POST OFFICE CODE
            listRMK.Add(new DataIndex { ColumnName = "RemarkElementName", StartIndex = 16, Length = 13 });  //REMARK ELEMENT NAME
            listRMK.Add(new DataIndex { ColumnName = "Remarks", StartIndex = 29, Length = 1500 });  //REMARK TEXT



            DataTable dtFacilities = new DataTable("Facilities");
            DataTable dtAttendence = new DataTable("Attendence");
            DataTable dtRunway = new DataTable("Runway");
            DataTable dtRemark = new DataTable("Remark");


            dtFacilities.Columns.Add("SiteNumber", typeof(string));  //0
            dtFacilities.Columns.Add("Type", typeof(string));  //1
            dtFacilities.Columns.Add("apt_ident", typeof(string)); //2
            dtFacilities.Columns.Add("EffectiveDate", typeof(DateTime)); //3
            dtFacilities.Columns[3].AllowDBNull = true;
            dtFacilities.Columns.Add("Region", typeof(string));  //4
            dtFacilities.Columns.Add("DistrictOffice", typeof(string));  //5
            dtFacilities.Columns.Add("State", typeof(string));  //6
            dtFacilities.Columns.Add("StateName", typeof(string)); //7
            dtFacilities.Columns.Add("County", typeof(string));  //8
            dtFacilities.Columns.Add("CountyState", typeof(string));  //9
            dtFacilities.Columns.Add("City", typeof(string));  //10
            dtFacilities.Columns.Add("AirportName", typeof(string));  //11
            dtFacilities.Columns.Add("Ownership", typeof(string));  //12
            dtFacilities.Columns.Add("Use", typeof(string)); //13
            dtFacilities.Columns.Add("Owner", typeof(string));  //14
            dtFacilities.Columns.Add("OwnerAddress", typeof(string));  //15
            dtFacilities.Columns.Add("OwnerCSZ", typeof(string));  //16
            dtFacilities.Columns.Add("OwnerPhone", typeof(string));  //17
            dtFacilities.Columns.Add("Manager", typeof(string));  //18
            dtFacilities.Columns.Add("ManagerAddress", typeof(string));  //19
            dtFacilities.Columns.Add("ManagerCSZ", typeof(string));  //20
            dtFacilities.Columns.Add("ManagerPhone", typeof(string));  //21
            dtFacilities.Columns.Add("X", typeof(string));  //22   //ARPLatitude
            dtFacilities.Columns.Add("ARPLatitudeS", typeof(string));  //23
            dtFacilities.Columns.Add("Y", typeof(string));  //24   //ARPLongitude
            dtFacilities.Columns.Add("ARPLongitudeS", typeof(string));  //25
            dtFacilities.Columns.Add("ARPMethod", typeof(string));  //26
            dtFacilities.Columns.Add("ARPElevation", typeof(decimal));  //27
            dtFacilities.Columns[27].AllowDBNull = true;
            dtFacilities.Columns.Add("ARPElevationMethod", typeof(string));  //28
            dtFacilities.Columns.Add("MagneticVariation", typeof(string));  //29
            dtFacilities.Columns.Add("MagneticVariationYear", typeof(int));  //30
            dtFacilities.Columns[30].AllowDBNull = true;
            dtFacilities.Columns.Add("TrafficPatternAltitude", typeof(int));  //31
            dtFacilities.Columns[31].AllowDBNull = true;
            dtFacilities.Columns.Add("ChartName", typeof(string));  //32
            dtFacilities.Columns.Add("DistanceFromCBD", typeof(int));  //33
            dtFacilities.Columns[33].AllowDBNull = true;
            dtFacilities.Columns.Add("DirectionFromCBD", typeof(string));  //34
            dtFacilities.Columns.Add("LandAreaCoveredByAirport", typeof(int)); //35
            dtFacilities.Columns[35].AllowDBNull = true;
            dtFacilities.Columns.Add("BoundaryARTCCID", typeof(string));  //36
            dtFacilities.Columns.Add("BoundaryARTCCComputerID", typeof(string));  //37
            dtFacilities.Columns.Add("BoundaryARTCCName", typeof(string));  //38
            dtFacilities.Columns.Add("ResponsibleARTCCID", typeof(string));  //39
            dtFacilities.Columns.Add("ResponsibleARTCCComputerID", typeof(string));  //40
            dtFacilities.Columns.Add("ResponsibleARTCCName", typeof(string));  //41
            dtFacilities.Columns.Add("TieInFSS", typeof(bool));  //42
            dtFacilities.Columns[42].AllowDBNull = true;
            dtFacilities.Columns.Add("TieInFSSID", typeof(string));  //43
            dtFacilities.Columns.Add("TieInFSSName", typeof(string));  //44
            dtFacilities.Columns.Add("AirportToFSSPhoneNumber", typeof(string));  //45
            dtFacilities.Columns.Add("TieInFSSTollFreeNumber", typeof(string));  //46
            dtFacilities.Columns.Add("AlternateFSSID", typeof(string));  //47
            dtFacilities.Columns.Add("AlternateFSSName", typeof(string));  //48
            dtFacilities.Columns.Add("AlternateFSSTollFreeNumber", typeof(string));  //49
            dtFacilities.Columns.Add("NOTAMFacilityID", typeof(string));  //50
            dtFacilities.Columns.Add("NOTAMService", typeof(bool));  //51
            dtFacilities.Columns[51].AllowDBNull = true;
            dtFacilities.Columns.Add("ActivationDate", typeof(string));  //52
            dtFacilities.Columns.Add("AirportStatusCode", typeof(string));  //53
            dtFacilities.Columns.Add("CertificationTypeDate", typeof(string));  //54
            dtFacilities.Columns.Add("FederalAgreements", typeof(string));  //55
            dtFacilities.Columns.Add("AirspaceDetermination", typeof(string));  //56
            dtFacilities.Columns.Add("CustomsAirportOfEntry", typeof(bool));  //57
            dtFacilities.Columns[57].AllowDBNull = true;
            dtFacilities.Columns.Add("CustomsLandingRights", typeof(bool));  //58
            dtFacilities.Columns[58].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryJointUse", typeof(bool));  //59
            dtFacilities.Columns[59].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryLandingRights", typeof(bool));  //60
            dtFacilities.Columns[60].AllowDBNull = true;
            dtFacilities.Columns.Add("InspectionMethod", typeof(string));  //61
            dtFacilities.Columns.Add("InspectionGroup", typeof(string));  //62
            dtFacilities.Columns.Add("LastInspectionDate", typeof(DateTime));  //63     MMDDYYYY ************************************
            dtFacilities.Columns[63].AllowDBNull = true;
            dtFacilities.Columns.Add("LastOwnerInformationDate", typeof(DateTime));  //64   MMDDYYYY ************************************
            dtFacilities.Columns[64].AllowDBNull = true;
            dtFacilities.Columns.Add("FuelTypes", typeof(string));  //65
            dtFacilities.Columns.Add("AirframeRepair", typeof(string));  //66
            dtFacilities.Columns.Add("PowerPlantRepair", typeof(string));  //67
            dtFacilities.Columns.Add("BottledOxygenType", typeof(string));  //68
            dtFacilities.Columns.Add("BulkOxygenType", typeof(string));  //69
            dtFacilities.Columns.Add("LightingSchedule", typeof(string));  //70
            dtFacilities.Columns.Add("BeaconSchedule", typeof(string));  //71
            dtFacilities.Columns.Add("ATCT", typeof(bool));  //72
            dtFacilities.Columns[72].AllowDBNull = true;
            dtFacilities.Columns.Add("UNICOMFrequencies", typeof(decimal)); //73
            dtFacilities.Columns[73].AllowDBNull = true;
            dtFacilities.Columns.Add("CTAFFrequency", typeof(decimal));  //74
            dtFacilities.Columns[74].AllowDBNull = true;
            dtFacilities.Columns.Add("SegmentedCircle", typeof(string));  //75
            dtFacilities.Columns.Add("BeaconColor", typeof(string));  //76
            dtFacilities.Columns.Add("NonCommercialLandingFee", typeof(bool));   //77
            dtFacilities.Columns[77].AllowDBNull = true;
            dtFacilities.Columns.Add("MedicalUse", typeof(bool));  //78
            dtFacilities.Columns[78].AllowDBNull = true;
            dtFacilities.Columns.Add("SingleEngineGA", typeof(int));  //79
            dtFacilities.Columns[79].AllowDBNull = true;
            dtFacilities.Columns.Add("MultiEngineGA", typeof(int));  //80
            dtFacilities.Columns[80].AllowDBNull = true;
            dtFacilities.Columns.Add("JetEngineGA", typeof(int));  //81
            dtFacilities.Columns[81].AllowDBNull = true;
            dtFacilities.Columns.Add("HelicoptersGA", typeof(int));  //82
            dtFacilities.Columns[82].AllowDBNull = true;
            dtFacilities.Columns.Add("GlidersOperational", typeof(int));  //83
            dtFacilities.Columns[83].AllowDBNull = true;
            dtFacilities.Columns.Add("MilitaryOperational", typeof(int));  //84
            dtFacilities.Columns[84].AllowDBNull = true;
            dtFacilities.Columns.Add("Ultralights", typeof(int));  //85
            dtFacilities.Columns[85].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsCommercial", typeof(int));  //86
            dtFacilities.Columns[86].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsCommuter", typeof(string));  //87
            dtFacilities.Columns.Add("OperationsAirTaxi", typeof(int));  //88
            dtFacilities.Columns[88].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsGALocal", typeof(int));  //89
            dtFacilities.Columns[89].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsGAItin", typeof(int));  //90
            dtFacilities.Columns[90].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsMilitary", typeof(int));  //91
            dtFacilities.Columns[91].AllowDBNull = true;
            dtFacilities.Columns.Add("OperationsDate", typeof(DateTime));  //92   
            dtFacilities.Columns[92].AllowDBNull = true;
            dtFacilities.Columns.Add("AirportPositionSource", typeof(string));  //93
            dtFacilities.Columns.Add("AirportPositionSourceDate", typeof(DateTime));  //94
            dtFacilities.Columns[94].AllowDBNull = true;
            dtFacilities.Columns.Add("AirportElevationSource", typeof(string));  //95
            dtFacilities.Columns.Add("AirportElevationSourceDate", typeof(DateTime));  //96
            dtFacilities.Columns[96].AllowDBNull = true;
            dtFacilities.Columns.Add("ContractFuelAvailable", typeof(bool));  //97
            dtFacilities.Columns[97].AllowDBNull = true;
            dtFacilities.Columns.Add("TransientStorage", typeof(string));  //98
            dtFacilities.Columns.Add("OtherServices", typeof(string));  //99
            dtFacilities.Columns.Add("WindIndicator", typeof(string));  //100
            dtFacilities.Columns.Add("icao_ident", typeof(string));  //101
            dtFacilities.Columns.Add("AirportRecordFiller", typeof(string));  //102
            dtFacilities.Columns.Add("CityId", typeof(int));  //103
            dtFacilities.Columns[103].AllowDBNull = true;

            ////////////////////////////Attendence////////////////////
            dtAttendence.Columns.Add("SiteNumber", typeof(string));  //0
            dtAttendence.Columns.Add("State", typeof(string));  //1
            dtAttendence.Columns.Add("SequenceNumber", typeof(int));  //2
            dtFacilities.Columns[2].AllowDBNull = true;
            dtAttendence.Columns.Add("AttendanceSchedule", typeof(string));  //3


            ////////////////////////////Runway////////////////////
            dtRunway.Columns.Add("SiteNumber", typeof(string));  //0
            dtRunway.Columns.Add("State", typeof(string));       //1
            dtRunway.Columns.Add("RunwayID", typeof(string));    //2
            dtRunway.Columns.Add("RunwayLength", typeof(int));   //3
            dtRunway.Columns[3].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWidth", typeof(int));    //4
            dtRunway.Columns[4].AllowDBNull = true;
            dtRunway.Columns.Add("RunwaySurfaceTypeCondition", typeof(string));  //5
            dtRunway.Columns.Add("RunwaySurfaceTreatment", typeof(string));      //6
            dtRunway.Columns.Add("PavementClass", typeof(string));               //7
            dtRunway.Columns.Add("EdgeLightsIntensity", typeof(string));         //8
            dtRunway.Columns.Add("BaseEndID", typeof(string));                   //9
            dtRunway.Columns.Add("BaseEndTrueAlignment", typeof(int));           //10
            dtRunway.Columns[10].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndILSType", typeof(string));              //11
            dtRunway.Columns.Add("BaseEndRightTrafficPattern", typeof(bool));    //12
            dtRunway.Columns[12].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndMarkingsType", typeof(string));         //13
            dtRunway.Columns.Add("BaseEndMarkingsCondition", typeof(string));    //14
            dtRunway.Columns.Add("BaseEndPhysicalLatitude", typeof(decimal));     //15
            dtRunway.Columns[15].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndPhysicalLatitudeS", typeof(string));    //16
            dtRunway.Columns.Add("BaseEndPhysicalLongitude", typeof(decimal));    //17
            dtRunway.Columns[17].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndPhysicalLongitudeS", typeof(string));   //18
            dtRunway.Columns.Add("BaseEndPhysicalElevation", typeof(decimal));   //19
            dtRunway.Columns[19].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndCrossingHeight", typeof(int));          //20
            dtRunway.Columns[20].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGlidePathAngle", typeof(decimal));      //21
            dtRunway.Columns[21].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLatitude", typeof(decimal));    //22
            dtRunway.Columns[22].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLatitudeS", typeof(string));   //23
            dtRunway.Columns.Add("BaseEndDisplacedLongitude", typeof(decimal));   //24
            dtRunway.Columns[24].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLongitudeS", typeof(string));  //25
            dtRunway.Columns.Add("BaseEndDisplacedElevation", typeof(decimal));  //26
            dtRunway.Columns[26].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedLength", typeof(int));         //27
            dtRunway.Columns[27].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTDZElevation", typeof(decimal));        //28
            dtRunway.Columns[28].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndVASI", typeof(string));                 //29
            dtRunway.Columns.Add("BaseEndRVR", typeof(string));                  //30
            dtRunway.Columns.Add("BaseEndRVV", typeof(bool));                    //31
            dtRunway.Columns[31].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndALS", typeof(string));                  //32 
            dtRunway.Columns.Add("BaseEndREIL", typeof(bool));                   //33
            dtRunway.Columns[33].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndCenterlineLights", typeof(bool));       //34
            dtRunway.Columns[34].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTouchdownLights", typeof(bool));        //35
            dtRunway.Columns[35].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectDescription", typeof(string));    //36
            dtRunway.Columns.Add("BaseEndObjectMarkLight", typeof(string));      //37
            dtRunway.Columns.Add("BaseEndPart77Category", typeof(string));       //38
            dtRunway.Columns.Add("BaseEndObjectClearSlope", typeof(int));        //39 
            dtRunway.Columns[39].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectHeight", typeof(int));            //40
            dtRunway.Columns[40].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectDistance", typeof(int));          //41
            dtRunway.Columns[41].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndObjectOffset", typeof(string));         //42  
            dtRunway.Columns.Add("ReciprocalEndID", typeof(string));             //43
            dtRunway.Columns.Add("ReciprocalEndTrueAlignment", typeof(int));     //44
            dtRunway.Columns[44].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndILSType", typeof(string));        //45
            dtRunway.Columns.Add("ReciprocalEndRightTrafficPattern", typeof(bool));  //46
            dtRunway.Columns[46].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndMarkingsType", typeof(string));       //47
            dtRunway.Columns.Add("ReciprocalEndMarkingsCondition", typeof(string));  //48
            dtRunway.Columns.Add("ReciprocalEndPhysicalLatitude", typeof(decimal));   //49
            dtRunway.Columns[49].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndPhysicalLatitudeS", typeof(string));  //50
            dtRunway.Columns.Add("ReciprocalEndPhysicalLongitude", typeof(decimal));  //51
            dtRunway.Columns[51].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndPhysicalLongitudeS", typeof(string)); //52 
            dtRunway.Columns.Add("ReciprocalEndPhysicalElevation", typeof(decimal)); //53
            dtRunway.Columns[53].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndCrossingHeight", typeof(int));        //54
            dtRunway.Columns[54].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGlidePathAngle", typeof(decimal));        //55
            dtRunway.Columns[55].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLatitude", typeof(decimal));  //56
            dtRunway.Columns[56].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLatitudeS", typeof(string)); //57
            dtRunway.Columns.Add("ReciprocalEndDisplacedLongitude", typeof(decimal)); //58
            dtRunway.Columns[58].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLongitudeS", typeof(string));//59
            dtRunway.Columns.Add("ReciprocalEndDisplacedElevation", typeof(decimal));//60
            dtRunway.Columns[60].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedLength", typeof(int));       //61
            dtRunway.Columns[61].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTDZElevation", typeof(decimal));      //62
            dtRunway.Columns[62].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndVASI", typeof(string));               //63 
            dtRunway.Columns.Add("ReciprocalEndRVR", typeof(string));                //64
            dtRunway.Columns.Add("ReciprocalEndRVV", typeof(bool));                  //65
            dtRunway.Columns[65].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndALS", typeof(string));                //66
            dtRunway.Columns.Add("ReciprocalEndREIL", typeof(bool));                 //67
            dtRunway.Columns[67].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndCenterlineLights", typeof(bool));    //68
            dtRunway.Columns[68].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTouchdownLights", typeof(bool));     //69
            dtRunway.Columns[69].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectDescription", typeof(string)); //70
            dtRunway.Columns.Add("ReciprocalEndObjectMarkLight", typeof(string));   //71
            dtRunway.Columns.Add("ReciprocalEndPart77Category", typeof(string));    //72
            dtRunway.Columns.Add("ReciprocalEndObjectClearSlope", typeof(int));     //73
            dtRunway.Columns[73].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectHeight", typeof(int));         //74
            dtRunway.Columns[74].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectDistance", typeof(int));       //75
            dtRunway.Columns[75].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndObjectOffset", typeof(string));      //76
            dtRunway.Columns.Add("RunwayLengthSource", typeof(string));             //77
            dtRunway.Columns.Add("RunwayLengthSourceDate", typeof(DateTime));       //78
            dtRunway.Columns[78].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacitySW", typeof(decimal)); //79
            dtRunway.Columns[79].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDW", typeof(decimal)); //80
            dtRunway.Columns[80].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDT", typeof(decimal)); //81
            dtRunway.Columns[81].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayWeightBerringCapacityDDT", typeof(decimal));    //82
            dtRunway.Columns[82].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGradient", typeof(decimal));               //83
            dtRunway.Columns[83].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndGradientDirection", typeof(string));       //84
            dtRunway.Columns.Add("BaseEndPositionSource", typeof(string));          //85
            dtRunway.Columns.Add("BaseEndPositionSourceDate", typeof(DateTime));    //86
            dtRunway.Columns[86].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndElevationSource", typeof(string));         //87
            dtRunway.Columns.Add("BaseEndElevationSourceDate", typeof(DateTime));   //88
            dtRunway.Columns[88].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedThresholdPositionSource", typeof(string)); //89
            dtRunway.Columns.Add("BaseEndDisplacedThresholdPositionSourceDate", typeof(DateTime));  //90
            dtRunway.Columns[90].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndDisplacedThresholdElevationSource", typeof(string));       //91
            dtRunway.Columns.Add("BaseEndDisplacedThresholdElevationSourceDate", typeof(DateTime)); //92
            dtRunway.Columns[92].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTouchdownZoneElevationSource", typeof(string));            //93
            dtRunway.Columns.Add("BaseEndTouchdownZoneElevationSourceDate", typeof(DateTime));      //94
            dtRunway.Columns[94].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTakeOffRunAvailableTORA", typeof(int));                    //95
            dtRunway.Columns[95].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndTakeOffDistanceAvailableTODA", typeof(int));               //96
            dtRunway.Columns[96].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndAcltStopDistanceAvailableASDA", typeof(int));              //97
            dtRunway.Columns[97].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLDA", typeof(int));                //98
            dtRunway.Columns[98].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSO", typeof(int));              //99
            dtRunway.Columns[99].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOIntRwyID", typeof(string));   //100
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOIntEntityDesc", typeof(string)); //101
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLatitude", typeof(decimal)); //102
            dtRunway.Columns[102].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLatitudeS", typeof(string)); //103
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLongitude", typeof(decimal)); //104
            dtRunway.Columns[104].AllowDBNull = true;
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOHldPtLongitudeS", typeof(string)); //105
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOCoordSrc", typeof(string));        //106 
            dtRunway.Columns.Add("BaseEndLandingDistanceAvailableLAHSOCoordSrcDate", typeof(DateTime));  //107
            dtRunway.Columns[107].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGradient", typeof(decimal));        //108
            dtRunway.Columns[108].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndGradientDirection", typeof(string));  //109
            dtRunway.Columns.Add("ReciprocalEndPositionSource", typeof(string));  //110
            dtRunway.Columns.Add("ReciprocalEndPositionSourceDate", typeof(DateTime)); //111
            dtRunway.Columns[111].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndElevationSource", typeof(string));  //112
            dtRunway.Columns.Add("ReciprocalEndElevationSourceDate", typeof(DateTime));  //113
            dtRunway.Columns[113].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdPositionSource", typeof(string)); //114
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdPositionSourceDate", typeof(DateTime));  //115
            dtRunway.Columns[115].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdElevationSource", typeof(string));  //116
            dtRunway.Columns.Add("ReciprocalEndDisplacedThresholdElevationSourceDate", typeof(DateTime)); //117
            dtRunway.Columns[117].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTouchdownZoneElevationSource", typeof(string));  //118
            dtRunway.Columns.Add("ReciprocalEndTouchdownZoneElevationSourceDate", typeof(DateTime)); //119
            dtRunway.Columns[119].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTakeOffRunAvailableTORA", typeof(int));  //120
            dtRunway.Columns[120].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndTakeOffDistanceAvailableTODA", typeof(int)); //121
            dtRunway.Columns[121].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndAcltStopDistanceAvailableASDA", typeof(int));  //122
            dtRunway.Columns[122].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLDA", typeof(int));  //123
            dtRunway.Columns[123].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSO", typeof(int)); //124
            dtRunway.Columns[124].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOIntRwyID", typeof(string)); //125
            dtRunway.Columns[125].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOIntEntityDesc", typeof(string));  //126
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude", typeof(decimal));  //127
            dtRunway.Columns[127].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitudeS", typeof(string));  //128
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude", typeof(decimal));  //129
            dtRunway.Columns[129].AllowDBNull = true;
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitudeS", typeof(string));  //130
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOCoordSrc", typeof(string));         //131
            dtRunway.Columns.Add("ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate", typeof(DateTime));  //132
            dtRunway.Columns[132].AllowDBNull = true;
            dtRunway.Columns.Add("RunwayRecordFiller", typeof(string));  //133



            ////////////////////////////Remark////////////////////
            dtRemark.Columns.Add("SiteNumber", typeof(string));  //0
            dtRemark.Columns.Add("State", typeof(string));  //1
            dtRemark.Columns.Add("RemarkElementName", typeof(string));  //2
            dtRemark.Columns.Add("Remarks", typeof(string));  //3
            try
            {
                DataRow dr;
                string strValue = "";
                string[] arr = { "", "", "" };
                string direction = "";
                foreach (var line in System.IO.File.ReadLines(@"D:\" + "APT.txt"))
                {
                    switch (line.Substring(0, 3))
                    {
                        case "APT":
                            {
                                dr = dtFacilities.NewRow();
                                foreach (var li in listAPT)
                                {

                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        case "EffectiveDate":
                                        case "OperationsDate":
                                        case "AirportPositionSourceDate":
                                        case "AirportElevationSourceDate":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDateTime(strValue);
                                                break;
                                            }
                                        //Decimal values
                                        case "ARPElevation":
                                        case "UNICOMFrequencies":
                                        case "CTAFFrequency":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDecimal(strValue);
                                                break;
                                            }
                                        // Int values
                                        case "MagneticVariationYear":
                                        case "TrafficPatternAltitude":
                                        case "DistanceFromCBD":
                                        case "LandAreaCoveredByAirport":
                                        case "SingleEngineGA":
                                        case "MultiEngineGA":
                                        case "JetEngineGA":
                                        case "HelicoptersGA":
                                        case "GlidersOperational":
                                        case "MilitaryOperational":
                                        case "Ultralights":
                                        case "OperationsCommercial":
                                        case "OperationsAirTaxi":
                                        case "OperationsGALocal":
                                        case "OperationsGAItin":
                                        case "OperationsMilitary":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        // Bool values
                                        case "TieInFSS":
                                        case "NOTAMService":
                                        case "CustomsAirportOfEntry":
                                        case "CustomsLandingRights":
                                        case "MilitaryJointUse":
                                        case "MilitaryLandingRights":
                                        case "ATCT":
                                        case "NonCommercialLandingFee":
                                        case "MedicalUse":
                                        case "ContractFuelAvailable":
                                            {
                                                dr[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)((strValue == "Y") ? true : false);
                                                break;
                                            }
                                        case "LastInspectionDate":
                                        case "LastOwnerInformationDate":
                                            {
                                                if (string.IsNullOrEmpty(strValue))
                                                {
                                                    dr[li.ColumnName] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    if (strValue.Length == 7)
                                                    {
                                                        dr[li.ColumnName] = new DateTime(Convert.ToInt16(strValue.Substring(3, 4)), Convert.ToInt16(strValue.Substring(0, 1)), Convert.ToInt16(strValue.Substring(1, 2)));
                                                    }
                                                    else
                                                    {
                                                        dr[li.ColumnName] = new DateTime(Convert.ToInt16(strValue.Substring(4, 4)), Convert.ToInt16(strValue.Substring(0, 2)), Convert.ToInt16(strValue.Substring(2, 2)));
                                                    }
                                                }
                                                break;
                                            }
                                        case "X":
                                        case "Y":
                                            {
                                                arr = strValue.Split('-');
                                                direction = arr[2].Substring(arr[2].Length - 1);
                                                arr[2] = arr[2].Remove(arr[2].Length - 1);
                                                dr[li.ColumnName] = (new Misc().CalculateLatitudeAndLongitude(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), direction)).ToString();
                                                break;
                                            }
                                        default:
                                            {
                                                dr[li.ColumnName] = strValue;
                                                break;
                                            }
                                    }
                                }
                                dr["CityId"] = DBNull.Value;
                                if (dr["StateName"].ToString() != "")
                                {
                                    dtFacilities.Rows.Add(dr);

                                }

                                break;
                            }
                        case "RWY":
                            {
                                DataRow drRWY;
                                drRWY = dtRunway.NewRow();
                                foreach (var li in listRWY)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        //Decimal Values
                                        case "BaseEndPhysicalElevation":
                                        case "BaseEndGlidePathAngle":
                                        case "BaseEndDisplacedElevation":
                                        case "BaseEndTDZElevation":
                                        case "ReciprocalEndPhysicalElevation":
                                        case "ReciprocalEndGlidePathAngle":
                                        case "ReciprocalEndDisplacedElevation":
                                        case "ReciprocalEndTDZElevation":
                                        case "RunwayWeightBerringCapacitySW":
                                        case "RunwayWeightBerringCapacityDW":
                                        case "RunwayWeightBerringCapacityDT":
                                        case "RunwayWeightBerringCapacityDDT":
                                        case "BaseEndGradient":
                                        case "ReciprocalEndGradient":

                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Math.Round(Convert.ToDecimal(strValue), 3);
                                                break;
                                            }
                                        //Integer Values
                                        case "RunwayLength":
                                        case "RunwayWidth":
                                        case "BaseEndTrueAlignment":
                                        case "BaseEndCrossingHeight":
                                        case "BaseEndDisplacedLength":
                                        case "BaseEndObjectClearSlope":
                                        case "BaseEndObjectHeight":
                                        case "BaseEndObjectDistance":
                                        case "ReciprocalEndTrueAlignment":
                                        case "ReciprocalEndCrossingHeight":
                                        case "ReciprocalEndDisplacedLength":
                                        case "ReciprocalEndObjectClearSlope":
                                        case "ReciprocalEndObjectHeight":
                                        case "ReciprocalEndObjectDistance":
                                        case "BaseEndTakeOffRunAvailableTORA":
                                        case "BaseEndTakeOffDistanceAvailableTODA":
                                        case "BaseEndAcltStopDistanceAvailableASDA":
                                        case "BaseEndLandingDistanceAvailableLDA":
                                        case "BaseEndLandingDistanceAvailableLAHSO":
                                        case "ReciprocalEndTakeOffRunAvailableTORA":
                                        case "ReciprocalEndTakeOffDistanceAvailableTODA":
                                        case "ReciprocalEndAcltStopDistanceAvailableASDA":
                                        case "ReciprocalEndLandingDistanceAvailableLDA":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSO":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        //Boolean values
                                        case "BaseEndRightTrafficPattern":
                                        case "BaseEndRVV":
                                        case "BaseEndREIL":
                                        case "BaseEndCenterlineLights":
                                        case "BaseEndTouchdownLights":
                                        case "ReciprocalEndRightTrafficPattern":
                                        case "ReciprocalEndRVV":
                                        case "ReciprocalEndREIL":
                                        case "ReciprocalEndCenterlineLights":
                                        case "ReciprocalEndTouchdownLights":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)((strValue == "Y") ? true : false);
                                                break;
                                            }
                                        //Lat long calculation
                                        case "BaseEndPhysicalLatitude":
                                        case "BaseEndPhysicalLongitude":
                                        case "BaseEndDisplacedLatitude":
                                        case "BaseEndDisplacedLongitude":
                                        case "ReciprocalEndPhysicalLatitude":
                                        case "ReciprocalEndPhysicalLongitude":
                                        case "ReciprocalEndDisplacedLatitude":
                                        case "ReciprocalEndDisplacedLongitude":
                                        case "BaseEndLandingDistanceAvailableLAHSOHldPtLatitude":
                                        case "BaseEndLandingDistanceAvailableLAHSOHldPtLongitude":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLatitude":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOHldPtLongitude":
                                            {
                                                if (string.IsNullOrEmpty(strValue))
                                                {
                                                    drRWY[li.ColumnName] = DBNull.Value;
                                                }
                                                else
                                                {
                                                    arr = strValue.Split('-');
                                                    direction = arr[2].Substring(arr[2].Length - 1);
                                                    arr[2] = arr[2].Remove(arr[2].Length - 1);
                                                    drRWY[li.ColumnName] = (new Misc().CalculateLatitudeAndLongitude(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), Convert.ToDouble(arr[2]), direction)).ToString();
                                                }
                                                break;
                                            }
                                        //Datetime
                                        case "RunwayLengthSourceDate":
                                        case "BaseEndPositionSourceDate":
                                        case "BaseEndElevationSourceDate":
                                        case "BaseEndDisplacedThresholdPositionSourceDate":
                                        case "BaseEndDisplacedThresholdElevationSourceDate":
                                        case "BaseEndTouchdownZoneElevationSourceDate":
                                        case "BaseEndLandingDistanceAvailableLAHSOCoordSrcDate":
                                        case "ReciprocalEndPositionSourceDate":
                                        case "ReciprocalEndElevationSourceDate":
                                        case "ReciprocalEndDisplacedThresholdPositionSourceDate":
                                        case "ReciprocalEndDisplacedThresholdElevationSourceDate":
                                        case "ReciprocalEndTouchdownZoneElevationSourceDate":
                                        case "ReciprocalEndLandingDistanceAvailableLAHSOCoordSrcDate":
                                            {
                                                drRWY[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToDateTime(strValue);
                                                break;

                                            }
                                        default:
                                            {
                                                drRWY[li.ColumnName] = strValue;
                                                break;
                                            }

                                    }
                                }
                                dtRunway.Rows.Add(drRWY);
                                break;
                            }
                        case "ATT":
                            {
                                DataRow drATT;
                                drATT = dtAttendence.NewRow();
                                foreach (var li in listATT)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    switch (li.ColumnName)
                                    {
                                        case "SequenceNumber":
                                            {
                                                drATT[li.ColumnName] = string.IsNullOrEmpty(strValue) ? DBNull.Value : (object)Convert.ToInt32(strValue);
                                                break;
                                            }
                                        default:
                                            {
                                                drATT[li.ColumnName] = strValue;
                                                break;
                                            }
                                    }
                                }
                                dtAttendence.Rows.Add(drATT);
                                break;
                            }
                        case "RMK":
                            {
                                DataRow drRMK;
                                drRMK = dtRemark.NewRow();
                                foreach (var li in listRMK)
                                {
                                    strValue = line.Substring(li.StartIndex, li.Length).Trim();
                                    drRMK[li.ColumnName] = strValue;
                                }
                                dtRemark.Rows.Add(drRMK);
                                break;
                            }
                    }
                }
                //dtFacilities.Rows.Cast<DataRow>().Where(r => r.ItemArray[7].ToString() == "").ToList().ForEach(r => r.Delete());
                SqlParameter[] param = new SqlParameter[1];
                //param[0] = new SqlParameter();
                //param[0].ParameterName = "tbFacility";
                //param[0].SqlDbType = SqlDbType.Structured;
                //param[0].Value = dtFacilities;
                //SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportData", param);

                //param = new SqlParameter[1];
                //param[0] = new SqlParameter();
                //param[0].ParameterName = "tbRunway";
                //param[0].SqlDbType = SqlDbType.Structured;
                //param[0].Value = dtRunway;
                //SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetRunwayData", param);

                //param = new SqlParameter[1];
                //param[0] = new SqlParameter();
                //param[0].ParameterName = "tbAirportAttendance";
                //param[0].SqlDbType = SqlDbType.Structured;
                //param[0].Value = dtAttendence;
                //SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportAttendanceData", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbAirportRemark";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dtRemark;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportRemarkData", param);
            }
            catch (Exception ex)
            {

            }
        }

        public ActionResult SetFileSizeForDocument()
        {


            var context = new GuardianAvionicsEntities();

            var docList = context.Documents.Where(d => !d.Deleted).ToList();
            foreach (var obj in docList)
            {
                try
                {
                    FileInfo finfo = new FileInfo(ConfigurationReader.DocumentPathKey + @"\" + obj.FileName);
                    if (finfo.Exists)
                    {
                        obj.FileSize = (Convert.ToInt32(finfo.Length));
                    }
                }
                catch (Exception ex)
                {

                }
            }
            context.SaveChanges();

            var docList1 = context.AircraftComponentModels.Where(d => !d.Deleted && d.UserMannual != null && d.UserMannual != "").ToList();
            foreach (var obj1 in docList1)
            {
                try
                {
                    FileInfo finfo = new FileInfo(ConfigurationReader.DocumentPathKey + @"\" + obj1.UserMannual);
                    if (finfo.Exists)
                    {
                        obj1.FileSize = (Convert.ToInt32(finfo.Length));
                    }
                }
                catch (Exception ex)
                {

                }
            }
            context.SaveChanges();

            return View();

        }

        #region Login & Register


        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult Document()
        {
            ViewBag.PageHeaderTitle = "Flight Bag";
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home", new { id = 1 });
            }
            Misc objMisc = new Misc();
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(profileId);
            DocumentResponseModelWeb res = new DocumentResponseModelWeb();
            res = new UnitDataHelper().GetDocumentForUser(new DocumnetRequestModel { ProfileId = profileId });
            res.ListOfDocuments.ForEach(s => { s.TId = objMisc.Encrypt(s.Id.ToString()); s.Id = 0; s.LastUpdateDate = Misc.dateToMMDDYYYY(s.LastUpdateDate.Split(' ')[0]); });
            //var DocumentModelWeb = new do
            GetLiveFlightByUserId(Convert.ToInt32(Session["ProfileId"]));
            return View("Document", res.ListOfDocuments);

        }




        //[SessionExpireFilterAttribute] this attribute cant be applied on
        //login action method as it will struck in infinite loop
        /// <summary>
        /// Return login view page and set values when error occoured.
        /// </summary>
        /// <param name="id">A parameter to identify that login has error or not</param>
        /// <returns>Login view</returns>
        /// 
        //public static decimal? ConvertToHours(string LexicalFormattedHours)
        //{
        //    try
        //    {
        //        TimeSpan timespan = XmlConvert.ToTimeSpan(LexicalFormattedHours);
        //        var a=(decimal)Math.Round(timespan.TotalHours, 2);
        //        return (decimal)Math.Round(timespan.TotalHours, 2);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        [HttpGet]
        public ActionResult Login(int? id)
        {
            //ConvertToHours("PT678H27M");
            var password1 = new Misc().Encrypt("imounts1!");
            if (Request.Cookies["UserName"] != null && Request.Cookies["Details"] != null)
            {
                string emailId = Request.Cookies["UserName"].Value;
                string password = Request.Cookies["Details"].Value;

                try
                {

                    emailId = new Misc().Decrypt(emailId);
                    password = new Misc().Decrypt(password);


                    LoginModel objModel = new LoginModel();
                    objModel.EmailId = emailId;
                    objModel.Password = password;
                    var responseLogin = new LoginHelper().IsFaceBookUser(objModel);
                    if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.AdminUser).ToString())
                    {
                        // Admin logged in

                        // save data in session variable.
                        Session["AdminName"] = responseLogin.FirstName;
                        Session["UserName"] = responseLogin.FirstName;
                        Session["AdminProfileId"] = responseLogin.ProfileId;
                        Session["UserType"] = responseLogin.UserType;
                        ViewBag.HasError = Boolean.FalseString;
                        Session["ImageUrl"] = responseLogin.ImageName;
                        return RedirectToAction("Dashboard", "Admin");
                    }
                    else if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
                    {
                        Session["UserLoggedIn"] = true;
                        Session["UserName"] = responseLogin.FirstName;
                        Session["ProfileId"] = responseLogin.ProfileId;
                        Session["UserType"] = responseLogin.UserType;
                        Session["ImageUrl"] = responseLogin.ImageName;
                        // no error found . set error flag as false
                        ViewBag.HasError = Boolean.FalseString;

                        if (responseLogin.UserType == Enumerations.UserType.Manufacturer.ToString())
                        {
                            return RedirectToAction("ListAllJPIData", "Data");
                        }
                        else if (responseLogin.UserType == Enumerations.UserType.Maintenance.ToString())
                        {
                            return RedirectToAction("SquawkList", "Aircraft");
                        }
                        else
                        {
                            return RedirectToAction("PilotSummary", "Home");
                        }
                    }
                    else
                    {
                        return View();
                    }

                }
                catch (Exception ex)
                {
                    return View();
                }

            }


            // var context = new GuardianAvionicsEntities();

            //if (id == null)
            //{
            //    return View();
            //}

            //if (id == 1)
            //{
            //    // login page has errors
            //    ViewBag.HasError = Boolean.TrueString;
            //    ModelState.AddModelError("", GAResource.SessionExpiredMessage);
            //    return View();
            //}

            //if (id != 2)
            //{
            //    string userType = CheckLoginCookies();
            //    if (userType == Enumerations.UserType.Admin.ToString())
            //    {
            //        return RedirectToAction("Index", "Admin");
            //    }

            //    if (userType == Enumerations.UserType.NotAdmin.ToString())
            //    {
            //        return RedirectToAction("PilotSummary");
            //    }
            //}
            return View();
        }


        public ActionResult AboutUs()
        {

            return View();
        }


        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult GoogleLogin()
        {
            //logger.Fatal("Google Login");


            if (!string.IsNullOrEmpty(Request.QueryString["error"]))
            {
                //logger.Fatal("Query String error");
                if (Request.QueryString["error"] == "access_denied")
                    return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
               
               string code=Request.QueryString["code"].ToString();
                string url = "https://accounts.google.com/o/oauth2/token";
                string clientid = ConfigurationReader.GmailClientId;
                string clientsecret = ConfigurationReader.GmailClientSecret;
                string redirection_url = Request.Url.AbsoluteUri.Split('?')[0];
                string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";
                UTF8Encoding utfenc = new UTF8Encoding();
                byte[] bytes = utfenc.GetBytes(poststring);
                Stream outputstream = null;
                try
                {
                    request.ContentLength = bytes.Length;
                    outputstream = request.GetRequestStream();
                    outputstream.Write(bytes, 0, bytes.Length);
                }
                catch { }
                var responsegoogle = (HttpWebResponse)request.GetResponse();
                var streamReader = new StreamReader(responsegoogle.GetResponseStream());
                string responseFromServer = streamReader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
                GoogleUserProfile profile = GetuserProfile(obj.access_token);

                //logger.Fatal("ID = " + profile.Id + Environment.NewLine + "Name = " + profile.DisplayName + Environment.NewLine + "Email =" + profile.Emails.Find(email => email.Type == "account").Value + Environment.NewLine + "Gender =" + profile.Gender + Environment.NewLine);
                var helper = new UserHelper();
                var objUser = new RegisterAndLoginByThirdParty
                {
                    EmailId = profile.email,
                    FirstName = profile.given_name,
                    LastName = profile.family_name,
                    SocialLoginAccountId = profile.id,
                    SocialLoginAccountType = 2
                };
                //logger.Fatal("Objest set");
                var response = helper.verifyThirdPartyLogin(objUser, true);

                //logger.Fatal("Call Verify method");
                //logger.Fatal("response.ResponseCode " + response.ResponseCode);

                 if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
                {
                    //logger.Fatal("Response = " + Newtonsoft.Json.JsonConvert.SerializeObject(response));
                    // save data in session variable.
                    Session["UserLoggedIn"] = true;
                    Session["UserName"] = response.FirstName;
                    Session["ProfileId"] = response.ProfileId;
                    Session["UserType"] = response.UserType;
                    Session["shownotification"] = false;
                    Session["PhoneNumber"] = response.CountryCode == null?"": response.CountryCode + response.PhoneNumber;
                    Session["EmailId"] = response.EmailId;
                    if (string.IsNullOrEmpty(response.ImageName))
                    {
                        Session["ImageUrl"] = Constants.UserProfileDefaultImage;
                    }
                    else
                    {
                        Session["ImageUrl"] = response.ImageName;
                    }
                    List<FeatureMasterModel> featureMasterModelList = new UserHelper().SetFeatureByUser(response.ProfileId);
                    Session["UserAuthorization"] = featureMasterModelList;
                    if (featureMasterModelList.FirstOrDefault(f => f.Name == "Flight Logs").IsEnable)
                    {
                        return RedirectToAction("PilotSummary", "Home");
                    }
                    else
                    {
                        return RedirectToAction("AircraftList", "Aircraft");
                    }

                }
                else if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString())
                {
                    return RedirectToAction("Login");
                }
                else if (response.ResponseCode == ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithGuardian).ToString())
                {
                    return RedirectToAction("Login");
                }
            }
            else
            {
                //logger.Fatal("Google Login Call");
                string clientid = ConfigurationReader.GmailClientId;
                string clientsecret = ConfigurationReader.GmailClientSecret;
                string redirection_url = Request.Url.AbsoluteUri.Split('?')[0];
                string url = "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&include_granted_scopes=true&redirect_uri=" + redirection_url + "&response_type=code&client_id=" + clientid + "";
                Response.Redirect(url);
            }
            return RedirectToAction("Login");
        }
        public GoogleUserProfile GetuserProfile(string accesstoken)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accesstoken + "";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse googleresponse = request.GetResponse();
            Stream dataStream = googleresponse.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            googleresponse.Close();
            GoogleUserProfile profile = new JavaScriptSerializer().Deserialize<GoogleUserProfile>(responseFromServer);
            return profile;
            //logger.Fatal("ID = " + profile.Id + Environment.NewLine + "Name = " + profile.DisplayName + Environment.NewLine + "Email =" + profile.Emails.Find(email => email.Type == "account").Value + Environment.NewLine + "Gender =" + profile.Gender + Environment.NewLine);
            //var helper = new UserHelper();
            //var objUser = new RegisterAndLoginByThirdParty
            //{
            //    EmailId = profile.email,
            //    FirstName = profile.given_name,
            //    LastName = profile.family_name,
            //    SocialLoginAccountId = profile.id,
            //    SocialLoginAccountType = 2
            //};
            ////logger.Fatal("Objest set");
            //var response = helper.verifyThirdPartyLogin(objUser, true);

            ////logger.Fatal("Call Verify method");
            ////logger.Fatal("response.ResponseCode " + response.ResponseCode);
            //if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
            //{
            //    //logger.Fatal("Response = " + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            //    // save data in session variable.
            //    Session["UserLoggedIn"] = true;
            //    Session["UserName"] = response.FirstName;
            //    Session["ProfileId"] = response.ProfileId;
            //    Session["UserType"] = response.UserType;
            //    if (string.IsNullOrEmpty(response.ImageName))
            //    {
            //        Session["ImageUrl"] = Constants.UserProfileDefaultImage;
            //    }
            //    else
            //    {
            //        Session["ImageUrl"] = response.ImageName;
            //    }
            //    List<FeatureMasterModel> featureMasterModelList = new UserHelper().SetFeatureByUser(response.ProfileId);
            //    Session["UserAuthorization"] = featureMasterModelList;
            //    if (featureMasterModelList.FirstOrDefault(f => f.Name == "Flight Logs").IsEnable)
            //    {
            //        return RedirectToAction("PilotSummary", "Home");
            //    }
            //    else
            //    {
            //        return RedirectToAction("AircraftList", "Aircraft");
            //    }

            //}
            //else if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString())
            //{
            //    return RedirectToAction("Login");
            //}
            //else if (response.ResponseCode == ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithGuardian).ToString())
            //{
            //    return RedirectToAction("Login");
            //}
            //return RedirectToAction("Login");
        }


        public void GoogleLoginCallBack()
        {
            //logger.Fatal("Google Login Callback");

            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
                string code = Request.QueryString["code"];
                string json = GoogleConnect.Fetch("me", code);
                GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
                //lblId.Text = profile.Id;
                //lblName.Text = profile.DisplayName;
                //lblEmail.Text = profile.Emails.Find(email => email.Type == "account").Value;
                //lblGender.Text = profile.Gender;
                //lblType.Text = profile.ObjectType;
                //ProfileImage.ImageUrl = profile.Image.Url;
                //pnlProfile.Visible = true;
                //btnLogin.Enabled = false;
                //logger.Fatal("ID = " + profile.Id + Environment.NewLine + "Fname Name = " + profile.Name.GivenName + Environment.NewLine + "LName = " + profile.Name.FamilyName + Environment.NewLine + "Email =" + profile.Emails.Find(email => email.Type == "account").Value + Environment.NewLine + "Gender =" + profile.Gender + Environment.NewLine);
            }
            else
            {
                //logger.Fatal("Google Login Call back query string [Code] is null");
            }
        }

        public class GoogleProfile
        {
            public string Id { get; set; }
            public string DisplayName { get; set; }
            public Name Name { get; set; }
            public Image Image { get; set; }
            public List<Email> Emails { get; set; }
            public string Gender { get; set; }
            public string ObjectType { get; set; }
        }
        public class Tokenclass
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
        }
        public class Email
        {
            public string Value { get; set; }
            public string Type { get; set; }
        }

        public class Image
        {
            public string Url { get; set; }
        }

        public class Name
        {
            public string GivenName { get; set; }
            public string FamilyName { get; set; }
        }

        public class GoogleUserProfile
        {
            public string id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string given_name { get; set; }
            public string family_name { get; set; }
            public string link { get; set; }
            public string picture { get; set; }
            public string gender { get; set; }
            public string locale { get; set; }
        }

        //private string CheckLoginCookies()
        //{
        //    HttpCookie profileId = Request.Cookies["ProfileId"];
        //    HttpCookie userName = Request.Cookies["UserName"];
        //    if (profileId == null || userName == null)
        //        return Boolean.FalseString;

        //    if (profileId.Value == "1")
        //    {
        //        Session["AdminName"] = userName.Value;
        //        Session["AdminProfileId"] = profileId.Value;
        //        Session["UserLoggedIn"] = true;

        //        return Enumerations.UserType.Admin.ToString();
        //    }
        //    else
        //    {
        //        Session["UserLoggedIn"] = true;
        //        Session["UserName"] = userName.Value;
        //        Session["ProfileId"] = profileId.Value;

        //        return Enumerations.UserType.NotAdmin.ToString();
        //    }
        //}

        ////////// Testing 
        #region code testing

        private void checkcodel()
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    /*
                     *  1 Find all 
                     *      a. rating which are going to expire in 3 days , not deleted , there license not deleted , there profile not deleted also license are not expired. 
                     *              we need Id , profile Id , Text = rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0.             
                     * 
                     *      b. license which are going to expire in 3 days , not deleted , there profile not deleted.
                     *              we need Id , profile Id and Text = license with reference Number #licenseRefNo is expiring in x days , Identity = 1.
                     *  
                     *      c. medical certificates which are going to expire in 3 days , not deleted , there profile not deleted.
                     *              we need Id = 0 , profileId , Text = medical certificate with reference Number #CertificateRefNo is expiring in x days , Identity = 2.
                     *   
                     *  2 Remove all the ratings for which license are included
                     *      
                     *  3 Add all the three list , Group them according to profile Id
                     *  
                     *  4 
                     *   pnList = new list<abcd>();
                     *   For each group check 
                     *      a. Is there any record in push table
                     *          a1. If no record then continue
                     *          a1. If record are there , is there any record which is valid , have token Id which is not black 
                     *              a11. If no then continue
                     *              a12. If yes then
                     *                  for each profile record
                     *                      pnList.add ( current record , profile record)
                     * 
                     * 
                     * 
                     * 5 add pnList in Push que.
                     * 
                     * 
                     * 
                     */


                    //rating for license with reference Number #licenseRefNo is expiring in x days , Identity = 0

                    // rating for which profile, license is not deleted , validUntil is not null , will expire in 3 days
                    var ExpiredRatings = context.Ratings.Where
                        (
                        rt => !rt.Deleted
                        && rt.ValidUntil != null
                        && !rt.License.Deleted
                        && !rt.License.Profile.Deleted
                        && rt.ValidUntil >= System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                        && rt.ValidUntil <= System.Data.Entity.DbFunctions.AddDays(System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow), 3)
                        && rt.License.ValidUntil > System.Data.Entity.DbFunctions.TruncateTime(DateTime.UtcNow)
                        )
                        .AsEnumerable()
                        .Select(s =>
                            new mlrList
                            {
                                id = s.LicenseId,
                                identity = 0,
                                profileId = s.License.ProfileId,
                                text = "Rating for license with reference Number "
                                        + s.License.ReferenceNumber
                                        + " is expiring on "
                                        + s.ValidUntil
                                        + " ."
                            })
                        .ToList();

                    // license with reference Number #licenseRefNo is expiring in x days , Identity = 1.

                    var ExpiredLicense = context.Licenses.Where
                        (
                            l => !l.Profile.Deleted
                            && !l.Deleted
                            //&& l.ValidUntil != null
                            && l.ValidUntil > DateTime.UtcNow
                            && l.ValidUntil < System.Data.Entity.DbFunctions.AddDays(DateTime.UtcNow, 3)
                        )
                        .Select(s =>
                            new mlrList
                            {
                                id = s.Id,
                                identity = 1,
                                profileId = s.ProfileId,
                                text = "License with reference Number "
                                        + s.ReferenceNumber
                                        + " is expiring on "
                                        + s.ValidUntil
                                        + " ."
                            })
                        .ToList();

                    // remove ratings for which license are expiring (are in ExpiredLicense list).
                    // a.id is rating's license id
                    ExpiredRatings = ExpiredRatings.Where(a => !ExpiredLicense.Any(el => el.id == a.id)).ToList();

                    //medical certificate with reference Number #CertificateRefNo is expiring in x days

                    var ExpiredM = context.MedicalCertificates.Where
                        (
                            m => !m.Deleted
                                && !m.Profile.Deleted
                                && m.ValidUntil != null
                                && m.ValidUntil > DateTime.UtcNow
                                && m.ValidUntil < System.Data.Entity.DbFunctions.AddDays(DateTime.UtcNow, 3)
                        )
                        .Select(s =>
                            new mlrList
                            {
                                id = s.Id,
                                identity = 2,
                                profileId = s.ProfileId,
                                text = "Medical certificate with reference Number "
                                        + s.ReferenceNumber
                                        + " is expiring on "
                                        + s.ValidUntil
                                        + " ."
                            })
                        .ToList();

                    // combine all the list into one
                    var combinedList = new List<mlrList>();

                    combinedList.AddRange(ExpiredRatings);
                    combinedList.AddRange(ExpiredLicense);
                    combinedList.AddRange(ExpiredM);

                    // group by profile id
                    //var groupList = combinedList.GroupBy(a => a.profileId);

                    var groupList = from cl in combinedList
                                    group cl by cl.profileId into grp
                                    orderby grp.Key
                                    select grp;

                    // final output list
                    var completeList = new List<mlrList>();

                    foreach (var group in groupList)
                    {
                        //if rateGroup is empty then continue
                        // if it has any element then group.first() will not we null
                        if (!group.Any())
                            continue;

                        var profileId = group.First().profileId;
                        var push = context.PushNotifications.Where(pr => pr.ProfileId == profileId).ToList();

                        var pushList = new List<PushNotification>();
                        //if profile is empty or push notification is not enabled or tokeinId is empty then continue

                        if (push.Any())
                            foreach (var p in push)
                            {
                                if (p == default(PushNotification) || !p.IsValid || string.IsNullOrEmpty(p.TokenId))
                                    continue;
                                pushList.Add(p);
                            }

                        if (pushList.Any())
                        {
                            // fore each element send push notification to all the profiles associated with that profile.
                            foreach (var element in group)
                            {
                                foreach (var p in pushList)
                                {
                                    // add token to mlrList and add it to combinedList
                                    completeList.Add(CreateMLRListObject(element, p.TokenId));
                                }
                            }
                        }

                        // send push notification for all the elements of this group
                        if (completeList.Any())
                            QueueMessages(completeList);

                        combinedList.Clear();
                    }


                }// using



                // send push notification
            }
            catch (Exception e)
            {

            }
        }

        private void QueueMessages(List<mlrList> combinedList)
        {
            ////create the puchbroker object
            //var push = new PushBroker();

            ////Wire up the events for all the services that the broker registers
            ////push.OnNotificationSent += NotificationSent;
            ////push.OnChannelException += ChannelException;
            ////push.OnServiceException += ServiceException;
            ////push.OnNotificationFailed += NotificationFailed;
            ////push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            ////push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            ////push.OnChannelCreated += ChannelCreated;
            ////push.OnChannelDestroyed += ChannelDestroyed;


            //// send push notification

            ////-------------------------
            //// APPLE NOTIFICATIONS
            ////-------------------------
            ////Configure and start Apple APNS
            //// IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to
            ////generate one for connecting to Sandbox, and one for connecting to Production.  You must
            //// use the right one, to match the provisioning profile you build your
            ////   app with!
            //try
            //{
            //    var appleCert = System.IO.File.ReadAllBytes(@"C:\Ideavate\GuardianAvionics\pushNotifications\Certificates_.p12");

            //    //IMPORTANT: If you are using a Development provisioning Profile, you must use
            //    // the Sandbox push notification server 
            //    //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
            //    // 'false')
            //    //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
            //    //Production push notification server
            //    //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
            //    //'true')
            //    push.RegisterAppleService(new ApplePushChannelSettings(false, appleCert, "", false));
            //    //Extension method
            //    //Fluent construction of an iOS notification
            //    //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
            //    // generated within your iOS app itself when the Application Delegate
            //    //  for registered for remote notifications is called, 
            //    // and the device token is passed back to you

            //    foreach (var msg in combinedList)
            //    {
            //        if (!String.IsNullOrEmpty(msg.tokenId) && !String.IsNullOrEmpty(msg.text))
            //            push.QueueNotification(new AppleNotification()
            //                                        .ForDeviceToken(msg.tokenId)//the recipient device id
            //                                        .WithAlert(msg.text)//the message
            //                                        .WithBadge(1)
            //                                        .WithSound("sound.caf")
            //                                        );
            //    }

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

        }
        ////Currently it will raise only for android devices
        //static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        //{
        //    //Do something here
        //}

        ////this even raised when a notification is successfully sent
        //static void NotificationSent(object sender, INotification notification)
        //{
        //    //Do something here
        //}

        ////this is raised when a notification is failed due to some reason
        //static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        //{
        //    //Do something here
        //}

        ////this is fired when there is exception is raised by the channel
        //static void ChannelException
        //    (object sender, IPushChannel channel, Exception exception)
        //{
        //    //Do something here
        //}

        ////this is fired when there is exception is raised by the service
        //static void ServiceException(object sender, Exception exception)
        //{
        //    //Do something here
        //}

        ////this is raised when the particular device subscription is expired
        //static void DeviceSubscriptionExpired(object sender,
        //string expiredDeviceSubscriptionId,
        //    DateTime timestamp, INotification notification)
        //{
        //    //Do something here
        //}

        ////this is raised when the channel is destroyed
        //static void ChannelDestroyed(object sender)
        //{
        //    //Do something here
        //}

        ////this is raised when the channel is created
        //static void ChannelCreated(object sender, IPushChannel pushChannel)
        //{
        //    //Do something here
        //}
        public class mlrList
        {
            public int id { get; set; }

            public int profileId { get; set; }

            public int identity { get; set; }

            public string text { get; set; }

            public string tokenId { get; set; }
        }
        /// <summary>
        /// adds token to mlrList object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="token"></param>
        /// <returns>mlrList object</returns>
        public mlrList CreateMLRListObject(mlrList obj, string token)
        {
            // we are checking token before sending so it cant be null
            return new mlrList
            {
                tokenId = token,
                text = obj.text.Replace(" 00:00:00.0000000", ""),
                profileId = obj.profileId,
                identity = obj.identity,
                id = obj.id,
            };
        }


        #endregion code testing
        ////////// Testing 

        public ActionResult Demo()
        {
            LoginModel objLogin = new LoginModel
            {
                EmailId = "demo@guardianavionics.com",
                Password = "demouser"
            };
            var LogHeler = new LoginHelper();
            var responseLogin = LogHeler.IsFaceBookUser(objLogin);


            List<FeatureMasterModel> featureMasterModelList = new UserHelper().SetFeatureByUser(responseLogin.ProfileId);
            Session["UserAuthorization"] = featureMasterModelList;

            // save data in session variable.
            Session["UserLoggedIn"] = true;
            Session["UserName"] = responseLogin.FirstName;
            Session["ProfileId"] = responseLogin.ProfileId;
            Session["UserType"] = responseLogin.UserType;
            Session["ImageUrl"] = responseLogin.ImageName;
            Session["shownotification"] = false;
            // no error found . set error flag as false
            ViewBag.HasError = Boolean.FalseString;
            return RedirectToAction("PilotSummary", "Home");
        }

        /// <summary>
        /// Checks user details and accordingly login user or send error.
        /// </summary>
        /// <param name="objLogin">LoginModel object</param>
        /// <param name="isKeepMeSignIn">Bool variable for setting flag for keep me sign in</param>
        /// <returns>Login View or ListAllJpiData view</returns>
        [HttpPost]
        public ActionResult Login(LoginModel objLogin, bool isKeepMeSignIn)
        {
            // assuming that login page has errors
            ViewBag.HasError = Boolean.TrueString;
            if (!ModelState.IsValid) return View();

            var LogHeler = new LoginHelper();

            var responseLogin = LogHeler.IsFaceBookUser(objLogin);

            UserHelper userHelper = new UserHelper();
            Session["SetCookies"] = isKeepMeSignIn;
            if (responseLogin.TwoStepAuthentication == true)
            {
                Session["shownotification"] = true;
            }
            else
            {
                Session["shownotification"] = false;
            }
            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.AdminUser).ToString())
            {
                // Admin logged in

                // save data in session variable.
                Session["AdminName"] = responseLogin.FirstName;
                Session["UserName"] = responseLogin.FirstName;
                Session["AdminProfileId"] = responseLogin.ProfileId;
                Session["UserType"] = responseLogin.UserType;
                Session["ProfileId"] = responseLogin.ProfileId;
                Session["PhoneNumber"] = responseLogin.CountryCode + responseLogin.PhoneNumber;
                Session["EmailId"] = responseLogin.EmailId;
                ViewBag.HasError = Boolean.FalseString;
                Session["ImageUrl"] = responseLogin.ImageName;
                var featureResponse = userHelper.SetFeatureByUser(responseLogin.ProfileId);
                Session["UserAuthorization"] = featureResponse;
                featureResponse.ForEach(f => f.IsEnable = true);
                //call function for generate with check and set otp after encryption

                // save data in cookies
                if (Request.Cookies["UserOTP"] == null && responseLogin.TwoStepAuthentication == true)
                {
                    GenerateOTPForAuthentication(responseLogin.CountryCode + responseLogin.PhoneNumber, responseLogin.EmailId);
                    return RedirectToAction("AuthenticationCode", "Home");
                }
                else
                {
                    if (isKeepMeSignIn)
                    {
                        SetCookieForLogin(objLogin.EmailId, objLogin.Password);
                    }
                    return RedirectToAction("Dashboard", "Admin");
                }
            }

            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString())
            {
                //ModelState.AddModelError("", "User is blocked , please contcat techsupport@guardianavionicsdata.com ".ToUpper());
                ModelState.AddModelError("", GA.Common.ResourcrFiles.UserProfile.Messages.BlockedUser.ToString());
                return View("Login");
            }

            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.EmailIdDoNotExist).ToString())
            {
                //ModelState.AddModelError("", "Email Id do not exist. ".ToUpper());
                ModelState.AddModelError("", GA.Common.ResourcrFiles.UserProfile.Messages.InvalidCredentials.ToString());
                return View("Login");
            }

            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.PasswordDoNotExist).ToString())
            {
                ModelState.AddModelError("", GA.Common.ResourcrFiles.UserProfile.Messages.InvalidCredentials.ToString());
                return View("Login");
            }

            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.InvalidUserNameOrPassword).ToString())
            {
                //ModelState.AddModelError("", "Email Id do not exist. ".ToUpper());
                ModelState.AddModelError("", GA.Common.ResourcrFiles.UserProfile.Messages.InvalidCredentials.ToString());
                return View("Login");
            }

            if (responseLogin.ResponseCode == ((int)Enumerations.LoginReturnCodes.Exception).ToString())
            {
                //ModelState.AddModelError("", "An Exception Occoured".ToUpper());
                ModelState.AddModelError("", GA.Common.ResourcrFiles.UserProfile.Messages.LoginException.ToString());
                return View("Login");
            }


            // save data in cookies


            List<FeatureMasterModel> featureMasterModelList = userHelper.SetFeatureByUser(responseLogin.ProfileId);
            Session["UserAuthorization"] = featureMasterModelList;

            // save data in session variable.
            Session["UserLoggedIn"] = true;
            Session["UserName"] = responseLogin.FirstName;
            Session["ProfileId"] = responseLogin.ProfileId;
            Session["UserType"] = responseLogin.UserType;
            Session["ImageUrl"] = responseLogin.ImageName;
            Session["PhoneNumber"] = responseLogin.CountryCode + responseLogin.PhoneNumber;
            Session["EmailId"] = responseLogin.EmailId;
            // no error found . set error flag as false
            ViewBag.HasError = Boolean.FalseString;
            if (Request.Cookies["UserOTP"] == null && responseLogin.TwoStepAuthentication == true)
            {
                GenerateOTPForAuthentication(responseLogin.CountryCode + responseLogin.PhoneNumber, responseLogin.EmailId);
                return RedirectToAction("AuthenticationCode", "Home");
            }
            if (isKeepMeSignIn)
            {
                SetCookieForLogin(objLogin.EmailId, objLogin.Password);
            }

            if (responseLogin.UserType == Enumerations.UserType.InventoryAdmin.ToString() ||
                responseLogin.UserType == Enumerations.UserType.InventorySubAdmin.ToString() ||
                responseLogin.UserType == Enumerations.UserType.InventoryProduction.ToString())
            {
                return RedirectToAction("PurchaseOrder", "Inventory");
            }
            else if (responseLogin.UserType == Enumerations.UserType.InventorySales.ToString())
            {
                return RedirectToAction("Invoice", "Inventory");
            }
            else if (responseLogin.UserType == Enumerations.UserType.Manufacturer.ToString())
            {
                return RedirectToAction("ListAllJPIData", "Data");
            }
            else if (responseLogin.UserType == Enumerations.UserType.Maintenance.ToString())
            {
                Session["ImageUrl"] = Constants.UserProfileDefaultImage;
                return RedirectToAction("SquawkList", "Aircraft");
            }
            else
            {

                if (featureMasterModelList.FirstOrDefault(f => f.Name == "Flight Logs").IsEnable)
                {
                    return RedirectToAction("PilotSummary", "Home");
                }
                else
                {
                    return RedirectToAction("AircraftList", "Aircraft");
                }
            }

        }


        [HttpPost]
        public ActionResult AuthenticationCode(LoginOTPModel getOTP)
        {
            string OTPvalue = Session["UserOTP"].ToString();
            if (getOTP.OTP == null)
            {
                return View();
            }
            if (OTPvalue != getOTP.OTP)
            {
                ModelState.AddModelError("", "Please enter valid OTP");
                return View();
            }
            bool setCookies = Convert.ToBoolean(Session["SetCookies"].ToString());
            string UserType = Session["UserType"].ToString();
            int ProfileId = Convert.ToInt32(Session["ProfileId"]);
            if (setCookies)
            {
                var context = new GuardianAvionicsEntities();
                var profiledetail = context.Profiles.Where(x => x.Id == ProfileId).FirstOrDefault();
                string Password = new Misc().Decrypt(profiledetail.Password);
                SetCookieForLogin(profiledetail.EmailId, Password);
            }
            SetCookieForLoginOTP(getOTP.OTP);
            if (UserType == Enumerations.UserType.InventoryAdmin.ToString() ||
                UserType == Enumerations.UserType.InventorySubAdmin.ToString() ||
                UserType == Enumerations.UserType.InventoryProduction.ToString())
            {
                return RedirectToAction("PurchaseOrder", "Inventory");
            }
            else if (UserType == Enumerations.UserType.InventorySales.ToString())
            {
                return RedirectToAction("Invoice", "Inventory");
            }
            else if (UserType == Enumerations.UserType.Manufacturer.ToString())
            {
                return RedirectToAction("ListAllJPIData", "Data");
            }
            else if (UserType == Enumerations.UserType.Maintenance.ToString())
            {
                Session["ImageUrl"] = Constants.UserProfileDefaultImage;
                return RedirectToAction("SquawkList", "Aircraft");
            }
            else
            {
                UserHelper userHelper = new UserHelper();
                List<FeatureMasterModel> featureMasterModelList = userHelper.SetFeatureByUser(ProfileId);
                Session["UserAuthorization"] = featureMasterModelList;
                if (featureMasterModelList.FirstOrDefault(f => f.Name == "Flight Logs").IsEnable)
                {
                    return RedirectToAction("PilotSummary", "Home");
                }
                else
                {
                    return RedirectToAction("AircraftList", "Aircraft");
                }
            }
        }

        public void GenerateOTPForAuthentication(string Phonenumber, string emailId)
        {
            try
            {
                WriteLog("calling GenerateOTPForAuthentication function" + Phonenumber);
                if (Phonenumber == null || Phonenumber == "")
                {
                    Phonenumber = Session["PhoneNumber"].ToString();
                }
                if (emailId == null || emailId == "")
                {
                    emailId = Session["EmailId"].ToString();
                }
                string randomstring = DoWork();
                if (Phonenumber.Length > 0)
                {
                    string accountSid = ConfigurationReader.TwiloAccountSid.ToString();
                    string authToken = ConfigurationReader.TwiloAuthToken.ToString();
                    string FromUserNumber = ConfigurationReader.TwiloFromuserNumber.ToString();
                    var client = new TwilioRestClient(accountSid, authToken);
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var message = MessageResource.Create(
                        body: "Use the code " + randomstring + " to verify your guardian account .To keep your account safe , never share your OTP with anyone ",
                        from: new Twilio.Types.PhoneNumber(FromUserNumber),
                        to: new Twilio.Types.PhoneNumber(Phonenumber), //Add here your number
                        client: client
                    );
                }
                if (emailId.Length > 0)
                {
                    SendOtpOnMail(randomstring, emailId);
                }
                Session["UserOTP"] = randomstring;
                WriteLog("send otp" + randomstring);
            }
            catch (Exception ex)
            {
                Session["UserOTP"] = "";
                WriteLog(ex.Message);
            }

        }
        public void SendOtpOnMail(string randomstring, string emailId)
        {
            var LogHeler = new LoginHelper();
            string result = LogHeler.SendOTPEmail(randomstring, emailId);
        }


        public void WriteLog(string strMessage)
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", "C:\\inetpub\\wwwroot\\GuardianAvionics\\TestEnv\\WebSite", "Log.txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();

            }
            catch (Exception ex)
            {

            }
        }


        [HttpGet]
        public ActionResult AuthenticationCode()
        {
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        public string DoWork()
        {
            string _numbers = "0123456789";
            Random random = new Random();
            StringBuilder builder = new StringBuilder(6);
            string numberAsString = "";
            int numberAsNumber = 0;
            for (var i = 0; i < 6; i++)
            {
                builder.Append(_numbers[random.Next(0, _numbers.Length)]);
            }
            numberAsString = builder.ToString();
            numberAsNumber = int.Parse(numberAsString);

            return numberAsString;
        }


        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult UserProfile()
        {
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            if (TempData["ProfileStatus"] != null)
            {
                ViewBag.ProfileMsg = TempData["ProfileStatus"].ToString();
                TempData["ProfileStatus"] = null;
            }
            else
            {
                ViewBag.ProfileMsg = "";
            }

            ViewBag.PageHeaderTitle = "Pilot Profile";
            var helper = new UserHelper();
            var userData = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
            Session["UserName"] = userData.FirstName;
            Session["ImageUrl"] = userData.ProfileImagePath;
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            userData.medicalCertificate = new MedicalCertificateModel();
            userData.MedicalCertificateClassList = helper.GetMedicalCertClass();
            userData.ClasssId = userData.MedicalCertificateClassList.Count == 0 ? null : (int?)userData.MedicalCertificateClassList.FirstOrDefault().value;
            GetLiveFlightByUserId(Convert.ToInt32(Session["ProfileId"]));
            return View("UserProfile", userData);
        }

        public ActionResult SaveProfileImage()
        {
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            var docfile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            string uniqueName = new UserHelper().GenerateUniqueImageNameUser(profileId.ToString());
            new Misc().SaveFileToS3Bucket(uniqueName, docfile.InputStream, "Image");
            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
            Session["ImageUrl"] = path + uniqueName;
            var context = new GuardianAvionicsEntities();
            var userDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == profileId);
            if (userDetails != null)
            {
                if (!string.IsNullOrEmpty(userDetails.ImageUrl))
                {
                    bool isFileExist = false;
                    new Misc().ReadFileFromS3("Image", userDetails.ImageUrl, out isFileExist);
                    if (isFileExist)
                    {
                        new Misc().DeleteFileFromS3Bucket(userDetails.ImageUrl, "Image");
                    }
                }
                userDetails.ImageUrl = uniqueName;

                context.Profiles.FirstOrDefault(f => f.Id == profileId).LastUpdated = DateTime.UtcNow;

                context.SaveChanges();


            }
            return Json(path + uniqueName);
        }

        [SessionExpireFilterAttribute]
        [HttpPost]
        public ActionResult UserProfile(UserProfileWeb objUserProfileWeb)
        {
            bool returnvalue = true;
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            try
            {
                DateTime dt = Convert.ToDateTime(objUserProfileWeb.DateOfBirth);
                if (dt < (DateTime)SqlDateTime.MinValue)
                {
                    isValid = false;
                }

            }
            catch (Exception ex)
            {
                isValid = false;
            }

            var helper = new UserHelper();
            if (!ModelState.IsValid || !isValid)
            {
                if (!isValid)
                {
                    ModelState.AddModelError("DateOfBirth", "Invalid date of birth");
                }
                var userData = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
                objUserProfileWeb.MedicalCertificateList = userData.MedicalCertificateList;
                objUserProfileWeb.LicensesList = userData.LicensesList;
                objUserProfileWeb.RatingsList = userData.RatingsList;
                objUserProfileWeb.medicalCertificate = new MedicalCertificateModel();
                objUserProfileWeb.MedicalCertificateClassList = helper.GetMedicalCertClass();
                objUserProfileWeb.ProfileImagePath = userData.ProfileImagePath;
                objUserProfileWeb.CountryCode = userData.CountryCode;
                objUserProfileWeb.Countries = userData.Countries;
                objUserProfileWeb.ClasssId = objUserProfileWeb.MedicalCertificateClassList.Count == 0 ? null : (int?)objUserProfileWeb.MedicalCertificateClassList.FirstOrDefault().value;
                //objUserProfileWeb.SubscriptionDetails = userData.SubscriptionDetails;
                objUserProfileWeb.SubViewModelList = userData.SubViewModelList;
                //objUserProfileWeb.TransactionModelList = userData.TransactionModelList;
                return View("UserProfile", objUserProfileWeb);
            }
            var isSaved = helper.SaveUserProfileWeb(objUserProfileWeb, Convert.ToInt32(Session["ProfileId"]));
            if (isSaved == "Email Id already exists.")
            {

                ModelState.AddModelError("EmailId", "Email Id already exists.");
                var userData = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
                objUserProfileWeb.MedicalCertificateList = userData.MedicalCertificateList;
                objUserProfileWeb.LicensesList = userData.LicensesList;
                objUserProfileWeb.RatingsList = userData.RatingsList;
                objUserProfileWeb.medicalCertificate = new MedicalCertificateModel();
                objUserProfileWeb.MedicalCertificateClassList = helper.GetMedicalCertClass();
                objUserProfileWeb.SubViewModelList = userData.SubViewModelList;
                objUserProfileWeb.ProfileImagePath = userData.ProfileImagePath;
                objUserProfileWeb.CountryCode = userData.CountryCode;
                objUserProfileWeb.Countries = userData.Countries;
                objUserProfileWeb.ClasssId = objUserProfileWeb.MedicalCertificateClassList.Count == 0 ? null : (int?)objUserProfileWeb.MedicalCertificateClassList.FirstOrDefault().value;
                return View("UserProfile", objUserProfileWeb);
            }

            if (objUserProfileWeb.CountryCode == "0" && objUserProfileWeb.PhoneNumber != null)
            {
                ModelState.AddModelError("PhoneNumber", "Please enter phone number with country code");
                returnvalue = false;
            }
            else if (objUserProfileWeb.CountryCode != "0" && objUserProfileWeb.PhoneNumber == null)
            {
                ModelState.AddModelError("PhoneNumber", "Please enter phone number with country code");
                returnvalue = false;
            }
            if (objUserProfileWeb.TwoFactorAuthentication == true)
            {
                if (objUserProfileWeb.PhoneNumber == null) { returnvalue = false; }

                if (objUserProfileWeb.EmailId == null) { returnvalue = false; }
            }
            //if ((objUserProfileWeb.PhoneNumber == null && objUserProfileWeb.CountryCode == "0") && objUserProfileWeb.TwoFactorAuthentication == true)
            //{
            //    ModelState.AddModelError("PhoneNumber", "Please enter phone number with country code");
            //    returnvalue = false;
            //}
            if (returnvalue == false)
            {
                //ModelState.AddModelError("PhoneNumber", "Please enter phone number with country code");
                var userData = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
                objUserProfileWeb.MedicalCertificateList = userData.MedicalCertificateList;
                objUserProfileWeb.LicensesList = userData.LicensesList;
                objUserProfileWeb.RatingsList = userData.RatingsList;
                objUserProfileWeb.medicalCertificate = new MedicalCertificateModel();
                objUserProfileWeb.MedicalCertificateClassList = helper.GetMedicalCertClass();
                objUserProfileWeb.SubViewModelList = userData.SubViewModelList;
                objUserProfileWeb.ProfileImagePath = userData.ProfileImagePath;
                objUserProfileWeb.CountryCode = userData.CountryCode;
                objUserProfileWeb.Countries = userData.Countries;
                objUserProfileWeb.ClasssId = objUserProfileWeb.MedicalCertificateClassList.Count == 0 ? null : (int?)objUserProfileWeb.MedicalCertificateClassList.FirstOrDefault().value;
                return View("UserProfile", objUserProfileWeb);
            }

            TempData["ProfileStatus"] = (isSaved == Boolean.TrueString ? "Success" : "Error");
            Session["UserName"] = objUserProfileWeb.FirstName;
            //return Json(helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"])));
            return RedirectToAction("UserProfile");
        }

        public JsonResult VerifyPhoneNumber(string Phonenumber, string emailId)
        {
            string data = "";
            var context = new GuardianAvionicsEntities();
            int profileid = Convert.ToInt32(Session["ProfileId"]);
            var detail = context.UserDetails.Where(x => x.ProfileId == profileid).FirstOrDefault();
            var profiles = context.Profiles.Where(x => x.Id == profileid).FirstOrDefault();
            var OldPhonenumber = detail.CountryCode + detail.PhoneNumber;
            bool callFunction = false;
            if (detail.TwoStepAuthentication == null)
            {
                if (Phonenumber != null && Phonenumber != "") { callFunction = true; }
                if (emailId != null && emailId != "") { callFunction = true; }
                if (callFunction == true)
                {
                    data = "show pop up";
                    GenerateOTPForAuthentication(Phonenumber, emailId);
                }
            }
            else
            {
                if (Phonenumber != null && Phonenumber != "") { if (OldPhonenumber != Phonenumber) { callFunction = true; } }
                if (emailId != null && emailId != "") { if (profiles.EmailId != emailId) { callFunction = true; } }
                if (callFunction == true)
                {
                    data = "show pop up";
                    GenerateOTPForAuthentication(Phonenumber, emailId);
                }
                else
                {
                    data = "hide pop up";
                }
            }
            return Json(new { data }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult matchOTPNumber(string OTP, string Phonenumber, string CountryCode, string emailId)
        {
            string returnstring = "true";
            string data = Session["UserOTP"].ToString();
            if (data != OTP)
            {
                returnstring = "false";
                return Json(new { returnstring }, JsonRequestBehavior.AllowGet);
            }
            var context = new GuardianAvionicsEntities();
            int profileid = Convert.ToInt32(Session["ProfileId"]);
            var detail = context.UserDetails.Where(x => x.ProfileId == profileid).FirstOrDefault();
            detail.TwoStepAuthentication = true;
            detail.PhoneNumber = Phonenumber;
            detail.CountryCode = CountryCode;
            var Email = context.Profiles.Where(x => x.Id == profileid).FirstOrDefault();
            Email.EmailId = emailId;
            context.SaveChanges();

            return Json(new { returnstring }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Logout()
        {
            Session.RemoveAll();
            Session.Abandon();
            Session.Clear();

            if (Request.Cookies["UserName"] != null)
            {
                Response.Cookies.Remove("UserName");
                HttpCookie myCookie = new HttpCookie("UserName");
                myCookie.Expires = DateTime.Now.AddYears(-10);
                myCookie.Value = null;
                Response.Cookies.Add(myCookie);
            }

            if (Request.Cookies["Details"] != null)
            {
                Response.Cookies.Remove("Details");
                HttpCookie myCookie = new HttpCookie("Details");
                myCookie.Expires = DateTime.Now.AddYears(-10);
                myCookie.Value = null;
                Response.Cookies.Add(myCookie);
            }
            return RedirectToAction("Login");
        }

        private void SetCookieForLogin(string emailId, string password)
        {
            HttpCookie Details, UserName, UserOTP;//, set the encrypted password in details

            #region"Initializing cookie"

            if (Request.Cookies["UserName"] == null || Request.Cookies["Details"] == null)
            {
                // create new cookies
                Details = new HttpCookie("Details");
                UserName = new HttpCookie("UserName");
                //UserLoggedIn = new HttpCookie("UserLoggedIn");
            }
            else
            {
                // update old cookies
                Details = Request.Cookies["Details"];
                UserName = Request.Cookies["UserName"];
                //UserOTP = new HttpCookie("UserOTP");
                //UserLoggedIn = Request.Cookies["UserLoggedIn"];
            }

            #endregion

            #region "Setting Cookie Value"

            // ReSharper disable once PossibleNullReferenceException
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            Details.Value = new Misc().Encrypt(password.ToString());

            // ReSharper disable once PossibleNullReferenceException
            UserName.Value = new Misc().Encrypt(emailId.ToString());

            //UserLoggedIn.Value = responseLogin.FirstName;

            UserName.Expires = DateTime.Now.AddDays(30);
            Details.Expires = DateTime.Now.AddDays(30);

            //UserLoggedIn.Expires = DateTime.Now.AddDays(30);

            Response.Cookies.Add(Details);
            Response.Cookies.Add(UserName);

            //Response.Cookies.Add(UserLoggedIn);

            #endregion
        }

        private void SetCookieForLoginOTP(string OTP)
        {
            HttpCookie UserOTP;//, set the encrypted password in details
            #region "Setting Cookie Value"
            if (OTP != "")
            {
                UserOTP = new HttpCookie("UserOTP");
                UserOTP.Value = new Misc().Encrypt(OTP.ToString());
                UserOTP.Expires = DateTime.Now.AddDays(15);
                Response.Cookies.Add(UserOTP);
            }
            #endregion
        }


        public ActionResult RegisterTemp()
        {
            return View();
        }

        //[DisplayName("Signup1")]
        /// <summary>
        /// Returns Register view
        /// flag = null if register by normal user. Flag = 1 means admin will register for manufacturer user
        /// </summary>
        /// <returns>Registeration view</returns>
        public ActionResult Register(int? flag, int? mId)
        {

            var helper = new UserHelper();
            var model = new UserModelWeb();
            model = helper.SetSequrityQuetions(model);
            var context = new GuardianAvionicsEntities();
            model.CountryCode = "0";
            model.Countries = context.Countries.Select(x => new SelectListItem()
            {
                Text = "(" + x.ShortName + ") " + x.CountryCode,
                Value = x.CountryCode
            }).ToList();
            model.Countries.Add(new SelectListItem() { Text = "--", Value = "0" });

            if (flag == null)
            {
                ViewBag.PageHeaderTitle = "New User Registration";
                model.IsRegisterByAdmin = false;
                model.ViewName = "";
            }
            else
            {
                ViewBag.PageHeaderTitle = "Manufacturer User Registration";
                if (flag == 1 && Session["UserType"] != null)
                {
                    if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString())
                    {
                        model.IsRegisterByAdmin = true;
                        if (mId != null)
                        {
                            model.ViewName = "ComponentManufacturer";
                            model.CompManufacturerId = mId ?? 0;
                        }
                        else
                        {
                            model.ViewName = "AdminHome";
                        }
                    }
                    else
                    {
                        //redirect to login
                        return RedirectToAction("Login");
                    }

                }
                else
                {
                    //Invalid operation and redirect to login
                    return RedirectToAction("Login");
                }
            }
            model.DateOfBirth = null;
            return View(model);
        }


        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// Register user and redirect to login view
        /// </summary>
        /// <param name="objUserWeb">UserModelWeb object containing fields for registration</param>
        /// <returns>when unsuccessful registration view otherwise login view</returns>
        [HttpPost]
        //[DisplayName("Signup2")]
        public ActionResult Register(UserModelWeb objUserWeb)
        {
            var helper = new UserHelper();
            var model = new UserModelWeb();
            model = helper.SetSequrityQuetions(model);
            model.DateOfBirth = objUserWeb.DateOfBirth;
            model.CountryCode = objUserWeb.CountryCode == null ? "0" : objUserWeb.CountryCode;
            model.CountryCode = objUserWeb.CountryCode == "" ? "0" : objUserWeb.CountryCode;
            var context = new GuardianAvionicsEntities();
            model.Countries = context.Countries.Select(x => new SelectListItem()
            {
                Text = "(" + x.ShortName + ") " + x.CountryCode,
                Value = x.CountryCode
            }).ToList();
            model.Countries.Add(new SelectListItem() { Text = "--", Value = "0" });
            //UserModel objUser = new UserModel();
            if (!ModelState.IsValid)
            {
                objUserWeb.ComponentManufacturerList = model.ComponentManufacturerList;
                objUserWeb.SecurityQuestionsList = model.SecurityQuestionsList;
                return View("Register", objUserWeb);
            }
            //convert object of UserModelWeb to UserModel.
            var objUser = new UserModel
            {
                City = objUserWeb.City,
                DateOfBirth = Misc.GetStringOnlyDate(objUserWeb.DateOfBirth),
                EmailId = objUserWeb.EmailId,
                FirstName = objUserWeb.FirstName,
                LastName = objUserWeb.LastName,
                Password = objUserWeb.Password,
                PhoneNumber = objUserWeb.PhoneNumber,
                CountryCode = objUserWeb.CountryCode,
                SecurityAnswer = objUserWeb.SecurityAnswer,
                State = objUserWeb.State,
                StreetAddress = objUserWeb.StreetAddress,
                ZipCode = objUserWeb.ZipCode,
                CompanyName = objUserWeb.CompanyName,
                UserType = (objUserWeb.IsRegisterByAdmin) ? Enumerations.UserType.Manufacturer.ToString() : Enumerations.UserType.User.ToString(),
                ManufacturerId = objUserWeb.CompManufacturerId,
                IsRegisterByAdmin = objUserWeb.IsRegisterByAdmin

            };

            var RegHelper = new UserHelper();

            //objUser.SecurityQuestion = RegHelper.GetSecurityQuestionFromModel(objUserWeb.DDLid);
            objUser.SecurityQuestionId = objUserWeb.DDLid;
            var regResponse = RegHelper.RegisterUser(objUser, true);

            if (regResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.DuplicateEmailId).ToString())
            {
                ModelState.AddModelError("", GAResource.DuplicateEmailIdMessage);
                return View("Register", model);
            }

            if (regResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                ModelState.AddModelError("", GAResource.ExceptionMessage);
                return View("Register", model);
            }

            if (regResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Success).ToString())
            {
                if (objUserWeb.IsRegisterByAdmin && Session["UserType"] != null)
                {
                    if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString())
                    {
                        if (objUserWeb.ViewName == "AdminHome")
                        {
                            return RedirectToAction("ManufacturerUser", "Admin");
                        }
                        else
                        {
                            return RedirectToAction("ComponentManufacturer", "Admin", new { flag = 1 });
                        }
                    }
                }
                else
                {
                    //var context = new GuardianAvionicsEntities();
                    var profile = context.Profiles.FirstOrDefault(f => f.EmailId == objUser.EmailId);

                    //Set 6 month free subscription

                    Session["UserAuthorization"] = RegHelper.SetFeatureByUser(profile.Id);
                    if (!String.IsNullOrEmpty(profile.UserDetail.ImageUrl))
                    {
                        string path = ConfigurationReader.ImageServerPathKey;
                        if (!string.IsNullOrEmpty(path))
                            Session["ImageUrl"] = path + profile.UserDetail.ImageUrl;
                        else
                            Session["ImageUrl"] = string.Empty;
                    }
                    else
                    {
                        Session["ImageUrl"] = Constants.UserProfileDefaultImage;
                    }


                    Session["UserLoggedIn"] = true;
                    Session["UserName"] = profile.UserDetail.FirstName;
                    Session["ProfileId"] = profile.Id;
                    Session["UserType"] = profile.UserMaster.UserType;
                    Session["shownotification"] = false;
                    // save data in Application's session state.
                    return RedirectToAction("PilotSummary", "Home");
                }

            }
            return View("Register", model);
        }


        #endregion Login & Register


        #region Forgot Password


        public ActionResult CheckEmail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CheckEmail(string Email)
        {
            var helper = new UserHelper();
            var VerifyEmailId = helper.CheckEmailId(Email);

            if (VerifyEmailId)
            {
                ModelState.AddModelError("", GAResource.EmailIdDoNotExistMessage);
                return View();
            }
            TempData["EmailId"] = Email;
            return RedirectToAction("CheckSequrityQuestion");
        }

        /// <summary>
        /// Checks if emailId exists in database
        /// </summary>
        /// <param name="Email">Email id of user</param>
        /// <returns>If email Id is correct password sent to email id</returns>
        public JsonResult Checkmail(string Email)
        {
            var helper = new UserHelper();
            var VerifyEmailId = helper.CheckEmailId(Email);

            return Json(VerifyEmailId ? GAResource.PasswordSentToEmailIdMessage : GAResource.EmailIdDoNotExistMessage);
        }


        public ActionResult CheckSequrityQuestion()
        {
            var email = TempData["EmailId"] as string;

            var helper = new UserHelper();
            var securityQuestion = helper.GetSecurityQuestion(email);

            ViewBag.SecurityQuestion = securityQuestion;
            ViewBag.EmailId = email;

            TempData.Keep();
            return View();
        }

        [HttpPost]
        public ActionResult CheckSequrityQuestion(string Answer)
        {
            var EmailId = ViewBag.Email;

            var helper = new UserHelper();
            bool IsCorrect = helper.CheckSecurityQuestion(Answer, EmailId);

            if (!IsCorrect)
            {
                ModelState.AddModelError("", "Incorrect Answer !!");
                return View();
            }
            return RedirectToAction("ResetPassword");
        }

        [HttpGet]
        public JsonResult ValidateSecurityAnswer(string email, string securityAnswer)
        {
            bool result = false;
            result = new UserHelper().ValidateSecurityAnswer(email, securityAnswer);
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string password)
        {
            return View();
        }


        public JsonResult SemdMailForResetPassword(string userEmail)
        {
            UserHelper objHelper = new UserHelper();
            string message = objHelper.SemdMailForResetPassword(userEmail);
            return Json(new { message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SelectQuestionByEmailid(string userEmail)
        {
            string message = "";
            ResetPasswordModel objResetPassModel = new ResetPasswordModel() { EmailId = userEmail };
            UserHelper objUserHelper = new UserHelper();
            var response = objUserHelper.GetSecurityQuestionByEmailId(objResetPassModel);
            message = (response.ResponseMessage != "Success") ? GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString() : response.SecurityQuestion;
            return Json(new { message }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult RegMaintenanceUser(string id)
        {

            RegMaintenenceUserModel obj = new RegMaintenenceUserModel();
            if (TempData["Message"] != null)
            {
                ViewBag.ProfileMessage = TempData["Message"].ToString();
                TempData["Message"] = null;
            }
            else
            {
                ViewBag.ProfileMessage = "";
            }
            string message = string.Empty;
            UserHelper objHelper = new UserHelper();
            message = objHelper.ValidateResetPasswordUrl(id);
            if (message.Split(',')[0] != "The link sent in the email cannot not be used multiple times.")
            {
                ViewBag.message = "Success";
                obj = new UserHelper().GetUserDetailByResetPassUniqueId(id);

            }
            else
            {
                ViewBag.message = "Failure";
            }

            ViewBag.UniqueCode = id;
            return View(obj);
        }

        [HttpPost]
        public ActionResult RegMaintenanceUser(RegMaintenenceUserModel objModel)
        {
            if (!ModelState.IsValid) return View();

            if (objModel.Password != objModel.ConfirmPassword)
            {
                ModelState.AddModelError("ConfirmPassword", "Password and Confirm password not match.");
                return View(objModel);
            }
            ViewBag.message = "Success";
            var response = new UserHelper().AddMaintainanceUserDetails(objModel);

            TempData["Message"] = response.ResponseMessage;
            ViewBag.ProfileMessage = response.ResponseMessage;

            if (response.ResponseMessage != "Success")
            {
                return View(objModel);
            }

            var profile = new UserHelper().GetProfileDetailByEmailId(objModel.EmailId);

            Session["ImageUrl"] = Constants.UserProfileDefaultImage;
            Session["UserLoggedIn"] = true;
            Session["UserName"] = profile.UserDetail.FirstName;
            Session["ProfileId"] = profile.Id;
            Session["UserType"] = profile.UserMaster.UserType;

            // save data in Application's session state.
            return RedirectToAction("SquawkList", "Aircraft");
        }


        public ActionResult LoggedInUserChangedPassword()
        {
            return View();
        }


        public ActionResult ChangePassword(string id)
        {
            string message = string.Empty;
            UserHelper objHelper = new UserHelper();
            message = objHelper.ValidateResetPasswordUrl(id);
            ViewBag.message = message;
            ViewBag.UniqueCode = id;
            return View();
        }


        public JsonResult ChangePasswordByEmail(string email, string newPassword, string UniqueCode)
        {
            ChangePasswordRequetModel objModel = new ChangePasswordRequetModel
            {
                EmailId = email,
                NewPassword = newPassword,
                Password = string.Empty,
                isQueAndAnsVerified = true
            };
            LoginHelper objHelper = new LoginHelper();
            string message = objHelper.ChangePassword(objModel, UniqueCode).ResponseMessage;
            return Json(new { message }, JsonRequestBehavior.AllowGet);
        }

        #endregion Forgot Password


        #region preference



        /// <summary>
        /// Gets all the preference of user and retrun view
        /// </summary>
        /// <returns>Preference veiw</returns>
        /// 
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult Preferences()
        {
            ViewBag.PageHeaderTitle = "Settings";
            if (Session == null)
            {
                @TempData["Message"] = GAResource.SessionExpiredMessage;
                RedirectToAction("Login", "Home", new { id = 2 });
            }

            // getting the preferencer of user
            var helper = new UserHelper();
            var model = new GetPrefereceModel();
            var Objmodel = new PreferenceModelWeb();

            // It will set all the list
            Objmodel = helper.SetSelectListForAllUnits(Objmodel);

            var profileId = Convert.ToInt32(Session["ProfileId"]);
            model.ProfileId = profileId;
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(profileId);
            // get preference data
            var response = helper.GetPreference(model);

            //  check the response to be success
            if (response.ResponseCode != ((int)Enumerations.GetPreference.Success).ToString()) return View(Objmodel);
            Misc objMisc = new Misc();

            // setting modal fields
            var objPreference = new UpdatePreferenceModel
            {
                DefaultAirportIdentifier = response.DefaultAirportIdentifier,
                DistanceUnit = response.DistanceUnit,
                FuelUnit = response.FuelUnit,
                SpeedUnit = response.SpeedUnit,
                VerticalSpeedUnit = response.VerticalSpeedUnit,
                WeightUnit = response.WeightUnit,
            };

            Objmodel.DefaultAirportIdentifier = objPreference.DefaultAirportIdentifier;
            // It will set all the units id's for all the list.
            Objmodel = helper.SetIdForAllUnits(Objmodel, objPreference);

            // set for dropbox sync 
            ViewBag.IsDropBoxSync = helper.SetValueForDropBox(profileId);

            const int pageSize = 10;
            var totalRows = mobjModel.CountCustomer();

            var userEmailresult = mobjModel.GetCustomerPage(profileId);
            if (!userEmailresult.Any())
            {
                userEmailresult = new List<UserEmail>();
            }

            Objmodel.TotalRows = totalRows;
            Objmodel.PageSize = pageSize;
            Objmodel.userEmail = userEmailresult.Select(s => new UserEmailWeb
            {
                EmailId = s.EmailId,
                IsEnabled = s.IsEnabled,
                ProfileId = s.ProfileId,
                TempId = objMisc.Encrypt(s.id.ToString())
            }).ToList();

            Objmodel.ProfileId = profileId.ToString();

            Objmodel.aircraftList = helper.setViewBagAircraftListing(profileId);

            // set pilot flying summary
            Objmodel = helper.GetFlyingSummary(Objmodel);
            GetLiveFlightByUserId(Convert.ToInt32(Session["ProfileId"]));
            return View("Preferences", Objmodel);
        }

        /// <summary>
        /// Sets the value of pilot flying summary
        /// </summary>
        /// <param name="Objmodel"></param>
        /// <returns></returns>
        //private PreferenceModelWeb GetFlyingSummary(PreferenceModelWeb Objmodel)
        //{
        //    try
        //    {
        //        var helper = new UserHelper();
        //        return helper.GetFlyingSummary(Objmodel);
        //    }
        //    catch (Exception e)
        //    {
        //        ExceptionHandler.ReportError(e);
        //        return Objmodel;
        //    }
        //}

        /// <summary>
        /// Updates user preference in database
        /// </summary>
        /// <param name="preference">Preference model</param>
        /// <returns>if success Preference view otherwise returns it with error</returns>
        [SessionExpireFilterAttribute]
        [HttpPost]
        public ActionResult Preferences(PreferenceModelWeb preference)
        {
            var preferenceEntity = new UpdatePreferenceModel();
            var helper = new UserHelper();

            // set values for preference model

            preferenceEntity.DefaultAirportIdentifier = preference.DefaultAirportIdentifier;
            preferenceEntity = helper.SetValuesForAllIds(preferenceEntity, preference);

            var profileId = Convert.ToInt32(Session["ProfileId"]);
            preferenceEntity.ProfileId = profileId.ToString();

            // update preference in db
            UpdatePreferenceResponseModel response = helper.UpdatePreference(preferenceEntity);

            if (response.ResponseCode != Enumerations.RegistrationReturnCodes.Exception.GetStringValue())
                return RedirectToAction("Preferences");

            ModelState.AddModelError("", GAResource.ExceptionMessage);
            return View();
        }

        [SessionExpireFilterAttribute]
        public ActionResult SaveLastPilotLogInFlyingSummary(PilotFlyingSummary pilotSummary)
        {
            var helper = new UserHelper();

            bool isSaved = helper.UpdateLastPilotLogFlyingSummary(pilotSummary);

            return Json(isSaved);
        }

        /// <summary>
        /// Sync and unsync from dropbox
        /// </summary>
        /// <returns>Message</returns>
        [SessionExpireFilterAttribute]
        public JsonResult DropBoxSync()
        {
            Boolean isSaved;
            isSaved = new UserHelper().DropboxUnSync(Convert.ToInt32(Session["ProfileId"]));
            return Json(isSaved, JsonRequestBehavior.AllowGet);
        }

        OAuthToken globalReqToken;
        System.Timers.Timer timer;
        // counter for stopping timer
        int timerCounter;
        int profileId;
        private void SaveAccessToken(string accessToken)
        {
            //Dropbox version V2 returns only accessToken, noe token and secret is not in use
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            try
            {
                int profileId = Convert.ToInt32(Session["ProfileId"]);

                var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);
                if (preference == default(Preference))
                    return;

                //preference.DropBoxToken = t;
                //preference.DropBoxTokenSecret = s;
                preference.IsDropboxSync = true;
                preference.AccessToken = accessToken;
                preference.LastUpdateDate = DateTime.UtcNow;
                context.SaveChanges();

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, "Dropbox access token timer enterval elapsed , saving access token .");
                //logger.Fatal("Exception Dropbox access token timer enterval elapsed , saving access token .", e);
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }

        }

        void StopDropboxAccessTokenTimer()
        {
            try
            {
                timer.Enabled = false;
                timer.Dispose();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
        }


        public ActionResult DropnetUrl()
        {
            var redirect = DropboxOAuth2Helper.GetAuthorizeUri(
                         OAuthResponseType.Code,
                         ConfigurationReader.DropboxConsumerKey,
                         ConfigurationReader.DropboxCallBackUrl);
            return Redirect(redirect.ToString());

        }

        public async Task<ActionResult> DropnetCallback(string code, string state)
        {
            try
            {
                var response = await DropboxOAuth2Helper.ProcessCodeFlowAsync(
                    code,
                    ConfigurationReader.DropboxConsumerKey,
                    ConfigurationReader.DropboxConsumerSecret,
                    ConfigurationReader.DropboxCallBackUrl);

                string accessToken = response.AccessToken;
                SaveAccessToken(accessToken);
                //using (var dbx = new DropboxClient(accessToken))
                //{
                //    using (FileStream stream = System.IO.File.Open(@"D:\prem-pb.csv", FileMode.Open))
                //    {
                //        var uploadResponse = await dbx.Files.UploadAsync("/Guardian Avionics/prem-pb.csv", WriteMode.Overwrite.Instance, body: stream);
                //    }
                //};
            }
            catch (Exception e)
            {
            }
            return RedirectToAction("Preferences");
        }
        //public ActionResult DropnetCallback(string oauth_token, string uid) //, string not_approved
        //{
        //    //Its a callback from dropbox!

        //    //ExceptionHandler.ReportError(new Exception(Session.Count.ToString()), "Session keys in mvc.");

        //    if (!string.IsNullOrEmpty(oauth_token))
        //    {
        //        _client.UserLogin = Session["DropNetUserLogin"] as DropNet.Models.UserLogin;
        //        var AccessToken = _client.GetAccessToken();

        //        //Session["AccessToken"] = ac
        //        SaveAccessToken(AccessToken.Secret, AccessToken.Token);

        //        //Session["DropNetUserLogin"] = null;
        //    }

        //    return RedirectToAction("Preferences");
        //}


        /// <summary>
        /// checks the input password with password saved in database
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <returns></returns>
        [SessionExpireFilter]
        public ActionResult CheckOldPassword(string oldPassword)
        {
            var helper = new UserHelper();
            return Json(helper.CheckCurrentPassword(oldPassword, Convert.ToInt32(Session["ProfileId"])));
        }

        /// <summary>
        /// Sets new password.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [SessionExpireFilter]
        public ActionResult SetNewPassword(string password, string OldPasword)
        {
            var helper = new UserHelper();
            return Json(helper.SetNewPassword(password, OldPasword, Convert.ToInt32(Session["ProfileId"])));
        }


        #endregion preference


        #region userProfile


        // Medical certificate
        /// <summary>
        /// Get the fields of medical certificate to edit , identified by ID
        /// </summary>
        /// <param name="id">Medical certificate Id</param>
        /// <returns>fields of medical certificate</returns>
        [SessionExpireFilterAttribute]
        public JsonResult EditMedicalCertificate(int? id)
        {
            var helper = new UserHelper();
            var medicalCertificate = helper.GetMedicalCertificateToEditWeb(id);

            return Json(medicalCertificate);
        }

        /// <summary>
        /// Delete medical certificate , identified by ID
        /// </summary>
        /// <param name="id">Medical certificate Id</param>
        /// <returns>List of remaining Medical certificates</returns>
        [SessionExpireFilterAttribute]
        //public ActionResult DeleteMedicalCertificate(int? id)
        public ActionResult DeleteMedicalCertificate(string id)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }

            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home", new { id = 1 });
            }
            var userData = new UserHelper().DeleteMedicalCertificateWeb(Convert.ToInt32(id), Convert.ToInt32(Session["ProfileId"]));
            return PartialView("_MedicalCertificate", userData);
        }

        /// <summary>
        /// Creates and Updates Medical certificate ,  identified by ID
        /// </summary>
        /// <param name="MType">Type</param>
        /// <param name="MVaildUntil">Valid Until</param>
        /// <param name="MReferenceNumber">Reference Number</param>
        /// <param name="MId">Id for updation and Id = 0 for creation</param>
        /// <returns></returns>
        [SessionExpireFilterAttribute]
        //public ActionResult SaveMedicalCertificate(string MType, string MVaildUntil, string MReferenceNumber, int MId, int? ClassId)
        public ActionResult SaveMedicalCertificate(string MType, string MVaildUntil, string MReferenceNumber, string MId, int? ClassId)
        {
            try
            {
                if (MId != "0")
                {
                    MId = new Misc().Decrypt(MId);
                }
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);

            }


            var helper = new UserHelper();

            string errorMsg = (Convert.ToDateTime(MVaildUntil)).Date < DateTime.Now.Date ? "Medical certificate expiration date cannot be less than current date" : "Success";

            if (errorMsg != "Success")
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

            // crete certificate for editing or saving
            var medCertificate = new MedicalCertificate
            {
                Id = Convert.ToInt32(MId),
                LastUpdated = DateTime.UtcNow,
                ReferenceNumber = MReferenceNumber,
                Type = MType,
                //ValidUntil = Misc.GetDateUS(MVaildUntil),
                ValidUntil = Misc.GetDateUS(MVaildUntil), //DateFormateChange
                MedicalCertClassId = ClassId,
            };
            medCertificate.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            // if medical certificate is new to create
            if (Convert.ToInt32(MId) == 0)
            {
                medCertificate.Id = 0;

                medCertificate.UniqueId = DateTime.UtcNow.Ticks;
            }

            // save modified medical certificate in db.
            var response = helper.SaveCertificateWeb(medCertificate);

            return PartialView("_MedicalCertificate", response);
        }
        // Medical certificate



        /// <summary>
        /// Saves user profile image
        /// </summary>
        /// <param name="userData">User details with image</param>
        /// <returns>User profile view</returns>
        [SessionExpireFilterAttribute]
        public ActionResult SaveUserProfile(UserProfileWeb userData)
        //, FormCollection form, string f, string l, string p, string d)
        {
            // update user profile db
            var isSaved = new UserHelper().SaveUserProfileImage(userData.UserProfileImage, Convert.ToInt32(Session["ProfileId"]));
            return RedirectToAction("UserProfile");
        }


        public JsonResult checkEmailExist(string emailId)
        {
            var helper = new UserHelper();
            return Json(helper.checkEmailExist(emailId));
        }

        /// <summary>
        /// Updates User details
        /// </summary>
        /// <param name="f">First Name</param>
        /// <param name="l">Last Name</param>
        /// <param name="p">Phone Number</param>
        /// <param name="d">Date of birth</param>
        /// <returns>Updates User Details</returns>
        [SessionExpireFilterAttribute]
        public ActionResult SaveUserDetails(string firstName, string lastName, string phoneNumber, string dateOfBirth, string streetAddress, string city, string state, string zipCode, string companyName, string emailId)
        {
            var helper = new UserHelper();
            var userData = new UserProfileWeb();
            userData.FirstName = firstName;
            userData.LastName = lastName;
            userData.PhoneNumber = phoneNumber;
            userData.DateOfBirth = dateOfBirth;
            userData.StreetAddress = streetAddress;
            userData.City = city;
            userData.State = state;
            userData.ZipCode = zipCode;
            userData.CompanyName = companyName;
            userData.EmailId = emailId;
            // update user profile db
            var isSaved = helper.SaveUserProfileWeb(userData, Convert.ToInt32(Session["ProfileId"]));

            var user = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
            Session["UserName"] = user.FirstName;
            //return Json(helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"])));
            return Json(user);
        }


        // Ratings 
        /// <summary>
        /// Updates and creates rating on the basis of Id
        /// </summary>
        /// <param name="rId">Rating id , id = 0 then create otherwise update rating</param>
        /// <param name="rType">Type of rating</param>
        /// <param name="rValidUntil">Valid Until date</param>
        /// <returns>Updated Ratings list</returns>
        [SessionExpireFilterAttribute]
        //public ActionResult SaveRatings(int? rId, string rType, string rValidUntil, int profileId, int licId)
        public ActionResult SaveRatings(string rId, string rType, string rValidUntil, int profileId, string licId)
        {
            try
            {
                if (rId != "0")
                {
                    rId = new Misc().Decrypt(rId);
                }
                licId = new Misc().Decrypt(licId);
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }
            string errorMsg = (Convert.ToDateTime(rValidUntil)).Date < DateTime.Now.Date ? "Error" : "Success";

            if (errorMsg == "Error")
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            // create rating for editing or saving
            var saveRatings = new Rating
            {
                Id = Convert.ToInt32(rId),
                LastUpdated = DateTime.UtcNow,
                Type = rType,
                ValidUntil = Misc.GetDateUS(rValidUntil), //DateFormateChange
            };

            // if ratings is new to create
            if (Convert.ToInt32(rId) == 0)
            {
                saveRatings.Id = 0;
                saveRatings.LicenseId = Convert.ToInt32(licId);
                saveRatings.UniqueId = DateTime.UtcNow.Ticks;
            }
            var response = new UserHelper().SaveRatingsWeb(saveRatings, profileId);
            return PartialView("_LicensePartial", response);
        }

        /// <summary>
        /// Gets the fields for ratings to edit
        /// </summary>
        /// <param name="id">Id of rating</param>
        /// <returns>fields for ratings</returns>
        [SessionExpireFilterAttribute]
        public JsonResult EditRatings(int? id)
        {
            var helper = new UserHelper();

            var liceseId = helper.GetLicenseIdFromRatingId(id);
            if (liceseId != 0)
                Session["LicenseIdOfRating"] = liceseId;

            var userData = new UserProfileWeb { ratings = helper.GetRatingsToEditWeb(id) };
            return Json(userData.ratings);
        }

        /// <summary>
        /// Deletes ratings
        /// </summary>
        /// <param name="id">Id of rating to delete</param>
        /// <returns>List of remaining ratings</returns>
        [SessionExpireFilterAttribute]
        public ActionResult DeleteRatings(string id, int profileId)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }
            // delete medical certificate
            var response = new UserHelper().DeleteRatings(Convert.ToInt32(id), profileId);

            return PartialView("_LicensePartial", response);
        }
        // Ratings 



        /// <summary>
        /// Updates or Creates license on the basis of license Id
        /// </summary>
        /// <param name="lType">Type of license</param>
        /// <param name="lValidUntil">Valid until date</param>
        /// <param name="lReferenceNumber">Reference Number of license</param>
        /// <param name="lId">Id of license , if Id = 0 then create license otherwise update</param>
        /// <returns>List of updated license</returns>
        [SessionExpireFilterAttribute]
        //public ActionResult SaveLicenses(string lType, string lValidUntil, string lReferenceNumber, int lId, int profileId)
        public ActionResult SaveLicenses(string lType, string lValidUntil, string lReferenceNumber, string lId, int profileId)
        {
            try
            {
                if (lId != "0")
                {
                    lId = new Misc().Decrypt(lId);
                }
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }
            var helper = new UserHelper();


            string errorMsg = (Convert.ToDateTime(lValidUntil)).Date < DateTime.Now.Date ? "Error" : "Success";

            if (errorMsg == "Error")
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }


            // create license for editing or saving
            var saveLicense = new License
            {
                Id = Convert.ToInt32(lId),
                LastUpdated = DateTime.UtcNow,
                ReferenceNumber = lReferenceNumber,
                Type = lType,
                ValidUntil = Misc.GetDateUS(lValidUntil), //DateFormateChange
                ProfileId = profileId
            };

            // if medical certificate is new to create
            if (Convert.ToInt32(lId) == 0)
            {
                saveLicense.Id = 0;
                //saveLicense.ProfileId = Convert.ToInt32(Session["ProfileId"]);
                saveLicense.UniqueId = DateTime.UtcNow.Ticks;
            }
            // save modified medical certificate in db.
            var responseLicense = helper.SaveLicenseWeb(saveLicense);
            return PartialView("_LicensePartial", responseLicense);
        }

        /// <summary>
        /// Gets the fields of license to edit.
        /// </summary>
        /// <param name="id">License id</param>
        /// <returns>fields of licenses</returns>
        [SessionExpireFilterAttribute]
        public JsonResult EditLicenses(int? id)
        {
            var helper = new UserHelper();

            if (id != null && id != 0)
                Session["LicenseIdOfRating"] = id;

            var licenses = new LicensesModel();
            licenses = helper.GetLicenseToEditWeb(id);

            return Json(licenses);

        }

        /// <summary>
        /// Deletes license
        /// </summary>
        /// <param name="id">Id of license to delede</param>
        /// <returns>list of updated licenses</returns>
        [SessionExpireFilterAttribute]
        // public ActionResult DeleteLicenses(int? id, int profileId)
        public ActionResult DeleteLicenses(string id, int profileId)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json("InvalidId", JsonRequestBehavior.AllowGet);
            }
            // delete medical certificate
            var response = new UserHelper().DeleteLicense(Convert.ToInt32(id), profileId);
            return PartialView("_LicensePartial", response);
        }

        /// <summary>
        /// Gets the list of ratings for a license
        /// </summary>
        /// <param name="id">License Id for which rating are to be shown </param>
        /// <returns>list of rating</returns>
        [SessionExpireFilterAttribute]
        public ActionResult ShowRatings(int? id)
        {
            var helper = new UserHelper();

            if (id != 0 && id != null)
                Session["LicenseIdOfRating"] = id;

            var userData = helper.GetUserProfile(Convert.ToInt32(Session["ProfileId"]));
            userData.RatingsList = helper.GetRatingsList(id);

            return PartialView("_Ratings", userData);
        }


        public int id { get; set; }


        #endregion userProfile


        #region pilotLogs


        ///// <summary>
        ///// Gets flight listing for pilot log
        ///// </summary>
        ///// <param name="pageNo">Page no</param>
        ///// <returns>list of flight</returns>
        //[SessionExpireFilterAttribute]
        //public ActionResult FlightList(int? pageNo, int? pilotLogId)
        //{
        //    List<LogBookModelWeb> model = new UserHelper().GetFlightList(Convert.ToInt32(Session["ProfileId"]));
        //    var pageNumber = (pageNo ?? 1);

        //    // set pilotLogId if its not null
        //    ViewBag.PilotLogId = pilotLogId ?? 0;

        //    ViewBag.flighList = new UserHelper().setIfrAppchsList();

        //    return View("PilotLogs", model.ToPagedList(pageNumber, Constants.PageSizeTable));
        //}

        /// <summary>
        /// Gets the pilot logs for current user
        /// </summary>
        /// <param name="pageNo">Page No</param>
        /// <returns>list of pilot log</returns>
        [SessionExpireFilterAttribute]
        public ActionResult PilotLogs(int? pageNo)
        {
            var helper = new UserHelper();
            int totalCount = 0;
            List<LogBookModelWeb> logBookList = helper.GetPilotLogs(Convert.ToInt32(Session["ProfileId"]), (pageNo == null) ? 1 : (int)pageNo, out totalCount);

            var pageNumber = (pageNo ?? 1);

            return View(logBookList.ToPagedList(pageNumber: pageNumber, pageSize: Constants.PageSizeTable));
        }



        //public ActionResult PilotLogs1(int? pageNo)
        //{
        //    var helper = new UserHelper();
        //    int totalCount;
        //    LogBookModelWeb logBookList. = helper.GetPilotLogs((int)Session["ProfileId"], (pageNo == null) ? 1 : (int)pageNo, out totalCount);

        //    var pageNumber = (pageNo ?? 1);

        //    return View("PilotLogs", logBookList);
        //}



        /// <summary>
        /// Gets the pilot log summary
        /// </summary>
        /// <param name="pageNo">Page Number</param>
        /// <returns>list of pilot log</returns>
        [SessionExpireFilterAttribute]
        //public ActionResult PilotSummary()
        //{

        //    var helper = new UserHelper();
        //    var totalCount = 0;
        //    List<LogBookModelWeb> logBookList = helper.GetPilotLogs(Convert.ToInt32(Session["ProfileId"]), (pageNo == null) ? 1 : (int)pageNo, out totalCount);

        //    ViewBag.NumberOfPages = new UnitDataHelper().JpiDataNoOfPages(totalCount);
        //    ViewBag.CurrentPage = pageNo;
        //    var pageNumber = (pageNo ?? 1);
        //    return PartialView("_logbookSummary", logBookList.ToPagedList(pageNumber: pageNumber, pageSize: Constants.PageSizeTable));
        //}

        /// <summary>
        /// Gets the fields for pilot log identified by pilot log id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [SessionExpireFilterAttribute]
        public ActionResult EditPilotLogs(int id)
        {
            // update logbook

            var helper = new UserHelper();
            var logBook = new LogBookModelWeb();

            double latitude;
            double longitude;
            //string fileName;

            logBook = helper.EditPilotLog(id, out latitude, out longitude); //, out fileName);
            ViewBag.AudioFilePath = logBook.AudioFileName;
            logBook.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return Json(new
            {
                logBook = logBook,
                latitude = latitude,
                longitude = longitude
                //fileName = fileName,
            });
        }



        public ActionResult ViewFlightPathOnGoogleEarth(string flightId)
        {
            var helper = new UnitDataHelper();
            int fId = Convert.ToInt32(flightId);
            string url = helper.GetUrlFlightPathOnGoogleEarth(fId);

            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    FileName = String.Format(GAResource.KMLFileName, fId),
            //    Inline = false,
            //};

            //Response.AppendHeader("Content-Disposition", cd.ToString());


            //return Json(url, Constants.ContextTypeForKml, JsonRequestBehavior.AllowGet);

            return Redirect(url);
        }



        //public JsonResult ViewFlightPathOnGoogleEarth(string flightId)
        //{
        //    var helper = new UnitDataHelper();
        //    int fId = Convert.ToInt32(flightId);
        //    string url = helper.GetUrlFlightPathOnGoogleEarth(fId);

        //    var cd = new System.Net.Mime.ContentDisposition
        //    {
        //        FileName = String.Format(GAResource.KMLFileName, fId),
        //        Inline = false,
        //    };

        //    Response.AppendHeader("Content-Disposition", cd.ToString());


        //    return Json(url, Constants.ContextTypeForKml, JsonRequestBehavior.AllowGet);
        //}



        public ActionResult DownloadKmlFileForFlight(string flightId)
        {
            var helper = new UnitDataHelper();
            int fId = Convert.ToInt32(flightId);
            string url = helper.GetUrlDownloadKmlFileForFlight(fId);


            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = String.Format(GAResource.KMLFileName, fId),
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(url, Constants.ContextTypeForKml);
        }


        public JsonResult DrawMaps(int flightId)
        {


            return Json("");
        }






        #endregion pilotLogs


        #region pilotSummary

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult PilotSummary()
        {
            if (Session["UserType"] == null || Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.UserType = Session["UserType"].ToString();
            Misc objMisc = new Misc();
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(Convert.ToInt32(Session["ProfileId"]));
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            int sno = 1;
            response.OrderByDescending(o => o.AircraftId).ToList().ForEach(f => { f.TId = objMisc.Encrypt(f.AircraftId.ToString()); f.SNO = sno++; });
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(Convert.ToInt32(Session["ProfileId"]));
            if (!isCurrSubscriptionIsPlan5AndActive)
            {
                sno = 1;
                int aircraftId = helper.GetFirstAircraftRegisteredByProfileId(Convert.ToInt32(Session["ProfileId"]));
                response.Where(w => w.AircraftId != aircraftId).ToList().ForEach(f => { f.AircraftId = 0; f.TId = null; });
                response.OrderByDescending(o => o.TId).ToList().ForEach(f => f.SNO = sno++);
            }
            GetLiveFlightByUserId(Convert.ToInt32(Session["ProfileId"]));
            return View("PilotSummary", response);
        }

        /// <summary>
        /// Sets the values for summary for pilot.
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilterAttribute]
        public ActionResult PilotSummaryDetails()
        {
            //PilotSummaryModel model = new PilotSummaryModel();
            //var helper = new UserHelper();

            //if (Session["UserType"] != null)
            //{
            //    //logger.Fatal("User Type is not null");
            //}
            //else
            //{
            //    //logger.Fatal("User Type is null");
            //    return View("Login");
            //}
            //int profileId = Convert.ToInt32(Session["ProfileId"]);
            //model = helper.GetPilotSummary(profileId);

            //return View("PilotSummary", model);
            if (Session["UserType"] == null || Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.UserType = Session["UserType"].ToString();
            Misc objMisc = new Misc();
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(Convert.ToInt32(Session["ProfileId"]));
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            response.ForEach(f => f.TId = objMisc.Encrypt(f.AircraftId.ToString()));
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);

            return View("PilotSummary", response);
        }

        #endregion pilotSummary

        #region FaceBook

        public JsonResult verifyFBLogin(string Email, string FirstName, string LastName, string Id, int ThirdPartyAccountType)
        {
            //logger.Fatal("verifyFBLogin method calls");
            var helper = new UserHelper();

            var objUser = new RegisterAndLoginByThirdParty
            {
                EmailId = Email,
                FirstName = FirstName,
                LastName = LastName,
                SocialLoginAccountId = Id,
                SocialLoginAccountType = ThirdPartyAccountType
            };

            var response = helper.verifyThirdPartyLogin(objUser, true);
            //logger.Fatal("Third Party response =" + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.AdminUser).ToString())
            {
                // Admin logged in

                // save data in session variable.
                Session["AdminName"] = response.FirstName;
                Session["AdminProfileId"] = response.ProfileId;
                Session["UserType"] = response.UserType;

                return Json("Admin");
            }
            else if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
            {
                // save data in session variable.
                Session["UserLoggedIn"] = true;
                Session["UserName"] = response.FirstName;
                Session["ProfileId"] = response.ProfileId;
                Session["UserType"] = response.UserType;
                Session["ImageUrl"] = response.ImageName;

                return Json("User");
            }
            else if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString())
            {
                return Json("UserBlocked");
            }
            else if (response.ResponseCode == ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithGuardian).ToString())
            {
                return Json("AlreadyRegisterWithGuardian");
            }
            else if (response.ResponseCode == ((int)Enumerations.LoginReturnCodes.AlreadyRegisterWithGmailAccount).ToString())
            {
                return Json("AlreadyRegisterWithGmailAccount");
            }
            return Json("User");
        }
        #endregion FaceBook

        [SessionExpireFilterAttribute]
        //public ActionResult UpdateRecord(int id, string emailId, int profileId, bool isEnabled)
        public ActionResult UpdateRecord(string id, string emailId, int profileId, bool isEnabled)
        {
            string result = "";
            int profileIdSession = 0;
            if (Session["ProfileId"] == null)
            {
                return Json(new { msg = "Session Expire" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                profileIdSession = Convert.ToInt32(Session["ProfileId"]);
            }

            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json(new { msg = "Invalid Request" }, JsonRequestBehavior.AllowGet);
            }
            if (profileIdSession != profileId)
            {
                return Json(new { msg = "Invalid Request" }, JsonRequestBehavior.AllowGet);
            }

            //ValidateRequest
            if (new ModelServices().GetCustomerPage(profileId).ToList().FirstOrDefault(f => f.id == Convert.ToInt32(id)) == null)
            {
                return Json(new { msg = "Invalid Request" }, JsonRequestBehavior.AllowGet);
            }


            try
            {
                result = mobjModel.UpdateCustomer(Convert.ToInt32(id), emailId, profileIdSession, isEnabled);
            }
            catch (Exception ex)
            {

            }

            return Json(new { msg = result }, JsonRequestBehavior.AllowGet);

        }


        [SessionExpireFilterAttribute]
        public ActionResult SaveRecord(string emailId, int profileId, bool isEnabled)
        {
            string[] result = new string[2];
            try
            {
                result = mobjModel.SaveCustomer(emailId, profileId, isEnabled);

            }
            catch (Exception ex)
            {
            }

            return Json(new { msg = result[0], pkId = result[1] }, JsonRequestBehavior.AllowGet);


        }

        [SessionExpireFilterAttribute]
        // public JsonResult DeleteRecord(int id)
        public JsonResult DeleteRecord(string id, int profileId)
        {
            bool result = false;
            if (Session["ProfileId"] == null)
            {
                return Json(new { result }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json(new { result }, JsonRequestBehavior.AllowGet);
            }

            //Validate Request
            if (new ModelServices().GetCustomerPage(Convert.ToInt32(Session["ProfileId"])).FirstOrDefault(f => f.id == Convert.ToInt32(id)) == null)
            {
                return Json(new { result }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                result = mobjModel.DeleteCustomer(Convert.ToInt32(id));
            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilterAttribute]
        [HttpGet]
        public JsonResult RestoreAircraft(string aircraftIds, int profileId)
        {
            if (Session["ProfileId"] == null)
            {
                return Json(new { response = "Session Expired" }, JsonRequestBehavior.AllowGet);
            }
            int profileIdSession = Convert.ToInt32(Session["ProfileId"]);
            if (profileId != profileIdSession)
            {
                return Json(new { response = "Invalid Request" }, JsonRequestBehavior.AllowGet);
            }
            string response = string.Empty;
            response = new AircraftHelper().RestoreAircraft(aircraftIds, profileId);
            return Json(new { response }, JsonRequestBehavior.AllowGet);

        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult PilotLogList()
        {
            ViewBag.PageHeaderTitle = "Pilot Logs";
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home", new { id = 1 });
            }

            Session["UserName"] = new UserHelper().GetUserNameByProfileId(Convert.ToInt32(Session["ProfileId"]));
            List<LogBookModelWeb> model = new UserHelper().GetFlightList(Convert.ToInt32(Session["ProfileId"]));
            Misc objMisc = new Misc();
            model.ForEach(f => f.TId = objMisc.Encrypt(f.Id.ToString()));

            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            ViewBag.flighList = new UserHelper().setIfrAppchsList();

            var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(Convert.ToInt32(Session["ProfileId"]));
            if (!isCurrSubscriptionIsPlan5AndActive)
            {
                int aircraftId = new AircraftHelper().GetFirstAircraftRegisteredByProfileId(Convert.ToInt32(Session["ProfileId"]));
                model.Where(w => w.AircraftId != aircraftId).ToList().ForEach(f => { f.AircraftId = 0; f.TId = null; });
            }
            GetLiveFlightByUserId(Convert.ToInt32(Session["ProfileId"]));
            return View("PilotLogList", model);
        }

        [SessionExpireFilterAttribute]
        public ActionResult PilotLogDetail(string tid, int? flag)
        {
            if (Session["UserType"].ToString() == Enumerations.UserType.DemoUser.ToString())
            {
                LogBookModelWeb logBookModelWeb = new LogBookModelWeb
                {
                    Actual = "00.0",
                    AudioFileName = "",
                    AircraftRegistration = "Demo",
                    CrossCountry = "00.0",
                    Date = System.DateTime.UtcNow.ToString("MM/dd/yyyy"),
                    Time = "13:21:00",
                    DayPIC = "00.0",
                    Hood = "00.0",
                    NightPIC = "00.0",
                    Sim = "00.0"
                };
                ViewBag.AudioFilePath = "";
                return View("PilotLogDetail", logBookModelWeb);
            }

            ViewBag.PageHeaderTitle = "Pilot Log Details";
            if (flag == null)
            {
                ViewBag.message = "";
            }
            else
            {
                ViewBag.message = GA.Common.ResourcrFiles.Common.Messages.UpdateRecord.ToString();
            }

            var helper = new UserHelper();
            var logBook = new LogBookModelWeb();

            double latitude;
            double longitude;
            //string fileName;

            try
            {
                tid = new Misc().Decrypt(tid);
            }
            catch (Exception exx)
            {
                return RedirectToAction("Login", "Home");
            }

            //Validate pilotlog
            List<LogBookModelWeb> objlogbooklist = new UserHelper().GetFlightList(Convert.ToInt32(Session["ProfileId"]));
            var objLogBook = objlogbooklist.FirstOrDefault(f => f.Id == Convert.ToInt32(tid));
            if (objLogBook == null)
            {
                //Invalid Request
                return RedirectToAction("Login", "Home");
            }
            logBook = helper.EditPilotLog(Convert.ToInt32(tid), out latitude, out longitude); //, out fileName);
            ViewBag.AudioFilePath = logBook.AudioFileName;
            //ViewBag.flighList = new UserHelper().setIfrAppchsList();
            logBook.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("PilotLogDetail", logBook);
        }


        /// <summary>
        /// Updates pilot log
        /// </summary>
        /// <param name="lb">Pilot log book Id</param>
        /// <param name="pageNo">current page no. of pilot log book</param>
        /// <returns>Updated list of pilot log , on given page no.</returns>
        [SessionExpireFilterAttribute]
        [HttpPost]
        public ActionResult PilotLogDetail(LogBookModelWeb lb)
        {
            // ModelState.SetModelValue("Id", new ValueProviderResult(Convert.ToInt32(new Misc().Decrypt(ModelState["Id"].Value.AttemptedValue)) , "", CultureInfo.InvariantCulture));
            if (!ModelState.IsValid)
            {
                ViewBag.AudioFilePath = lb.AudioFileName;
                ViewBag.flighList = new UserHelper().setIfrAppchsList();
                lb.ProfileId = Convert.ToInt32(Session["ProfileId"]);
                return View("PilotLogDetail", lb);
            }
            var helper = new UserHelper();
            GeneralResponse response = helper.UpdateLogBook(lb);

            if (response.ResponseMessage != Enumerations.Logbook.Success.GetStringValue())
            {

                if (response.ResponseMessage == Enumerations.Logbook.Exception.GetStringValue())
                {
                    ModelState.AddModelError("", GA.Common.ResourcrFiles.Common.Messages.ErrorWhileUpdateRecord.ToString());
                }

                if (response.ResponseMessage == Enumerations.Logbook.BothPilotAndCoPilotEmailIdNotFound.GetStringValue())
                {
                    ModelState.AddModelError("PilotEmailId", "Pilot " + GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString());
                    ModelState.AddModelError("CoPilotEmailId", "CoPilot " + GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString());
                }

                if (response.ResponseMessage == Enumerations.Logbook.PilotEmailIdNotFound.GetStringValue())
                {
                    ModelState.AddModelError("PilotEmailId", "Pilot " + GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString());
                }

                if (response.ResponseMessage == Enumerations.Logbook.CoPilotEmailIdNotFound.GetStringValue())
                {
                    ModelState.AddModelError("CoPilotEmailId", "CoPilot " + GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString());
                }

                ViewBag.AudioFilePath = lb.AudioFileName;
                ViewBag.flighList = new UserHelper().setIfrAppchsList();
                lb.ProfileId = Convert.ToInt32(Session["ProfileId"]);
                return View("PilotLogDetail", lb);

            }

            return RedirectToAction("PilotLogDetail", new { tid = new Misc().Encrypt(lb.Id.ToString()), flag = 1 });
        }
        public ActionResult FlightDataLogMobile(int? logId, int? profileId)
        {
            ViewBag.CallFrom = "FlightList";


            if (logId == null || profileId == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.PageHeaderTitle = "Airframe Datalog";
            ViewBag.IsPageLoad = "true";
            ViewBag.profileId = (profileId);
            //ViewBag.PilotLogId = Convert.ToInt32(pid);
            ViewBag.PilotLogId = logId;
            return View("FlightDataLogMobile");
        }

        [SessionExpireFilterAttribute]
        //public ActionResult FlightDataLog(string pid, int? flag, int? aircraftId)
        public ActionResult FlightDataLog(string pid, int? flag, string aId)
        {
            ViewBag.TotalCylinder = 0;
            if (flag == null)
            {
                ViewBag.CallFrom = "PilotLog";
            }
            else
            {
                ViewBag.CallFrom = "FlightList";
                ViewBag.AircraftId = aId;
            }
            int logId = 0;
            try
            {
                logId = Convert.ToInt32(new Misc().Decrypt(pid));
                if (!string.IsNullOrEmpty(aId))
                {
                    var AircraftId = new Misc().Decrypt(aId);
                    string xmlfile = ConfigurationReader.EngineConfigXMLFilePath + @"\" + AircraftId.ToString() + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(xmlfile);
                    var CylinderCount = xmldoc.SelectNodes("plist/dict/integer");
                    int getCylinderCount = Convert.ToInt32(CylinderCount[0].InnerText);
                    ViewBag.TotalCylinder = getCylinderCount;
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            //Validate pilotlog
            //List<LogBookModelWeb> objlogbooklist = new UserHelper().GetFlightList(Convert.ToInt32(Session["ProfileId"]));
            //var objLogBook = objlogbookli//st.FirstOrDefault(f => f.Id == Convert.ToInt32(logId));
            //if (objLogBook == null)
            //{
            //    //Invalid Request
            //    return RedirectToAction("Login", "Home");
            //}

            ViewBag.PageHeaderTitle = "Airframe Datalog";
            ViewBag.IsPageLoad = "true";
            ViewBag.profileId = Convert.ToInt32(Session["ProfileId"]);
            //ViewBag.PilotLogId = Convert.ToInt32(pid);
            ViewBag.PilotLogId = pid;
            return View("FlightDataLog");
        }

        [SessionExpireFilterAttribute]
        public ActionResult FlightDataLogLive(string pid, int? flag, int? aircraftId, long? uniqueId)
        {
            if (flag == null || flag == -1)
            {
                ViewBag.CallFrom = "PilotLog";
            }
            else
            {
                ViewBag.CallFrom = "FlightList";
            }
            try
            {
                pid = new Misc().Decrypt(pid);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            List<LogBookModelWeb> objlogbooklist = new UserHelper().GetFlightList(Convert.ToInt32(Session["ProfileId"]));
            var objLogBook = objlogbooklist.FirstOrDefault(f => f.Id == Convert.ToInt32(pid));
            try
            {
                if (aircraftId != null)
                {
                    string xmlfile = ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(xmlfile);
                    var CylinderCount = xmldoc.SelectNodes("plist/dict/integer");
                    int getCylinderCount = Convert.ToInt32(CylinderCount[0].InnerText);
                    ViewBag.TotalCylinder = getCylinderCount;
                }
            }
            catch (Exception ex)
            {
                //return RedirectToAction("Login", "Home");
            }
            if (objLogBook == null)
            {
                //Invalid Request
                return RedirectToAction("Login", "Home");
            }

            ViewBag.AircraftId = aircraftId;
            ViewBag.uniqueId = uniqueId;
            ViewBag.PageHeaderTitle = "Airframe Datalog";
            ViewBag.IsPageLoad = "true";
            ViewBag.profileId = Convert.ToInt32(Session["ProfileId"]);
            ViewBag.PilotLogId = Convert.ToInt32(pid);
            return View("FlightDataLogLive");
        }





        public ActionResult FlightDataLogTemp(int pilotLogId, int? flag, int? aircraftId)
        {
            if (flag == null)
            {
                ViewBag.CallFrom = "PilotLog";
            }
            else
            {
                ViewBag.CallFrom = "FlightList";
                ViewBag.AircraftId = aircraftId;
            }
            ViewBag.PageHeaderTitle = "Datalog";
            ViewBag.IsPageLoad = "true";
            ViewBag.profileId = Convert.ToInt32(Session["ProfileId"]);
            ViewBag.PilotLogId = pilotLogId;
            return View("FlightDataLogTemp");
        }


        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [SessionExpireFilterAttribute]
        public ActionResult AircraftList()
        {
            ViewBag.PageHeaderTitle = "Aircraft List";
            UnitDataHelper objHelper = new UnitDataHelper();
            int profileId = 0;
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home", new { id = 1 });
            }
            else
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            Misc objMisc = new Misc();
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(profileId);
            List<DataLogListing> model = objHelper.GetAircraftList(profileId);
            model.ForEach(f => f.TId = objMisc.Encrypt(f.pilotLogId.ToString()));
            ViewBag.ProfileId = profileId;
            return View("AircraftList", model);
        }


        public ActionResult AircraftListMobile(int profileId)
        {
            ViewBag.PageHeaderTitle = "Aircraft List";
            UnitDataHelper objHelper = new UnitDataHelper();


            List<DataLogListing> model = objHelper.GetAircraftList(profileId);
            ViewBag.ProfileId = profileId;
            return View("AircraftListMobile", model);
        }


        //public ActionResult FlightList(int aircraftId)
        public ActionResult FlightList(string aId)
        {
            ViewBag.PageHeaderTitle = "Flight List";
            UnitDataHelper objHelper = new UnitDataHelper();
            string aircraftRegNo = string.Empty;

            int profileId = 0;
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home", new { id = 1 });
            }
            else
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            string aircraftId = "";
            try
            {
                aircraftId = new Misc().Decrypt(aId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            //Validate request
            List<DataLogListing> objAircraftList = objHelper.GetAircraftList(profileId);
            var objDatalog = objAircraftList.FirstOrDefault(f => f.pilotLogId == Convert.ToInt32(aircraftId));
            if (objDatalog == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Misc objMisc = new Misc();

            List<DataLogListing> model = objHelper.GetFlightListByAircraftId(profileId, Convert.ToInt32(aircraftId), out aircraftRegNo);
            model.ForEach(f => f.TId = objMisc.Encrypt(f.pilotLogId.ToString()));
            ViewBag.ProfileId = profileId;
            ViewBag.AircraftId = aId;
            ViewBag.aircraftRegNo = aircraftRegNo;
            return View("FlightList", model);

        }


        public ActionResult FlightListMobile(int aId, int profileId)
        {
            ViewBag.PageHeaderTitle = "Flight List";
            UnitDataHelper objHelper = new UnitDataHelper();
            string aircraftRegNo = string.Empty;
            string aircraftId = aId.ToString();
            //Validate request
            List<DataLogListing> objAircraftList = objHelper.GetAircraftList(profileId);
            var objDatalog = objAircraftList.FirstOrDefault(f => f.pilotLogId == Convert.ToInt32(aircraftId));
            if (objDatalog == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Misc objMisc = new Misc();

            List<DataLogListing> model = objHelper.GetFlightListByAircraftId(profileId, Convert.ToInt32(aircraftId), out aircraftRegNo);
            model.ForEach(f => f.TId = objMisc.Encrypt(f.pilotLogId.ToString()));
            ViewBag.ProfileId = profileId;
            ViewBag.AircraftId = aId;
            ViewBag.aircraftRegNo = aircraftRegNo;
            return View("FlightListMobile", model);

        }

        [SessionExpireFilterAttribute]
        public ActionResult SaveDocument()
        {
            if (Session["ProfileId"] == null)
            {
                return Json("Session Expire");
            }

            DocumentResponseModelWeb response = new DocumentResponseModelWeb();
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            var context = new GuardianAvionicsEntities();
            string title = "";
            string uniqueName = "";
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var docfile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                var docId = Convert.ToInt16(System.Web.HttpContext.Current.Request.Params["docId"]);
                bool isduplicate = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Params["isDuplicate"]);
                title = System.Web.HttpContext.Current.Request.Params["Title"].ToString();
                uniqueName = System.Web.HttpContext.Current.Request.Params["UniqueName"].ToString();
                response = new UnitDataHelper().CreateNewDocument(docfile, profileId, isduplicate, title, uniqueName, docId);
                if (response == null)
                {
                    return PartialView("_DocumentPartial", new List<DocumentModel>());
                }
                else if (response.ListOfDocuments == null)
                {
                    return PartialView("_DocumentPartial", new List<DocumentModel>());
                }

                Misc objMisc = new Misc();
                response.ListOfDocuments.ForEach(s => { s.TId = objMisc.Encrypt(s.Id.ToString()); s.Id = 0; s.LastUpdateDate = Misc.dateToMMDDYYYY(s.LastUpdateDate.Split(' ')[0]); });
            }
            return PartialView("_DocumentPartial", response.ListOfDocuments);
        }

        [SessionExpireFilterAttribute]
        //public ActionResult DeleteDocument(int documentId)
        public ActionResult DeleteDocument(string documentId)
        {
            if (Session["ProfileId"] == null)
            {
                return Json("Session Expire");
            }

            try
            {
                documentId = new Misc().Decrypt(documentId);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request");
            }

            DocumentResponseModelWeb response = new DocumentResponseModelWeb();
            int profileId = Convert.ToInt32(Session["ProfileId"]);

            //Validate request
            response = new UnitDataHelper().GetDocumentForUser(new DocumnetRequestModel { ProfileId = profileId });
            if (!response.ListOfDocuments.Any(f => f.Id == Convert.ToInt32(documentId)))
            {
                return Json("Invalid Request");
            }


            response = new UnitDataHelper().DeleteDocumentById(Convert.ToInt32(documentId), profileId);
            if (response == null)
            {
                return PartialView("_DocumentPartial", new List<DocumentModel>());
            }
            else if (response.ListOfDocuments == null)
            {
                return PartialView("_DocumentPartial", new List<DocumentModel>());
            }
            Misc objMisc = new Misc();
            response.ListOfDocuments.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; f.LastUpdateDate = Misc.dateToMMDDYYYY(f.LastUpdateDate.Split(' ')[0]); });
            return PartialView("_DocumentPartial", response.ListOfDocuments);
        }

        [SessionExpireFilterAttribute]
        public ActionResult GetUserEmailList(int profileId)
        {
            if (Session["ProfileId"] == null)
            {
                return Json("Session Expire");
            }
            int profileIdSession = Convert.ToInt32(Session["ProfileId"]);
            if (profileIdSession != profileId)
            {
                return Json("Invalid Request");
            }
            Misc objMisc = new Misc();
            var userlist = new UserHelper().GetUserEmailList(profileId).Select(s => new UserEmailWeb
            {
                EmailId = s.EmailId,
                IsEnabled = s.IsEnabled,
                ProfileId = s.ProfileId,
                TempId = objMisc.Encrypt(s.id.ToString())
            }).ToList();
            return PartialView("_UserEmailListPreference", userlist);
        }

        [SessionExpireFilterAttribute]
        public ActionResult GetAircraftListToRestore(int profileId)
        {
            return PartialView("_RestoreAircraftListPartial", new UserHelper().setViewBagAircraftListing(profileId));
        }



        public ActionResult TestTable(int pilotLogId, int? flag, int? aircraftId)
        {
            if (flag == null)
            {
                ViewBag.CallFrom = "PilotLog";
            }
            else
            {
                ViewBag.CallFrom = "FlightList";
                ViewBag.AircraftId = aircraftId;
            }
            ViewBag.PageHeaderTitle = "Datalog";
            ViewBag.IsPageLoad = "true";
            ViewBag.profileId = Convert.ToInt32(Session["ProfileId"]);
            ViewBag.PilotLogId = pilotLogId;
            return View("TestTable");
        }

        public ActionResult SquawkList()
        {
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login");
            }
            int profileId = Convert.ToInt32(Session["ProfileId"]);


            //return View("SquawkList", new AircraftHelper().GetSquawkList(profileId).SquawkList);
            return View("SquawkList", new AircraftHelper().GetSquawkListTemp(profileId).SquawkListModel);
        }

        public ActionResult CloseIssue(int issueId, string comment, int profileId)
        {
            profileId = Convert.ToInt32(Session["ProfileId"]);
            return PartialView("_SquawkPartial", new AircraftHelper().CloseIssue(profileId, issueId, comment).SquawkListModel);
        }


        public ActionResult TempMap()
        {
            return View();
        }
        #region paypal
        public void IPN()
        {
            ExceptionHandler.ReportError(new Exception(), "APN Method call");

            //logger.Fatal("IPN Method Call " + Environment.NewLine);
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");
            try
            {
                GetPayPalResponse(formVals, Convert.ToBoolean(ConfigurationReader.IsPaypalSandbox));
            }
            catch (Exception ex)
            {
                //logger.Fatal("exception in IPN = " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
            }
        }

        public void GetPayPalResponse(Dictionary<string, string> formVals, bool useSandbox)
        {
            //logger.Fatal("GetPayPalResponse Method Call " + Environment.NewLine);
            string paypalUrl = useSandbox ? "https://www.sandbox.paypal.com/cgi-bin/webscr"
                : "https://www.paypal.com/cgi-bin/webscr";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(paypalUrl);

            // Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);
            //logger.Fatal("strRequest = " + strRequest);
            StringBuilder sb = new StringBuilder();
            sb.Append(strRequest);

            foreach (string key in formVals.Keys)
            {
                sb.AppendFormat("&{0}={1}", key, formVals[key]);
            }

            strRequest += sb.ToString();
            req.ContentLength = strRequest.Length;

            //for proxy
            //WebProxy proxy = new WebProxy(new Uri("http://urlort#");
            //req.Proxy = proxy;
            //Send the request to PayPal and get the response
            string response = "";
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII))
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                streamOut.Write(strRequest);
                streamOut.Close();
                using (StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    response = streamIn.ReadToEnd();
                    ExceptionHandler.ReportError(new Exception(), "IPN Response");
                    ExceptionHandler.ReportError(new Exception(), response);
                }
            }
            //logger.Fatal("Paypal IPN response status = " + response + Environment.NewLine);
            //logger.Fatal("Paypal IPN response status 2 = " + response + Environment.NewLine);
            if (response == "VERIFIED")
            {
                ExceptionHandler.ReportError(new Exception(), "IPN Response Verified");
                //logger.Fatal("In VERIFIED = " + response + Environment.NewLine);
                PaypalResponse objPaypalResponse = new PaypalResponse();
                try
                {
                    //Process response
                    string[] strArray = strRequest.Split('&');
                    string key, value;

                    for (int i = 0; i < strArray.Length - 1; i++)
                    {
                        if (string.IsNullOrEmpty(strArray[i]))
                        {
                            continue;
                        }
                        string[] strArrayTemp = strArray[i].Split('=');
                        if (strArrayTemp.Length != 2)
                        {
                            continue;
                        }
                        key = strArrayTemp[0];
                        value = HttpUtility.UrlDecode(strArrayTemp[1]);
                        switch (key)
                        {
                            case "mc_gross":
                                objPaypalResponse.GrossTotal = double.Parse(value);
                                break;
                            //case "invoice":
                            //    objPaypalResponse.InvoiceNumber = int.Parse(value);
                            //    break;
                            case "payment_status":
                                objPaypalResponse.PaymentStatus = value;
                                break;
                            case "first_name":
                                objPaypalResponse.PayerFirstName = value;
                                break;
                            case "last_name":
                                objPaypalResponse.PayerLastName = value;
                                break;
                            case "mc_fee":
                                objPaypalResponse.PaymentFee = double.Parse(value);
                                break;
                            case "business":
                                objPaypalResponse.BusinessEmail = value;
                                break;
                            case "payer_email":
                                objPaypalResponse.PayerEmail = value;
                                break;
                            case "receiver_email":
                                objPaypalResponse.RecieverEmail = value;
                                break;
                            case "item_name":
                                objPaypalResponse.ItemName = value;
                                break;
                            case "mc_currency":
                                objPaypalResponse.Currency = value;
                                break;
                            case "txn_id":
                                objPaypalResponse.TransactionId = value;
                                break;
                            case "subscr_id":
                                objPaypalResponse.SubProfileId = value;
                                break;
                            case "payment_date":
                                objPaypalResponse.PaymentDate = value;
                                break;
                            case "address_country":
                                objPaypalResponse.PayerAddressCountry = value;
                                break;
                            case "address_state":
                                objPaypalResponse.PayerAddressState = value;
                                break;
                            case "address_city":
                                objPaypalResponse.PayerAddressCity = value;
                                break;
                            case "address_street":
                                objPaypalResponse.PayerAddressStreet = value;
                                break;
                            case "address_zip":
                                objPaypalResponse.PayerAddressZip = value;
                                break;
                            case "txn_type":
                                objPaypalResponse.TransactionType = value;
                                break;
                            case "payment_type":
                                objPaypalResponse.PaymentType = value;
                                break;
                            case "item_number":
                                objPaypalResponse.SubscriptionItemId = Convert.ToInt32(value);
                                break;
                            case "receipt_id":
                                objPaypalResponse.ReceiptId = value;
                                break;
                            case "payer_id":
                                objPaypalResponse.PayerId = value;
                                break;
                            case "receiver_id":
                                objPaypalResponse.ReceiverId = value;
                                break;
                            case "recurring_payment_id":
                                {
                                    objPaypalResponse.SubProfileId = value;
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception while process the response into object for request = " + strRequest + Environment.NewLine);
                }
                ExceptionHandler.ReportError(new Exception(), "IPN Response object ");
                ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(objPaypalResponse));
                if (!string.IsNullOrEmpty(objPaypalResponse.SubProfileId))
                {
                    //logger.Fatal("sub profile id is not null  " + Environment.NewLine);
                    try
                    {
                        if (objPaypalResponse.TransactionType != "subscr_signup")  // !subscr_signup because we already send mail on sub purchase
                        {
                            PaypalPaymentHelper objPayPalHelper = new PaypalPaymentHelper();
                            Task.Factory.StartNew(() => { objPayPalHelper.SendEmail(objPaypalResponse); });
                        }
                    }
                    catch (Exception ex)
                    {
                        //logger.Fatal("Exception while Sending Email for Request = " + strRequest + Environment.NewLine);
                    }
                    new PaypalPaymentHelper().SetTransaction(objPaypalResponse);
                }
                else
                {
                    //logger.Fatal("sub profile id is NULL" + Environment.NewLine);
                }

            }
            return;
        }

        public ActionResult Subscription()
        {
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.PageHeaderTitle = "Subscription Plans";
            if (Session["UserType"] != null)
            {
                if (Session["UserType"].ToString() == "Admin")
                    ViewBag.IsAdmin = true;
                else
                    ViewBag.IsAdmin = false;

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            var subTableModel = new UserHelper().GetSubscriptionWeb(Convert.ToInt32(Session["ProfileId"]));
            return View(subTableModel);
        }


        public ActionResult PayPalCallBackURL()
        {
            //logger.Fatal("PayPalCallBackURL Method Call" + Environment.NewLine);
            //StringBuilder strBuilder = new StringBuilder();
            //strBuilder.Append("Paypal query string parameters =  " + Environment.NewLine);
            //foreach (String key in Request.QueryString.AllKeys)
            //{
            //    strBuilder.Append("Key: " + key + " Value: " + Request.QueryString[key] + Environment.NewLine);
            //}


            string tx = Request.QueryString["tx"];
            string res = GetPayPalResponse(tx);
            ExceptionHandler.ReportError(new Exception(), "PayPalCallBackURL");
            ExceptionHandler.ReportError(new Exception(), res);
            //logger.Fatal("PayPalCallBackURL Response data = " + res + Environment.NewLine);
            TempData["RespPaypal"] = res;
            return RedirectToAction("PayPalReceipt");
        }


        public string GetPayPalResponse(string tx)
        {

            try
            {
                string authToken = ConfigurationReader.Token;
                string txToken = tx;
                string query = string.Format("cmd=_notify-synch&tx={0}&at={1}", txToken, authToken);
                string url = WebConfigurationManager.AppSettings["urlSubmitPayment"];
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = query.Length;
                StreamWriter outStreamWriter = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
                outStreamWriter.Write(query);
                outStreamWriter.Close();
                StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream());
                string res = reader.ReadToEnd();

                reader.Close();
                return res;
            }
            catch (Exception ex)
            {
                return "";
                //logger.Fatal("Exception while GetPayPalResponsemethod - " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
            }

        }

        public void RefundSubAmountFromPaypal(int userProfileId)
        {
            bool isEligibleForRefund = false;
            string frequency = "";
            string subProfileId = "";
            string transactionId = "";
            DateTime purchaseDate = DateTime.UtcNow;
            decimal subAmount = 0;
            decimal subDays = 0;
            decimal subRemainingDays = 0;
            decimal refundAmount = 0;

            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == userProfileId);
            if (profile != null)
            {
                if (profile.CurrentSubscriptionId != null)
                {
                    var tempMapSub = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                    if (tempMapSub.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal && tempMapSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Active)
                    {
                        subProfileId = tempMapSub.PaypalSubscriptionProfileId;

                        //user's current subscription is active and purchased by paypal
                        //Now we need to cancel the subscription and refund to the customer

                        subAmount = tempMapSub.SubscriptionPaymentDefinition.Amount;
                        frequency = tempMapSub.SubscriptionPaymentDefinition.RegularFrequency;

                        purchaseDate = tempMapSub.InappPurchaseDate; //this column is common for both paypal purchase date and inapppurchasedate
                        var lastReceipt = context.SubscriptionTransactions.OrderBy(o => o.PaymentDate).FirstOrDefault(f => f.MapSubAndUserId == tempMapSub.Id);
                        if (lastReceipt != null)
                        {
                            transactionId = lastReceipt.TransactionId;
                            var daysCountAfterRenewal = (decimal)(DateTime.UtcNow - (lastReceipt.PaymentDate ?? DateTime.UtcNow)).TotalDays;
                            subDays = (frequency == "Monthly") ? 30 : 365;
                            if (daysCountAfterRenewal < subDays)
                            {
                                //eligible for refund
                                isEligibleForRefund = true;
                                decimal totalDays = (decimal)(DateTime.UtcNow - purchaseDate).TotalDays;

                                subRemainingDays = (frequency == "Monthly") ? (30 - totalDays) : (365 - totalDays);
                                subDays = (frequency == "Monthly") ? 30 : 365;
                                refundAmount = (subAmount / subDays) * subRemainingDays;
                                new PaypalAgreement().RefundPAypal(subProfileId, transactionId, Convert.ToString(refundAmount));
                            }
                        }
                    }
                }
            }
        }


        public ActionResult PayPalReceipt()
        {
            //First cancel the current subscription plan
            var userSubscription = new UserHelper().GetSubscriptionByUser(Convert.ToInt32(Session["ProfileId"]));
            if (!string.IsNullOrEmpty(userSubscription.SubProdileId))
            {
                if (userSubscription.PurchasedFrom == Enumerations.SubscriptionSourceEnum.Free.ToString())
                {
                    new UserHelper().MarkUserSubscriptionAsCancel(userSubscription.UserSubMapID);
                }
                else
                {
                    RefundSubAmountFromPaypal(Convert.ToInt32(Session["ProfileId"]));
                    new UserHelper().MarkUserSubscriptionAsCancel(userSubscription.UserSubMapID);
                    new PaypalAgreement().CancelAgreement(userSubscription.SubProdileId, "Purchase New Subscription");
                }
            }

            //Also set the status as cancelled for inapp purchase
            ViewBag.AircraftProfile = "";
            ViewBag.PageHeaderTitle = "Paypal Receipt";
            string key, value;
            string data = TempData["RespPaypal"].ToString();
            PaypalResponse objPaypalResponse = new PaypalResponse();
            try
            {
                // string[] strArray = data.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                string[] stringSeparators = new string[] { "\n" };
                string[] strArray = data.Split(stringSeparators, StringSplitOptions.None);


                //logger.Fatal("strArray count = " + strArray.Length);

                objPaypalResponse.ProfileId = Convert.ToInt32(Session["ProfileId"]);

                for (int i = 0; i < strArray.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(strArray[i]))
                    {
                        continue;
                    }
                    string[] strArrayTemp = strArray[i].Split('=');
                    if (strArrayTemp.Length != 2)
                    {
                        continue;
                    }
                    key = strArrayTemp[0];
                    value = HttpUtility.UrlDecode(strArrayTemp[1]);
                    switch (key)
                    {
                        case "mc_gross":
                            objPaypalResponse.GrossTotal = double.Parse(value);
                            break;
                        //case "invoice":
                        //    objPaypalResponse.InvoiceNumber = int.Parse(value);
                        //    break;
                        case "payment_status":
                            objPaypalResponse.PaymentStatus = value;
                            break;
                        case "first_name":
                            objPaypalResponse.PayerFirstName = value;
                            break;
                        case "last_name":
                            objPaypalResponse.PayerLastName = value;
                            break;
                        case "mc_fee":
                            objPaypalResponse.PaymentFee = double.Parse(value);
                            break;
                        case "business":
                            objPaypalResponse.BusinessEmail = value;
                            break;
                        case "payer_email":
                            objPaypalResponse.PayerEmail = value;
                            break;
                        case "receiver_email":
                            objPaypalResponse.RecieverEmail = value;
                            break;
                        case "item_name":
                            objPaypalResponse.ItemName = value;
                            break;
                        case "mc_currency":
                            objPaypalResponse.Currency = value;
                            break;
                        case "txn_id":
                            objPaypalResponse.TransactionId = value;
                            break;
                        case "subscr_id":
                            objPaypalResponse.SubProfileId = value;
                            break;
                        case "payment_date":
                            objPaypalResponse.PaymentDate = value;
                            break;
                        case "address_country":
                            objPaypalResponse.PayerAddressCountry = value;
                            break;
                        case "address_state":
                            objPaypalResponse.PayerAddressState = value;
                            break;
                        case "address_city":
                            objPaypalResponse.PayerAddressCity = value;
                            break;
                        case "address_street":
                            objPaypalResponse.PayerAddressStreet = value;
                            break;
                        case "address_zip":
                            objPaypalResponse.PayerAddressZip = value;
                            break;
                        case "txn_type":
                            objPaypalResponse.TransactionType = "subscr_signup";
                            break;
                        case "payment_type":
                            objPaypalResponse.PaymentType = value;
                            break;
                        case "item_number":
                            objPaypalResponse.SubscriptionItemId = Convert.ToInt32(value);
                            break;
                        case "receipt_id":
                            objPaypalResponse.ReceiptId = value;
                            break;
                        case "payer_id":
                            objPaypalResponse.PayerId = value;
                            break;
                        case "receiver_id":
                            objPaypalResponse.ReceiverId = value;
                            break;
                    }
                }
                //logger.Fatal("Call for send email");
                ExceptionHandler.ReportError(new Exception(), "A=   objPaypalResponse = " + Newtonsoft.Json.JsonConvert.SerializeObject(objPaypalResponse));
                PaypalPaymentHelper objHelper = new PaypalPaymentHelper();
                Task.Factory.StartNew(() => { objHelper.SendEmail(objPaypalResponse); });
                ExceptionHandler.ReportError(new Exception(), "B= Email Method Call");
                string response = objHelper.SetSubscriptionPurchase(objPaypalResponse);

                //logger.Fatal("Receipt  = " + Newtonsoft.Json.JsonConvert.SerializeObject(objPaypalResponse));
                Session["UserAuthorization"] = new UserHelper().SetFeatureByUser(objPaypalResponse.ProfileId);
            }
            catch (Exception ex)
            {
                //logger.Fatal("Exception while process response - " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));

            }
            return View(objPaypalResponse);
        }

        public ActionResult GetTransactionDetails(string id)
        {
            return Json(new { response = new PaypalPaymentHelper().GetTransactionDetails(id) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInappTransactionDetails(string id)
        {
            var result = new PaypalPaymentHelper().GetInAppTransactionDetails(id);
            return Json(new
            {
                response = new
                {
                    Amount = string.Format("{0:C}", result.Amount),
                    ExpireDate = result.ExpireDate.ToString("g"),
                    ProductId = result.ProductId,
                    PurchaseDate = result.PurchaseDate.ToString("g"),
                    TransactionID = result.TransactionID
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Test()
        {
            return View();
        }

        public bool SetAutoRenewOff(string subProfileId)
        {
            var response = new PaypalAgreement().SuspendProfile(subProfileId);
            if (response)
            {
                new PaypalPaymentHelper().SuspendPaypalProfile(subProfileId);
                return true;
            }
            return false;
        }

        public bool ReActivateProfile(string subProfileId)
        {
            PayPalSDK.PaypalAgreement obj = new PaypalAgreement();
            var response = obj.ReActivateAgreement(subProfileId, "Reactivate");
            if (response == true)
            {
                new PaypalPaymentHelper().ReactivatePaypalProfile(subProfileId);
                return true;
            }
            return false;
        }
        #endregion paypal
        public ActionResult FlightDatalogDemo()
        {
            ViewBag.PageHeaderTitle = "Airframe Datalog";
            ViewBag.IsPageLoad = "true";
            return View();
        }

        public void GetLiveFlightByUserId(int profileId)
        {
            UserHelper helper = new UserHelper();
            var liveData = helper.GetLiveFlightByUserId(profileId);
            Session["LiveFlightData"] = liveData;
        }


        #region Manual Methods
        public void CreateKMLFileForDemo()
        {
            new UnitDataHelper().SaveKmlFileOnSeverDemo();
        }
        public void ParseDemoDatalogFile()
        {
            new ParseDemoFlightData().ParseDataFiles();
        }

        #endregion Manual Methods

    }

}