﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GA.ApplicationLayer;
using GA.Common;
using GA.DataTransfer;
using System.Web.Mvc;


namespace GuardianAvionicsWeb.Controllers
{
    public class IPassengerAppController : Controller
    {
        //
        // GET: /IPassengerApp/

        public ActionResult IPassengerSettings()
        {
            int profileId = 0;
            if (Session["ProfileId"] != null)
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            else
            {
                RedirectToAction("Login", "Home");
            }
            IPassengerHelper objHelper = new IPassengerHelper();

            return View("IPassengerSettings", objHelper.GetIPassengerMediaDetails(profileId));
        }

    }
}
