﻿using GA.ApplicationLayer;
using GA.DataTransfer.Classes_for_Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuardianAvionicsWeb.Controllers
{
    public class TestKTController : Controller
    {
        //
        // GET: /TestKT/

        [HttpGet]
        public ActionResult AddTestKT()
        {
            TestKTHelper testKTObj = new TestKTHelper();
            var model = testKTObj.GetTestKT();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddTestKT(TestKTModel testKTmodel)
        {
            TestKTHelper testKTobj = new TestKTHelper();
            var model = testKTobj.AddTestKT(testKTmodel);
            return RedirectToAction("ListTestKT");
        }

        [HttpGet]
        public ActionResult DetailTestKT(int id)
        {
            TestKTHelper testKTobj = new TestKTHelper();
            var model = testKTobj.DetailTestKT(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult ListTestKT()
        {
            TestKTHelper testKTobj = new TestKTHelper();
            var model = testKTobj.ListTestKT();
            return View(model);
        }

        [HttpGet]
        public ActionResult EditTestKT(int id)
        {
            TestKTHelper testKTobj = new TestKTHelper();
            var model = testKTobj.DetailTestKT(id);
            return View(model);

        }

        [HttpPost]
        public ActionResult EditTestKT(TestKTModel testKTModel)
        {
            TestKTHelper testKTobj = new TestKTHelper();
            var model = testKTobj.EditTestKT(testKTModel);
            return RedirectToAction("ListTestKT");
        }

        
        public ActionResult DeleteTestKT(int id)
        {
            TestKTHelper testKTobj = new TestKTHelper();
            bool result = testKTobj.DeleteTestKT(id);
            if (result)
            {
                var model = testKTobj.GetTestKT();
                return RedirectToAction("ListTestKT");
            }
            else
            {
                return null;
            }            
        }
    }
}
