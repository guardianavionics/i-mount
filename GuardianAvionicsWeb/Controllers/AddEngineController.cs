﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.OleDb;
using GA.DataLayer;
using GA.Common;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace GuardianAvionicsWeb.Controllers
{
    public class AddEngineController : Controller
    {
        //
        // GET: /AddEngine/

        public void AddEngineAndPropeller()
        {
            setEngineData();
        }


        public void setEngineData()
        {
            
            string path = ConfigurationReader.UnZipFilePath + "\\engine";
            var context = new GuardianAvionicsEntities();
            ExceptionHandler.ReportError(new Exception("Start Inserting Data into Database for Master File"), "---Debug-----");
            string fileName = "";
            string dirName = path;
            try
            {
                // MASTERANSI mast
                fileName = "Engine.txt";
                DataTable dt;
                using (OleDbConnection cn =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + fileName, cn))
                    {
                        dt = new DataTable("MASTER");
                        adapter.Fill(dt);
                    }
                }

                var engine = context.Engines.Where(e => !e.Deleted).Select(s => s.Name).ToList();
                var engineManuOnServer = dt.AsEnumerable().Where(a => a.Field<string>(0).Length > 29)
                    .Select(o => new
                    {
                        Name = o.Field<string>(0).Substring(24, 6).Trim()
                    }).Distinct().ToList();


                var engineMannufacturer = engineManuOnServer.Where(e => !engine.Contains(e.Name) && e.Name != "").ToList();
                if (engineMannufacturer.Count > 0)
                {


                    DataTable dtManufacturer = new DataTable();
                    dtManufacturer.Columns.Add("Name", typeof(string));
                    dtManufacturer.Columns.Add("Id", typeof(int));
                    dtManufacturer.Columns.Add("Deleted", typeof(bool));
                    dtManufacturer.Columns.Add("LastUpdated", typeof(DateTime));

                    foreach (var manufacturer in engineMannufacturer)
                    {
                        DataRow dr = dtManufacturer.NewRow();
                        dr[0] = manufacturer.Name;
                        dr[1] = 0;
                        dr[2] = false;
                        dr[3] = DateTime.UtcNow;
                        dtManufacturer.Rows.Add(dr);


                    }
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "tbEngine";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dtManufacturer;

                    int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConfigurationManager.AppSettings["DataBaseConnection"], CommandType.StoredProcedure, "spSetEngine", param));
                }
                setPropellerData();
            }
            catch (Exception e)
            {
                ExceptionHandler.ReportError(e);
            }
        }

        public void setPropellerData()
        {
            string path = ConfigurationReader.UnZipFilePath + "\\prop";
            var context = new GuardianAvionicsEntities();
            ExceptionHandler.ReportError(new Exception("Start Inserting Data into Database for Master File"), "---Debug-----");
            string fileName = "";
            string dirName = path;
            try
            {
                // MASTERANSI mast
                fileName = "Prop.txt";
                DataTable dt;
                using (OleDbConnection cn =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + fileName, cn))
                    {
                        dt = new DataTable("MASTER");
                        adapter.Fill(dt);
                    }
                }

                var propeller = context.Propellers.Where(e => !e.Deleted).Select(s => s.Name).ToList();
                var propellerManuOnServer = dt.AsEnumerable().Where(a => a.Field<string>(0).Length > 29)
                    .Select(o => new
                    {
                        Name = o.Field<string>(0).Substring(24, 6).Trim()
                    }).Distinct().ToList();


                var propMannufacturer = propellerManuOnServer.Where(e => !propeller.Contains(e.Name) && e.Name != "").ToList();

                if (propMannufacturer.Count > 0)
                {
                    DataTable dtManufacturer = new DataTable();
                    dtManufacturer.Columns.Add("Name", typeof(string));
                    dtManufacturer.Columns.Add("Id", typeof(int));
                    dtManufacturer.Columns.Add("Deleted", typeof(bool));
                    dtManufacturer.Columns.Add("LastUpdated", typeof(DateTime));

                    foreach (var manufacturer in propMannufacturer)
                    {
                        DataRow dr = dtManufacturer.NewRow();
                        dr[0] = manufacturer.Name;
                        dr[1] = 0;
                        dr[2] = false;
                        dr[3] = DateTime.UtcNow;
                        dtManufacturer.Rows.Add(dr);


                    }
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "tbPropeller";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dtManufacturer;

                    int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConfigurationManager.AppSettings["DataBaseConnection"], CommandType.StoredProcedure, "spSetPropeller", param));
                    dt.Rows.Clear();
                }

            }
            catch (Exception e)
            {
                ExceptionHandler.ReportError(e);
            }


        }

    }
}
