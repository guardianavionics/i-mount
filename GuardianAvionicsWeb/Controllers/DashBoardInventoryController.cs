﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuardianAvionicsWeb.Controllers
{
    public class DashBoardInventoryController : Controller
    {
        // GET: DashBoardInventory
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DashBoard()
        {
            ViewBag.UserType= Session["UserType"].ToString();
            return View();
        }
    }
}