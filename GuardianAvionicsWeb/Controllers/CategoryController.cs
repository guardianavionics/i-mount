﻿using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuardianAvionicsWeb.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            if (Session["ProfileId"] == null)
            {
               return RedirectToAction("Login", "Home");
            }
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            ViewBag.CatogaryList = context.Category.Where(x => x.IsActive == true&&x.AircraftId==null).OrderBy(x => x.Priority).ToList(); //&& x.AircraftId== aircraftId
            //ViewBag.CatogaryList = context.Category.Where(x => x.IsActive == true).OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        
        public ActionResult getLiveflight() {
            if(Session["ProfileId"]==null)
            {
                return RedirectToAction("Login", "Home");
            }
            int Id = Convert.ToInt32(Session["ProfileId"]);
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            var Livelist = context.PilotLogs.Where(x => x.FlightDataType == 1 && x.PilotId== Id).ToList();
            //return PartialView("_CategoryItems", CatogaryItemList);
            return Json(new { Livelist }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CatItemList(int Id=0)
        {
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            var CatogaryItemList = context.CategoryItems.Where(x => x.IsActive == true&&x.CategoryId==Id).OrderBy(x => x.RowIndex).ToList();
            //return PartialView("_CategoryItems", CatogaryItemList);
            return Json(new { CatogaryItemList }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeletedCategory(int id)
        {
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            var Category = context.Category.Where(x => x.Id == id).FirstOrDefault();
            Category.IsActive = false;
            //context.Category.Remove(Category);
            var Item =context.CategoryItems.Where(x => x.CategoryId == id).ToList();
            foreach(var i in Item)
            {
                i.IsActive = false;
            }
            //context.CategoryItems.RemoveRange(Item);
            context.SaveChanges();
            var aircraft = context.AircraftProfiles.Where(x => x.Id == Category.AircraftId).FirstOrDefault();
            aircraft.LastUpdated = DateTime.Now;
            context.SaveChanges();
            string GetData = "remove successfully";
            return Json(new { GetData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletedItem(int id)
        {
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            var Item = context.CategoryItems.Where(x => x.Id == id).FirstOrDefault();
            Item.IsActive = false;
            //context.CategoryItems.Remove(Item);
            context.SaveChanges();
            var updatecat = context.Category.Where(x => x.Id == Item.CategoryId).FirstOrDefault();
            var aircraft = context.AircraftProfiles.Where(x => x.Id == updatecat.AircraftId).FirstOrDefault();
            aircraft.LastUpdated = DateTime.Now;
            context.SaveChanges();
            string GetData = "remove successfully";
            return Json(new { GetData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Getdetail(int id)
        {
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            Category cat = context.Category.Where(x => x.Id == id).FirstOrDefault();
            return Json(new { cat }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetItemdetail(int id)
        {
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            CategoryItems catItem = context.CategoryItems.Where(x => x.Id == id).FirstOrDefault();
            return Json(new { catItem }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddupdateCategory(Category category)
        {
            string GetData = "";
            string Id = "";
            int aircraftId = 0;
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            if (category.Id == 0)
            {
                Category categoryAdd = new Category();
                categoryAdd.CategoryName = category.CategoryName;
                categoryAdd.AircraftId = category.AircraftId;
                categoryAdd.CreatedDate = DateTime.Now;
                categoryAdd.CreatedBy = Convert.ToInt32(Session["ProfileId"]);
                //categoryAdd.Priority = category.Priority;
                if (category.CategoryType == 2) {
                    categoryAdd.TabColor = "d9534f";
                }
                categoryAdd.CategoryType = category.CategoryType;
                categoryAdd.IsActive = true;
                context.Category.Add(categoryAdd);
                context.SaveChanges();
                GetData = "Added";
                aircraftId = Convert.ToInt32(category.AircraftId);
                Id = categoryAdd.Id.ToString();
                var setios=context.Category.Where(x => x.Id == categoryAdd.Id).FirstOrDefault();
                setios.IOSCategoryId = categoryAdd.Id;
                context.SaveChanges();
            }
            else
            {
                var categoryEdit = context.Category.Where(x => x.Id == category.Id).FirstOrDefault();
                categoryEdit.CategoryName = category.CategoryName;
                //categoryEdit.AircraftId = category.AircraftId;
                //categoryEdit.TabColor = category.TabColor;
                //categoryEdit.Priority = category.Priority;
                if (category.CategoryType == 2)
                {
                    categoryEdit.TabColor = "d9534f";
                }
                else
                {
                    categoryEdit.TabColor =null;
                }
                categoryEdit.CategoryType = category.CategoryType;
                categoryEdit.UpdatedDate = DateTime.Now;
                categoryEdit.UpdatedBy = Convert.ToInt32(Session["ProfileId"]);
                context.SaveChanges();
                GetData = "Updated";
                aircraftId = Convert.ToInt32(categoryEdit.AircraftId);
                Id = categoryEdit.Id.ToString();
            }
            int catid = Convert.ToInt32(Id);
            var aircraft=context.AircraftProfiles.Where(x => x.Id == aircraftId).FirstOrDefault();
            aircraft.LastUpdated = DateTime.Now;
            context.SaveChanges();
            return Json(new { GetData , Id }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Users", "User");
        }

        public ActionResult AddupdateCategoryItem(CategoryItems item)
        {
            string GetData = "";
            int CatId = 0;
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            if (item.Id == 0)
            {
                var index=context.CategoryItems.Where(x => x.CategoryId == item.CategoryId).OrderByDescending(x => x.RowIndex).FirstOrDefault();
                CategoryItems ItemAdd = new CategoryItems();
                ItemAdd.CategoryId = item.CategoryId;
                ItemAdd.CategoryItemName = item.CategoryItemName;
                ItemAdd.CreatedDate = DateTime.Now;
                ItemAdd.CreatedBy = Convert.ToInt32(Session["ProfileId"]);
                ItemAdd.IsActive = true;
                ItemAdd.Condition = item.Condition;
                if (index == null) { ItemAdd.RowIndex = 1; }
                else { ItemAdd.RowIndex = index.RowIndex + 1; }
                context.CategoryItems.Add(ItemAdd);
                context.SaveChanges();
                GetData = "Added";
                CatId = ItemAdd.CategoryId;
                var setiositem = context.CategoryItems.Where(x => x.Id == ItemAdd.Id).FirstOrDefault();
                setiositem.IOSCategoryItemId = ItemAdd.Id;
                context.SaveChanges();
            }
            else
            {
                var itemEdit = context.CategoryItems.Where(x => x.Id == item.Id).FirstOrDefault();
                //itemEdit.CategoryId = item.CategoryId;
                itemEdit.CategoryItemName = item.CategoryItemName;
                itemEdit.Condition = item.Condition;
                itemEdit.UpdatedDate = DateTime.Now;
                itemEdit.UpdatedBy = Convert.ToInt32(Session["ProfileId"]);
                context.SaveChanges();
                CatId = itemEdit.CategoryId;
                GetData = "Updated";
            }
            var updatecat=context.Category.Where(x => x.Id == CatId).FirstOrDefault();
            updatecat.UpdatedDate = DateTime.Now;
            updatecat.UpdatedBy = Convert.ToInt32(Session["ProfileId"]);
            context.SaveChanges();

            var aircraft = context.AircraftProfiles.Where(x => x.Id == updatecat.AircraftId).FirstOrDefault();
            aircraft.LastUpdated = DateTime.Now;
            context.SaveChanges();
            return Json(new { GetData,CatId }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Users", "User");
        }
        public ActionResult UpdateIndex(int OldIndex,int NewIndex,int SetNewIndexInthisIdId,int ChangeIndexofThisId)
        {
            using (GuardianAvionicsEntities context = new GuardianAvionicsEntities()) {
                var setNewIndex = context.CategoryItems.Where(x => x.Id == SetNewIndexInthisIdId).FirstOrDefault();
                setNewIndex.RowIndex = NewIndex;
                var setNewIndexval = context.CategoryItems.Where(x => x.Id == ChangeIndexofThisId).FirstOrDefault();
                setNewIndexval.RowIndex = OldIndex;
                context.SaveChanges();
                return Json(new { cat=true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}