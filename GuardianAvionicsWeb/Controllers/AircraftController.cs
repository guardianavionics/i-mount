﻿using System;
using GA.ApplicationLayer;
using GA.Common;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Xml;
using System.Text;
using System.Web;
using NReco.VideoConverter;
using TallComponents.PDF;
using System.Drawing;
using System.IO;
using GA.DataTransfer.Classes_for_Services;
using System.Linq;
using GA.DataLayer;

namespace GuardianAvionicsWeb.Controllers
{
    /// <summary>
    /// All the Action methods for aircraft
    /// </summary>
    [HandleError(View = "Error")]
    [SessionExpireFilterAttribute]
    public class AircraftController : Controller
    {
        //public ActionResult AircraftDetails(int? aId, int? flag = 0)
        public ActionResult AircraftDetails(string aId, int? flag = 0)
        {
            //flag = 0 for user
            //flag = 1 for Admin
            try
            {
                if (string.IsNullOrEmpty(aId) || Session["ProfileId"] == null || Session["UserType"] == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    aId = new Misc().Decrypt(aId);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.Master = "_UserLayout.cshtml";
            if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString() || flag == 1)
            {
                ViewBag.Master = "_Layout.cshtml";
            }

            //Validate Aircraft with user
            AircraftHelper helper = new AircraftHelper();

            ViewBag.IsAircraftProfile = "true";
            ViewBag.PageHeaderTitle = "Aircraft Profile";
            ViewBag.User = Session["UserType"].ToString();

            if (TempData["AircraftUpdateStatus"] != null)
            {
                string msg = TempData["AircraftUpdateStatus"].ToString();
                if (msg == "Success")
                {
                    ViewBag.ProfileUpdatemsg = GA.Common.ResourcrFiles.Common.Messages.UpdateProfile.ToString();
                }
                else
                {
                    ViewBag.ProfileUpdatemsg = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileProcessing.ToString();
                }
                TempData["AircraftUpdateStatus"] = null;
            }
            else
            {
                ViewBag.ProfileUpdatemsg = "";
            }
            var AircraftData = new AircraftProfileModelWeb();

            // set viewbag with list of aircrafts data

            // by default set the default aircraft profile image.
            ViewBag.AircraftProfileImageSource = Constants.AircraftDefaultImage;
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            var profile = new UserHelper().GetProfileDetailById(profileId);

            // aId == 0 is case of new aircraft creation
            if (Convert.ToInt32(aId) != 0)
            {
                int aircraftId = Convert.ToInt32(aId);
                Misc objMisc = new Misc();
                AircraftData = helper.GetAircraftDetails(aircraftId, profileId);
                AircraftData.aircraftSalesDetailList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
                GuardianAvionicsEntities context = new GuardianAvionicsEntities();
                ViewBag.CatogaryList = context.Category.Where(x => x.IsActive == true &&x.AircraftId== aircraftId).OrderBy(x => x.Priority).ToList(); //&& x.AircraftId== aircraftId
                if (!System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
                {
                    ViewBag.EngineData = "No Record available for engine settings.";
                    ViewBag.IsEngineDataAvailable = false;
                }
                else
                {
                    try
                    {
                        ViewBag.EngineData = new CommonHelper().ParsePListForEngineSetting(aircraftId);
                        ViewBag.IsEngineDataAvailable = true;
                    }
                    catch (Exception exEngineData)
                    {
                        ViewBag.EngineData = GA.Common.ResourcrFiles.AircraftProfile.Messages.ErrorInEngineData.ToString(); ;
                        ViewBag.IsEngineDataAvailable = false;
                    }
                }
                ViewBag.Id = aircraftId;

                if (profile.UserMaster.UserType == Enumerations.UserType.Admin.ToString())
                {
                    AircraftData.IsOwner = true;
                }

                ViewBag.IsAircraftOwner = AircraftData.IsOwner;
                Session["IsOwner"] = AircraftData.IsOwner;
                ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
                ViewBag.ComponentModelList = AircraftData.ComponentModelList;
            }
            else
            {
                AircraftData = helper.GetEmptyAircraftDetail(Convert.ToInt32(Session["ProfileId"]));
                ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
                ViewBag.ComponentModelList = AircraftData.ComponentModelList;
                ViewBag.EngineData = "No Record available for engine settings.";
                ViewBag.IsEngineDataAvailable = false;
                ViewBag.IsAircraftOwner = false;
            }
           
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("AircraftDetails", AircraftData);

        }

        public ActionResult DashBoardAircraftDetails(string aId, int? flag = 0)
        {
            //flag = 0 for user
            //flag = 1 for Admin
           
            try
            {
                if (string.IsNullOrEmpty(aId) || Session["ProfileId"] == null || Session["UserType"] == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    aId = new Misc().Decrypt(aId);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.Master = "_UserLayout.cshtml";
            if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString() || flag == 1)
            {
                ViewBag.Master = "_Layout.cshtml";
            }

            //Validate Aircraft with user
            AircraftHelper helper = new AircraftHelper();

            ViewBag.IsAircraftProfile = "true";
            ViewBag.PageHeaderTitle = "Aircraft Profile";
            ViewBag.User = Session["UserType"].ToString();

            if (TempData["AircraftUpdateStatus"] != null)
            {
                string msg = TempData["AircraftUpdateStatus"].ToString();
                if (msg == "Success")
                {
                    ViewBag.ProfileUpdatemsg = GA.Common.ResourcrFiles.Common.Messages.UpdateProfile.ToString();
                }
                else
                {
                    ViewBag.ProfileUpdatemsg = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileProcessing.ToString();
                }
                TempData["AircraftUpdateStatus"] = null;
            }
            else
            {
                ViewBag.ProfileUpdatemsg = "";
            }
            var AircraftData = new AircraftProfileModelWeb();

            // set viewbag with list of aircrafts data

            // by default set the default aircraft profile image.
            ViewBag.AircraftProfileImageSource = Constants.AircraftDefaultImage;
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            int aircraftid= Convert.ToInt32(aId);
            int profileId = Convert.ToInt32(context.AircraftProfiles.Where(x => x.Id == aircraftid).Select(y => y.OwnerProfileId).FirstOrDefault());
            var profile = new UserHelper().GetProfileDetailById(profileId);

            // aId == 0 is case of new aircraft creation
            if (Convert.ToInt32(aId) != 0)
            {
                int aircraftId = Convert.ToInt32(aId);
                Misc objMisc = new Misc();
                AircraftData = helper.GetAircraftDetails(aircraftId, profileId);
                AircraftData.aircraftSalesDetailList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
                ViewBag.CatogaryList = context.Category.Where(x => x.IsActive == true && x.AircraftId == aircraftId).OrderBy(x => x.Priority).ToList(); //&& x.AircraftId== aircraftId
                if (!System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
                {
                    ViewBag.EngineData = "No Record available for engine settings.";
                    ViewBag.IsEngineDataAvailable = false;
                }
                else
                {
                    try
                    {
                        ViewBag.EngineData = new CommonHelper().ParsePListForEngineSetting(aircraftId);
                        ViewBag.IsEngineDataAvailable = true;
                    }
                    catch (Exception exEngineData)
                    {
                        ViewBag.EngineData = GA.Common.ResourcrFiles.AircraftProfile.Messages.ErrorInEngineData.ToString(); ;
                        ViewBag.IsEngineDataAvailable = false;
                    }
                }
                ViewBag.Id = aircraftId;

                if (profile.UserMaster.UserType == Enumerations.UserType.Admin.ToString())
                {
                    AircraftData.IsOwner = true;
                }

                ViewBag.IsAircraftOwner = AircraftData.IsOwner;
                Session["IsOwner"] = AircraftData.IsOwner;
                ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
                ViewBag.ComponentModelList = AircraftData.ComponentModelList;
            }
            else
            {
                AircraftData = helper.GetEmptyAircraftDetail(Convert.ToInt32(Session["ProfileId"]));
                ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
                ViewBag.ComponentModelList = AircraftData.ComponentModelList;
                ViewBag.EngineData = "No Record available for engine settings.";
                ViewBag.IsEngineDataAvailable = false;
                ViewBag.IsAircraftOwner = false;
            }

            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("AircraftDetails", AircraftData);

        }

        public ActionResult AircraftList()
        {
            Session["UserAuthorization"] = new UserHelper().SetFeatureByUser(Convert.ToInt32(Session["ProfileId"]));
            if (Session["UserType"] == null || Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.UserType = Session["UserType"].ToString();
            Misc objMisc = new Misc();
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(Convert.ToInt32(Session["ProfileId"]));
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            //List<AircraftDetail> response = (List<AircraftDetail>)helper.setViewBagAircraftListing(Convert.ToInt32(Session["ProfileId"]));
            //response.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            int sno = 1;
            response.OrderByDescending(o => o.AircraftId).ToList().ForEach(f => { f.TId = objMisc.Encrypt(f.AircraftId.ToString()); f.SNO = sno++; });
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(Convert.ToInt32(Session["ProfileId"]));
            if (!isCurrSubscriptionIsPlan5AndActive && Session["UserType"].ToString() != Enumerations.UserType.Maintenance.ToString())
            {
                sno = 1;
                int aircraftId = helper.GetFirstAircraftRegisteredByProfileId(Convert.ToInt32(Session["ProfileId"]));
                response.Where(w => w.AircraftId != aircraftId).ToList().ForEach(f => { f.AircraftId = 0; f.TId = null; });
                response.OrderByDescending(o => o.TId).ToList().ForEach(f => f.SNO = sno++);
            }
            return View("AircraftList", response);
        }

        /// <summary>
        /// Displays aircraft details along with the list of aircraft
        /// </summary>
        /// <param name="aId">Id of aircraft for which unit changed</param>
        /// <returns>Aircraft data</returns>
        public ActionResult AircraftProfile(int? aId)
        {

            //flag = 1 for Profile save success
            //flag = 2 for error while saveing profile

            if (Session["UserType"] != null)
            {
                ViewBag.User = Session["UserType"].ToString();

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            var helper = new AircraftHelper();
            var AircraftData = new AircraftProfileModelWeb();

            // set viewbag with list of aircrafts data
            ViewBag.AircraftList = helper.setViewBagAircraftListing(Convert.ToInt32(Session["ProfileId"]));

            // considering its case of new aircraft creation
            ViewBag.Id = 0;

            // by default set the default aircraft profile image.
            ViewBag.AircraftProfileImageSource = Constants.AircraftDefaultImage;

            // aId == 0 is case of new aircraft creation
            if (aId != 0)
            {
                int aircraftId;
                if (aId == null)
                {
                    // no aircraft selected
                    aircraftId = helper.getAircraftIdAircraftProfilePage(Convert.ToInt32(Session["ProfileId"]));
                    ViewBag.Id = aircraftId;

                    if (aircraftId == default(int))
                    {
                        // no aircraft for user , can create new
                        AircraftData = helper.GetEmptyAircraftDetail(Convert.ToInt32(Session["ProfileId"]));
                        ViewBag.IsEngineDataAvailable = false;
                        return View("AircraftProfile", AircraftData);
                    }
                }
                else // this the case of new aircraft creation
                {
                    aircraftId = (int)aId;
                    ViewBag.Id = aircraftId;
                }

                // setting values for selected aircraft

                AircraftData = helper.GetAircraftDetails(aircraftId, Convert.ToInt32(Session["ProfileId"]));
                if (!System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
                {
                    ViewBag.EngineData = "No Record available for engine settings.";
                    ViewBag.IsEngineDataAvailable = false;
                }
                else
                {
                    try
                    {
                        ViewBag.EngineData = new CommonHelper().ParsePListForEngineSetting(aircraftId);
                        ViewBag.IsEngineDataAvailable = true;
                    }
                    catch (Exception exEngineData)
                    {
                        ViewBag.EngineData = GA.Common.ResourcrFiles.AircraftProfile.Messages.ErrorInEngineData.ToString(); ;
                        ViewBag.IsEngineDataAvailable = false;
                    }
                }



                ViewBag.Id = aircraftId;
                ViewBag.AircraftProfileImageSource = helper.setAircraftImageSource(aircraftId);
                ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
                ViewBag.ComponentModelList = AircraftData.ComponentModelList;
            }
            else
            {
                AircraftData = helper.GetEmptyAircraftDetail(Convert.ToInt32(Session["ProfileId"]));
            }

            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("AircraftProfile", AircraftData);
        }

        /// <summary>
        /// Updates values for aircraft
        /// </summary>
        /// <param name="obj">AircraftProfileModelWeb object</param>
        /// <returns>returns AircraftProfile veiw</returns>
        [SessionExpireFilterAttribute]
        [HttpPost]
        public ActionResult AircraftDetails(AircraftProfileModelWeb obj)
        {
            var helper = new AircraftHelper();
            
            if (ModelState.IsValid)
            {
                bool isSaved;
                int id;

                if (obj.AircraftId != 0)
                {
                    isSaved = helper.UpdateAircraftProfile(obj);

                    TempData["AircraftUpdateStatus"] = (isSaved ? "Success" : "Error");
                }
                else
                {
                    var response = helper.CreateAircraft(obj, Convert.ToInt32(Session["ProfileId"]));
                    //obj.AircraftId = id;
                }

                if (obj != null)
                {
                    // if error occours then show error message
                    var imageSaveMsg = helper.SaveAircraftImage(obj);
                }
                int flag = 0;
                if (Session["UserType"] != null)
                {
                    if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString())
                    {
                        flag = 1;
                    }
                }

                return RedirectToAction("AircraftDetails", new { aid = new Misc().Encrypt(obj.AircraftId.ToString()), flag = flag });
            }

            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                if (modelState.Errors.Count > 0)
                {
                    foreach (ModelError error in modelState.Errors)
                    {

                    }
                }
            }

            // model is not valid 
            // populate dropdown list

            helper.SetSelectListForAllUnits(obj);

            // set viewbag with list of aircrafts data
            ViewBag.Id = obj.AircraftId;
            ViewBag.ProfileUpdatemsg = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileProcessing.ToString();
            var objTempAircraft = helper.GetAircraftDetails(obj.AircraftId, Convert.ToInt32(Session["ProfileId"]));
            obj.aircraftSalesDetailList = objTempAircraft.aircraftSalesDetailList;
            obj.iPassengerMediaDetails = objTempAircraft.iPassengerMediaDetails;
            obj.AircraftModel = new AircraftHelper().GetAircraftModelsByManufactureId(obj.Make);
            obj.Maintenance.MaintenanceUserList = objTempAircraft.Maintenance.MaintenanceUserList;
            obj.Maintenance.SquawkListModel.IssueList = objTempAircraft.Maintenance.SquawkListModel.IssueList;

            if (!System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + obj.AircraftId.ToString() + ".xml"))
            {
                ViewBag.EngineData = "No Record available for engine settings.";
                ViewBag.IsEngineDataAvailable = false;
            }
            else
            {
                try
                {
                    ViewBag.EngineData = new CommonHelper().ParsePListForEngineSetting(obj.AircraftId);
                    ViewBag.IsEngineDataAvailable = true;
                }
                catch (Exception exEngineData)
                {
                    ViewBag.EngineData = GA.Common.ResourcrFiles.AircraftProfile.Messages.ErrorInEngineData.ToString();
                    ViewBag.IsEngineDataAvailable = false;
                }
            }

            ViewBag.AircraftComponentManufacturerList = obj.AircraftComponentManufacturerList;
            ViewBag.ComponentModelList = obj.ComponentModelList;
            ViewBag.IsAircraftOwner = objTempAircraft.IsOwner;
            obj.IsOwner = objTempAircraft.IsOwner;
            return View("AircraftDetails", obj);

        }

        public ActionResult EngineSetting(int aircraftId)
        {
            ViewBag.EngineData = new CommonHelper().ParsePListForEngineSetting(aircraftId);

            return View();
        }

        public ActionResult SaveEngineSetting(string htmlEngineData, int aircraftId)
        {
            byte[] data = Convert.FromBase64String(htmlEngineData);
            StringBuilder decodedString = new StringBuilder();
            decodedString.Append(Encoding.UTF8.GetString(data));

            decodedString = decodedString.Replace("<true></true>", "<true />").Replace("<false></false>", "<false />").Replace("<tru />", "<true />");
            string message = GA.Common.ResourcrFiles.Common.Messages.UpdateRecord.ToString();
            htmlEngineData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\">" + decodedString.ToString() + "</plist>";

            if (System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
            {
                System.IO.File.Delete(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
            }
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(htmlEngineData);
            }
            catch (Exception ex)
            {
                message = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileUpdateRecord.ToString();
            }
            doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
            new AircraftHelper().UpdateAircraftLastUpdateDate(aircraftId, "Engine");
            return Json(message);


        }

        /// <summary>
        /// Changes the unit of fuel for aircraft
        /// </summary>
        /// <param name="fuelUnitName">New Fuel unit</param>
        /// <param name="aircraftId">Id of aircraft for which unit changed</param>
        /// <returns>Aircraft data with changed unit</returns>
        public ActionResult FuelUnitChanged(string fuelUnitName, int aircraftId)
        {
            var helper = new AircraftHelper();
            AircraftProfileModelWeb AircraftData;

            if (aircraftId != default(int) || fuelUnitName != default(string))
            {
                AircraftData = helper.GetAircraftDetails(aircraftId);

                AircraftData.AircraftId = aircraftId;
                AircraftData = helper.ChangeFuelUnit(fuelUnitName, AircraftData);
            }
            else
            {
                int aircraftId1 = helper.getAircraftIdAircraftProfilePage(Convert.ToInt32(Session["ProfileId"]));
                AircraftData = helper.GetAircraftDetails(aircraftId1);
            }

            return Json(AircraftData);
        }

        /// <summary>
        /// Changes the unit of vertical speed for aircraft
        /// </summary>
        /// <param name="VerticalSpeedUnitName">New Vertical Speed unit</param>
        /// <param name="aircraftId">Id of aircraft for which unit changed</param>
        /// <returns>Aircraft data with changed unit</returns>
        public ActionResult VerticalSpeedUnitChanged(string VerticalSpeedUnitName, int aircraftId)
        {
            var helper = new AircraftHelper();
            AircraftProfileModelWeb AircraftData;
            if (aircraftId != default(int) || VerticalSpeedUnitName != default(string))
            {
                AircraftData = helper.GetAircraftDetails(aircraftId);
                AircraftData.AircraftId = aircraftId;
                AircraftData = helper.ChangeVerticalSpeedUnit(VerticalSpeedUnitName, AircraftData);
            }
            else
            {
                int aircraftId1 = helper.getAircraftIdAircraftProfilePage(Convert.ToInt32(Session["ProfileId"]));
                AircraftData = helper.GetAircraftDetails(aircraftId1);
            }

            return Json(AircraftData);
        }

        /// <summary>
        /// Changes the unit of speed for aircraft
        /// </summary>
        /// <param name="SpeedUnitName">New Speed unit</param>
        /// <param name="aircraftId">Id of aircraft for which unit changed</param>
        /// <returns>Aircraft data with changed unit</returns>
        public ActionResult SpeedUnitChanged(string SpeedUnitName, int aircraftId)
        {
            var helper = new AircraftHelper();
            AircraftProfileModelWeb AircraftData;

            if (aircraftId != default(int) || SpeedUnitName != default(string))
            {
                AircraftData = helper.GetAircraftDetails(aircraftId);
                AircraftData.AircraftId = aircraftId;
                AircraftData = helper.ChangeSpeedUnit(SpeedUnitName, AircraftData);
            }
            else
            {
                var aircraftId1 = helper.getAircraftIdAircraftProfilePage(Convert.ToInt32(Session["ProfileId"]));
                AircraftData = helper.GetAircraftDetails(aircraftId1);
            }

            return Json(AircraftData);
        }

        /// <summary>
        /// Deletes the aircraft from aircraft listing
        /// </summary>
        /// <param name="aircraftId">Id of aircraft to be deleted</param>
        /// <returns>Message for successful deletion</returns>

        //public ActionResult DeleteAircraft(int aircraftId, int profileId)
        public ActionResult DeleteAircraft(string aircraftId, int profileId)
        {
            if (!string.IsNullOrEmpty(aircraftId))
            {
                try
                {
                    aircraftId = new Misc().Decrypt(aircraftId);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            var helper = new AircraftHelper();
            ViewBag.ProfileId = profileId;
            ViewBag.UserType = Session["UserType"].ToString();
            var output = helper.DeleteAircraft(Convert.ToInt32(aircraftId), profileId);
            if (output == 1)
            {
                ViewBag.DeleteMessage = GA.Common.ResourcrFiles.AircraftProfile.Messages.DeleteAircraft.ToString();

            }
            else
            {
                ViewBag.DeleteMessage = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileDeleteRecord.ToString();
            }
            Misc objMisc = new Misc();
            //List<AircraftDetail> response = (List<AircraftDetail>)helper.setViewBagAircraftListing(profileId);
            //response.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

            var response = helper.GetAircraftList(profileId);
            response.ForEach(f => f.TId = objMisc.Encrypt(f.AircraftId.ToString()));

            return PartialView("_AircraftListPartial", response);
        }

        /// <summary>
        /// Takes NNumber of aircraft and fetches aircraft make , model , serial number , manufacturer year and no. of engine from FAA's database
        /// </summary>
        /// <param name="NNumber">N-number</param>
        /// <returns>returns false if Aircraft is not found in FAA's database</returns>
        public JsonResult PreFillAircraftFields(string NNumber)
        {
            var helper = new AircraftHelper();
            AircraftProfileModelWeb aircraft = helper.GetAircraftDetailsFromFaa(NNumber);
            if (aircraft == null)
            {
                return Json("False");
            }
            aircraft.Make = aircraft.Make == 1 ? 0 : aircraft.Make;
            aircraft.Model = aircraft.Model == 1 ? 0 : aircraft.Model;
            return Json(aircraft);
        }

        /// <summary>
        /// Checks if there is any other aircraft created with this nNmber.
        /// </summary>
        /// <param name="NNumber"></param>
        /// <param name="aircraftId"></param>
        /// <returns>true when nNumber is unique else false</returns>
        public JsonResult IsNNumberUnique(string NNumber, int aircraftId)
        {
            var profileId = Convert.ToInt32(Session["ProfileId"]);

            if (!string.IsNullOrEmpty(NNumber) && profileId != default(int))
            {
                var helper = new AircraftHelper();
                return Json(helper.IsNNumberUniqueForUser(NNumber: NNumber, profileId: profileId, aircraftId: aircraftId));
            }
            return Json(false);
        }


        public JsonResult GetModelsByManuID(int manufactureId)
        {
            AircraftHelper obj = new AircraftHelper();
            var ModelList = obj.GetAircraftModelsByManufactureId(manufactureId);
            return Json(new SelectList(ModelList, "Value", "Text"));
        }

        public JsonResult GetComponentModelsByManuID(int manufactureId, int componentId)
        {
            AircraftHelper obj = new AircraftHelper();
            var ModelList = obj.GetComponentModelsByManufactureId(manufactureId, componentId);
            return Json(new SelectList(ModelList, "Value", "Text"));
        }

        public ActionResult GetCompManuListByIds(string manuIds, int aircraftId)
        {
            AircraftHelper obj = new AircraftHelper();
            var manuList = obj.GetCompManuListByIds(manuIds, aircraftId);
            var AircraftData = new AircraftProfileModelWeb();
            AircraftData.manufacturerAuthForDataLogList = manuList;
            return PartialView("_ManufacturerAuthForDataLog", AircraftData);
        }


        public ActionResult GetFileNamebyComponentModelId(int componentModelId)
        {
            AircraftHelper aircraftHelper = new AircraftHelper();
            var fileName = aircraftHelper.GetFileNamebyComponentModelId(componentModelId);
            //var downloadLink = "<a href='" + Url.Action("DownloadUserMannual", "Aircraft", new { fileId = componentModelId }) + "' title='Download User Manual'><i class='fa fa-sign-out white'></i>&nbsp;Download</a>";

            return Content(fileName);
        }

        public ActionResult DownloadUserMannual(int fileId)
        {
            string docName = new AircraftHelper().GetUserMannual(fileId);
            var serverPath = ConfigurationReader.DocumentPathKey;
            var path = serverPath + @"\" + docName;

            // add ContentDisposition header for proper opening of file in browser.
            var cd = new System.Net.Mime.ContentDisposition
            {
                // for example foo.bak
                FileName = docName,

                // always prompt the user for downloading, set to true if you want 
                // the browser to try to show the file inline
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            string extention = string.Empty;
            try
            {
                extention = System.IO.Path.GetExtension(path);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
            }

            var contentType = new UnitDataHelper().GetContextTypeByFileExtention(extention);

            return File(path, contentType);
        }

        public ActionResult DeleteAircraftSalesDetailType(string id, int AircraftId)
        {
            Misc objMisc = new Misc();
            try
            {
                id = objMisc.Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request");
            }
            var objHelper = new AircraftHelper();
            var response = objHelper.DeleteAircraftSalesDetailType(Convert.ToInt32(id), AircraftId);
            response.aircraftSalesDetailList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_TfboExportPartial", response.aircraftSalesDetailList);
        }

        public ActionResult UpdateAircraftSalesDetailType(string id, decimal price, string GLAccNo, int AircraftId)
        {
            Misc objMisc = new Misc();
            try
            {
                id = objMisc.Decrypt(id);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request");
            }
            var objHelper = new AircraftHelper();
            var response = objHelper.UpdateAircraftSalesDetailType(Convert.ToInt32(id), price, GLAccNo, AircraftId);
            response.aircraftSalesDetailList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_TfboExportPartial", response.aircraftSalesDetailList);
        }



        [HttpPost]
        public ActionResult UploadIPassengerVideoFile() //Rename UploadPassengerProMapFile to UploadFMSAndPassengerProMapFile
        {
            IPassengerMediaDetails objResponse = new IPassengerMediaDetails();
            try
            {
                string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                var videoFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
                fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") + "." + fileName.Split('.')[1];
                string videoTitle = System.Web.HttpContext.Current.Request.Form["VideoTitle"].ToString();
                int aircraftId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["AircraftId"]);

                var path = ConfigurationReader.IPassengerMediaPath + @"\Video\" + fileName;
                decimal fileSize = decimal.Round((((decimal)videoFile.ContentLength / 1024) / 1024), 3);
                videoFile.SaveAs(path);
                new Misc().SaveFileToS3Bucket(fileName, videoFile.InputStream, "IPassVideo");
                string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\Video\" + fileName.Split('.')[0] + "thumbnail.jpg";

                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                ffMpeg.GetVideoThumbnail(path, thumbnailImagePath, 1);
                new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassVideo");
                try
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(thumbnailImagePath);
                }
                catch (Exception ex)
                {

                }
                AircraftHelper obj = new AircraftHelper();
                objResponse = obj.AddIPassengerVideo(videoTitle, fileName, aircraftId, fileSize, false);
                if (objResponse.ResponseMessage == "Success")
                {
                    ViewBag.IsAddedSuccessfully = "true";
                }
                else
                {
                    ViewBag.IsAddedSuccessfully = "false";
                }
                ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            }
            catch (Exception ex)
            {
                ViewBag.IsAddedSuccessfully = "false";
                return PartialView("_IPassengerVideo", new IPassengerMediaDetails { VideoList = new List<MediaDetails>() });
            }
            return PartialView("_IPassengerVideo", objResponse);
        }




        //public ActionResult AddIPassengerVideo(string title, string fileName, int aircraftId)
        //{
        //    AircraftHelper obj = new AircraftHelper();
        //    IPassengerMediaDetails objResponse = obj.AddIPassengerVideo(title, fileName, aircraftId);
        //    if (objResponse.ResponseMessage == "Success")
        //    {
        //        ViewBag.IsAddedSuccessfully = "true";
        //    }
        //    else
        //    {
        //        ViewBag.IsAddedSuccessfully = "false";
        //    }

        //    return PartialView("_Video", objResponse);

        //}

        [HttpPost]
        public ActionResult UploadIPassengerImageFile()
        {

            var imageFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
            fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") + "." + fileName.Split('.')[1];
            string imageTitle = System.Web.HttpContext.Current.Request.Form["ImageTitle"].ToString();
            int aircraftId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["AircraftId"]);

            var path = ConfigurationReader.IPassengerMediaPath + @"\Image\" + fileName;
            decimal fileSize = decimal.Round((((decimal)imageFile.ContentLength / 1024) / 1024), 3);
            imageFile.SaveAs(path);
            new Misc().SaveFileToS3Bucket(fileName, imageFile.InputStream, "IPassImage");
            string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\Image\" + fileName.Split('.')[0] + "thumbnail.jpg";
            CreateThumbnail(50, path, thumbnailImagePath);
            new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassImage");
            try
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(thumbnailImagePath);
            }
            catch (Exception ex)
            {

            }
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerImage(imageTitle, fileName, aircraftId,fileSize, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerImage", objResponse);

        }

        [HttpPost]
        public ActionResult UploadIPassengerPDFFile()
        {

            var pdfFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
            fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") + "." + fileName.Split('.')[1];
            string pdfTitle = System.Web.HttpContext.Current.Request.Form["PDFTitle"].ToString();
            int aircraftId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["AircraftId"]);

            var path = ConfigurationReader.IPassengerMediaPath + @"\PDF\" + fileName;
            decimal fileSize = decimal.Round((((decimal)pdfFile.ContentLength / 1024) / 1024), 3);
            pdfFile.SaveAs(path);
            new Misc().SaveFileToS3Bucket(fileName, pdfFile.InputStream, "IPassPDF");
            string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\PDF\" + fileName.Split('.')[0] + "thumbnail.jpg";
            SavePDFThumbnail(path, thumbnailImagePath);
            new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassPDF");
            try
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(thumbnailImagePath);
            }
            catch (Exception ex)
            {

            }
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerPDF(pdfTitle, fileName, aircraftId,fileSize, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IpassengerPDF", objResponse);
        }

        //public ActionResult AddIPassengerImage(string title, string fileName, int aircraftId)
        //{
        //    AircraftHelper obj = new AircraftHelper();
        //    IPassengerMediaDetails objResponse = obj.AddIPassengerImage(title, fileName, aircraftId);
        //    if (objResponse.ResponseMessage == "Success")
        //    {
        //        ViewBag.IsAddedSuccessfully = "true";
        //    }
        //    else
        //    {
        //        ViewBag.IsAddedSuccessfully = "false";
        //    }

        //    return PartialView("_Images", objResponse);

        //}


        //public ActionResult AddIPassengerPDF(string title, string fileName, int aircraftId)
        //{
        //    AircraftHelper obj = new AircraftHelper();
        //    IPassengerMediaDetails objResponse = obj.AddIPassengerPDF(title, fileName, aircraftId);
        //    //if (objResponse.ResponseMessage == "Success")
        //    //{
        //    //    ViewBag.IsAddedSuccessfully = "true";
        //    //}
        //    //else
        //    //{
        //    //    ViewBag.IsAddedSuccessfully = "false";
        //    //}
        //    return PartialView("_PDF", objResponse);

        //}

        //public ActionResult UpdateDeleteIPassengerImage(string title, int id, int aircraftId, bool isUpdate)
        public ActionResult UpdateDeleteIPassengerImage(string title, string id, int aircraftId, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerImage(title, Convert.ToInt32(id), aircraftId, isUpdate, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerImage", objResponse);
        }

        //public ActionResult UpdateDeleteIPassengerPDF(string title, int id, int aircraftId, bool isUpdate)
        public ActionResult UpdateDeleteIPassengerPDF(string title, string id, int aircraftId, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerPDF(title, Convert.ToInt32(id), aircraftId, isUpdate, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerPDF", objResponse);
        }

        // public ActionResult UpdateDeleteIPassengerVideo(string title, int id, bool isUpdate, int aircraftId)
        public ActionResult UpdateDeleteIPassengerVideo(string title, string id, bool isUpdate, int aircraftId)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            var response = new AircraftHelper().UpdateDeleteIPassengerVideo(title, Convert.ToInt32(id), isUpdate, aircraftId, false);
            return PartialView("_IPassengerVideo", response);
        }



        public ActionResult AddIPassengerWarning(string title, int aircraftId)
        {
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerWarning(title, aircraftId, false);
            if (objResponse.ResponseMessage == "Success")
            {
                ViewBag.IsAddedSuccessfully = "true";
            }
            else
            {
                ViewBag.IsAddedSuccessfully = "false";
            }
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerWarning", objResponse);

        }

        //public ActionResult UpdateDeleteIPassengerWarning(string title, int id, int aircraftId, bool isUpdate)
        public ActionResult UpdateDeleteIPassengerWarning(string title, string id, int aircraftId, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerWarning(title, Convert.ToInt32(id), aircraftId, isUpdate, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerWarning", objResponse);
        }

        public ActionResult AddIPassengerInstruction(string title, int aircraftId)
        {
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerInstruction(title, aircraftId, false);
            if (objResponse.ResponseMessage == "Success")
            {
                ViewBag.IsAddedSuccessfully = "true";
            }
            else
            {
                ViewBag.IsAddedSuccessfully = "false";
            }
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerInstruction", objResponse);
        }

        //public ActionResult UpdateDeleteIPassengerInstruction(string title, int id, int aircraftId, bool isUpdate)
        public ActionResult UpdateDeleteIPassengerInstruction(string title, string id, int aircraftId, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerInstruction(title, Convert.ToInt32(id), aircraftId, isUpdate, false);
            ViewBag.IsAircraftOwner = Convert.ToBoolean(Session["IsOwner"]);
            return PartialView("_IPassengerInstruction", objResponse);
        }

        void CreateThumbnail(int ThumbnailMax, string OriginalImagePath, string ThumbnailImagePath)
        {
            // Loads original image from file
            System.Drawing.Image imgOriginal = System.Drawing.Image.FromFile(OriginalImagePath);
            // Finds height and width of original image
            float OriginalHeight = imgOriginal.Height;
            float OriginalWidth = imgOriginal.Width;
            // Finds height and width of resized image
            int ThumbnailWidth;
            int ThumbnailHeight;
            if (OriginalHeight > OriginalWidth)
            {
                ThumbnailHeight = ThumbnailMax;
                ThumbnailWidth = (int)((OriginalWidth / OriginalHeight) * (float)ThumbnailMax);
            }
            else
            {
                ThumbnailWidth = ThumbnailMax;
                ThumbnailHeight = (int)((OriginalHeight / OriginalWidth) * (float)ThumbnailMax);
            }
            // Create new bitmap that will be used for thumbnail
            System.Drawing.Bitmap ThumbnailBitmap = new System.Drawing.Bitmap(ThumbnailWidth, ThumbnailHeight);
            System.Drawing.Graphics ResizedImage = System.Drawing.Graphics.FromImage(ThumbnailBitmap);
            // Resized image will have best possible quality
            ResizedImage.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            ResizedImage.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ResizedImage.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw resized image
            ResizedImage.DrawImage(imgOriginal, 0, 0, ThumbnailWidth, ThumbnailHeight);
            // Save thumbnail to file
            ThumbnailBitmap.Save(ThumbnailImagePath);
        }

        public void SavePDFThumbnail(string pdfFilePath, string thumbnailImgPath)
        {
            TallComponents.PDF.Rasterizer.Document pdfDocument = new TallComponents.PDF.Rasterizer.Document(new FileStream(pdfFilePath, FileMode.Open, FileAccess.Read));
            TallComponents.PDF.Rasterizer.Page pdfPage = pdfDocument.Pages[0];

            //create a bitmap to draw to and a graphics object
            using (Bitmap bitmap = new Bitmap((int)pdfPage.Width, (int)pdfPage.Height))
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                //draw the image from the first page to the graphics object, which is connected to the bitmap object
                pdfPage.Draw(graphics);
                bitmap.Save(thumbnailImgPath);
            }

        }


        public ActionResult CloseIssue(int issueId, string comment, int profileId)
        {
            if (profileId == 0)
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            //string message = "Issue Closed Successfully";
            return PartialView("_SquawkListPartial", new AircraftHelper().CloseIssue(profileId, issueId, comment));
        }

        public ActionResult CloseIssueFromAircraftModule(int issueId, string comment, int aircraftId, int profileId)
        {
            return PartialView("_SquawkListPartial", new AircraftHelper().CloseIssueFromAircraftModule(issueId, comment, aircraftId, profileId));
        }


        public JsonResult AddMaintenanceUser(string emailId, int aircraftId, int profileId)
        {
            RegisterMaintenanceUserRequest objRequest = new RegisterMaintenanceUserRequest
            {
                AircraftId = aircraftId,
                EmailId = emailId
            };

            UserHelper objHelper = new UserHelper();
            var response = objHelper.RegisterMaintenanceUser(objRequest);
            string message = "";
            if (response.ResponseMessage != Enumerations.RegistrationReturnCodes.Success.GetStringValue())
            {
                switch (response.ResponseCode)
                {
                    case "9":
                        {
                            message = GA.Common.ResourcrFiles.UserProfile.Messages.InvalidEmailId.ToString();
                        }
                        break;
                    case "8": //DuplicateEmailId
                        {
                            message = GA.Common.ResourcrFiles.UserProfile.Messages.DupluicateEmailIdMaintenanceUser.ToString();
                        }
                        break;
                    case "15": //MaintenanceUserAlreadyMappedWithSameAircraft
                        {
                            message = GA.Common.ResourcrFiles.UserProfile.Messages.MaintenanceUserAlreadyRegisteredWithAircraft.ToString();
                        }
                        break;

                }
            }
            else
            {
                message = "Success";
            }

            return Json(new { message }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GetMaintenanceDetails(int aircraftId, int profileId)
        {
            var response = new AircraftHelper().GetMaintenanceDetails(aircraftId, profileId);

            return PartialView("_Maintenance", response);
        }

        public ActionResult GenerateIssue(int? aircraftId, int profileId, string title, int? assigneeId, int priorityId)
        {
            aircraftId = aircraftId == 0 ? null : aircraftId;
            assigneeId = assigneeId == 0 ? null : assigneeId;
            IssueModel objRequest = new IssueModel
            {
                AircraftId = aircraftId,
                AssigneeId = assigneeId == 0 ? null : assigneeId,
                AssignerId = profileId,
                ClosedById = null,
                PriorityId = priorityId,
                StatusId = (int)Enumerations.IssueStatus.Open,
                Title = title
            };
            var message = "";
            var response = new AircraftHelper().GenerateIssue(objRequest);

            if (response.ResponseMessage == Enumerations.AircraftIssue.Success.ToString())
            {
                message = "Success";
            }
            else
            {
                message = "Error";
            }
            return Json(new { message }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetSquwakListByAircraftId(int aircraftId, int profileId, string hdnIsAircraftProfile = "true")
        {
            MaintenanceModelWeb obj = new MaintenanceModelWeb
            {
                SquawkListModel = new SquawkListModelWeb(),
                MaintenanceUserList = new List<MaintenanceUserModel>()
            };
            obj = new AircraftHelper().GetSquwakListByAircraftId(aircraftId, profileId);
            ViewBag.IsAircraftProfile = hdnIsAircraftProfile;
            return PartialView("_SquawkListPartial", obj);
        }

        public ActionResult GetSquwakList(int profileId, string isAircraftProfile)
        {
            MaintenanceModelWeb obj = new MaintenanceModelWeb
            {
                SquawkListModel = new SquawkListModelWeb(),
                MaintenanceUserList = new List<MaintenanceUserModel>()
            };
            obj = new AircraftHelper().GetSquawkListTemp(profileId);

            ViewBag.IsAircraftProfile = isAircraftProfile;
            return PartialView("_SquawkListPartial", obj);
        }

        public ActionResult SquawkList()
        {
            ViewBag.IsAircraftProfile = "false";
            if (Session["ProfileId"] == null)
            {
                return RedirectToAction("Login");
            }
            int profileId = Convert.ToInt32(Session["ProfileId"]);

            ViewBag.ProfileId = profileId;
            ViewBag.PageHeaderTitle = "Squawk List";

            return View("SquawkList", new AircraftHelper().GetSquawkListTemp(profileId));

        }

        public ActionResult GetMaintenanceUserByAIrcraftId(int aircraftId, int profileId)
        {
            bool isOwner = false;
            var userList = new AircraftHelper().GetMaintenanceUserByAIrcraftId(aircraftId, profileId, out isOwner);
            var jsonResult = Json(new { userList = userList, isOwner = isOwner }, JsonRequestBehavior.AllowGet);
            return jsonResult;
        }

        public ActionResult UpdateIssue(int issueId, int? assigneeId, int priority, int profileId, string IsAircraftProfile)
        {
            ViewBag.IsAircraftProfile = false;
            IssueModel objRequest = new IssueModel();
            objRequest.AssigneeId = assigneeId == 0 ? null : assigneeId;
            objRequest.ClosedById = null;
            objRequest.Id = issueId;
            objRequest.PriorityId = priority;
            var response = new AircraftHelper().UpdateIssue(objRequest);
            string message = "";
            switch (response.ResponseCode)
            {
                case "5034": //InvalidIssueId
                    {
                        message = "Invalid request";
                    }
                    break;
                case "0": //InvalidIssueId
                    {
                        message = "Success";
                    }
                    break;
                case "9999": //InvalidIssueId
                    {
                        message = "Error";
                    }
                    break;

            }

            var jsonResult = Json(new { message = message }, JsonRequestBehavior.AllowGet);
            return jsonResult;

        }
       
    }


}