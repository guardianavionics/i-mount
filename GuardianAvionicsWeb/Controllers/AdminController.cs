﻿using System;
using System.Collections.Generic;
using GA.ApplicationLayer;
using System.Web.Mvc;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
using Microsoft.Ajax.Utilities;
using PagedList;
using GA.Common;
using System.Linq;
using System.Web;
using System.Configuration;
using Ionic.Zip;
using Amazon.S3.Model;
using Amazon.S3;
using System.IO;
using System.Drawing;
using GA.DataTransfer.Classes_for_Services;
using GuardianAvionicsWeb.Filters;

namespace GuardianAvionicsWeb.Controllers
{
    [HandleError(View = "Error")]
    [SessionExpireFilterAttribute]
    public class AdminController : Controller
    {
        public ActionResult AdminHome(int? pageNo, bool isBlocked = false)
        {
            var helper = new AdminHelper();
            Session["UserType"] = "Admin";
            var model = helper.GetUserList(isBlocked);
            var pNo = pageNo ?? 1;
            ViewBag.IsBlocked = isBlocked;
            ViewBag.SearchList = new SelectList(new[]
            {
                new { Id = "1", Name = "EmailId" },
                new { Id = "2", Name = "First Name" },
                new { Id = "3", Name = "Last Name" },
            }, "Id", "Name");

            // return View(model.UserList.ToPagedList(pageNumber: pNo, pageSize: Constants.PageSizeTable));
            return View("AdminHome", model);
        }

        public ActionResult ManufacturerUser()
        {
            List<UserInfo> obj = new AdminHelper().GetmanufacturerUserList();
            ViewBag.PageHeaderTitle = "Manufacturer User";
            return View("ManufacturerUser", obj);
        }

        //public ActionResult BlockUser(int profileId, string isBlocked)
        public ActionResult BlockUser(string profileId, string isBlocked)
        {
            try
            {
                profileId = new Misc().Decrypt(profileId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            List<UserInfo> obj = new List<UserInfo>();
            Misc objMIsc = new Misc();
            obj = new AdminHelper().BlockUnblockUser(Convert.ToInt32(profileId), isBlocked);
            obj.ForEach(f => { f.TId = objMIsc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });

            return PartialView("_ActiveUserPartial", obj);
        }

        public ActionResult UnBlockUser(string profileId, string isBlocked)
        {
            try
            {
                profileId = new Misc().Decrypt(profileId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            Misc objMIsc = new Misc();
            List<UserInfo> obj = new List<UserInfo>();
            obj = new AdminHelper().BlockUnblockUser(Convert.ToInt32(profileId), isBlocked);
            obj.ForEach(f => { f.TId = objMIsc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });

            return PartialView("_InActiveUserPartial", obj);
        }

        public ActionResult GetInActiveUser()
        {
            List<UserInfo> obj = new List<UserInfo>();
            obj = new AdminHelper().GetInActiveUser();
            Misc objMIsc = new Misc();
            obj.ForEach(f => { f.TId = objMIsc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });

            return PartialView("_InActiveUserPartial", obj);
        }

        public ActionResult GetActiveUser()
        {
            List<UserInfo> obj = new List<UserInfo>();
            obj = new AdminHelper().GetActiveUser();
            Misc objMIsc = new Misc();
            obj.ForEach(f => { f.TId = objMIsc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });

            return PartialView("_ActiveUserPartial", obj);
        }

        public ActionResult Index(string userType = "Active")
        {
            AdminHome obj = new GA.DataTransfer.Classes_for_Web.AdminHome();
            obj = new AdminHelper().GetActiveInActiveUsers();
            ViewBag.PageHeaderTitle = "Manage User";
            ViewBag.UserType = userType;
            Misc objMisc = new Misc();
            obj.InActiveUserList.ForEach(f => { f.TId = objMisc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });
            obj.UserList.ForEach(f => { f.TId = objMisc.Encrypt(f.ProfileId.ToString()); f.ProfileId = 0; });

            return View("Index", obj);
        }

        public ActionResult PassengerMapFile()
        {
            var adminHelper = new AdminHelper();
            var aviationChartModel = adminHelper.GetPassengerProMapFileList();
            ViewBag.RegionList = adminHelper.GetRegionList();

            return View("PassengerMapFile", aviationChartModel);
        }

        public ActionResult ChartFile()
        {
            ViewBag.PageHeaderTitle = "Aviation Chart";
            var adminHelper = new AdminHelper();
            var obj = adminHelper.GetChartDetailsForWeb();

            return View("ChartFile", obj);
        }

        public ActionResult LinkedUnit()
        {
            ViewBag.PageHeaderTitle = "Linked Units";
            var adminHelper = new AdminHelper();
            var aeroUnitsModel = adminHelper.GetLinkedAeroUnits();

            var context = new GuardianAvionicsEntities();
            ViewBag.ReasonList = context.ReasonMaster.Select(s => new ListItems
            {
                text = s.ReasonText,
                value = s.Id,
            }).ToList();

            return View("LinkedUnit", aeroUnitsModel);
        }

        public ActionResult ProductionUnit()
        {
            ViewBag.PageHeaderTitle = "Production Units";
            var adminHelper = new AdminHelper();
            var aeroUnitsModel = adminHelper.GetUnlinkedAeroUnits();
            return View("ProductionUnit", aeroUnitsModel);
        }

        public ActionResult PreLinkedUnit()
        {
            ViewBag.PageHeaderTitle = "Blocked Units";
            var adminHelper = new AdminHelper();
            var aeroUnitsModel = adminHelper.GetPreviouslyLinkedAeroUnits();
            return View("PreLinkedUnit", aeroUnitsModel);
        }

        public ActionResult ReplacedUnit()
        {
            ViewBag.PageHeaderTitle = "Replaced Units";
            var adminHelper = new AdminHelper();
            var aeroUnitsModel = adminHelper.GetReplacedAeroUnits();
            return View("ReplacedUnit", aeroUnitsModel);
        }


        //public ActionResult GetmanufacturerUserList(int? pageNo)
        //{
        //    var helper = new AdminHelper();
        //    var model = helper.GetmanufacturerUserList();
        //    ViewBag.IsBlocked = false;
        //    ViewBag.SearchList = new SelectList(new[]
        //    {
        //        new { Id = "1", Name = "EmailId" },
        //        new { Id = "2", Name = "First Name" },
        //        new { Id = "3", Name = "Last Name" },
        //    }, "Id", "Name");
        //    return PartialView("_ManufactruerUserList", model.ManufacturerUserList.ToPagedList(pageNumber: pageNo ?? 1, pageSize: Constants.PageSizeTable));
        //}

        public ActionResult UserListByBlockStatus(int? pageNo, bool isBlocked)
        {
            var helper = new AdminHelper();

            AdminHome model = helper.GetUserList(isBlocked);
            int pNo = pageNo ?? 1;
            ViewBag.IsBlocked = isBlocked;

            ViewBag.SearchList = new SelectList(new[]
            {
                new { Id = "1", Name = "EmailId" },
                new { Id = "2", Name = "First Name" },
                new { Id = "2", Name = "Last Name" },
            }, "Id", "Name");

            return PartialView("_UserList", model.UserList.ToPagedList(pageNumber: pNo, pageSize: Constants.PageSizeTable));
        }

        public ActionResult GetUserDetailsByKeyWord(string keyword, bool blockstatus, string searchBy, int? pageNo, string userType)
        {
            var helper = new AdminHelper();
            AdminHome model = helper.GetUserDetailsByKeyWord(keyword, blockstatus, searchBy, userType);
            ViewBag.IsBlocked = blockstatus;
            ViewBag.SearchList = new SelectList(new[]
            {
                new { Id = "1", Name = "EmailId" },
                new { Id = "2", Name = "First Name" },
                new { Id = "3", Name = "Last Name" },
            }, "Id", "Name");
            int pNo = pageNo ?? 1;

            ViewBag.keyWord = keyword;
            //ViewBag.searchBy = searchBy;

            return PartialView("_UserList", model.UserList.ToPagedList(pageNumber: pNo, pageSize: Constants.PageSizeTable));
        }

        public ActionResult DeleteManufactureruser(int id)
        {
            var helper = new AdminHelper();
            var response = helper.DeleteManufactureruser(id);
            ViewBag.IsBlocked = false;


            return PartialView("_ManufacturerUserPartial", response);
        }

        public ActionResult GetManufacturerUserDetailsByKeyWord(string keyword, string searchBy, int? pageNo, string userType)
        {
            var helper = new AdminHelper();
            AdminHome model = helper.GetUserDetailsByKeyWord(keyword, false, searchBy, userType);
            ViewBag.IsBlocked = false;
            ViewBag.SearchList = new SelectList(new[]
            {
                new { Id = "1", Name = "EmailId" },
                new { Id = "2", Name = "First Name" },
                new { Id = "3", Name = "Last Name" },
            }, "Id", "Name");
            int pNo = pageNo ?? 1;

            ViewBag.keyWord = keyword;

            return PartialView("_ManufactruerUserList", model.ManufacturerUserList.ToPagedList(pageNumber: pNo, pageSize: Constants.PageSizeTable));
        }

        public ActionResult SearchUser(string keyword, bool blockstatus, string searchBy, string userType)
        {
            // Get Tags from database
            var helper = new AdminHelper();
            string[] result = helper.SearchUser(keyword, blockstatus, searchBy, userType);
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoginAsUser(string profileId)
        {
            try
            {
                profileId = new Misc().Decrypt(profileId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            Session["ProfileId"] = Convert.ToInt32(profileId);
            Profile profile = new UserHelper().GetProfileDetailById(Convert.ToInt32(profileId));

            if (profile != null)
            {
                Session["UserType"] = profile.UserMaster.UserType;
            }

            // no error found . set error flag as false
            ViewBag.HasError = Boolean.FalseString;

            if (Session["UserType"] != null)
            {
                if (Session["UserType"].ToString() == Enumerations.UserType.Manufacturer.ToString())
                {
                    return RedirectToAction("ListAllJPIData", "Data");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

            return RedirectToAction("PilotSummaryDetails", "Home");
        }

        public JsonResult SendPushNotificationToAllUser(string message)
        {
            var helper = new AdminHelper();
            bool result = helper.SendPushNotificationToAllUser(message);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendPushNotificationToUsers(string usernames, string message)
        {
            var helper = new AdminHelper();
            bool result = helper.SendPushNotificationToUsers(usernames, message);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsMapFileExists(string mapFileName)
        {
            return Json(new AdminHelper().IsMapFileExists(mapFileName));
        }

        public ActionResult SaveMapFile(FormCollection formCollection, HttpPostedFileBase file)
        {
            var mapFile = new MapFile
            {
                Name = formCollection["FMSUniqueFileName"],
                FileSize = (Convert.ToInt32(formCollection["FMSFileSize"]) / 1024) / 1024,
                Title = formCollection["FMSMapFileTitle"],
                Deleted = false,
                LastUpdated = DateTime.UtcNow,
                Url = formCollection["FMSUniqueFileName"],
            };
            var adminHelper = new AdminHelper();
            adminHelper.SaveMapFile(mapFile);
            return GetMapFileList(1);
        }

        public ActionResult SavePassengerProMapFile(FormCollection formCollection, HttpPostedFileBase file)
        {
            var passengerProMapFile = new PassengerProMapFile
            {
                Name = formCollection["UniqueFileName"],
                FileSize = (Convert.ToInt32(formCollection["FileSize"]) / 1024) / 1024,
                Title = formCollection["MapFileTitle"],
                ZoomLevel = Convert.ToInt32(formCollection["ZoomLevel"]),
                PassengerProRegionMasterId = Convert.ToInt32(formCollection["RegionId"]),
                Deleted = false,
                CreateDate = DateTime.UtcNow
            };
            var adminHelper = new AdminHelper();
            adminHelper.SavePassengerProMapFile(passengerProMapFile);
            return GetPassengerProMapFileList();
        }

        public ActionResult AviationChart()
        {
            ChartProgressBar.dictionary = new Dictionary<string, int>();
            var adminHelper = new AdminHelper();
            var aviationChartModel = adminHelper.GetMapFiles();
            ViewBag.RegionList = adminHelper.GetRegionList();
            return View(aviationChartModel);
        }

        public ActionResult GetMapFileList(int? pageNo)
        {
            var adminHelper = new AdminHelper();
            var mapFileList = adminHelper.GetMapFileList();
            int pNo = pageNo ?? 1;
            return PartialView("_MapFileList", mapFileList.ToPagedList(pageNumber: pNo, pageSize: Constants.PageSizeTable));
        }

        public ActionResult GetPassengerProMapFileList()
        {
            var adminHelper = new AdminHelper();
            var passengerProMapFileList = adminHelper.GetPassengerProMapFileList();
            return PartialView("_PassengerProMapFileList", passengerProMapFileList);
        }

        public JsonResult DeleteMapFile(int mapFileId)
        {
            var result = new AdminHelper().DeleteMapFileById(mapFileId);
            return Json(result ? GAResource.DocumentDeletionSuccessMessage : GAResource.DocumentDeletionFailedMessage);
        }

        public ActionResult DeletePassengerProMapFile(int mapFileId)
        {
            var result = new AdminHelper().DeletePassengerProMapFileById(mapFileId);
            return RedirectToAction("GetPassengerProMapFileList");
        }

        [HttpPost]
        public void UploadFMSAndPassengerProMapFile() //Rename UploadPassengerProMapFile to UploadFMSAndPassengerProMapFile
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                string fileName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
                SaveZipFile(file, fileName);
            }
        }

        public bool SaveZipFile(HttpPostedFileBase file, string uniqueName)
        {
            try
            {
                var fileName = file.FileName;
                string fileExt = file.FileName.Split('.')[1];
                string path = string.Empty;
                if (fileExt.ToUpper() == "ZIP" || fileExt.ToUpper() == "RAR")
                {
                    path = ConfigurationReader.MapFileUrl + @"\" + uniqueName;
                    file.SaveAs(path);
                }
                else
                {
                    path = ConfigurationReader.MapFileUrl + @"\" + fileName;
                    file.SaveAs(path);
                }

                if (fileExt.ToUpper() != "ZIP" || fileExt.ToUpper() != "RAR")
                {
                    string zipPath = ConfigurationReader.MapFileUrl + @"\" + uniqueName;

                    ZipFile objZip = new ZipFile();
                    objZip.AddFile(path, string.Empty);
                    objZip.Save(zipPath);
                    System.IO.File.Delete(path);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #region ConfigurationSetting

        public ActionResult Engine()
        {
            ViewBag.PageHeaderTitle = "Engine";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetEngines();
            return View("Engine", response);
        }

        public ActionResult Propeller()
        {
            ViewBag.PageHeaderTitle = "Propeller";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetPropellers();
            return View("Propeller", response);
        }

        public ActionResult RatingType()
        {
            ViewBag.PageHeaderTitle = "Rating Type";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetRatingTypes();
            return View("RatingType", response);
        }

        public ActionResult MedicalCertificate()
        {
            ViewBag.PageHeaderTitle = "Medical Certificate Class";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetMedicalCertificateClass();
            return View("MedicalCertificate", response);
        }

        public ActionResult FlightBag()
        {
            ViewBag.PageHeaderTitle = "Flight Bag Documents";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetFlightBag();
            return View("FlightBag", response);
        }

        public ActionResult AircraftManufacturer()
        {
            ViewBag.PageHeaderTitle = "Aircraft Manufacturer";
            AdminHelper obj = new AdminHelper();
            var response = obj.GetAircraftManufacturer();
            return View("AircraftManufacturer", response);
        }

        public ActionResult AircraftModel(int manufacturerId, string aircraftManufacturerName)
        {
            ViewBag.PageHeaderTitle = "Aircraft Models of " + aircraftManufacturerName;
            var response = new List<ListItems>();
            response = new AircraftHelper().GetAircraftModelsByManufactureId(manufacturerId);
            response = response.Where(a => a.value != 0 && a.value != -1).ToList();
            ViewBag.MakeId = manufacturerId;
            return View("AircraftModel", response);
        }

        public ActionResult ComponentManufacturer()
        {
            ViewBag.PageHeaderTitle = "Component Manufacturer";
            var response = new List<ComponentManufacturerAndUser>();
            response = new AdminHelper().GetComponentManufacturer();
            return View("ComponentManufacturer", response);
        }

        public ActionResult ComponentModel(int id, string name)
        {
            ViewBag.PageHeaderTitle = "Models of " + name;
            var response = new List<ComponentModel_ConfigSetting>();
            var context = new GuardianAvionicsEntities();
            ViewBag.CompManuId = id;
            ViewBag.ComponentList = context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
            {
                text = s.ComponentName,
                value = s.Id,
            }).ToList();
            response = new AdminHelper().GetComponentModelList(id);
            return View("ComponentModel", response);
        }

        public ActionResult ConfigurationSetting(int? flag)
        {
            ViewBag.Focus = (flag != null) ? "div_ComponentManufacturer" : "";
            var response = new AircraftHelper().GetEngines();
            ViewBag.ViewName = "Engine";
            return View(response);
        }

        public ActionResult ConfigurationTest(int? flag)
        {
            ViewBag.Focus = (flag != null) ? "div_ComponentManufacturer" : "";
            var response = new AircraftHelper().GetEngines();
            ViewBag.ViewName = "Engine";
            return View(response);
        }

        public ActionResult GetEngine()
        {
            var response = new AircraftHelper().GetEngines();
            ViewBag.ViewName = "Engine";
            return PartialView("_Engine", response);
            //return View(response);
        }

        public ActionResult GetPropeller()
        {
            var response = new AircraftHelper().GetPropellers();
            ViewBag.ViewName = "Propeller";
            return PartialView("_Propeller", response);
            //return View("ConfigurationTest", response);
        }

        public ActionResult GetRatingTypes()
        {
            var response = new AircraftHelper().GetRatingTypes();
            ViewBag.ViewName = "RatingType";
            return PartialView("_RatingType", response);
            //return View("ConfigurationTest",response);
        }

        public ActionResult GetMedicalCertificateClass()
        {
            var response = new AircraftHelper().GetMedicalCertificateClass();
            ViewBag.ViewName = "MedicalCertificate";
            return PartialView("_MedicalCertificateClass", response);
            //return View("ConfigurationTest", response);
        }

        public ActionResult GetFlightBagDocuments()
        {
            var response = new AircraftHelper().GetFlightBagDocuments();
            ViewBag.ViewName = "FlightBag";
            return PartialView("_Document", response);
            //return View("ConfigurationTest", response);
        }

        public ActionResult GetAircraftManufacturer()
        {
            var response = new AircraftHelper().GetAircraftManufacturer();
            ViewBag.ViewName = "AircraftManufacturer";
            return PartialView("_AircraftManufacturer", response);
            //return View("ConfigurationTest", response);
        }

        public ActionResult GetComponentManufacturer()
        {
            var response = new AircraftHelper().GetComponentManufacturer();
            ViewBag.ViewName = "ComponentManufacturer";
            return PartialView("_ComponentManufacturer", response);
            //return View("ConfigurationTest", response);
        }

        public ActionResult UpdateDeleteEngine(string name, int id, bool isUpdate)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeleteEngine(name, id, isUpdate);
            return PartialView("_EnginePartial", response);
        }

        public JsonResult IsEngineExist(string engineName,int id)
        {
            var response = false;
            response = new AdminHelper().isEngineExists(engineName,id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEngineName(string name)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddEngineName(name);
            return PartialView("_EnginePartial", response);
        }

        public ActionResult UpdateDeleteRatingType(string name, int id, bool isUpdate)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeleteRatingType(name, id, isUpdate);
            return PartialView("_RatingPartial", response);
        }

        public JsonResult isRatingTypeExists(string ratingTypeName ,int id)
        {
            var response = false;
            response = new AdminHelper().isRatingTypeExists(ratingTypeName, id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRatingType(string name)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddRatingType(name);
            return PartialView("_RatingPartial", response);
        }

        public ActionResult UpdateDeletePropeller(string name, int id, bool isUpdate)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeletePropeller(name, id, isUpdate);
            return PartialView("_PropellerPartial", response);
        }

        public JsonResult isPropellerExists(string propellerName ,int id)
        {
            var response = false;
            response = new AdminHelper().isPropellerExists(propellerName, id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPropeller(string name)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddPropeller(name);
            return PartialView("_PropellerPartial", response);
        }

        public ActionResult UpdateDeleteMedCertClass(string name, int id, bool isUpdate)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeleteMedCertClass(name, id, isUpdate);
            return PartialView("_MedicalCertificatePartial", response);
        }

        public JsonResult isMedCertClassExists(string medCertClassName,int id)
        {
            var response = false;
            response = new AdminHelper().isMedCertClassExists(medCertClassName,id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMedCertClass(string name)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddMedCertClass(name);
            return PartialView("_MedicalCertificatePartial", response);
        }

        public ActionResult UpdateDeleteAircraftManufacturer(string name, int id, bool isUpdate)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeleteAircraftManufacturer(name, id, isUpdate);
            return PartialView("_AircraftManufacturerPartial", response);
        }

        public ActionResult UpdateDeleteComponentManufacturer(string name, int id, bool isUpdate)
        {
            var response = new List<ComponentManufacturerAndUser>();
            response = new AdminHelper().UpdateDeleteComponentManufacturer(name, id, isUpdate);
            return PartialView("_ComponentManufacturerPartial", response);
        }

        public ActionResult DeleteUserFromComponentManufacturer(string UserEmailId)
        {
            var response = new List<ComponentManufacturerAndUser>();
            response = new AdminHelper().DeleteUserFromComponentManufacturer(UserEmailId);
            return PartialView("_ComponentManufacturerPartial", response);
        }


        public JsonResult IsAircraftManufacturerExists(string manufacturerName, int id)
        {
            var response = false;
            response = new AdminHelper().isAircraftManufacturerExists(manufacturerName,id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsAircraftModelExists(string modelName, int makeId)
        {
            var response = false;
            response = new AdminHelper().isAircraftModelExists(modelName, makeId);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddAircraftManufacturer(string name)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddAircraftManufacturer(name);
            return PartialView("_AircraftManufacturerPartial", response);
        }

        /// <summary>
        /// currently there is no use of temp parameter
        /// </summary>
        /// <param name="manufacturerName"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        public ActionResult SetComponentManufacturer(string manufacturerName)
        {
            var response = new AdminHelper().SetComponentManufacturer(manufacturerName);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Check component manufacturer exist
        /// </summary>
        /// <param name="manufacturerName"></param>
        /// <returns></returns>
        public ActionResult IsManufacturerExists(string manufacturerName,int id)
        {
            var response = false;
            response = new AdminHelper().IsManufacturerExists(manufacturerName ,id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddComponentManufacturer(string name)
        {
            var response = new List<ComponentManufacturerAndUser>();
            response = new AdminHelper().AddComponentManufacturer(name);
            return PartialView("_ComponentManufacturerPartial", response);
        }

        public ActionResult UpdateDeleteAircraftModel(string name, int id, bool isUpdate, int makeId)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().UpdateDeleteAircraftModel(name, id, isUpdate, makeId);
            ViewBag.MakeId = makeId;
            return PartialView("_AircraftModelPartial", response);
        }

        public ActionResult UpdateDeleteComponentModel(string name, int id, bool isUpdate, int componentId, int componentManufacturerId, string fileName, string uniqueNameForURL, int fileSize, string mimeType)
        {
            var response = new List<ComponentModel_ConfigSetting>();
            response = new AdminHelper().UpdateDeleteComponentModel(name, id, isUpdate, componentId, componentManufacturerId, fileName, uniqueNameForURL, fileSize, mimeType);
            return PartialView("_ComponentModelPartial", response);
        }



        public ActionResult AddAircraftModel(string name, int makeId)
        {
            var response = new List<ListItems>();
            response = new AdminHelper().AddAircraftModel(name, makeId);
            ViewBag.MakeId = makeId;
            return PartialView("_AircraftModelPartial", response);
        }




        public ActionResult AddComponentModel(FormCollection formCollection)
        {
            string uniqueName = string.Empty;
            string uniqueNameForURL = "";
            int fileSize = 0;
            string mimeType = "";
            if (System.Web.HttpContext.Current.Request.Form["IsFileAvailable"].ToString() == "Yes")
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                uniqueName = System.Web.HttpContext.Current.Request.Form["FileUniqueName"].ToString();
                fileSize = Convert.ToInt32(pic.ContentLength);
                uniqueNameForURL = System.Web.HttpContext.Current.Request.Form["UniqueNameForURL"].ToString();
                new Misc().SaveFileToS3Bucket(uniqueNameForURL, pic.InputStream, "Doc");
                //pic.SaveAs(ConfigurationReader.DocumentPathKey + @"\" + uniqueName);
                mimeType = pic.ContentType;
            }

            int CompManuId = Convert.ToInt32(formCollection["CompManuId"]);

            var aircraftComponentModel = new AircraftComponentModel()
            {
                Name = formCollection["modelName"],
                AircraftComponentManufacturerId = Convert.ToInt32(formCollection["CompManuId"]),
                AircraftComponentId = Convert.ToInt32(formCollection["ComponentId"]),
                Deleted = false,
                LastUpdateDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow,
                UserMannual = formCollection["FileUniqueName"],
                FileSize = fileSize,
                MimeType = mimeType,
                UniqueName = uniqueNameForURL
            };

            var response = new GetConstants();
            response = new AdminHelper().AddComponentModel(aircraftComponentModel);
            var response1 = new List<ComponentModel_ConfigSetting>();
            var context = new GuardianAvionicsEntities();
            ViewBag.CompManuId = CompManuId;
            ViewBag.ComponentList = context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
            {
                text = s.ComponentName,
                value = s.Id,
            }).ToList();

            response1 = new AdminHelper().GetComponentModelList(CompManuId);
            return PartialView("_ComponentModelPartial", response1);
        }

        public ActionResult GetModelsByManufacturerId(int makeId)
        {
            var response = new GetConstants();
            response.AircraftModel = new AircraftHelper().GetAircraftModelsByManufactureId(makeId);
            ListItems obj = new ListItems();
            response.AircraftModel = response.AircraftModel.Where(a => a.value != 0 && a.value != -1).ToList();
            return PartialView("_AircraftModel", response);
        }

        public ActionResult GetComponentModelsByManufacturerId(int makeId, string componentName)
        {
            var response = new GetConstants();
            //response.ComponentModel = new AircraftHelper().GetComponentModelsByManufactureId(makeId, componentName);
            //ListItems obj = new ListItems();
            //response.ComponentModel = response.ComponentModel.Where(a => a.Value != 0).ToList();
            return PartialView("_ComponentModel", response);
        }

        [HttpPost]
        public ActionResult UploadComponentModelFile()
        {
            //if (Request.Files.Count > 0)
            //{
            //    HttpPostedFileBase file = Request.Files[0];
            //    string fileName = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
            //    SaveComponentModelFile(file, fileName);
            //}

            var context = new GuardianAvionicsEntities();
            //if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())

            //{
            string uniqueName = string.Empty;
            string uniqueNameForURL = string.Empty;
            int fileSize = 0;
            string mimeType = "";
            if (System.Web.HttpContext.Current.Request.Form["IsFileAvailable"].ToString() == "Yes")
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                uniqueName = System.Web.HttpContext.Current.Request.Form["FileUniqueName"].ToString();
                fileSize = Convert.ToInt32(pic.ContentLength);
                uniqueNameForURL = System.Web.HttpContext.Current.Request.Form["UniqueNameForURL"].ToString();
                //pic.SaveAs(ConfigurationReader.DocumentPathKey + @"\" + uniqueName);
                new Misc().SaveFileToS3Bucket(uniqueNameForURL, pic.InputStream, "Doc");
                mimeType = pic.ContentType;
            }

            string modelName = System.Web.HttpContext.Current.Request.Form["modelName"].ToString();
            int id = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["Id"]);
            int ComponentId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["ComponentId"]);
            int CompManuId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["CompManuId"]);

            return UpdateDeleteComponentModel(modelName, id, true, ComponentId, CompManuId, uniqueName, uniqueNameForURL, fileSize, mimeType);

            //}
        }

        public bool SaveComponentModelFile(HttpPostedFileBase file, string uniqueName)
        {
            try
            {
                var fileName = file.FileName;
                string fileExt = file.FileName.Split('.')[1];
                string path = string.Empty;
                path = ConfigurationReader.DocumentPathKey + @"\" + uniqueName;
                System.IO.File.Delete(path);
                file.SaveAs(path);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public JsonResult GetComponentManufacturerList()
        {
            AdminHelper obj = new AdminHelper();
            var ModelList = obj.GetComponentManufacturerList();
            return Json(new SelectList(ModelList, "Value", "Text"));
        }

       #endregion ConfigurationSetting

        #region Areo Unit

        public ActionResult AeroUnits()
        {
            var adminHelper = new AdminHelper();
            //ViewBag.LinkedUnlinked = adminHelper.GetLinkedUnlinkedUnitRatio();
            var aeroUnitsModel = adminHelper.GetAeroUnitsDetails();
            return View("AeroUnits", aeroUnitsModel);
        }

        /// <summary>
        /// Gets the list of all the units which are not linked
        /// </summary>
        /// <returns></returns>
        //[SessionExpireFilter]
        public ActionResult GetUnlinkedAeroUnits()
        {
            var adminHelper = new AdminHelper();
            //ViewBag.LinkedUnlinked = adminHelper.GetLinkedUnlinkedUnitRatio();

            var unlinkedAeroUnits = adminHelper.GetUnlinkedAeroUnits();
            return PartialView("GetUnlinkedAeroUnits", unlinkedAeroUnits);
        }

        public ActionResult GetLinkedAeroUnits()
        {
            var adminHelper = new AdminHelper();
            //ViewBag.LinkedUnlinked = adminHelper.GetLinkedUnlinkedUnitRatio();

            var linkedAeroUnits = adminHelper.GetLinkedAeroUnits();
            return PartialView("GetLinkedAeroUnits", linkedAeroUnits);
        }

        public ActionResult GetLPreviouslyLnkedAeroUnits()
        {
            var adminHelper = new AdminHelper();

            var prevlinkedAeroUnits = adminHelper.GetPreviouslyLinkedAeroUnits();
            return PartialView("GetLPreviouslyLnkedAeroUnits", prevlinkedAeroUnits);
        }

        /// <summary>
        /// Get all unlinked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        //[SessionExpireFilter]
        public ActionResult UnlinkedUnitList(string keyword)
        {
            var adminHelper = new AdminHelper();
            string[] list = adminHelper.UnlinkedUnitList(keyword);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get all linked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        //[SessionExpireFilter]
        public ActionResult LinkedUnitList(string keyword)
        {
            var adminHelper = new AdminHelper();
            string[] list = adminHelper.LinkedUnitList(keyword);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get all Previously linked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        //[SessionExpireFilter]
        public ActionResult PreviouslyLinkedUnitList(string keyword)
        {
            var adminHelper = new AdminHelper();
            string[] list = adminHelper.PreviuoslyLinkedUnitList(keyword);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        ///// <summary>
        ///// Gets the list of units which are not linked and matches the keyword
        ///// </summary>
        ///// <param name="keyword"></param>
        ///// <param name="pageNo"></param>
        ///// <returns></returns>        
        ////[SessionExpireFilter]
        //public ActionResult SearchUnlinkedUnits(string keyword, int? pageNo)
        //{
        //    return PartialView("_UnLinkedUnitDetailsTable", new AdminHelper().GetUnLinkedUnitDetailsByKeyWord(keyword).ToPagedList(pageNumber: pageNo ?? 1, pageSize: Constants.PageSizeTable));
        //}

        public ActionResult DeleteAeroUnits(string aeroUnitNo)
        {
            var adminHelper = new AdminHelper();
            var unlinkedAeroUnits = adminHelper.DeleteAeroUnits(aeroUnitNo);
            return PartialView("_ProductionUnitPartial", unlinkedAeroUnits);
        }

        public ActionResult UnBlockedAeroUnit(string aeroUnitNo)
        {
            var adminHelper = new AdminHelper();
            var previouslyLinkedUnit = adminHelper.UnBlockedAeroUnit(aeroUnitNo);
            return PartialView("_PreLinkedUnitPartial", previouslyLinkedUnit);

        }

        public ActionResult BlockAeroUnit(int aeroUnitId, string reason)
        {
            var adminHelper = new AdminHelper();
            var linkedAeroUnits = adminHelper.BlockAeroUnit(aeroUnitId, reason);
            return PartialView("_LinkedUnitPartial", linkedAeroUnits);
        }
        public ActionResult ChangeAeroUnit(int aeroUnitId, int reason, string newSrNo, string comment)
        {
            var adminHelper = new AdminHelper();
            
            var result = adminHelper.ChangeAeroUnit(aeroUnitId, reason, newSrNo, comment);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SaveDocument()
        {
            string fileName = "";
            int fileSizeInKB = 0;
            string mimeType = "";
            string uniqueName = "";
            var context = new GuardianAvionicsEntities();

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                fileSizeInKB = Convert.ToInt32(pic.ContentLength);
                uniqueName = System.Web.HttpContext.Current.Request.Form["UniqueName"].ToString();
                fileName = pic.FileName;
                new Misc().SaveFileToS3Bucket(uniqueName, pic.InputStream, "Doc");
                // pic.SaveAs(ConfigurationReader.DocumentPathKey + @"\" + uniqueName);

                mimeType = pic.ContentType;

            }
            List<DocumentModelWeb> response;

            string title = System.Web.HttpContext.Current.Request.Form["Title"].ToString();

            var doc = new Document();
            doc.AircraftId = null;
            doc.Deleted = false;
            doc.IsForAll = true;
            doc.LastUpdated = DateTime.UtcNow;
            doc.ProfileId = 0;
            doc.Url = uniqueName;
            doc.FileName = fileName;
            doc.FileSize = fileSizeInKB;
            doc.Title = title;
            doc.MimeType = mimeType;
            context.Documents.Add(doc);
            context.SaveChanges();


            response = new List<DocumentModelWeb>();
            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath;
            var docs = context.Documents.Where(d => d.IsForAll && !d.Deleted && d.FileSize != null).ToList();
            var adminHelper = new AdminHelper();
            if (docs.Count != 0)
            {
                int count = docs.Count;
                foreach (var d in docs)
                {
                    response.Add(new DocumentModelWeb
                    {
                        //Deleted = d.Deleted,
                        SNO = count,
                        Id = d.Id,
                        LastUpdateDate = Misc.GetStringOfDate(d.LastUpdated),
                        IconUrl = adminHelper.GetIconURL(d.FileName, d.MimeType),
                        Name = d.FileName,
                        IsForAll = d.IsForAll,
                        CanDeleteDocument = (!d.IsForAll && d.AircraftId == null),
                        Title = d.Title,
                        FileSize = adminHelper.ToFileSize(Convert.ToInt64(d.FileSize ?? 0)),
                        Url = path + d.Url
                    });
                    count = count - 1;
                }
            }

            return PartialView("_FlightBagPartial", response);
        }



        public ActionResult DeleteDocumentForAll(int id)
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<DocumentModelWeb>();
            var doc = context.Documents.FirstOrDefault(f => f.Id == id);
            var mapAdminDocAndUSer = context.AdminDocsDeletedFromUsers.Where(f => f.DocumentId == id).ToList();
            if (mapAdminDocAndUSer != null)
            {
                context.AdminDocsDeletedFromUsers.RemoveRange(mapAdminDocAndUSer);
            }
            //var mapDocAndUser = context.AdminDocsDeletedFromUsers
            new Misc().DeleteFileFromS3Bucket(doc.Url, "Doc");

            context.Documents.Remove(doc);
            context.SaveChanges();
            var path = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath;
            var adminHelper = new AdminHelper();
            var docs = context.Documents.Where(d => d.IsForAll && !d.Deleted && d.FileSize != null).ToList();

            if (docs.Count != 0)
            {
                int count = docs.Count;
                foreach (var d in docs)
                {
                    response.Add(new DocumentModelWeb
                    {

                        //Deleted = d.Deleted,
                        SNO = count,
                        Id = d.Id,
                        LastUpdateDate = Misc.GetStringOfDate(d.LastUpdated),
                        IconUrl = adminHelper.GetIconURL(d.FileName, d.MimeType),
                        Name = d.FileName,
                        IsForAll = d.IsForAll,
                        Title = d.Title,
                        FileSize = adminHelper.ToFileSize(Convert.ToInt64(d.FileSize)),
                        CanDeleteDocument = (!d.IsForAll && d.AircraftId == null),
                        Url = path + d.Url
                    });
                    count = count - 1;
                }
            }
            else
            {
                response = new List<DocumentModelWeb>();
            }
            return PartialView("_FlightBagPartial", response);
        }

        #endregion Areo Unit

        public ActionResult HardwareFirmWareUpdate()
        {
            return View(new AdminHelper().GetHardwareFirmwareFileList());
        }

        public ActionResult GetHardwareFirmWareFiles()
        {
            return PartialView("_HardwareFirmwareFile", new AdminHelper().GetHardwareFirmwareFileList());
        }

        [HttpPost]
        public ActionResult UploadHardwareFirmwareFile()  
        {
            var context = new GuardianAvionicsEntities();
            
                var hardwareFirmwareFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
               string  filename = System.Web.HttpContext.Current.Request.Form["filename"].ToString();
                string version = System.Web.HttpContext.Current.Request.Form["version"].ToString();
                string versionSummary = System.Web.HttpContext.Current.Request.Form["versionSummary"].ToString();
                hardwareFirmwareFile.SaveAs(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename);
                ZipFile objZip = new ZipFile();
                objZip.AddFile(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename, string.Empty);
                objZip.Save(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename.Replace(".syn",".zip"));

                new Misc().UploadFile(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename.Replace(".syn", ".zip"), filename.Replace(".syn", ".zip"), "HardWareFirmware");

                System.IO.File.Delete(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename);
                System.IO.File.Delete(ConfigurationReader.HardwareFirmwarePathKey + @"\" + filename.Replace(".syn", ".zip"));

            var hardwareFirmwarefile = new HardwareFirmwareFile
            {
                FileName = filename,
                Version = version,
                VersionSummary = versionSummary,
                IsDeleted = false,
                CreateDate = DateTime.UtcNow,
                ZipFileName = filename.Replace(".syn", ".zip"),
                IsLatestVersion = true
            };
            var adminHelper = new AdminHelper();
            GeneralResponse response = adminHelper.SavehardwareFirmwareData(hardwareFirmwarefile);
            return Json(new { response }, JsonRequestBehavior.AllowGet);

           
        }

        public ActionResult UpdateHardwareFirmwareFile(HardwareFirmwareUpdateModel model)
        {
            var adminHelper = new AdminHelper();
            GeneralResponse response = adminHelper.UpdateHardwareFirmwareFile(model);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult DeleteFirmwareFile(int id)
        {
            var adminHelper = new AdminHelper();
            GeneralResponse response = adminHelper.DeleteFirmwareFile(id);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetChartFileProgressBarValue(string key)
        {
            int totalPercent = 0;
            if (ChartProgressBar.dictionary != null)
            {
                if (ChartProgressBar.dictionary.ContainsKey(key))
                {
                    totalPercent = Convert.ToInt32(ChartProgressBar.dictionary[key]) < 1 ? 1 : Convert.ToInt32(ChartProgressBar.dictionary[key]);
                    string msg = totalPercent.ToString() + "^" + ChartProgressBar.strResponse.ToString();
                    ChartProgressBar.strResponse.Clear();
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("1^", JsonRequestBehavior.AllowGet);


        }

        public void UploadFile(HttpContext httpContext)
        {
            new UploadAllChartFiles().UploadFile(httpContext);

        }

        public ActionResult ResetKeyAndGetChartDetails()
        {
            ChartProgressBar.dictionary.Clear();
            return GetChartDetails();
        }

        public ActionResult GetChartDetails()
        {
            AdminHelper obj = new AdminHelper();
            return PartialView("_ChartFilePartial", obj.GetChartDetails());
        }

        public ActionResult DeleteChartFile(int id)
        {
            AdminHelper obj = new AdminHelper();
            obj.DeleteChartFile(id);
            return PartialView("_ChartFilePartial", obj.GetChartDetails());
        }

        public ActionResult Dashboard()
        {
            ViewBag.PageHeaderTitle = "Dashboard";
            return View("Dashboard", new AdminHelper().GetDashboardDetails());
        }


        public ActionResult UserSubscriptionList()
        {
            ViewBag.PageHeaderTitle = "User Subscription List";
            PaypalPaymentHelper obj = new PaypalPaymentHelper();
            Misc misc = new Misc();
            var subscriptionList = obj.GetSubscriptionListAllUser();
            subscriptionList.ForEach(f => f.ProfileId = misc.Encrypt(f.ProfileId));
            return View(subscriptionList);
        }

        public ActionResult ReceiptList(string profileId)
        {
            if (Session["UserType"] != null)
            {
                if (Session["UserType"].ToString() == "Admin")
                {
                    ViewBag.IsAdmin = true;
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.PageHeaderTitle = "Subscription Details";
            PaypalPaymentHelper obj = new PaypalPaymentHelper();

            return View(obj.GetAllReceiptBySubProfileId(Convert.ToInt32(new Misc().Decrypt(profileId))));
        }

        public ActionResult AddSubscription()
        {
            AddSubscriptionViewModel model = new AddSubscriptionViewModel();
            model.FeatureList = new List<GA.DataTransfer.Classes_for_Services.FeatureMasterModel>();
            model.FeatureList = new AdminHelper().GetFeatureList().Select(s => new GA.DataTransfer.Classes_for_Services.FeatureMasterModel
            {
                GroupType = s.GroupType,
                Id = s.Id,
                IsEnable = false,
                Name = s.Name
            }).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddSubscription(AddSubscriptionViewModel obj)
        {

            int result = new AdminHelper().SetSubscription(obj);

            return RedirectToAction("Subscription", "Admin");
        }

        public ActionResult Subscription()
        {
            if (Session["UserType"] != null)
            {
                if (Session["UserType"].ToString() == "Admin")
                {
                    ViewBag.IsAdmin = true;
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            var subTableModel = new UserHelper().GetSubscriptionWeb();
            return View(subTableModel);

        }

        public ActionResult AircraftList()
        {
            Misc objMisc = new Misc();
            ViewBag.UserType = Session["UserType"].ToString();
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            int sno = 1;
            response.OrderByDescending(o => o.AircraftId).ToList().ForEach(f => { f.TId = objMisc.Encrypt(f.AircraftId.ToString()); f.SNO = sno++; f.AircraftId = 0; });
            return View(response);
        }

        public ActionResult AircraftListofCheckList()
        {
            Misc objMisc = new Misc();
            ViewBag.UserType = Session["UserType"].ToString();
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            int sno = 1;
            response.OrderByDescending(o => o.AircraftId).ToList().ForEach(f => { f.TId = objMisc.Encrypt(f.AircraftId.ToString()); f.SNO = sno++; f.AircraftId = 0; });
            return View(response);
        }

        [HttpGet]
        public ActionResult RegisterAircraft()
        {
            if (Session["UserType"] != null)
            {
                ViewBag.User = Session["UserType"].ToString();

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Id = 0;

            // by default set the default aircraft profile image.
            ViewBag.AircraftProfileImageSource = Constants.AircraftDefaultImage;

            ViewBag.Id = 0;

            var helper = new AircraftHelper();
            var AircraftData = new AircraftProfileModelWeb();
            AircraftData = helper.GetEmptyAircraftDetail(Convert.ToInt32(Session["ProfileId"]));
            ViewBag.AircraftComponentManufacturerList = AircraftData.AircraftComponentManufacturerList;
            ViewBag.ComponentModelList = AircraftData.ComponentModelList;
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("RegisterAircraft", AircraftData);
        }

        [HttpPost]
        public ActionResult RegisterAircraft(AircraftProfileModelWeb aircraft)
        {
            
            if (ModelState.IsValid)
            {
               var response = new AircraftHelper().CreateAircraft(aircraft, 0);
                if (response.ResponseCode == "0")
                {
                    response.ResponseMessage = "Aircraft profile created successfully";
                }
                return Json(new { response = response.ResponseMessage });
            }
            return View("RegisterAircraft");
        }


        #region IPassenger
        public ActionResult IPassenger()
        {
            IPassengerHelper objPassengerHelper = new IPassengerHelper();
            var obj = objPassengerHelper.GetIPassengerMediaDetails(0);
            return View(obj);
        }



        [HttpPost]
        public ActionResult UploadIPassengerVideoFile() //Rename UploadPassengerProMapFile to UploadFMSAndPassengerProMapFile
        {
            IPassengerMediaDetails objResponse = new IPassengerMediaDetails();
            try
            {
                string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                var videoFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
                fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") + "." + fileName.Split('.')[1];
                string videoTitle = System.Web.HttpContext.Current.Request.Form["VideoTitle"].ToString();

                var path = ConfigurationReader.IPassengerMediaPath + @"\Video\" + fileName;
                decimal fileSize = decimal.Round((((decimal)videoFile.ContentLength / 1024)/1024), 3);
                videoFile.SaveAs(path);
                new Misc().SaveFileToS3Bucket(fileName, videoFile.InputStream, "IPassVideo");
                string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\Video\" + fileName.Split('.')[0] + "thumbnail.jpg";

                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                ffMpeg.GetVideoThumbnail(path, thumbnailImagePath, 1);
                new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassVideo");
                try
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(thumbnailImagePath);
                }
                catch (Exception ex)
                {

                }
                AircraftHelper obj = new AircraftHelper();
                objResponse = obj.AddIPassengerVideo(videoTitle, fileName, 0,fileSize ,  true);
                if (objResponse.ResponseMessage == "Success")
                {
                    ViewBag.IsAddedSuccessfully = "true";
                }
                else
                {
                    ViewBag.IsAddedSuccessfully = "false";
                }
            }
            catch (Exception ex)
            {
                ViewBag.IsAddedSuccessfully = "false";
                return PartialView("_IPassengerVideoAdmin", new IPassengerMediaDetails { VideoList = new List<MediaDetails>() });
            }
            return PartialView("_IPassengerVideoAdmin", objResponse);
        }

        [HttpPost]
        public ActionResult UploadIPassengerImageFile()
        {

            var imageFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
            fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") +"." + fileName.Split('.')[1]; 
            string imageTitle = System.Web.HttpContext.Current.Request.Form["ImageTitle"].ToString();

            var path = ConfigurationReader.IPassengerMediaPath + @"\Image\" + fileName;
            decimal fileSize = decimal.Round((((decimal)imageFile.ContentLength / 1024) / 1024), 3);
            imageFile.SaveAs(path);
            new Misc().SaveFileToS3Bucket(fileName, imageFile.InputStream, "IPassImage");
            string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\Image\" + fileName.Split('.')[0] + "thumbnail.jpg";
            CreateThumbnail(50, path, thumbnailImagePath);
            new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassImage");
            try
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(thumbnailImagePath);
            }
            catch (Exception ex)
            {

            }
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerImage(imageTitle, fileName, 0,fileSize ,true);
            return PartialView("_IPassengerImageAdmin", objResponse);

        }

        [HttpPost]
        public ActionResult UploadIPassengerPDFFile()
        {

            var pdfFile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            string fileName = System.Web.HttpContext.Current.Request.Form["fileUniqueName"].ToString();
            fileName = DateTime.Now.ToString("yyyyMMddHHmmssf") + "." + fileName.Split('.')[1];
            string pdfTitle = System.Web.HttpContext.Current.Request.Form["PDFTitle"].ToString();

            var path = ConfigurationReader.IPassengerMediaPath + @"\PDF\" + fileName;
            decimal fileSize = decimal.Round((((decimal)pdfFile.ContentLength / 1024) / 1024), 3);
            pdfFile.SaveAs(path);
            new Misc().SaveFileToS3Bucket(fileName, pdfFile.InputStream, "IPassPDF");
            string thumbnailImagePath = ConfigurationReader.IPassengerMediaPath + @"\PDF\" + fileName.Split('.')[0] + "thumbnail.jpg";
            SavePDFThumbnail(path, thumbnailImagePath);
            new Misc().UploadFile(thumbnailImagePath, fileName.Split('.')[0] + "thumbnail.jpg", "IPassPDF");
            try
            {
                System.IO.File.Delete(path);
                System.IO.File.Delete(thumbnailImagePath);
            }
            catch (Exception ex)
            {

            }
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerPDF(pdfTitle, fileName, 0,fileSize ,true);
            return PartialView("_IpassengerPDFAdmin", objResponse);
        }

        public ActionResult UpdateDeleteIPassengerVideo(string title, string id, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            var response = new AircraftHelper().UpdateDeleteIPassengerVideo(title, Convert.ToInt32(id), isUpdate, 0, true);
            return PartialView("_IPassengerVideoAdmin", response);
        }


        void CreateThumbnail(int ThumbnailMax, string OriginalImagePath, string ThumbnailImagePath)
        {
            // Loads original image from file
            System.Drawing.Image imgOriginal = System.Drawing.Image.FromFile(OriginalImagePath);
            // Finds height and width of original image
            float OriginalHeight = imgOriginal.Height;
            float OriginalWidth = imgOriginal.Width;
            // Finds height and width of resized image
            int ThumbnailWidth;
            int ThumbnailHeight;
            if (OriginalHeight > OriginalWidth)
            {
                ThumbnailHeight = ThumbnailMax;
                ThumbnailWidth = (int)((OriginalWidth / OriginalHeight) * (float)ThumbnailMax);
            }
            else
            {
                ThumbnailWidth = ThumbnailMax;
                ThumbnailHeight = (int)((OriginalHeight / OriginalWidth) * (float)ThumbnailMax);
            }
            // Create new bitmap that will be used for thumbnail
            System.Drawing.Bitmap ThumbnailBitmap = new System.Drawing.Bitmap(ThumbnailWidth, ThumbnailHeight);
            System.Drawing.Graphics ResizedImage = System.Drawing.Graphics.FromImage(ThumbnailBitmap);
            // Resized image will have best possible quality
            ResizedImage.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            ResizedImage.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ResizedImage.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            // Draw resized image
            ResizedImage.DrawImage(imgOriginal, 0, 0, ThumbnailWidth, ThumbnailHeight);
            // Save thumbnail to file
            ThumbnailBitmap.Save(ThumbnailImagePath);
        }

        public void SavePDFThumbnail(string pdfFilePath, string thumbnailImgPath)
        {
            TallComponents.PDF.Rasterizer.Document pdfDocument = new TallComponents.PDF.Rasterizer.Document(new FileStream(pdfFilePath, FileMode.Open, FileAccess.Read));
            TallComponents.PDF.Rasterizer.Page pdfPage = pdfDocument.Pages[0];

            //create a bitmap to draw to and a graphics object
            using (Bitmap bitmap = new Bitmap((int)pdfPage.Width, (int)pdfPage.Height))
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                //draw the image from the first page to the graphics object, which is connected to the bitmap object
                pdfPage.Draw(graphics);
                bitmap.Save(thumbnailImgPath);
            }

        }

        public ActionResult UpdateDeleteIPassengerImage(string title, string id, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerImage(title, Convert.ToInt32(id), 0, isUpdate, true);
            return PartialView("_IPassengerImageAdmin", objResponse);
        }

        public ActionResult UpdateDeleteIPassengerPDF(string title, string id, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerPDF(title, Convert.ToInt32(id), 0, isUpdate, true);
            return PartialView("_IPassengerPDFAdmin", objResponse);
        }

        public ActionResult AddIPassengerWarning(string title)
        {
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerWarning(title, 0, true);
            if (objResponse.ResponseMessage == "Success")
            {
                ViewBag.IsAddedSuccessfully = "true";
            }
            else
            {
                ViewBag.IsAddedSuccessfully = "false";
            }
            return PartialView("_IPassengerWarningAdmin", objResponse);

        }

        public ActionResult UpdateDeleteIPassengerWarning(string title, string id, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerWarning(title, Convert.ToInt32(id), 0, isUpdate, true);
            return PartialView("_IPassengerWarningAdmin", objResponse);
        }

        public ActionResult AddIPassengerInstruction(string title)
        {
            AircraftHelper obj = new AircraftHelper();
            IPassengerMediaDetails objResponse = obj.AddIPassengerInstruction(title, 0, true);
            if (objResponse.ResponseMessage == "Success")
            {
                ViewBag.IsAddedSuccessfully = "true";
            }
            else
            {
                ViewBag.IsAddedSuccessfully = "false";
            }
            return PartialView("_IPassengerInstructionAdmin", objResponse);
        }

        //public ActionResult UpdateDeleteIPassengerInstruction(string title, int id, int aircraftId, bool isUpdate)
        public ActionResult UpdateDeleteIPassengerInstruction(string title, string id, bool isUpdate)
        {
            try
            {
                id = new Misc().Decrypt(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

            IPassengerMediaDetails objResponse = new AircraftHelper().UpdateDeleteIPassengerInstruction(title, Convert.ToInt32(id), 0, isUpdate, true);
            return PartialView("_IPassengerInstructionAdmin", objResponse);
        }

        #endregion IPassenger

        #region ErrorLog
        public ActionResult ErrorLogs()
        {
            return View(new AdminHelper().GetErrorLogs());
        }

        public JsonResult GetData(string query)
        {
            List<string> list = new List<string> { "atul@ideavate.com", "bhoopesh@ideavate.com", "bhopal@ideavate.com", "anand@ideavate.com" };
            list = list.Where(w => w.Contains(query)).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchErrorLog(string emailId, string startDate, string endDate, string status)
        {
            AdminHelper adminHelper = new AdminHelper();
            DateTime? dtStartDate = string.IsNullOrEmpty(startDate) ? (DateTime?)null : Convert.ToDateTime(startDate);
            DateTime? dtEndtDate = string.IsNullOrEmpty(endDate) ? (DateTime?)null : Convert.ToDateTime(endDate);
           var errorlogList = adminHelper.SearchErrorLog(emailId, dtStartDate, dtEndtDate, status);
            return PartialView("_ErrorLogs", errorlogList);
        }

        public JsonResult GetErrorLogById(int id)
        {
            ErrorLogModel errorLogModel = new ErrorLogModel();
            AdminHelper adminHelper = new AdminHelper();
            errorLogModel = adminHelper.GetErrorLogById(id);
            return Json(errorLogModel, JsonRequestBehavior.AllowGet);
        }

        #endregion ErrorLog

    }
}
