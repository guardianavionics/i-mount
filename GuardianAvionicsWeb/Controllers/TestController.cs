﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GA.DataLayer;
using System.Collections.Generic;
using GA.Common;

namespace GuardianAvionicsWeb.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult SetIpassengerFileSize()
        {
            bool isFileExist = true;
            Misc misc = new Misc();
           
            var context = new GuardianAvionicsEntities();
            var ipassengerList = context.IPassengers.Where(w => w.FileName != "").ToList();
            foreach (var ipassenger in ipassengerList)
            {
                ipassenger.FileSize = misc.GetFileSizeFromS3BucketFile("IPassVideo", ipassenger.FileName);
            }
            context.SaveChanges();
            var deleteList = context.IPassengers.Where(w => w.FileName != "" && w.FileSize == null).ToList();
            var ipassengerIds = deleteList.Select(s => s.Id).ToList();
           var deletedByUsers =  context.IPassengerDetailsDeletedByUsers.Where(w => ipassengerIds.Contains(w.IPassengerId)).ToList();
            context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedByUsers);
            context.IPassengers.RemoveRange(deleteList);
            context.SaveChanges();

            return View();
        }


      
    }
}
