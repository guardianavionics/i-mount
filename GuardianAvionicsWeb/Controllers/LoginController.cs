﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GA.DataLayer;
using GA.DataTransfer;
using GA.ApplicationLayer;

namespace GuardianAvionicsWeb.Controllers
{
    public class LoginController : Controller
    {
        GuardianAvionicsEntities objDA = new GuardianAvionicsEntities();
        //
        // GET: /Login/

        public ActionResult Index()
        {

            return View("Login");
        }

        [HttpPost]
        public ActionResult Index(/*string EmailId,string Password*/ Profile _profile)
        {
            // bool isValid = objDA.LoginUser(_profile);

            //if (isValid)
            //{

            var pr = objDA.Profiles
                .Select(
                profile => profile.EmailId == _profile.EmailId
                    && profile.Password == _profile.Password);

            if (pr.ToList().Count() == 0)
            {
                ModelState.AddModelError("", "Username or Password does not exists.");
                return View("Login");
            }
            else
            {
                return RedirectToAction("ListAllJPIData", "Aircraft");
            }
        }

        public ActionResult LoginUser()//Profile objProfile)
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser( LoginModel objLogin )//Profile objProfile)
        {
            return View();
        }

    }
}
