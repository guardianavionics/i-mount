﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GA.ApplicationLayer;
using GA.DataTransfer.Classes_for_Web;
using Newtonsoft.Json;

namespace GuardianAvionicsWeb.Controllers
{
    public class QuickBloxController : Controller
    {
        //
        // GET: /QuickBlox/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllUserToLink()
        {
            List<string> quickbloxIdList = new UserHelper().GetAllAircraftByProfileId(Convert.ToInt32(Session["ProfileId"]));
            return Json(new { quickbloxIdList =  (quickbloxIdList) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetUserQuickBloxId(string quickbloxId)
        {
            new UserHelper().SetUserQuickBloxId(Convert.ToInt32(Session["ProfileId"]), quickbloxId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUser()
        {
            UserInfo user = new UserHelper().GetUserById(Convert.ToInt32(Session["ProfileId"]));
            return Json(new { user }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Chat()
        {           
            return View();
        }

        public ActionResult GetUnreadMessageCount()
        {
            GA.DataTransfer.Classes_for_Web.UserInfo user = new GA.ApplicationLayer.UserHelper().GetUserById(Convert.ToInt32(Session["ProfileId"]));
            var unreadMessageCount = new GA.ApplicationLayer.QuickBloxHelper(user.EmailId).GetToken();
            ViewBag.UnreadMsg = unreadMessageCount;
            return Json(new { unreadMessageCount }, JsonRequestBehavior.AllowGet);
        }
    }
}
