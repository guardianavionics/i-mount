﻿using GA.ApplicationLayer;
using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuardianAvionicsWeb.Controllers
{
    public class CheckListController : Controller
    {
        // GET: CheckList
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AircraftListofCheckList()
        {
            Session["UserAuthorization"] = new UserHelper().SetFeatureByUser(Convert.ToInt32(Session["ProfileId"]));
            if (Session["UserType"] == null || Session["ProfileId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.UserType = Session["UserType"].ToString();
            Misc objMisc = new Misc();
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(Convert.ToInt32(Session["ProfileId"]));
            ViewBag.PageHeaderTitle = "Aircraft List";
            AircraftHelper helper = new AircraftHelper();
            //List<AircraftDetail> response = (List<AircraftDetail>)helper.setViewBagAircraftListing(Convert.ToInt32(Session["ProfileId"]));
            //response.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            var response = helper.GetAircraftList(Convert.ToInt32(Session["ProfileId"]));
            int sno = 1;
            response.OrderByDescending(o => o.AircraftId).ToList().ForEach(f => { f.TId = objMisc.Encrypt(f.AircraftId.ToString()); f.SNO = sno++; });
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(Convert.ToInt32(Session["ProfileId"]));
            if (!isCurrSubscriptionIsPlan5AndActive && Session["UserType"].ToString() != Enumerations.UserType.Maintenance.ToString())
            {
                sno = 1;
                int aircraftId = helper.GetFirstAircraftRegisteredByProfileId(Convert.ToInt32(Session["ProfileId"]));
                response.Where(w => w.AircraftId != aircraftId).ToList().ForEach(f => { f.AircraftId = 0; f.TId = null; });
                response.OrderByDescending(o => o.TId).ToList().ForEach(f => f.SNO = sno++);
            }
            return View("AircraftListofCheckList", response);
        }
        public ActionResult CheckList(string aId, int? flag = 0)
        {
            try
            {
                
                if (string.IsNullOrEmpty(aId) || Session["ProfileId"] == null || Session["UserType"] == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    aId = new Misc().Decrypt(aId);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
            var AircraftData = new AircraftProfileModelWeb();
            ViewBag.Master = "_UserLayout.cshtml";
            if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString() || flag == 1)
            {
                ViewBag.Master = "_Layout.cshtml";
            }

            //Validate Aircraft with user
            ViewBag.User = Session["UserType"].ToString();

            
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            var profile = new UserHelper().GetProfileDetailById(profileId);

            // aId == 0 is case of new aircraft creation
            if (Convert.ToInt32(aId) != 0)
            {
                int aircraftId = Convert.ToInt32(aId);
                Misc objMisc = new Misc();
                AircraftHelper helper = new AircraftHelper();
                AircraftData = helper.GetAircraftDetails(aircraftId, profileId);
                GuardianAvionicsEntities context = new GuardianAvionicsEntities();
                ViewBag.CatogaryList = context.Category.Where(x => x.IsActive == true && x.AircraftId == aircraftId).OrderBy(x => x.Priority).ToList(); //&& x.AircraftId== aircraftId
                ViewBag.Id = aircraftId;

                if (profile.UserMaster.UserType == Enumerations.UserType.Admin.ToString())
                {
                    AircraftData.IsOwner = true;
                   
                }
                
                Session["IsOwner"] = AircraftData.IsOwner;
                

            }
            ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
            return View("CheckList");
        }
    }
}