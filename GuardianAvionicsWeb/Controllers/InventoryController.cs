﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GA.ApplicationLayer;
using GA.DataTransfer.Classes_for_Web;
using GA.Common;
using GA.DataTransfer;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using GuardianAvionicsWeb.Filters;
using System.Globalization;
using System.Web.UI;
using System.Net;
using System.Windows.Forms;
using System.Web.UI.WebControls;

namespace GuardianAvionicsWeb.Controllers
{
    [SessionExpireFilter]
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/


        public ActionResult Index()
        {
            ViewBag.UserType = GetUserTypeFromSession();

            return View();
        }




        public JsonResult GetPurchaseOrderInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            //List<Organization> organizations = new List<Organization>();
            List<PurchaseOrderModel> rmaList = new List<PurchaseOrderModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;

            rmaList = new InventoryHelper().GetPurchaseOrderInChunk(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter);

            var response = new { data = rmaList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult PurchaseOrder()
        {
            ViewBag.UserType = GetUserTypeFromSession();
            InventoryHelper inventoryHelper = new InventoryHelper();

            var partNumberList = inventoryHelper.GetPartNameAndDescription();
            ViewBag.PartNumberListTemp = JsonConvert.SerializeObject(partNumberList);

            ViewBag.VendorList = inventoryHelper.GetVendorNameAndId();

            List<ReceiverModel> receiverList = inventoryHelper.GetReceiverList();
            receiverList.Insert(0, new ReceiverModel { Name = "Select Receiver", EncryptId = "" });
            ViewBag.ReceiverList = receiverList.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();

            ViewBag.PageHeaderTitle = "Receive Order";
            return View();
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult GetPurchaseOrder()
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            PurchaseOrderListModel purchaseOrderListModel = new PurchaseOrderListModel();
            purchaseOrderListModel.PurchaseOrderList = inventoryHelper.GetPurchaseOrderList();
            return PartialView("_PurchaseOrderList", purchaseOrderListModel.PurchaseOrderList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult AddPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            if (purchaseOrderModel.IsEditBoardSerialNoAtReceive)
            {
                purchaseOrderModel.BoardNoList = JsonConvert.DeserializeObject<List<BoardSerialAndSW>>(System.Web.HttpContext.Current.Request.Form["BoardNoList"].ToString());
            }

            PurchaseOrderListModel purchaseOrderListModel = new PurchaseOrderListModel();
            purchaseOrderListModel.PurchaseOrderList = new List<PurchaseOrderModel>();
            try
            {
                if (Session["ProfileId"] == null)
                {
                    purchaseOrderListModel.ResponseCode = "-1";
                    purchaseOrderListModel.ResponseMessage = "Session expired, please logout and relogin.";
                    return PartialView("_PurchaseOrderList", purchaseOrderListModel.PurchaseOrderList);
                }
                InventoryHelper inventoryHelper = new InventoryHelper();
                Misc misc = new Misc();

                purchaseOrderModel.PartNo = purchaseOrderModel.PartNo;
                purchaseOrderModel.VendorId = misc.Decrypt(purchaseOrderModel.VendorId);
                purchaseOrderModel.ReceiverId = misc.Decrypt(purchaseOrderModel.ReceiverId);
                purchaseOrderModel.CreatedBy = Convert.ToInt32(Session["ProfileId"]);
                var response = inventoryHelper.AddPurchaseOrder(purchaseOrderModel);
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                purchaseOrderListModel.ResponseCode = "9999";
                purchaseOrderListModel.ResponseMessage = "Exception occurs while inserting the purchase order !!!";
                return PartialView("_PurchaseOrderList", purchaseOrderListModel.PurchaseOrderList);
            }
        }

        public ActionResult AutoComplete(string text)
        {
            return Json(new { });
        }

        public ActionResult ValidatePurchaseOrder(int StartSerialNo, int Quantity, int? StartingBoardSerialNo, string PONumber, string PartNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.ValidatePurchaseOrder(StartSerialNo, Quantity, StartingBoardSerialNo, PONumber.Trim(), PartNo.Trim());
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult EditPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            Misc misc = new Misc();
            try
            {

                purchaseOrderModel.PurchaseOrderId = misc.Decrypt(purchaseOrderModel.PurchaseOrderId);
                purchaseOrderModel.VendorId = misc.Decrypt(purchaseOrderModel.VendorId);
                purchaseOrderModel.ReceiverId = misc.Decrypt(purchaseOrderModel.ReceiverId);
                response = inventoryHelper.EditPurchaseOrder(purchaseOrderModel);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PurchaseOrder.Exception).ToString();
                response.ResponseMessage = Enumerations.PurchaseOrder.Exception.GetStringValue();
            }

            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult DeletePurchaseOrder(string id)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            Misc misc = new Misc();
            try
            {
                int purchaseOrderId = Convert.ToInt32(misc.Decrypt(id));
                response = inventoryHelper.DeletePurchaseOrder(purchaseOrderId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PurchaseOrder.Exception).ToString();
                response.ResponseMessage = Enumerations.PurchaseOrder.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult GetPurchaseOrderById(string id)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            int purchaseOrderId = Convert.ToInt32(new Misc().Decrypt(id.ToString()));
            PurchaseOrderModel response = inventoryHelper.GetPurchaseOrderById(purchaseOrderId);
            return Json(new { response });
        }

        public void PurchaseOrderExportExcel()
        {
            var gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = new InventoryHelper().GetPurchaseOrderList().Select(s => new
            {
                SNO = s.SNO,
                CreateDate = String.Format("{0:d}", s.CreateDate),
                PartNo = s.PartNo,
                Description = s.Description,
                PurchaseOrderNumber = s.PurchaseOrderNumber,
                Quantity = s.Quantity,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable == true ? "Yes" : "No",
                SoftwareLevel = s.SoftwareLevel,
                ReceivedBy = s.ReceivedBy,
                StartSerialNo = s.StartSerialNo,
                EndSerialNo = s.StartSerialNo + (s.Quantity - 1),
                VendorName = s.VendorName,
                StartingBoardSerialNo = s.StartingBoardSerialNo,
                EndBoardSerialNo = s.StartingBoardSerialNo == null ? null : (s.StartingBoardSerialNo + s.Quantity - 1),

            });
            gv.DataBind();
            gv.HeaderRow.BackColor = System.Drawing.Color.Gray;

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=PurchaseOrders.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                GridViewRow row = gv.Rows[i];
                //Apply text style to each Row
                row.Cells[2].Attributes.Add("class", "textmode");
            }
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }

        public ActionResult PurchaseOrderExportPDF()
        {
            var purchaseOrderList = new InventoryHelper().GetPurchaseOrderList().Select(s => new
            {
                SNO = s.SNO,
                CreateDate = String.Format("{0:d}", s.CreateDate),
                PartNo = s.PartNo,
                Description = s.Description,
                PurchaseOrderNumber = s.PurchaseOrderNumber,
                Quantity = s.Quantity,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable == true ? "Yes" : "No",
                SoftwareLevel = s.SoftwareLevel,
                ReceivedBy = s.ReceivedBy,
                StartSerialNo = s.StartSerialNo,
                EndSerialNo = s.StartSerialNo + (s.Quantity - 1),
                VendorName = s.VendorName,
                StartingBoardSerialNo = s.StartingBoardSerialNo,
                EndBoardSerialNo = s.StartingBoardSerialNo == null ? null : (s.StartingBoardSerialNo + s.Quantity - 1),

            });
            var document = new Document(PageSize.A4, 1, 1, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate()); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);


            PdfPCell cellHead = new PdfPCell(new Phrase("Purchase Orders", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellHead.Border = PdfPCell.NO_BORDER;
            cellHead.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellHead);

            PdfPCell cellEmpty = new PdfPCell(new Phrase("", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellEmpty.Border = PdfPCell.NO_BORDER;
            cellEmpty.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellEmpty);

            int tableColumns = 14;
            PdfPTable tblPurchaseOrder = new PdfPTable(tableColumns);
            tblPurchaseOrder.WidthPercentage = 100;
            tblPurchaseOrder.HorizontalAlignment = 0;
            tblPurchaseOrder.SetWidths(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            tblPurchaseOrder.DefaultCell.Padding = 3;
            tblPurchaseOrder.SpacingBefore = 10f;
            tblPurchaseOrder.SpacingAfter = 10f;

            tblPurchaseOrder.HeaderRows = 1;
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Sno", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Create Date", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Po Number", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Part No", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Is s/w Applicable?", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("S/w level", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Vendor", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Received By", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Quantity", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Start Serial No.", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("End Serial No", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Start Board Serial No", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("End Board Serial No", boldTableFont)));

            foreach (var item in purchaseOrderList)
            {
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.SNO.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.CreateDate, normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.PurchaseOrderNumber.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.PartNo, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Description, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.IsSoftwareLevelApplicable, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.SoftwareLevel, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.VendorName, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.ReceivedBy, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Quantity.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.StartSerialNo.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.EndSerialNo.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.StartingBoardSerialNo.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.EndBoardSerialNo.ToString(), normalTableFont)));
            }
            document.Add(tblTitle);
            document.Add(tblPurchaseOrder);
            document.Close();
            return DownloadToClient(output.ToArray(), "PurchaseOrders");
        }

        public void VendorExportExcel()
        {
            var gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = new InventoryHelper().GetVendorList().Select(s => new
            {
                Sno = s.SNO,
                VendorName = s.Name,
                ContactName = s.ContactName,
                PhoneNumber = s.PhoneNumber,
                Email = s.Email,
                Website = s.Website,
                Comments = s.Comments
            });
            gv.DataBind();
            gv.HeaderRow.BackColor = System.Drawing.Color.Gray;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=VendorList.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }

        public ActionResult VendorExportPDF()
        {
            var vendorList = new InventoryHelper().GetVendorList().Select(s => new
            {
                Sno = s.SNO,
                VendorName = s.Name,
                ContactName = s.ContactName,
                PhoneNumber = s.PhoneNumber,
                Email = s.Email,
                Website = s.Website,
                Comments = s.Comments
            });
            var document = new Document(PageSize.A4, 1, 1, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);


            PdfPCell cellHead = new PdfPCell(new Phrase("Vendor List", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellHead.Border = PdfPCell.NO_BORDER;
            cellHead.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellHead);

            PdfPCell cellEmpty = new PdfPCell(new Phrase("", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellEmpty.Border = PdfPCell.NO_BORDER;
            cellEmpty.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellEmpty);

            int tableColumns = 7;
            PdfPTable tblPurchaseOrder = new PdfPTable(tableColumns);
            tblPurchaseOrder.WidthPercentage = 100;
            tblPurchaseOrder.HorizontalAlignment = 0;
            tblPurchaseOrder.SetWidths(new int[] { 1, 1, 1, 1, 1, 1, 1 });
            tblPurchaseOrder.DefaultCell.Padding = 3;
            tblPurchaseOrder.SpacingBefore = 10f;
            tblPurchaseOrder.SpacingAfter = 10f;

            tblPurchaseOrder.HeaderRows = 1;
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Sno", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("VendorName", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("ContactName", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("PhoneNumber", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Email", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Website", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Comments", boldTableFont)));

            foreach (var item in vendorList)
            {
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Sno.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.VendorName, normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.ContactName, normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.PhoneNumber, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Email, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Website, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Comments, normalTableFont)));
            }
            document.Add(tblTitle);
            document.Add(tblPurchaseOrder);
            document.Close();
            return DownloadToClient(output.ToArray(), "VendorList");
        }

        public iTextSharp.text.Image GetImage(string url)
        {
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(new Uri(url));
            return img;
        }

        public void KitAssemblyExportExcel()
        {
            var gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = new InventoryHelper().GetKitAssembly().Select(s => new
            {
                KitPartNumber = s.PartName,
                SuggestedSalesPrice = s.SuggestedSalesPrice,
                Description = s.Description,
                ItemCount = s.PartList.Count(),
                ExtendedSalesPrice = s.PartList.Sum(a => a.SalesPrice * a.Quantity)
            });
            gv.DataBind();
            gv.HeaderRow.BackColor = System.Drawing.Color.Gray;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=KitAssemblyList.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }

        public ActionResult KitAssemblyExportPDF()
        {
            var vendorList = new InventoryHelper().GetKitAssembly().Select(s => new
            {
                KitPartNumber = s.PartName,
                //Image = string.IsNullOrEmpty(s.ImageName) ? GetImage(Server.MapPath("../Images/NoImage.png")) : GetImage(s.ImageName),
                SuggestedSalesPrice = s.SuggestedSalesPrice,
                Description = s.Description,
                ItemCount = s.PartList.Count(),
                ExtendedSalesPrice = s.PartList.Sum(a => a.SalesPrice * a.Quantity)
            });
            var document = new Document(PageSize.A4, 1, 1, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate()); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);


            PdfPCell cellHead = new PdfPCell(new Phrase("Kit Assembly List", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellHead.Border = PdfPCell.NO_BORDER;
            cellHead.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellHead);

            PdfPCell cellEmpty = new PdfPCell(new Phrase("", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellEmpty.Border = PdfPCell.NO_BORDER;
            cellEmpty.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellEmpty);

            int tableColumns = 5;
            PdfPTable tblPurchaseOrder = new PdfPTable(tableColumns);
            tblPurchaseOrder.WidthPercentage = 100;
            tblPurchaseOrder.HorizontalAlignment = 0;
            tblPurchaseOrder.SetWidths(new int[] { 1, 1, 1, 1, 1 });
            tblPurchaseOrder.DefaultCell.Padding = 3;
            tblPurchaseOrder.SpacingBefore = 10f;
            tblPurchaseOrder.SpacingAfter = 10f;

            tblPurchaseOrder.HeaderRows = 1;
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Kit P/N", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Suggested Sales Price", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Item Count", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Extended Sales", boldTableFont)));

            foreach (var item in vendorList)
            {

                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.KitPartNumber, normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.SuggestedSalesPrice.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Description, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.ItemCount.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.ExtendedSalesPrice.ToString(), normalTableFont)));
            }
            document.Add(tblTitle);
            document.Add(tblPurchaseOrder);
            document.Close();
            return DownloadToClient(output.ToArray(), "KitAssemblyList");
        }

        public void PartNoExportExcel()
        {
            var gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = new InventoryHelper().GetPartNumberList().Select(s => new
            {
                Sno = s.SNO,
                PartNumber = s.PartNumber,
                ModelNumber = s.ModelNumber,
                Description = s.Description,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable ? "Yes" : "No",
                CostPrice = s.CostPrice,
                SalesPrice = s.SalesPrice,
                TotalUnitsAvailableForSale = s.TotalUnitsAvailableForSale,
                RecommendQuantity = s.RecommendQuantity,
                VendorName = s.VendorName,
                Comment = s.Comment
            });
            gv.DataBind();
            gv.HeaderRow.BackColor = System.Drawing.Color.Gray;
            foreach (DataGridViewColumn column in gv.Columns)
            {
                column.ValueType = typeof(string);
            }
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=PartList.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                GridViewRow row = gv.Rows[i];
                //Apply text style to each Row
                row.Cells[1].Attributes.Add("class", "textmode");
                row.Cells[2].Attributes.Add("class", "textmode");
            }

            //format the excel cells to text format
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }

        public ActionResult PartNoExportPDF()
        {
            var PartList = new InventoryHelper().GetPartNumberList().Select(s => new
            {
                Sno = s.SNO,
                PartNumber = s.PartNumber,
                ModelNumber = s.ModelNumber,
                Description = s.Description,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable ? "Yes" : "No",
                CostPrice = s.CostPrice,
                SalesPrice = s.SalesPrice,
                RecommendQuantity = s.RecommendQuantity,
                Comment = s.Comment,
                VendorName = s.VendorName,
                TotalUnitsAvailableForSale = s.TotalUnitsAvailableForSale
            });
            var document = new Document(PageSize.A4, 1, 1, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate()); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);


            PdfPCell cellHead = new PdfPCell(new Phrase("Part List", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellHead.Border = PdfPCell.NO_BORDER;
            cellHead.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellHead);

            PdfPCell cellEmpty = new PdfPCell(new Phrase("", headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellEmpty.Border = PdfPCell.NO_BORDER;
            cellEmpty.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellEmpty);

            int tableColumns = 11;
            PdfPTable tblPurchaseOrder = new PdfPTable(tableColumns);
            tblPurchaseOrder.WidthPercentage = 100;
            tblPurchaseOrder.HorizontalAlignment = 0;
            tblPurchaseOrder.SetWidths(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            tblPurchaseOrder.DefaultCell.Padding = 3;
            tblPurchaseOrder.SpacingBefore = 10f;
            tblPurchaseOrder.SpacingAfter = 10f;

            tblPurchaseOrder.HeaderRows = 1;
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Sno", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Part No", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Model No", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Is s/w Applicable?", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Cost Price", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Sales Price", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Qty Available FOr Sale", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Recommended Qty On Shelf", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Vendor", boldTableFont)));
            tblPurchaseOrder.AddCell(new PdfPCell(new Phrase("Comment", boldTableFont)));

            foreach (var item in PartList)
            {
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Sno.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.PartNumber, normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.ModelNumber.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Description, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.IsSoftwareLevelApplicable, normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.CostPrice.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.SalesPrice.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.TotalUnitsAvailableForSale.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.RecommendQuantity.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.VendorName.ToString(), normalTableFont)));
                tblPurchaseOrder.AddCell(new PdfPCell(new Phrase(item.Comment.ToString(), normalTableFont)));
            }
            document.Add(tblTitle);
            document.Add(tblPurchaseOrder);
            document.Close();
            return DownloadToClient(output.ToArray(), "PartNumberList");
        }



        public JsonResult GetPartNumberInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            //List<Organization> organizations = new List<Organization>();
            List<PartNumberModel> customerList = new List<PartNumberModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;
            customerList = new InventoryHelper().GetPartNumberInChunk(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter);

            var response = new { data = customerList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount };
            return Json(response, JsonRequestBehavior.AllowGet);
        }




        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult PartNo(string partNo = "")
        {

            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Part Number";
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<VendorModel> vendorList = inventoryHelper.GetVendorList();
            vendorList.Insert(0, new VendorModel { Name = "Select Vendor", EncryptId = "" });
            ViewBag.VendorList = vendorList.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();

            ViewBag.MasterPartList = JsonConvert.SerializeObject(inventoryHelper.GetAllMasterPartNo());
            return View();
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        //public ActionResult AddPartNumber(string PartNo, string partName, string description, bool isSoftwareLevelApplicable, decimal costPrice, decimal salesPrice, int recommendQty, string comment)
        public ActionResult AddPartNumber(PartNumberModel partNumberModel)
        {
            Misc misc = new Misc();
            InventoryHelper inventoryHelper = new InventoryHelper();
            partNumberModel.VendorId = string.IsNullOrEmpty(partNumberModel.VendorId) ? null : misc.Decrypt(partNumberModel.VendorId);
            GeneralResponse response = inventoryHelper.AddPartNumber(partNumberModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult GetPartNo()
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            return PartialView("_PartNoList", inventoryHelper.GetPartNumberList());
        }

        public ActionResult GetPartsAndAliasByPartNumber(string draw, int length, int start, string partNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            var partList = inventoryHelper.GetPartsAndAliasByPartNumber(partNo);
            var response = new { data = partList, draw = draw, recordsFiltered = partList.Count(), recordsTotal = partList.Count() };
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetPartById(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetPartById(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult GetPartDetailsbyId(string partId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            int Id = Convert.ToInt32(new Misc().Decrypt(partId));
            var Part = inventoryHelper.GetPartDetailsbyId(Id);
            Misc misc = new Misc();
            string vendorId = null;
            vendorId = (Part.Vendor != null) ? misc.Encrypt(Part.Vendor.Id.ToString()) : null;
            int maxBoardSerialNumber = inventoryHelper.GetMaxBoardNumber();
            return Json(new { Description = Part.Description, IsSoftwareLevelApplicable = Part.IsSoftwareLevelApplicable, BoardSerialNumber = maxBoardSerialNumber, VendorId = vendorId, IsSerialNumberRequired = Part.IsSerialNumberRequired ?? true });
        }

        public ActionResult GetPartDetailsbyPartNo(string partNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();

            var Part = inventoryHelper.GetPartDetailsbyPartNo(partNo);
            Misc misc = new Misc();
            string vendorId = null;
            vendorId = (Part.Vendor != null) ? misc.Encrypt(Part.Vendor.Id.ToString()) : null;
            int maxBoardSerialNumber = inventoryHelper.GetMaxBoardNumber();
            return Json(new { Description = Part.Description, IsSoftwareLevelApplicable = Part.IsSoftwareLevelApplicable, BoardSerialNumber = maxBoardSerialNumber, VendorId = vendorId, IsSerialNumberRequired = Part.IsSerialNumberRequired ?? true });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult EditPartNo(PartNumberModel partNumberModel)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                Misc misc = new Misc();
                InventoryHelper inventoryHelper = new InventoryHelper();
                partNumberModel.VendorId = string.IsNullOrEmpty(partNumberModel.VendorId) ? null : misc.Decrypt(partNumberModel.VendorId);
                partNumberModel.Id = Convert.ToInt32(misc.Decrypt(partNumberModel.PartNumberId));
                response = inventoryHelper.UpdatePartNo(partNumberModel);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                response.ResponseMessage = "OOps!! Exception occurs while updating the part number.";
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult DeletePartNo(string id)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                InventoryHelper inventoryHelper = new InventoryHelper();
                int partNoId = Convert.ToInt32(new Misc().Decrypt(id));
                response = inventoryHelper.DeletePartNo(partNoId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                response.ResponseMessage = "OOps!! Exception occurs while deleting the part number.";
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMaxSerialNumber()
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            int maxSerialNumber = inventoryHelper.GetMaxSerialNumber();
            return Json(maxSerialNumber + 1, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult Units(string key = "", string value = "")
        {
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Units";
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<SelectListItem> allStatusList = inventoryHelper.GetStatus();
            allStatusList.Insert(0, new SelectListItem { Text = "All", Value = "0" });
            ViewBag.AllStatus = allStatusList;
            ViewBag.StatusList = new List<SelectListItem> {
                new SelectListItem { Text="Select", Value="" }
            };
            var partNumberList = inventoryHelper.GetPartNameAndModelNumber();
            ViewBag.PartNumberList = JsonConvert.SerializeObject(partNumberList);
            if (string.IsNullOrEmpty(key))
            {
                return View(inventoryHelper.GetUnits("", "", 0));
            }
            int id = Convert.ToInt32(new Misc().Decrypt(value));
            List<UnitModel> unitList = inventoryHelper.GetUnits(key, value, 0);
            ViewBag.PoNumber = inventoryHelper.GetPurchaseOrderById(id).PurchaseOrderNumber;
            
           
            return View(unitList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetUnitsByMultipleKey(string purchaseOrderNumber, string serialNo, string boardSerialNo, int? statusId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitList = inventoryHelper.GetUnitsByMultipleKey(purchaseOrderNumber, serialNo, boardSerialNo, statusId);

            ViewBag.StatusList = new List<SelectListItem> {
                new SelectListItem { Text="Select", Value="" }
            };
            return PartialView("_UnitList", unitList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetUnits(string key, string value, int statusId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitList = inventoryHelper.GetUnits(key, value, statusId);

            ViewBag.StatusList = new List<SelectListItem> {
                new SelectListItem { Text="Select", Value="" }
            };
            return PartialView("_UnitList", unitList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetUnitsForSale(string key, string value)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitList = inventoryHelper.GetUnits(key, value, 0);

            ViewBag.ListCount = unitList.Count;
            ViewBag.StatusList = new List<SelectListItem> {
                new SelectListItem { Text="Select", Value="" }
            };
            return PartialView("_Sales", unitList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetMultipleUnits(string unitIds)
        {
            Misc misc = new Misc();
            List<string> obj = JsonConvert.DeserializeObject<List<string>>(unitIds);
            List<Int32> unitIdList = obj.Select(s => Convert.ToInt32(misc.Decrypt(s))).ToList();
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitList = inventoryHelper.GetMultipleUnits(unitIdList);
            return PartialView("_SalesUnitPartial", unitList);
        }

        public ActionResult GetMultipleUnitsBySerialNo()
        {
            Misc misc = new Misc();
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<int> serialNoList = JsonConvert.DeserializeObject<List<int>>(System.Web.HttpContext.Current.Request.Form["SerialNoList"].ToString());
            GeneralResponse response = new GeneralResponse();
            List<UnitModel> unitList = inventoryHelper.GetMultipleUnitsBySerialNo(serialNoList, out response);

            return Json(new { unitList, response }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult LinkPartsToMaster()
        //{
        //    List<string> partNoList = JsonConvert.DeserializeObject<List<string>>(System.Web.HttpContext.Current.Request.Form["PartNoList"].ToString());
        //    string masterPartNo = System.Web.HttpContext.Current.Request.Form["PartNo"].ToString();
        //    var response = new InventoryHelper().LinkPartsToMaster(partNoList, masterPartNo);
        //    return Json(new { response }, JsonRequestBehavior.AllowGet);
        //}

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetUnitBySerialNo(string serialNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            UnitModel unitModel = new UnitModel();
            int value;
            if (int.TryParse(serialNo, out value))
            {
                unitModel = inventoryHelper.GetUnitBySerialNo(Convert.ToInt32(serialNo));
            }
            else
            {
                unitModel.ResponseCode = ((int)Enumerations.Unit.InvalidSerialNumber).ToString();
                unitModel.ResponseMessage = Enumerations.Unit.InvalidSerialNumber.GetStringValue();
            }
            return Json(new { unitModel }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUnitsByInvoiceNo(string invoiceNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitModelList = new List<UnitModel>();
            GeneralResponse response = new GeneralResponse();
            unitModelList = inventoryHelper.GetUnitsByInvoice(invoiceNo, out response).Where(w => w.UnitStatus == Enumerations.UnitStatus.Sold.ToString()).ToList();

            return Json(new { unitModelList, response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSoldUnitBySerialNo(string serialNo)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            UnitModel unitModel = new UnitModel();
            int value;
            if (int.TryParse(serialNo, out value))
            {
                unitModel = inventoryHelper.GetSoldUnitBySerialNo(Convert.ToInt32(serialNo));
            }
            else
            {
                unitModel.ResponseCode = ((int)Enumerations.Unit.InvalidSerialNumber).ToString();
                unitModel.ResponseMessage = Enumerations.Unit.InvalidSerialNumber.GetStringValue();
            }
            return Json(new { unitModel }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetUnitById(string unitId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            int id = Convert.ToInt32(new Misc().Decrypt(unitId));
            UnitModel unitModel = inventoryHelper.GetUnitById(id);
            List<SelectListItem> statusList = new List<SelectListItem>();
            if (unitModel.ResponseCode == ((int)Enumerations.Unit.Success).ToString())
            {
                unitModel.PurchaseOrderId = new Misc().Encrypt(unitModel.PurchaseOrderId);
                //List<SelectListItem> unitStatusList = inventoryHelper.GetStatus();
                //statusList = unitStatusList.Where(w => Convert.ToInt32(w.Value) >= unitModel.StatusId && Convert.ToInt32(w.Value) <= (unitModel.StatusId + 1)).ToList();
                statusList = GetNextStatusByStatusId(unitModel.StatusId);
            }
            return Json(new { unitModel, statusList }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult UpdateUnit(UnitModel unitModel)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                unitModel.Id = Convert.ToInt32(new Misc().Decrypt(unitModel.UnitId));
                response = inventoryHelper.UpdateUnit(unitModel);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult DeleteUnit(string id)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                int unitId = Convert.ToInt32(new Misc().Decrypt(id));
                response = inventoryHelper.DeleteUnit(unitId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStatusByStatusId(int statusId)
        {
            return Json(GetNextStatusByStatusId(statusId), JsonRequestBehavior.AllowGet);
        }


        public List<SelectListItem> GetNextRMAStatus(string statusId)
        {
            Misc misc = new Misc();
            //List<SelectListItem> statusList = new InventoryHelper().GetStatusRMA().Where(w => w.Id >= Convert.ToInt32(misc.Decrypt(statusId))).Select(s => new SelectListItem
            //{
            //    Text = s.Status,
            //    Value = misc.Encrypt(s.Id.ToString())
            //}).ToList();
            List<SelectListItem> statusList = new InventoryHelper().GetStatusRMA().Select(s => new SelectListItem
            {
                Text = s.Status,
                Value = misc.Encrypt(s.Id.ToString())
            }).ToList();
            statusList.Insert(0, new SelectListItem { Text = "Select Status", Value = "" });
            return statusList;
        }

        public List<SelectListItem> GetNextStatusByStatusId(int statusId)
        {
            List<SelectListItem> unitStatus = new List<SelectListItem>();
            switch (statusId)
            {
                case 1:
                    {
                        unitStatus.Insert(0, new SelectListItem { Text = Enumerations.UnitStatus.Received.GetStringValue(), Value = ((int)Enumerations.UnitStatus.Received).ToString() });
                        unitStatus.Insert(1, new SelectListItem { Text = Enumerations.UnitStatus.AvailableForSale.GetStringValue(), Value = ((int)Enumerations.UnitStatus.AvailableForSale).ToString() });
                        unitStatus.Insert(2, new SelectListItem { Text = Enumerations.UnitStatus.Destroy.GetStringValue(), Value = ((int)Enumerations.UnitStatus.Destroy).ToString() });
                        break;
                    }
                case 2:
                    {
                        unitStatus.Insert(0, new SelectListItem { Text = Enumerations.UnitStatus.AvailableForSale.GetStringValue(), Value = ((int)Enumerations.UnitStatus.AvailableForSale).ToString() });
                        unitStatus.Insert(1, new SelectListItem { Text = Enumerations.UnitStatus.TradshowDemo.GetStringValue(), Value = ((int)Enumerations.UnitStatus.TradshowDemo).ToString() });
                        unitStatus.Insert(2, new SelectListItem { Text = Enumerations.UnitStatus.Destroy.GetStringValue(), Value = ((int)Enumerations.UnitStatus.Destroy).ToString() });
                        break;
                    }
                case 3:
                    {
                        unitStatus.Insert(0, new SelectListItem { Text = Enumerations.UnitStatus.Sold.GetStringValue(), Value = ((int)Enumerations.UnitStatus.Sold).ToString() });

                        break;
                    }
                case 4:
                    {
                        unitStatus.Insert(0, new SelectListItem { Text = Enumerations.UnitStatus.TradshowDemo.GetStringValue(), Value = ((int)Enumerations.UnitStatus.TradshowDemo).ToString() });
                        unitStatus.Insert(1, new SelectListItem { Text = Enumerations.UnitStatus.AvailableForSale.GetStringValue(), Value = ((int)Enumerations.UnitStatus.AvailableForSale).ToString() });
                        break;
                    }
                case 5:
                    {
                        unitStatus.Insert(0, new SelectListItem { Text = Enumerations.UnitStatus.Destroy.GetStringValue(), Value = ((int)Enumerations.UnitStatus.Destroy).ToString() });
                        break;
                    }
            }
            return unitStatus;
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult UpdateMultipleUnits(string values, int statusId)
        {
            Misc misc = new Misc();
            GeneralResponse response = new GeneralResponse();
            InventoryHelper inventoryHelper = new InventoryHelper();
            try
            {
                List<string> obj = JsonConvert.DeserializeObject<List<string>>(values);
                List<Int32> unitIdList = obj.Select(s => Convert.ToInt32(misc.Decrypt(s))).ToList();
                response = inventoryHelper.UpdateMultipleUnits(unitIdList, statusId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult EditSales(string id)
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.PageHeaderTitle = "Invoice";
            ViewBag.UserType = GetUserTypeFromSession();
            InventoryHelper inventoryHelper = new InventoryHelper();
            InvoiceDetailsModel invoiceModel = new InvoiceDetailsModel
            {
                InvoiceModel = new InvoiceModel { InvoiceDate = DateTime.Now },
                UnitModelList = new List<UnitModel>()
            };
            List<CustomerModel> customerList = inventoryHelper.GetCustomerList();
            customerList.Insert(0, new CustomerModel { Name = "Select customer name", EncryptId = "" });
            ViewBag.CustomerList = customerList.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();

            invoiceModel = inventoryHelper.GetInventoryDetailsById(Convert.ToInt32(new Misc().Decrypt(id)));
            ViewBag.SerialNoList = JsonConvert.SerializeObject(invoiceModel.UnitModelList.Select(s => s.SerialNumber ?? 0).ToList());


            return View(invoiceModel);

        }


        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult Sales()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.PageHeaderTitle = "Invoice";
            ViewBag.UserType = GetUserTypeFromSession();
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitModel> unitList = new List<UnitModel>();

            List<CustomerModel> customerList = inventoryHelper.GetCustomerList();
            customerList.Insert(0, new CustomerModel { Name = "Select customer name", EncryptId = "" });
            ViewBag.CustomerList = customerList.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();

            return View(unitList);
        }



        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult SaleMultipleUnits(string serialNos, string invoiceNo, string invoiceDate, string customerId, string emailId, string customerName)
        {
            Misc misc = new Misc();
            GeneralResponse response = new GeneralResponse();
            InventoryHelper inventoryHelper = new InventoryHelper();
            try
            {
                List<int> serialNoList = JsonConvert.DeserializeObject<List<int>>(serialNos);
                customerId = new Misc().Decrypt(customerId);
                response = inventoryHelper.SaleMultipleUnit(serialNoList, invoiceNo, Convert.ToDateTime(invoiceDate), Convert.ToInt32(customerId), emailId, customerName);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult UpdateInvoice(string serialNos, string invoiceNo, string invoiceDate, string customerId, string emailId, string InvoiceId, string CustomerName)
        {
            Misc misc = new Misc();
            GeneralResponse response = new GeneralResponse();
            InventoryHelper inventoryHelper = new InventoryHelper();
            try
            {
                List<int> serialNoList = JsonConvert.DeserializeObject<List<int>>(serialNos);
                customerId = new Misc().Decrypt(customerId);
                response = inventoryHelper.UpdateInvoice(serialNoList, invoiceNo, Convert.ToDateTime(invoiceDate), Convert.ToInt32(customerId), emailId, Convert.ToInt32(new Misc().Decrypt(InvoiceId)), CustomerName);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales,InventoryProduction")]
        public ActionResult GetInvoice()
        {

            InventoryHelper inventoryHelper = new InventoryHelper();
            List<InvoiceModel> model = inventoryHelper.GetInvoiceList("", "");
            return PartialView("_InvoicePartial", model);
        }

        public ActionResult InvoiceTest()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            InventoryHelper inventoryHelper = new InventoryHelper();
            ViewBag.PageHeaderTitle = "Invoice";
            //List<InvoiceModel> model = inventoryHelper.GetInvoiceList("", "");
            return View();
        }

        public JsonResult GetInvoiceInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            //List<Organization> organizations = new List<Organization>();
            List<InvoiceModel> invoiceList = new List<InvoiceModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;
            invoiceList = new InventoryHelper().GetInvoiceInChunk(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter);

            var response = new { data = invoiceList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        //[OutputCacheAttribute(NoStore = true, Duration = 0, VaryByParam = "*")]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales,InventoryProduction")]
        public ActionResult Invoice()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            InventoryHelper inventoryHelper = new InventoryHelper();
            ViewBag.PageHeaderTitle = "Invoice";
            List<InvoiceModel> model = inventoryHelper.GetInvoiceList("", "");
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales,InventoryProduction")]
        public ActionResult SearchInvoice(string key, string value)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<InvoiceModel> model = inventoryHelper.GetInvoiceList(key, value);
            return PartialView("_InvoicePartial", model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales,InventoryProduction")]
        public ActionResult InvoiceDetails(string id)
        {
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Invoice Details";
            InventoryHelper inventoryHelper = new InventoryHelper();
            int invoiceId = Convert.ToInt32(new Misc().Decrypt(id));
            InvoiceDetailsModel invoiceDetailsModel = inventoryHelper.GetInventoryDetailsById(invoiceId);
            return View(invoiceDetailsModel);
        }


        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales,InventoryProduction")]
        public ActionResult DeleteInvoice(string id)
        {
            int invId = Convert.ToInt32(new Misc().Decrypt(id));
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.DeleteInvoice(invId);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SaleUnit(string id, string invoiceNo, string invoiceDate)
        //{
        //    InventoryHelper inventoryHelper = new InventoryHelper();
        //}



        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult Reports()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Inventory Reports";
            InventoryHelper inventoryHelper = new InventoryHelper();
            Misc misc = new Misc();
            List<PartNumberModel> partNumberList = inventoryHelper.GetPartNumberList().OrderBy(o => o.PartNumber).ToList();
            partNumberList.Insert(0, new PartNumberModel { PartNumber = "All", PartNumberId = "" });
            ViewBag.PartNumberList = partNumberList.Select(s => new SelectListItem
            {
                Text = s.PartNumber,
                Value = misc.Decrypt(s.PartNumberId) //required to decrypt again as enc data throws error while passing it to url
            }).ToList();
            List<SelectListItem> allStatusList = inventoryHelper.GetStatus();
            allStatusList.Insert(0, new SelectListItem { Text = "All", Value = "0" });
            ViewBag.AllStatus = allStatusList;
            return View(new List<UnitDetailedSheetModel>());

        }


        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetReport(string partId, string partNumber, string serialNumber, int status, bool isDetailedSheet, string startDate, string endDate)
        {
            DateTime dtStartDate = string.IsNullOrEmpty(startDate) ? DateTime.MinValue : Convert.ToDateTime(startDate);
            DateTime dtEndDate = string.IsNullOrEmpty(endDate) ? DateTime.MaxValue : (Convert.ToDateTime(endDate)).AddDays(1);
            Misc misc = new Misc();
            InventoryHelper inventoryHelper = new InventoryHelper();
            int? pId = null;
            if (!string.IsNullOrEmpty(partId))
            {
                pId = Convert.ToInt32(partId);
            }
            int? serNumber = null;
            if (!string.IsNullOrEmpty(serialNumber))
            {
                serNumber = Convert.ToInt32(serialNumber);
            }
            List<UnitDetailedSheetModel> unitModelList = inventoryHelper.GetReport(pId, serNumber, status, isDetailedSheet, dtStartDate, dtEndDate);
            return PartialView("_ReportPartial", unitModelList);
        }


        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult GetAvailableForSaleReport(int statusId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<UnitDetailedSheetModel> unitModelList = inventoryHelper.GetAvailableForSaleReport(statusId);
            var document = new Document(PageSize.A4, 50, 50, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate()); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var redColorFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.RED);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);

            string agentDescription = string.Empty;

            Paragraph p1 = new Paragraph();
            Phrase ph1 = new Phrase("Inventory Report\n\n", headerFont);
            p1.Add(ph1);
            Phrase ph2 = new Phrase("Available for sale", boldTableSubHeaderFont);
            p1.Add(ph2);
            PdfPCell cellTitle = new PdfPCell(p1);
            cellTitle.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellTitle.VerticalAlignment = 1;
            cellTitle.Border = PdfPCell.NO_BORDER;
            cellTitle.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellTitle);
            PdfPCell cellDate = new PdfPCell(new Phrase("Date : " + DateTime.Now.ToString("MM-dd-yyyy"), headerFont)) { HorizontalAlignment = 2 };
            cellDate.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellDate.VerticalAlignment = 1;
            cellDate.Border = PdfPCell.NO_BORDER;
            cellDate.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellDate);

            PdfPTable tblStudents = new PdfPTable(8);
            tblStudents.WidthPercentage = 100;
            tblStudents.HorizontalAlignment = 0;
            tblStudents.SetWidths(new int[] { 1, 1, 1, 1, 1, 1, 1, 1 });
            tblStudents.DefaultCell.Padding = 3;
            tblStudents.SpacingBefore = 10f;
            tblStudents.SpacingAfter = 10f;

            tblStudents.HeaderRows = 1;
            tblStudents.AddCell(new PdfPCell(new Phrase("Part No", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Unit Count (Available for Sale)", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Recomended Qty", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Cost Price", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Total Cost", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("List Price", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("Total Sales Price", boldTableFont)));

            CultureInfo cultureInfo = new CultureInfo("en-US");
            iTextSharp.text.Font dynamicFont;
            foreach (var item in unitModelList.ToList())
            {
                dynamicFont = (item.UnitCount < item.RecomendedQty) ? redColorFont : normalTableFont;

                tblStudents.AddCell(new PdfPCell(new Phrase(item.PartNumber, dynamicFont)) { HorizontalAlignment = 0 });
                tblStudents.AddCell(new PdfPCell(new Phrase(item.Description, dynamicFont)) { HorizontalAlignment = 0 });
                tblStudents.AddCell(new PdfPCell(new Phrase(item.UnitCount.ToString(), dynamicFont)) { HorizontalAlignment = 0 });
                tblStudents.AddCell(new PdfPCell(new Phrase(item.RecomendedQty.ToString(), dynamicFont)) { HorizontalAlignment = 0 });
                tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", item.CostPrice), dynamicFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", (item.CostPrice * item.UnitCount)), dynamicFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", item.SalesPrice), dynamicFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", (item.SalesPrice * item.UnitCount)), dynamicFont)));
            }


            tblStudents.AddCell(new PdfPCell(new Phrase("", normalTableFont)) { HorizontalAlignment = 0 });
            tblStudents.AddCell(new PdfPCell(new Phrase("", normalTableFont)) { HorizontalAlignment = 0 });
            tblStudents.AddCell(new PdfPCell(new Phrase("", normalTableFont)) { HorizontalAlignment = 0 });
            tblStudents.AddCell(new PdfPCell(new Phrase("", normalTableFont)) { HorizontalAlignment = 0 });
            tblStudents.AddCell(new PdfPCell(new Phrase("Total", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", unitModelList.Sum(s => s.UnitCount * s.CostPrice)), boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase("", boldTableFont)));
            tblStudents.AddCell(new PdfPCell(new Phrase(string.Format(cultureInfo, "{0:C}", unitModelList.Sum(s => s.UnitCount * s.SalesPrice)), boldTableFont)));


            document.Add(tblTitle);
            document.Add(tblStudents);

            document.Close();
            return DownloadToClient(output.ToArray(), "AvailableForSaleReport");
        }


        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public ActionResult DownloadPDF(string partId, string partNumber, string serialNumber, int status, bool isDetailedSheet, string startDate, string endDate)
        {
            DateTime dtStartDate = string.IsNullOrEmpty(startDate) ? DateTime.MinValue : Convert.ToDateTime(startDate);
            DateTime dtEndDate = string.IsNullOrEmpty(endDate) ? DateTime.MaxValue : (Convert.ToDateTime(endDate)).AddDays(1);
            Misc misc = new Misc();
            InventoryHelper inventoryHelper = new InventoryHelper();
            int? pId = null;
            if (!string.IsNullOrEmpty(partId))
            {
                pId = Convert.ToInt32(partId);
            }
            int? serNumber = null;
            if (!string.IsNullOrEmpty(serialNumber))
            {
                serNumber = Convert.ToInt32(serialNumber);
            }
            List<UnitDetailedSheetModel> unitModelList = inventoryHelper.GetReport(pId, serNumber, status, isDetailedSheet, dtStartDate, dtEndDate);


            var document = new Document(PageSize.A4, 50, 50, 25, 25);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate()); // for horizontal layout
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            document.Open();

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var titleFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.BLACK);
            var headerFont = new iTextSharp.text.Font(bfTimes, 16, iTextSharp.text.Font.BOLDITALIC, BaseColor.WHITE);
            var subTitleFont = new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            var boldTableSubHeaderFont = new iTextSharp.text.Font(bfTimes, 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            var normalTableFont = new iTextSharp.text.Font(bfTimes, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            var endingMessageFont = times;
            var bodyFont = times;

            var tblTitle = new PdfPTable(3);
            tblTitle.WidthPercentage = 100;
            tblTitle.DefaultCell.Padding = 3;
            tblTitle.DefaultCell.Border = PdfPCell.NO_BORDER;

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("../Images/logo_smartCloudw_nl.png"));
            PdfPCell cellReport = new PdfPCell(jpg) { HorizontalAlignment = 0 };
            cellReport.Border = PdfPCell.NO_BORDER;
            cellReport.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellReport);

            string agentDescription = string.Empty;


            Paragraph headerPara = new Paragraph();
            Phrase ph1 = new Phrase("Inventory " + (isDetailedSheet ? "Detailed" : "Summary") + " Report\n\n", headerFont);
            headerPara.Add(ph1);
            if (!string.IsNullOrEmpty(partId))
            {
                Phrase ph2 = new Phrase("Part No = " + partNumber + "\n", boldTableSubHeaderFont);
                headerPara.Add(ph2);
            }

            string unitStatus = "";
            if (status == 0)
            {
                unitStatus = "All";
            }
            else
            {
                unitStatus = ((Enumerations.UnitStatus)status).ToString();
            }
            Phrase ph5 = new Phrase("Status = " + unitStatus + " \n", boldTableSubHeaderFont);
            headerPara.Add(ph5);


            if (!string.IsNullOrEmpty(serialNumber))
            {
                Phrase ph3 = new Phrase("Serial No = " + serialNumber + "\n", boldTableSubHeaderFont);
                headerPara.Add(ph3);
            }
            if (!(string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate)))
            {
                Phrase ph4 = new Phrase("Date " + startDate + " to " + endDate + "\n", boldTableSubHeaderFont);
                headerPara.Add(ph4);
            }




            PdfPCell cellTitle = new PdfPCell(headerPara);
            cellTitle.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            cellTitle.VerticalAlignment = 1;
            cellTitle.Border = PdfPCell.NO_BORDER;
            cellTitle.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellTitle);
            PdfPCell cellAgent = new PdfPCell(new Phrase("Date \n " + DateTime.Now.ToString("MM-dd-yyyy"), headerFont)) { HorizontalAlignment = 1, VerticalAlignment = 1 };
            cellAgent.Border = PdfPCell.NO_BORDER;
            cellAgent.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#314a7b"));
            tblTitle.AddCell(cellAgent);

            int tableColumns = 1;
            PdfPTable tblStudents = new PdfPTable(tableColumns);
            if (isDetailedSheet)
            {
                tableColumns = 10;
                tblStudents = new PdfPTable(tableColumns);

                tblStudents.WidthPercentage = 100;
                tblStudents.HorizontalAlignment = 0;
                tblStudents.SetWidths(new int[] { 80, 80, 80, 80, 80, 80, 80, 80, 80, 80 });
                tblStudents.DefaultCell.Padding = 3;
                tblStudents.SpacingBefore = 10f;
                tblStudents.SpacingAfter = 10f;

                tblStudents.HeaderRows = 1;
                tblStudents.AddCell(new PdfPCell(new Phrase("Part No", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Serial No.", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Software Level", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Status", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Date", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Status", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Date", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Status", boldTableFont)));
                tblStudents.AddCell(new PdfPCell(new Phrase("Date", boldTableFont)));


                foreach (var item in unitModelList.ToList())
                {
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.PartNumber, normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.Description, normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.SerialNumber.ToString(), normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.SoftwareLevel, normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.RecStatus, normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.RecDate.ToString("dd MMMM yyyy"), normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.AvailableForSaleStatus, normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase((item.AvailableForSaleDate == null) ? "" : item.AvailableForSaleDate.Value.ToString("dd MMMM yyyy"), normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.SoldStatus, normalTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase((item.SoldDate == null) ? "" : item.SoldDate.Value.ToString("dd MMMM yyyy"), normalTableFont)));
                }
            }
            else
            {
                tableColumns = 9;
                tblStudents = new PdfPTable(tableColumns);

                tblStudents.WidthPercentage = 100;
                tblStudents.HorizontalAlignment = 0;
                tblStudents.SetWidths(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 });
                tblStudents.DefaultCell.Padding = 3;
                tblStudents.SpacingBefore = 10f;
                tblStudents.SpacingAfter = 10f;

                tblStudents.HeaderRows = 1;
                tblStudents.AddCell(new PdfPCell(new Phrase("Part No", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Description", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Status", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Unit Count", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Cost Price", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Extended Cost Price", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Sales Price", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Extended Sales Price", boldTableFont)) { HorizontalAlignment = 1 });
                tblStudents.AddCell(new PdfPCell(new Phrase("Margin (%)", boldTableFont)) { HorizontalAlignment = 1 });

                foreach (var item in unitModelList.ToList())
                {
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.PartNumber, normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.Description, normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.RecStatus, normalTableFont)) { HorizontalAlignment = 0 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.UnitCount.ToString(), normalTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.CostPrice.ToString(), normalTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase((item.CostPrice * item.UnitCount).ToString(), normalTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.SalesPrice.ToString(), normalTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase((item.UnitCount * item.SalesPrice).ToString(), normalTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(item.Margin.ToString() + "%", normalTableFont)) { HorizontalAlignment = 2 });
                }
                if (unitModelList.Count > 0)
                {
                    decimal totalCost = unitModelList.Sum(s => s.UnitCount * s.CostPrice);
                    decimal totalSales = unitModelList.Sum(s => s.UnitCount * s.SalesPrice);
                    decimal margin = Math.Floor(((totalSales - totalCost) / totalCost) * 100);

                    tblStudents.AddCell(new PdfPCell(new Phrase("", boldTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase("", boldTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase("Total", boldTableFont)));
                    tblStudents.AddCell(new PdfPCell(new Phrase(unitModelList.Sum(s => s.UnitCount).ToString(), boldTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase("", boldTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(totalCost.ToString(), boldTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase("", boldTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(totalSales.ToString(), boldTableFont)) { HorizontalAlignment = 2 });
                    tblStudents.AddCell(new PdfPCell(new Phrase(margin.ToString() + "%", boldTableFont)) { HorizontalAlignment = 2 });
                }
            }

            document.Add(tblTitle);
            document.Add(tblStudents);


            document.Close();
            return DownloadToClient(output.ToArray(), "ReferralCommission");
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction,InventorySales")]
        public FileContentResult DownloadToClient(byte[] bytes, string attachmentName)
        {
            var arrByte = AddPageNumberToPDF(bytes);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.pdf", attachmentName));
            Response.BinaryWrite(arrByte);
            return File(arrByte, "application/pdf");
        }

        public byte[] AddPageNumberToPDF(byte[] bytes)
        {
            iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase("Page " + i.ToString() + " of " + pages.ToString(), blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            return bytes;
        }

        #region Vendor
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult Vendor()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Vendor";
            List<VendorModel> vendorList = new InventoryHelper().GetVendorList();
            return View(vendorList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult AddVendor(VendorModel vendorModel)
        {
            var helper = new InventoryHelper();
            List<VendorModel> vendorList = new List<VendorModel>();
            var response = helper.AddVendor(vendorModel);

            return Json(new { response = response, list = new InventoryHelper().GetVendorList() });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult DeleteVendor(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int vendorId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteVendor(vendorId);
            return Json(new { response });

        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult EditVendor(VendorModel vendorModel)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            vendorModel.Id = Convert.ToInt32(misc.Decrypt(vendorModel.EncryptId));
            var response = helper.EditVendor(vendorModel);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin")]
        public ActionResult GetVendor()
        {
            List<VendorModel> vendorList = new InventoryHelper().GetVendorList();
            return PartialView("_VendorList", vendorList);
        }

        #endregion Vendor

        #region Recceiver
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult Receiver()
        {
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Employee";
            List<ReceiverModel> receiverList = new InventoryHelper().GetReceiverList();
            return View(receiverList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult AddReceiver(string name)
        {
            var helper = new InventoryHelper();
            List<ReceiverModel> vendorList = new List<ReceiverModel>();
            var response = helper.AddReceiver(name);

            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult DeleteReceiver(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int receiverId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteReceiver(receiverId);
            return Json(new { response });

        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult EditReceiver(string receiverName, string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int receiverId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.EditReceiver(receiverName, receiverId);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventoryProduction")]
        public ActionResult GetReceiver()
        {
            List<ReceiverModel> vendorList = new InventoryHelper().GetReceiverList();
            return PartialView("_ReceiverList", vendorList);
        }

        #endregion Recceiver

        #region Customer
        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult Customer()
        {
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Customer Type";
            List<CustomerModel> CustomerList = new InventoryHelper().GetCustomerList();
            return View(CustomerList);
        }



        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult AddCustomer(string name)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddCustomer(name);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult DeleteCustomerType(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int customerId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteCustomerType(customerId);
            return Json(new { response });

        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult EditCustomer(string customerName, string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int customerId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.EditCustomer(customerName, customerId);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult GetCustomer()
        {
            List<CustomerModel> customerList = new InventoryHelper().GetCustomerList();
            return PartialView("_CustomerList", customerList);
        }
        #endregion

        #region EngineeringDocs

        public ActionResult EngineeringDocs(string partNo)
        {
            ViewBag.PartNo = partNo;
            ViewBag.ControllerName = "Inventory";
            if (string.IsNullOrEmpty(partNo))
            {
                return RedirectToAction("Logout", "Home");
            }
            ViewBag.PageHeaderTitle = "Documents for part no. " + partNo + " (" + new InventoryHelper().GetPartDetailsbyPartNo(partNo).Description + ")";


            var partNumberMaster = new InventoryHelper().GetPartDetailsbyPartNo(partNo);
            if (partNumberMaster.Id != partNumberMaster.MasterPartId)
            {
                partNo = new InventoryHelper().GetPartDetailsbyId(partNumberMaster.MasterPartId ?? 0).PartNumber;
                ViewBag.PartTitle = "This model is an alias to the Master Document " + partNo;
                ViewBag.PartSubTitle = "(" + new InventoryHelper().GetPartDetailsbyPartNo(partNo).Description + ")";
            }
            ViewBag.UserType = GetUserTypeFromSession();
            return View(new InventoryHelper().GetEngineeringDocs(partNo));
        }

        public ActionResult GetAllEngineeringDocsById(string engDocId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            var engDoc = inventoryHelper.GetAllEngineeringDocsById(Convert.ToInt32(new Misc().Decrypt(engDocId)));
            return PartialView("_EngineeringDocs", engDoc);
        }

        public ActionResult DeleteFileOrDirectory(string id, bool isFolder)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            var response = inventoryHelper.DeleteFileOrDirectory(Convert.ToInt32(new Misc().Decrypt(id)), isFolder);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEngineeringDirectory(string directoryName, string parentId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddEngineeringDirectory(directoryName, Convert.ToInt32(new Misc().Decrypt(parentId)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> files)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<HttpPostedFile> fileList = new List<HttpPostedFile>();
            for (int i = 0; i < System.Web.HttpContext.Current.Request.Files.Count; i++)
            {
                fileList.Add(System.Web.HttpContext.Current.Request.Files[i]);
            }
            string parentId = System.Web.HttpContext.Current.Request.Form["ParentId"].ToString();
            var response = inventoryHelper.UploadEngineeringFiles(fileList, Convert.ToInt32(new Misc().Decrypt(parentId)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenameFileOrFolder(string id, string name)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                response = inventoryHelper.RenameFileOrFolder(Convert.ToInt32(new Misc().Decrypt(id)), name);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.EngineeringDocEnum.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        #endregion EngineeringDocs


        #region Assembly

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult KitAssembly()
        {

            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Kit Assembly";
            ViewBag.ControllerName = "Inventory";
            ViewBag.PartList = new InventoryHelper().GetPartNumberListForAssemblyKit();
            return View(new InventoryHelper().GetKitAssembly());
        }

        public ActionResult GetKitAssembly()
        {
            return PartialView("_KitAssemblyList", new InventoryHelper().GetKitAssembly());
        }

        public ActionResult GetPartNumberListForAssemblyKit()
        {
            return Json(new { partList = new InventoryHelper().GetPartNumberListForAssemblyKit() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveAssembly(KitAssemblyModel obj)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                obj.File = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                obj.PartList = JsonConvert.DeserializeObject<List<PartNumberModel>>(System.Web.HttpContext.Current.Request.Form["PartList"].ToString());
                if (obj.File != null)
                {
                    string[] arrfilename = obj.File.FileName.Split('.');
                    obj.ImageName = Misc.RandomString(20) + "." + arrfilename[arrfilename.Length - 1];
                }
                response = inventoryHelper.AddKitAssembly(obj);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.EngineeringDocEnum.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAssembly(KitAssemblyModel obj)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                obj.File = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                obj.PartList = JsonConvert.DeserializeObject<List<PartNumberModel>>(System.Web.HttpContext.Current.Request.Form["PartList"].ToString());
                if (obj.File != null)
                {
                    string[] arrfilename = obj.File.FileName.Split('.');
                    obj.ImageName = Misc.RandomString(20) + "." + arrfilename[arrfilename.Length - 1];
                }
                response = inventoryHelper.UpdateKitAssembly(obj);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.EngineeringDocEnum.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteKitAssembly(int id)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            try
            {
                response = inventoryHelper.DeleteKitAssembly(id);
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.KitAssemblyEnum.Exception.GetStringValue();
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public void AddAssemblyKitImage(KitAssemblyModel obj)
        {
            var docfile = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            var fileName = System.Web.HttpContext.Current.Request.Form["FileName"].ToString();
            var aaaa = JsonConvert.DeserializeObject<List<PartNumberModel>>(System.Web.HttpContext.Current.Request.Form["PartList"].ToString());
            new InventoryHelper().UploadLitAssemblyImage(docfile, fileName);
        }


        public ActionResult GetKitAssemblyById(int id)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            return Json(new { kitAssemblyModel = inventoryHelper.GetKitAssemblyById(id) }, JsonRequestBehavior.AllowGet);
        }
        #endregion Assembly

        public ActionResult GetFolderList(string partNo)
        {
            var response = Newtonsoft.Json.JsonConvert.SerializeObject(new InventoryHelper().GetFolderList(partNo));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MoveFileAndFolder(int destinationFolderId, string strArrIds, string partNo)
        {
            Misc misc = new Misc();
            List<int> arrIds = strArrIds.Split(',').Select(s => Convert.ToInt32(misc.Decrypt(s))).ToList();
            InventoryHelper inventoryHelper = new InventoryHelper();
            var response = inventoryHelper.MoveFileAndFolder(arrIds, destinationFolderId);
            string folderList = "";
            if (response.ResponseCode == "0")
            {
                folderList = Newtonsoft.Json.JsonConvert.SerializeObject(new InventoryHelper().GetFolderList(partNo));
            }
            return Json(new { response, folderList }, JsonRequestBehavior.AllowGet);
        }
        public string GetUserTypeFromSession()
        {
            return Session["UserType"].ToString();
        }

        #region RMA

        public ActionResult SearchInventoryCustomer(string value)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var invCustomerList = invHelper.SearchInventoryCustomer(value).ToList();
            return Json(new { invCustomerList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchSerialNo(string value)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var serialNoList = invHelper.GetSerialNoWithModel(value).Select(s => new
            {
                s.SerialNumber,
                s.ModelNo,
                s.Description
            }).ToList();
            return Json(new { serialNoList }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetRMAInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            //List<Organization> organizations = new List<Organization>();
            List<RMAModel> rmaList = new List<RMAModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;
            int maxRMANo = 0;
            rmaList = new InventoryHelper().GetRMAInChunk(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter, out maxRMANo);

            var response = new { data = rmaList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount, maxRMANo = maxRMANo };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RMA()
        {
            InventoryHelper invHelper = new InventoryHelper();
            ViewBag.ControllerName = "Inventory";
            Misc misc = new Misc();
            ViewBag.PageHeaderTitle = "Return Manufacturing Authorization";
            ViewBag.UserType = GetUserTypeFromSession();
            var receiverList = invHelper.GetReceiverList().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();
            receiverList.Insert(0, new SelectListItem { Text = "Select Employee", Value = "" });
            ViewBag.ReceiverList = receiverList;

            var statusList = invHelper.GetStatusRMA().Select(s => new SelectListItem
            {
                Text = s.Status,
                Value = misc.Encrypt(s.Id.ToString())
            }).ToList();
            statusList.Insert(0, new SelectListItem { Text = "Select Status", Value = "" });
            ViewBag.StatusList = statusList;

            List<SelectListItem> CustomerTypeList = invHelper.GetCustomerList().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();
            CustomerTypeList.Insert(0, new SelectListItem { Text = "Select Customer Type", Value = "" });
            ViewBag.CustomerTypeList = CustomerTypeList;

            var discrepancyList = invHelper.GetDiscrepancyList().Select(s => new SelectListItem
            {
                Text = s.DiscrepancyDetail,
                Value = s.EncryptId
            }).ToList();
            discrepancyList.Insert(0, new SelectListItem { Text = "Select Discrepancy", Value = "" });
            ViewBag.DiscrepencyList = discrepancyList;


            var resolutionList = invHelper.GetResolutionList().Select(s => new SelectListItem
            {
                Text = s.ResolutionDetail,
                Value = s.EncryptId
            }).ToList();
            resolutionList.Insert(0, new SelectListItem { Text = "Select Resolution", Value = "" });
            ViewBag.ResolutionList = resolutionList;

            return View();
        }



        public ActionResult GetRMAList()
        {
            return PartialView("_RMAList", new InventoryHelper().GetRMAList());
        }

        public ActionResult SetRMA(List<SelectListItem> objList, string customerId, string discrepancyId, string receiverId, string resolutionId, string trackingNo, string rmaId, List<UnitModel> newSerialNumberList, string comments)
        {
            GeneralResponse response = new GeneralResponse();
            RMAModel rmaModel = new RMAModel();
            Misc misc = new Misc();
            rmaModel.CustomerId = (misc.Decrypt(customerId));
            if (!string.IsNullOrEmpty(discrepancyId))
            {
                rmaModel.DiscrepancyId = (misc.Decrypt(discrepancyId));
            }
            rmaModel.ReceiverId = receiverId;
            if (!string.IsNullOrEmpty(resolutionId))
            {
                rmaModel.ResolutionId = (misc.Decrypt(resolutionId));
            }
            rmaModel.TrackingNo = trackingNo;
            rmaModel.ReceiverId = misc.Decrypt(receiverId);
            rmaModel.Comments = comments;
            if (string.IsNullOrEmpty(rmaId))
            {
                response = new InventoryHelper().AddRMA(rmaModel, objList, newSerialNumberList);
            }
            else
            {
                rmaModel.Id = Convert.ToInt32(new Misc().Decrypt(rmaId));
                response = new InventoryHelper().EditRMA(rmaModel, objList, newSerialNumberList);
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddRMA()
        {
            RMAModel rmaModel = new RMAModel();
            Misc misc = new Misc();
            rmaModel.CustomerId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["CustomerId"].ToString()));
            rmaModel.SerialNo = System.Web.HttpContext.Current.Request.Form["SerialNo"].ToString();
            rmaModel.ModelNo = System.Web.HttpContext.Current.Request.Form["ModelNo"].ToString();
            rmaModel.Description = System.Web.HttpContext.Current.Request.Form["PartDescription"].ToString();
            string discrepancyId = System.Web.HttpContext.Current.Request.Form["DiscrepancyId"].ToString();
            if (!string.IsNullOrEmpty(discrepancyId))
            {
                rmaModel.DiscrepancyId = (misc.Decrypt(discrepancyId));
            }

            rmaModel.StatusId = misc.Decrypt(System.Web.HttpContext.Current.Request.Form["StatusId"].ToString());
            rmaModel.ReceiverId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["ReceiverId"].ToString()));
            string resolutionId = System.Web.HttpContext.Current.Request.Form["ResoultionId"].ToString();
            if (!string.IsNullOrEmpty(resolutionId))
            {
                rmaModel.ResolutionId = (misc.Decrypt(resolutionId));
            }
            rmaModel.TrackingNo = System.Web.HttpContext.Current.Request.Form["TrackingNo"].ToString();
            GeneralResponse response = new InventoryHelper().AddRMA(rmaModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditRMA()
        {
            RMAModel rmaModel = new RMAModel();
            Misc misc = new Misc();
            rmaModel.CustomerId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["CustomerId"].ToString()));
            rmaModel.SerialNo = System.Web.HttpContext.Current.Request.Form["SerialNo"].ToString();
            rmaModel.ModelNo = System.Web.HttpContext.Current.Request.Form["ModelNo"].ToString();
            string discrepancyId = System.Web.HttpContext.Current.Request.Form["DiscrepancyId"].ToString();
            if (!string.IsNullOrEmpty(discrepancyId))
            {
                rmaModel.DiscrepancyId = (misc.Decrypt(discrepancyId));
            }
            rmaModel.StatusId = misc.Decrypt(System.Web.HttpContext.Current.Request.Form["StatusId"].ToString());
            rmaModel.ReceiverId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["ReceiverId"].ToString()));
            rmaModel.Description = System.Web.HttpContext.Current.Request.Form["PartDescription"].ToString();
            string resolutionId = System.Web.HttpContext.Current.Request.Form["ResoultionId"].ToString();
            if (!string.IsNullOrEmpty(resolutionId))
            {
                rmaModel.ResolutionId = (misc.Decrypt(resolutionId));
            }
            rmaModel.TrackingNo = System.Web.HttpContext.Current.Request.Form["TrackingNo"].ToString();
            rmaModel.Id = Convert.ToInt32(misc.Decrypt(System.Web.HttpContext.Current.Request.Form["RMAId"].ToString()));
            GeneralResponse response = new InventoryHelper().EditRMA(rmaModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRMADetailsById(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetRMADetailsById(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response, statusList = GetNextRMAStatus(response.StatusId) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult deleteRMA(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.deleteRMA(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStatusHistoryByRMAId(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetStatusHistoryByRMAId(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { statusHistoryList = response }, JsonRequestBehavior.AllowGet);
        }

        #endregion RMA


        public ActionResult InventoryCustomer()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.PageHeaderTitle = "Customer";
            ViewBag.UserType = GetUserTypeFromSession();

            List<SelectListItem> CustomerTypeList = new InventoryHelper().GetCustomerList().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();
            CustomerTypeList.Insert(0, new SelectListItem { Text = "Select Customer Type", Value = "" });
            ViewBag.CustomerTypeList = CustomerTypeList;
            return View(); //new InventoryHelper().GetInventoryCustomers()
        }


        public JsonResult GetInventoryCustomer(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            //List<Organization> organizations = new List<Organization>();
            List<InventoryCustomerModel> customerList = new List<InventoryCustomerModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;
            customerList = new InventoryHelper().TestCustomer(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter);

            var response = new { data = customerList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount, msg = "Hello" };
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult GetInventoryCustomer()
        //{
        //    InventoryHelper invHelper = new InventoryHelper();
        //    return PartialView("_InventoryCustomerList", new InventoryHelper().GetInventoryCustomers());
        //}

        public ActionResult GetInventoryCustomerList()
        {
            InventoryHelper invHelper = new InventoryHelper();
            var invCustomerList = invHelper.GetInventoryCustomers().ToList();

            return Json(new { CustomerList = invCustomerList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditInventoryCustomer()
        {
            InventoryCustomerModel inventoryCustomerModel = new InventoryCustomerModel();
            Misc misc = new Misc();
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["CustomerTypeId"].ToString()))
            {
                inventoryCustomerModel.CustomerTypeId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["CustomerTypeId"].ToString()));
            }
            inventoryCustomerModel.FirstName = System.Web.HttpContext.Current.Request.Form["FirstName"].ToString();
            inventoryCustomerModel.LastName = System.Web.HttpContext.Current.Request.Form["LastName"].ToString();
            inventoryCustomerModel.Email = System.Web.HttpContext.Current.Request.Form["Email"].ToString();
            inventoryCustomerModel.PhoneNo = System.Web.HttpContext.Current.Request.Form["PhoneNo"].ToString();
            inventoryCustomerModel.CompanyName = System.Web.HttpContext.Current.Request.Form["CompanyName"].ToString();
            inventoryCustomerModel.Id = Convert.ToInt32(misc.Decrypt(System.Web.HttpContext.Current.Request.Form["CustomerId"].ToString()));
            InventoryHelper invHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            response = invHelper.EditInventoryCustomer(inventoryCustomerModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInventoryCustomer()
        {
            InventoryCustomerModel inventoryCustomerModel = new InventoryCustomerModel();
            Misc misc = new Misc();
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["CustomerTypeId"].ToString()))
            {
                inventoryCustomerModel.CustomerTypeId = (misc.Decrypt(System.Web.HttpContext.Current.Request.Form["CustomerTypeId"].ToString()));
            }
            inventoryCustomerModel.FirstName = System.Web.HttpContext.Current.Request.Form["FirstName"].ToString();
            inventoryCustomerModel.LastName = System.Web.HttpContext.Current.Request.Form["LastName"].ToString();
            inventoryCustomerModel.Email = System.Web.HttpContext.Current.Request.Form["Email"].ToString();
            inventoryCustomerModel.PhoneNo = System.Web.HttpContext.Current.Request.Form["PhoneNo"].ToString();
            inventoryCustomerModel.CompanyName = System.Web.HttpContext.Current.Request.Form["CompanyName"].ToString();
            InventoryHelper invHelper = new InventoryHelper();
            GeneralResponse response = new GeneralResponse();
            response = invHelper.AddInventoryCustomer(inventoryCustomerModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteInventoryCustomer(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.DeleteInventoryCustomer(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerByType(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetInventoryCustomersByTypeId(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { customerList = response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInventoryCustomerById(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetInventoryCustomerById(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }



        public ActionResult AddReturnManufactureAuth(string rmaId = "")
        {
            RMAModel rmaModel = new RMAModel
            {
                UnitModelList = new List<UnitModel>()
            };
            ViewBag.ControllerName = "Inventory";

            ViewBag.UserType = GetUserTypeFromSession();
            Misc misc = new Misc();
            InventoryHelper invHelper = new InventoryHelper();



            var statusList = invHelper.GetStatusRMA().Select(s => new SelectListItem
            {
                Text = s.Status,
                Value = s.Id.ToString(),
            }).ToList();
            statusList.Insert(0, new SelectListItem { Text = "Select RMA Status", Value = "" });
            ViewBag.DDLRMAStatus = statusList;
            ViewBag.RMAStatusList = JsonConvert.SerializeObject(statusList);



            var receiverList = invHelper.GetReceiverList().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();
            receiverList.Insert(0, new SelectListItem { Text = "Select Employee", Value = "" });
            ViewBag.ReceiverList = receiverList;



            List<SelectListItem> CustomerTypeList = invHelper.GetCustomerList().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.EncryptId
            }).ToList();
            CustomerTypeList.Insert(0, new SelectListItem { Text = "Select Customer Type", Value = "" });
            ViewBag.CustomerTypeList = CustomerTypeList;

            var discrepancyList = invHelper.GetDiscrepancyList().Select(s => new SelectListItem
            {
                Text = s.DiscrepancyDetail,
                Value = s.EncryptId
            }).ToList();
            discrepancyList.Insert(0, new SelectListItem { Text = "Select Discrepancy", Value = "" });
            ViewBag.DiscrepencyList = discrepancyList;


            var resolutionList = invHelper.GetResolutionList().Select(s => new SelectListItem
            {
                Text = s.ResolutionDetail,
                Value = s.EncryptId
            }).ToList();
            resolutionList.Insert(0, new SelectListItem { Text = "Select Resolution", Value = "" });
            ViewBag.ResolutionList = resolutionList;


            if (!string.IsNullOrEmpty(rmaId))
            {
                ViewBag.PageHeaderTitle = "Edit RMA";
                ViewBag.NextRMANo = "";
                rmaModel = invHelper.GetRMADetailsById(Convert.ToInt32(new Misc().Decrypt(rmaId)));
            }
            else
            {
                ViewBag.PageHeaderTitle = "Add RMA";
                ViewBag.NextRMANo = Convert.ToString(invHelper.GetMaxRMANo() + 1);
            }
            return View(rmaModel);
        }


        #region Discrepancy

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult Discrepancy()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Discrepancy";
            List<DiscrepancyModel> DiscrepancyList = new InventoryHelper().GetDiscrepancyList();
            return View(DiscrepancyList);
        }

        public ActionResult GetDiscrepancy()
        {
            List<DiscrepancyModel> discrepancyList = new InventoryHelper().GetDiscrepancyList();
            return PartialView("_DiscrepancyList", discrepancyList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult AddDiscrepancy(string name)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddDiscrepancy(name);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult EditDiscrepancy(string discrepancyDetail, string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int discrepancyId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.EditDiscrepancy(discrepancyDetail, discrepancyId);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult DeleteDiscrepancy(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int discrepancyId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteDiscrepancy(discrepancyId);
            return Json(new { response });

        }

        #endregion Discrepancy

        #region Resolution

        public ActionResult Resolution()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Resolution";
            List<ResolutionModel> resoltionList = new InventoryHelper().GetResolutionList();
            return View(resoltionList);
        }

        public ActionResult GetResolution()
        {
            List<ResolutionModel> resolutionList = new InventoryHelper().GetResolutionList();
            return PartialView("_ResolutionList", resolutionList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult AddResolution(string resolutionDetail)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddResolution(resolutionDetail);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult EditResolution(string resolutionDetail, string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int resolutionId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.EditResolution(resolutionDetail, resolutionId);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult DeleteResolution(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int resolutionId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteResolution(resolutionId);
            return Json(new { response });

        }

        #endregion Resolution

        #region RMA Status

        public ActionResult RMAStatus()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "RMAStatus";
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int sno = 0;
            var statusList = helper.GetStatusRMA().Select(s => new RMAStatusMOdel
            {
                Status = s.Status,
                EncryptId = misc.Encrypt(s.Id.ToString()),
                UnitStatus = s.UnitStatusId == null ? "" : s.UnitStatus.Status,
                UnitStatusId = s.UnitStatusId == null ? 0 : s.UnitStatusId,
                SNO = ++sno
            }).ToList();

            var unitStatusList = helper.GetStatus();
            unitStatusList.Insert(0, new SelectListItem { Text = "Select Unit Status", Value = "0" });
            ViewBag.UnitStatusList = unitStatusList.Select(s => new SelectListItem
            {
                Text = s.Text,
                Value = s.Value
            }).ToList();

            return View(statusList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult AddRMAStatus(RMAStatusMOdel rmaModel)
        {

            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddRMAStatus(rmaModel.Status, Convert.ToInt32(rmaModel.UnitStatusId));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRMAStatus()
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int sno = 0;
            var statusList = helper.GetStatusRMA().Select(s => new RMAStatusMOdel
            {
                Status = s.Status,
                EncryptId = misc.Encrypt(s.Id.ToString()),
                UnitStatus = s.UnitStatusId == null ? "" : s.UnitStatus.Status,
                UnitStatusId = s.UnitStatusId == null ? 0 : s.UnitStatusId,
                SNO = ++sno
            }).ToList();
            return PartialView("_RMAStatusList", statusList);
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult EditRMAStatus(string status, string id, int UnitStatusId)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int statusId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.EditRMAStatus(status, statusId, UnitStatusId);
            return Json(new { response });
        }

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult DeleteRMAStatus(string id)
        {
            Misc misc = new Misc();
            var helper = new InventoryHelper();
            int statusId = Convert.ToInt32(misc.Decrypt(id));
            var response = helper.DeleteRMAStatus(statusId);
            return Json(new { response });

        }




        #endregion RMA Status

        #region GAVBigDog

        [InventoryFilter(Role = "Admin,InventoryAdmin,InventorySubAdmin,InventorySales")]
        public ActionResult GAVDocs()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "Documents for GAV-BigDog";
            return View(new InventoryHelper().GetGAVDocs(1));
        }

        public ActionResult GetFolderListGAV()
        {
            var response = Newtonsoft.Json.JsonConvert.SerializeObject(new InventoryHelper().GetFolderListGAV());
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllGAVDocsById(string gavDocId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            var gavDoc = inventoryHelper.GetAllGAVDocsById(Convert.ToInt32(new Misc().Decrypt(gavDocId)));
            return PartialView("_GavDocs", gavDoc);
        }

        [HttpPost]
        public ActionResult UploadGAVFile(IEnumerable<HttpPostedFileBase> files)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            List<HttpPostedFile> fileList = new List<HttpPostedFile>();
            for (int i = 0; i < System.Web.HttpContext.Current.Request.Files.Count; i++)
            {
                fileList.Add(System.Web.HttpContext.Current.Request.Files[i]);
            }
            string parentId = System.Web.HttpContext.Current.Request.Form["ParentId"].ToString();
            var response = inventoryHelper.UploadGAVFiles(fileList, Convert.ToInt32(new Misc().Decrypt(parentId)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddGAVDirectory(string directoryName, string parentId)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            GeneralResponse response = inventoryHelper.AddGAVDirectory(directoryName, Convert.ToInt32(new Misc().Decrypt(parentId)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteFileOrDirectoryGAV(string id, bool isFolder)
        {
            InventoryHelper inventoryHelper = new InventoryHelper();
            var response = inventoryHelper.DeleteFileOrDirectoryGAV(Convert.ToInt32(new Misc().Decrypt(id)), isFolder);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult validateAccessForGAVDocs(string password, string docName)
        {


            bool response = new InventoryHelper().validateAccessForGAVDocs(password, docName);
            if (response == true)
            {
                switch (docName)
                {
                    case "Accounting Data":
                        {
                            Session["IsAuthorizedForAccData"] = true;
                            break;
                        }
                    case "Premire-Millionair Fuel Data":
                        {
                            Session["IsAuthorizedForPreMillionairFuelData"] = true;
                            break;
                        }
                    case "Guardian Avionics Data":
                        {
                            Session["IsAuthorizedForGuardianAvionicsData"] = true;
                            break;
                        }
                }
            }
            else
            {
                switch (docName)
                {
                    case "Accounting Data":
                        {
                            Session["IsAuthorizedForAccData"] = false;
                            break;
                        }
                    case "Premire-Millionair Fuel Data":
                        {
                            Session["IsAuthorizedForPreMillionairFuelData"] = false;
                            break;
                        }
                    case "Guardian Avionics Data":
                        {
                            Session["IsAuthorizedForGuardianAvionicsData"] = false;
                            break;
                        }
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);


        }

        public ActionResult IsAuthorizedForGav(string docName)
        {
            switch (docName)
            {
                case "Accounting Data":
                    {
                        if (Session["IsAuthorizedForAccData"] == null)
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (Convert.ToBoolean(Session["IsAuthorizedForAccData"]))
                            {
                                return Json(true, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(false, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                case "Premire-Millionair Fuel Data":
                    {
                        if (Session["IsAuthorizedForPreMillionairFuelData"] == null)
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (Convert.ToBoolean(Session["IsAuthorizedForPreMillionairFuelData"]))
                            {
                                return Json(true, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(false, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                case "Guardian Avionics Data":
                    {
                        if (Session["IsAuthorizedForGuardianAvionicsData"] == null)
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (Convert.ToBoolean(Session["IsAuthorizedForGuardianAvionicsData"]))
                            {
                                return Json(true, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(false, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
            }
            return Json(false, JsonRequestBehavior.AllowGet);

        }

        #endregion GAVBigDog

        #region PartIndex

        public ActionResult PartIndex()
        {
            ViewBag.ControllerName = "Inventory";
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.PageHeaderTitle = "PartIndex";
            return View();
        }

        public JsonResult GetPartIndexInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            List<PartIndexModel> partIndexList = new List<PartIndexModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;

            partIndexList = new InventoryHelper().GetPartIndexInChunk(length, start, search, out totalCount, out totalRowsAfterFilter);

            var response = new { data = partIndexList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPartIndexById(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.GetPartIndexById(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPartIndex(string index, string description)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.AddPartIndex(index, description);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditPartIndex(string index, string description, string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.EditPartIndex(index, description, Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePartIndex(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.DeletePartIndex(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        #endregion PartIndex

        #region InventoryReduction

        public JsonResult GetInventoryReductionInChunk(string draw, int length, int start)
        {
            string search = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            List<InventoryReductionModel> reductionList = new List<InventoryReductionModel>();
            int totalCount = 0;
            int totalRowsAfterFilter = 0;

            reductionList = new InventoryHelper().GetInventoryReductionInChunk(length, start, search, sortColumnName, sortDirection, out totalCount, out totalRowsAfterFilter);

            var response = new { data = reductionList, draw = draw, recordsFiltered = totalRowsAfterFilter, recordsTotal = totalCount };
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public ActionResult InventoryReduction()
        {
            ViewBag.UserType = GetUserTypeFromSession();
            ViewBag.ControllerName = "Inventory";
            ViewBag.PageHeaderTitle = "Inventory Reduction";
            InventoryHelper inventoryHelper = new InventoryHelper();
            ViewBag.PartList = JsonConvert.SerializeObject(inventoryHelper.GetNonSalablePartNumber());
            return View();
        }

        public ActionResult GetInventoryReductionById(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            GeneralResponse generalResponse = new GeneralResponse();
            var response = invHelper.GetInventoryReductionById(Convert.ToInt32(new Misc().Decrypt(id)), out generalResponse);
            return Json(new { response, generalResponse }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateInventoryReduction(InventoryReductionModel invReductionModel)
        {
            invReductionModel.Id = Convert.ToInt32(new Misc().Decrypt(invReductionModel.encId));
            InventoryHelper invHelper = new InventoryHelper();
            GeneralResponse generalResponse = new GeneralResponse();
            var response = invHelper.UpdateInventoryReduction(invReductionModel);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddInventoryReduction(InventoryReductionModel model)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.AddInventoryReduction(model);
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteInventoryReduction(string id)
        {
            InventoryHelper invHelper = new InventoryHelper();
            var response = invHelper.DeleteInventoryReduction(Convert.ToInt32(new Misc().Decrypt(id)));
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        #endregion InventoryReduction

    }


}
