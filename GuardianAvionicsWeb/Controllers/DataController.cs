﻿using GA.ApplicationLayer;
using GA.DataTransfer;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GA.Common;
using GA.DataTransfer.Classes_for_Services;
using System.Web;
using GA.DataTransfer.Classes_for_Web;
using Newtonsoft.Json;
using PagedList;
using System.Linq;
//using NLog;
using System.Net;
using System.Xml;
using GA.DataLayer;

namespace GuardianAvionicsWeb.Controllers
{
    [HandleError(View = "Error")]

    public class DataController : Controller
    {
        //// private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #region Airframe Data

        public ActionResult IndexTemp()
        {

            return View();

        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]

        [SessionExpireFilterAttribute]
        public ActionResult Index(string aircraftId)
        {
            if (!string.IsNullOrEmpty(aircraftId))
            {
                try
                {
                    aircraftId = new Misc().Decrypt(aircraftId);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Login", "Home");
                }
            }

            ViewBag.Master = "_UserLayout.cshtml";
            ViewBag.PageHeaderTitle = "Flight Logs";
            if (Session["UserType"] != null)
            {
                ViewBag.User = Session["UserType"].ToString();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(profileId);
            var helper = new UnitDataHelper();

            GraphModel model = helper.GetAircraftListForGraph(profileId);
            model.dataLogList = new List<DataLogListing>();
            ViewBag.AircraftId = 0;
            if (!string.IsNullOrEmpty(aircraftId))
            {
                var flightListGraph = new UnitDataHelper().GetFlightListGraph(Convert.ToInt32(aircraftId), profileId);
                model.flightList = flightListGraph.flightList;
                model.flightList.ForEach(f => { f.TId = new Misc().Encrypt(f.Id.ToString()); f.Id = 0; });
                model.AircraftId = Convert.ToInt32(aircraftId);
                ViewBag.AircraftId = Convert.ToInt32(aircraftId);
            }

            ViewBag.AID = new Misc().Encrypt(aircraftId);
            if (model.AircraftNNumberList.Count == 1)
                ViewBag.IsSingleAircraft = true;

            @ViewBag.IsJPI = true;
            @ViewBag.IsGarmin = false;
            @ViewBag.IsULP = false;
            return View("Index", model);
        }

        public ActionResult DashBoardIndex(string Pilotlogid)
        {
            
            if (!string.IsNullOrEmpty(Pilotlogid))
            {
                try
                {
                    Pilotlogid = new Misc().Decrypt(Pilotlogid);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            GuardianAvionicsEntities context = new GuardianAvionicsEntities();
            int pilotlogid = Convert.ToInt32(Pilotlogid);
            string aircraftId = context.PilotLogs.Where(x => x.Id == pilotlogid).Select(x => x.AircraftId).FirstOrDefault().ToString();

            ViewBag.PageHeaderTitle = "Flight Logs";
            if (Session["UserType"] != null)
            {
                ViewBag.User = Session["UserType"].ToString();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Master = "_UserLayout.cshtml";
            if (Session["UserType"].ToString() == Enumerations.UserType.Admin.ToString())
            {
                ViewBag.Master = "_Layout.cshtml";
            }

            int aircraftidv = Convert.ToInt32(aircraftId);
            int profileId = Convert.ToInt32(context.AircraftProfiles.Where(x => x.Id == aircraftidv).Select(y => y.OwnerProfileId).FirstOrDefault());
            //int profileId = Convert.ToInt32(Session["ProfileId"]);
            Session["UserName"] = new UserHelper().GetUserNameByProfileId(profileId);
            var helper = new UnitDataHelper();

            GraphModel model = helper.GetAircraftListForGraph(profileId);
            model.dataLogList = new List<DataLogListing>();
            ViewBag.AircraftId = 0;
            if (!string.IsNullOrEmpty(aircraftId))
            {
                var flightListGraph = new UnitDataHelper().GetFlightListGraph(Convert.ToInt32(aircraftId), profileId);
                model.flightList = flightListGraph.flightList;
                model.flightList.ForEach(f => { f.TId = new Misc().Encrypt(f.Id.ToString()); f.Id = 0; });
                model.AircraftId = Convert.ToInt32(aircraftId);
                ViewBag.AircraftId = Convert.ToInt32(aircraftId);
            }

            ViewBag.AID = new Misc().Encrypt(aircraftId);
            if (model.AircraftNNumberList.Count == 1)
                ViewBag.IsSingleAircraft = true;
            @ViewBag.IsJPI = true;
            @ViewBag.IsGarmin = false;
            @ViewBag.IsULP = false;
            return View("Index", model);
        }


        [SessionExpireFilterAttribute]
        public ActionResult ListAllJPIData(int? SortFor, int? PilotLogId, bool? FromPilotPage)
        {
            try
            {
                if (Session["UserType"] != null)
                {
                    ViewBag.User = Session["UserType"].ToString();

                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }

                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;
                ViewBag.DataFor = Constants.JpiAircraft;
                ViewBag.ProfileId = Convert.ToInt32(Session["ProfileId"]);
                ViewBag.FromPilotLogPage = FromPilotPage ?? false;
                ViewBag.aircraftRegNo = "Flight Listing";


                if (SortFor == 0 && PilotLogId != null)
                {
                    ViewBag.DataFor = Constants.JpiFlight;
                    ViewBag.PilotLogId = PilotLogId;
                    //TempData["PilotLogId"] = PilotLogId;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            var m = new List<DataLogListing>();
            return View(m.ToPagedList(pageNumber: 1, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }

        //[SessionExpireFilterAttribute]
        public ActionResult ListJpiMobile(int profileId)
        {
            //Session["ProfileId"] = profileId;
            ViewBag.DataFor = Constants.JpiAircraft;
            ViewBag.ProfileId = profileId;
            ViewBag.IsJPI = false;
            ViewBag.IsGarmin = false;
            ViewBag.IsULP = false;
            var model = new List<DataLogListing>();
            ViewBag.ActionMethod = "ListJpiMobile";

            return View("ListJpiMobile", model.ToPagedList(pageNumber: 1, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }

        public ActionResult DynamicGraph()
        {
            return View();
        }

        // [SessionExpireFilterAttribute]
        /// <summary>
        /// Fetch all the data for user and return in _JPIDataPartialTable partial view
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public ActionResult ListJpi(int pageNo, int profileId)
        {
            //int profileId = (int)Session["ProfileId"];

            var objHelper = new UnitDataHelper();
            var totalCount = 0;
            var jpi = objHelper.ListJpiData(profileId, pageNo, out totalCount);

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            ViewBag.DataFor = Constants.JpiAll;
            ViewBag.LabelText = GAResource.ShowingCompleteEngineDataMessage;

            Session["ShowingDataFor"] = Constants.JpiAll;
            ViewBag.ProfileId = profileId;
            ViewBag.ActionMethod = "ListJpi";
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }



        // [SessionExpireFilterAttribute]
        public ActionResult ListJpiDataLastFlight(int pageNo, int profileId)
        {
            //int profileId = (int)Session["ProfileId"];

            var objHelper = new UnitDataHelper();
            var totalCount = 0;
            var labelText = "";
            int pilotLogIdOut = 0;

            var jpi = objHelper.ListJpiDataLastFlight(profileId, pageNo, out labelText, out pilotLogIdOut);

            // ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            ViewBag.DataFor = Constants.JpiLastFlight;
            ViewBag.LabelText = labelText;

            Session["PilotLogId"] = pilotLogIdOut;
            Session["ShowingDataFor"] = Constants.JpiFlight;
            ViewBag.ProfileId = profileId;
            ViewBag.ActionMethod = "ListJpiDataLastFlight";
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }


        // [SessionExpireFilterAttribute]
        public ActionResult ListJpiForLastAircraft(int pageNo, int profileId)
        {
            //int profileId = (int)Session["ProfileId"];

            var objHelper = new UnitDataHelper();
            var totalCount = 0;
            var jpi = objHelper.ListJpiData(profileId, pageNo, out totalCount);

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            ViewBag.DataFor = Constants.JpiAll;
            ViewBag.LabelText = GAResource.ShowingCompleteEngineDataMessage;

            Session["ShowingDataFor"] = Constants.JpiAll;
            Session["PilotLogId"] = profileId;
            ViewBag.ProfileId = profileId;
            ViewBag.ActionMethod = "ListJpi";
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }

        // [SessionExpireFilterAttribute]
        //public ActionResult ListJpiPilot(int pageNo, int pilotLogId, int profileId, string IsCallFromPilotPage, int totalRecordFetch, int timeInterval, string StartDate, int requestDataForLevel)
        public ActionResult ListJpiPilot(int pageNo, string pid, int profileId, string IsCallFromPilotPage, int totalRecordFetch, int timeInterval, string StartDate, int requestDataForLevel)
        {


            int pilotLogId = 0;
            try
            {
                pilotLogId = Convert.ToInt32(new Misc().Decrypt(pid));
            }
            catch (Exception ex)
            {
                pilotLogId = Convert.ToInt32(pid);
                //return RedirectToAction("Login", "Home");
            }
            DateTime? startDateForInnerData = null;
            if (!string.IsNullOrEmpty(StartDate))
            {
                startDateForInnerData = Convert.ToDateTime(StartDate);
            }

            if (profileId == 0)
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }

            var objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            var pilotLogIdOut = 0;
            string engineCommandType = "";
            bool isDataAvailableForFlight = false;
            string aircraftImageURL = "";
            string pilotImageURL = string.Empty;
            string pilotName = string.Empty;
            int flightTimeInterval = 0;
            var context = new GA.DataLayer.GuardianAvionicsEntities();
            dynamic jpi;
            bool isAircraftWithoutUnit = (context.PilotLogs.FirstOrDefault(p => p.Id == pilotLogId).AeroUnitMasterId == null);
            if (isAircraftWithoutUnit)
            {
                jpi = (List<GA.CommonForParseDataFile.DataLogWithoutUnit>)objHelper.ListJpiDataForAircraftWithoutUnit(profileId, pageNo, pilotLogId, totalRecordFetch, timeInterval, out totalCount, out labelText, out pilotLogIdOut, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, startDateForInnerData, requestDataForLevel);
            }
            else
            {
                jpi = (List<DataLogListing>)objHelper.ListJpiData(profileId, pageNo, pilotLogId, totalRecordFetch, timeInterval, out totalCount, out labelText, out pilotLogIdOut, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, startDateForInnerData, requestDataForLevel);
            }

            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {

                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");

            }
            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiFlight;
            Session["PilotLogId"] = pilotLogId;
            ViewBag.ProfileId = profileId;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.PilotLogId = pilotLogId;
            ViewBag.ActionMethod = "ListJpiPilot";
            ViewBag.VBIsCallFromPilotPage = IsCallFromPilotPage;

            bool IsDataAvailable = true;
            if (jpi.Count < 60)
            {
                IsDataAvailable = false;
            }


            string flightNo = string.Empty;
            string aircraftNNumber = string.Empty;
            if (jpi != null)
            {
                if (jpi.Count > 0)
                {
                    if (isAircraftWithoutUnit)
                    {
                        flightNo = ((List<GA.CommonForParseDataFile.DataLogWithoutUnit>)jpi).FirstOrDefault().FlightNumber;
                        aircraftNNumber = ((List<GA.CommonForParseDataFile.DataLogWithoutUnit>)jpi).FirstOrDefault().AircraftNNumber;
                    }
                    else
                    {
                        flightNo = ((List<DataLogListing>)jpi).FirstOrDefault().FlightNumber;
                        aircraftNNumber = ((List<DataLogListing>)jpi).FirstOrDefault().AircraftNNumber;
                    }

                }
            }
            //return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: jpi.Count));
            return Json(new { jpiData = jpi, IsJPI = engineCommandType.Contains("JPI"), IsGarmin = engineCommandType.Contains("Garmin"), IsULP = engineCommandType.Contains("ULP"), totalRecordFetch = (totalRecordFetch + 60), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = isAircraftWithoutUnit, flightTimeInterval = flightTimeInterval }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ListJpiPilotLive(int pilotLogId, int profileId, string IsCallFromPilotPage, int lastRecordId, int? timeInterval, DateTime? StartDate, int? requestDataForLevel)
        {


            if (profileId == 0)
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            var objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            var pilotLogIdOut = 0;
            string engineCommandType = "";
            bool isDataAvailableForFlight = false;
            string aircraftImageURL = "";
            string pilotImageURL = string.Empty;
            string pilotName = string.Empty;
            int flightTimeInterval = 0;
            int lastRecordFetchId = 0;
            bool isFlightFinished = false;
            string route = "";
            string wayPoint = "";
            List<LatLong> latLongList = new List<LatLong>();
            List<MFDSettings> MFDList = new List<MFDSettings>();
            int[] PLHeader;

            var context = new GA.DataLayer.GuardianAvionicsEntities();
            dynamic jpi;
            bool isAircraftWithoutUnit = (context.PilotLogs.FirstOrDefault(p => p.Id == pilotLogId).AeroUnitMasterId == null);
            if (isAircraftWithoutUnit)
            {
                jpi = (List<GA.CommonForParseDataFile.DataLogWithoutUnit>)objHelper.ListJpiDataForAircraftWithoutUnitLive(profileId, pilotLogId, lastRecordId, timeInterval, StartDate, requestDataForLevel, out totalCount, out labelText, out pilotLogIdOut, out engineCommandType, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, out lastRecordFetchId, out isFlightFinished, out latLongList, out route, out wayPoint, out MFDList, out PLHeader);
            }
            else
            {
                jpi = (List<DataLogListing>)objHelper.ListJpiDataLive(profileId, pilotLogId, lastRecordId, timeInterval, StartDate, requestDataForLevel, out totalCount, out labelText, out pilotLogIdOut, out engineCommandType, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, out lastRecordFetchId, out isFlightFinished, out latLongList, out route, out wayPoint, out MFDList, out PLHeader);
            }


            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {

                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");

            }



            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiFlight;
            Session["PilotLogId"] = pilotLogId;
            ViewBag.ProfileId = profileId;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.PilotLogId = pilotLogId;
            ViewBag.ActionMethod = "ListJpiPilot";
            ViewBag.VBIsCallFromPilotPage = IsCallFromPilotPage;

            bool IsDataAvailable = true;
            if (jpi.Count < 60)
            {
                IsDataAvailable = false;
            }
            string flightNo = string.Empty;
            string aircraftNNumber = string.Empty;
            if (lastRecordId == 0)
            {

                if (jpi != null)
                {
                    if (jpi.Count > 0)
                    {
                        if (isAircraftWithoutUnit)
                        {
                            flightNo = ((List<GA.CommonForParseDataFile.DataLogWithoutUnit>)jpi).FirstOrDefault().FlightNumber;
                            aircraftNNumber = ((List<GA.CommonForParseDataFile.DataLogWithoutUnit>)jpi).FirstOrDefault().AircraftNNumber;
                        }
                        else
                        {
                            flightNo = ((List<DataLogListing>)jpi).FirstOrDefault().FlightNumber;
                            aircraftNNumber = ((List<DataLogListing>)jpi).FirstOrDefault().AircraftNNumber;
                        }
                    }
                }
            }
            bool calibrationLeft = false;
            bool calibrationRight = false;
            var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);
            var airCraft = context.AircraftProfiles.Where(x => x.Id == plog.AircraftId).FirstOrDefault();
            try
            {
                if (airCraft.InstalledEngine.Contains("EM100"))
                {
                    var calleft = context.FuelDetails.Where(x => x.aircraftId == plog.AircraftId && x.fuelsideType == "left").FirstOrDefault();
                    var calright = context.FuelDetails.Where(x => x.aircraftId == plog.AircraftId && x.fuelsideType == "right").FirstOrDefault();
                    calibrationLeft = calleft == null ? true : false;
                    calibrationRight = calright == null ? true : false;
                }
            }

            catch (Exception ex)
            {

            }
            if (System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + airCraft.Id + ".xml"))
            {
                string xmlfile = ConfigurationReader.EngineConfigXMLFilePath + @"\" + airCraft.Id + ".xml";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(xmlfile);
                var CylinderCount = xmldoc.SelectNodes("plist/dict/dict/dict/integer");
                var STALL = CylinderCount[2].InnerText;
                var VMC = CylinderCount[3].InnerText;
                var VNE = CylinderCount[4].InnerText;
                var VX = CylinderCount[5].InnerText;
                var VY = CylinderCount[6].InnerText;
                //var CylinderCountVal = xmldoc.SelectNodes("plist/dict/integer");
                //var CylinderCount = Convert.ToInt32(CylinderCountVal[0].InnerText);
            }


            var jsonResult = Json(new { calibrationLeft = calibrationLeft, calibrationRight = calibrationRight, jpiData = jpi, IsJPI = engineCommandType.Contains("JPI"), IsGarmin = engineCommandType.Contains("Garmin"), IsULP = engineCommandType.Contains("ULP"), lastRecordFetchId = (lastRecordFetchId), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = isAircraftWithoutUnit, flightTimeInterval = flightTimeInterval, isFlightFinished = isFlightFinished, latLongList = latLongList, route = route, wayPoint = wayPoint, MFDList = MFDList, PLHeader = PLHeader }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

            //return Json(new { jpiData = jpi, IsJPI = engineCommandType.Contains("JPI"), IsGarmin = engineCommandType.Contains("Garmin"), IsULP = engineCommandType.Contains("ULP"), lastRecordFetchId = (lastRecordFetchId), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = isAircraftWithoutUnit, flightTimeInterval = flightTimeInterval }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListJpiPilotViewMore(int pageNo, int pilotLogId, int profileId, string IsCallFromPilotPage, int totalRecordFetch, int timeInterval)
        {
            if (profileId == 0)
            {
                profileId = Convert.ToInt32(Session["ProfileId"]);
            }
            var objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            var pilotLogIdOut = 0;
            string engineCommandType = "";
            bool isDataAvailableForFlight = false;
            string aircraftImageURL = "";
            string pilotImageURL = string.Empty;
            string pilotName = string.Empty;
            int flightTimeInterval = 0;
            var jpi = objHelper.ListJpiData(profileId, pageNo, pilotLogId, totalRecordFetch, timeInterval, out totalCount, out labelText, out pilotLogIdOut, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, null, 0);

            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {
                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");

            }

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiFlight;
            Session["PilotLogId"] = pilotLogId;
            ViewBag.ProfileId = profileId;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.PilotLogId = pilotLogId;
            ViewBag.ActionMethod = "ListJpiPilot";
            ViewBag.VBIsCallFromPilotPage = IsCallFromPilotPage;

            bool IsDataAvailable = true;
            if (jpi.Count < 60)
            {
                IsDataAvailable = false;
            }
            return Json(new { jpiData = jpi, IsJPI = engineCommandType.Contains("JPI"), IsGarmin = engineCommandType.Contains("Garmin"), IsULP = engineCommandType.Contains("ULP"), totalRecordFetch = (totalRecordFetch + 60), IsDataAvailable = IsDataAvailable }, JsonRequestBehavior.AllowGet); ;
        }
        // [SessionExpireFilterAttribute]
        public ActionResult ListJpiPilotForMultipleLogId(int pageNo, string LogIds, int profileId)
        {
            var objHelper = new UnitDataHelper();
            int[] ids = { };
            try
            {
                ids = LogIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            if (ids.Length == 1)
            {
                return RedirectToAction("ListJpiPilot", new { pageNo = pageNo, pilotLogId = ids[0], profileId = profileId, IsCallFromPilotPage = "false" });
            }

            int totalCount;
            string labelText;
            string engineCommandType = "";
            var jpi = objHelper.ListJpiData(profileId, pageNo, ids, out totalCount, out labelText, out engineCommandType);

            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {
                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");
            }
            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["PilotLogMultipleIds"] = ids;

            Session["ShowingDataFor"] = Constants.JpiMultipleFlight;
            ViewBag.ProfileId = profileId;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.logIds = LogIds;
            ViewBag.ActionMethod = "ListJpiPilotForMultipleLogId";
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }

        // [SessionExpireFilterAttribute]
        public ActionResult ListJpiAircraft(int pageNo, int aircraftId, int profileId)
        {
            UnitDataHelper objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            string engineCommandType = string.Empty;

            List<DataLogListing> jpi = objHelper.ListJpiDataAircraft(profileId, pageNo, aircraftId, out totalCount, out labelText, out engineCommandType);
            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {
                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");
            }
            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;
            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiAircraft;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiAircraft;
            Session["AircraftId"] = aircraftId;

            ViewBag.IsNoData = totalCount == 0;
            ViewBag.ProfileId = profileId;
            ViewBag.AircraftId = aircraftId;
            ViewBag.ActionMethod = "ListJpiAircraft";
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }

        //[SessionExpireFilterAttribute]
        public ActionResult FlightList(int pageNo, int profileId)
        {
            UnitDataHelper objHelper = new UnitDataHelper();

            int totalCount = 0;
            List<DataLogListing> model = objHelper.GetFlightList(profileId, pageNo, out totalCount);

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;
            ViewBag.ProfileId = profileId;
            ViewBag.AircraftId = null;
            ViewBag.aircraftRegNo = "Flight Listing";
            return PartialView("_FlightList", model.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForFlightList)));
        }

        public ActionResult FlightListByAircraftId(int pageNo, int profileId, int aircraftId)
        {
            UnitDataHelper objHelper = new UnitDataHelper();

            int totalCount = 0;
            string aircraftRegNo = string.Empty;
            List<DataLogListing> model = objHelper.GetFlightListByAircraftId(profileId, aircraftId, out aircraftRegNo);

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;
            ViewBag.ProfileId = profileId;
            ViewBag.AircraftId = aircraftId;
            ViewBag.aircraftRegNo = aircraftRegNo;
            return PartialView("_FlightList", model.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForFlightList)));
        }

        //[SessionExpireFilterAttribute]
        public ActionResult AircraftList(int pageNo, int profileId)
        {
            UnitDataHelper objHelper = new UnitDataHelper();
            List<DataLogListing> model = objHelper.GetAircraftList(profileId);
            ViewBag.ProfileId = profileId;
            return PartialView("_AircraftListingPartial", model.ToPagedList(pageNumber: 1, pageSize: (model.Count <= 0) ? 1 : model.Count));
        }

        // [SessionExpireFilterAttribute]
        public ActionResult ListJpiDateTime(int pageNo, string toDate, string toTime, string fromDate, string fromTime, int profileId)
        {
            UnitDataHelper objHelper = new UnitDataHelper();

            int totalCount = 0;

            var labelText = string.Empty;

            List<DataLogListing> jpi = objHelper.ListJpiDataDateTime(profileId, pageNo, Convert.ToString(toDate),
                Convert.ToString(toTime), Convert.ToString(fromDate), Convert.ToString(fromTime), out totalCount, out labelText);

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load sorted data 
            ViewBag.DataFor = Constants.JpiDateTime;
            ViewBag.LabelText = labelText;

            //public ActionResult ListJpiDateTime(int pageNo, string toDate, string toTime, string fromDate, string fromTime, int profileId)

            Session["ShowingDataFor"] = Constants.JpiDateTime;
            Session["ToDate"] = toDate;
            Session["ToTime"] = toTime;
            Session["FromDate"] = fromDate;
            Session["FromTime"] = fromTime;

            ViewBag.IsNoData = totalCount == 0;
            ViewBag.ProfileId = profileId;

            ViewBag.ActionMethod = "ListJpiDateTime";
            ViewBag.IsCallFromPilotLog = "false";
            return PartialView("_JPIDataPartialTable", jpi.ToPagedList(pageNumber: pageNo, pageSize: Convert.ToInt32(ConfigurationReader.NoOfRowsForAirframeDataLog)));
        }


        public ActionResult ExportDatalog(int profileId, int pilotlogId)
        {
            var helper = new UnitDataHelper();
            string fileName;
            byte[] bArray = helper.ExportAllDataToPcPilotLog(pilotlogId, profileId, out fileName);

            var contextDisposition = new System.Net.Mime.ContentDisposition
            {
                FileName = fileName,
                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", contextDisposition.ToString());

            if (bArray != null && bArray.Length > 0)
                return File(bArray, Constants.ContentTypeForXlsx);

            return Content(GAResource.FileCanNotBeDownloadedMessage);
        }

        [SessionExpireFilterAttribute]
        public ActionResult ExportToPc()
        {
            var helper = new UnitDataHelper();
            try
            {
                // get session variables
                // check session
                if (Session["ShowingDataFor"] == null)
                {
                    // show all data.
                    return Content(GAResource.FileCanNotBeDownloadedMessage);
                }

                switch (Convert.ToString(Session["ShowingDataFor"]))
                {
                    case Constants.JpiAll:
                        {
                            string fileName;
                            byte[] bArray = helper.ExportAllDataToPc(Convert.ToInt32(Session["ProfileId"]), out fileName);

                            var cd = new System.Net.Mime.ContentDisposition
                            {
                                FileName = fileName,
                                Inline = true,
                            };
                            Response.AppendHeader("Content-Disposition", cd.ToString());

                            if (bArray != default(byte[]))
                                return File(bArray, Constants.ContentTypeForXlsx);

                        }
                        break;

                    case Constants.JpiAircraft:
                        {
                            if (Session["AircraftId"] != null)
                            {
                                // get bytes of file to be downloaded.
                                string fileName = string.Empty;
                                byte[] bArray = helper.ExportAllDataToPcAircraft(Convert.ToInt32(Session["AircraftId"]), Convert.ToInt32(Session["ProfileId"]), out fileName);

                                var contextDisposition = new System.Net.Mime.ContentDisposition
                                {
                                    FileName = fileName,
                                    Inline = true,
                                };
                                Response.AppendHeader("Content-Disposition", contextDisposition.ToString());

                                if (bArray != null && bArray.Length > 0)
                                    return File(bArray, Constants.ContentTypeForXlsx);
                            }

                        }
                        break;
                    case Constants.JpiFlight:
                        {
                            if (Session["PilotLogId"] != null)
                            {
                                // get bytes of file to be downloaded.
                                string fileName;
                                byte[] bArray = helper.ExportAllDataToPcPilotLog(Convert.ToInt32(Session["PilotLogId"]), Convert.ToInt32(Session["ProfileId"]), out fileName);

                                var contextDisposition = new System.Net.Mime.ContentDisposition
                                {
                                    FileName = fileName,
                                    Inline = true,
                                };
                                Response.AppendHeader("Content-Disposition", contextDisposition.ToString());

                                if (bArray != null && bArray.Length > 0)
                                    return File(bArray, Constants.ContentTypeForXlsx);
                            }
                        }
                        break;

                    case Constants.JpiDateTime:
                        {
                            if (Session["ToDate"] != null &&
                                Session["ToTime"] != null &&
                                Session["FromDate"] != null &&
                                Session["FromTime"] != null)
                            {
                                // get bytes of file to be downloaded.
                                string fileName;
                                byte[] bArray = helper.ExportAllDataToPcDateTime((String)Session["ToDate"], (String)Session["ToTime"],
                                        (String)Session["FromDate"], (String)Session["FromTime"], Convert.ToInt32(Session["ProfileId"]), out fileName);

                                var contextDisposition = new System.Net.Mime.ContentDisposition
                                {
                                    FileName = fileName,
                                    Inline = true,
                                };
                                Response.AppendHeader("Content-Disposition", contextDisposition.ToString());

                                if (bArray != null && bArray.Length > 0)
                                    return File(bArray, Constants.ContentTypeForXlsx);
                            }

                        }
                        break;

                    case Constants.JpiMultipleFlight:
                        {
                            if (Session["PilotLogMultipleIds"] != null)
                            {
                                // get bytes of file to be downloaded.
                                string fileName;
                                byte[] bArray = helper.ExportAllDataToPcPilotLog(Session["PilotLogMultipleIds"] as int[], Convert.ToInt32(Session["ProfileId"]), out fileName);

                                var contextDisposition = new System.Net.Mime.ContentDisposition
                                {
                                    FileName = fileName,
                                    Inline = true,
                                };
                                Response.AppendHeader("Content-Disposition", contextDisposition.ToString());

                                if (bArray != null && bArray.Length > 0)
                                    return File(bArray, Constants.ContentTypeForXlsx);
                            }
                        }
                        break;
                }

                return Content(GAResource.FileCanNotBeDownloadedMessage);

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, "Export of data to pc.");
                //logger.Fatal("Exception Export of data to pc.", e);
                return Content(GAResource.FileCanNotBeDownloadedMessage);
            }
        }


        #endregion Airframe Data

        #region Documents


        /// <summary>
        /// Gets document for current user
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilterAttribute]
        public ActionResult Document(int? pageNo)
        {
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            DocumentResponseModelWeb res = new DocumentResponseModelWeb();
            res = new UnitDataHelper().GetDocumentForUser(new DocumnetRequestModel { ProfileId = profileId });
            res.ListOfDocuments.ForEach(s => { s.LastUpdateDate = Misc.dateToMMDDYYYY(s.LastUpdateDate.Split(' ')[0]); });
            //var DocumentModelWeb = new do
            var pageNumber = (pageNo ?? 1);
            return View("Document", res.ListOfDocuments.ToPagedList(pageNumber: pageNumber, pageSize: Constants.PageSizeTable));
        }

        [SessionExpireFilterAttribute]
        public JsonResult CheckDocumentExist(string docName)
        {
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            int docId = 0;
            bool msg = new UnitDataHelper().CheckDocumentForDuplicacy(docName, profileId, out docId);
            return Json(new { msg = msg, docId = docId }, JsonRequestBehavior.AllowGet);
        }

        //[SessionExpireFilterAttribute]
        //public ActionResult SaveDocument(HttpPostedFileBase file, string replace)
        //{
        //    int profileId = Convert.ToInt32(Session["ProfileId"]);

        //    if (replace == Boolean.TrueString.ToLower())
        //    {
        //        // delete doc
        //        new UnitDataHelper().DeleteDocument(file, profileId);
        //    }

        //    // create new doc
        //    new UnitDataHelper().CreateNewDocument(file, profileId);

        //    return RedirectToAction("Document");
        //}

        //[SessionExpireFilterAttribute] is not required as we dont need profile Id here or any variable from session.
        /// <summary>
        /// Gets the document's url , set content type to open document on browser.
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        public ActionResult ShowDocument(string DocumentId)
        {
            string url = new UnitDataHelper().GetNameOfDocument(Convert.ToInt32(DocumentId));

            var arr = url.Split('.');

            var contentType = new UnitDataHelper().GetContextTypeByFileExtention("." + arr[arr.Length - 1]);

            using (WebClient wc = new WebClient())
            {
                var byteArr = wc.DownloadData(ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath + url);
                return File(byteArr, contentType);
            }

            //var serverPath = ConfigurationReader.DocumentPathKey;
            //var path = serverPath + @"\" + url;

            //if (!System.IO.File.Exists(serverPath + @"\" + url))
            //{
            //    return Json("File Not Found", JsonRequestBehavior.AllowGet);
            //}

            // add ContentDisposition header for proper opening of file in browser.
            var cd = new System.Net.Mime.ContentDisposition
            {
                // for example foo.bak
                //FileName = url,
                FileName = url,

                // always prompt the user for downloading, set to true if you want 
                // the browser to try to show the file inline
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            //string extention = string.Empty;
            //try
            //{
            //    extention = System.IO.Path.GetExtension(path);
            //}
            //catch (Exception e)
            //{
            //    //ExceptionHandler.ReportError(e);
            //    //logger.Fatal("Exception ", e);
            //}


            return File(ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath + url, contentType);
        }

        //[SessionExpireFilterAttribute] is not required as we dont need profile Id here or any variable from session.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        //public JsonResult DeleteDocument(int documentId)
        //{
        //    var result = new UnitDataHelper().DeleteDocumentById(documentId);
        //    return Json(result ? GAResource.DocumentDeletionSuccessMessage : GAResource.DocumentDeletionFailedMessage);
        //}


        #endregion Documents

        #region Graphs

        [SessionExpireFilterAttribute]
        public ActionResult Graphs()
        {
            if (Session["UserType"] != null)
            {
                ViewBag.User = Session["UserType"].ToString();

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            int profileId = Convert.ToInt32(Session["ProfileId"]);
            var helper = new UnitDataHelper();

            GraphModel model = helper.GetAircraftListForGraph(profileId);
            model.dataLogList = new List<DataLogListing>();

            if (model.AircraftNNumberList.Count == 1)
                ViewBag.IsSingleAircraft = true;

            @ViewBag.IsJPI = true;
            @ViewBag.IsGarmin = false;
            @ViewBag.IsULP = false;
            return View("Graph", model);
        }

        public ActionResult GraphDemo()
        {
            return View();
        }


        [SessionExpireFilterAttribute]
        public ActionResult GetFlightListForGraph(int aircraftId)
        {
            var helper = new UnitDataHelper();

            int profileId = Convert.ToInt32(Session["ProfileId"]);
            Misc objMisc = new Misc();
            GraphModel model = helper.GetFlightListGraph(aircraftId, profileId);
            model.flightList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            ViewBag.aircraftId = aircraftId;
            ViewBag.AID = new Misc().Encrypt(aircraftId.ToString());
            return PartialView("_GraphFlightListing", model);
        }

        public ActionResult DynamicGraphRecall()
        {
            var listOfData = new long[2][][];
            DateTime dt = new DateTime(2015, 2, 16, 15, 3, 0);

            DateTime dt1 = new DateTime(2015, 2, 16, 15, 4, 0);
            DateTime date = DateTime.Now;
            if (date < dt || date > dt1)
            {
                var helper = new UnitDataHelper();
                listOfData = helper.DynamicGraphRecall();
            }

            return Json(listOfData);
        }

        public ActionResult GetDynamicGraph(string flightIdList)
        {
            var listOfData = new long[2][][];
            var helper = new UnitDataHelper();
            listOfData = helper.DynamicGraphArray();
            return Json(listOfData);
        }

        public ActionResult GetDatalogForGraph(string flightIdList, string datetime, int? pageNo)
        {
            try
            {
                flightIdList = new Misc().Decrypt(flightIdList);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request");
            }

            int[] ids = { Convert.ToInt32(flightIdList) };

            string[] arr = datetime.Split(',');
            string engineCommandType = "";
            DateTime dt = new DateTime(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2]), Convert.ToInt32(arr[3]), Convert.ToInt32(arr[4]), Convert.ToInt32(arr[5]));
            var helper = new UnitDataHelper();
            var response = new List<DataLogListing>();
            response = helper.GetDatalogForGraph(ids, dt, out engineCommandType);
            if (string.IsNullOrEmpty(engineCommandType))
            {
                ViewBag.IsJPI = false;
                ViewBag.IsGarmin = false;
                ViewBag.IsULP = false;

            }
            else
            {
                ViewBag.IsJPI = engineCommandType.Contains("JPI");
                ViewBag.IsGarmin = engineCommandType.Contains("Garmin");
                ViewBag.IsULP = engineCommandType.Contains("ULP");
            }
            ViewBag.ActionMethod = "GetDatalogForGraph";
            ViewBag.FlightIds = flightIdList;
            ViewBag.dateTimeAtPoint = datetime;

            DateTime dtFrom = dt.AddMinutes(-5);
            DateTime dtTo = dt.AddMinutes(5);
            ViewBag.HeadingJpiTable = "Showing Datalog from " + Misc.GetStringOnlyDateUS(dtFrom) + " " + Convert.ToDateTime(dtFrom).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture) + " To " + Misc.GetStringOnlyDateUS(dtTo) + " " + Convert.ToDateTime(dtTo).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            return PartialView("_JPIDataPartialGraph", response);
        }

        [SessionExpireFilterAttribute]
        public ActionResult GetGraph(string flightId)
        {
            bool isDemoUser = false;
            if (Session["UserType"].ToString() == Enumerations.UserType.DemoUser.ToString())
            {
                isDemoUser = true;
                flightId = "0";
            }
            else
            {
                try
                {
                    flightId = new Misc().Decrypt(flightId);
                }
                catch (Exception ex)
                {
                    return Json("Invalid Request");
                }
            }
            var helper = new UnitDataHelper();
            int[] ids = { Convert.ToInt32(flightId) };
            try
            {
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception flightIdList= " + flightId, e);
            }

            //  var aaa = new List<MapData>();
            var listOfData = new double[18][][];
            for (int i = 0; i < 18; i++)
            {
                listOfData[i] = new double[2][];
            }

            if (ids.Length > 0)
            {
                listOfData = helper.GetGraphDataDoubleArray(ids, isDemoUser);
            }
            else
            {

            }
            //logger.Fatal("Start End " + DateTime.Now);
            //var ss = Newtonsoft.Json.JsonConvert.SerializeObject(listOfData);
            // return Json(listOfData);
            return new ContentResult()
            {
                Content = Newtonsoft.Json.JsonConvert.SerializeObject(listOfData),
                ContentType = "application/json",
            };
        }

        [SessionExpireFilterAttribute]
        public ActionResult GetGoogleMapsForFlight(string flightIdList)
        {

            bool isDemoUser = false;
            if (Session["UserType"].ToString() == Enumerations.UserType.DemoUser.ToString())
            {
                isDemoUser = true;
                flightIdList = "0";
            }
            else
            {
                try
                {
                    flightIdList = new Misc().Decrypt(flightIdList);
                }
                catch (Exception ex)
                {
                    return Json("Invalid Request");
                }
            }

            int[] flightIds = new int[1];
            try
            {
                flightIds[0] = Convert.ToInt32(flightIdList);
            }
            catch (Exception e)
            {
                return Json("Error");
            }

            var helper = new UnitDataHelper();

            string filePath = "";
            if (isDemoUser)
            {
                // filePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "demoFlight.kml";
                filePath = "https://s3-us-west-2.amazonaws.com/guardianavionics/GuardianLive/KmlFiles/demoFlight.kml";
            }
            else
            {
                filePath = helper.GetUrlForHostedKmlFile(flightIds);
            }


            double[] latLong = helper.GetLatLongForFlight(flightIds, isDemoUser);

            return Json(new
            {
                filePath = filePath,
                latitude = latLong[0],
                longitude = latLong[1],
            });
        }

        [SessionExpireFilterAttribute]
        public ActionResult DownloadKmlFileForFlight(string flightIdList)
        {
            string url = "";
            if (Session["UserType"].ToString() == Enumerations.UserType.DemoUser.ToString())
            {
                url = (ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "demoFlight.kml");
                return Json(new { url }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                flightIdList = new Misc().Decrypt(flightIdList);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request", JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(flightIdList))
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

            int[] flightIds = { Convert.ToInt32(flightIdList) };


            var helper = new UnitDataHelper();
            url = helper.GetUrlDownloadKmlFileForMultipleFlights(flightIds);
            var fileName = helper.GetServerHostedUrlKmlFile(flightIds);

            return Json(new { url }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilterAttribute] //gunjan
        public ActionResult DownloadFdrFileForFlight(string flightIdList)
        {
            string url = "";
            if (Session["UserType"].ToString() == Enumerations.UserType.DemoUser.ToString())
            {
                url = (ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "demoFlight.fdr");
                return Json(new { url }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                flightIdList = new Misc().Decrypt(flightIdList);
            }
            catch (Exception ex)
            {
                return Json("Invalid Request", JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(flightIdList))
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

            int[] flightIds = { Convert.ToInt32(flightIdList) };


            var helper = new UnitDataHelper();
            url = helper.GetUrlDownloadFdrFileForMultipleFlights(flightIds);
            var fileName = helper.GetServerHostedUrlFDRFile(flightIds);

            return Json(new { url }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Maps(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult GetLiveDataFlightDuration(string flightIds)
        {
            return Json(new UnitDataHelper().GetLiveDataFlightDuration(flightIds));
        }

        #endregion Graphs


        public ActionResult ListJpiDataDemo(int pageNo, string IsCallFromPilotPage, int totalRecordFetch, int timeInterval, string StartDate, int requestDataForLevel)
        {

            DateTime? startDateForInnerData = null;
            if (!string.IsNullOrEmpty(StartDate))
            {
                startDateForInnerData = Convert.ToDateTime(StartDate);
            }

            var objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            string engineCommandType = "";
            bool isDataAvailableForFlight = false;
            string aircraftImageURL = "";
            string pilotImageURL = string.Empty;
            string pilotName = string.Empty;
            int flightTimeInterval = 0;
            var context = new GA.DataLayer.GuardianAvionicsEntities();
            dynamic jpi;

            jpi = (List<DataLogListing>)objHelper.ListJpiDataDemo(pageNo, totalRecordFetch, timeInterval, out totalCount, out labelText, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, startDateForInnerData, requestDataForLevel);

            ViewBag.IsJPI = false;
            ViewBag.IsGarmin = true;
            ViewBag.IsULP = false;

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiFlight;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.ActionMethod = "ListJpiPilotDemo";
            ViewBag.VBIsCallFromPilotPage = IsCallFromPilotPage;

            bool IsDataAvailable = true;
            if (jpi.Count < 60)
            {
                IsDataAvailable = false;
            }

            string flightNo = "1";
            string aircraftNNumber = "Demo";

            return Json(new { jpiData = jpi, IsJPI = false, IsGarmin = true, IsULP = false, totalRecordFetch = (totalRecordFetch + 60), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = false, flightTimeInterval = flightTimeInterval }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListJpiPilotDemo(int pageNo, string IsCallFromPilotPage, int totalRecordFetch, int timeInterval, string StartDate, int requestDataForLevel)
        {

            DateTime? startDateForInnerData = null;
            if (!string.IsNullOrEmpty(StartDate))
            {
                startDateForInnerData = Convert.ToDateTime(StartDate);
            }

            var objHelper = new UnitDataHelper();

            var totalCount = 0;
            var labelText = string.Empty;
            string engineCommandType = "";
            bool isDataAvailableForFlight = false;
            string aircraftImageURL = "";
            string pilotImageURL = string.Empty;
            string pilotName = string.Empty;
            int flightTimeInterval = 0;
            var context = new GA.DataLayer.GuardianAvionicsEntities();
            dynamic jpi;
            bool isAircraftWithoutUnit = false;
            jpi = (List<DataLogListing>)objHelper.ListJpiDataDemo(pageNo, totalRecordFetch, timeInterval, out totalCount, out labelText, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, startDateForInnerData, requestDataForLevel);


            ViewBag.IsJPI = false;
            ViewBag.IsGarmin = true;
            ViewBag.IsULP = false;

            ViewBag.NumberOfPages = objHelper.JpiDataNoOfPages(totalCount);
            ViewBag.CurrentPage = pageNo;

            // used to load data sorted for a flight
            ViewBag.DataFor = Constants.JpiFlight;
            ViewBag.LabelText = labelText;

            Session["ShowingDataFor"] = Constants.JpiFlight;
            ViewBag.IsNoData = totalCount == 0;
            ViewBag.ActionMethod = "ListJpiPilotDemo";
            ViewBag.VBIsCallFromPilotPage = IsCallFromPilotPage;

            bool IsDataAvailable = true;
            if (jpi.Count < 60)
            {
                IsDataAvailable = false;
            }

            string flightNo = "1";
            string aircraftNNumber = "Demo";

            //return Json(new { jpiData = jpi, IsJPI = engineCommandType.Contains("JPI"), IsGarmin = engineCommandType.Contains("Garmin"), IsULP = engineCommandType.Contains("ULP"), totalRecordFetch = (totalRecordFetch + 60), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = isAircraftWithoutUnit, flightTimeInterval = flightTimeInterval }, JsonRequestBehavior.AllowGet);
            return Json(new { jpiData = jpi, IsJPI = false, IsGarmin = true, IsULP = false, totalRecordFetch = (totalRecordFetch + 60), IsDataAvailable = IsDataAvailable, FlightNo = flightNo, AircraftNNumber = aircraftNNumber, IsDataAvailableForFlight = isDataAvailableForFlight, aircraftImageURL = aircraftImageURL, pilotImageURL = pilotImageURL, pilotName = pilotName, isAircraftWithoutUnit = isAircraftWithoutUnit, flightTimeInterval = flightTimeInterval }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FlightLogDemo()
        {

            return View();
        }
    }
}
