﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using GA.ApplicationLayer;
using System.Text;
using GA.DataTransfer.Classes_for_Services;
using GA.Common;
using GA.DataTransfer;
using GA.DataLayer;
using Newtonsoft.Json;
using GA.CommonForParseDataFile;
using GA.DataTransfer.Classes_for_Web;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.IO;
using GuardianAvionicsWeb.Controllers;
//using NLog;

namespace GuardianAvionicsWeb
{

    public class MyWebSocketHandler : WebSocketHandler
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];

        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private static WebSocketCollection clients = new WebSocketCollection();
        private static Dictionary<string, bool> dictHoldData = new Dictionary<string, bool>();
        private string name;

        //For every time sender call onMessage then callcount will increase by 1. And when its value equals to 100 , then we will insert the data into DB
        //This will save the sql calling again n again
        private PilotLog socPilotLog;      //socPilotLog => to save pilot log details locally and when we will insert the data after every 100 record then this pilot log with be update in the database.
        private DataTable dt_AirframeDataLogAll = new DataTable();
        private DataTable dt_AirframeDataLog = new DataTable();
        private int? aeroUnitId = null;
        private int aircraftId = 0;

        string HeardBeat = "";
        string ComActive = "";
        string ComStby = "";
        string NavActive = "";
        string NavStby = "";



        public byte[] bytes;
        public int byteLocation = 0;
        public long result;
        public int dataLength = 0;
        public int frameType = 0;
        public int icheck = 0;
        //public string[] header = new string[] { };
        public List<int> header = new List<int>();
        public List<int> tempHeader = new List<int>();
        //Logger Log = LogManager.GetCurrentClassLogger();
        int TOTAL_BITS = 8;
        int bitLocation = 0;
        StringBuilder strBuilder = new StringBuilder();
        int recCount = 0;

        public void Hello()
        {

        }

        public override void OnOpen()
        {
            byte[] arrByte = new byte[] { };
            try
            {
                icheck = 0;
                //ParseLiveData(true);
                // Log.Fatal("Open Method Call" + Environment.NewLine);
                string socName = this.WebSocketContext.QueryString["name"];
                String sessionval = this.WebSocketContext.QueryString.ToString();
                HomeController catcn = new HomeController();
                catcn.setsession(sessionval);

                if (socName == null)
                {
                    // Log.Fatal("socName == null" + Environment.NewLine);
                    arrByte = new byte[] { 165, 90, 2 }; //Invalid Name
                    Send(arrByte);
                    return;
                }
                name = socName;

                if (socName.Substring(socName.Length - 1) == "R")
                {
                    // Log.Fatal(socName + " is connect" + Environment.NewLine);
                    clients.Add(this);
                    dictHoldData.Add(socName.Replace('R', 'S'), true);
                    return;
                }

                if (socName.Substring(socName.Length - 3) == "PLD")
                {
                    clients.Add(this);
                    return;
                }

                if (socName.Length > 6)
                {
                    if (socName.Substring(socName.Length - 6) == "NewLog")
                    {
                        clients.Add(this);
                        //arrByte = new byte[] { 165, 90, 1 }; //Invalid Name
                        //Send(arrByte);
                        return;
                    }
                }


                int? pilotId = this.WebSocketContext.QueryString["pilotId"] == "0" ? (int?)null : Convert.ToInt32(this.WebSocketContext.QueryString["pilotId"]);
                int? coPilotId = this.WebSocketContext.QueryString["coPilotId"] == "0" ? (int?)null : Convert.ToInt32(this.WebSocketContext.QueryString["coPilotId"]);
                string engineType = this.WebSocketContext.QueryString["engineType"] == "0" ? null : this.WebSocketContext.QueryString["engineType"];



                string[] arrSocParam = socName.Split('-');
                aircraftId = Convert.ToInt32(arrSocParam[0]);
                long? uniqueId = Convert.ToInt64(arrSocParam[1]);

                clients.Add(this);


                var context = new GuardianAvionicsEntities();
                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);

                //Check that the aircraft is having Aero Unit or not
                if (string.IsNullOrEmpty(aircraft.AeroUnitNo))
                {
                    aeroUnitId = null;
                    socPilotLog = context.PilotLogs.FirstOrDefault(f => f.AircraftId == aircraftId && f.UniqeId == uniqueId && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                }
                else
                {
                    var aeroUnitMaster = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Blocked && !a.Deleted);
                    aeroUnitId = aeroUnitMaster.Id;
                    socPilotLog = context.PilotLogs.FirstOrDefault(f => f.AircraftId == aircraftId && f.UniqeId == uniqueId && f.AeroUnitMasterId == aeroUnitMaster.Id && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                }


                if (this.socPilotLog == null)
                {
                    //Before creating the new flight first set the finished status of the previous flights

                    var prevLogs = context.PilotLogs.Where(p => p.AircraftId == aircraftId && !p.Finished && p.FlightDataType == (int)Enumerations.FlightDataType.LiveData).ToList();
                    prevLogs.ForEach(f => f.Finished = true);


                    //Create New Flight
                    socPilotLog = context.PilotLogs.Create();
                    socPilotLog.AircraftId = aircraft.Id;
                    socPilotLog.PilotId = pilotId;
                    socPilotLog.LastUpdated = DateTime.UtcNow;
                    socPilotLog.Deleted = false;
                    socPilotLog.Actual = "00:00:00";
                    socPilotLog.CrossCountry = "00:00:00";
                    socPilotLog.Date = DateTime.UtcNow;
                    socPilotLog.DayPIC = "00:00:00";
                    socPilotLog.Hood = "00:00:00";
                    socPilotLog.NightPIC = "00:00:00";
                    socPilotLog.Sim = "00:00:00";
                    socPilotLog.Finished = false;
                    socPilotLog.IsSavedOnDropbox = false;
                    socPilotLog.Route = "";
                    socPilotLog.WayPoint = "";
                    socPilotLog.UniqeId = uniqueId;
                    socPilotLog.CoPilotId = coPilotId;
                    socPilotLog.Headers = "";
                    //  addPilotLogLive.FlightId = objRequest.UniqueId;
                    socPilotLog.AeroUnitMasterId = aeroUnitId;
                    socPilotLog.IsEmailSent = false;
                    socPilotLog.CommandRecFrom = engineType;
                    socPilotLog.FlightDataType = (int)Enumerations.FlightDataType.LiveData;
                    socPilotLog.LiveDataMaxSpeed = 0;
                    context.PilotLogs.Add(this.socPilotLog);
                    context.SaveChanges();


                    List<int?> listProfiles = new List<int?>();
                    if (socPilotLog.PilotId != null)
                    {
                        listProfiles.Add(socPilotLog.PilotId);
                    }
                    if (socPilotLog.CoPilotId != null)
                    {
                        listProfiles.Add(socPilotLog.CoPilotId);
                    }
                    if (aircraft.OwnerProfileId != null)
                    {
                        listProfiles.Add(aircraft.OwnerProfileId);
                    }
                    listProfiles = listProfiles.Select(s => (int?)s.Value).Distinct().ToList();

                    Misc objMisc = new Misc();
                    var logBookModel = new
                    {
                        Id = socPilotLog.Id,
                        TID = objMisc.Encrypt(socPilotLog.Id.ToString()),
                        FlightId = socPilotLog.FlightId ?? 0,
                        AircraftRegistration = socPilotLog.AircraftProfile.Registration,
                        Date = Misc.GetStringOnlyDateUS(socPilotLog.Date) + " " + (socPilotLog.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        TotalFlightDuration = socPilotLog.FlightDataType == Convert.ToInt32(Enumerations.FlightDataType.LiveData) ? socPilotLog.DayPIC : Misc.ConvertMinToOneTenthOFTime(new Misc().ConvertHHMMToMinutes(socPilotLog.DayPIC)),
                        FlightDataType = ((Enumerations.FlightDataType)socPilotLog.FlightDataType).ToString(),
                        Finished = socPilotLog.Finished,
                        UniqeId = socPilotLog.UniqeId ?? 0,
                        AircraftId = socPilotLog.AircraftId,
                        IsNewLog = "true"
                    };

                    foreach (var item in listProfiles)
                    {
                        if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == (item + "-NewLog")) != null)
                        {
                            clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == (item + "-NewLog")).Send(serializer.Serialize(logBookModel));
                        }
                    }
                }
                else
                {
                    var prevLogs = context.PilotLogs.Where(p => p.AircraftId == aircraftId && p.Id < this.socPilotLog.Id && !p.Finished && p.FlightDataType == (int)Enumerations.FlightDataType.LiveData).ToList();
                    prevLogs.ForEach(f => f.Finished = true);

                    List<int?> listProfiles = new List<int?>();
                    if (socPilotLog.PilotId != null)
                    {
                        listProfiles.Add(socPilotLog.PilotId);
                    }
                    if (socPilotLog.CoPilotId != null)
                    {
                        listProfiles.Add(socPilotLog.CoPilotId);
                    }
                    if (aircraft.OwnerProfileId != null)
                    {
                        listProfiles.Add(aircraft.OwnerProfileId);
                    }
                    listProfiles = listProfiles.Select(s => (int?)s.Value).Distinct().ToList();

                    if (socPilotLog.Finished)
                    {
                        //if pilot log is finished then we need to create the connection
                        //check the connection is already created or not
                        if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == (socPilotLog.AircraftId + "-" + socPilotLog.UniqeId + "-PLD")) == null)
                        {
                            var logBookModel = new
                            {
                                Id = socPilotLog.Id,
                                FlightId = socPilotLog.FlightId ?? 0,
                                AircraftRegistration = socPilotLog.AircraftProfile.Registration,
                                Date = Misc.GetStringOnlyDateUS(socPilotLog.Date) + " " + (socPilotLog.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                                TotalFlightDuration = socPilotLog.FlightDataType == Convert.ToInt32(Enumerations.FlightDataType.LiveData) ? socPilotLog.DayPIC : Misc.ConvertMinToOneTenthOFTime(new Misc().ConvertHHMMToMinutes(socPilotLog.DayPIC)),
                                FlightDataType = ((Enumerations.FlightDataType)socPilotLog.FlightDataType).ToString(),
                                Finished = socPilotLog.Finished,
                                UniqeId = socPilotLog.UniqeId ?? 0,
                                AircraftId = socPilotLog.AircraftId,
                                IsNewLog = "false"
                            };

                            foreach (var item in listProfiles)
                            {
                                if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == (item + "-NewLog")) != null)
                                {
                                    clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == (item + "-NewLog")).Send(serializer.Serialize(logBookModel));
                                }
                            }
                        }
                    }

                    //pilotLogid = pilotLog.Id;
                    //Edit Pilot log
                    socPilotLog.PilotId = pilotId;
                    socPilotLog.LastUpdated = DateTime.UtcNow;
                    socPilotLog.Finished = false;
                    socPilotLog.CoPilotId = coPilotId;

                    //liveDataMaxSpeed = pilotLog.LiveDataMaxSpeed ?? 0;
                    context.SaveChanges();
                }

                dt_AirframeDataLogAll.Columns.AddRange(new DataColumn[] {
                new DataColumn("Id",typeof(int)),
                new DataColumn("PilotLogId",typeof(int)),
                new DataColumn("GPRMCDate",typeof(DateTime)),
                new DataColumn("DataLog",typeof(string)),
            });

                dt_AirframeDataLog.Columns.AddRange(new DataColumn[] {
                new DataColumn("Id",typeof(int)),
                new DataColumn("PilotLogId",typeof(int)),
                new DataColumn("GPRMCDate",typeof(DateTime)),
                new DataColumn("DataLog",typeof(string)),
            });
                // clients.Broadcast(name + " has connected.");
                arrByte = new byte[] { 165, 90, 1 }; //connection success

                //Task.Run(() =>
                //{
                //   // Log.Fatal(strBuilder.ToString());
                //    strBuilder.Clear();
                //});
                Send(arrByte);
            }
            catch (Exception ex)
            {
                arrByte = new byte[] { 165, 90, 2 }; //Invalid Name
                Send(arrByte);
                // Log.Fatal(strBuilder.ToString());
                strBuilder.Clear();
                // Log.Fatal("Error in ONOPen Method");
            }

        }

        public override void OnMessage(byte[] message)
        {
            //strBuilder.Append("OnMEssage Call Start by " + name + Environment.NewLine);
            //strBuilder.Append("dictHoldData " + strBuilder.Append(Newtonsoft.Json.JsonConvert.SerializeObject(dictHoldData)));
            //Log.Fatal("On Message call - " + this.name + Environment.NewLine);
            // strBuilder.Append(ByteArrayToString(message) + Environment.NewLine);
            try
            {
                recCount = recCount + 1;
                bytes = message;
                if (dictHoldData.ContainsKey(name))
                {
                    ParseLiveData(true);
                }
                else
                {
                    ParseLiveData(false);
                }
            }
            catch (Exception ex)
            {
                byte[] arrByte = new byte[] { };
                arrByte = new byte[] { 165, 90, 4 }; //Invalid Name
                Send(arrByte);
            }

            // strBuilder.Append("PArsing Success" + Environment.NewLine);
            //Task.Run(() =>
            //{
            //   // Log.Fatal(strBuilder.ToString());
            //    strBuilder.Clear();
            //});
        }


        public override void OnMessage(string message)
        {
            //bytes = StringToByteArray("a55a34040444b4355421767ab5231f72d788969a9aa82ecf090863ed2b9112d4d4d0c5d9eb490c7d64e5225a326278bb429a518f9a61f0");
            //ParseLiveData(false);
            //Send("OnMessage Connect message response");
            //message = "{\"ComStby\":\"118.550\",\"NavActive\":\"108.900\",\"NavStby\":\"109.800\",\"ComActive\":\"123.550\",\"FlightPlan\":\"KTUS,33\",\"HeartBeat\":\"5\"}";
            //Send(message);
            //var details = JObject.Parse(message);
            //if (details["FlightPlan"] != null)
            //{
            //    socPilotLog.Route = details["FlightPlan"].ToString();
            //}
            //if (details["HeartBeat"] != null)
            //{
            //    HeardBeat = details["HeartBeat"].ToString();
            //}
            //if (details["ComActive"] != null)
            //{
            //    //ComActive = "123.550";
            //    ComActive = details["ComActive"].ToString();
            //}
            //if (details["ComStby"] != null)
            //{
            //    ComStby = details["ComStby"].ToString();
            //}
            //if (details["NavActive"] != null)
            //{
            //    NavActive = details["NavActive"].ToString();
            //}

            //if (details["NavStby"] != null)
            //{
            //    NavStby = details["NavStby"].ToString();
            //}
            if (message == "UnHold")
            {
                if (dictHoldData.ContainsKey(name.Replace('R', 'S')))
                {
                    dictHoldData.Remove(name.Replace('R', 'S'));
                }
            }

            if (message.Contains("FlightPlan~"))
            {
                string[] arrFlightPlan = message.Split('~');
                socPilotLog.Route = arrFlightPlan[1];
            }
            if (message.Contains("HeartBeat~"))
            {
                string[] arrHeartBeat = message.Split('~');
                HeardBeat = arrHeartBeat[1];
                //socPilotLog.Route = arrFlightPlan[1];
            }
            if (message.Contains("NavStby~"))
            {
                string[] arrHeartBeat = message.Split('~');
                NavStby = arrHeartBeat[1];
                //socPilotLog.Route = arrFlightPlan[1];
            }
            if (message.Contains("NavActive~"))
            {
                string[] arrHeartBeat = message.Split('~');
                NavActive = arrHeartBeat[1];
                //socPilotLog.Route = arrFlightPlan[1];
            }
            if (message.Contains("ComStby~"))
            {
                string[] arrHeartBeat = message.Split('~');
                ComStby = arrHeartBeat[1];
                //socPilotLog.Route = arrFlightPlan[1];
            }
            if (message.Contains("ComActive~"))
            {
                string[] arrHeartBeat = message.Split('~');
                ComActive = arrHeartBeat[1];
                //socPilotLog.Route = arrFlightPlan[1];
            }
        }


        public override void OnClose()
        {
            //On connection close, First insert the data into database

            HomeController catcn = new HomeController();
            catcn.setsession("NewLog");
            if (dt_AirframeDataLogAll.Rows.Count > 0)
            {
                Task.Run(() =>
                {
                    var context = new GuardianAvionicsEntities();
                    var pLog = context.PilotLogs.FirstOrDefault(f => f.Id == socPilotLog.Id);
                    pLog.DayPIC = socPilotLog.DayPIC;
                    pLog.Finished = socPilotLog.Finished;
                    pLog.WayPoint = socPilotLog.WayPoint;
                    pLog.Route = socPilotLog.Route;
                    context.SaveChanges();

                    SqlParameter[] param = new SqlParameter[3];

                    param[0] = new SqlParameter();
                    param[0].ParameterName = "@aircraftId";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = aircraftId;

                    param[1] = new SqlParameter();
                    param[1].ParameterName = "@pilotLogId";
                    param[1].SqlDbType = SqlDbType.Int;
                    param[1].Value = socPilotLog.Id;

                    param[2] = new SqlParameter();
                    param[2].ParameterName = "tbAirframeDatalog";
                    param[2].SqlDbType = SqlDbType.Structured;
                    param[2].Value = dt_AirframeDataLogAll;

                    if (dt_AirframeDataLogAll.Rows.Count > 0)
                    {
                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLogLive", param));
                    }
                    dt_AirframeDataLogAll.Rows.Clear();
                });
            }

            clients.Remove(this);
            clients.Clear();

        }
        public byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public void ParseLiveData(bool isRequestToHoldData)
        {

            byteLocation = 0;
            bitLocation = 0;
            // strBuilder.Append("ParseLiveData call" + Environment.NewLine);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            var context = new GuardianAvionicsEntities();

            StringBuilder sbHeader = new StringBuilder();

            DataRow dr;
            List<string> arrEngineParam = new List<string>();
            int preByteLocation = 0;
            try
            {

                // strBuilder.Append("Byte Length -" + bytes.Length + Environment.NewLine);
                if (bytes.Length != 0)
                {
                    if (bytes[byteLocation] == 165 && bytes[byteLocation + 1] == 90)
                    {
                        byteLocation = byteLocation + 2; // +2 because delimiter is of two byte and if success we have to read from 3rd byte (i.e., index = 2)
                    }
                    else
                    {

                        var flag = true;
                        byteLocation = byteLocation + 1;
                        while (flag && byteLocation < bytes.Length - 1)
                        {

                            if (bytes[byteLocation] == 165 && bytes[byteLocation + 1] == 90)
                            {
                                flag = false;
                                byteLocation = byteLocation + 2;
                                break;
                            }
                            byteLocation = byteLocation + 1;
                        }
                    }
                    while (byteLocation < bytes.Length)
                    {
                        dataLength = bytes[byteLocation];
                        byteLocation = byteLocation + 1;
                        preByteLocation = byteLocation;
                        frameType = bytes[byteLocation];
                        byteLocation = byteLocation + 1;
                        switch (frameType)
                        {
                            case 1:
                                {
                                    strBuilder.Append("Header Received" + Environment.NewLine);
                                    // Log.Fatal("Header frame execute" + Environment.NewLine);
                                    if (recCount != 1)
                                    {
                                        header = new List<int>();
                                    }
                                    else
                                    {
                                        header = new List<int>();
                                        tempHeader = new List<int>();
                                    }

                                    //Getting header
                                    //Now read 1-1 byte to get the header Id's 
                                    for (int i = 0; i < dataLength - 1; i++)
                                    {

                                        header.Add(bytes[byteLocation]);
                                        //strBuilder.Append("Header Index = " + i.ToString() + ", value = " + header[i].ToString() + Environment.NewLine);

                                        byteLocation = byteLocation + 1;
                                    }
                                    // Log.Fatal("Header count = " + header.Count + Environment.NewLine);
                                    byteLocation = byteLocation + 2;
                                    //  strBuilder.Append("Header Block" + Environment.NewLine);
                                    //  strBuilder.Append("Temp HEader count = " + tempHeader.Count);
                                    //  strBuilder.Append("Curr HEader count = " + header.Count);
                                    if (recCount != 1)
                                    {
                                        //Now check the header is changed or not
                                        if (tempHeader.Count != header.Count)
                                        {
                                            // strBuilder.Append("Header Changed" + Environment.NewLine);
                                            //Header changed
                                            header = tempHeader.Union(header).ToList();
                                            socPilotLog.Headers = string.Join(",", header);
                                            tempHeader = header;
                                            if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')) != null)
                                            {
                                                clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')).Send("Header Changed");
                                            }
                                            var pLog = context.PilotLogs.FirstOrDefault(f => f.Id == socPilotLog.Id);
                                            pLog.Headers = socPilotLog.Headers;
                                            pLog.LastUpdated = DateTime.UtcNow;
                                            context.SaveChanges();
                                        }
                                        else
                                        {
                                            if (header.Union(tempHeader).ToList().Count != header.Count)
                                            {
                                                //  strBuilder.Append("Header Changed" + Environment.NewLine);
                                                header = header.Union(tempHeader).ToList();
                                                tempHeader = header;
                                                socPilotLog.Headers = string.Join(",", header);
                                                var pLog = context.PilotLogs.FirstOrDefault(f => f.Id == socPilotLog.Id);
                                                pLog.Headers = socPilotLog.Headers;
                                                pLog.LastUpdated = DateTime.UtcNow;
                                                context.SaveChanges();
                                                if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')) != null)
                                                {
                                                    clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')).Send("Header Changed");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tempHeader = header;
                                        socPilotLog.Headers = string.Join(",", header);
                                    }

                                    break;
                                }
                            case 2:
                                {
                                    // Log.Fatal("Data frame execute" + Environment.NewLine);
                                    //frametype = 2 means getting header values
                                    if (header.Count == 0)
                                    {
                                        byteLocation = preByteLocation + dataLength;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            string strValuTemp = "";
                                            for (int i = 0; i < header.Count; i++)
                                            {
                                                result = ReadByteData(8, false);
                                                strValuTemp = SetValueAndSign((int)result, header[i]);
                                                // strBuilder.Append("Header ind = " + header[i].ToString() + ", NoOfBytesToRead = " + result.ToString() + ", Value = " + strValuTemp + Environment.NewLine);
                                                dictionary.Add(((Enumerations.enumHeader)header[i]).ToString(), strValuTemp);

                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            // Log.Fatal("exception occurs while parsing the values" + Environment.NewLine);
                                            strBuilder.Append("exception occurs while parsing the values." + Environment.NewLine);
                                        }
                                        byteLocation = dataLength + preByteLocation;
                                        byteLocation = byteLocation + 2;
                                        bitLocation = 0;
                                        string date = "0" + dictionary["Date"];
                                        date = date.Substring(date.Length - 8);
                                        string tempTime = "0" + dictionary["Time"];
                                        tempTime = tempTime.Substring(tempTime.Length - 6);
                                        dictionary.Remove("Date");
                                        dictionary.Remove("Time");
                                        dictionary.Add("Date", Misc.FormatedDateFromMMDDYYLive(date, tempTime).ToString());

                                        if (dictionary.ContainsKey("Speed"))
                                        {
                                            //if (Convert.ToInt32(dictionary["Speed"]) > this.socPilotLog.LiveDataMaxSpeed)
                                            // this.socPilotLog.LiveDataMaxSpeed = Convert.ToInt32(dictionary["Speed"]);
                                        }
                                        //strBuilder.Append(Newtonsoft.Json.JsonConvert.SerializeObject(dictionary));
                                        ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(dictionary) + Environment.NewLine);
                                        dr = dt_AirframeDataLog.NewRow();
                                        dr[0] = 0;
                                        dr[1] = socPilotLog.Id;
                                        dr[2] = dictionary["Date"];
                                        dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                        dt_AirframeDataLog.Rows.Add(dr);
                                        dictionary.Clear();
                                        //  strBuilder.Append("Values read" + Environment.NewLine);
                                    }
                                    break;
                                }
                            case 3:
                                {
                                    socPilotLog.Finished = true;
                                    //  strBuilder.Append("Flight Finished " + Environment.NewLine);
                                    if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')) != null)
                                    {
                                        clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')).Send("Flight End");
                                    }
                                    if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace("S", "PLD")) != null)
                                    {
                                        clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace("S", "PLD")).Send("Flight End");
                                    }
                                    // strBuilder.Append("socPilotLog.Id =  " + socPilotLog.Id + Environment.NewLine);
                                    var pLog = context.PilotLogs.FirstOrDefault(f => f.Id == socPilotLog.Id);

                                    pLog.Finished = socPilotLog.Finished;

                                    pLog.LastUpdated = DateTime.UtcNow;
                                    context.SaveChanges();
                                    //  strBuilder.Append("Switch Case For Finished Completed" + Environment.NewLine);
                                    return;


                                }
                            case 4:
                                {
                                    //frametype = 4 means Flight Plan
                                    {
                                        List<char> fpCharList = new List<char>();
                                        int noOfChars = 0;
                                        StringBuilder strRoute = new StringBuilder();
                                        int noOfWayPoint = (int)ReadByteData(8, false);
                                        for (int p = 0; p < noOfWayPoint; p++)
                                        {
                                            noOfChars = (int)ReadByteData(4, false);
                                            for (int i = 0; i < noOfChars; i++)
                                            {
                                                fpCharList.Add((char)(ReadByteData(8, false)));
                                            }
                                            strRoute.Append(new string(fpCharList.ToArray()));
                                            strRoute.Append(",");
                                            fpCharList.Clear();
                                            //Read lat length
                                            result = ReadByteData(8, false);
                                            strRoute.Append(SetValueAndSign((int)result, 3));
                                            strRoute.Append(",");
                                            result = ReadByteData(8, false);
                                            strRoute.Append(SetValueAndSign((int)result, 4));
                                            if (p < noOfWayPoint - 1)
                                            {
                                                strRoute.Append(",");
                                            }
                                        }
                                        ExceptionHandler.ReportError(new Exception(), "Route = " + strRoute.ToString() + Environment.NewLine);
                                        socPilotLog.Route = strRoute.ToString();
                                        byteLocation = dataLength + preByteLocation;
                                        byteLocation = byteLocation + 2;
                                        bitLocation = 0;
                                        break;
                                    }
                                }
                            case 5:
                                {

                                    // strBuilder.Append("Frame ID = 5");
                                    try
                                    {


                                        List<char> fpCharList = new List<char>();
                                        int noOfChars = 0;
                                        StringBuilder strWayPoint = new StringBuilder();
                                        noOfChars = (int)ReadByteData(4, false);
                                        for (int i = 0; i < noOfChars; i++)
                                        {
                                            fpCharList.Add((char)(ReadByteData(8, false)));
                                        }
                                        strWayPoint.Append(new string(fpCharList.ToArray()));
                                        fpCharList.Clear();
                                        ExceptionHandler.ReportError(new Exception(), "Waypoint = " + strWayPoint.ToString() + Environment.NewLine);
                                        socPilotLog.WayPoint = strWayPoint.ToString();
                                        byteLocation = dataLength + preByteLocation;
                                        byteLocation = byteLocation + 2;
                                        bitLocation = 0;
                                        //  strBuilder.Append("Waypoint = " + socPilotLog.WayPoint + Environment.NewLine);
                                    }
                                    catch (Exception ex)
                                    {
                                        // Log.Fatal("Parse live data - Exception while parsing the message - " + this.name + Environment.NewLine);
                                    }
                                    break;
                                }

                            case 6:
                                {
                                    // Log.Fatal("Data frame execute" + Environment.NewLine);
                                    //frametype = 2 means getting header values
                                    if (header.Count == 0)
                                    {
                                        byteLocation = preByteLocation + dataLength;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            string strValuTemp = "";
                                            for (int i = 0; i < header.Count; i++)
                                            {
                                                result = ReadByteData(8, false);
                                                strValuTemp = SetValueAndSignDuplicate((int)result, header[i]);
                                                // strBuilder.Append("Header ind = " + header[i].ToString() + ", NoOfBytesToRead = " + result.ToString() + ", Value = " + strValuTemp + Environment.NewLine);
                                                dictionary.Add(((Enumerations.enumHeaderDuplicate)header[i]).ToString(), strValuTemp);

                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            // Log.Fatal("exception occurs while parsing the values" + Environment.NewLine);
                                            WriteLog("exception occurs while parsing the values." + Environment.NewLine.ToString());
                                            strBuilder.Append("exception occurs while parsing the values." + Environment.NewLine);
                                        }
                                        byteLocation = dataLength + preByteLocation;
                                        byteLocation = byteLocation + 2;
                                        bitLocation = 0;
                                        string date = "0" + dictionary["Date"];
                                        date = date.Substring(date.Length - 8);
                                        string tempTime = "0" + dictionary["Time"];
                                        tempTime = tempTime.Substring(tempTime.Length - 6);
                                        dictionary.Remove("Date");
                                        dictionary.Remove("Time");
                                        dictionary.Add("Date", Misc.FormatedDateFromMMDDYYLive(date, tempTime).ToString());

                                        if (dictionary.ContainsKey("Speed"))
                                        {
                                            //if (Convert.ToInt32(dictionary["Speed"]) > this.socPilotLog.LiveDataMaxSpeed)
                                            // this.socPilotLog.LiveDataMaxSpeed = Convert.ToInt32(dictionary["Speed"]);
                                        }
                                        //strBuilder.Append(Newtonsoft.Json.JsonConvert.SerializeObject(dictionary));
                                        ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(dictionary) + Environment.NewLine);
                                        dr = dt_AirframeDataLog.NewRow();
                                        dr[0] = 0;
                                        dr[1] = socPilotLog.Id;
                                        dr[2] = dictionary["Date"];
                                        dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                        dt_AirframeDataLog.Rows.Add(dr);
                                        dictionary.Clear();
                                        //  strBuilder.Append("Values read" + Environment.NewLine);
                                    }
                                    break;

                                }
                        }
                    }
                }
                else
                {
                    // Log.Fatal("Byte array length = 0");
                }
                // Log.Fatal("Parse live data -  msg read complete - " + this.name + Environment.NewLine);
                SendLiveDataTOWeb objData = new SendLiveDataTOWeb();
                objData.PLHeader = header.ToArray();
                WriteLog("lower of objData.PLHeader" + objData.PLHeader.Length.ToString() + Environment.NewLine.ToString());
                // Log.Fatal("dt_AirframeDataLog.Rows.Count = " + dt_AirframeDataLog.Rows.Count + Environment.NewLine);
                if (dt_AirframeDataLog.Rows.Count == 0)
                {
                    // Log.Fatal("Parse live data -  dt_AirframeDataLog count = 0" + Environment.NewLine);
                    return;
                }
                WriteLog("lower of objData.PLHeader" + dt_AirframeDataLog.Rows.Count + Environment.NewLine.ToString());
                //if (icheck == 0)
                //{
                var airCraft = context.AircraftProfiles.Where(x => x.Id == aircraftId).FirstOrDefault();
                objData.calibrationLeft = false;
                objData.calibrationRight = false;
                
                try
                {
                    if (airCraft.InstalledEngine != null)
                    {
                        if (airCraft.InstalledEngine.Contains("EM100"))
                        {
                            var calleft = context.FuelDetails.Where(x => x.aircraftId == aircraftId && x.fuelsideType == "left").FirstOrDefault();
                            var calright = context.FuelDetails.Where(x => x.aircraftId == aircraftId && x.fuelsideType == "right").FirstOrDefault();
                            objData.calibrationLeft = calleft == null ? true : false;
                            objData.calibrationRight = calright == null ? true : false;
                        }
                    }
                    WriteLog("check cylinder count=" + objData.CylinderCount + Environment.NewLine.ToString());
                    if (System.IO.File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId + ".xml"))
                    {
                        string xmlfile = ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId + ".xml";
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.Load(xmlfile);
                        var CylinderCount = xmldoc.SelectNodes("plist/dict/dict/dict/integer");
                        objData.STALL = CylinderCount[2].InnerText;
                        objData.VMC = CylinderCount[3].InnerText;
                        objData.VNE = CylinderCount[4].InnerText;
                        objData.VX = CylinderCount[5].InnerText;
                        objData.VY = CylinderCount[6].InnerText;
                        var CylinderCountVal = xmldoc.SelectNodes("plist/dict/integer");
                        objData.CylinderCount = Convert.ToInt32(CylinderCountVal[0].InnerText);
                    }
                    WriteLog("cylinder count=" + objData.CylinderCount + Environment.NewLine.ToString());
                }
                catch (Exception ex)
                {
                    WriteLog("exception with xml file" + dt_AirframeDataLog.Rows.Count + Environment.NewLine.ToString());
                }
                //    icheck = icheck + 1;
                //}




                //Now calculate the time of datalog which gets in API

                DateTime startDate = Convert.ToDateTime(dt_AirframeDataLog.Rows[0]["GPRMCDate"]);
                DateTime EndDate = Convert.ToDateTime(dt_AirframeDataLog.Rows[dt_AirframeDataLog.Rows.Count - 1]["GPRMCDate"]);
                //  strBuilder.Append("DayPIC = " + socPilotLog.DayPIC  + Environment.NewLine);
                //  strBuilder.Append("start date = " + startDate.ToString() + ", End Data =  " + EndDate.ToString() + Environment.NewLine);

                int totalSecondInAPIData = (int)(EndDate - startDate).TotalSeconds + 1;

                int totalFlightTimeInSec = totalSecondInAPIData + ((int)TimeSpan.Parse(this.socPilotLog.DayPIC).TotalSeconds);

                TimeSpan time = TimeSpan.FromSeconds(totalFlightTimeInSec);
                socPilotLog.DayPIC = time.ToString(@"hh\:mm\:ss");
                // strBuilder.Append("totalSecondInAPIData = " + totalSecondInAPIData + Environment.NewLine);
                // strBuilder.Append("totalFlightTimeInSec = " + totalFlightTimeInSec + Environment.NewLine);
                // strBuilder.Append("Update Daypic = " + socPilotLog.DayPIC + Environment.NewLine);

                var response = dt_AirframeDataLog.AsEnumerable().OrderBy(o => o.Field<int>("ID")).Select(m => new AirframeDatalog()
                {
                    Id = m.Field<int>("ID"),
                    DataLog = m.Field<string>("DataLog")
                }).ToList();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                List<DataLogWithoutUnit> dataLogListTemp = new List<DataLogWithoutUnit>();
                List<DataLogListing> dataLogListwithUnit = new List<DataLogListing>();
                List<LatLong> listLatLong = new List<LatLong>();

                if (aeroUnitId == null)
                {
                    response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                    dataLogListTemp.ForEach(f => listLatLong.Add(new LatLong { Latitude = f.Latitude, Longitude = f.Longitude, Yaw = f.Yaw }));
                    dataLogListTemp.ForEach(a =>
                    {
                        a.Time = new DateTime(DateTime.Parse(a.DateString, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                        a.DateString = Convert.ToDateTime(a.DateString).ToShortDateString();
                        a.IsSecData = "true";

                    });
                    objData.jpiData = dataLogListTemp;
                }
                else
                {
                    response.ForEach(f => dataLogListwithUnit.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                    dataLogListwithUnit.ForEach(f => listLatLong.Add(new LatLong { Latitude = f.Latitude, Longitude = f.Longitude, Yaw = f.Yaw }));
                    dataLogListwithUnit.ForEach(a =>
                    {
                        a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                        a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                        a.IsSecData = "true";
                        a.HB = HeardBeat;
                        a.ComActive = ComActive;
                        a.ComStby = ComStby;
                        a.NavActive = NavActive;
                        a.NavStby = NavStby;
                        //a.HB = "1";
                        //a.ComActive = "10";
                        //a.ComStby = "100";
                        //a.NavActive = "101";
                        //a.NavStby = "102";
                    });
                    objData.jpiData = dataLogListwithUnit;
                }


                if (string.IsNullOrEmpty(socPilotLog.CommandRecFrom))
                {
                    objData.IsJPI = false;
                    objData.IsGarmin = false;
                    objData.IsULP = false;
                }
                else
                {
                    objData.IsJPI = socPilotLog.CommandRecFrom.Contains("JPI");
                    objData.IsGarmin = socPilotLog.CommandRecFrom.Contains("Garmin");
                    objData.IsULP = socPilotLog.CommandRecFrom.Contains("ULP");
                }

                try
                {
                    objData.MFDList = new CommonHelper().ParsePListForMFD(socPilotLog.AircraftId);
                }
                catch (Exception ex)
                {
                    // Log.Fatal("Parse Live data - exception while parsing the MFDList");
                }

                //  strBuilder.Append("Engine Setting = " + Newtonsoft.Json.JsonConvert.SerializeObject(objData.MFDList));
                objData.lastRecordFetchId = 0;
                objData.IsDataAvailable = true;
                objData.IsDataAvailableForFlight = true;
                //objData.flightTimeInterval = 
                objData.isFlightFinished = socPilotLog.Finished;
                objData.latLongList = listLatLong;
                objData.isAircraftWithoutUnit = (aeroUnitId == null);

                objData.route = socPilotLog.Route;
                objData.wayPoint = socPilotLog.WayPoint;
                //  strBuilder.Append("isRequestToHoldData" + isRequestToHoldData + Environment.NewLine);
                //  strBuilder.Append("clients.count" + clients.Count + Environment.NewLine);
                if (!dictHoldData.ContainsKey(name))
                {
                    //UnHold Data
                    if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')) != null)
                    {
                        // strBuilder.Append("Client available");
                        clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace('S', 'R')).Send(serializer.Serialize(objData));
                    }
                    else
                    {
                        //  strBuilder.Append("client not available");
                    }

                    if (clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace("S", "PLD")) != null)
                    {
                        // strBuilder.Append("Log available");
                        clients.SingleOrDefault(r => ((MyWebSocketHandler)r).name == this.name.Replace("S", "PLD")).Send(socPilotLog.DayPIC);
                    }
                    else
                    {
                        // strBuilder.Append("Log Not available");
                    }

                    dt_AirframeDataLog.AsEnumerable().CopyToDataTable(dt_AirframeDataLogAll, LoadOption.OverwriteChanges);
                    dt_AirframeDataLog.Rows.Clear();
                }


                //  strBuilder.Append("Table Count = " + dt_AirframeDataLogAll.Rows.Count.ToString());

                if (dt_AirframeDataLogAll.Rows.Count >= 10 || recCount == 1)
                {

                    Task.Run(() =>
                    {
                        var pLog = context.PilotLogs.FirstOrDefault(f => f.Id == socPilotLog.Id);
                        pLog.DayPIC = socPilotLog.DayPIC;
                        pLog.Finished = socPilotLog.Finished;
                        pLog.WayPoint = socPilotLog.WayPoint;
                        pLog.Route = socPilotLog.Route;
                        pLog.Headers = socPilotLog.Headers;
                        pLog.LastUpdated = DateTime.UtcNow;
                        context.SaveChanges();

                        SqlParameter[] param = new SqlParameter[3];

                        param[0] = new SqlParameter();
                        param[0].ParameterName = "@aircraftId";
                        param[0].SqlDbType = SqlDbType.Int;
                        param[0].Value = aircraftId;

                        param[1] = new SqlParameter();
                        param[1].ParameterName = "@pilotLogId";
                        param[1].SqlDbType = SqlDbType.Int;
                        param[1].Value = socPilotLog.Id;

                        param[2] = new SqlParameter();
                        param[2].ParameterName = "tbAirframeDatalog";
                        param[2].SqlDbType = SqlDbType.Structured;
                        param[2].Value = dt_AirframeDataLogAll;

                        if (dt_AirframeDataLogAll.Rows.Count > 0)
                        {
                            int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLogLive", param));
                        }
                        dt_AirframeDataLogAll.Rows.Clear();
                    });
                }

            }
            catch (Exception exx)
            {
                strBuilder.Append("Exception while parsing (Parsing Method) " + Environment.NewLine);
                // Log.Fatal(strBuilder.ToString());
                strBuilder.Clear();
                var error = context.ErrorLogs.Create();
                error.ErrorDate = DateTime.UtcNow;
                error.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(exx);
                error.ErrorSource = "API - GetLiveData";
                error.IsEmailSend = false;
                error.LastUpdateDate = DateTime.UtcNow;
                error.ModuleName = "API";
                error.RequestParameters = "";
                error.ResolvedStatus = false;
                context.ErrorLogs.Add(error);
                context.SaveChanges();
            }
        }

        public string SetValueAndSign(int noOfBitsToRead, int headerId)
        {

            long value = 0;
            //var context = new GuardianAvionicsEntities();
            switch (headerId)
            {
                case 3:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString((double)value / 10000);
                    }
                case 4:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString((double)value / 10000);
                    }
                case 7:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 8:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 9:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 10:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 32:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 33:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        // return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 37:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        // strBuilder.Append("AMP = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 38:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 39:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        // strBuilder.Append("FF = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 40:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 41:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 42:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 43:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 44:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 45:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 49:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString(value);
                    }
                case 63:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 65:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 69:
                    {

                        value = ReadByteData(noOfBitsToRead, false);
                        //strBuilder.Append("MPG = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 71:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 72:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //strBuilder.Append("USD = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 74:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 75:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }

                default:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, false));
                    }
            }
        }

        public string SetValueAndSignDuplicate(int noOfBitsToRead, int headerId)
        {

            long value = 0;
            //var context = new GuardianAvionicsEntities();
            switch (headerId)
            {
                case 3:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString((double)value / 10000);
                    }
                case 4:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString((double)value / 10000);
                    }
                case 7:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 8:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 9:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                    }
                case 10:
                    {
                        //return Convert.ToString(ReadByteData(noOfBitsToRead, false));
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.00}", ((double)value / 100));
                    }
                case 11:
                    {
                        //return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                        value = ReadByteData(noOfBitsToRead, true);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.00}", ((double)value / 10));
                    }
                case 12:
                    {
                        //return Convert.ToString(ReadByteData(noOfBitsToRead, true));
                        value = ReadByteData(noOfBitsToRead, true);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.00}", ((double)value / 100));
                    }
                case 13:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 35:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 36:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        // return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 40:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        // strBuilder.Append("AMP = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 41:
                    {
                        value = ReadByteData(noOfBitsToRead, true);


                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 42:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        // strBuilder.Append("FF = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 43:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 44:
                    {
                        value = ReadByteData(noOfBitsToRead, false);

                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 45:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 46:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 47:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 48:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 52:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        return Convert.ToString(value);
                    }
                case 66:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 68:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 72:
                    {

                        value = ReadByteData(noOfBitsToRead, false);
                        //strBuilder.Append("MPG = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 74:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 75:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //strBuilder.Append("USD = " + value + ", double = " + Convert.ToString((double)value / 10));
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 77:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 78:
                    {
                        value = ReadByteData(noOfBitsToRead, false);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 10));
                    }
                case 81:
                    {
                        value = ReadByteData(noOfBitsToRead, true);
                        //return Convert.ToString((double)value / 10);
                        return string.Format("{0:0.0}", ((double)value / 100));
                    }

                default:
                    {
                        return Convert.ToString(ReadByteData(noOfBitsToRead, false));
                    }
            }
        }


        public double ConvertDegreeAngleToDouble(double degrees, double minutes, double seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return degrees + (minutes / 60) + (seconds / 3600);
        }

        public long ReadByteData(int noOfBits, bool considerSign)
        {
            {
                int bytee = bytes[byteLocation];
                long value = 0;
                bool isNegativeValue = false;

                int remBits = TOTAL_BITS - bitLocation;

                if (noOfBits <= remBits)
                {
                    if (bitLocation == 0)
                    {
                        remBits = TOTAL_BITS - noOfBits;
                        value = bytee >> remBits;
                    }
                    else
                    {
                        remBits = TOTAL_BITS - noOfBits - bitLocation;
                        value = bytee >> remBits;
                        value = value & (decimalValueOfBits(noOfBits));
                    }

                    if (considerSign)
                    {
                        isNegativeValue = (value >> (noOfBits - 1)) == 1 ? true : false;
                        value = (value & (decimalValueOfBits(noOfBits - 1)));
                    }

                    bitLocation += noOfBits;

                }
                else
                {
                    int bitsAvailable;
                    value = bytee & (decimalValueOfBits(remBits));

                    if (considerSign)
                    {
                        isNegativeValue = (value >> (remBits - 1)) == 1 ? true : false;
                        value = value & (decimalValueOfBits(remBits - 1));
                    }

                    bitsAvailable = (TOTAL_BITS - remBits);

                    byteLocation++;
                    bitLocation = 0;
                    remBits = noOfBits - remBits;

                    int tmpValue;
                    int bitsTaken;

                    while (remBits != 0)
                    {
                        tmpValue = bytes[byteLocation];
                        if (remBits < TOTAL_BITS)
                        {
                            tmpValue = tmpValue >> (TOTAL_BITS - remBits);
                            bitsTaken = remBits;
                            bitLocation = bitsTaken;
                        }
                        else
                        {
                            bitsTaken = TOTAL_BITS;
                            bitLocation = 0;
                            byteLocation++;
                            bitsAvailable = TOTAL_BITS;
                        }

                        value = (value << bitsTaken) | tmpValue;
                        remBits -= bitsTaken;
                    }
                }


                if (bitLocation == TOTAL_BITS)
                {
                    bitLocation = 0;
                    byteLocation++;
                }

                if (isNegativeValue)
                    value *= -1;

                return value;
            }
        }

        public long decimalValueOfBits(int bits)
        {
            long value = 1;
            for (int i = 1; i < bits; i++)
            {
                value += Convert.ToInt64(Math.Pow(2, i));
            }
            return value;
        }


        public string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        //changes done for check
        public static void WriteLog(string strMessage)
        {
            try
            {
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", "C:\\inetpub\\wwwroot\\GuardianAvionics\\LiveEnv\\SocketLog", "Log.txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
            }
            catch (Exception ex)
            {
            }
        }

    }
}