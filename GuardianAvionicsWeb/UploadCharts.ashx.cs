﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GA.DataTransfer.Classes_for_Web;
using System.Net;
using Ionic.Zip;
using GA.Common;
using System.Data;
using System.Data.SQLite;
using GA.DataLayer;
using GA.ApplicationLayer;
//using NLog;
using System.Data.SqlClient;
using GA.DataTransfer.Classes_for_Services;
using System.IO;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

namespace GuardianAvionicsWeb
{
    /// <summary>
    /// Summary description for UploadCharts
    /// </summary>
    public class UploadCharts : IHttpHandler
    {
        // private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static bool g_IsMethodCall = false;
        private SQLiteConnection sqlite;
        public List<string> stateList;
        public List<string> stateProcessed = new List<string>();
        public int CurrStateCount = 0;
        public int TotalState = 0;
        public int ChartTypeCount = 0;
        public List<string> ChartType = new List<string>() { "hi", "lo", "sec" };
        //public List<string> ChartType = new List<string>() { "hi" };
        public string fileName = string.Empty;
        public string tempFileName = string.Empty;
        public string version = string.Empty;
        public string password = string.Empty;
        public double fileSize = 0.0;
        public int totalFileDownloaded = 0;
        public string randomKey = "";

        public System.Text.StringBuilder sbPlates = new System.Text.StringBuilder();
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];

        public void ProcessRequest(HttpContext httpContext)
        {
            //logger.Fatal("Upload Chart ProcessRequest call 1-" + HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
            //if (g_IsMethodCall)
            //{
            //    return;
            //}
            //g_IsMethodCall = true;

            //logger.Fatal("Upload Chart ProcessRequest call 2 -" + HttpContext.Current.Request.UrlReferrer.AbsoluteUri);

            var context = new GuardianAvionicsEntities();
            try
            {
                string[] fileNames=System.IO.Directory.GetFiles(ConfigurationReader.ChartFilePath, @"*.zip");
                foreach (string file in fileNames)
                {
                    File.Delete(file);
                }
                
            }
            catch(Exception ex)
            {

            }
            ChartProgressBar.strResponse = new System.Text.StringBuilder();

            randomKey = httpContext.Request.Params["RandomKey"].ToString();

            version = httpContext.Request.Params["Version"].ToString();
            password = httpContext.Request.Params["Password"].ToString();
            try
            {

                int chartVersion = Convert.ToInt32(version);
                var chartVersions = context.ChartVersions.FirstOrDefault(f => f.Version == chartVersion);
                if (chartVersions != null)
                {
                    return;
                }

                chartVersions = context.ChartVersions.Create();
                chartVersions.Version = Convert.ToInt16(version);
                chartVersions.CreateDate = DateTime.UtcNow;
                chartVersions.DownloadStatus = "InProcess";
                context.ChartVersions.Add(chartVersions);
                context.SaveChanges();
            }
            catch (Exception exx)
            {
                string aaa = "";
            }

            GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary = new Dictionary<string, int> { { randomKey, 1 } };

            stateList = context.States.Select(s => s.StateName).ToList();
            TotalState = stateList.Count;
            foreach (var sName in stateList)
            {
                foreach (var chType in ChartType)
                {
                    try
                    {
                        UploadAllFiles(sName, chType);
                        File.Delete(ConfigurationReader.ChartFilePath + tempFileName);
                    }
                    catch (Exception ex)
                    {
                        ChartProgressBar.strResponse.Append("Error occured while processing the state - " + sName + ", charttype = . " + chType + "Error - " + ex.Message + Environment.NewLine);
                    }
                }
            }

            try
            {

                if (RemoteFileExists("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".ChartData.sqlite.zip"))
                {
                    AddAirportFrequency();
                    AddRunwayData();
                    AddNavaidsData();

                }
            }
            catch (Exception ex)
            {
                var chartVersions = context.ChartVersions.FirstOrDefault(f => f.Version == Convert.ToInt32(version));
                chartVersions.DownloadStatus = "Error in APT";
                context.SaveChanges();
            }
            try
            {
                string[] fileNames = System.IO.Directory.GetFiles(ConfigurationReader.ChartFilePath, @"*.zip");
                foreach (string file in fileNames)
                {
                    File.Delete(file);
                }
                
                ChartProgressBar.strResponse.Append("Downloading Start for Plates" + fileName + Environment.NewLine);
                bool isPlatesTextFileExist = false;
                bool isPlatesSqliteFileExist = false;
                bool isPlatesPngFileExist = false;
                isPlatesTextFileExist = RemoteFileExists("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.TXT.zip");
                isPlatesSqliteFileExist = RemoteFileExists("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.sqlite.zip");
                isPlatesPngFileExist = RemoteFileExists("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates1024.PNG.zip");

                if (isPlatesTextFileExist && isPlatesSqliteFileExist && isPlatesPngFileExist)
                {
                    SetPlates();
                    var chartVersions = context.ChartVersions.FirstOrDefault(f => f.Version == Convert.ToInt32(version));
                    chartVersions.DownloadStatus = "Success";
                    context.SaveChanges();
                    return;
                }
                else if (!isPlatesTextFileExist)
                {
                    ChartProgressBar.strResponse.Append("Cannot download Plates data as the Text file does not exists." + Environment.NewLine);
                }
                else if (!isPlatesSqliteFileExist)
                {
                    ChartProgressBar.strResponse.Append("Cannot download Plates data as the sqlite.zip file does not exists." + Environment.NewLine);
                }
                else if (!isPlatesPngFileExist)
                {
                    ChartProgressBar.strResponse.Append("Cannot download Plates data as the sqlite.zip file does not exists." + Environment.NewLine);
                }
                

            }
            catch (Exception ex)
            {
                var chartVersions = context.ChartVersions.FirstOrDefault(f => f.Version == Convert.ToInt32(version));
                chartVersions.DownloadStatus = "Error in Plates";

                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Upload Charts";
                errorLog.ErrorSource = "SetPlates()";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "No Request Parameter";
                context.ErrorLogs.Add(errorLog);

                context.SaveChanges();
            }
            finally
            {
                g_IsMethodCall = false;
            }

            httpContext.Response.ContentType = "text/plain";
            httpContext.Response.Write("Hello World");
        }

        public void UploadAllFiles(string sName, string chType)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            string StateName = sName.ToString().Replace(" ", "");

            fileName = StateName + "." + chType.ToString().ToUpper() + ".MBTiles.zip";
            tempFileName = "Temp." + StateName + "." + chType.ToString().ToUpper() + ".MBTiles.zip";
            
            ChartProgressBar.strResponse.Append("Downloading Start for " + fileName + Environment.NewLine);
            if (StateName == "Arizona")
            {

            }
            string FileUrl = "http://chartdata.seattleavionics.com/charts/" + version + @"/us/" + chType + "/" + fileName;

            if (RemoteFileExists(FileUrl))
            {
                //webClient.DownloadFileAsync(new Uri(FileUrl), ConfigurationReader.ChartFilePath + tempFileName);
                webClient.DownloadFile(new Uri(FileUrl), ConfigurationReader.ChartFilePath + tempFileName);
                using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + tempFileName))
                {
                    zip.Password = password;
                    zip.ExtractProgress += zip_ExtractProgress;
                    zip.ExtractAll(ConfigurationReader.ChartFilePath + "UnZipFiles");
                }

                //Skip renaming the file 
                //File.Move(ConfigurationReader.ChartFilePath + tempFileName, ConfigurationReader.ChartFilePath + fileName);
                //Delete the temp file
                if (File.Exists(ConfigurationReader.ChartFilePath + tempFileName))
                {
                    new Misc().UploadFile(ConfigurationReader.ChartFilePath + tempFileName, fileName, "MapFile");
                    File.Delete(ConfigurationReader.ChartFilePath + tempFileName);
                }
            }
            else
            {
                ChartProgressBar.strResponse.Append("File not exists - " + FileUrl);
            }
        }
        public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {

            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                ChartProgressBar.strResponse.Append("Downloading Complete for " + fileName + Environment.NewLine);

                //string sqliteFileName = fileName.Replace(".zip", ".sqlite"); //Updation For Error Handling
                string sqliteFileName = fileName.Replace(".zip", ".sqlite");
                sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + sqliteFileName);
                string query = "select * from metadata";
                DataTable dt = new DataTable();
                dt = selectQuery(query);
                ChartProgressBar.strResponse.Append("Datatable count = " + dt.Rows.Count);
                var objEntity = new GuardianAvionicsEntities();
                string InsertOrUpdate = "Update";
                string name = dt.Rows[0][1].ToString();
                var chartFileDetails = objEntity.ChartDetails.FirstOrDefault(f => f.Name == name);
                if (chartFileDetails == null)
                {
                    InsertOrUpdate = "Insert";
                    chartFileDetails = objEntity.ChartDetails.Create();
                }

                string columnName = string.Empty;
                string region = string.Empty;
                string shortRegion = string.Empty;
                string subRegion = string.Empty;
                string shortSubRegion = string.Empty;
                string layerType = string.Empty;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    columnName = dt.Rows[i][0].ToString();
                    switch (columnName)
                    {
                        case "name": { chartFileDetails.Name = dt.Rows[i][1].ToString(); break; }
                        case "short_name": { chartFileDetails.ShortName = dt.Rows[i][1].ToString(); break; }
                        case "type": { chartFileDetails.Type = dt.Rows[i][1].ToString(); break; }
                        case "version": { chartFileDetails.Version = dt.Rows[i][1].ToString(); break; }
                        case "description": { chartFileDetails.Description = dt.Rows[i][1].ToString(); break; }
                        case "format": { chartFileDetails.Format = dt.Rows[i][1].ToString(); break; }
                        case "exp_date": { chartFileDetails.ExpDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
                        case "expiration": { chartFileDetails.Expiration = Convert.ToDateTime(dt.Rows[i][1]); break; }
                        case "region": { region = dt.Rows[i][1].ToString(); break; }
                        case "short_region": { shortRegion = dt.Rows[i][1].ToString(); break; }
                        case "subregion": { subRegion = dt.Rows[i][1].ToString(); break; }
                        case "short_subregion": { shortSubRegion = dt.Rows[i][1].ToString(); break; }
                        case "provider": { chartFileDetails.Provider = dt.Rows[i][1].ToString(); break; }
                        case "short_provider": { chartFileDetails.ShortProvider = dt.Rows[i][1].ToString(); break; }
                        case "copyright": { chartFileDetails.Copyright = dt.Rows[i][1].ToString(); break; }
                        case "create_date": { chartFileDetails.CreateDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
                        case "layer_type": { layerType = dt.Rows[i][1].ToString(); break; }
                        case "short_layer_type": { chartFileDetails.ShortLayerType = dt.Rows[i][1].ToString(); break; }
                        case "layer_sort": { chartFileDetails.LayerSort = dt.Rows[i][1].ToString(); break; }
                        case "min_zoom": { chartFileDetails.MinZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
                        case "max_zoom": { chartFileDetails.MaxZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
                        case "bounds": { chartFileDetails.Bounds = dt.Rows[i][1].ToString(); break; }
                    }
                }

                fileSize = Math.Round((new System.IO.FileInfo(ConfigurationReader.ChartFilePath + tempFileName).Length / 1024f) / 1024f, 2);
                chartFileDetails.FileSize = (decimal)fileSize;
                chartFileDetails.FileName = fileName;
                chartFileDetails.Password = new Encryption().Encrypt(password, Constants.GlobalEncryptionKey);
                chartFileDetails.CountryId = new AdminHelper().SetCountry(region, shortRegion);
                chartFileDetails.StateId = new AdminHelper().SetState(subRegion, shortSubRegion);
                chartFileDetails.ChartTypeId = new AdminHelper().SetChart(layerType);
                chartFileDetails.LayerType = layerType;
                chartFileDetails.LastUpdateDate = DateTime.UtcNow;


                if (InsertOrUpdate == "Insert")
                {
                    objEntity.ChartDetails.Add(chartFileDetails);
                    objEntity.SaveChanges();
                }
                else
                {
                    objEntity.SaveChanges();
                }

                try
                {
                    if (File.Exists(ConfigurationReader.ChartFilePath + "UnZipFiles\\" + sqliteFileName))
                    {
                        File.Delete(ConfigurationReader.ChartFilePath + "UnZipFiles\\" + sqliteFileName);
                    }
                }
                catch (Exception ex)
                {

                }

                if (File.Exists(ConfigurationReader.ChartFilePath + fileName))
                {
                    File.Delete(ConfigurationReader.ChartFilePath + fileName);
                }
                //if (File.Exists(ConfigurationReader.ChartFilePath + @"UnZipFiles\" + fileName))
                if (File.Exists(ConfigurationReader.ChartFilePath+ "UnZipFiles\\" + fileName))
                {
                    File.Delete(ConfigurationReader.ChartFilePath + "UnZipFiles\\" + fileName);
                }


                totalFileDownloaded = totalFileDownloaded + 1;

                ChartProgressBar.strResponse.Append("File - " + fileName + " processed successfully." + Environment.NewLine);

                if (Convert.ToInt32((totalFileDownloaded * 100) / (TotalState * 3)) > 97)
                {
                    GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = 97;
                }
                else
                {
                    GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = Convert.ToInt32((totalFileDownloaded * 100) / (TotalState * 3));
                }
            }
        }

        public DataTable selectQuery(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();

            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            sqlite.Close();
            return dt;
        }

        public void SetPlates()
        {
            ChartProgressBar.strResponse.Append("Plates Text file downloaded Process Start" + fileName + Environment.NewLine);
            System.Net.WebClient webClient = new System.Net.WebClient();
            //logger.Fatal("Plates Text file downloaded Process Start." + Environment.NewLine);
            //webClient.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.TXT.zip"), ConfigurationReader.PlateFilePath + "PlatesText.zip");
            webClient.DownloadFile(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.TXT.zip"), ConfigurationReader.PlateFilePath + "PlatesText.zip");
            //webClient.DownloadFileCompleted += (s, e) =>
            //{
            ChartProgressBar.strResponse.Append("Downloading completed for Plates Text File" + Environment.NewLine);
            //logger.Fatal("Plates Text file downloaded. " + Environment.NewLine);
            if (Directory.Exists(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesText"))
            {
                Directory.Delete(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesText", true);
            }

            using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "PlatesText.zip"))
            {
                zip.Password = password;
                zip.ExtractProgress += zip_ExtractProgressText;
                zip.ExtractAll(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesText");
            }
            //};
        }

        public void zip_ExtractProgressText(object sender, ExtractProgressEventArgs exx)
        {
            if (exx.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {

                try
                {
                    //logger.Fatal("Text file unzip successfully. " + Environment.NewLine);

                    InsertOrUpdateState();
                    //logger.Fatal("InsertOrUpdateState  " + Environment.NewLine);
                    InsertOrUpdateCity();
                    //logger.Fatal("InsertOrUpdateCity  " + Environment.NewLine);
                    DataTable dt = new DataTable("Airports");
                    dt.Columns.Add("AirportName", typeof(string));   //0
                    dt.Columns.Add("Military", typeof(string)); //1
                    dt.Columns.Add("apt_ident", typeof(string));  //2
                    dt.Columns.Add("icao_ident", typeof(string)); //3
                    dt.Columns.Add("airport_name_Id", typeof(int));     //4
                    dt.Columns.Add("city_name_Id", typeof(int)); //5 
                    dt.Columns.Add("Country", typeof(string)); //6 
                    dt.Columns.Add("X", typeof(decimal)); //7 
                    dt.Columns.Add("Y", typeof(decimal)); //8 
                    dt.Columns.Add("CityId", typeof(decimal)); //9 
                    //logger.Fatal("AAAAAAAAAAAA  " + Environment.NewLine);
                    DataRow dr = null;
                    string sep = "\t";
                    foreach (var line in System.IO.File.ReadLines(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText\Airports.txt").Skip(7))
                    {
                        dr = dt.NewRow();
                        string[] arrColumns = line.Split(sep.ToCharArray());
                        dr[0] = arrColumns[0];
                        dr[1] = arrColumns[1];
                        dr[2] = arrColumns[2];
                        dr[3] = arrColumns[3];
                        dr[4] = Convert.ToInt16(arrColumns[4]);
                        dr[5] = Convert.ToInt16(arrColumns[5]);
                        dr[6] = arrColumns[6];
                        dr[7] = Convert.ToDecimal(arrColumns[7]);
                        dr[8] = Convert.ToDecimal(arrColumns[8]);
                        dr[9] = 1;
                        dt.Rows.Add(dr);
                    }
                    //logger.Fatal("BBBBBBBBBBBBBB  " + Environment.NewLine);
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "tbAirport";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dt;
                    //logger.Fatal("CCCCCCCCC  " + Environment.NewLine);
                    if (dt.Rows.Count > 0)
                    {
                        int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirports", param));
                    }
                    ChartProgressBar.strResponse.Append("Airport Data inserted successfully" + Environment.NewLine);
                    GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = 98;


                    System.Net.WebClient webClient = new System.Net.WebClient();
                    ChartProgressBar.strResponse.Append("Downloading Start for Plates PNG" + Environment.NewLine);
                    //logger.Fatal("Plates SQLite file downloaded Process Start." + Environment.NewLine);
                    //webClient.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.sqlite.zip"), ConfigurationReader.PlateFilePath + "Plates.zip");
                    webClient.DownloadFile(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.sqlite.zip"), ConfigurationReader.PlateFilePath + "Plates.zip");
                    //logger.Fatal("DDDDDDDDDDDDDDD  " + Environment.NewLine);
                    //webClient.DownloadFileCompleted += (s, e) =>
                    //{
                    //logger.Fatal("Plates SQLite file downloaded. " + Environment.NewLine);
                    if (File.Exists(ConfigurationReader.PlateFilePath + "UnZipFiles\\Plates.sqlite"))
                    {
                        File.Delete(ConfigurationReader.PlateFilePath + "UnZipFiles\\Plates.sqlite");
                    }
                    //logger.Fatal("EEEEEEEEEEEE  " + Environment.NewLine);
                    using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "Plates.zip"))
                    {
                        zip.Password = password;
                        zip.ExtractProgress += zip_ExtractProgresssqlite;
                        zip.ExtractAll(ConfigurationReader.PlateFilePath + "UnZipFiles");
                    }
                    //};

                    //logger.Fatal("FFFFFFFFFFFFFFFFFFF  " + Environment.NewLine);
                    System.Net.WebClient webClientPnfFile = new System.Net.WebClient();
                    //logger.Fatal("Plates PNG file downloaded Process Start." + Environment.NewLine);
                    //webClientPnfFile.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates1024.PNG.zip"), ConfigurationReader.PlateFilePath + "PlatesPNG.zip");
                    webClient.DownloadFile(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates1024.PNG.zip"), ConfigurationReader.PlateFilePath + "PlatesPNG.zip");
                    //webClientPnfFile.DownloadFileCompleted += (s, e) =>
                    //{
                    //logger.Fatal("GGGGGGG  " + Environment.NewLine);
                    //logger.Fatal("Plates SQLite file downloaded Successfully." + Environment.NewLine);
                    if (Directory.Exists(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesPNG"))
                    {
                        Directory.Delete(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesPNG", true);
                    }
                    //Directory.CreateDirectory(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");
                    //logger.Fatal("Plates SQLite file starts unZip." + Environment.NewLine);
                    using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "PlatesPNG.zip"))
                    {
                        //logger.Fatal("HHHHHHHHHHH  " + Environment.NewLine);
                        zip.Password = password;
                        zip.ExtractProgress += zip_ExtractProgressPNG;
                        zip.ExtractAll(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");
                    }
                    //};

                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
                }
            }
        }

        public void zip_ExtractProgresssqlite(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                try
                {
                    //logger.Fatal("sqlite file unzip successfully. " + Environment.NewLine);
                    sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePathForPlates + "Plates.sqlite");
                    string query = "select Airport_Name_ID,Chart_Code,Chartseq,Chart_Name,File_Name,civil,faanfd15,faanfd18,copter,Lat1,Long1,Lat2,Long2,X1,Y1,X2,Y2,Orientation,Draw_Left,Draw_Right,Draw_Top,Draw_Bottom,ValidWithinX,ValidWithinY,ValidWithinRadius,ExcludeAreas,UserAction,Invalid,LastChanged from charts";
                    DataTable dt = new DataTable();
                    dt = selectQuery(query);
                    SetPlatesInToDB(dt);
                    // tempProcess();

                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
                }
            }
        }

        public void zip_ExtractProgressPNG(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                ChartProgressBar.strResponse.Append("lates PNG File extract successfully" + Environment.NewLine);
                //logger.Fatal("Plates PNG File unextract successfully." + Environment.NewLine);
                DirectoryInfo DirInfo = new DirectoryInfo(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");

                try
                {
                    DataTable dt = new DataTable("PNGDetails");
                    dt.Columns.Add("FileName", typeof(string));   //0
                    dt.Columns.Add("ListPrice", typeof(int));  //2
                    DataRow dr = null;
                    foreach (FileInfo fi in DirInfo.EnumerateFiles())
                    {
                        dr = dt.NewRow();
                        dr[0] = fi.Name;
                        dr[1] = fi.Length;
                        dt.Rows.Add(dr);
                    }
                    GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = 99;
                    UpdateFileSizePlates(dt);
                }
                catch (Exception Ex)
                {
                    //logger.Fatal("Exception in zip_ExtractProgressPNG  " + Newtonsoft.Json.JsonConvert.SerializeObject(Ex) + " " + Environment.NewLine);
                }
            }
        }

        public void SetPlatesInToDB(DataTable dt)
        {
            //logger.Fatal("Start inserting plates data into DB." + Environment.NewLine);
            var context = new GuardianAvionicsEntities();
            dt.Columns[2].Caption = "ChartSeq";
            dt.Columns[5].Caption = "Civil";
            dt.Columns[6].Caption = "Faanfd15";
            dt.Columns[7].Caption = "Faanfd18";
            dt.Columns[8].Caption = "Copter";
            

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "tbPlate";
            param[0].SqlDbType = SqlDbType.Structured;
            param[0].Value = dt;

            //foreach (DataRow dr in dt.Rows)
            //{
            //    dr["Civil"] = "abc";
            //    dr.EndEdit();
            //}
            //dt.AcceptChanges();

            if (dt.Rows.Count > 0)
            {
                int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlates", param));
            }

            //logger.Fatal("Plates data inserted successfully in DB." + Environment.NewLine);
        }

        public void UpdateFileSizePlates(DataTable dt)
        {
            //logger.Fatal("Plates PNG file Size updation start " + Environment.NewLine);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "tbPlateFileSize";
            param[0].SqlDbType = SqlDbType.Structured;
            param[0].Value = dt;

            if (dt.Rows.Count > 0)
            {
                int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlatesFileSize", param));
            }
            //logger.Fatal("Plates PNG file Size updation completed " + Environment.NewLine);
            CreateTextFileForPlates();
        }

        public void CreateTextFileForPlates()
        {
            CopyPlatesFileByState();
            //logger.Fatal("Plates PNG file - start creating JSON. " + Environment.NewLine);
            var context = new GuardianAvionicsEntities();

            var objState = context.States.ToList();
            PlatesResponseModel objPaltesResponse = new PlatesResponseModel();


            foreach (State state in objState)
            {
                //objPaltesResponse.StateList = context.States.Where(s => s.Id == state.Id && s.State_Code_Id != null).Select(s => new PlateStates
                objPaltesResponse.StateList = context.States.Where(s => s.Id == state.Id ).Select(s => new PlateStates
                {
                    Id = s.Id,
                    ShortName = s.ShortName,
                    State_Code_Id = s.State_Code_Id,
                    StateName = s.StateName,
                    PlateFilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGForAllState + "/" + s.Id.ToString() + ".zip",
                    PlatesFileSizeInMB = s.PlateFileSizeInMB,
                    //CityList = s.Cities.Where(wc => wc.City_Name_Id != null && wc.State_Code_Id != null).Select(c => new PlateCitys
                    CityList = s.Cities.Select(c => new PlateCitys
                    {
                        City_Name_Id = c.City_Name_Id ?? 0,
                        CityName = c.CityName,
                        Id = c.Id,
                        State_Code_Id = c.State_Code_Id,
                        StateId = c.StateId,
                        Volume = c.Volume,
                        //AirportList = c.Airports.Where(wa => wa.airport_name_Id != null && wa.city_name_Id != null).Select(a => new PlateAirPorts
                        AirportList = c.Airports.Select(a => new PlateAirPorts
                        {
                            airport_name_Id = a.airport_name_Id,
                            AirportName = a.AirportName,
                            apt_ident = a.apt_ident,
                            city_name_Id = a.city_name_Id,
                            CityId = a.CityId,
                            Country = a.Country,
                            icao_ident = a.icao_ident,
                            Id = a.Id,
                            Military = a.Military,
                            X = a.X,
                            Y = a.Y,
                            PlateList = a.Plates.Select(p => new PlateDetails
                            {
                                Airport_Name_Id = p.Airport_Name_Id,
                                AirportId = p.AirportId,
                                Chart_Code = p.Chart_Code,
                                Chart_Name = p.Chart_Name,
                                ChartSeq = p.ChartSeq,
                                Civil = p.Civil,
                                Copter = p.Copter,
                                Draw_Bottom = p.Draw_Bottom,
                                Draw_Left = p.Draw_Left,
                                Draw_Right = p.Draw_Right,
                                Draw_Top = p.Draw_Top,
                                ExcludeAreas = p.ExcludeAreas,
                                Faanfd15 = p.Faanfd15,
                                Faanfd18 = p.Faanfd18,
                                //File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
                                File_Name = p.File_Name, // ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/" + p.File_Name,
                                Id = p.Id,
                                Invalid = p.Invalid == "true" ? true : false,
                                LastChanged = p.LastChanged,
                                Lat1 = p.Lat1,
                                Lat2 = p.Lat2,
                                Long1 = p.Long1,
                                Long2 = p.Long2,
                                Orientation = p.Orientation,
                                UserAction = p.UserAction,
                                ValidWithinRadius = p.ValidWithinRadius,
                                ValidWithinX = p.ValidWithinX,
                                ValidWithinY = p.ValidWithinY,
                                X1 = p.X1,
                                X2 = p.X2,
                                Y1 = p.Y1,
                                Y2 = p.Y2,
                                FileSize = p.FileSize == null ? (long)0.0 : Math.Round(((long)p.FileSize / 1024f) / 1024f, 6)
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

                System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + @"JsonFiles\" + state.Id.ToString() + ".txt", Newtonsoft.Json.JsonConvert.SerializeObject(objPaltesResponse));
            }
            PlatesResponseModel objStateList = new PlatesResponseModel();
            objStateList.StateList = context.States.Select(s => new PlateStates
            {
                Id = s.Id,
                ShortName = s.ShortName,
                State_Code_Id = s.State_Code_Id,
                StateName = s.StateName,
                //CityList = s.Cities.Where(wc => wc.City_Name_Id != null).Select(c => new PlateCitys
                CityList = s.Cities.Select(c => new PlateCitys
                {
                    City_Name_Id = c.City_Name_Id ?? 0,
                    CityName = c.CityName,
                    Id = c.Id,
                    State_Code_Id = c.State_Code_Id,
                    StateId = c.StateId,
                    Volume = c.Volume,
                    //AirportList = c.Airports.Where(wa => wa.airport_name_Id != null).Select(a => new PlateAirPorts
                    AirportList = c.Airports.Select(a => new PlateAirPorts
                    {
                        airport_name_Id = a.airport_name_Id,
                        AirportName = a.AirportName,
                        apt_ident = a.apt_ident,
                        city_name_Id = a.city_name_Id,
                        CityId = a.CityId,
                        Country = a.Country,
                        icao_ident = a.icao_ident,
                        Id = a.Id,
                        Military = a.Military,
                        X = a.X,
                        Y = a.Y,
                    }).ToList()
                }).ToList()
            }).ToList();

            if (File.Exists(ConfigurationReader.PlateFilePath + "JsonFiles\\USAirportList.txt"))
            {
                File.Delete(ConfigurationReader.PlateFilePath + "JsonFiles\\USAirportList.txt");
            }

            System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + "JsonFiles\\USAirportList.txt", Newtonsoft.Json.JsonConvert.SerializeObject(objStateList));
            //Now Upload the plates json and png files to s3 Bucket
            new Misc().UploadFolderToS3Bucket(ConfigurationReader.PlateFilePath + "JsonFiles", "PlateJson");

            if (Directory.Exists(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesPNG"))
            {
                Directory.Delete(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesPNG", true);
            }

            if (Directory.Exists(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesText"))
            {
                Directory.Delete(ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesText", true);
            }

            GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = 100;
            ChartProgressBar.strResponse.Append("Plates PNG file process Successsfully" + Environment.NewLine);
            ChartProgressBar.strResponse.Append("Task Completed" + Environment.NewLine);
            //logger.Fatal("Plates PNG file - creating JSON Completed " + Environment.NewLine);
            g_IsMethodCall = false;
        }

        public void CopyPlatesFileByState()
        {
            var context = new GuardianAvionicsEntities();
            List<string> fileList;
            string sourceDirectory = ConfigurationReader.PlateFilePath + "UnZipFiles\\PlatesPNG";
            string destinationDirectory;
            List<State> stateList = context.States.ToList();
            int stateId = 0;
            FileInfo fileInfo;
            foreach (State state in stateList)
            {
                stateId = state.Id;
                destinationDirectory = ConfigurationReader.PlateFilePath + "PlatePNGForAllState\\" + stateId.ToString();
                Directory.CreateDirectory(destinationDirectory);
                fileList = context.Plates.Where(w => w.Airport.City.State.Id == stateId).Select(s => s.File_Name).Distinct().ToList();
                foreach (var fileName in fileList)
                {
                    if (System.IO.File.Exists(sourceDirectory + @"\" + fileName))
                    {
                        System.IO.File.Copy(sourceDirectory + @"\" + fileName, destinationDirectory + @"\" + fileName);
                    }
                }
                System.IO.Compression.ZipFile.CreateFromDirectory(ConfigurationReader.PlateFilePath + @"PlatePNGForAllState\" + stateId.ToString(), ConfigurationReader.PlateFilePath + @"PlatePNGForAllState\" + stateId.ToString() + ".zip", System.IO.Compression.CompressionLevel.Optimal, false);
                fileInfo = new FileInfo(ConfigurationReader.PlateFilePath + @"PlatePNGForAllState\" + stateId.ToString() + ".zip");

                state.PlateFileSizeInMB = Math.Round(((fileInfo.Length / (decimal)1024) / 1024), 6);

                Directory.Delete(ConfigurationReader.PlateFilePath + "PlatePNGForAllState\\" + stateId.ToString(), true);
            }
            context.SaveChanges();
            new Misc().UploadFolderToS3Bucket(ConfigurationReader.PlateFilePath + @"PlatePNGForAllState", "PlatePNGForAllState");

            //Now delete all file and folders from the PlatePNGForAllState
            System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationReader.PlateFilePath + @"PlatePNGForAllState");

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }

        public bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        public void InsertOrUpdateState()
        {
            try
            {
                DataTable dt = new DataTable("States");
                dt.Columns.Add("StateName", typeof(string)); //0
                dt.Columns.Add("ShortName", typeof(string));   //1
                dt.Columns.Add("State_Code_Id", typeof(Int32));  //2

                DataRow dr = null;
                string sep = "\t";
                foreach (var line in System.IO.File.ReadLines(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText\States.txt").Skip(7))
                {
                    dr = dt.NewRow();
                    string[] arrColumns = line.Split(sep.ToCharArray());
                    dr[1] = arrColumns[0];
                    dr[0] = arrColumns[1];
                    dr[2] = Convert.ToInt32(arrColumns[2]);
                    dt.Rows.Add(dr);
                }

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbState";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dt;

                if (dt.Rows.Count > 0)
                {
                    int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetStates", param));
                }
                ChartProgressBar.strResponse.Append("State Data Updated successfully" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                var context = new GuardianAvionicsEntities();
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Upload Charts";
                errorLog.ErrorSource = "InsertOrUpdateState";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "No Request Parameter";
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();

                ChartProgressBar.strResponse.Append("Exception occurs while processing the State Data" + Environment.NewLine);
            }
        }

        public void InsertOrUpdateCity()
        {
            try
            {

                DataTable dt = new DataTable("Citys");
                dt.Columns.Add("CityName", typeof(string));   //0
                dt.Columns.Add("Volume", typeof(string)); //1
                dt.Columns.Add("City_Name_Id", typeof(Int32));  //2
                dt.Columns.Add("State_Code_Id", typeof(Int32));  //3
                dt.Columns.Add("StateId", typeof(Int32));  //4


                DataRow dr = null;
                string sep = "\t";
                foreach (var line in System.IO.File.ReadLines(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText\Cities.txt").Skip(7))
                {
                    dr = dt.NewRow();
                    string[] arrColumns = line.Split(sep.ToCharArray());
                    dr[0] = arrColumns[0];
                    dr[1] = arrColumns[1];
                    dr[2] = Convert.ToInt32(arrColumns[2]);
                    dr[3] = Convert.ToInt32(arrColumns[3]);
                    dr[4] = 1; // There is no use of the field. Will be removed 
                    dt.Rows.Add(dr);
                }

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbCity";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dt;

                if (dt.Rows.Count > 0)
                {
                    int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetCitys", param));
                }
                ChartProgressBar.strResponse.Append("City Data Updated successfully" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                var context = new GuardianAvionicsEntities();
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Upload Charts";
                errorLog.ErrorSource = "InsertOrUpdateCity";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "No Request Parameter";
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                ChartProgressBar.strResponse.Append("Exception occurs while processing the City Data" + Environment.NewLine);
            }
        }


        public void AddAirportFrequency()
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.DownloadFile(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".ChartData.sqlite.zip"), ConfigurationReader.ChartFilePath + "ChartData.zip");
            if (System.IO.File.Exists(ConfigurationReader.ChartFilePath + "UnZipFiles\\ChartData.sqlite"))
            {
                System.IO.File.Delete(ConfigurationReader.ChartFilePath + "UnZipFiles\\ChartData.sqlite");
            }

            using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + "ChartData.zip"))
            {
                zip.Password = password;
                zip.ExtractProgress += zip_ExtractChartData;
                zip.ExtractAll(ConfigurationReader.ChartFilePath + @"UnZipFiles");
            }
        }

        public void zip_ExtractChartData(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                try
                {
                    sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + "ChartData.sqlite");
                    string query = "select featureId,MilitaryName as NavAIDType, FAA_HOST_ID, Z,  Unicom, CTAF, ATIS,TWR,  GND, X, Y, City, StatePostalCode,FriendlyName,ICAO  from Airports where country = 'US' and FacilityTypeId = '1' and statepostalcode != \"\" ";
                    DataTable dt = new DataTable();
                    dt.Columns.Add("FeatureId", typeof(string));  //0
                    dt.Columns.Add("MilitaryName", typeof(string));       //1
                    dt.Columns[1].AllowDBNull = true;
                    dt.Columns.Add("FAA_HOST_ID", typeof(string)); //2
                    dt.Columns[2].AllowDBNull = true;
                    dt.Columns.Add("Z", typeof(string));           //3
                    dt.Columns[3].AllowDBNull = true;
                    dt.Columns.Add("Unicom", typeof(string));      //4
                    dt.Columns[4].AllowDBNull = true;
                    dt.Columns.Add("CTAF", typeof(string));        //5
                    dt.Columns[5].AllowDBNull = true;
                    dt.Columns.Add("ATIS", typeof(string));        //6
                    dt.Columns[6].AllowDBNull = true;
                    dt.Columns.Add("TWR", typeof(string));         //7
                    dt.Columns[7].AllowDBNull = true;
                    dt.Columns.Add("GND", typeof(string));         //8
                    dt.Columns[8].AllowDBNull = true;
                    dt.Columns.Add("X", typeof(string));         //9
                    dt.Columns[9].AllowDBNull = true;
                    dt.Columns.Add("Y", typeof(string));         //10
                    dt.Columns[10].AllowDBNull = true;
                    dt.Columns.Add("City", typeof(string));         //11
                    dt.Columns[11].AllowDBNull = true;
                    dt.Columns.Add("StatePostalCode", typeof(string));         //12
                    dt.Columns[12].AllowDBNull = true;
                    dt.Columns.Add("FriendlyName", typeof(string));         //13
                    dt.Columns[13].AllowDBNull = true;
                    dt.Columns.Add("ICAO", typeof(string));         //14
                    dt.Columns[14].AllowDBNull = true;
                    dt = selectQuery(query);
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "@tbAirportAltitude";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dt;
                    SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportAltitude", param);

                    dt = new DataTable();
                    query = "Select FeatureID, Frequency, Description, Comments, Phone, Title from TWRCOM";
                    dt.Columns.Add("FeatureID", typeof(string));  //0
                    dt.Columns[0].AllowDBNull = true;
                    dt.Columns.Add("Frequency", typeof(string));       //1
                    dt.Columns[1].AllowDBNull = true;
                    dt.Columns.Add("Description", typeof(string)); //2
                    dt.Columns[2].AllowDBNull = true;
                    dt.Columns.Add("Comments", typeof(string));           //3
                    dt.Columns[3].AllowDBNull = true;
                    dt.Columns.Add("Phone", typeof(string));      //4
                    dt.Columns[4].AllowDBNull = true;
                    dt.Columns.Add("Title", typeof(string));        //5
                    dt.Columns[5].AllowDBNull = true;
                    dt = selectQuery(query);

                    param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "@tbAirportFrequency";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dt;
                    SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportFrequency", param);

                }
                catch (Exception ex)
                {

                }
            }
        }

        public void AddNavaidsData()
        {
            try
            {
                sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + "ChartData.sqlite");
                string query = "select UniqueID, FeatureID, TYPE, NAME, FREQ, NAV_RANGE, Y, X, Z, ARPT_ICAO, MagVar, State from Navaids where Country = 'US' ";
                DataTable dt = new DataTable();
                dt.Columns.Add("UniqueID", typeof(string));  //0
                dt.Columns[0].AllowDBNull = true;
                dt.Columns.Add("FeatureID", typeof(string));       //1
                dt.Columns[1].AllowDBNull = true;
                dt.Columns.Add("TYPE", typeof(int)); //2
                dt.Columns[2].AllowDBNull = true;
                dt.Columns.Add("NAME", typeof(string));           //3
                dt.Columns[3].AllowDBNull = true;
                dt.Columns.Add("FREQ", typeof(string));      //4
                dt.Columns[4].AllowDBNull = true;
                dt.Columns.Add("NAV_RANGE", typeof(string));        //5
                dt.Columns[5].AllowDBNull = true;
                dt.Columns.Add("Y", typeof(decimal));        //6
                dt.Columns[6].AllowDBNull = true;
                dt.Columns.Add("X", typeof(decimal));         //7
                dt.Columns[7].AllowDBNull = true;
                dt.Columns.Add("Z", typeof(string));         //8
                dt.Columns[8].AllowDBNull = true;
                dt.Columns.Add("ARPT_ICAO", typeof(string));         //9
                dt.Columns[9].AllowDBNull = true;
                dt.Columns.Add("MagVar", typeof(decimal));         //10
                dt.Columns[10].AllowDBNull = true;
                dt.Columns.Add("State", typeof(decimal));         //11
                dt.Columns[11].AllowDBNull = true;

                dt = selectQuery(query);
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@tbNavaidData";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dt;
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetNavaidData", param);





                dt = new DataTable();
                query = "Select startdateutc, enddateutc, modifydateutc from info";
                dt.Columns.Add("startdateutc", typeof(string));  //0
                dt.Columns[0].AllowDBNull = true;
                dt.Columns.Add("enddateutc", typeof(string));       //1
                dt.Columns[1].AllowDBNull = true;
                dt.Columns.Add("modifydateutc", typeof(string)); //2
                dt.Columns[2].AllowDBNull = true;
                dt = selectQuery(query);

                var context = new GuardianAvionicsEntities();



                AirportFreqAndNavaidResponseModel objAptStateList = new AirportFreqAndNavaidResponseModel();
                objAptStateList.StateList = context.States.Select(s => new AirportStates
                {
                    Id = s.Id,
                    ShortName = s.ShortName,
                    State_Code_Id = s.State_Code_Id,
                    StateName = s.StateName,

                    NavaidList = s.Navaids.Select(na => new AirportNavaid
                    {
                        Altitude = na.Altitude,
                        ARPT_ICAO = na.ARPT_ICAO,
                        FeatureID = na.FeatureID,
                        Freq = na.Freq,
                        Mag_Var = na.Mag_Var,
                        Name = na.Name,
                        Nav_Range = na.Nav_Range,
                        Type = na.Type,
                        UniqueID = na.UniqueID,
                        X = na.X,
                        Y = na.Y
                    }).ToList(),
                    //CityList = s.Cities.Where(wc => wc.City_Name_Id != null).Select(c => new AirportCitys
                    CityList = s.Cities.Select(c => new AirportCitys
                    {
                        City_Name_Id = c.City_Name_Id ?? 0,
                        CityName = c.CityName,
                        Id = c.Id,
                        State_Code_Id = c.State_Code_Id,
                        StateId = c.StateId,
                        Volume = c.Volume,
                        //AirportList = c.Airports.Where(wa => wa.airport_name_Id != null).Select(a => new AirportForFreqAndNavaid
                        AirportList = c.Airports.Select(a => new AirportForFreqAndNavaid
                        {
                            airport_name_Id = a.airport_name_Id,
                            AirportName = a.AirportName,
                            apt_ident = a.apt_ident,
                            city_name_Id = a.city_name_Id,
                            CityId = a.CityId,
                            Country = a.Country,
                            icao_ident = a.icao_ident,
                            Id = a.Id,
                            Military = a.Military,
                            X = a.X,
                            Y = a.Y,
                            Altittude = a.Altitude,
                            FeatureId = a.FeatureId,
                            FrequencyList = context.AirportFrequencies.Where(fr => fr.FeatureId == a.FeatureId).Select(f => new AptFrequency
                            {
                                Comments = f.Comments,
                                Description = f.Description,
                                FeatureId = f.FeatureId,
                                Frequency = f.Frequency,
                                Phone = f.Phone,
                                Title = f.Title
                            }).ToList(),
                            RunwayList = context.AirportRunways.Where(ar => ar.FeatureId == a.FeatureId).Select(sar => new AptRunway
                            {
                                FeatureId = sar.FeatureId,
                                BEID = sar.BEID,
                                Length = sar.Length,
                                REID = sar.REID,
                                Surface = sar.Surface,
                                Width = sar.Width
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

                if (File.Exists(ConfigurationReader.PlateFilePath + "JsonFiles\\AirportNavaidAndFrequency.txt"))
                {
                    File.Delete(ConfigurationReader.PlateFilePath + "JsonFiles\\AirportNavaidAndFrequency.txt");
                }

                System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + "JsonFiles\\AirportNavaidAndFrequency.txt", Newtonsoft.Json.JsonConvert.SerializeObject(objAptStateList));
                //Now Upload the plates json and png files to s3 Bucket


                bool isFileExist = false;
                new Misc().ReadFileFromS3("JsonMisc", "AirportNavaidAndFrequency.txt", out isFileExist);

                if (isFileExist)
                {
                    new Misc().DeleteFileFromS3Bucket("AirportNavaidAndFrequency.txt", "JsonMisc");
                }

                new Misc().UploadFile(ConfigurationReader.PlateFilePath + @"JsonFiles\AirportNavaidAndFrequency.txt", "AirportNavaidAndFrequency.txt", "JsonMisc");


                var objNavStatus = context.AirportNavAndFreqStatus.FirstOrDefault();
                if (objNavStatus == null)
                {
                    objNavStatus = context.AirportNavAndFreqStatus.Create();
                    objNavStatus.StartDate = Convert.ToDateTime(dt.Rows[0][0]);
                    objNavStatus.EndDate = Convert.ToDateTime(dt.Rows[0][1]);
                    objNavStatus.ModifyDate = Convert.ToDateTime(dt.Rows[0][2]);
                    objNavStatus.FileSizeInMB = Convert.ToDecimal(Math.Round((new System.IO.FileInfo(ConfigurationReader.PlateFilePath + @"JsonFiles\AirportNavaidAndFrequency.txt").Length / 1024f) / 1024f, 2));
                    context.AirportNavAndFreqStatus.Add(objNavStatus);
                }
                else
                {
                    objNavStatus.StartDate = Convert.ToDateTime(dt.Rows[0][0]);
                    objNavStatus.EndDate = Convert.ToDateTime(dt.Rows[0][1]);
                    objNavStatus.ModifyDate = Convert.ToDateTime(dt.Rows[0][2]);
                    objNavStatus.FileSizeInMB = Convert.ToDecimal(Math.Round((new System.IO.FileInfo(ConfigurationReader.PlateFilePath + @"JsonFiles\AirportNavaidAndFrequency.txt").Length / 1024f) / 1024f, 2));
                }
                context.SaveChanges();



                if (System.IO.File.Exists(ConfigurationReader.ChartFilePath + "UnZipFiles\\ChartData.sqlite"))
                {
                    System.IO.File.Delete(ConfigurationReader.ChartFilePath + "UnZipFiles\\ChartData.sqlite");
                }

            }
            catch (Exception ex)
            {

            }
        }

        public void AddRunwayData()
        {
            sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + "ChartData.sqlite");
            string query = "select ARPT_ID as FeatureId, RunwayLength as Length, RunwayWidth as Width, BEID, REID, Surface from Runways";
            DataTable dt = new DataTable();
            dt.Columns.Add("FeatureId", typeof(string));  //0
            dt.Columns[0].AllowDBNull = true;
            dt.Columns.Add("Length", typeof(int));       //1
            dt.Columns[1].AllowDBNull = true;
            dt.Columns.Add("Width", typeof(int)); //2
            dt.Columns[2].AllowDBNull = true;
            dt.Columns.Add("BEID", typeof(string));           //3
            dt.Columns[3].AllowDBNull = true;
            dt.Columns.Add("REID", typeof(string));      //4
            dt.Columns[4].AllowDBNull = true;
            dt.Columns.Add("Surface", typeof(int));        //5
            dt.Columns[5].AllowDBNull = true;
            dt = selectQuery(query);

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@tbAirportRunway";
            param[0].SqlDbType = SqlDbType.Structured;
            param[0].Value = dt;
            SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirportRunwayData", param);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}