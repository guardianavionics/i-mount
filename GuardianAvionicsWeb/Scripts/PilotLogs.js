﻿function HideExportBtn() {
    $('#btnExport').hide();
}

function ShowSaveBtn() {
    $('#btnSavePilotLogs').show();
}

function HideSaveBtn() {
    $('#btnSavePilotLogs').hide();
}

function HideFlightListing() {
    $('#pilotLog').hide();
}

function ShowFlightListing() {
    $('#pilotLog').show();
}

function AddRemoveSelectionClass(btnAdd, btnRemove1, btnRemove2) {

    //$('#' + btnAdd).addClass("");
    //$('#' + btnRemove1, '#' + btnRemove2).removeClass("");
}

function HideDataLog() {
    HideExportBtn();
    $('#divJpiData').hide();
}

function ShowDataLog() {
    $('#divJpiData').show();
}

function ShowPilotLogDetails() {
    $('#divPilotLogDetails').show();
}

function HidePilotLogDetails() {
    HideSaveBtn();
    $('#divPilotLogDetails').hide();
}


function ShowDataMapBtns() {
    //$('#btnData, #btnMapAndGraph').show();
    $('#btnData').show();
}

function HideDataMapBtns() {
    // hide option for viewing data and map , when no data available for user.
    $('#btnData').hide();
    //    $('#btnData, #btnMapAndGraph').hide();

}


// pilot log failed to edit

function Failure() {
    alert("Failed to edit pilot log. Please try again. ");
}

// change page on list of pilot log

function pagingPilotlog(pageNo) {
    $.post("/Home/PilotSummary", { pageNo: pageNo }, function (data) {
        //alert("paging pilot log");
        ListPilotLog(data);
    });
}

// show pilot log summary

function ListPilotLog(data) {
    //console.log(data)

    //alert("ListPilotLog");
    //alert(data);

    $('#pilotlogSummary').html(data);
}

//function SimplePagination() {
//    $(function () {
//        $(selector).pagination({
//            items: 100,
//            itemsOnPage: 10,
//            cssStyle: 'compact-theme'
//        });
//    });
//};



function ShowGraph() {

    var showGraphList = [];

    showGraphList.push($('#Id').val());

    var stringIds = JSON.stringify(showGraphList);
    //alert(stringIds);

    $.post("/Data/GetGraph", {
        flightIdList: stringIds,
    }, function (data) {

        $(function () {
            $('#divGraphCHT').highcharts({
                chart: {
                    zoomType: 'x' //'xy'
                },
                title: {
                    text: 'cylinder head temperature'
                },
                xAxis: {
                    type: 'datetime',
                    //minRange: 14 * 24 * 60 * 60 * 1000 // fourteen days
                },
                yAxis: {
                    title: {
                        text: 'Temp'
                    }
                },
                legend: {
                    enabled: true
                },
                series: [
                    {
                        name: 'CHT 1',
                        data:
                            data[0]
                    },
                    {
                        name: 'CHT 2',
                        data:
                            data[1]
                    },
                    {
                        name: 'CHT 3',
                        data:
                            data[2]
                    },
                    {
                        name: 'CHT 4',
                        data:
                            data[3]
                    },
                    {
                        name: 'CHT 5',
                        data:
                            data[4]
                    },
                    {
                        name: 'CHT 6',
                        data:
                            data[5]
                    },
                    {
                        name: 'EGT 1',
                        data:
                            data[6]
                    },
                    {
                        name: 'EGT 2',
                        data:
                            data[7]
                    },
                    {
                        name: 'EGT 3',
                        data:
                            data[8]
                    },
                    {
                        name: 'EGT 4',
                        data:
                            data[9]
                    },
                    {
                        name: 'EGT 5',
                        data:
                            data[10]
                    },
                    {
                        name: 'EGT 6',
                        data:
                            data[11]
                    },
                    {
                        name: 'TIT 1',
                        data:
                            data[12]
                    },
                    {
                        name: 'RPM',
                        data:
                            data[13]
                    },
                    {
                        name: 'OAT',
                        data:
                            data[14]
                    },
                    {
                        name: 'FF',
                        data:
                            data[15]
                    },
                    {
                        name: 'OILP',
                        data:
                            data[16]
                    },
                    {
                        name: 'OILT',
                        data:
                            data[17]
                    },
                    {
                        name: 'GPS Altitude',
                        data:
                            data[18]
                    },
                    {
                        name: 'GPS Speed',
                        data:
                            data[19]
                    },
                ]
            });

        }); // function for showing graph $(function () {

        HideLoader();

    }); //  response of $.post("/Data/GetGraph" function (data) {

} // ShowGraph


function ShowFlightListingPilotlogpage() {
    $('#pilotlogSummary').hide();
    $('#divJpi').show();
}

function OpenUrlInNewWindow(url) {
    window.open(url); //, '_blank');
}


function HideGraphAndMaps() {
    $('#divGraphAndMaps').hide();
}

//////////////////////////////////////////////document.ready/////////////////////////////////////////////////////////////


var FromEndDate = new Date();
$(function () {
    //$("#Date").datepicker({ minDate: new Date(1970, 1, 1, 1, 1, 1), maxDate: "-1D", changeMonth: true, changeYear: true }); //, dateFormat: "dd/mm/yy"
    $('#Date').datepicker({
        format: 'mm/dd/yyyy',
        endDate: FromEndDate,
        autoclose: true
    });
});


$(document).ready(function(e) {

    $('.f_link').click(function (e) {
        e.preventDefault();
    });

  


    $('#btnSummary').click(function (e) {

        ShowLoader();

        if ($('#pilotlogSummary').css('display') == 'none') {
            $.post("/Home/PilotSummary",
                { pageNo: 1 },
                function (data) {

                    $('#divJpi').hide();
                    $('#pilotlogSummary').show();

                    ListPilotLog(data);

                    HideLoader();
                });
        } else {

            $('#pilotlogSummary').hide();
        }
    });
});


