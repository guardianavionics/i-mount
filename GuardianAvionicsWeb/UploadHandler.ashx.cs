﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GA.DataTransfer.Classes_for_Web;
using System.Net;
using Ionic.Zip;
using GA.Common;
using System.Data;
using System.Data.SQLite;
using GA.DataLayer;
using GA.ApplicationLayer;


namespace GuardianAvionicsWeb
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {

        private SQLiteConnection sqlite;
        string fileName = string.Empty;
        double fileSize = 0.0;
        bool isDownloadFromURL = false;
        string randomKey = string.Empty;
        string password = string.Empty;

        public void ProcessRequest(HttpContext context)
        {
            randomKey = context.Request.Params["RandomKey"].ToString();
            
            
            password = context.Request.Params["Password"].ToString();
            if (context.Request.Files.Count > 0)
            {

                ChartProgressBar.dictionary.Add(randomKey, 85);
                HttpFileCollection files = context.Request.Files;
                foreach (string key in files)
                {

                    HttpPostedFile file = files[key];
                    fileName = file.FileName;
                    // fileSize = Math.Round((file.ContentLength / 1024f) / 1024f, 2);
                    isDownloadFromURL = false;


                    string filePath = ConfigurationReader.ChartFilePath + fileName;
                    file.SaveAs(filePath);
                    fileSize = Math.Round((new System.IO.FileInfo(ConfigurationReader.ChartFilePath + fileName).Length / 1024f) / 1024f, 2);
                    ChartProgressBar.dictionary[randomKey] = 88;
                    using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + fileName))
                    {

                        zip.Password = password;
                        zip.ExtractProgress += zip_ExtractProgress;
                        zip.ExtractAll(ConfigurationReader.ChartFilePath + "UnZipFiles");
                    }
                }
            }
            else
            {
                string FileUrl = context.Request.Params["ChartFileUrl"].ToString();
                string[] arr = FileUrl.Split('/');
                fileName = arr[arr.Length - 1];
                isDownloadFromURL = true;

                WebClient webClient = new WebClient();
                webClient.DownloadProgressChanged += (s, e) =>
                {
                    double bytesIn = double.Parse(e.BytesReceived.ToString());
                    double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                    double percentage = bytesIn / totalBytes * 100;
                    if (percentage < 85)
                    {
                        ChartProgressBar.dictionary[randomKey] = (int)percentage;
                    }
                };
                webClient.DownloadFileCompleted += (s, e) =>
                {


                    fileSize = Math.Round((new System.IO.FileInfo(ConfigurationReader.ChartFilePath + fileName).Length / 1024f) / 1024f, 2);

                    ChartProgressBar.dictionary[randomKey] = 85;
                    using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + fileName))
                    {

                        zip.Password = password;
                        zip.ExtractProgress += zip_ExtractProgress;
                        zip.ExtractAll(ConfigurationReader.ChartFilePath + "UnZipFiles");
                    }

                };
                webClient.DownloadFileAsync(new Uri(FileUrl), ConfigurationReader.ChartFilePath + fileName);
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }


        //public void ProcessChartFile(string fileName, string password, double fileSize, bool isDownloadFromURL, string randomKey)
        //{

        //    //using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + fileName))
        //    //{

        //    //    zip.Password = password;
        //    //    zip.ExtractProgress += zip_ExtractProgress;
        //    //    zip.ExtractAll(ConfigurationReader.ChartFilePath + "UnZipFiles");
        //    //}




        //}

        public DataTable selectQuery(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();

            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            sqlite.Close();
            return dt;
        }

        public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {

                try
                {
                    string sqliteFileName = fileName.Replace(".zip", ".sqlite");

                    ChartProgressBar.dictionary[randomKey] = 92;

                    sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + sqliteFileName);
                    string query = "select * from metadata";
                    DataTable dt = new DataTable();
                    dt = selectQuery(query);
                    var objEntity = new GuardianAvionicsEntities();
                    string InsertOrUpdate = "Update";
                    string name = dt.Rows[0][1].ToString();
                    var chartFileDetails = objEntity.ChartDetails.FirstOrDefault(f => f.Name == name);
                    if (chartFileDetails == null)
                    {
                        InsertOrUpdate = "Insert";
                        chartFileDetails = objEntity.ChartDetails.Create();
                    }

                    ChartProgressBar.dictionary[randomKey] = 95;

                    string columnName = string.Empty;
                    string region = string.Empty;
                    string shortRegion = string.Empty;
                    string subRegion = string.Empty;
                    string shortSubRegion = string.Empty;
                    string layerType = string.Empty;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        columnName = dt.Rows[i][0].ToString();
                        switch (columnName)
                        {
                            case "name": { chartFileDetails.Name = dt.Rows[i][1].ToString(); break; }
                            case "short_name": { chartFileDetails.ShortName = dt.Rows[i][1].ToString(); break; }
                            case "type": { chartFileDetails.Type = dt.Rows[i][1].ToString(); break; }
                            case "version": { chartFileDetails.Version = dt.Rows[i][1].ToString(); break; }
                            case "description": { chartFileDetails.Description = dt.Rows[i][1].ToString(); break; }
                            case "format": { chartFileDetails.Format = dt.Rows[i][1].ToString(); break; }
                            case "exp_date": { chartFileDetails.ExpDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
                            case "expiration": { chartFileDetails.Expiration = Convert.ToDateTime(dt.Rows[i][1]); break; }
                            case "region": { region = dt.Rows[i][1].ToString(); break; }
                            case "short_region": { shortRegion = dt.Rows[i][1].ToString(); break; }
                            case "subregion": { subRegion = dt.Rows[i][1].ToString(); break; }
                            case "short_subregion": { shortSubRegion = dt.Rows[i][1].ToString(); break; }
                            case "provider": { chartFileDetails.Provider = dt.Rows[i][1].ToString(); break; }
                            case "short_provider": { chartFileDetails.ShortProvider = dt.Rows[i][1].ToString(); break; }
                            case "copyright": { chartFileDetails.Copyright = dt.Rows[i][1].ToString(); break; }
                            case "create_date": { chartFileDetails.CreateDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
                            case "layer_type": { layerType = dt.Rows[i][1].ToString(); break; }
                            case "short_layer_type": { chartFileDetails.ShortLayerType = dt.Rows[i][1].ToString(); break; }
                            case "layer_sort": { chartFileDetails.LayerSort = dt.Rows[i][1].ToString(); break; }
                            case "min_zoom": { chartFileDetails.MinZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
                            case "max_zoom": { chartFileDetails.MaxZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
                            case "bounds": { chartFileDetails.Bounds = dt.Rows[i][1].ToString(); break; }
                        }
                    }
                    chartFileDetails.FileSize = (decimal)fileSize;
                    chartFileDetails.FileName = fileName;
                    chartFileDetails.Password = new Encryption().Encrypt(password, Constants.GlobalEncryptionKey);
                    chartFileDetails.CountryId = new AdminHelper().SetCountry(region, shortRegion);
                    chartFileDetails.StateId = new AdminHelper().SetState(subRegion, shortSubRegion);
                    chartFileDetails.ChartTypeId = new AdminHelper().SetChart(layerType);
                    chartFileDetails.LayerType = layerType;
                    chartFileDetails.LastUpdateDate = DateTime.UtcNow;

                    ChartProgressBar.dictionary[randomKey] = 98;


                    if (InsertOrUpdate == "Insert")
                    {
                        objEntity.ChartDetails.Add(chartFileDetails);
                        objEntity.SaveChanges();
                    }
                    else
                    {
                        objEntity.SaveChanges();
                    }


                    ChartProgressBar.dictionary[randomKey] = 100;
                    if (ChartProgressBar.dictionary.ContainsKey(randomKey))
                    {
                        // ChartProgressBar.dictionary.Remove(randomKey);
                    }



                    System.IO.File.Delete(ConfigurationReader.ChartFilePath + @"UnZipFiles\" + sqliteFileName);
                }
                catch (Exception ex)
                {

                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}