﻿using Microsoft.Web.WebSockets;
//using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuardianAvionicsWeb
{
    /// <summary>
    /// Summary description for LiveDataHandler
    /// </summary>
    public class LiveDataHandler : IHttpHandler
    {
        //Logger Log = LogManager.GetCurrentClassLogger();
        public void ProcessRequest(HttpContext context)
        {
            //Log.Fatal("ProcessRequest - " + (context.IsWebSocketRequest ? "true" : "false"));

            if (context.IsWebSocketRequest)
            {
                // context.AcceptWebSocketRequest(ProcessWSChat);
                context.AcceptWebSocketRequest(new MyWebSocketHandler());

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}