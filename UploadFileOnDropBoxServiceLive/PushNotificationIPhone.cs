﻿using System;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using NLog;

namespace UploadFileOnDropBoxServiceLive
{
    public class PushNotificationIPhone
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        //Currently it will raise only for android devices
        static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Do something here
        }

        //this even raised when a notification is successfully sent
        static void NotificationSent(object sender, INotification notification)
        {
            //Do something here
        }

        //this is raised when a notification is failed due to some reason
        static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the channel
        static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the service
        static void ServiceException(object sender, Exception exception)
        {
            //Do something here
        }

        //this is raised when the particular device subscription is expired
        static void DeviceSubscriptionExpired(object sender,
        string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
        }

        //this is raised when the channel is destroyed
        static void ChannelDestroyed(object sender)
        {
            //Do something here
        }

        //this is raised when the channel is created
        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
        }


        public void Push(string[] tokenids, string message)
        {
            //create the puchbroker object
            var push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;


            // send push notification

            //-------------------------
            // APPLE NOTIFICATIONS
            //-------------------------
            //Configure and start Apple APNS
            // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to
            //generate one for connecting to Sandbox, and one for connecting to Production.  You must
            // use the right one, to match the provisioning profile you build your
            //   app with!
            try
            {
                //var appleCert = File.ReadAllBytes(@"D:\MY Data prabal\MyRoadInfo\push notification\Key.p12");



                //var appleCert = File.ReadAllBytes(Server.MapPath(@"../Certificates.p12"));
                //var appleCert = File.ReadAllBytes(Server.MapPath("Resources/key.p12")); 
                //var appleCert = File.ReadAllBytes(@"C:\Ideavate\GuardianAvionics\pushNotifications\key.p12");

                var path = System.Configuration.ConfigurationManager.AppSettings["CertificatePath"];
                //var path = ConfigurationReader.CertificatePath;
                bool isPushNotificationForDistributionCertificate = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsPushNotificationForDistributionCertificate"]);
                var appleCert = System.IO.File.ReadAllBytes(path);

                //IMPORTANT: If you are using a Development provisioning Profile, you must use
                // the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                // 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                //Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                //'true')
                //push.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "password"));


                push.RegisterAppleService(new PushSharp.Apple.ApplePushChannelSettings(isPushNotificationForDistributionCertificate, appleCert, ""));


                //Extension method
                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                // generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, 
                // and the device token is passed back to you

                //    push.RegisterGcmService(new GcmPushChannelSettings("AIzaSyAH7WcgCdJmfGbL8fvc2OlKJ07-_odrQ6A"));

                //    push.QueueNotification(new GcmNotification()
                //                .ForDeviceRegistrationId("APA91bGrCxBmcCqgT46ZeGybaUSQuuhUXEAUBoy_5uDGlYm1DcVHQNwsctAU7HQL72FNRGEMlMVFHosSB8OHlevqep7HnapIkLlcnxnL_3tvMj8c7z7rkNEfdO5TE8SVkOF2n6fat-A5WD_ywIe9RyIEysvkvVZvzQ")
                //                .WithJson("{\"alert\":\"Hello World!" + i + "\",\"badge\":7,\"sound\":\"sound.caf\"}"));

                //}

                foreach (var deviceid in tokenids)
                {
                    logger.Fatal("Push Method : deviceId " +  deviceid + " message = " + message );
                    push.QueueNotification(new PushSharp.Apple.AppleNotification()
                   .ForDeviceToken(deviceid)
                        //the recipient device id
                   .WithAlert(message) //the message
                   .WithBadge(1)
                   .WithSound("sound.caf")
                   );

                    logger.Fatal("Push Method complete");
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                logger.Fatal("Exception in sending Push Notification " , ex);
            }


        }


    }
}
