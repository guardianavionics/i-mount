﻿using GA.DataLayer;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using GA.Common;
using HighchartsExportClient;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UploadFileOnDropBoxServiceLive
{

    public class CreateGoogleMapSnap
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        public Logger logger = LogManager.GetCurrentClassLogger();
        public void GenerateSnapShot()
        {
            var context = new GuardianAvionicsEntities();
            var pilotLogList = context.PilotLogs.Where(p => (p.IsEmailSent != true) && p.DayPIC != "00:00:00" && p.Finished && (p.DropBoxErrorMsg == null || p.DropBoxErrorMsg == "")).ToList();

            //logger.Fatal("PilotLogList Count" + pilotLogList.Count);

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            var dataLogList = new List<DataLogListing>();
            foreach (var pilotLog in pilotLogList)
            {
                try
                {
                    dataLogList = new List<DataLogListing>();
                    //var airframedatalog = context.AirframeDatalogs.Where(a => a.PilotLogId == pilotLog.Id).Select(s => s.DataLog).ToList();

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLog.Id;

                    DataSet dsDataLog = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);

                    if (dsDataLog.Tables.Count > 0)
                    {
                        if (dsDataLog.Tables[0].Rows.Count > 0)
                        {
                            var objairframedatalog = dsDataLog.Tables[0].AsEnumerable()
                         .Select(r => r.Field<string>("DataLog"))
                         .ToList();

                            foreach (var airframedata in objairframedatalog)
                            {
                                dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    StringBuilder str1 = new StringBuilder();
                    StringBuilder str2 = new StringBuilder();
                    var tList = dataLogList.Where(w => w.Latitude != null && w.Latitude != "" && w.Longitude != null && w.Longitude != "").Select(s => new { Latitude = Math.Round(Convert.ToDouble(s.Latitude), 4), Longitude = Math.Round(Convert.ToDouble(s.Longitude), 4) }).Distinct().ToList();
                    foreach (var dl in tList)
                    {
                        str1.Append("|");
                        str1.Append(dl.Latitude);
                        str1.Append(",");
                        str1.Append(dl.Longitude);
                    }

                    var objLast = tList.LastOrDefault();
                    var marker = "markers=icon:https://s3-us-west-2.amazonaws.com/guardianavionics/GuardianLive/Images/DefaultImages/end_marker.png|" + objLast.Latitude + "," + objLast.Longitude + "&";
                    str2.Append("https://maps.googleapis.com/maps/api/staticmap?" + marker + "path=color:0x00BB00|weight:5");
                    str2.Append(str1.ToString());
                    str2.Append("&size=600x200&key=AIzaSyBKZ7qjEccZNVtJXkfZrkwg__3Yp0r_ZjM");

                    while (str2.ToString().Length > 7300)
                    {
                        tList = tList.Where((w, index) => index % 10 != 0).ToList();
                        str1 = new StringBuilder();
                        str2 = new StringBuilder();
                        foreach (var dl in tList)
                        {
                            str1.Append("|");
                            str1.Append(dl.Latitude);
                            str1.Append(",");
                            str1.Append(dl.Longitude);

                        }

                        objLast = tList.LastOrDefault();

                        marker = "markers=icon:https://s3-us-west-2.amazonaws.com/guardianavionics/GuardianLive/Images/DefaultImages/end_marker.png|" + objLast.Latitude + "," + objLast.Longitude + "&";
                        str2.Append("https://maps.googleapis.com/maps/api/staticmap?" + marker + "path=color:0x00BB00|weight:5");
                        str2.Append(str1.ToString());
                        str2.Append("&size=600x200&key=AIzaSyBKZ7qjEccZNVtJXkfZrkwg__3Yp0r_ZjM");
                    }

                    System.Net.WebClient webClient = new System.Net.WebClient();

                    try
                    {
                        logger.Fatal("Google MAP Static Image url for log id = " + pilotLog.Id.ToString() + " is " + str2.ToString());
                        webClient.DownloadFile(str2.ToString(), ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + ".png");

                        new Misc().UploadFile(ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + ".png", pilotLog.Id.ToString() + ".png", "GoogleMapSnapshopPath");

                    }
                    catch (Exception ex)
                    {
                        logger.Fatal("Google MAP Static Image url for log id = " + pilotLog.Id.ToString() + " has exception  " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                    }

                    //try
                    //{
                    //    //////////////////Google Graph snap shot 
                    //    var speedList = dataLogList.Where(w => w.Speed != null && w.Speed != "").Select(s => new { Speed = Convert.ToInt32(s.Speed), Date = Convert.ToDateTime(s.Date) }).ToList();
                    //    if (speedList.Count != 0)
                    //    {
                    //        var result = Task.Run(async () => { return await GetImageLinkFromOptions_Works(speedList.Select(s => s.Speed).ToList(), speedList.Select(s => s.Date.ToString("HH:mm:ss")).ToList(), "Speed"); }).Result;
                    //        logger.Fatal("Google Graph  Static Image url for log id = " + pilotLog.Id.ToString() + "-Speed is " + result);
                    //        webClient.DownloadFile(result, ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-Speed.png");
                    //        new Misc().UploadFile(ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-Speed.png", pilotLog.Id.ToString() + "-Speed.png", "GoogleMapSnapshopPath");
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    logger.Fatal("Exception occurs in GenerateSnapShot() Download Graph Image - " + ex.Message);
                    //}

                    var paramList = new List<string> { "FF", "CHT", "EGT" };
                    var graphSeriesDataList = new List<GraphSeriesData>();
                    List<GraphSeriesData>[] arrGraphDataList = new List<GraphSeriesData>[6];
                    int i = 0;
                    List<EgtChtData> egtChtList = new List<EgtChtData>();
                    string result = "";
                    foreach (var engineParam in paramList)
                    {
                        try
                        {
                            switch (engineParam)
                            {
                                //case "Speed":
                                //    {
                                //        graphSeriesDataList = dataLogList.Where(w => w.Speed != null && w.Speed != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Speed), Date = Convert.ToDateTime(s.Date) }).ToList();
                                //        break;
                                //    }
                                //case "CLD":
                                //    {
                                //        graphSeriesDataList = dataLogList.Where(w => w.CLD != null && w.CLD != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.CLD), Date = Convert.ToDateTime(s.Date) }).ToList();
                                //        break;
                                //    }
                                case "FF":
                                    {
                                        //graphSeriesDataList = dataLogList.Where(w => w.FF != null && w.FF != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.FF), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        egtChtList = new List<EgtChtData>();


                                        graphSeriesDataList = dataLogList.Where(w => w.TIT1 != null && w.TIT1 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.TIT1), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var tit1Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                tit1Array[i] = new double[2];
                                                try
                                                {
                                                    tit1Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    tit1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    tit1Array[i][1] = 0;
                                                    tit1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = tit1Array, name = "TIT", yAxis = 0, color = "black" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.FF != null && w.FF != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.FF), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var ffArray = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                ffArray[i] = new double[2];
                                                try
                                                {
                                                    ffArray[i][1] = Convert.ToDouble(sData.DataValue);
                                                    ffArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    ffArray[i][1] = 0;
                                                    ffArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = ffArray, name = "FF", yAxis = 1, color = "green" });
                                        }


                                        break;
                                    }
                                //case "RPM":
                                //    {
                                //        graphSeriesDataList = dataLogList.Where(w => w.RPM != null && w.RPM != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.RPM), Date = Convert.ToDateTime(s.Date) }).ToList();
                                //        break;
                                //    }

                                case "EGT":
                                    {
                                        egtChtList = new List<EgtChtData>();
                                        graphSeriesDataList = dataLogList.Where(w => w.Egt1 != null && w.Egt1 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt1), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt1Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt1Array[i] = new double[2];
                                                try
                                                {
                                                    egt1Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt1Array[i][1] = 0;
                                                    egt1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt1Array, name = "EGT1", yAxis = 0, color = "red" });
                                        }


                                        graphSeriesDataList = dataLogList.Where(w => w.Egt2 != null && w.Egt2 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt2), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt2Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt2Array[i] = new double[2];
                                                try
                                                {
                                                    egt2Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt2Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt2Array[i][1] = 0;
                                                    egt2Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt2Array, name = "EGT2", yAxis = 0, color = "blue" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Egt3 != null && w.Egt3 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt3), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt3Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt3Array[i] = new double[2];
                                                try
                                                {
                                                    egt3Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt3Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt3Array[i][1] = 0;
                                                    egt3Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt3Array, name = "EGT3", yAxis = 0, color = "black" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Egt4 != null && w.Egt4 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt4), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt4Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt4Array[i] = new double[2];
                                                try
                                                {
                                                    egt4Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt4Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt4Array[i][1] = 0;
                                                    egt4Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt4Array, name = "EGT4", yAxis = 0, color = "orange" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Egt5 != null && w.Egt5 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt5), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt5Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt5Array[i] = new double[2];
                                                try
                                                {
                                                    egt5Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt5Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt5Array[i][1] = 0;
                                                    egt5Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt5Array, name = "EGT5", yAxis = 0, color = "yellow" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Egt6 != null && w.Egt6 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Egt6), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var egt6Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                egt6Array[i] = new double[2];
                                                try
                                                {
                                                    egt6Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    egt6Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    egt6Array[i][1] = 0;
                                                    egt6Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = egt6Array, name = "EGT6", yAxis = 0, color = "#DEB887" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Speed != null && w.Speed != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Speed), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var speedArray = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                speedArray[i] = new double[2];
                                                try
                                                {
                                                    speedArray[i][1] = Convert.ToDouble(sData.DataValue);
                                                    speedArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    speedArray[i][1] = 0;
                                                    speedArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = speedArray, name = "Speed", yAxis = 1, color = "green" });
                                        }
                                        break;
                                    }
                                case "CHT":
                                    {
                                        egtChtList = new List<EgtChtData>();
                                        graphSeriesDataList = dataLogList.Where(w => w.Cht1 != null && w.Cht1 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht1), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht1Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht1Array[i] = new double[2];
                                                try
                                                {
                                                    cht1Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht1Array[i][1] = 0;
                                                    cht1Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht1Array, name = "CHT1", yAxis = 0, color = "red" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Cht2 != null && w.Cht2 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht2), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht2Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht2Array[i] = new double[2];
                                                try
                                                {
                                                    cht2Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht2Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht2Array[i][1] = 0;
                                                    cht2Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht2Array, name = "CHT2", yAxis = 0, color = "blue" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Cht3 != null && w.Cht3 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht3), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht3Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht3Array[i] = new double[2];
                                                try
                                                {
                                                    cht3Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht3Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht3Array[i][1] = 0;
                                                    cht3Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht3Array, name = "CHT3", yAxis = 0, color = "black" });
                                        }



                                        graphSeriesDataList = dataLogList.Where(w => w.Cht4 != null && w.Cht4 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht4), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht4Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht4Array[i] = new double[2];
                                                try
                                                {
                                                    cht4Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht4Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht4Array[i][1] = 0;
                                                    cht4Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht4Array, name = "CHT4", yAxis = 0, color = "orange" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Cht5 != null && w.Cht5 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht5), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht5Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht5Array[i] = new double[2];
                                                try
                                                {
                                                    cht5Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht5Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht5Array[i][1] = 0;
                                                    cht5Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht5Array, name = "CHT5", yAxis = 0, color = "yellow" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.Cht6 != null && w.Cht6 != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.Cht6), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var cht6Array = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                cht6Array[i] = new double[2];
                                                try
                                                {
                                                    cht6Array[i][1] = Convert.ToDouble(sData.DataValue);
                                                    cht6Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    cht6Array[i][1] = 0;
                                                    cht6Array[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = cht6Array, name = "CHT6", yAxis = 0, color = "#DEB887" });
                                        }

                                        graphSeriesDataList = dataLogList.Where(w => w.RPM != null && w.RPM != "").Select(s => new GraphSeriesData { DataValue = Convert.ToDouble(s.RPM), Date = Convert.ToDateTime(s.Date) }).ToList();
                                        if (graphSeriesDataList.Count != 0)
                                        {
                                            i = 0;
                                            var rpmArray = new double[graphSeriesDataList.Count][];
                                            foreach (var sData in graphSeriesDataList)
                                            {
                                                rpmArray[i] = new double[2];
                                                try
                                                {
                                                    rpmArray[i][1] = Convert.ToDouble(sData.DataValue);
                                                    rpmArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date); // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                                                }
                                                catch (Exception e)
                                                {
                                                    rpmArray[i][1] = 0;
                                                    rpmArray[i][0] = Misc.GetMilliSecondsFrom01011970(sData.Date);
                                                }
                                                i++;
                                            }
                                            egtChtList.Add(new EgtChtData { data = rpmArray, name = "RPM", yAxis = 1, color = "green" });
                                        }

                                        break;
                                    }
                            }
                            //if (engineParam != "EGT" && engineParam != "CHT")
                            //{
                            //    if (graphSeriesDataList.Count != 0)
                            //    {
                            //        result = Task.Run(async () => { return await GetImageLinkFromOptions_Works(graphSeriesDataList.Select(s => s.DataValue).ToList(), graphSeriesDataList.Select(s => s.Date.ToString("HH:mm:ss")).ToList(), engineParam); }).Result;
                            //        logger.Fatal("Google Graph  Static Image url for " + engineParam + " log id = " + pilotLog.Id.ToString() + "-" + engineParam + " is " + result);
                            //        webClient.DownloadFile(result, ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-" + engineParam + ".png");
                            //        new Misc().UploadFile(ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-" + engineParam + ".png", pilotLog.Id.ToString() + "-" + engineParam + ".png", "GoogleMapSnapshopPath");
                            //    }
                            //}
                            //else
                            //{
                            //if (egtChtList.Count > 0)
                            //{
                            result = Task.Run(async () => { return await GetImageLinkFromOptions_Works(egtChtList, engineParam); }).Result;
                            logger.Fatal("Google Graph  Static Image url for " + engineParam + " log id = " + pilotLog.Id.ToString() + "-" + engineParam + " is " + result);
                            webClient.DownloadFile(result, ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-" + engineParam + ".png");
                            new Misc().UploadFile(ConfigurationReader.GoogleMapSnapshopPath + pilotLog.Id.ToString() + "-" + engineParam + ".png", pilotLog.Id.ToString() + "-" + engineParam + ".png", "GoogleMapSnapshopPath");
                            //}
                            //}
                        }
                        catch (Exception exLoop)
                        {
                            logger.Fatal("Exception occurs in Google Graph export loop for parameter " + engineParam + ". ErrorMessage - " + exLoop.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Fatal("Exception occurs in GenerateSnapShot() - " + ex.Message);
                }
            }
        }

        public async Task<string> GetImageLinkFromOptions_Works(List<Double> seriesData, List<string> categoryData, string graphHead)
        {
            var client = new HighchartsClient("http://export.highcharts.com/");
            var options = new
            {
                xAxis = new
                {
                    categories = categoryData.ToArray(),
                    type = "datetime"
                },
                series = new[]
                {
                    new { data = seriesData.ToArray(), name = graphHead  } //new[] { 10, 20, 30 } 
                }
            };

            var res = await client.GetChartImageLinkFromOptionsAsync(JsonConvert.SerializeObject(options));


            return res;
        }

        public async Task<string> GetImageLinkFromOptions_Works(List<EgtChtData> arrDataList, string engineParam)
        {
            var client = new HighchartsClient("http://export.highcharts.com/");
            string res = "";
            string leftHeading = "";
            string rightHeading = "";
            string title = "";
            if (engineParam == "EGT")
            {
                leftHeading = "EGT";
                rightHeading = "Speed";
                title = "EGT V/s Speed";
            }
            else if (engineParam == "CHT")
            {
                leftHeading = "CHT";
                rightHeading = "RPM";
                title = "CHT V/s RPM";
            }
            else
            {
                leftHeading = "TIT";
                rightHeading = "FF";
                title = "TIT V/s FF";
            }

            //if (engineParam == "EGT")
            //{
            var options = new
            {
                xAxis = new
                {
                    type = "datetime"
                },
                yAxis = new[] {
                    new {labels = new { style = new  { color = "" } } , title = new { text =  leftHeading, style = new { color = "" } }, opposite = false  },
                    new {labels = new { style = new  { color = "green" } } , title = new { text = rightHeading, style = new { color = "green" }}, opposite = true }
                    },
                series = arrDataList.ToArray(),
                title = new { text = title }
            };
            res = await client.GetChartImageLinkFromOptionsAsync(JsonConvert.SerializeObject(options));
            //}
            //else
            //{
            //    var options = new
            //    {
            //        xAxis = new
            //        {
            //            type = "datetime"
            //        },
            //        series = arrDataList.ToArray()
            //    };
            //    res = await client.GetChartImageLinkFromOptionsAsync(JsonConvert.SerializeObject(options));
            //}

            return res;
        }
    }

    public class GraphSeriesData
    {
        public double DataValue { get; set; }

        public DateTime Date { get; set; }
    }

    public class EgtChtData
    {
        public double[][] data { get; set; }

        public string name { get; set; }

        public int yAxis { get; set; }

        public string color { get; set; }
    }
}
