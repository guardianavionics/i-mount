﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Dropbox.Api;
using NLog;
//using OAuthProtocol;
using Nemiro.OAuth;
using System.Configuration;
using System.IO;
using Dropbox.Api.Files;
using System.Threading.Tasks;

namespace UploadFileOnDropBoxService
{
    
    class DropboxFileUpload
    {
        public Logger logger = LogManager.GetCurrentClassLogger();
      

     

        /// <summary>
        /// Upload File in dropbox.
        /// </summary>
        /// <param name="token">User token</param>
        /// <param name="tokenSecret">User token secret</param>
        /// <param name="filePath">Path where file is saved on server</param>
        /// <param name="fileName">Name of file</param>
        /// <returns>Message for success of faliure</returns>
        //public string UploadFileDropbox(string token = "kt4my1pghxu69o9", string tokenSecret = "A7F29F31-1783-4841-B7B5-CEE8E6EDB676", string filePath = "", string fileName = "")        
        public async Task<string> UploadFileDropbox(string tokenAccess, string filePath, string fileName)
        {
            string consumerKey = ConfigurationManager.AppSettings["DropboxConsumerKey"].Trim();
            string consumerSecret = ConfigurationManager.AppSettings["DropboxConsumerSecret"].Trim();
            try
            {
                //var accessToken = new OAuthProtocol.OAuthToken(token, tokenSecret);
                //var api = new DropboxApi(consumerKey, consumerSecret, accessToken);
                //var file = api.UploadFile("dropbox/Guardian Avionics/Flight Data", fileName, @filePath);

                using (var dbx = new DropboxClient(tokenAccess))
                {
                    using (FileStream stream = System.IO.File.Open(filePath, FileMode.Open))
                    {
                        var response = await dbx.Files.UploadAsync("/Guardian Avionics/Flight Data/" + fileName, WriteMode.Overwrite.Instance, body: stream);
                    }
                };
                return Boolean.TrueString;
            }
            catch (Exception e)
            {
                logger.Fatal("Exception From dropbox", e);
            }
            return string.Empty;
        }
    }
}
