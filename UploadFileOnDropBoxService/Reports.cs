﻿
using GA.Common;
using GA.DataLayer;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Amazon.S3;
using Amazon.Runtime;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.Data;

namespace UploadFileOnDropBoxService
{
    public class Reports
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        //public void EmailReport()
        //{
        //    try
        //    {
        //        int dayOfWeek = (int)DateTime.Now.DayOfWeek;
        //        if (dayOfWeek != 1) //Check for Monday
        //        {
        //            return;
        //        }
        //        StringBuilder strBuilder = new GA.CommonForParseDataFile.Reports().GenerateReport();
        //        var cid = Guid.NewGuid().ToString();

        //        var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Reports.html";

        //        string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");

        //        string h1 = string.Format(htmlBody,
        //            strBuilder.ToString(),
        //            cid
        //            );
        //        AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

        //        // add user Image   C:\Ideavate\GuardianAvionics\GAImages\logo1.png
        //        LinkedResource inline = new LinkedResource(System.Configuration.ConfigurationManager.AppSettings["LogoImagePath"], contentType: new ContentType("image/png"));
        //        inline.ContentId = cid;//Guid.NewGuid().ToString();
        //        inline.ContentType.Name = "Logo";

        //        avHtml.LinkedResources.Add(inline);

        //        MailMessage mail = new MailMessage();
        //        mail.AlternateViews.Add(avHtml);
        //        mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);
        //        mail.To.Add("ash@millionair.com");
        //        mail.CC.Add(System.Configuration.ConfigurationManager.AppSettings["EmailToCC"]);
        //        mail.Subject = "Sales Report";
        //        mail.Body = h1;

        //        mail.IsBodyHtml = true;
        //        SmtpClient smpt = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["Host"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"]));
        //        smpt.EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableSSL"]);
        //        smpt.UseDefaultCredentials = false;
        //        smpt.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailIdForSendingEmail"], System.Configuration.ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
        //        smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        smpt.Send(mail);

        //    }
        //    catch (Exception e)
        //    {
        //        logger.Fatal("Error in report, send email method  SendEmailServer method" + Environment.NewLine + e.Message);
        //    }
        //}



        public void SendReport()
        {
            try
            {
                int dayOfWeek = (int)DateTime.Now.DayOfWeek;
                if (dayOfWeek != 5) //Check for Monday
                {
                    return;
                }
                StringBuilder strBuilder = new StringBuilder();
                StringBuilder strCust = new StringBuilder();
                GenerateReport(out strBuilder, out strCust);

                var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\Report.html";

                string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");

                string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "logo1.png";
                string cidFBImageIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "facebook.png";
                string cidTwitterIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "twitter.png";

                string h1 = string.Format(htmlBody,
                   strBuilder.ToString(),
                   cid, //1
                    cidFBImageIcon, //2
                    cidTwitterIcon, //3
                    strCust //4
                    );


                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);
                bool isFileExist = true;
                Stream fStream;
                fStream = ReadFileFromS3("DefaultImages", "logo1.png", out isFileExist);
                LinkedResource inline = new LinkedResource(fStream, contentType: new ContentType("image/png"));
                inline.ContentId = cid;//Guid.NewGuid().ToString();
                inline.ContentType.Name = "Logo";



                MailMessage mail = new MailMessage();
                //mail.AlternateViews.Add(avHtml);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
                mail.To.Add("atul.agrawal@ideavate.com");
                //      mail.To.Add("ash@millionair.com");
                mail.CC.Add(ConfigurationManager.AppSettings["EmailToCC"]);

                mail.Subject = "Guardian Avionics Report";
                mail.Body = h1;
                mail.Attachments.Add(new Attachment(ConfigurationReader.ExportFilePath + @"\PilotLogReport.csv"));
                mail.IsBodyHtml = true;
                SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
                smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
                smpt.UseDefaultCredentials = false;
                smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailPassword"]);
                smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                smpt.Send(mail);
            }
            catch (Exception e)
            {
                logger.Fatal("Exception : " + Newtonsoft.Json.JsonConvert.SerializeObject(e));
                //ExceptionHandler.ReportError(e, Newtonsoft.Json.JsonConvert.SerializeObject(model == default(PilotLogSummaryEmailModel) ? new PilotLogSummaryEmailModel() : model));
            }
        }


        public StringBuilder GenerateReport(out StringBuilder strBuilder, out StringBuilder strCust)
        {
            strBuilder = new StringBuilder();
            strCust = new StringBuilder();
            DataTable dt = new DataTable("Monthly Logs");
            try
            {
                DateTime dateFrom = DateTime.Now.AddDays(-7).Date;
                DateTime dateFromMonthly = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime dateFromYearly = new DateTime(DateTime.Now.Year, 1, 1);
                DateTime dateTo = DateTime.Now.Date;
                string totalMinFlownWeekly = "";
                string totalMinFlownMonthly = "";
                string totalMinFlownYearly = "";
                int hoursFlown = 0;
                int customerRegisteredInWeek = 0;
                int customerRegisteredInMonth = 0;
                int customerRegisteredInYear = 0;
                int activeAircraftInWeek = 0;
                int activeAircraftInMonth = 0;
                int activeAircraftInYear = 0;


                var context = new GuardianAvionicsEntities();
                var profileList = context.Profiles.Where(w => !w.Deleted && w.DateCreated >= dateFromYearly && w.DateCreated < dateTo && w.UserTypeId != (int)Enumerations.UserType.Maintenance).ToList();

                if (profileList != null)
                {
                    customerRegisteredInYear = profileList.Count;

                    var objTemp = profileList.Where(w => !w.Deleted && w.DateCreated >= dateFromMonthly && w.DateCreated < dateTo).ToList();
                    if (objTemp != null)
                    {
                        customerRegisteredInMonth = objTemp.Count;

                        List<int> aircraftIdList;
                        strCust.Append("<tr bgcolor=\"#c8e2eb\">");
                        strCust.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\">Customer Name</td>");
                        strCust.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\">Email Id</td>");
                        strCust.Append("<td valign=\"top\" width=\"50%\" style=\"border-bottom:1px solid #ddd;\"> <table style=\"width:100%;text-align:center\"><tr  ><td colspan=\"3\">Aircraft</td></tr><tr><td width=\"33%\">NNumber</td><td width=\"33%\">Model</td><td width=\"33%\">UserType</td></tr></table></td>");
                        strCust.Append("</tr>");

                        int sno = 1;
                        foreach (var profile in objTemp)
                        {
                            if (sno % 2 == 0)
                            {
                                strCust.Append("<tr bgcolor=\"#c8e2eb\">");
                            }
                            else
                            {
                                strCust.Append("<tr>");
                            }
                            sno = sno + 1;
                            //there is some entry missing in the User details table due to some error while registration
                            //that's why needs to check for null
                            if (profile.UserDetail != null)
                            {
                                strCust.Append("<td valign=\"top\" width=\"25%\" >" + profile.UserDetail.FirstName + " " + profile.UserDetail.LastName + "</td>");
                            }
                            else
                            {
                                strCust.Append("<td valign=\"top\" width=\"25%\"  ></td>");
                            }

                            strCust.Append("<td valign=\"top\" width=\"25%\"  >" + profile.EmailId + "</td>");

                            aircraftIdList = new List<int>();
                            aircraftIdList = context.MappingAircraftAndPilots.Where(m => m.ProfileId == profile.Id).Select(s => s.AircraftId).ToList();
                            aircraftIdList.AddRange(context.PilotLogs.Where(p => (p.PilotId == profile.Id || p.CoPilotId == profile.Id) && p.FlightDataType == 2).Select(s => s.AircraftId).ToList());
                            aircraftIdList = aircraftIdList.Distinct().ToList();
                            var ObjAircraftList = context.AircraftProfiles.Where(a => aircraftIdList.Contains(a.Id) && !a.Deleted).Select(s => new { NNumber = s.Registration, ModelNo = s.AircraftModelList.ModelName, UserType = (s.OwnerProfileId == profile.Id) ? "Owner" : "Pilot/CoPilot" }).ToList();
                            strCust.Append("<td valign=\"top\" width=\"50%\" style=\"border-bottom:1px solid #ddd;\">");
                            if (ObjAircraftList.Count != 0)
                            {
                                strCust.Append(" <table style=\"width:100%;text-align:center;\">");
                                foreach (var objAircraft in ObjAircraftList)
                                {
                                    strCust.Append("<tr><td width=\"33%\">" + objAircraft.NNumber + "</td><td width=\"33%\">" + objAircraft.ModelNo + "</td><td width=\"33%\">" + objAircraft.UserType + "</td></tr>");
                                }
                                strCust.Append("</table>");
                            }
                            else
                            {
                                strCust.Append(" <table style=\"width:100%;text-align:center;\">");
                                strCust.Append("<tr><td width=\"33%\">-</td><td width=\"33%\">-</td><td width=\"33%\">-</td></tr>");
                                strCust.Append("</table>");
                            }
                            strCust.Append("</td>");

                        }
                    }
                    objTemp = profileList.Where(w => !w.Deleted && w.DateCreated >= dateFrom && w.DateCreated < dateTo).ToList();
                    if (objTemp != null)
                    {
                        customerRegisteredInWeek = objTemp.Count;
                    }

                }
                else
                {
                    strCust.Append("<tr bgcolor=\"#c8e2eb\" style=\"text-align:center\">");
                    strCust.Append("<td valign=\"top\" width=\"25%\"  >Customer Name</td>");
                    strCust.Append("<td valign=\"top\" width=\"25%\"  >Email Id</td>");
                    strCust.Append("<td valign=\"top\" width=\"50%\"  > <table style=\"width:100%\"><tr><td colspan=\"3\">Aircraft</td></tr><tr><td>NNumber</td><td>Model</td><td>UserType</td></tr></table></td>");
                    strCust.Append("</tr>");
                    strCust.Append("<tr bgcolor=\"#c8e2eb\" style=\"text-align:center\">");
                    strCust.Append("<td valign=\"top\" colspan=\"3\" style=\"border-bottom:1px solid #ddd;text-align:center;\">No Record Available</td>");
                    strCust.Append("</tr>");
                }

                TimeSpan time = TimeSpan.FromMinutes(0);
                var pilotLogList = context.PilotLogs.Where(p => p.FlightDataType == 2 && p.Finished && p.Date >= dateFromYearly && p.Date < dateTo).ToList();
                if (pilotLogList != null)
                {
                    pilotLogList.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                    time = TimeSpan.FromMinutes(hoursFlown);
                    totalMinFlownYearly = time.ToString(@"hh\:mm\:ss");

                    activeAircraftInYear = pilotLogList.Select(s => s.AircraftId).Distinct().Count();

                    var objTemp = pilotLogList.Where(p => p.FlightDataType == 2 && p.Finished && p.Date >= dateFromMonthly && p.Date < dateTo).ToList();
                    if (objTemp != null)
                    {
                        hoursFlown = 0;
                        objTemp.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                        time = TimeSpan.FromMinutes(hoursFlown);
                        totalMinFlownMonthly = time.ToString(@"hh\:mm\:ss");
                        activeAircraftInMonth = objTemp.Select(s => s.AircraftId).Distinct().Count();

                        //Now get the Customers List last flown 
                        List<int?> profileIdList = new List<int?>();
                        profileIdList = objTemp.Where(w => w.PilotId != null).Distinct().Select(s => s.PilotId).ToList();
                        profileIdList.AddRange(objTemp.Where(w => w.CoPilotId != null).Distinct().Select(s => s.CoPilotId).ToList());
                        profileIdList = profileIdList.Distinct().ToList();

                        dt.Columns.Add("Name", typeof(string));   //0
                        dt.Columns.Add("Email", typeof(string)); //1
                        dt.Columns.Add("NNumber", typeof(string));  //2
                        dt.Columns.Add("Serial No", typeof(string)); //3
                        dt.Columns.Add("Duration", typeof(string));     //4
                        dt.Columns.Add("LastFlown", typeof(string)); //5 
                        dt.Columns.Add("Id", typeof(int)); //5 

                        DataRow dr = null;

                        foreach (var profileId in profileIdList)
                        {
                            var pilotlog = objTemp.Where(f => f.PilotId == profileId || f.CoPilotId == profileId).OrderByDescending(o => o.Id).FirstOrDefault();
                            dr = dt.NewRow();
                            if (pilotlog.PilotId == profileId)
                            {
                                dr[0] = (pilotlog.Profile.UserDetail == null) ? "" : (pilotlog.Profile.UserDetail.FirstName + " " + pilotlog.Profile.UserDetail.LastName);
                                dr[1] = pilotlog.Profile.EmailId;
                            }
                            else
                            {
                                dr[0] = (pilotlog.Profile1.UserDetail == null) ? "" : (pilotlog.Profile1.UserDetail.FirstName + " " + pilotlog.Profile1.UserDetail.LastName);
                                dr[1] = pilotlog.Profile1.EmailId;
                            }
                            dr[2] = pilotlog.AircraftProfile.Registration;
                            dr[3] = pilotlog.AircraftProfile.AircraftSerialNo;
                            dr[4] = pilotlog.DayPIC;
                            dr[5] = pilotlog.Date.ToShortDateString();
                            dr[6] = pilotlog.Id;
                            dt.Rows.Add(dr);

                        }
                    }
                    objTemp = pilotLogList.Where(p => p.FlightDataType == 2 && p.Finished && p.Date >= dateFrom && p.Date < dateTo).ToList();
                    if (objTemp != null)
                    {
                        hoursFlown = 0;
                        objTemp.ForEach(f => hoursFlown += (int)TimeSpan.Parse(f.DayPIC).TotalMinutes);
                        time = TimeSpan.FromMinutes(hoursFlown);
                        totalMinFlownWeekly = time.ToString(@"hh\:mm\:ss");
                        activeAircraftInWeek = objTemp.Select(s => s.AircraftId).Distinct().Count();
                    }
                }

                strBuilder.Append("<tr bgcolor=\"#c8e2eb\">");
                strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"></td>");
                strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Weekly</strong><br>" + dateFrom.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Monthly</strong><br>" + dateFromMonthly.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                strBuilder.Append("<td valign=\"top\" width=\"25%\" style=\"border-bottom:1px solid #ddd;\"><strong>Yearly</strong><br>" + dateFromYearly.ToString("dd-MMM-yyyy") + " to " + dateTo.ToString("dd-MMM-yyyy") + "</td>");
                strBuilder.Append("</tr>");

                strBuilder.Append("<tr>");
                strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Customer Registered</strong></td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + customerRegisteredInWeek + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + customerRegisteredInMonth + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + customerRegisteredInYear + "</td>");
                strBuilder.Append("</tr>");

                strBuilder.Append("<tr bgcolor=\"#c8e2eb\">");
                strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Total hours flown</strong></td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownWeekly + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownMonthly + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + totalMinFlownYearly + "</td>");
                strBuilder.Append("</tr>");

                strBuilder.Append("<tr>");
                strBuilder.Append("<td valign=\"top\"  style=\"border-bottom:1px solid #ddd;\"><strong>Active Aircrafts</strong></td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInWeek + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInMonth + "</td>");
                strBuilder.Append("<td valign=\"top\"   style=\"border-bottom:1px solid #ddd;\">" + activeAircraftInYear + "</td>");
                strBuilder.Append("</tr>");


                DataView dv = dt.DefaultView;
                dv.Sort = "Id desc";
                dt = dv.ToTable();
                dt.Columns.Remove("Id");
                ExportExcel objExport = new ExportExcel();
                objExport.ExportToCSV(dt, ConfigurationReader.ExportFilePath + @"\PilotLogReport.csv");

            }
            catch (Exception ex)
            {
                return new StringBuilder();
            }
            return strBuilder;
        }

        public Stream ReadFileFromS3(string folderName, string fileName, out bool isFileExist)
        {
            isFileExist = true;
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey))
            {
                Stream rs = new System.IO.MemoryStream();
                GetObjectRequest getObjectRequest = new GetObjectRequest();
                getObjectRequest.BucketName = ConfigurationReader.S3Bucket;
                getObjectRequest.Key = GetS3FolderName(folderName) + fileName;
                try
                {
                    using (var getObjectResponse = client.GetObject(getObjectRequest))
                    {
                        return getObjectResponse.ResponseStream;

                        //below 3 lines of code is to save the s3 bucket file to the local path
                        //getObjectResponse.ResponseStream.CopyTo(rs);
                        //System.Drawing.Image img = System.Drawing.Image.FromStream(rs);
                        //img.Save("D:\\ttt.png", ImageFormat.Png);
                    }
                }
                catch (Exception ex)
                {
                    //Error occurs when the file is not exist on the server
                    isFileExist = false;
                }
                return rs;
            }
        }

        public string GetS3FolderName(string folderName)
        {
            string path = "";
            switch (folderName)
            {
                case "Doc":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketDocPath;
                    break;
                case "MapFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketMapFilePath;
                    break;
                case "Image":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages;
                    break;
                case "IPassVideo":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassVideo;
                    break;
                case "IPassImage":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassImage;
                    break;
                case "IPassPDF":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketIPassPDF;
                    break;
                case "PlateJson":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlateJsonFolder;
                    break;
                case "PlatePNGAndTxt":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketPlatePNGAndTxtFolder;
                    break;
                case "AudioFile":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketAudioFilePath;
                    break;
                case "HardWareFirmware":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketHardwareFirmwareFilePath;
                    break;
                case "DefaultImages":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages;
                    break;
                case "KmlFiles":
                    path = ConfigurationReader.S3BucketEnv + ConfigurationReader.BucketKmlFile;
                    break;
            }
            return path;
        }
    }
}
