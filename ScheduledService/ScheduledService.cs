﻿using GA.Common;
using Schedule;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ScheduledService
{
    public partial class ScheduledService : ServiceBase
    {

        private readonly List<CustomScheduleTimer> scheduleTickTimers = new List<CustomScheduleTimer>();
        private delegate void ScheduleHandler(CustomScheduleTimer cst);

        public ScheduledService()
        {
            InitializeComponent();
            // 1
            //OnStart(new string[1]);
        }

        protected override void OnStart(string[] args)
        {
            ProcessReportAgent();
        }

        protected override void OnStop()
        {
        }

        private void ProcessReportAgent()
        {
           
            List<CustomScheduleTimer> schedules = new List<CustomScheduleTimer> {
                 new CustomScheduleTimer { Interval = "Daily", Time = ConfigurationReader.RMAStatusReportSchedulrTime, ScheduleFor="RMA Status Report" }
            };

            foreach (CustomScheduleTimer schedule in schedules)
            {

                CreateSchedule(schedule);

            }
        }

        private void CreateSchedule(CustomScheduleTimer cst)
        {
            try
            {
                CustomScheduleTimer tempcst = cst;
                string interval = cst.Interval;
                string scheduleFor = cst.ScheduleFor;
                DisposeJob(cst); // Dispose job and remove it.
                cst = tempcst;
                scheduleTickTimers.Add(cst);

                switch (cst.Interval)
                {
                    case "Daily":

                        if (cst.ScheduleFor == "RMA Status Report")
                        {
                            cst.AddJob(new ScheduledTime("Daily", cst.Time), new ScheduleHandler(SendRMAStatusReport), cst);
                            // 3
                            // SendRMAStatusReport(cst);
                            cst.Start();
                        }
                        break;
                    //case "Weekly":
                    //    scheduleFormatString = string.Join(",", interval.Days);
                    //    interval.Days.Sort();
                    //    interval.Days.ForEach(x =>
                    //    {
                    //        cst.AddJob(new ScheduledTime("Weekly", string.Format("{0},{1}", x, interval.Time)), new ScheduleHandler(SendSavedReport), cst);
                    //        cst.Start();
                    //    });
                    //    break;
                    //case "Monthly":
                    //    scheduleFormatString = interval.Date.ToString();
                    //    cst.AddJob(new ScheduledTime("Monthly", string.Format("{0},{1}", interval.Date.ToString(), interval.Time)), new ScheduleHandler(SendSavedReport), cst);
                    //    cst.Start();
                    //    break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void DisposeJob(CustomScheduleTimer cst)
        {
            try
            {
                //get schedule from existing collection and dispose it, and remove from schedule timers list.              
                scheduleTickTimers.Where(x => x.ScheduleFor == cst.ScheduleFor).ToList().ForEach(x =>
                {
                    x.Stop();
                    x.ClearJobs();
                    x.Dispose();
                });
                scheduleTickTimers.RemoveAll(x => x.ScheduleFor == cst.ScheduleFor);
            }
            catch (Exception ex)
            {
            }
        }

        private void SendRMAStatusReport(CustomScheduleTimer cst)
        {
            RMAStatusReport report = new RMAStatusReport();
            report.RMAReport();
            DisposeJob(cst);
            ProcessReportAgent();
        }
    }
}
