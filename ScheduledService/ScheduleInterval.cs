﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduledService
{
   public class ScheduleInterval
    {

        public ScheduleInterval(string Interval, string Time)
        {
            this.Interval = Interval;
            this.Time = Time;
        }

        internal ScheduleInterval()
        {
        }

        #region Properties

        public int Date { get; set; }
        public List<string> Days { get; set; }
        public string Interval { get; set; }
        public string Time { get; set; }

        public string ScheduleFor { get; set; }

        #endregion

    }
}
