﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schedule;
namespace ScheduledService
{
  public  class CustomScheduleTimer : ScheduleTimer
    {
        public CustomScheduleTimer()
        {
             
        }
        public string Interval { get; set; }
        public string ScheduleFor { get; set; }
        public string Time {get;set;}
    }

}
