﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.DataLayer;
using GA.Common;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;

namespace ScheduledService
{
   public class RMAStatusReport
    {
        public void RMAReport()
        {
            var context = new GuardianAvionicsEntities();
            List<string> statusList = new List<string> {
                "Awaiting to Receive part- Return part to Customer","Received- Given to Production for Test/repair/OH as necessary - Return to customer","Completed and given to sales for return to the Customer"
            };
            var statusRMAList = context.StatusRMA.Where(w => statusList.Contains(w.Status)).ToList();

            var result = statusRMAList.Select(r => new {
                Status = r.Status,
                RMA = r.RMA.Select(s => new
                {
                    StatusDateChanged = s.RMAStatusHistory.FirstOrDefault(f=>f.StatusId == s.StatusRMAId) == null ? "" : s.RMAStatusHistory.FirstOrDefault(f => f.StatusId == s.StatusRMAId).StatusUpdateDate.ToString("MM/dd/yyyy"),
                    CustomerName = s.InventoryCustomer.FirstName + " " + s.InventoryCustomer.LastName,
                    CompanyName = s.InventoryCustomer.CompanyName,
                    SerialNo = s.SerialNumber,
                    ModelNo = s.ModelNumber,
                    Description = s.Description
                }).ToList()
            } ).ToList();

            var pathHtmlFile = System.Windows.Forms.Application.StartupPath + "\\Content\\RMAStatusReport.html";
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");
            string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "smart_plane.png";

            StringBuilder strHtml = new StringBuilder();
            string htmlStatus1 = "";
            string htmlStatus2 = "";
            string htmlStatus3 = "";

            var rmaStatus = result.Where(w => w.Status == "Awaiting to Receive part- Return part to Customer").FirstOrDefault();
            foreach (var rma in rmaStatus.RMA)
            {
                strHtml.Append(" <tr>");
                strHtml.Append("<td>"+ rma.StatusDateChanged +"</td>");
                strHtml.Append("<td>" + rma.CompanyName + "</td>");
                strHtml.Append("<td>" + rma.CustomerName + "</td>");
                strHtml.Append("<td>" + rma.SerialNo + "</td>");
                strHtml.Append("<td>" + rma.ModelNo + "</td>");
                strHtml.Append("<td>" + rma.Description + "</td>");
                strHtml.Append(" </tr>");
            }
            htmlStatus1 = strHtml.ToString();
            strHtml.Clear();


            rmaStatus = result.Where(w => w.Status == "Received- Given to Production for Test/repair/OH as necessary - Return to customer").FirstOrDefault();
            foreach (var rma in rmaStatus.RMA)
            {
                strHtml.Append(" <tr>");
                strHtml.Append("<td>" + rma.StatusDateChanged + "</td>");
                strHtml.Append("<td>" + rma.CompanyName + "</td>");
                strHtml.Append("<td>" + rma.CustomerName + "</td>");
                strHtml.Append("<td>" + rma.SerialNo + "</td>");
                strHtml.Append("<td>" + rma.ModelNo + "</td>");
                strHtml.Append("<td>" + rma.Description + "</td>");
                strHtml.Append("</tr>");
            }
            htmlStatus2 = strHtml.ToString();
            strHtml.Clear();


            rmaStatus = result.Where(w => w.Status == "Completed and given to sales for return to the Customer").FirstOrDefault();
            foreach (var rma in rmaStatus.RMA)
            {
                strHtml.Append("<tr>");
                strHtml.Append("<td>" + rma.StatusDateChanged + "</td>");
                strHtml.Append("<td>" + rma.CompanyName + "</td>");
                strHtml.Append("<td>" + rma.CustomerName + "</td>");
                strHtml.Append("<td>" + rma.SerialNo + "</td>");
                strHtml.Append("<td>" + rma.ModelNo + "</td>");
                strHtml.Append("<td>" + rma.Description + "</td>");
                strHtml.Append("</tr>");
            }
            htmlStatus3 = strHtml.ToString();
            strHtml.Clear();

            string h1 = string.Format(htmlBody,
                   cid,
                   htmlStatus1,
                   htmlStatus2,
                   htmlStatus3
                   );

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
            mail.To.Add(ConfigurationManager.AppSettings["MailTo"]); 
            mail.Subject = "Customers Open RMA’s, Action Required";
            mail.Body = h1;
            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailPassword"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);
        }
    }
}
