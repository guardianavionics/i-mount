﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using GA.ApplicationLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using NLog;

namespace GuardianAvionics
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PassengerPro : IPassengerPro
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public PassengerMapFileResponse GetPassengerMapFileUrl()
        {
            var identifier = System.DateTime.UtcNow.Ticks;

            Log.Info("Request GetPassengerMapFileUrl ");
            var response = new UnitDataHelper().GetPassengerMapFileUrl();
            Log.Info("Response GetPassengerMapFileUrl " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            return response;
        }
        
    }
}
