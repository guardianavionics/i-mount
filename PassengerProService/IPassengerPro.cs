﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using System.ComponentModel; // description tag is in this namespace
//using System.EnterpriseServices;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace GuardianAvionics
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPassengerPro
    {
        [Description("Service for fetching url for passenger map file download")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetPassengerMapFileUrl"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PassengerMapFileResponse GetPassengerMapFileUrl();
     
    }
    
}
