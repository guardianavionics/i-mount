﻿using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net.Mail;
using System.Configuration;
using System.IO;
//using NLog;

namespace GA.ApplicationLayer
{
    /// <summary>
    /// Login user
    /// </summary>
    public class LoginHelper
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        public NewLoginResponseModel UserLoginAPI(LoginModel objLogin)
        {
            var objContext = new GuardianAvionicsEntities();
            var profile = objContext.Profiles.FirstOrDefault(p => p.EmailId.ToLower() == objLogin.EmailId.ToLower() && !p.Deleted);
            if (profile == null)
            {
                return new NewLoginResponseModel
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdDoNotExist).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.EmailIdDoNotExist.GetStringValue()
                };
            }
            else
            {
                var validateResponse = ValidateLoginModel(objLogin, objContext);

                if (validateResponse.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
                {
                }
                else
                {
                    if (profile.ThirdPartyId != null)
                    {
                        if (profile.ThirdPartyAccountType == 1)
                        {
                            return new NewLoginResponseModel
                            {
                                ResponseCode = ((int)Enumerations.LoginReturnCodes.AlreadyRegisterWithFaceBookAccount).ToString(),
                                ResponseMessage = Enumerations.LoginReturnCodes.AlreadyRegisterWithFaceBookAccount.GetStringValue()
                            };
                        }
                        else if (profile.ThirdPartyAccountType == 2)
                        {
                            return new NewLoginResponseModel
                            {
                                ResponseCode = ((int)Enumerations.LoginReturnCodes.AlreadyRegisterWithGmailAccount).ToString(),
                                ResponseMessage = Enumerations.LoginReturnCodes.AlreadyRegisterWithGmailAccount.GetStringValue()
                            };
                        }

                    }
                }
            }

            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new NewLoginResponseModel
                    {
                        ResponseCode = ((int)Enumerations.LoginReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.LoginReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                // Give response with all the data
                using (var context = new GuardianAvionicsEntities())
                {
                    // validate login model
                    var validateResponse = ValidateLoginModel(objLogin, context);

                    if (validateResponse.ResponseCode != ((int)Enumerations.LoginReturnCodes.Success).ToString())
                    {
                        return new NewLoginResponseModel
                        {
                            ResponseCode = validateResponse.ResponseCode,
                            ResponseMessage = validateResponse.ResponseMessage
                        };
                    }

                    profile = context.Profiles.FirstOrDefault(p => p.EmailId == objLogin.EmailId && !p.Deleted); // .Include(a => a.UserDetails)
                    var objLoginResponse = new NewLoginResponseModel();

                    if (profile.Id == 1)
                    {
                        // Admin user

                        objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.AdminCannotLogin).ToString();
                        objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.AdminCannotLogin.GetStringValue();

                        return objLoginResponse;
                    }

                    if (profile.IsBlocked)
                    {
                        // User is blocked

                        objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString();
                        objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.UserBlocked.GetStringValue();
                        return objLoginResponse;
                    }
                    if (profile.UserMaster.UserType == Enumerations.UserType.Maintenance.ToString())
                    {

                        objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.MaintenanceUserCannotLoginFromApp).ToString();
                        objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.MaintenanceUserCannotLoginFromApp.GetStringValue();
                        return objLoginResponse;
                    }

                    // set response
                    objLoginResponse.ProfileId = profile.Id;
                    objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString();
                    objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue();
                    objLoginResponse.EmailId = profile.EmailId;
                    objLoginResponse.Password = profile.Password;
                    objLoginResponse.SocialLoginAccountType = profile.ThirdPartyAccountType ?? 0;
                    objLoginResponse.UserType = profile.UserMaster.UserType;
                    objLoginResponse.isNewVersionAvaliable = Convert.ToBoolean(ConfigurationReader.IsNewVersionAvailable);
                    // take a variable and save current date time in it . then save this date time in all the modified values and also return this date time only.


                    //objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1)));
                    objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);


                    var userData = context.UserDetails.FirstOrDefault(u => u.ProfileId == profile.Id);

                    // ReSharper disable once PossibleNullReferenceException
                    objLoginResponse.FirstName = userData.FirstName;
                    objLoginResponse.LastName = userData.LastName;
                    objLoginResponse.StreetAddress = userData.StreetAddress;
                    objLoginResponse.City = userData.City;
                    objLoginResponse.State = userData.State;
                    objLoginResponse.ZipCode = userData.ZipCode.ToString();
                    objLoginResponse.PhoneNumber = userData.PhoneNumber;
                    objLoginResponse.DateOfBirth = Misc.GetStringOnlyDate(userData.DateOfBirth);
                    objLoginResponse.CompanyName = userData.CompanyName;
                    objLoginResponse.EmailId = profile.EmailId;
                    objLoginResponse.Key = profile.RecordId;
                    // save token id

                    if (!String.IsNullOrEmpty(objLogin.NewTokenId))
                    {
                        var pushTObjList = context.PushNotifications.Where(pn => pn.TokenId == objLogin.NewTokenId).ToList();

                        if (pushTObjList.Count() > 0)
                        {
                            context.PushNotifications.RemoveRange(pushTObjList);
                        }
                        context.PushNotifications.Add(CommonHelper.CreatePushNotificationObject(profile.Id, objLogin.NewTokenId, true));
                        context.SaveChanges();
                    }

                    // send user profile image
                    if (!String.IsNullOrEmpty(userData.ImageUrl))
                    {
                        objLoginResponse.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + userData.ImageUrl;
                    }
                    else
                    {
                        objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                    }


                    // add preference

                    var medicalCertificateList = (context.MedicalCertificates.Where(m => m.ProfileId == profile.Id && m.Deleted == false).Select(m => m)).ToList();
                    objLoginResponse.MedicalCertificateList = new List<MedicalCertificateModel>();
                    foreach (var certificate in medicalCertificateList)
                    {
                        objLoginResponse.MedicalCertificateList.Add(
                            new MedicalCertificateModel
                            {
                                ReferenceNumber = certificate.ReferenceNumber,
                                Type = certificate.Type,
                                ValidUntil = Misc.GetStringOnlyDate(certificate.ValidUntil),
                                Id = certificate.Id,
                                UniqueId = certificate.UniqueId ?? default(long),
                                ClassId = certificate.MedicalCertClassId,
                                ClassName = certificate.MedicalCertClassId == null ? "" : certificate.MedicalCertClass.Name,
                            }
                        );
                    }

                    var lincenseList = (context.Licenses.Where(l => l.ProfileId == profile.Id && l.Deleted == false).Select(l => l)).ToList();

                    objLoginResponse.LicensesList = new List<LicensesModel>();

                    foreach (var license in lincenseList)
                    {
                        var ratingsList = (context.Ratings.Where(r => r.LicenseId == license.Id && r.Deleted == false).Select(l => l)).ToList();

                        var objLicenseModel = new LicensesModel { Ratingslist = new List<RatingModel>(), Id = license.Id };

                        foreach (var rating in ratingsList)
                        {
                            objLicenseModel.Ratingslist.Add(
                            new RatingModel
                            {
                                Id = rating.Id,
                                Type = rating.Type,
                                ValidUntil = Misc.GetStringOnlyDate(rating.ValidUntil),
                                UniqueId = rating.UniqueId ?? default(long),
                            }
                            );
                        }

                        objLicenseModel.ReferenceNumber = license.ReferenceNumber;
                        objLicenseModel.Type = license.Type;
                        objLicenseModel.ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil);
                        objLicenseModel.UniqueId = license.UniqueId ?? default(long);

                        objLoginResponse.LicensesList.Add(objLicenseModel);
                    }

                    // set deleted in response

                    if (!String.IsNullOrEmpty(objLogin.LastUpdateDateTime))
                    {
                        var LastUpdateddate = Misc.GetDate(objLogin.LastUpdateDateTime);
                        var deletedMedical = context.MedicalCertificates.Where(m => m.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();
                        var deletedRate = context.Ratings.Where(m => m.License.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted && !m.License.Deleted).Select(m => m.Id).ToList();
                        var deletedLicense = context.Licenses.Where(m => m.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();

                        if (deletedLicense.Count > 0)
                            objLoginResponse.DeletedLicences = deletedLicense.ToArray();
                        else
                            objLoginResponse.DeletedLicences = new int[] { };

                        if (deletedMedical.Count > 0)
                            objLoginResponse.DeletedMedicalCertificate = deletedMedical.ToArray();
                        else
                            objLoginResponse.DeletedLicences = new int[] { };

                        if (deletedRate.Count > 0)
                            objLoginResponse.DeletedRatings = deletedRate.ToArray();
                        else
                            objLoginResponse.DeletedLicences = new int[] { };
                    }

                    //logger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(objLoginResponse));
                    return objLoginResponse;
                }
            }
            catch (Exception ex)
            {
                return new NewLoginResponseModel
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public LoginResponseModel IsFaceBookUser(LoginModel objLogin)
        {
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(p => p.EmailId.ToLower() == objLogin.EmailId.ToLower() && !p.Deleted);
            if (profile == null)
            {
                return new LoginResponseModel
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.InvalidUserNameOrPassword).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.InvalidUserNameOrPassword.GetStringValue()
                };
            }
            else
            {
                var objLoginResponse = ValidateLoginModel(objLogin, context);
                if (objLoginResponse.ResponseCode == ((int)Enumerations.LoginReturnCodes.Success).ToString())
                {

                }
                else
                {
                    if (profile.ThirdPartyId != null)
                    {
                        return new LoginResponseModel
                        {
                            ResponseCode = ((int)Enumerations.LoginReturnCodes.InvalidUserNameOrPassword).ToString(),
                            ResponseMessage = Enumerations.LoginReturnCodes.InvalidUserNameOrPassword.GetStringValue()
                        };
                    }
                }
            }

            return LoginUser(objLogin);
        }

        public string SendOTPEmail(string randomstring, string emailId)
        {
            try
            {
                string body=createEmailBody("", randomstring);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);

                mail.To.Add(emailId);


                mail.Subject = "OTP for 2 factor authentication";
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["Host"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
                smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                smpt.UseDefaultCredentials = false;
                smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailIdForSendingEmail"], ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
                smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                smpt.Send(mail);
                return "sent";
            }
            catch(Exception ex)
            {
                return "not send";
            }
        }
        private string createEmailBody(string userName,  string OTP)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate  
            string filepath = ConfigurationManager.AppSettings["OTPTemplate"].ToString(); 
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{username}", userName); //replacing the required things  
            body = body.Replace("{OTPCode}", OTP);
            return body;

        }

        /// <summary>
        /// Login user and sends response
        /// </summary>
        /// <param name="LoginModel"></param>
        /// <returns></returns>
        public LoginResponseModel LoginUser(LoginModel LoginModel)
        {
            try
            {
                // //Logger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(LoginModel));



                // Give response with all the data
                using (var context = new GuardianAvionicsEntities())
                {
                    // validate login model
                    var objLoginResponse = ValidateLoginModel(LoginModel, context);

                    if (objLoginResponse.ResponseCode != ((int)Enumerations.LoginReturnCodes.Success).ToString())
                        return objLoginResponse;

                    var profile = context.Profiles.FirstOrDefault(p => p.EmailId == LoginModel.EmailId && !p.Deleted); // .Include(a => a.UserDetails)

                    // profile will never be empty as we have checked it in ValidateLoginModel(LoginModel);
                    // ReSharper disable once PossibleNullReferenceException
                    if (profile.UserTypeId == (int)Enumerations.UserType.Admin)
                    {
                        // Admin user

                        objLoginResponse.ProfileId = profile.Id;
                        objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.AdminUser).ToString();
                        objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.AdminUser.GetStringValue();
                        objLoginResponse.FirstName = profile.UserDetail.FirstName;
                        objLoginResponse.UserType = profile.UserMaster.UserType;
                        objLoginResponse.Key = profile.RecordId;

                        if (!String.IsNullOrEmpty(profile.UserDetail.ImageUrl))
                        {
                            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                            if (!string.IsNullOrEmpty(path))
                                objLoginResponse.ImageName = path + profile.UserDetail.ImageUrl;
                            else
                                objLoginResponse.ImageName = string.Empty;
                        }
                        else
                        {
                            objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                        }

                        return objLoginResponse;
                    }

                    if (profile.IsBlocked)
                    {
                        // User is blocked

                        objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString();
                        objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.UserBlocked.GetStringValue();
                        return objLoginResponse;
                    }

                    // set response
                    objLoginResponse.ProfileId = profile.Id;
                    objLoginResponse.ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString();
                    objLoginResponse.ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue();
                    objLoginResponse.EmailId = profile.EmailId;
                    objLoginResponse.Password = profile.Password;
                    objLoginResponse.SocialLoginAccountType = profile.ThirdPartyAccountType ?? 0;
                    objLoginResponse.UserType = profile.UserMaster.UserType;
                    // take a variable and save current date time in it . then save this date time in all the modified values and also return this date time only.


                    //objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1)));
                    objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);


                    var userData = context.UserDetails.FirstOrDefault(u => u.ProfileId == profile.Id);

                    // ReSharper disable once PossibleNullReferenceException
                    objLoginResponse.FirstName = userData.FirstName;
                    objLoginResponse.LastName = userData.LastName;
                    objLoginResponse.StreetAddress = userData.StreetAddress;
                    objLoginResponse.City = userData.City;
                    objLoginResponse.State = userData.State;
                    objLoginResponse.ZipCode = userData.ZipCode.ToString();
                    objLoginResponse.PhoneNumber = userData.PhoneNumber;
                    objLoginResponse.CountryCode = userData.CountryCode;
                    objLoginResponse.DateOfBirth = Misc.GetStringOnlyDate(userData.DateOfBirth);
                    objLoginResponse.CompanyName = userData.CompanyName;
                    objLoginResponse.EmailId = profile.EmailId;
                    objLoginResponse.TwoStepAuthentication = userData.TwoStepAuthentication;
                    objLoginResponse.Key = profile.RecordId;
                    // save token id

                    if (!String.IsNullOrEmpty(LoginModel.NewTokenId))
                    {
                        var pushTObjList = context.PushNotifications.Where(pn => pn.TokenId == LoginModel.NewTokenId).ToList();

                        if (pushTObjList.Count() > 0)
                        {
                            context.PushNotifications.RemoveRange(pushTObjList);
                        }
                        context.PushNotifications.Add(CommonHelper.CreatePushNotificationObject(profile.Id, LoginModel.NewTokenId, true));
                    }

                    // send user profile image
                    if (!String.IsNullOrEmpty(userData.ImageUrl))
                    {
                        //string path = ConfigurationReader.ImageServerPathKey;
                        string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                        if (!string.IsNullOrEmpty(path))
                            objLoginResponse.ImageName = path + userData.ImageUrl;
                        else
                            objLoginResponse.ImageName = string.Empty;
                    }
                    else
                    {
                        objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                    }




                    //var medicalCertificateList = (context.MedicalCertificates.Where(m => m.ProfileId == profile.Id && m.Deleted == false).Select(m => m)).ToList();
                    //objLoginResponse.MedicalCertificateList = new List<MedicalCertificateModel>();
                    //foreach (var certificate in medicalCertificateList)
                    //{
                    //    objLoginResponse.MedicalCertificateList.Add(
                    //        new MedicalCertificateModel
                    //        {
                    //            ReferenceNumber = certificate.ReferenceNumber,
                    //            Type = certificate.Type,
                    //            ValidUntil = Misc.GetStringOnlyDate(certificate.ValidUntil),
                    //            Id = certificate.Id,
                    //            UniqueId = certificate.UniqueId ?? default(long),
                    //            ClassId = certificate.MedicalCertClassId,
                    //            ClassName = certificate.MedicalCertClassId == null ? "" : certificate.MedicalCertClass.Name,
                    //        }
                    //    );
                    //}

                    //var lincenseList = (context.Licenses.Where(l => l.ProfileId == profile.Id && l.Deleted == false).Select(l => l)).ToList();

                    //objLoginResponse.LicensesList = new List<LicensesModel>();

                    //foreach (var license in lincenseList)
                    //{
                    //    var ratingsList = (context.Ratings.Where(r => r.LicenseId == license.Id && r.Deleted == false).Select(l => l)).ToList();

                    //    var objLicenseModel = new LicensesModel { Ratingslist = new List<RatingModel>(), Id = license.Id };

                    //    foreach (var rating in ratingsList)
                    //    {
                    //        objLicenseModel.Ratingslist.Add(
                    //        new RatingModel
                    //        {
                    //            Id = rating.Id,
                    //            Type = rating.Type,
                    //            ValidUntil = Misc.GetStringOnlyDate(rating.ValidUntil),
                    //            UniqueId = rating.UniqueId ?? default(long),
                    //        }
                    //        );
                    //    }

                    //    objLicenseModel.ReferenceNumber = license.ReferenceNumber;
                    //    objLicenseModel.Type = license.Type;
                    //    objLicenseModel.ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil);
                    //    objLicenseModel.UniqueId = license.UniqueId ?? default(long);

                    //    objLoginResponse.LicensesList.Add(objLicenseModel);
                    //}

                    //var aircraftList = context.AircraftProfiles.Where(a => context.MappingAircraftAndPilots.Where(pam => pam.ProfileId == profile.Id && pam.Deleted == false).Select(s => s.AircraftId).ToList().Contains(a.Id) && !a.Deleted).ToList();

                    //if (aircraftList.Count > 0)
                    //{
                    //    objLoginResponse.AircraftList = new List<AircraftModel>();
                    //    foreach (var aircraft in aircraftList)
                    //    {
                    //        objLoginResponse.AircraftList.Add(AircraftProfileResponseModel.Create(aircraft, null));
                    //    }
                    //    IPassengerHelper iPassengerHelper = new IPassengerHelper();
                    //    foreach (var obj in objLoginResponse.AircraftList)
                    //    {
                    //        obj.IPassengerList = iPassengerHelper.GetIPassengerMediaDetailsAPI(obj.AircraftId);
                    //    }
                    //}

                    ////System.Diagnostics.Debug.Write("\n Time taken on login page for getting all the license and there ratings with nested for loop " + DateTime.UtcNow.TimeOfDay + " " + DateTime.UtcNow.Millisecond + Environment.NewLine);

                    //// set deleted in response

                    //if (!String.IsNullOrEmpty(LoginModel.LastUpdateDateTime))
                    //{
                    //    var LastUpdateddate = Misc.GetDate(LoginModel.LastUpdateDateTime);
                    //    var deletedMedical = context.MedicalCertificates.Where(m => m.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();
                    //    var deletedRate = context.Ratings.Where(m => m.License.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted && !m.License.Deleted).Select(m => m.Id).ToList();
                    //    var deletedLicense = context.Licenses.Where(m => m.ProfileId == profile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();

                    //    if (deletedLicense.Count > 0)
                    //        objLoginResponse.DeletedLicences = deletedLicense.ToArray();
                    //    else
                    //        objLoginResponse.DeletedLicences = new int[] { };

                    //    if (deletedMedical.Count > 0)
                    //        objLoginResponse.DeletedMedicalCertificate = deletedMedical.ToArray();
                    //    else
                    //        objLoginResponse.DeletedLicences = new int[] { };

                    //    if (deletedRate.Count > 0)
                    //        objLoginResponse.DeletedRatings = deletedRate.ToArray();
                    //    else
                    //        objLoginResponse.DeletedLicences = new int[] { };
                    //}

                    //objLoginResponse.DropDownListData =
                    //    new AircraftHelper().GetFieldsForDropDownlist(new ManufacturereModelListRequest()
                    //    {
                    //        LastUpdatedDateTime = ""
                    //    });

                    //logger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(objLoginResponse));
                    return objLoginResponse;
                }
            }
            catch (Exception ex)
            {

                var context = new GuardianAvionicsEntities();

                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Web-API";
                errorLog.ErrorSource = "LoginUser";
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = Newtonsoft.Json.JsonConvert.SerializeObject(LoginModel);
                context.ErrorLogs.Add(errorLog);
                return new LoginResponseModel
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue()
                };
            }
        }



        /// <summary>
        /// Validates login model
        /// </summary>
        /// <param name="loginModel"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public LoginResponseModel ValidateLoginModel(LoginModel loginModel, GuardianAvionicsEntities context)
        {
            var objLoginResponse = new LoginResponseModel();

            if (string.IsNullOrEmpty(loginModel.EmailId))
                return objLoginResponse.Create(objLoginResponse.CreateGenerlResponse(Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty));

            if (string.IsNullOrEmpty(loginModel.Password))
                return objLoginResponse.Create(objLoginResponse.CreateGenerlResponse(Enumerations.LoginReturnCodes.PasswordCannotNullOrEmpty));

            var profile = context.Profiles.FirstOrDefault(p => p.EmailId.ToLower() == loginModel.EmailId.ToLower() && !p.Deleted); //  && !p.IsBlocked

            //var profile = context.Profiles.FirstOrDefault(p => String.Equals(p.EmailId, loginModel.EmailId, StringComparison.CurrentCultureIgnoreCase)
            //                                                   && !p.Deleted
            //                                                   && !p.IsBlocked);
            //var profile = context.Profiles.FirstOrDefault(p => String.Equals(p.EmailId, loginModel.EmailId, StringComparison.CurrentCultureIgnoreCase)
            //                                                   && !p.Deleted); // && !p.IsBlocked                                                               

            if (profile == null)
            {
                return objLoginResponse.Create(objLoginResponse.CreateGenerlResponse(Enumerations.LoginReturnCodes.EmailIdDoNotExist));
            }

            var aa = new Misc().Decrypt(profile.Password);
            if (new Misc().Decrypt(profile.Password) != loginModel.Password)
            {
                return objLoginResponse.Create(objLoginResponse.CreateGenerlResponse(Enumerations.LoginReturnCodes.PasswordDoNotExist));
            }

            return objLoginResponse.Create(objLoginResponse.CreateGenerlResponse(Enumerations.LoginReturnCodes.Success));
        }

        public GeneralResponse ChangePassword(ChangePasswordRequetModel model, string UniqueCode = null)
        {
            var response = new GeneralResponse();

            if (string.IsNullOrEmpty(model.EmailId))
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty.GetStringValue()
                };
            }

            if (!model.isQueAndAnsVerified)
            {
                if (string.IsNullOrEmpty(model.Password))
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LoginReturnCodes.PasswordCannotNullOrEmpty).ToString(),
                        ResponseMessage = Enumerations.LoginReturnCodes.PasswordCannotNullOrEmpty.GetStringValue()
                    };
                }
            }

            if (string.IsNullOrEmpty(model.NewPassword))
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.NewPasswordCannotNullOrEmpty).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.NewPasswordCannotNullOrEmpty.GetStringValue()
                };
            }

            var context = new GuardianAvionicsEntities();
            try
            {
                var objProfile = context.Profiles.FirstOrDefault(p => p.EmailId == model.EmailId && !p.Deleted);
                if (objProfile != null)
                {
                    if (model.isQueAndAnsVerified)
                    {
                        objProfile.Password = new Misc().Encrypt(model.NewPassword);

                        if (UniqueCode != null)
                        {
                            var resetPassword = context.ResetPasswords.FirstOrDefault(f => f.UniqueCode == UniqueCode);
                            if (resetPassword != null)
                            {
                                resetPassword.IsUniqueCodeUsed = true;
                            }
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        if (new Misc().Decrypt(objProfile.Password) == model.Password)
                        {
                            objProfile.Password = new Misc().Encrypt(model.NewPassword);
                            context.SaveChanges();
                        }
                        else
                        {
                            return new GeneralResponse
                            {
                                ResponseCode = ((int)Enumerations.LoginReturnCodes.PasswordDoNotExist).ToString(),
                                ResponseMessage = Enumerations.LoginReturnCodes.PasswordDoNotExist.GetStringValue()
                            };
                        }
                    }
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString(),
                        ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue()
                    };
                }
                else
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdDoNotExist).ToString(),
                        ResponseMessage = Enumerations.LoginReturnCodes.EmailIdDoNotExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue()
                };
            }
            finally
            {
                context.Dispose();
            }
        }

        public SecurityQuestionRequestModel GetSecurityQuestion()
        {
            var objResponse = new SecurityQuestionRequestModel();
            var context = new GuardianAvionicsEntities();
            try
            {
                var quetions = context.SecurityQuestions.Where(x=>x.IsActive==true).ToList();

                if (quetions.Count > 0)
                {
                    var sList = quetions.Select(q => new ListItems
                    {
                        text = q.Question,
                        value = q.Id,
                    }).ToList();

                    objResponse.SecurityQuestionList = sList;
                }
                else
                {
                    objResponse.SecurityQuestionList = new List<ListItems>
                    {
                        new ListItems
                        {
                            text = string.Empty,
                            value = 1
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                context.Dispose();
            }
            return objResponse;
        }
    }
}
