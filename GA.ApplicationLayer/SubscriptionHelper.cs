﻿using GA.Common;
using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.ApplicationLayer
{
    public class SubscriptionHelper
    {

        public bool IsSubscriptionActive(int mapSubAndUserId)
        {
            //return true;
            var isSubActive = false;
            var context = new GuardianAvionicsEntities();
            var lastReceipt = new SubscriptionTransaction();
            var lastInappReceipt = new InappReceipt();
            var expDate = DateTime.UtcNow;
            var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == mapSubAndUserId);
            if (mapSubAndUser.PaypalStatusId == (int)Enumerations.PaypalStatus.Cancelled)
            {
                return false;
            }
            if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
            {

                lastReceipt = mapSubAndUser.SubscriptionTransactions.OrderByDescending(f => f.PaymentDate).FirstOrDefault();
                if (lastReceipt != null)
                {
                    switch (mapSubAndUser.SubscriptionPaymentDefinition.RegularFrequency)
                    {
                        case "Weekly":
                            {
                                expDate = lastReceipt.PaymentDate.Value.AddDays(7);
                                break;
                            }
                        case "Monthly":
                            {
                                expDate = lastReceipt.PaymentDate.Value.AddMonths(1);
                                //expDate = lastReceipt.PaymentDate.Value.AddMinutes(10);
                                break;
                            }
                        case "Yearly":
                            {
                                //expDate = lastReceipt.PaymentDate.Value.AddMinutes(10);
                                expDate = lastReceipt.PaymentDate.Value.AddYears(1);
                                break;
                            }
                    }
                    if ((expDate - DateTime.UtcNow).TotalMinutes >= 0)
                    {
                        isSubActive = true;
                    }
                    else
                    {
                        isSubActive = false;
                    }
                }
                else
                {
                    isSubActive = false;
                }
            }
            else if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
            {
                lastInappReceipt = mapSubAndUser.InappReceipts.OrderByDescending(o => o.ExpireDate).FirstOrDefault();
                if (lastInappReceipt != null)
                {
                    isSubActive = ((lastInappReceipt.ExpireDate - DateTime.UtcNow).TotalMinutes >= 0);
                }
                else
                {
                    isSubActive = false;
                }
            }
            else
            {
                isSubActive = ((GetExpiryDateForFreePlan(mapSubAndUser.InappPurchaseDate) - DateTime.UtcNow).TotalMinutes >= 0);
            }

            return isSubActive;
        }


        public DateTime GetExpiryDateForFreePlan(DateTime createDate)
        {
            DateTime expDate = DateTime.UtcNow;
            int freeSubscriptionFrequencyInterval = Convert.ToInt32(ConfigurationReader.FreeSubscriptionFrequencyInterval);
            if (ConfigurationReader.FreeSubscriptionFrequency == "Minutes")
            {
                expDate = createDate.AddMinutes(freeSubscriptionFrequencyInterval);
            }
            else if (ConfigurationReader.FreeSubscriptionFrequency == "Months")
            {
                expDate = createDate.AddMonths(freeSubscriptionFrequencyInterval);
            }
            return expDate;
        }

        public bool IsCurrSubscriptionIsPlan5AndActive(int profileId)
        {
            //return true; //  09/07/2019 temporary remove subscription 

            bool response = false;
            var context = new GuardianAvionicsEntities();
           var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            if (profile.CurrentSubscriptionId != null)
            {
                var isplanActive = IsSubscriptionActive(profile.CurrentSubscriptionId ??0);
                if (isplanActive)
                {
                    var subName = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId).SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName;
                    if (subName == "Plan 5")
                    {
                        response = true;
                    }

                }
            }
            return response;
        }
    }
}
