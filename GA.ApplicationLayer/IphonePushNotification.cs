﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace GA.ApplicationLayer
{
    public class IphonePushNotification
    {
        /// <summary>
        /// Send Push notification to Android Device
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="serverPort"></param>
        /// <param name="certificatePath"></param>
        /// <param name="tokenId"></param>
        /// <param name="alertMsg"></param>
        /// <param name="pushNotificationMessageType"></param>
        /// <param name="phoneId"></param>
        /// <returns></returns>
        public bool PushToiPhone(string serverUrl, int serverPort, string certificatePath,
            string tokenId)
        {

            

            try
            {
                int port = serverPort;
                String hostname = serverUrl;

                //string path = "D:/02 Projects/Smard/Docs/CertificatesFamila.p12";
               // Functions.ApiRequestLogFile("<b>---Time And Date--" + DateTime.Now + "----PushToiPhone-----" + "--------------</serverPort>" + serverPort + "</br>");

                string path = certificatePath;

                X509Certificate2 clientCertificate = new X509Certificate2(path, "", X509KeyStorageFlags.MachineKeySet);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, port);

                SslStream sslStream = new SslStream(client.GetStream(),
                                                    false,
                                                    new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                                    null);

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (Exception e)
                {
                    string exce = e.Message.ToString() + "-------" + e.StackTrace.ToString() +
                                  "-------" + e.InnerException.ToString();
                    client.Close();
                    return false;
                }

                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0); //------- The command
                writer.Write((byte)0); //------- The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //------- The deviceId length (big-endian second byte)
                //3239304632554994d6c5a82f1217d9ff8bf0e52e
                String deviceID = tokenId; //"30b0bc753a8da842524e540fab4530df2b932beb1cd4e7200953f28d5ce031d3";
                writer.Write(HexStringToByteArray(deviceID.ToUpper()));

              String payload = "{\"aps\":{\"alert\":\"" + "Test Msg" + "\",\"badge\":0,\"sound\":\"default\"}, \"Type\" : \"" + Convert.ToInt32(1) + "\"}";
                // String payload = "{\"aps\":{\"alert\":\"" + alertMsg +"\",\"badge\":0,\"sound\":\"default\"}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) +"\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
               /* String payload = string.Empty;
                if (Convert.ToInt32(pushNotificationMessageType) == 1)
                {
                    // payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"DELETE\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Server_DELETE_Device\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                }
                if (Convert.ToInt32(pushNotificationMessageType) == 3)
                {
                    //payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"BLOCK\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Server_BLOCK\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";

                }
                
                // Lock Card : Updated 11 July 2013 : Deepti
                if (Convert.ToInt32(pushNotificationMessageType) == 4)
                {
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Payment_Notify_Blocked\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                }
                if (Convert.ToInt32(pushNotificationMessageType) == 5)
                {
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Payment_Notify_Deleted\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                }
                if (Convert.ToInt32(pushNotificationMessageType) == 6)
                {
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Payment_Notify_Added\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                }

                if (Convert.ToInt32(pushNotificationMessageType) == 7)
                {
                    //payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Transation_Notify_Added\"}}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                    payload = "{\"aps\":{\"alert\": { \"loc-key\" : \"msg_Transation_Notify_Added\",\"loc-args\":[\"" + Convert.ToString(amount) + "\"] }},\"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";
                }*/

                //String payload = "{\"aps\":{\"alert\":\"" + alertMsg + "\"}, \"phoneId\" : \"" + Convert.ToInt32(phoneId) + "\",\"Type\" : \"" + Convert.ToInt32(pushNotificationMessageType) + "\"}";


                //Functions.ApiRequestLogFile("Message---------" + "--------------" + payload + "----alertMsg" + alertMsg + "---Length Of message--" + payload.Length.ToString());
                //objFunction.ApiRequestLogFile("Message---------" + "--------------" + payload);
                writer.Write((byte)0);
                writer.Write((byte)payload.Length);

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                sslStream.Write(array);
                sslStream.Flush();

                client.Close();
                writer.Close();
               // Functions.ApiRequestLogFile("PushToiPhone Send Sucesss");
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);


                return false;
            }
            return true;

        }

        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static byte[] HexStringToByteArray(String s)
        {
            s = s.Replace(" ", "");

            byte[] buffer = new byte[s.Length / 2];

            for (int i = 0; i < s.Length; i += 2)
            {
                buffer[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
            }

            return buffer;
        }
    }
}
