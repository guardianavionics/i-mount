﻿using GA.DataLayer;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA.ApplicationLayer
{




    public class ParseDemoFlightData
    {
        public List<JPIHeaderCalculation> jPIHeaderCalculation;
        public List<JPIHeaderCalculation> jPIHeaderCalculationULP;
        public List<JPIHeaderCalculation> GarminAltitudeAirDataValues;
        public PilotLogRawData g_pilotLogRawData { get; set; }
        public AircraftProfile aircraftProfile;
        public List<string> g_commandArray;
        public DataTable dt_AirframeDataLog;
        public PilotLog g_PilotLog;
        public List<PilotLog> g_PilotLogList = new List<PilotLog>();
        public AirframeDatalog g_FirstDataLog;
        //public AirframeDatalog g_LastDataLog; ***
        public string g_LastDataLog = string.Empty;
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];

        string JPIHeader = "OILP,OILT,RPM,MAP,FF,FP,FQL,FQR,CalculatedFuelRemaining,VOLTS,VOLTS2,AMP,TotalAircraftTime,EngineTime,Cht6,Egt6,Cht5,Egt5,Cht4,Egt4,Cht3,Egt3,Cht2,Egt2,Cht1,Egt1,TIT1,TIT2,ElevatorTrimPosition,UnitsIndicator,FlapPosition,UnitsIndicator2,CarbTemp,UnitsIndicator3,CoolantPressure,UnitsIndicator4,CoolantTemperature,UnitsIndicator5,AMP2,UnitsIndicator6,AileronTrimPosition,UnitsIndicator7,RubberTrimPosition,UnitsIndicator8,FuelQty3,UnitsIndicator9,FuelQty4,UnitsIndicator10,DiscreteInput1,DiscreteInput2,DiscreteInput3,DiscreteInput4";
        string ULPHeader = "IgnStatus,SensorStatus,RPM,ThrottlePosition,Baro,Airtemp,OILP,OILT,FP,FF,EcuTemp,Batteryvoltage,CHT1,CHT2,CHT3,CHT4,CHT5,CHT6,EGT1,EGT2,EGT3,EGT4,EGT5,EGT6,Sen1,Sen2,Sen3,Sen4,Sen5,MAP";

        Dictionary<string, string> GarminEngineDataMessage;
        Dictionary<string, string> GarminAltitudeAirDataMessage;


        public void ParseDataFiles()
        {
            jPIHeaderCalculation = new List<JPIHeaderCalculation>();
            jPIHeaderCalculationULP = new List<JPIHeaderCalculation>();
            GarminAltitudeAirDataValues = new List<JPIHeaderCalculation>();
            g_pilotLogRawData = new PilotLogRawData();
            aircraftProfile = new AircraftProfile();
            g_commandArray = new List<string>();
            dt_AirframeDataLog = new DataTable();

            g_PilotLog = new PilotLog();


            GarminAltitudeAirDataMessage = new Dictionary<string, string>();
            GarminEngineDataMessage = new Dictionary<string, string>();

            GarminAltitudeAirDataMessage.Add("Pitch", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 1, index = 11, value = 4, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("Roll", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 2, index = 15, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("VSpd", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 3, index = 45, value = 4, convertTo = "CelciusToF", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("OAT", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 4, index = 49, value = 3, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });



            GarminEngineDataMessage.Add("OILP", ""); //OILP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 1, index = 11, value = 3, convertTo = "", IsRoundOf = false, KeyName = "OILP" });

            GarminEngineDataMessage.Add("OILT", ""); //OILT
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 2, index = 14, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "OILT" });
            GarminEngineDataMessage.Add("RPM", ""); //RPM
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 3, index = 18, value = 4, convertTo = "", IsRoundOf = false, KeyName = "RPM" });
            GarminEngineDataMessage.Add("MAP", ""); //MAP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 4, index = 26, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "MAP" });
            GarminEngineDataMessage.Add("FF", ""); //FF
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 5, index = 29, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FF" });
            GarminEngineDataMessage.Add("FP", ""); //FP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 6, index = 35, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FP" });
            GarminEngineDataMessage.Add("FQL", ""); //FQL
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 7, index = 38, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FQL" });
            GarminEngineDataMessage.Add("FQR", ""); //FQR
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 8, index = 41, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FQR" });
            GarminEngineDataMessage.Add("CalculatedFuelRemaining", ""); //CalculatedFuelRemaining
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 9, index = 44, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "CalculatedFuelRemaining" });
            GarminEngineDataMessage.Add("Volts", ""); //Volts
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 10, index = 47, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "Volts" });
            GarminEngineDataMessage.Add("Volts2", ""); //Volts2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 11, index = 50, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "Volts2" });
            GarminEngineDataMessage.Add("AMP", ""); //AMP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 12, index = 53, value = 4, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "AMP" });
            GarminEngineDataMessage.Add("TotalAircraftTime", ""); //TotalAircraftTime
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 13, index = 57, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "TotalAircraftTime" });
            GarminEngineDataMessage.Add("EngineTime", ""); //EngineTime
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 14, index = 62, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "EngineTime" });
            GarminEngineDataMessage.Add("Cht6", ""); //Cht6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 15, index = 67, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht6" });
            GarminEngineDataMessage.Add("Egt6", ""); //Egt6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 16, index = 71, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt6" });
            GarminEngineDataMessage.Add("Cht5", ""); //Cht5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 17, index = 75, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht5" });
            GarminEngineDataMessage.Add("Egt5", ""); //Egt5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 18, index = 79, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt5" });
            GarminEngineDataMessage.Add("Cht4", ""); //Cht4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 19, index = 83, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht4" });
            GarminEngineDataMessage.Add("Egt4", ""); //Egt4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 20, index = 87, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt4" });
            GarminEngineDataMessage.Add("Cht3", ""); //Cht3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 21, index = 91, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht3" });
            GarminEngineDataMessage.Add("Egt3", ""); //Egt3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 22, index = 95, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt3" });
            GarminEngineDataMessage.Add("Cht2", ""); //Cht2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 23, index = 99, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht2" });
            GarminEngineDataMessage.Add("Egt2", ""); //Egt2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 24, index = 103, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt2" });
            GarminEngineDataMessage.Add("Cht1", ""); //Cht1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 25, index = 107, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht1" });
            GarminEngineDataMessage.Add("Egt1", ""); //Egt1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 26, index = 111, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt1" });
            GarminEngineDataMessage.Add("TIT1", ""); //TIT1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 27, index = 115, value = 4, convertTo = "", IsRoundOf = false, KeyName = "TIT1" });
            GarminEngineDataMessage.Add("TIT2", ""); //TIT2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 28, index = 119, value = 4, convertTo = "", IsRoundOf = false, KeyName = "TIT2" });
            GarminEngineDataMessage.Add("ElevatorTrimPosition", ""); //ElevatorTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 29, index = 159, value = 5, convertTo = "", IsRoundOf = false, KeyName = "ElevatorTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator", ""); //UnitsIndicator
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 30, index = 164, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator" });
            GarminEngineDataMessage.Add("FlapPosition", ""); //FlapPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 31, index = 177, value = 5, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "FlapPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator2", ""); //UnitsIndicator2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 32, index = 182, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator2" });
            GarminEngineDataMessage.Add("CarbTemp", ""); //CarbTemp
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 33, index = 123, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false, KeyName = "CarbTemp" });
            GarminEngineDataMessage.Add("UnitsIndicator3", ""); //UnitsIndicator3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 34, index = 128, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator3" });
            GarminEngineDataMessage.Add("CoolantPressure", ""); //CoolantPressure
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 35, index = 153, value = 5, convertTo = "OneHundredOfValue", IsRoundOf = false, KeyName = "CoolantPressure" });
            GarminEngineDataMessage.Add("UnitsIndicator4", ""); //UnitsIndicator4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 36, index = 158, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator4" });
            GarminEngineDataMessage.Add("CoolantTemperature", ""); //CoolantTemperature
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 37, index = 147, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false, KeyName = "CoolantTemperature" });
            GarminEngineDataMessage.Add("UnitsIndicator5", ""); //UnitsIndicator5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 38, index = 152, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator5" });
            GarminEngineDataMessage.Add("AMP2", ""); //AMP2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 39, index = 129, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "AMP2" });
            GarminEngineDataMessage.Add("UnitsIndicator6", ""); //UnitsIndicator6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 40, index = 134, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator6" });
            GarminEngineDataMessage.Add("AileronTrimPosition", ""); //AileronTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 41, index = 165, value = 5, convertTo = "", IsRoundOf = false, KeyName = "AileronTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator7", ""); //UnitsIndicator7
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 42, index = 170, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator7" });
            GarminEngineDataMessage.Add("RubberTrimPosition", ""); //RubberTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 43, index = 171, value = 5, convertTo = "", IsRoundOf = false, KeyName = "RubberTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator8", ""); //UnitsIndicator8
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 44, index = 176, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator8" });
            GarminEngineDataMessage.Add("FuelQty3", ""); //FuelQty3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 45, index = 135, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "FuelQty3" });
            GarminEngineDataMessage.Add("UnitsIndicator9", ""); //UnitsIndicator9
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 46, index = 140, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator9" });
            GarminEngineDataMessage.Add("FuelQty4", ""); //FuelQty4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 47, index = 141, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "FuelQty4" });
            GarminEngineDataMessage.Add("UnitsIndicator10", ""); //UnitsIndicator10
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 48, index = 146, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator10" });
            GarminEngineDataMessage.Add("DiscreteInput1", ""); //DiscreteInput1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 49, index = 201, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput1" });
            GarminEngineDataMessage.Add("DiscreteInput2", ""); //DiscreteInput2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 50, index = 202, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput2" });
            GarminEngineDataMessage.Add("DiscreteInput3", ""); //DiscreteInput3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 51, index = 203, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput3" });
            GarminEngineDataMessage.Add("DiscreteInput4", ""); //DiscreteInput4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 52, index = 204, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput4" });


            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 1, index = 0, value = 4, convertTo = "", IsRoundOf = false }); //Ignstatus
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 2, index = 5, value = 4, convertTo = "", IsRoundOf = false }); //SensorStatus
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 3, index = 10, value = 4, convertTo = "", IsRoundOf = false }); //RPM
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 4, index = 15, value = 4, convertTo = "", IsRoundOf = false }); //Throttleposition
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 5, index = 20, value = 4, convertTo = "", IsRoundOf = false }); //Baro
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 6, index = 25, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //Airtemp
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 7, index = 31, value = 5, convertTo = "BarToPressurePerSquareInch", IsRoundOf = true }); //OILP
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 8, index = 37, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //OILT
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 9, index = 43, value = 5, convertTo = "BarToPressurePerSquareInch", IsRoundOf = true }); //FP
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 10, index = 49, value = 5, convertTo = "LitreToGallonPerHrs", IsRoundOf = true }); //FF
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 11, index = 55, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //Ecu temp
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 12, index = 61, value = 5, convertTo = "", IsRoundOf = false }); //Battery voltage
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 13, index = 67, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 14, index = 72, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 15, index = 77, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 16, index = 82, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 17, index = 87, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 18, index = 92, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT6
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 19, index = 97, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 20, index = 102, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 21, index = 107, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 22, index = 112, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 23, index = 117, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 24, index = 122, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT6
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 25, index = 127, value = 4, convertTo = "", IsRoundOf = false }); //Sensor1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 26, index = 132, value = 4, convertTo = "", IsRoundOf = false }); //Sensor2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 27, index = 137, value = 4, convertTo = "", IsRoundOf = false }); //Sensor3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 28, index = 142, value = 4, convertTo = "", IsRoundOf = false }); //Sensor4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 29, index = 147, value = 4, convertTo = "", IsRoundOf = false }); //Sensor5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 30, index = 152, value = 4, convertTo = "OneTenthOfValue", IsRoundOf = true }); //MAP

            dt_AirframeDataLog.Columns.Add("Id", typeof(int));
            dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
            dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
            dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));

            Int32 transactionId = 1;
            List<string> commandList = new List<string>();




            // Example #2
            // Read each line of the file into a string array. Each element
            // of the array is one line of the file.
            string[] lines = System.IO.File.ReadAllLines(@"D:\Projects\Guardian\GuardianDocs\log.txt");

            // Display the file contents by using a foreach loop.
            System.Console.WriteLine("Contents of WriteLines2.txt = ");
            foreach (string line in lines)
            {
                if (line != "")
                {
                    ParsingCommands(transactionId.ToString() + "," + line);
                }
            }
            if (dt_AirframeDataLog.Rows.Count > 0)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "tbAirframeDatalogDemo";
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].Value = dt_AirframeDataLog;
                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLogDemo", param));            }
        }


        public void ParsingCommands(string storedDataCommand)
        {
            try
            {
                #region ParseCommand
                g_commandArray.Clear();
                g_commandArray = storedDataCommand.Split(',').ToList();
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                var context = new GuardianAvionicsEntities();

                switch (g_commandArray[1])
                {

                    case "$PGAVF":
                        {
                            break;
                        }
                    case "$PGAVL":
                        {
                            break;
                        }
                    case "$PGAVW":
                        {
                            break;
                        }
                    case "$PGVCC":
                        {
                            break;
                        }
                    case "$PGAV2":
                        {
                            break;
                        }
                    case "$GPRMC":
                        {


                            //Here we have to get all the commands which are previously saved in the PilotLogRawData by aircraftid
                            //var oldPilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                            var oldGprmc = string.IsNullOrEmpty(g_pilotLogRawData.GPRMC) ? new List<string>() : g_pilotLogRawData.GPRMC.Split(',').ToList();
                            var oldJPIHD = string.IsNullOrEmpty(g_pilotLogRawData.JPIHD) ? (string.IsNullOrEmpty(aircraftProfile.JpiHeader) ? new List<string>() : aircraftProfile.JpiHeader.Split(',').ToList()) : g_pilotLogRawData.JPIHD.Split(',').ToList();
                            var oldJpiDT = string.IsNullOrEmpty(g_pilotLogRawData.JPIDT) ? new List<string>() : g_pilotLogRawData.JPIDT.Split(',').ToList();
                            var oldGpgga = string.IsNullOrEmpty(g_pilotLogRawData.GPGGA) ? new List<string>() : g_pilotLogRawData.GPGGA.Split(',').ToList();
                            var oldRPYL = string.IsNullOrEmpty(g_pilotLogRawData.RPYL) ? new List<string>() : g_pilotLogRawData.RPYL.Split(',').ToList();
                            dictionary.Clear();

                            if (oldGprmc.Count > 0)
                            {
                                // dictionary.Add("Time", oldGprmc[0]);
                                dictionary.Add("Latitude", oldGprmc[2]);
                                dictionary.Add("Longitude", oldGprmc[3]);
                                dictionary.Add("Speed", oldGprmc[4]);
                                dictionary.Add("Date", FormatedDate(oldGprmc[6], oldGprmc[0]).ToString());


                                if (g_PilotLog.CommandRecFrom == "Garmin")
                                {
                                    foreach (var obj in GarminEngineDataMessage)
                                    {
                                        dictionary.Add(obj.Key, obj.Value);
                                    }

                                    foreach (var obj in GarminAltitudeAirDataMessage)
                                    {
                                        dictionary.Add(obj.Key, obj.Value);
                                    }
                                }
                                else
                                {
                                    if (oldJpiDT.Count > 0 && oldJPIHD.Count > 0)
                                    {
                                        if (oldJPIHD.Count == oldJpiDT.Count)
                                        {
                                            for (int i = 0; i < oldJPIHD.Count; i++)
                                            {
                                                if (!dictionary.ContainsKey(oldJPIHD[i]))
                                                {
                                                    dictionary.Add(oldJPIHD[i], oldJpiDT[i]);
                                                }

                                            }
                                        }
                                    }
                                }


                                if (oldGpgga.Count > 0)
                                {
                                    dictionary.Add("Altitude", oldGpgga[8]);
                                }

                                if (oldRPYL.Count > 0)
                                {
                                    dictionary.Add("Roll", oldRPYL[0]);
                                    dictionary.Add("Pitch", oldRPYL[1]);
                                    dictionary.Add("Yaw", oldRPYL[4]);
                                }

                                if (dictionary.Count > 0)
                                {
                                    //Insert json data in Airframe datalog
                                    DataRow dr = dt_AirframeDataLog.NewRow();
                                    dr[0] = 0;
                                    dr[1] = g_PilotLog.Id;
                                    dr[2] = dictionary["Date"];
                                    dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                    dt_AirframeDataLog.Rows.Add(dr);

                                    dictionary.Clear();
                                }
                            }
                            g_pilotLogRawData.GPRMC = string.Empty;

                            var newGRRMC = storedDataCommand.Split(',').ToList();

                            if (newGRRMC.Count != 14)
                            {
                                break;
                            }

                            if (oldGprmc.Count > 0)
                            {
                                if (newGRRMC[1] == oldGprmc[0])
                                {
                                    break;
                                }
                            }

                            if (Convert.ToInt32(newGRRMC[8]) <50)
                            {
                                break;
                            }
                            int lastIndex = newGRRMC.Count - 1;
                            int index = newGRRMC[lastIndex].IndexOf('*');
                            newGRRMC[lastIndex] = newGRRMC[lastIndex].Substring(0, index);
                            newGRRMC.RemoveRange(0, 2);

                            //calculate the Latitude and longitude
                            double degree = Convert.ToDouble(newGRRMC[2].Substring(0, 2));
                            double minute = Convert.ToDouble(newGRRMC[2].Substring(2, newGRRMC[2].Length - 2));
                            string direction = newGRRMC[3];
                            double latitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                            degree = Convert.ToDouble(newGRRMC[4].Substring(0, 3));
                            minute = Convert.ToDouble(newGRRMC[4].Substring(3, newGRRMC[4].Length - 3));
                            direction = newGRRMC[5];
                            double longitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                            newGRRMC[2] = latitude.ToString();
                            newGRRMC[4] = longitude.ToString();

                            newGRRMC.RemoveAt(5); // remove the longitude Direction
                            newGRRMC.RemoveAt(3); // remove the latitude direction

                            g_pilotLogRawData.GPRMC = string.Join(",", newGRRMC);
                            break;
                        }
                    case "$JPIHD":
                        {
                            storedDataCommand = storedDataCommand.Replace("TIT-L", "TIT1").Replace("TIT-R", "TIT2").Replace("OIL-T", "OILT").Replace("OIL-P", "OILP").Replace("OAT-C", "OAT").Replace("WP REQ", "REQ").Replace("H:M", "HM").Replace("BAT", "VOLTS").Replace("FUEL-F", "FF").Replace(",TIT,", ",TIT1,");
                            var jpiHeader = storedDataCommand.Split(',').ToList();
                            jpiHeader.RemoveAt(jpiHeader.Count - 1);
                            jpiHeader.RemoveRange(0, 2);

                            g_pilotLogRawData.JPIHD = string.Join(",", jpiHeader);
                            break;
                        }
                    case "$JPIDT":
                        {
                            g_PilotLog.CommandRecFrom = "JPI";

                            var jpiData = storedDataCommand.Split(',').ToList();

                            jpiData.RemoveAt(jpiData.Count - 1);
                            jpiData.RemoveRange(0, 2);

                            int jpiHeaderCount = 0;
                            if (!string.IsNullOrEmpty(aircraftProfile.JpiHeader))
                            {
                                jpiHeaderCount = aircraftProfile.JpiHeader.Count(f => f == ',') + 1;

                                //Now check that header count and JPIData count id equal or not
                                if (jpiHeaderCount == jpiData.Count)
                                {
                                    double tempValue = 0.0;
                                    //first check that all the values be in double
                                    for (int a = 0; a < jpiData.Count; a++)
                                    {
                                        try
                                        {
                                            tempValue = Convert.ToDouble(jpiData[a]);
                                        }
                                        catch
                                        {
                                            jpiData[a] = jpiData[a] == "NA" ? "" : "0.0";
                                        }
                                    }

                                    g_pilotLogRawData.JPIDT = string.Join(",", jpiData);
                                }
                                else
                                {
                                    var prevJpiData = g_pilotLogRawData.JPIDT.Split(',').ToList();
                                    if (jpiHeaderCount == prevJpiData.Count)
                                    {
                                        g_pilotLogRawData.JPIDT = string.Join(",", prevJpiData);
                                    }
                                }
                                //check that the flight is finished or not
                            }
                            break;
                        }
                    case "$GPGGA":
                        {
                            var gpgga = storedDataCommand.Split(',').ToList();

                            if (gpgga.Count < 11)
                            {
                                break;
                            }
                            gpgga.RemoveRange(0, 2);
                            var flag = false;
                            try
                            {
                                double altitude = Convert.ToDouble(gpgga[8]);
                                flag = true;
                                gpgga[8] = (Math.Ceiling(altitude * 3.28084)).ToString();
                            }
                            catch (Exception)
                            {
                                var prevGpgga = g_pilotLogRawData.GPGGA.Split(',').ToList();

                                if (prevGpgga.Count >= 9)
                                {
                                    gpgga[8] = prevGpgga[8];
                                    flag = true;
                                }
                            }

                            if (flag)
                            {
                                g_pilotLogRawData.GPGGA = string.Join(",", gpgga);
                            }
                            break;
                        }
                    case "$RPYL":
                        {
                            var rpyl = storedDataCommand.Split(',').ToList();
                            if (rpyl.Count >= 6)
                            {
                                rpyl.RemoveRange(0, 2);
                            }
                            g_pilotLogRawData.RPYL = string.Join(",", rpyl);
                            break;
                        }
                    default:
                        {
                            // Parsing JPI command For Garmin
                            if (g_commandArray[1].Substring(0, 2) == "=3")
                            {
                                aircraftProfile.JpiHeader = JPIHeader;

                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);

                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in jPIHeaderCalculation.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    // strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "CelciusToFAndOneTenth":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToFAndOneTenth(Convert.ToDouble(strValue));
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        // strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneHundredOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneHundredOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    GarminEngineDataMessage[dict.KeyName] = strValue;
                                                    //strJPIDT.Append(strValue);
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                g_PilotLog.CommandRecFrom = "Garmin";
                            }
                            else if (g_commandArray[1].Substring(0, 2) == "=1")
                            {

                                aircraftProfile.JpiHeader = JPIHeader;

                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);

                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in GarminAltitudeAirDataValues.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    // strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "CelciusToFAndOneTenth":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToFAndOneTenth(Convert.ToDouble(strValue));
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        // strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneHundredOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneHundredOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                g_PilotLog.CommandRecFrom = "Garmin";
                            }

                            //check for ULP command 
                          else  if (g_commandArray[1].Length == 4 && g_commandArray.Count == 31)
                            {
                                aircraftProfile.JpiHeader = ULPHeader;
                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);
                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in jPIHeaderCalculationULP.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }

                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), true);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "LitreToGallonPerHrs":
                                                {
                                                    try
                                                    {
                                                        strValue = LitreToGallonPerHrs(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "BarToPressurePerSquareInch":
                                                {
                                                    try
                                                    {
                                                        strValue = BarToPressurePerSquareInch(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    strJPIDT.Append(strValue);
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                strJPIDT.Remove(strJPIDT.Length - 1, 1); // remove ',' from the last index
                                g_pilotLogRawData.JPIHD = ULPHeader;
                                g_PilotLog.CommandRecFrom = "ULP";
                                g_pilotLogRawData.JPIDT = strJPIDT.ToString();
                            }
                            break;
                        }
                }
                //  }
                #endregion ParseCommand
            }
            catch (Exception e)
            {
                //  throw;
            }
        }


        public double CalculateLatitudeAndLongitude(double degree, double minute, string strDirection)
        {
            int latsign = 1;

            double second = 0.0;
            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (second / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }

        public DateTime FormatedDate(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(0, 2));
                dd = (dd > 31 || dd < 1) ? 1 : dd;

                var mm = Convert.ToInt32(date.Substring(2, 2));
                mm = (mm > 12 || mm < 1) ? 1 : mm;

                var yy = 0;
                if (date.Length == 6)
                {
                    yy = Convert.ToInt32(date.Substring(4, 2));
                    yy = (yy > 7999 || yy < 1) ? 1 : yy;
                    yy += 2000;
                }
                else
                {
                    yy = Convert.ToInt32(date.Substring(4, 4));
                }


                var h = Convert.ToInt32(time.Substring(0, 2));
                h = (h > 23 || h < 0) ? 1 : h;

                var m = Convert.ToInt32(time.Substring(2, 2));
                m = (m > 59 || m < 0) ? 1 : m;

                var s = Convert.ToInt32(time.Substring(4, 2));
                s = (s > 59 || s < 1) ? 1 : s;

                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));

        }


        public string OneTenthOfValue(double value, bool isroundOf)
        {
            if (isroundOf)
            {
                return Convert.ToString(Math.Round((value * 0.1)));
            }
            else
            {
                return Convert.ToString(Math.Round((value * 0.1), 1, MidpointRounding.ToEven));

            }
        }

        public string OneHundredOfValue(double value, bool isroundOf)
        {
            if (isroundOf)
            {
                return Convert.ToString(Math.Round((value * 0.01)));
            }
            else
            {
                return Convert.ToString(Math.Round((value * 0.01), 1, MidpointRounding.ToEven));

            }
        }

        public string CelciusToF(double value)
        {
            return Convert.ToString(((value * 1.8) + 32));
        }

        public string CelciusToFAndOneTenth(double value)
        {
            return Convert.ToString(((value * 1.8) + 32) * 0.1);
        }

        public string LitreToGallonPerHrs(double value, bool isRoundOf)
        {
            if (isRoundOf)
            {
                return Convert.ToString(Math.Round((value * 0.26), 1, MidpointRounding.ToEven));
            }
            else
            {
                return Convert.ToString(value * 0.26);
            }
        }

        public string BarToPressurePerSquareInch(double value, bool isRoundOf)
        {
            if (isRoundOf)
            {
                return Convert.ToString(Math.Round((value * 14.5037738), 1, MidpointRounding.ToEven));
            }
            else
            {
                return Convert.ToString(value * 14.5037738);
            }
        }
    }
}
