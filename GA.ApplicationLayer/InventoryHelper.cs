﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.DataLayer;
using GA.DataTransfer.Classes_for_Web;
using GA.Common;
using GA.DataTransfer;
using System.Web.Mvc;
using System.Data.Entity.SqlServer;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;

namespace GA.ApplicationLayer
{
    public class InventoryHelper
    {
        public List<PartNumberModel> GetPartNumberInChunk(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.PartNumberMasters.Count();

            var listWithAvailableCount = context.UnitSerialNumbers.GroupBy(g => g.PartNumberId).Where(w => w.Key.ToString() != "").Select(s => new { Id = s.Key, UnitCount = s.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Count() }).ToList();

            //var listWithAvailableCount = context.UnitSerialNumbers.GroupBy(g => g.PurchaseOrder.PartNumberMaster.Id).Where(w => w.Key.ToString() != "").Select(s => new { Id = s.Key, UnitCount = s.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Count() }).ToList();

            List<int> idList = context.EngineeringDocs.Where(w => w.DocumentName == "Image").Select(s => s.Id).ToList();
            var partWithImageNameList = context.EngineeringDocs.Where(w => idList.Contains(w.ParentId) && !w.IsFolder).GroupBy(g => g.ParentId).Where(w => w.FirstOrDefault() != null).Select(s => new { partId = s.FirstOrDefault().PartId, ImageName = s.FirstOrDefault().DocumentName }).ToList();

            IQueryable<PartNumberMaster> partNoList;
            partNoList = context.PartNumberMasters;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                partNoList = partNoList.Where(w => w.PartNumber.ToLower().Contains(search) ||
                                                    w.ModelNumber.ToLower().Contains(search) ||
                                                    w.Description.ToLower().Contains(search) ||
                                                    w.Comment.ToLower().Contains(search) ||
                                                    w.Vendor.Name.ToLower().Contains(search));
            }

            totalRowsAfterFilter = partNoList.Count();

            //Sorting
            switch (sortColumnName)
            {
                case "SNO":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.Id).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "PartNumber":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.PartNumber).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.PartNumber).Skip(start).Take(length);
                        break;
                    }
                case "ModelNumber":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.ModelNumber).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.ModelNumber).Skip(start).Take(length);
                        break;
                    }
                case "Description":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.Description).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.Description).Skip(start).Take(length);
                        break;
                    }
                case "CostPrice":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.CostPrice).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.CostPrice).Skip(start).Take(length);
                        break;
                    }
                case "SalesPrice":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.SalesPrice).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.SalesPrice).Skip(start).Take(length);
                        break;
                    }
                case "RecommendQty":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.RecommendQty).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.RecommendQty).Skip(start).Take(length);
                        break;
                    }
                case "Vendor":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.Vendor).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.Vendor).Skip(start).Take(length);
                        break;
                    }
                case "Comment":
                    {
                        partNoList = sortDirection == "asc" ? partNoList.OrderBy(o => o.Comment).Skip(start).Take(length) : partNoList.OrderByDescending(o => o.Comment).Skip(start).Take(length);
                        break;
                    }

            }

            List<PartNumberModel> partNumberList = partNoList.Select(s => new PartNumberModel
            {
                Id = s.Id,
                PartNumber = s.PartNumber,
                ModelNumber = string.IsNullOrEmpty(s.ModelNumber) ? "" : s.ModelNumber,
                Description = string.IsNullOrEmpty(s.Description) ? "" : s.Description,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable ?? false,
                CostPrice = s.CostPrice ?? 0,
                SalesPrice = s.SalesPrice ?? 0,
                RecommendQuantity = s.RecommendQty ?? 0,
                Comment = string.IsNullOrEmpty(s.Comment) ? "" : s.Comment,
                VendorName = s.Vendor == null ? "" : s.Vendor.Name,
                VendorId = s.DefaultVendorId == null ? null : s.DefaultVendorId.ToString(),
                IsSerialNumberRequired = s.IsSerialNumberRequired ?? true,
                TotalUnitsAvailableForSale = 0,
                MasterPartId = s.MasterPartId,
                IsAliasCreated = s.IsAliasCreated ?? false,
                // AliasList = s.IsAliasCreated == true ? context.PartNumberMasters.Where(w=>w.MasterPartId == s.Id).Select(se=> se.PartNumber).ToList() : new List<string>(),
                IsMasterPart = s.Id == (s.MasterPartId ?? 0),
                MasterPartNumber = (s.Id == (s.MasterPartId ?? 0)) ? "" : context.PartNumberMasters.FirstOrDefault(f => f.Id == s.MasterPartId).PartNumber,
                IsInActive = s.IsInActive ?? false,
                CanSold = s.CanSold ?? true
            }).ToList();


            partNumberList.Join(listWithAvailableCount, (allParts) => allParts.Id, (partCountList) => partCountList.Id, (allParts, partCountList) =>
            {
                allParts.TotalUnitsAvailableForSale = partCountList.UnitCount;
                return allParts;
            }).ToList();


            partNumberList.Join(partWithImageNameList, (allParts) => allParts.Id, (imageNameList) => imageNameList.partId, (allParts, imageNameList) =>
            {
                allParts.ImageName = imageNameList.ImageName;
                return allParts;
            }).ToList();

            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;

            Misc misc = new Misc();

            if (sortDirection == "asc")
            {
                partNumberList.ForEach(f =>
                {
                    f.AliasList = f.IsAliasCreated == true ? context.PartNumberMasters.Where(w => w.MasterPartId == f.Id && w.Id != f.Id).Select(se => se.PartNumber).ToList() : new List<string>();
                    f.PartNumberId = misc.Encrypt(f.Id.ToString());
                    f.ImageName = string.IsNullOrEmpty(f.ImageName) ? "" : path + f.Id.ToString() + "/Image/" + f.ImageName;
                    f.SNO = ++start;
                    f.Id = 0;
                    f.VendorId = (f.VendorId != null) ? misc.Encrypt(f.VendorId) : "";
                });
            }
            else
            {
                start = totalRecord - start;
                partNumberList.ForEach(f =>
                {
                    f.AliasList = f.IsAliasCreated == true ? context.PartNumberMasters.Where(w => w.MasterPartId == f.Id && w.Id != f.Id).Select(se => se.PartNumber).ToList() : new List<string>();
                    f.PartNumberId = misc.Encrypt(f.Id.ToString());
                    f.ImageName = string.IsNullOrEmpty(f.ImageName) ? "" : path + f.Id.ToString() + "/Image/" + f.ImageName;
                    f.SNO = start--;
                    f.Id = 0;
                    f.VendorId = (f.VendorId != null) ? misc.Encrypt(f.VendorId) : "";
                });
            }



            return partNumberList;
        }

        public List<InventoryCustomerModel> TestCustomer(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.InventoryCustomer.Count();

            IQueryable<InventoryCustomer> invCustomer;
            invCustomer = context.InventoryCustomer;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                invCustomer = invCustomer.Where(w => w.FirstName.ToLower().Contains(search) ||
                                                    w.LastName.ToLower().Contains(search) ||
                                                    w.Email.ToLower().Contains(search) ||
                                                    w.CompanyName.ToLower().Contains(search) ||
                                                    w.PhoneNo.ToLower().Contains(search));
            }

            totalRowsAfterFilter = invCustomer.Count();

            //Sorting
            switch (sortColumnName)
            {
                case "SNO":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.Id).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "FirstName":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.FirstName).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.FirstName).Skip(start).Take(length);
                        break;
                    }
                case "LastName":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.LastName).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.LastName).Skip(start).Take(length);
                        break;
                    }
                case "Email":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.Email).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.Email).Skip(start).Take(length);
                        break;
                    }
                case "CompanyName":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.CompanyName).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.CompanyName).Skip(start).Take(length);
                        break;
                    }
                case "PhoneNo":
                    {
                        invCustomer = sortDirection == "asc" ? invCustomer.OrderBy(o => o.PhoneNo).Skip(start).Take(length) : invCustomer.OrderByDescending(o => o.PhoneNo).Skip(start).Take(length);
                        break;
                    }
            }


            Misc misc = new Misc();

            //Paging
            List<InventoryCustomerModel> response = invCustomer.Select(s => new InventoryCustomerModel
            {
                CustomerTypeId = s.CustomerTypeId == null ? "" : s.CustomerTypeId.ToString(),
                CustomerType = s.CustomerTypeId == null ? "" : s.CustomerType.Name,
                FirstName = string.IsNullOrEmpty(s.FirstName) ? "" : s.FirstName.Trim(),
                LastName = string.IsNullOrEmpty(s.LastName) ? "" : s.LastName.Trim(),
                Email = s.Email,
                PhoneNo = s.PhoneNo,
                Id = s.Id,
                CompanyName = string.IsNullOrEmpty(s.CompanyName) ? "" : s.CompanyName
            }).ToList();

            if (sortDirection == "asc")
            {
                response.ForEach(f => { f.SNO = ++start; f.EncryptId = misc.Encrypt(f.Id.ToString()); f.CustomerTypeId = (f.CustomerTypeId == "") ? "" : misc.Encrypt(f.CustomerTypeId.ToString()); f.Id = 0; });
            }
            else
            {
                start = totalRecord - start;
                response.ForEach(f => { f.SNO = start--; f.EncryptId = misc.Encrypt(f.Id.ToString()); f.CustomerTypeId = (f.CustomerTypeId == "") ? "" : misc.Encrypt(f.CustomerTypeId.ToString()); f.Id = 0; });
            }


            return response;
        }


        public List<PurchaseOrderModel> GetPurchaseOrderInChunk(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.PurchaseOrders.Count();


            IQueryable<PurchaseOrder> purchaseOrderList;
            purchaseOrderList = context.PurchaseOrders;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                purchaseOrderList = purchaseOrderList.Where(w => w.PurchaseOrderNumber.ToLower().Contains(search) ||
                                                    w.PartNumberMaster.PartNumber.ToLower().Contains(search) ||
                                                    w.PartNumberMaster.Description.ToLower().Contains(search) ||
                                                    w.Vendor.Name.ToLower().Contains(search) ||
                                                    w.InventoryReceiver.Name.ToLower().Contains(search) ||
                                                    w.StartSerialNo.ToString().Contains(search) ||
                                                    (w.StartSerialNo + w.Quantity).ToString().Contains(search) ||
                                                    w.StartBoardSerialNo.ToString().Contains(search) ||
                                                    (w.StartBoardSerialNo + w.Quantity).ToString().Contains(search));
            }

            totalRowsAfterFilter = purchaseOrderList.Count();

            //Sorting
            switch (sortColumnName)
            {
                case "SNO":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.Id).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "PurchaseOrderNumber":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.PurchaseOrderNumber).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.PurchaseOrderNumber).Skip(start).Take(length);
                        break;
                    }
                case "PartNo":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.PartNumberMaster.PartNumber).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.PartNumberMaster.PartNumber).Skip(start).Take(length);
                        break;
                    }
                case "Description":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.PartNumberMaster.Description).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.PartNumberMaster.Description).Skip(start).Take(length);
                        break;
                    }
                case "SoftwareLevel":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.SoftwareLevel).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.SoftwareLevel).Skip(start).Take(length);
                        break;
                    }
                case "VendorName":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.Vendor.Name).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.Vendor.Name).Skip(start).Take(length);
                        break;
                    }
                case "ReceivedBy":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.InventoryReceiver.Name).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.InventoryReceiver.Name).Skip(start).Take(length);
                        break;
                    }
                case "Quantity":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.Quantity).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.Quantity).Skip(start).Take(length);
                        break;
                    }
                case "StartSerialNo":
                    {
                        purchaseOrderList = sortDirection == "asc" ? purchaseOrderList.OrderBy(o => o.StartSerialNo).Skip(start).Take(length) : purchaseOrderList.OrderByDescending(o => o.StartSerialNo).Skip(start).Take(length);
                        break;
                    }

            }

            List<PurchaseOrderModel> response = purchaseOrderList.Select(s => new PurchaseOrderModel
            {
                Id = s.Id,
                CreateDate = s.CreateDate,
                CreatedBy = s.CreatedBy,
                CreateByName = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                PartNo = s.PartNumberMaster.PartNumber,
                Description = s.PartNumberMaster.Description,
                PurchaseOrderNumber = s.PurchaseOrderNumber,
                Quantity = s.Quantity,
                IsSoftwareLevelApplicable = s.PartNumberMaster.IsSoftwareLevelApplicable ?? false,
                SoftwareLevel = s.SoftwareLevel,
                ReceivedBy = s.InventoryReceiverId == null ? "" : s.InventoryReceiver.Name,
                StartSerialNo = s.StartSerialNo,
                VendorName = s.Vendor == null ? "" : s.Vendor.Name,
                StartingBoardSerialNo = s.StartBoardSerialNo,
                IsSerialNumberRequired = s.PartNumberMaster.IsSerialNumberRequired ?? true
            }).ToList();

            int SerialNo = response.Count;

            Misc misc = new Misc();

            if (sortDirection == "asc")
            {
                response.ForEach(f =>
                {
                    f.PurchaseOrderId = misc.Encrypt(f.Id.ToString());
                    f.Id = 0;
                    f.SNO = ++start;
                    f.CreatedOn = f.CreateDate.ToString("MM/dd/yyyy");
                });
            }
            else
            {
                start = totalRecord - start;
                response.ForEach(f =>
                {
                    f.PurchaseOrderId = misc.Encrypt(f.Id.ToString());
                    f.SNO = start--;
                    f.Id = 0;
                    f.CreatedOn = f.CreateDate.ToString("MM/dd/yyyy");
                });
            }
            return response;
        }

        public List<PurchaseOrderModel> GetPurchaseOrderList()
        {
            var context = new GuardianAvionicsEntities();
            List<PurchaseOrderModel> purchaseOrderList = context.PurchaseOrders.OrderByDescending(o => o.Id).Select(s => new PurchaseOrderModel
            {
                Id = s.Id,
                CreateDate = s.CreateDate,
                CreatedBy = s.CreatedBy,
                CreateByName = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                PartNo = s.PartNumberMaster.PartNumber,
                Description = s.PartNumberMaster.Description,
                PurchaseOrderNumber = s.PurchaseOrderNumber,
                Quantity = s.Quantity,
                IsSoftwareLevelApplicable = s.PartNumberMaster.IsSoftwareLevelApplicable ?? false,
                SoftwareLevel = s.SoftwareLevel,
                ReceivedBy = s.InventoryReceiverId == null ? "" : s.InventoryReceiver.Name,
                StartSerialNo = s.StartSerialNo,
                VendorName = s.Vendor == null ? "" : s.Vendor.Name,
                StartingBoardSerialNo = s.StartBoardSerialNo,
                IsSerialNumberRequired = s.PartNumberMaster.IsSerialNumberRequired ?? true
            }).ToList();
            Misc misc = new Misc();
            int SerialNo = purchaseOrderList.Count;
            purchaseOrderList.ForEach(f => { f.PurchaseOrderId = misc.Encrypt(f.Id.ToString()); f.SNO = SerialNo--; f.Id = 0; });
            return purchaseOrderList;
        }

        public PurchaseOrderModel GetPurchaseOrderById(int id)
        {
            var context = new GuardianAvionicsEntities();
            Misc misc = new Misc();
            PurchaseOrderModel purchaseOrderModel = new PurchaseOrderModel();
            PurchaseOrder purchaseOrder = context.PurchaseOrders.FirstOrDefault(f => f.Id == id);
            if (purchaseOrder != null)
            {
                purchaseOrderModel.CreateDate = purchaseOrder.CreateDate;
                purchaseOrderModel.CreatedBy = purchaseOrder.CreatedBy;
                purchaseOrderModel.CreateByName = purchaseOrder.Profile.UserDetail.FirstName + " " + purchaseOrder.Profile.UserDetail.LastName;
                purchaseOrderModel.PartNo = purchaseOrder.PartNumberMaster.PartNumber;
                purchaseOrderModel.Description = purchaseOrder.PartNumberMaster.Description;
                purchaseOrderModel.PartNumberId = misc.Encrypt(purchaseOrder.PartNumberId.ToString());
                purchaseOrderModel.PurchaseOrderNumber = purchaseOrder.PurchaseOrderNumber;
                purchaseOrderModel.Quantity = purchaseOrder.Quantity;
                purchaseOrderModel.StartSerialNo = purchaseOrder.StartSerialNo;
                purchaseOrderModel.VendorId = purchaseOrder.VendorId == null ? "" : misc.Encrypt(purchaseOrder.VendorId.ToString());
                purchaseOrderModel.ReceiverId = purchaseOrder.InventoryReceiverId == null ? "" : misc.Encrypt(purchaseOrder.InventoryReceiverId.ToString());
                purchaseOrderModel.PurchaseOrderId = misc.Encrypt(purchaseOrder.Id.ToString());
            }
            return purchaseOrderModel;
        }

        public void EngineeringFolderTemp()
        {
            var context = new GuardianAvionicsEntities();

            var partList = context.PartNumberMasters.ToList();
            AWS.AWS aws = new AWS.AWS();
            foreach (var part in partList)
            {
                EngineeringDocs engDocs = new EngineeringDocs
                {
                    DocumentName = "Documents",
                    IsFolder = true,
                    ParentId = 0,
                    IsDefault = true,
                    PartId = part.Id
                };
                context.EngineeringDocs.Add(engDocs);
                context.SaveChanges();

                List<EngineeringDocsModel> engDocList = new List<EngineeringDocsModel> {
                    new EngineeringDocsModel  {SNO = 1 , ParentId =engDocs.Id, DocumentName = "Customer", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 2 , ParentId =engDocs.Id, DocumentName = "Image", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 3 , ParentId =engDocs.Id, DocumentName = "Production", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 4 , ParentId =engDocs.Id, DocumentName = "Marketing", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 5 , ParentId =engDocs.Id, DocumentName = "Engineering", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 6 , ParentId =0, DocumentName = "Archives", ParentName = "Customer" },
                    new EngineeringDocsModel {SNO = 7 , ParentId =0, DocumentName = "Archives", ParentName = "Image" },
                    new EngineeringDocsModel {SNO = 8 , ParentId =0, DocumentName = "Archives", ParentName = "Production" },
                    new EngineeringDocsModel {SNO = 9 , ParentId =0, DocumentName = "Archives", ParentName = "Marketing" },
                    new EngineeringDocsModel {SNO = 10 , ParentId =0, DocumentName = "Certification", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 11 , ParentId =0, DocumentName = "Electrical", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 12 , ParentId =0, DocumentName = "Mechanical", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 13 , ParentId =0, DocumentName = "Requirements Documents", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 14 , ParentId =0, DocumentName = "Archives", ParentName = "Certification" },
                    new EngineeringDocsModel {SNO = 15 , ParentId =0, DocumentName = "BOM", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 16 , ParentId =0, DocumentName = "Firmware", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 17 , ParentId =0, DocumentName = "Layout", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 18 , ParentId =0, DocumentName = "Schematic", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 19 , ParentId =0, DocumentName = "Test Documents", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 20 , ParentId =0, DocumentName = "Archives", ParentName = "BOM" },
                    new EngineeringDocsModel {SNO = 21 , ParentId =0, DocumentName = "Archives", ParentName = "Firmware" },
                    new EngineeringDocsModel {SNO = 22 , ParentId =0, DocumentName = "Archives", ParentName = "Layout" },
                    new EngineeringDocsModel {SNO = 23 , ParentId =0, DocumentName = "Archives", ParentName = "Schematic" },
                    new EngineeringDocsModel {SNO = 24 , ParentId =0, DocumentName = "Archives", ParentName = "Test Documents" },
                    new EngineeringDocsModel {SNO = 25 , ParentId =0, DocumentName = "2D Panel Installation Ref", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 26 , ParentId =0, DocumentName = "2D Technical Prints", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 27 , ParentId =0, DocumentName = "3D Tooling Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 28 , ParentId =0, DocumentName = "3D Viewable eDrawings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 29 , ParentId =0, DocumentName = "Decal Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 30 , ParentId =0, DocumentName = "Final Color Renderings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 31 , ParentId =0, DocumentName = "Illustration Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 32 , ParentId =0, DocumentName = "Installation Drawings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 33 , ParentId =0, DocumentName = "Outdated", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 34 , ParentId =0, DocumentName = "Archives", ParentName = "Requirements Documents" }
                };

                EngineeringDocs engSubDocs = new EngineeringDocs();
                foreach (var doc in engDocList.OrderBy(o => o.SNO))
                {
                    engSubDocs = new EngineeringDocs
                    {
                        DocumentName = doc.DocumentName,
                        IsFolder = true,
                        ParentId = doc.ParentId,
                        PartId = part.Id,
                        IsDefault = true
                    };
                    context.EngineeringDocs.Add(engSubDocs);
                    context.SaveChanges();
                    engDocList.Where(w => w.ParentName == engSubDocs.DocumentName).ToList().ForEach(f => f.ParentId = engSubDocs.Id);
                }

                aws.CreateFolderOns3(engDocs.PartId.ToString(), "");

                //Now Create the subFolders
                string sourcePath = new Misc().GetS3FolderName("EngineeringDocs") + "RootDocument";
                string destinationPath = new Misc().GetS3FolderName("EngineeringDocs") + GetParentFolderPath(engDocs.Id);
                aws.AddSubFoldersInRootOfEngDocs(sourcePath, destinationPath);
            }

        }

        public GeneralResponse AddPartNumber(PartNumberModel partNumberModel)
        {

            var response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                PartNumberMaster partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNumberModel.PartNumber);
                if (partNumberMaster != null)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.PartNoAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.PartNumber.PartNoAlreadyExist.GetStringValue();
                    return response;
                }
                partNumberMaster = new PartNumberMaster();
                partNumberMaster.PartNumber = partNumberModel.PartNumber;
                partNumberMaster.Description = partNumberModel.Description;
                partNumberMaster.IsSoftwareLevelApplicable = partNumberModel.IsSoftwareLevelApplicable;
                partNumberMaster.CostPrice = Math.Round(partNumberModel.CostPrice, 4);
                partNumberMaster.CanSold = partNumberModel.CanSold;
                if (partNumberModel.CanSold)
                {
                    partNumberMaster.IsSerialNumberRequired = partNumberModel.IsSerialNumberRequired;

                    partNumberMaster.SalesPrice = partNumberModel.SalesPrice;
                }
                else
                {
                    partNumberMaster.IsSerialNumberRequired = false;
                    partNumberMaster.SalesPrice = 0;
                }
                partNumberMaster.RecommendQty = partNumberModel.RecommendQuantity;
                partNumberMaster.Comment = partNumberModel.Comment;
                partNumberMaster.ModelNumber = partNumberModel.ModelNumber;
                partNumberMaster.IsInActive = partNumberModel.IsInActive;
                partNumberMaster.DefaultVendorId = string.IsNullOrEmpty(partNumberModel.VendorId) ? (int?)null : Convert.ToInt32(partNumberModel.VendorId);
                context.PartNumberMasters.Add(partNumberMaster);
                context.SaveChanges();
                partNumberMaster.MasterPartId = partNumberMaster.Id;

                if (partNumberModel.PartNumber.Contains("-"))
                {
                    string index = partNumberModel.PartNumber.Split('-')[0] + "-";
                    var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex == index);
                    if (partIndex != null)
                    {
                        partIndex.LastAddedPartNo = partNumberModel.PartNumber;
                    }
                }
                context.SaveChanges();
                EngineeringDocs engDocs = new EngineeringDocs
                {
                    DocumentName = "Documents",
                    IsFolder = true,
                    ParentId = 0,
                    IsDefault = true,
                    PartId = partNumberMaster.Id
                };
                context.EngineeringDocs.Add(engDocs);
                context.SaveChanges();


                List<EngineeringDocsModel> engDocList = new List<EngineeringDocsModel> {
                    new EngineeringDocsModel  {SNO = 1 , ParentId =engDocs.Id, DocumentName = "Customer", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 2 , ParentId =engDocs.Id, DocumentName = "Image", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 3 , ParentId =engDocs.Id, DocumentName = "Production", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 4 , ParentId =engDocs.Id, DocumentName = "Marketing", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 5 , ParentId =engDocs.Id, DocumentName = "Engineering", ParentName = "Documents" },
                    new EngineeringDocsModel {SNO = 6 , ParentId =0, DocumentName = "Archives", ParentName = "Customer" },
                    new EngineeringDocsModel {SNO = 7 , ParentId =0, DocumentName = "Archives", ParentName = "Image" },
                    new EngineeringDocsModel {SNO = 8 , ParentId =0, DocumentName = "Archives", ParentName = "Production" },
                    new EngineeringDocsModel {SNO = 9 , ParentId =0, DocumentName = "Archives", ParentName = "Marketing" },
                    new EngineeringDocsModel {SNO = 10 , ParentId =0, DocumentName = "Certification", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 11 , ParentId =0, DocumentName = "Electrical", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 12 , ParentId =0, DocumentName = "Mechanical", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 13 , ParentId =0, DocumentName = "Requirements Documents", ParentName = "Engineering" },
                    new EngineeringDocsModel {SNO = 14 , ParentId =0, DocumentName = "Archives", ParentName = "Certification" },
                    new EngineeringDocsModel {SNO = 15 , ParentId =0, DocumentName = "BOM", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 16 , ParentId =0, DocumentName = "Firmware", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 17 , ParentId =0, DocumentName = "Layout", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 18 , ParentId =0, DocumentName = "Schematic", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 19 , ParentId =0, DocumentName = "Test Documents", ParentName = "Electrical" },
                    new EngineeringDocsModel {SNO = 20 , ParentId =0, DocumentName = "Archives", ParentName = "BOM" },
                    new EngineeringDocsModel {SNO = 21 , ParentId =0, DocumentName = "Archives", ParentName = "Firmware" },
                    new EngineeringDocsModel {SNO = 22 , ParentId =0, DocumentName = "Archives", ParentName = "Layout" },
                    new EngineeringDocsModel {SNO = 23 , ParentId =0, DocumentName = "Archives", ParentName = "Schematic" },
                    new EngineeringDocsModel {SNO = 24 , ParentId =0, DocumentName = "Archives", ParentName = "Test Documents" },
                    new EngineeringDocsModel {SNO = 25 , ParentId =0, DocumentName = "2D Panel Installation Ref", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 26 , ParentId =0, DocumentName = "2D Technical Prints", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 27 , ParentId =0, DocumentName = "3D Tooling Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 28 , ParentId =0, DocumentName = "3D Viewable eDrawings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 29 , ParentId =0, DocumentName = "Decal Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 30 , ParentId =0, DocumentName = "Final Color Renderings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 31 , ParentId =0, DocumentName = "Illustration Exports", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 32 , ParentId =0, DocumentName = "Installation Drawings", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 33 , ParentId =0, DocumentName = "Outdated", ParentName = "Mechanical" },
                    new EngineeringDocsModel {SNO = 34 , ParentId =0, DocumentName = "Archives", ParentName = "Requirements Documents" }
                };
                EngineeringDocs engSubDocs = new EngineeringDocs();
                foreach (var doc in engDocList.OrderBy(o => o.SNO))
                {
                    engSubDocs = new EngineeringDocs
                    {
                        DocumentName = doc.DocumentName,
                        IsFolder = true,
                        ParentId = doc.ParentId,
                        PartId = partNumberMaster.Id,
                        IsDefault = true
                    };
                    context.EngineeringDocs.Add(engSubDocs);
                    context.SaveChanges();
                    engDocList.Where(w => w.ParentName == engSubDocs.DocumentName).ToList().ForEach(f => f.ParentId = engSubDocs.Id);
                }

                AWS.AWS aws = new AWS.AWS();
                aws.CreateFolderOns3(engDocs.PartId.ToString(), "");

                //Now Create the subFolders
                string sourcePath = new Misc().GetS3FolderName("EngineeringDocs") + "RootDocument";
                string destinationPath = new Misc().GetS3FolderName("EngineeringDocs") + GetParentFolderPath(engDocs.Id);
                aws.AddSubFoldersInRootOfEngDocs(sourcePath, destinationPath);

                response.ResponseCode = ((int)Enumerations.PartNumber.Success).ToString();
                response.ResponseMessage = Enumerations.PartNumber.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                response.ResponseMessage = Enumerations.PartNumber.Exception.GetStringValue();
                return response;
            }
            return response;
        }

        public List<PartNumberModel> GetPartNumberListForAssemblyKit()
        {
            var context = new GuardianAvionicsEntities();
            List<int> idList = context.EngineeringDocs.Where(w => w.DocumentName == "Image").Select(s => s.Id).ToList();
            var partWithImageNameList = context.EngineeringDocs.Where(w => idList.Contains(w.ParentId) && !w.IsFolder).GroupBy(g => g.ParentId).Where(w => w.FirstOrDefault() != null).Select(s => new { partId = s.FirstOrDefault().PartId, ImageName = s.FirstOrDefault().DocumentName }).ToList();

            List<PartNumberModel> partNoList = context.PartNumberMasters.Where(w => w.IsInActive != true).OrderByDescending(o => o.Id).Select(s => new PartNumberModel
            {
                Id = s.Id,
                PartNumber = s.PartNumber,
                Description = s.Description,
                CostPrice = s.CostPrice ?? 0,
                SalesPrice = s.SalesPrice ?? 0,
            }).ToList();
            partNoList.Join(partWithImageNameList, (allParts) => allParts.Id, (imageNameList) => imageNameList.partId, (allParts, imageNameList) =>
            {
                allParts.ImageName = imageNameList.ImageName;
                return allParts;
            }).ToList();

            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;

            Misc misc = new Misc();
            int SerialNo = partNoList.Count;
            partNoList.ForEach(f =>
            {
                f.ImageName = string.IsNullOrEmpty(f.ImageName) ? "../Images/NoImage.png" : path + f.Id.ToString() + "/Image/" + f.ImageName;
            });
            return partNoList;
        }

        public List<UnitModel> GetSerialNoWithModel(string value)
        {
            var context = new GuardianAvionicsEntities();
            var serialNoList = context.UnitSerialNumbers.Where(w => w.SerialNumber.Value.ToString().Contains(value) && w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold).ToList();
            return serialNoList.Select(s => new UnitModel
            {
                SerialNumber = s.SerialNumber,
                ModelNo = string.IsNullOrEmpty(s.PurchaseOrder.PartNumberMaster.ModelNumber) ? "" : s.PurchaseOrder.PartNumberMaster.ModelNumber,
                Description = string.IsNullOrEmpty(s.PurchaseOrder.PartNumberMaster.Description) ? "" : s.PurchaseOrder.PartNumberMaster.Description,
            }).Take(10).ToList();
        }


        public PartNumberModel GetPartById(int partId)
        {
            var context = new GuardianAvionicsEntities();
            var listWithAvailableCount = context.UnitSerialNumbers.GroupBy(g => g.PurchaseOrder.PartNumberMaster.Id).Where(w => w.Key.ToString() != "" && w.Key == partId).Select(s => new { Id = s.Key, UnitCount = s.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Count() }).ToList();

            int id = context.EngineeringDocs.FirstOrDefault(w => w.DocumentName == "Image" && w.PartId == partId).Id;
            string documentName = "";
            var engineeringDocs = context.EngineeringDocs.FirstOrDefault(w => w.ParentId == id && !w.IsFolder);

            PartNumberModel partModel = new PartNumberModel();
            PartNumberMaster partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.Id == partId);

            partModel.Id = partNumberMaster.Id;
            partModel.PartNumber = partNumberMaster.PartNumber;
            partModel.ModelNumber = string.IsNullOrEmpty(partNumberMaster.ModelNumber) ? "" : partNumberMaster.ModelNumber;
            partModel.Description = string.IsNullOrEmpty(partNumberMaster.Description) ? "" : partNumberMaster.Description;
            partModel.IsSoftwareLevelApplicable = partNumberMaster.IsSoftwareLevelApplicable ?? false;
            partModel.CostPrice = partNumberMaster.CostPrice ?? 0;
            partModel.SalesPrice = partNumberMaster.SalesPrice ?? 0;
            partModel.RecommendQuantity = partNumberMaster.RecommendQty ?? 0;
            partModel.Comment = string.IsNullOrEmpty(partNumberMaster.Comment) ? "" : partNumberMaster.Comment;
            partModel.VendorName = partNumberMaster.Vendor == null ? "" : partNumberMaster.Vendor.Name;
            partModel.VendorId = partNumberMaster.DefaultVendorId == null ? null : partNumberMaster.DefaultVendorId.ToString();
            partModel.IsSerialNumberRequired = partNumberMaster.IsSerialNumberRequired ?? true;
            partModel.TotalUnitsAvailableForSale = 0;
            partModel.MasterPartId = partNumberMaster.MasterPartId;
            partModel.IsAliasCreated = partNumberMaster.IsAliasCreated ?? false;
            partModel.AliasList = partNumberMaster.IsAliasCreated == true ? context.PartNumberMasters.Where(w => w.MasterPartId == partId && w.Id != partId).Select(se => se.PartNumber).ToList() : new List<string>();
            partModel.IsMasterPart = partNumberMaster.Id == (partNumberMaster.MasterPartId ?? 0);
            partModel.MasterPartNumber = (partNumberMaster.Id == (partNumberMaster.MasterPartId ?? 0)) ? "" : context.PartNumberMasters.FirstOrDefault(f => f.Id == partNumberMaster.MasterPartId).PartNumber;
            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;
            partModel.TotalUnitsAvailableForSale = listWithAvailableCount.Count();
            partModel.ImageName = string.IsNullOrEmpty(documentName) ? "" : path + partNumberMaster.Id.ToString() + "/Image/" + documentName;
            partModel.IsInActive = partNumberMaster.IsInActive ?? false;
            Misc misc = new Misc();
            partModel.PartNumberId = misc.Encrypt(partModel.Id.ToString());
            partModel.Id = 0;
            partModel.VendorId = (partModel.VendorId != null) ? misc.Encrypt(partModel.VendorId) : "";
            partModel.CanSold = partNumberMaster.CanSold ?? true;
            return partModel;
        }

        public List<PartNumberModel> GetPartsAndAliasByPartNumber(string partNo)
        {
            var context = new GuardianAvionicsEntities();
            var partNoMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNo);
            if (partNoMaster == null)
            {
                return new List<PartNumberModel>();
            }


            var listWithAvailableCount = context.UnitSerialNumbers.GroupBy(g => g.PurchaseOrder.PartNumberMaster.Id).Where(w => w.Key.ToString() != "").Select(s => new { Id = s.Key, UnitCount = s.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Count() }).ToList();

            List<int> idList = context.EngineeringDocs.Where(w => w.DocumentName == "Image").Select(s => s.Id).ToList();
            var partWithImageNameList = context.EngineeringDocs.Where(w => idList.Contains(w.ParentId) && !w.IsFolder).GroupBy(g => g.ParentId).Where(w => w.FirstOrDefault() != null).Select(s => new { partId = s.FirstOrDefault().PartId, ImageName = s.FirstOrDefault().DocumentName }).ToList();

            List<PartNumberModel> partNoList = context.PartNumberMasters.Where(w => w.MasterPartId == partNoMaster.MasterPartId).OrderByDescending(o => o.IsAliasCreated).Select(s => new PartNumberModel
            {
                Id = s.Id,
                PartNumber = s.PartNumber,
                ModelNumber = string.IsNullOrEmpty(s.ModelNumber) ? "" : s.ModelNumber,
                Description = string.IsNullOrEmpty(s.Description) ? "" : s.Description,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable ?? false,
                CostPrice = s.CostPrice ?? 0,
                SalesPrice = s.SalesPrice ?? 0,
                RecommendQuantity = s.RecommendQty ?? 0,
                Comment = string.IsNullOrEmpty(s.Comment) ? "" : s.Comment,
                VendorName = s.Vendor == null ? "" : s.Vendor.Name,
                VendorId = s.DefaultVendorId == null ? null : s.DefaultVendorId.ToString(),
                IsSerialNumberRequired = s.IsSerialNumberRequired ?? true,
                TotalUnitsAvailableForSale = 0,
                MasterPartId = s.MasterPartId,
                IsAliasCreated = s.IsAliasCreated ?? false,
                // AliasList = s.IsAliasCreated == true ? context.PartNumberMasters.Where(w=>w.MasterPartId == s.Id).Select(se=> se.PartNumber).ToList() : new List<string>(),
                IsMasterPart = s.Id == (s.MasterPartId ?? 0),
                MasterPartNumber = (s.Id == (s.MasterPartId ?? 0)) ? "" : context.PartNumberMasters.FirstOrDefault(f => f.Id == s.MasterPartId).PartNumber
            }).ToList();

            partNoList.Join(listWithAvailableCount, (allParts) => allParts.Id, (partCountList) => partCountList.Id, (allParts, partCountList) =>
            {
                allParts.TotalUnitsAvailableForSale = partCountList.UnitCount;
                return allParts;
            }).ToList();


            partNoList.Join(partWithImageNameList, (allParts) => allParts.Id, (imageNameList) => imageNameList.partId, (allParts, imageNameList) =>
            {
                allParts.ImageName = imageNameList.ImageName;
                return allParts;
            }).ToList();

            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;

            Misc misc = new Misc();
            int SerialNo = partNoList.Count;
            partNoList.ForEach(f =>
            {
                f.AliasList = f.IsAliasCreated == true ? context.PartNumberMasters.Where(w => w.MasterPartId == f.Id && w.Id != f.Id).Select(se => se.PartNumber).ToList() : new List<string>();
                f.PartNumberId = misc.Encrypt(f.Id.ToString());
                f.ImageName = string.IsNullOrEmpty(f.ImageName) ? "" : path + f.Id.ToString() + "/Image/" + f.ImageName;
                f.SNO = SerialNo--;
                f.Id = 0;
                f.VendorId = (f.VendorId != null) ? misc.Encrypt(f.VendorId) : "";
            });
            return partNoList;
        }

        public List<PartNumberModel> GetAllMasterPartNo()
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.Where(w => w.IsAliasCreated == true).Select(s => new PartNumberModel { PartNumber = s.PartNumber }).ToList();
        }

        public List<PartNumberModel> GetNonSalablePartNumber()
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.Where(w => w.CanSold == false && (w.IsInActive ?? false) != true).Select(s => new PartNumberModel { PartNumber = s.PartNumber }).ToList();
        }

        public List<SelectListItem> GetPartNameAndDescription()
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.Where(w => w.IsInActive == null || w.IsInActive != true).Select(s => new SelectListItem
            {
                Text = s.PartNumber,
                Value = string.IsNullOrEmpty(s.Description) ? " " : s.Description
            }).ToList();
        }

        public List<SelectListItem> GetPartNameAndModelNumber()
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.Where(w => w.IsInActive == null || w.IsInActive != true).Select(s => new SelectListItem
            {
                Text = s.PartNumber,
                Value = string.IsNullOrEmpty(s.ModelNumber) ? " " : s.ModelNumber
            }).ToList();
        }

        public List<PartNumberModel> GetPartNumberList()
        {
            var context = new GuardianAvionicsEntities();

            var listWithAvailableCount = context.UnitSerialNumbers.GroupBy(g => g.PurchaseOrder.PartNumberMaster.Id).Where(w => w.Key.ToString() != "").Select(s => new { Id = s.Key, UnitCount = s.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Count() }).ToList();

            List<int> idList = context.EngineeringDocs.Where(w => w.DocumentName == "Image").Select(s => s.Id).ToList();
            var partWithImageNameList = context.EngineeringDocs.Where(w => idList.Contains(w.ParentId) && !w.IsFolder).GroupBy(g => g.ParentId).Where(w => w.FirstOrDefault() != null).Select(s => new { partId = s.FirstOrDefault().PartId, ImageName = s.FirstOrDefault().DocumentName }).ToList();

            List<PartNumberModel> partNoList = context.PartNumberMasters.OrderByDescending(o => o.Id).Select(s => new PartNumberModel
            {
                Id = s.Id,
                PartNumber = s.PartNumber,
                ModelNumber = string.IsNullOrEmpty(s.ModelNumber) ? "" : s.ModelNumber,
                Description = string.IsNullOrEmpty(s.Description) ? "" : s.Description,
                IsSoftwareLevelApplicable = s.IsSoftwareLevelApplicable ?? false,
                CostPrice = s.CostPrice ?? 0,
                SalesPrice = s.SalesPrice ?? 0,
                RecommendQuantity = s.RecommendQty ?? 0,
                Comment = string.IsNullOrEmpty(s.Comment) ? "" : s.Comment,
                VendorName = s.Vendor == null ? "" : s.Vendor.Name,
                VendorId = s.DefaultVendorId == null ? null : s.DefaultVendorId.ToString(),
                IsSerialNumberRequired = s.IsSerialNumberRequired ?? true,
                TotalUnitsAvailableForSale = 0,
                MasterPartId = s.MasterPartId,
                IsAliasCreated = s.IsAliasCreated ?? false,
                // AliasList = s.IsAliasCreated == true ? context.PartNumberMasters.Where(w=>w.MasterPartId == s.Id).Select(se=> se.PartNumber).ToList() : new List<string>(),
                IsMasterPart = s.Id == (s.MasterPartId ?? 0),
                MasterPartNumber = (s.Id == (s.MasterPartId ?? 0)) ? "" : context.PartNumberMasters.FirstOrDefault(f => f.Id == s.MasterPartId).PartNumber
            }).ToList();


            partNoList.Join(listWithAvailableCount, (allParts) => allParts.Id, (partCountList) => partCountList.Id, (allParts, partCountList) =>
            {
                allParts.TotalUnitsAvailableForSale = partCountList.UnitCount;
                return allParts;
            }).ToList();


            partNoList.Join(partWithImageNameList, (allParts) => allParts.Id, (imageNameList) => imageNameList.partId, (allParts, imageNameList) =>
            {
                allParts.ImageName = imageNameList.ImageName;
                return allParts;
            }).ToList();

            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;

            Misc misc = new Misc();
            int SerialNo = partNoList.Count;
            partNoList.ForEach(f =>
            {
                f.AliasList = f.IsAliasCreated == true ? context.PartNumberMasters.Where(w => w.MasterPartId == f.Id && w.Id != f.Id).Select(se => se.PartNumber).ToList() : new List<string>();
                f.PartNumberId = misc.Encrypt(f.Id.ToString());
                f.ImageName = string.IsNullOrEmpty(f.ImageName) ? "" : path + f.Id.ToString() + "/Image/" + f.ImageName;
                f.SNO = SerialNo--;
                f.Id = 0;
                f.VendorId = (f.VendorId != null) ? misc.Encrypt(f.VendorId) : "";
            });
            return partNoList;
        }

        public PartNumberMaster GetPartDetailsbyId(int partId)
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.FirstOrDefault(f => f.Id == partId);
        }

        public PartNumberMaster GetPartDetailsbyPartNo(string partNo)
        {
            var context = new GuardianAvionicsEntities();
            return context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNo);
        }



        public int GetMaxBoardNumber()
        {
            int maxBoardNo = 0;
            var context = new GuardianAvionicsEntities();
            maxBoardNo = context.UnitSerialNumbers.Max(m => (int?)m.BoardSerialNumber) ?? 0;
            return ++maxBoardNo;
        }

        public GeneralResponse ValidatePurchaseOrder(int startSerialNumber, int quantity, int? boardSerialNo, string PONumber, string partNo)
        {
            var context = new GuardianAvionicsEntities();
            //var purchaseOrder = context.PurchaseOrders.FirstOrDefault(f => f.PurchaseOrderNumber == PONumber);
            //if (purchaseOrder != null)
            //{
            //    return new GeneralResponse
            //    {
            //        ResponseCode = ((int)Enumerations.PurchaseOrder.PONumberAlreadyExist).ToString(),
            //        ResponseMessage = string.Format(Enumerations.PurchaseOrder.PONumberAlreadyExist.GetStringValue(), startSerialNumber, (startSerialNumber + quantity - 1))
            //    };
            //}

            var partNoModel = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNo);
            if (partNoModel == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.PartNumber.PartNoNotExist).ToString(),
                    ResponseMessage = string.Format(Enumerations.PartNumber.PartNoNotExist.GetStringValue())
                };
            }

            if (partNoModel.IsSerialNumberRequired ?? true)
            {
                var unitList = context.UnitSerialNumbers.Where(w => w.SerialNumber >= startSerialNumber && w.SerialNumber <= (startSerialNumber + quantity - 1)).ToList();
                if (unitList.Count != 0)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Unit.SerialNoAlreadyExistWithinRange).ToString(),
                        ResponseMessage = string.Format(Enumerations.Unit.SerialNoAlreadyExistWithinRange.GetStringValue(), startSerialNumber, (startSerialNumber + quantity - 1))
                    };
                }
                if (boardSerialNo != null && boardSerialNo != 0)
                {
                    unitList = context.UnitSerialNumbers.Where(w => w.BoardSerialNumber >= boardSerialNo && w.BoardSerialNumber <= (boardSerialNo + quantity - 1)).ToList();
                    if (unitList.Count != 0)
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Unit.BoardSerialNoAlreadyExistWithinRange).ToString(),
                            ResponseMessage = string.Format(Enumerations.Unit.BoardSerialNoAlreadyExistWithinRange.GetStringValue(), boardSerialNo, (boardSerialNo + quantity - 1))
                        };
                    }
                }
            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.PurchaseOrder.Success).ToString(),
                ResponseMessage = Enumerations.PurchaseOrder.Success.GetStringValue()
            };
        }

        public GeneralResponse AddPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();

                PurchaseOrder purchaseOrder = context.PurchaseOrders.Create();
                purchaseOrder.PurchaseOrderNumber = purchaseOrderModel.PurchaseOrderNumber;

                var partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == purchaseOrderModel.PartNo);
                purchaseOrder.PartNumberId = partNumberMaster.Id;
                purchaseOrder.VendorId = string.IsNullOrEmpty(purchaseOrderModel.VendorId) ? (int?)null : Convert.ToInt32(purchaseOrderModel.VendorId);
                purchaseOrder.InventoryReceiverId = string.IsNullOrEmpty(purchaseOrderModel.ReceiverId) ? (int?)null : Convert.ToInt32(purchaseOrderModel.ReceiverId);
                purchaseOrder.Quantity = purchaseOrderModel.Quantity;
                if (partNumberMaster.IsSerialNumberRequired ?? true)
                {
                    purchaseOrder.StartSerialNo = purchaseOrderModel.StartSerialNo;
                    purchaseOrder.StartBoardSerialNo = purchaseOrderModel.StartingBoardSerialNo;
                }
                else
                {
                    purchaseOrder.StartSerialNo = 0;
                    purchaseOrder.StartBoardSerialNo = 0;
                }
                purchaseOrder.CreatedBy = purchaseOrderModel.CreatedBy;
                purchaseOrder.CreateDate = DateTime.UtcNow;
                purchaseOrder.SoftwareLevel = purchaseOrderModel.SoftwareLevel;
                context.PurchaseOrders.Add(purchaseOrder);
                context.SaveChanges();
                int unitStatus = (int)Enumerations.UnitStatus.AvailableForSale;
                bool isSoftwareLevelApplicable = true;
                if (!(purchaseOrder.PartNumberMaster.IsSoftwareLevelApplicable ?? false))
                {
                    isSoftwareLevelApplicable = false;
                }
                List<UnitSerialNumber> unitSerialNumberList = new List<UnitSerialNumber>();
                int serialNo = purchaseOrderModel.StartSerialNo;
                UnitSerialNumber unitSerialNumber;

                int maxSerialNumber = GetMaxSerialNumber();
                bool isSerialNoInSync = true;
                if ((maxSerialNumber + 1) != purchaseOrderModel.StartSerialNo)
                {
                    isSerialNoInSync = false;
                }


                for (int i = 0; i < purchaseOrderModel.Quantity; i++)
                {
                    unitSerialNumber = new UnitSerialNumber
                    {
                        PurchaseOrderId = purchaseOrder.Id,
                        SerialNumber = (partNumberMaster.IsSerialNumberRequired ?? true) ? serialNo : 0,
                        UnitCurrentStatusId = unitStatus,
                        BoardSerialNumber = (partNumberMaster.IsSerialNumberRequired ?? true) ? (isSoftwareLevelApplicable ? (purchaseOrderModel.IsEditBoardSerialNoAtReceive ? purchaseOrderModel.BoardNoList[i].BoardSerialNo : purchaseOrderModel.StartingBoardSerialNo) : null) : null,
                        IsSerialNoInSync = isSerialNoInSync,
                        PartNumberId = partNumberMaster.Id,//2019/08/05
                        SoftwareLevel = isSoftwareLevelApplicable ? (purchaseOrderModel.IsEditBoardSerialNoAtReceive ? purchaseOrderModel.BoardNoList[i].SWLevel : purchaseOrderModel.SoftwareLevel) : null,
                    };
                    serialNo++;
                    if (isSoftwareLevelApplicable)
                    {
                        purchaseOrderModel.StartingBoardSerialNo++;
                    }
                    if (unitSerialNumber.PurchaseOrderId == null)
                    {
                        throw new Exception();
                    }
                    unitSerialNumberList.Add(unitSerialNumber);
                }
                context.UnitSerialNumbers.AddRange(unitSerialNumberList);
                context.SaveChanges();

                response.ResponseCode = ((int)Enumerations.PurchaseOrder.Success).ToString();
                response.ResponseMessage = Enumerations.PurchaseOrder.Success.GetStringValue();

            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PurchaseOrder.Exception).ToString();
                response.ResponseMessage = Enumerations.PurchaseOrder.Exception.GetStringValue();
            }
            return response;
        }

        public GeneralResponse EditPurchaseOrder(PurchaseOrderModel purchaseOrderModel)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                int purchaseOrderId = Convert.ToInt32(purchaseOrderModel.PurchaseOrderId);
                PurchaseOrder purchaseOrder = context.PurchaseOrders.FirstOrDefault(f => f.Id == purchaseOrderId);
                if (purchaseOrder != null)
                {
                    var partNo = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == purchaseOrderModel.PartNo);
                    if (partNo == null)
                    {
                        response.ResponseCode = ((int)Enumerations.PartNumber.PartNoNotExist).ToString();
                        response.ResponseMessage = Enumerations.PartNumber.PartNoNotExist.GetStringValue();
                        return response;
                    }
                    if (purchaseOrderModel.PartNo != purchaseOrder.PartNumberMaster.PartNumber)
                    {
                        var unitList = context.UnitSerialNumbers.Where(w => w.PurchaseOrderId == purchaseOrderId && w.UnitCurrentStatusId != (int)Enumerations.UnitStatus.AvailableForSale).ToList();
                        if (unitList.Count == 0)
                        {
                            int startSerialNo = purchaseOrder.StartSerialNo;
                            int endSerialNo = startSerialNo + (purchaseOrder.Quantity - 1);
                            int partNumberId = Convert.ToInt32(purchaseOrderModel.PartNumberId);
                            purchaseOrder.PartNumberId = partNo.Id;
                            purchaseOrder.VendorId = Convert.ToInt32(purchaseOrderModel.VendorId);
                            purchaseOrder.InventoryReceiverId = Convert.ToInt32(purchaseOrderModel.ReceiverId);
                            purchaseOrder.PurchaseOrderNumber = purchaseOrderModel.PurchaseOrderNumber;
                            context.SaveChanges();
                            response.ResponseCode = ((int)Enumerations.PurchaseOrder.Success).ToString();
                            response.ResponseMessage = Enumerations.PurchaseOrder.Success.GetStringValue();
                            for (int i = 0; i < unitList.Count; i++)
                            {
                                unitList[i].PartNumberId = partNo.Id;
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            response.ResponseCode = ((int)Enumerations.PurchaseOrder.CannotEditPartNo).ToString();
                            response.ResponseMessage = Enumerations.PurchaseOrder.CannotEditPartNo.GetStringValue();
                        }
                    }
                    else
                    {
                        purchaseOrder.VendorId = Convert.ToInt32(purchaseOrderModel.VendorId);
                        purchaseOrder.InventoryReceiverId = Convert.ToInt32(purchaseOrderModel.ReceiverId);
                        purchaseOrder.PurchaseOrderNumber = purchaseOrderModel.PurchaseOrderNumber;
                        context.SaveChanges();
                        response.ResponseCode = ((int)Enumerations.PurchaseOrder.Success).ToString();
                        response.ResponseMessage = Enumerations.PurchaseOrder.Success.GetStringValue();
                    }
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.PurchaseOrder.InvalidRequest).ToString();
                    response.ResponseMessage = Enumerations.PurchaseOrder.InvalidRequest.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return response;
        }

        public GeneralResponse DeletePurchaseOrder(int id)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            try
            {
                PurchaseOrder purchaseOrder = context.PurchaseOrders.FirstOrDefault(f => f.Id == id);
                if (purchaseOrder != null)
                {
                    var unitList = purchaseOrder.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold).ToList();
                    if (unitList.Count == 0)
                    {
                        unitList = purchaseOrder.UnitSerialNumbers.ToList();
                        int[] arrIds = unitList.Select(s => s.Id).ToArray();
                        context.UnitStatusHistories.RemoveRange(context.UnitStatusHistories.Where(w => arrIds.Contains(w.UnitSerialNumberId)));
                        context.SaveChanges();
                        context.UnitSerialNumbers.RemoveRange(unitList);
                        context.SaveChanges();
                        context.PurchaseOrders.Remove(purchaseOrder);
                        context.SaveChanges();
                        response.ResponseCode = ((int)Enumerations.PurchaseOrder.Success).ToString();
                        response.ResponseMessage = Enumerations.PurchaseOrder.Success.GetStringValue();
                    }
                    else
                    {
                        response.ResponseCode = ((int)Enumerations.PurchaseOrder.CannotDeletePurchaseOrder).ToString();
                        response.ResponseMessage = Enumerations.PurchaseOrder.CannotDeletePurchaseOrder.GetStringValue();
                    }
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.PurchaseOrder.InvalidRequestToDelete).ToString();
                    response.ResponseMessage = Enumerations.PurchaseOrder.InvalidRequestToDelete.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.PurchaseOrder.Exception).ToString();
                response.ResponseMessage = Enumerations.PurchaseOrder.Exception.GetStringValue();
            }
            return response;
        }

        public GeneralResponse UpdatePartNo(PartNumberModel partNumberModel)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            try
            {
                PartNumberMaster partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNumberModel.PartNumber && f.Id != partNumberModel.Id);
                if (partNumberMaster != null)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.PartNoAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.PartNumber.PartNoAlreadyExist.GetStringValue();
                    return response;
                }
                partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.Id == partNumberModel.Id);
                if (partNumberMaster != null)
                {
                    if ((partNumberMaster.CanSold ?? true) != partNumberModel.CanSold)
                    {
                        if (partNumberModel.CanSold)
                        {
                            if (context.InventoryReduction.Where(w => w.PartId == partNumberModel.Id).Count() > 0)
                            {
                                response.ResponseCode = ((int)Enumerations.PartNumber.CannotChangeCanSoldStatus).ToString();
                                response.ResponseMessage = string.Format(Enumerations.PartNumber.CannotChangeCanSoldStatus.GetStringValue(), "UnSalable", "Salable", "inventory reduction");
                                return response;
                            }
                        }
                        else
                        {
                            if (context.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold && w.PurchaseOrder.PartNumberMaster.Id == partNumberModel.Id).Count() > 0)
                            {
                                response.ResponseCode = ((int)Enumerations.PartNumber.CannotChangeCanSoldStatus).ToString();
                                response.ResponseMessage = string.Format(Enumerations.PartNumber.CannotChangeCanSoldStatus.GetStringValue(), "Salable", "UnSalable", "invoice");
                                return response;
                            }
                        }
                    }

                    if (partNumberModel.IsAliasCreated)
                    {
                        partNumberMaster.IsAliasCreated = true;
                        if (partNumberModel.Alias != null)
                        {
                            response = LinkPartsToMaster(partNumberModel.Alias.Split(',').ToList(), partNumberModel.Id);
                            if (response.ResponseCode != "0")
                            {
                                return response;
                            }
                        }
                    }
                    else
                    {
                        if (partNumberMaster.IsAliasCreated == true)
                        {
                            context.PartNumberMasters.Where(w => w.MasterPartId == partNumberModel.Id && w.Id != partNumberModel.Id).ToList().ForEach(f => f.MasterPartId = f.Id);
                            partNumberMaster.IsAliasCreated = false;
                            context.SaveChanges();
                        }
                    }
                    string oldPartNumber = partNumberMaster.PartNumber;
                    partNumberMaster.PartNumber = partNumberModel.PartNumber;
                    partNumberMaster.Description = partNumberModel.Description;
                    partNumberMaster.IsSoftwareLevelApplicable = partNumberModel.IsSoftwareLevelApplicable;
                    partNumberMaster.CostPrice = partNumberModel.CostPrice;
                    partNumberMaster.RecommendQty = partNumberModel.RecommendQuantity;
                    partNumberMaster.Comment = partNumberModel.Comment;
                    partNumberMaster.ModelNumber = partNumberModel.ModelNumber;
                    partNumberMaster.IsInActive = partNumberModel.IsInActive;
                    partNumberMaster.DefaultVendorId = string.IsNullOrEmpty(partNumberModel.VendorId) ? (int?)null : Convert.ToInt32(partNumberModel.VendorId);
                    partNumberMaster.CanSold = partNumberModel.CanSold;
                    if (partNumberMaster.CanSold ?? true)
                    {
                        partNumberMaster.IsSerialNumberRequired = partNumberModel.IsSerialNumberRequired;
                        partNumberMaster.SalesPrice = partNumberModel.SalesPrice;
                    }
                    context.SaveChanges();

                    if (oldPartNumber != partNumberModel.PartNumber)
                    {
                        string maxPartNo = "";
                        string index = "";
                        if (oldPartNumber.Contains("-"))
                        {
                            index = oldPartNumber.Split('-')[0] + "-";
                            var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex.ToString().StartsWith(index));
                            if (partIndex != null)
                            {
                                maxPartNo = GetMaxPartNumberByIndex(index);
                                partIndex.LastAddedPartNo = maxPartNo;
                                context.SaveChanges();
                            }
                        }
                        if (partNumberModel.PartNumber.Contains("-"))
                        {
                            maxPartNo = "";
                            index = partNumberModel.PartNumber.Split('-')[0] + "-";
                            var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex.ToString().StartsWith(index));
                            if (partIndex != null)
                            {
                                maxPartNo = GetMaxPartNumberByIndex(index);
                                partIndex.LastAddedPartNo = maxPartNo;
                                context.SaveChanges();
                            }
                        }
                    }
                    response.ResponseCode = ((int)Enumerations.PartNumber.Success).ToString();
                    response.ResponseMessage = Enumerations.PartNumber.Success.GetStringValue();
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                    response.ResponseMessage = "Invalid request for updating the part number.";
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return response;
        }

        public GeneralResponse DeletePartNo(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                PartNumberMaster partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.Id == id);
                if (partNumberMaster == null)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                    response.ResponseMessage = "Invalid request to delete the part number.";
                    return response;
                }
                if (partNumberMaster.PurchaseOrders.Count > 0)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                    response.ResponseMessage = "Cannot delete part number as purchase order is created for this part number.";
                    return response;
                }

                if (partNumberMaster.InventoryReduction.Count > 0)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                    response.ResponseMessage = "Cannot delete part number as inventory reduction record is created for this part number.";
                    return response;
                }

                var partList = context.PartNumberMasters.Where(w => w.MasterPartId == id && w.Id != id).ToList();
                if (partList.Count > 0)
                {
                    response.ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString();
                    response.ResponseMessage = "Cannot delete part number as some of the alias are linked with this part number.";
                    return response;
                }
                string partNo = partNumberMaster.PartNumber;
                context.EngineeringDocs.RemoveRange(context.EngineeringDocs.Where(w => w.PartId == partNumberMaster.Id).ToList());
                context.PartNumberMasters.Where(w => w.Id != partNumberMaster.Id && w.MasterPartId == partNumberMaster.Id).ToList().ForEach(f => f.MasterPartId = f.Id);
                context.PartNumberMasters.Remove(partNumberMaster);
                context.SaveChanges();

                if (partNo.Contains("-"))
                {
                    string index = partNo.Split('-')[0] + "-";
                    var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex.StartsWith(index));
                    if (partIndex != null)
                    {
                        partNo = GetMaxPartNumberByIndex(index);
                        partIndex.LastAddedPartNo = partNo;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.ResponseCode = ((int)Enumerations.PartNumber.Success).ToString();
            response.ResponseMessage = Enumerations.PartNumber.Success.GetStringValue();
            return response;
        }

        public int GetMaxSerialNumber()
        {
            int maxSerialNo = 0;
            var context = new GuardianAvionicsEntities();
            maxSerialNo = context.UnitSerialNumbers.Where(w => w.IsSerialNoInSync == true).Max(m => (int?)m.SerialNumber) ?? 1;
            return maxSerialNo;
        }

        public List<UnitModel> GetUnitsByMultipleKey(string purchaseOrderNumber, string serialNo, string boardSerialNo, int? statusId)
        {
            var context = new GuardianAvionicsEntities();
            List<UnitModel> unitList = new List<UnitModel>();
            Misc misc = new Misc();
            IQueryable<UnitSerialNumber> units;

            int unitSno = string.IsNullOrEmpty(serialNo) ? 0 : Convert.ToInt32(serialNo);
            int? boardsno = string.IsNullOrEmpty(boardSerialNo) ? 0 : Convert.ToInt32(boardSerialNo);
            units = context.UnitSerialNumbers;
            if (!string.IsNullOrEmpty(purchaseOrderNumber))
            {
                units = units.Where(w => w.PurchaseOrder.PurchaseOrderNumber == purchaseOrderNumber);
            }
            if (!string.IsNullOrEmpty(serialNo))
            {
                units = units.Where(w => w.SerialNumber == unitSno);
            }
            if (!string.IsNullOrEmpty(boardSerialNo))
            {
                units = units.Where(w => w.BoardSerialNumber == boardsno);
            }
            if (statusId != 0)
            {
                units = units.Where(w => w.UnitCurrentStatusId == statusId);
            }

            if (string.IsNullOrEmpty(purchaseOrderNumber) && string.IsNullOrEmpty(serialNo) && string.IsNullOrEmpty(boardSerialNo) && statusId == 0)
            {
                units = units.OrderByDescending(o => o.Id).Take(5);
                units = units.OrderBy(o => o.Id);
            }

            unitList = units.Select(s => new UnitModel
            {
                PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                NewPartNumberAssigned = string.IsNullOrEmpty(s.NewPartNumber) ? s.PurchaseOrder.PartNumberMaster.PartNumber : (s.PurchaseOrder.PartNumberMaster.PartNumber != s.NewPartNumber ? s.NewPartNumber : s.PurchaseOrder.PartNumberMaster.PartNumber),
                Description = s.PurchaseOrder.PartNumberMaster.Description,
                SerialNumber = s.SerialNumber,
                UnitStatus = s.UnitStatus.Status,
                SoftwareLevel = s.SoftwareLevel,
                UnitId = s.Id.ToString(),
                BoardSerialNumber = s.BoardSerialNumber,
                InvoiceNumber = s.MappingInvoiceAndUnit.Count() != 0 ? s.MappingInvoiceAndUnit.OrderByDescending(o => o.Id).FirstOrDefault(f => f.UnitId == s.Id).Invoice.InvoiceNumber : "",
                InvoiceDate = s.MappingInvoiceAndUnit.Count() != 0 ? s.MappingInvoiceAndUnit.OrderByDescending(o => o.Id).FirstOrDefault(f => f.UnitId == s.Id).Invoice.InvoiceDate : DateTime.Now,
            }).ToList();
            int sno = 1;
            unitList.ForEach(f => { f.UnitId = misc.Encrypt(f.UnitId); f.SNO = sno++; });
            return unitList;
        }

        public List<UnitModel> GetUnits(string key, string value, int? statusId)
        {
            var context = new GuardianAvionicsEntities();

            List<UnitModel> unitList = new List<UnitModel>();
            Misc misc = new Misc();
            int serialNo = 1;
            IQueryable<UnitSerialNumber> units;
            units = context.UnitSerialNumbers;

            if (string.IsNullOrEmpty(key))
            {
                unitList = units.OrderByDescending(o => o.Id).Take(5).Select(s => new UnitModel
                {
                    PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                    PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                    NewPartNumberAssigned = string.IsNullOrEmpty(s.NewPartNumber) ? s.PurchaseOrder.PartNumberMaster.PartNumber : (s.PurchaseOrder.PartNumberMaster.PartNumber != s.NewPartNumber ? s.NewPartNumber : s.PurchaseOrder.PartNumberMaster.PartNumber),
                    Description = s.PurchaseOrder.PartNumberMaster.Description,
                    SerialNumber = s.SerialNumber,
                    UnitStatus = s.UnitStatus.Status,
                    SoftwareLevel = s.SoftwareLevel,
                    UnitId = s.Id.ToString(),
                    BoardSerialNumber = s.BoardSerialNumber,
                    InvoiceNumber = s.InvoiceId == null ? "" : s.Invoice.InvoiceNumber,
                    InvoiceDate = s.InvoiceId == null ? DateTime.Now : s.Invoice.InvoiceDate

                }).ToList();
                serialNo = 1;
                unitList.ForEach(f => { f.UnitId = misc.Encrypt(f.UnitId); f.SNO = serialNo++; });
                return unitList;
            }

            if (statusId != 0)
            {
                units = units.Where(w => w.UnitCurrentStatusId == statusId);
            }
            switch (key)
            {
                case "PurchaseOrderId":
                    {
                        int purchaseOrderId = Convert.ToInt32(new Misc().Decrypt(value));
                        unitList = units.Where(w => w.PurchaseOrderId == purchaseOrderId).Select(s => new UnitModel
                        {
                            PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                            PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                            NewPartNumberAssigned = string.IsNullOrEmpty(s.NewPartNumber) ? s.PurchaseOrder.PartNumberMaster.PartNumber : (s.PurchaseOrder.PartNumberMaster.PartNumber != s.NewPartNumber ? s.NewPartNumber : s.PurchaseOrder.PartNumberMaster.PartNumber),
                            Description = s.PurchaseOrder.PartNumberMaster.Description,
                            SerialNumber = s.SerialNumber,
                            UnitStatus = s.UnitStatus.Status,
                            SoftwareLevel = s.SoftwareLevel,
                            UnitId = s.Id.ToString(),
                            BoardSerialNumber = s.BoardSerialNumber,
                            InvoiceNumber = s.InvoiceId == null ? "" : s.Invoice.InvoiceNumber,
                            InvoiceDate = s.InvoiceId == null ? DateTime.Now : s.Invoice.InvoiceDate
                        }).ToList();
                        break;
                    }
                case "PurchaseOrderNumber":
                    {
                        unitList = units.Where(w => w.PurchaseOrder.PurchaseOrderNumber.Contains(value)).Select(s => new UnitModel
                        {
                            PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                            PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                            NewPartNumberAssigned = string.IsNullOrEmpty(s.NewPartNumber) ? s.PurchaseOrder.PartNumberMaster.PartNumber : (s.PurchaseOrder.PartNumberMaster.PartNumber != s.NewPartNumber ? s.NewPartNumber : s.PurchaseOrder.PartNumberMaster.PartNumber),
                            Description = s.PurchaseOrder.PartNumberMaster.Description,
                            SerialNumber = s.SerialNumber,
                            UnitStatus = s.UnitStatus.Status,
                            SoftwareLevel = s.SoftwareLevel,
                            UnitId = s.Id.ToString(),
                            BoardSerialNumber = s.BoardSerialNumber,
                            InvoiceNumber = s.InvoiceId == null ? "" : s.Invoice.InvoiceNumber,
                            InvoiceDate = s.InvoiceId == null ? DateTime.Now : s.Invoice.InvoiceDate
                        }).ToList();
                        break;
                    }
                case "PartNumber":
                    {
                        int partNoId = Convert.ToInt32(new Misc().Decrypt(value));
                        string status = Enumerations.UnitStatus.AvailableForSale.GetStringValue();
                        unitList = units.Where(w => w.PurchaseOrder.PartNumberMaster.Id == partNoId && w.UnitStatus.Status == status).Select(s => new UnitModel
                        {
                            PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                            PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                            NewPartNumberAssigned = string.IsNullOrEmpty(s.NewPartNumber) ? s.PurchaseOrder.PartNumberMaster.PartNumber : (s.PurchaseOrder.PartNumberMaster.PartNumber != s.NewPartNumber ? s.NewPartNumber : s.PurchaseOrder.PartNumberMaster.PartNumber),
                            Description = s.PurchaseOrder.PartNumberMaster.Description,
                            SerialNumber = s.SerialNumber,
                            UnitStatus = s.UnitStatus.Status,
                            SoftwareLevel = s.SoftwareLevel,
                            UnitId = s.Id.ToString(),
                            BoardSerialNumber = s.BoardSerialNumber,
                            InvoiceNumber = s.InvoiceId == null ? "" : s.Invoice.InvoiceNumber,
                            InvoiceDate = s.InvoiceId == null ? DateTime.Now : s.Invoice.InvoiceDate
                        }).ToList();
                        break;
                    }
            }
            unitList.ForEach(f => { f.UnitId = misc.Encrypt(f.UnitId); f.SNO = serialNo++; });
            return unitList;
        }

        public List<UnitModel> GetMultipleUnitsBySerialNo(List<Int32> arrSerialNo, out GeneralResponse response)
        {
            response = new GeneralResponse();
            response.ResponseCode = ((int)Enumerations.Invoice.Success).ToString();
            response.ResponseMessage = Enumerations.Invoice.Success.GetStringValue();
            var context = new GuardianAvionicsEntities();
            Misc misc = new Misc();
            List<UnitModel> unitModelList = new List<UnitModel>();
            try
            {
                var unitModelListTemp = context.UnitSerialNumbers.Where(f => arrSerialNo.Contains(f.SerialNumber ?? 0) && f.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold).ToList();
                if (unitModelListTemp.Count > 0)
                {
                    string serialNos = string.Join(",", unitModelListTemp.Select(s => s.SerialNumber).ToList());
                    response.ResponseCode = ((int)Enumerations.Invoice.SerialNumberAlreadySold).ToString();
                    response.ResponseMessage = string.Format(Enumerations.Invoice.SerialNumberAlreadySold.GetStringValue(), serialNos);
                    return new List<UnitModel>();
                }

                unitModelListTemp = context.UnitSerialNumbers.Where(f => arrSerialNo.Contains(f.SerialNumber ?? 0) && f.PurchaseOrder.PartNumberMaster.IsInActive == true).ToList();
                if (unitModelListTemp.Count > 0)
                {

                    string serialNos = string.Join(",", unitModelListTemp.Select(s => s.SerialNumber).ToList());
                    response.ResponseCode = ((int)Enumerations.Invoice.UnitBelongToInActivePartNo).ToString();
                    response.ResponseMessage = string.Format(Enumerations.Invoice.UnitBelongToInActivePartNo.GetStringValue(), serialNos);
                    return new List<UnitModel>();
                }

                unitModelList = context.UnitSerialNumbers.Where(f => arrSerialNo.Contains(f.SerialNumber ?? 0) && f.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale).Select(s => new UnitModel
                {
                    PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                    PurchaseOrderId = s.PurchaseOrderId.ToString(),
                    PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                    Description = s.PurchaseOrder.PartNumberMaster.Description,
                    Vendor = (s.PurchaseOrder.VendorId == null) ? "" : s.PurchaseOrder.Vendor.Name,
                    SerialNumber = s.SerialNumber,
                    StatusId = s.UnitCurrentStatusId,
                    UnitStatus = s.UnitStatus.Status,
                    SoftwareLevel = s.PurchaseOrder.SoftwareLevel,
                    Id = s.Id,
                    BoardSerialNumber = s.BoardSerialNumber,
                }).ToList();

            }
            catch (Exception ex)
            {
                throw;
            }
            return unitModelList;
        }

        public GeneralResponse LinkPartsToMaster(List<string> partNoList, int masterPartId)
        {
            var context = new GuardianAvionicsEntities();



            PartNumberMaster masterPart = context.PartNumberMasters.FirstOrDefault(f => f.Id == masterPartId);
            masterPart.IsAliasCreated = true;

            List<PartNumberMaster> partNumberList = context.PartNumberMasters.Where(w => partNoList.Contains(w.PartNumber)).ToList();
            if (partNumberList.Count() != partNoList.Count())
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Part numbers - " + string.Join(",", partNoList.Where(p => !partNumberList.Any(p2 => p2.PartNumber == p)).ToList()) + " not exist in our records"
                };
            }


            partNumberList = context.PartNumberMasters.Where(w => partNoList.Contains(w.PartNumber) && w.IsAliasCreated == true).ToList();
            if (partNumberList.Count > 0)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Cannot add the part numbers " + string.Join(",", partNumberList.Select(s => s.PartNumber).ToArray()) + " as these are the master part numbers."
                };
            }

            partNumberList = context.PartNumberMasters.Where(w => partNoList.Contains(w.PartNumber) && w.MasterPartId != w.Id && w.MasterPartId != masterPartId).ToList();
            if (partNumberList.Count > 0)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Part numbers " + string.Join(" ", partNumberList.Select(s => s.PartNumber).ToArray()) + " are already an alias of different part number."
                };
            }

            partNumberList = context.PartNumberMasters.Where(w => w.MasterPartId == masterPartId && w.Id != masterPartId).ToList();
            partNumberList.ForEach(f => f.MasterPartId = f.Id);
            context.SaveChanges();

            partNumberList = context.PartNumberMasters.Where(w => partNoList.Contains(w.PartNumber)).ToList();
            partNumberList.ForEach(f => f.MasterPartId = masterPart.Id);
            context.SaveChanges();

            //Here will update the master part number in the list
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }

        public List<UnitModel> GetMultipleUnits(List<Int32> arrUnitId)
        {
            var context = new GuardianAvionicsEntities();
            List<UnitModel> unitList = new List<UnitModel>();
            unitList = context.UnitSerialNumbers.Where(w => arrUnitId.Contains(w.Id)).Select(s => new UnitModel
            {
                PurchaseOrderNumber = s.PurchaseOrder.PurchaseOrderNumber,
                PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                Description = s.PurchaseOrder.PartNumberMaster.Description,
                SerialNumber = s.SerialNumber,
                UnitStatus = s.UnitStatus.Status,
                SoftwareLevel = s.PurchaseOrder.SoftwareLevel,
                UnitId = s.Id.ToString(),
                BoardSerialNumber = s.BoardSerialNumber
            }).ToList();
            int serialNo = 1;
            Misc misc = new Misc();
            unitList.ForEach(f => { f.UnitId = misc.Encrypt(f.UnitId); f.SNO = serialNo++; });
            return unitList;
        }

        public List<UnitModel> GetUnitsByInvoice(string invoiceNo, out GeneralResponse response)
        {
            var context = new GuardianAvionicsEntities();
            response = new GeneralResponse();
            var invoice = context.Invoice.FirstOrDefault(f => f.InvoiceNumber == invoiceNo);
            if (invoice == null)
            {
                response.ResponseCode = ((int)Enumerations.Invoice.InvoiceNoNotExist).ToString();
                response.ResponseMessage = Enumerations.Invoice.InvoiceNoNotExist.GetStringValue();
                return new List<UnitModel>();
            }

            response.ResponseCode = ((int)Enumerations.Invoice.Success).ToString();
            response.ResponseMessage = Enumerations.Invoice.Success.GetStringValue();
            return invoice.MappingInvoiceAndUnit.Select(s => new UnitModel
            {
                PurchaseOrderNumber = s.UnitSerialNumber.PurchaseOrder.PurchaseOrderNumber,
                PartNumber = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber,
                Description = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.Description,
                Vendor = (s.UnitSerialNumber.PurchaseOrder.VendorId == null) ? "" : s.UnitSerialNumber.PurchaseOrder.Vendor.Name,
                SerialNumber = s.UnitSerialNumber.SerialNumber,
                StatusId = s.UnitSerialNumber.UnitCurrentStatusId,
                UnitStatus = s.UnitSerialNumber.UnitStatus.Status,
                SoftwareLevel = s.UnitSerialNumber.PurchaseOrder.SoftwareLevel,
                Id = s.Id,
                BoardSerialNumber = s.UnitSerialNumber.BoardSerialNumber
            }).ToList();

        }


        public UnitModel GetSoldUnitBySerialNo(int serialNo)
        {
            var context = new GuardianAvionicsEntities();
            Misc misc = new Misc();
            UnitModel unitModel = new UnitModel();
            UnitSerialNumber unitSerialNumber = context.UnitSerialNumbers.FirstOrDefault(f => f.SerialNumber == serialNo);
            if (unitSerialNumber != null)
            {
                unitSerialNumber = context.UnitSerialNumbers.FirstOrDefault(f => f.SerialNumber == serialNo && f.PurchaseOrder.PartNumberMaster.CanSold != false);
                if (unitSerialNumber != null)
                {
                    if (unitSerialNumber.UnitStatus.Status == Enumerations.UnitStatus.Sold.ToString())
                    {
                        unitModel.PurchaseOrderNumber = unitSerialNumber.PurchaseOrder.PurchaseOrderNumber;
                        unitModel.PurchaseOrderId = unitSerialNumber.PurchaseOrderId.ToString();
                        unitModel.PartNumber = unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber;
                        unitModel.Description = unitSerialNumber.PurchaseOrder.PartNumberMaster.Description;
                        unitModel.Vendor = (unitSerialNumber.PurchaseOrder.VendorId == null) ? "" : unitSerialNumber.PurchaseOrder.Vendor.Name;
                        unitModel.SerialNumber = unitSerialNumber.SerialNumber;
                        unitModel.StatusId = unitSerialNumber.UnitCurrentStatusId;
                        unitModel.UnitStatus = unitSerialNumber.UnitStatus.Status;
                        unitModel.SoftwareLevel = unitSerialNumber.PurchaseOrder.SoftwareLevel;
                        unitModel.Id = unitSerialNumber.Id;
                        unitModel.BoardSerialNumber = unitSerialNumber.BoardSerialNumber;
                        unitModel.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                        unitModel.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
                    }
                    else
                    {
                        unitModel.ResponseCode = ((int)Enumerations.Unit.OnlySoldUnitsCanAddInRMA).ToString();
                        unitModel.ResponseMessage = Enumerations.Unit.OnlySoldUnitsCanAddInRMA.GetStringValue();
                    }
                }

                else
                {

                    unitModel.ResponseCode = ((int)Enumerations.Unit.CannotCreateRMAForNonSalableUnit).ToString();
                    unitModel.ResponseMessage = Enumerations.Unit.CannotCreateRMAForNonSalableUnit.GetStringValue();
                }
            }
            else if (context.AddonUnits.FirstOrDefault(f => f.SerialNumber == serialNo) != null)
            {
                unitModel.ResponseCode = ((int)Enumerations.Unit.UnitAddedManually).ToString();
                unitModel.ResponseMessage = Enumerations.Unit.UnitAddedManually.GetStringValue();
            }
            else
            {
                unitModel.ResponseCode = ((int)Enumerations.Unit.SerialNoNotExist).ToString();
                unitModel.ResponseMessage = Enumerations.Unit.SerialNoNotExist.GetStringValue();
                return unitModel;
            }
            return unitModel;
        }

        public UnitModel GetUnitBySerialNo(int serialNo)
        {
            var context = new GuardianAvionicsEntities();
            Misc misc = new Misc();
            UnitModel unitModel = new UnitModel();
            try
            {
                UnitSerialNumber unitSerialNumber = context.UnitSerialNumbers.FirstOrDefault(f => f.SerialNumber == serialNo && f.PurchaseOrder.PartNumberMaster.CanSold != false);
                if (unitSerialNumber != null)
                {
                    if (unitSerialNumber.UnitStatus.Status == Enumerations.UnitStatus.Sold.ToString())
                    {
                        unitModel.ResponseCode = ((int)Enumerations.Unit.UnitAlreadySold).ToString();
                        unitModel.ResponseMessage = string.Format(Enumerations.Unit.UnitAlreadySold.GetStringValue(), serialNo);
                        return unitModel;
                    }

                    if (unitSerialNumber.UnitStatus.Status != Enumerations.UnitStatus.AvailableForSale.GetStringValue())
                    {
                        unitModel.ResponseCode = ((int)Enumerations.Unit.UnitEligiblityForSale).ToString();
                        unitModel.ResponseMessage = string.Format(Enumerations.Unit.UnitEligiblityForSale.GetStringValue(), serialNo, unitSerialNumber.UnitStatus.Status);
                        return unitModel;
                    }

                    if (unitSerialNumber.PurchaseOrder.PartNumberMaster.IsInActive == true)
                    {
                        unitModel.ResponseCode = ((int)Enumerations.Unit.UnitBelongsToInActivePart).ToString();
                        unitModel.ResponseMessage = string.Format(Enumerations.Unit.UnitBelongsToInActivePart.GetStringValue(), serialNo);
                        return unitModel;
                    }

                    unitModel.PurchaseOrderNumber = unitSerialNumber.PurchaseOrder.PurchaseOrderNumber;
                    unitModel.PurchaseOrderId = unitSerialNumber.PurchaseOrderId.ToString();
                    unitModel.NewPartNumberAssigned = unitSerialNumber.NewPartNumber;
                    unitModel.PartNumber = unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber;
                    unitModel.Description = unitSerialNumber.PurchaseOrder.PartNumberMaster.Description;
                    unitModel.Vendor = (unitSerialNumber.PurchaseOrder.VendorId == null) ? "" : unitSerialNumber.PurchaseOrder.Vendor.Name;
                    unitModel.SerialNumber = unitSerialNumber.SerialNumber;
                    unitModel.StatusId = unitSerialNumber.UnitCurrentStatusId;
                    unitModel.UnitStatus = unitSerialNumber.UnitStatus.Status;
                    unitModel.SoftwareLevel = unitSerialNumber.PurchaseOrder.SoftwareLevel;
                    unitModel.Id = unitSerialNumber.Id;
                    unitModel.BoardSerialNumber = unitSerialNumber.BoardSerialNumber;
                    unitModel.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                    unitModel.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
                }
                else
                {
                    unitModel.ResponseCode = ((int)Enumerations.Unit.SerialNoNotExist).ToString();
                    unitModel.ResponseMessage = Enumerations.Unit.SerialNoNotExist.GetStringValue();
                    return unitModel;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return unitModel;
        }

        public UnitModel GetUnitById(int id)
        {
            var context = new GuardianAvionicsEntities();
            Misc misc = new Misc();
            UnitModel unitModel = new UnitModel();
            try
            {

                UnitSerialNumber unitSerialNumber = context.UnitSerialNumbers.FirstOrDefault(f => f.Id == id);
                if (unitSerialNumber != null)
                {

                    unitModel.PurchaseOrderNumber = unitSerialNumber.PurchaseOrder.PurchaseOrderNumber;
                    unitModel.PurchaseOrderId = unitSerialNumber.PurchaseOrderId.ToString();
                    unitModel.PartNumber = unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber;
                    unitModel.ModelNo = unitSerialNumber.PurchaseOrder.PartNumberMaster.ModelNumber;
                    unitModel.Description = unitSerialNumber.PurchaseOrder.PartNumberMaster.Description;
                    unitModel.Vendor = (unitSerialNumber.PurchaseOrder.VendorId == null) ? "" : unitSerialNumber.PurchaseOrder.Vendor.Name;
                    unitModel.SerialNumber = unitSerialNumber.SerialNumber;
                    unitModel.StatusId = unitSerialNumber.UnitCurrentStatusId;
                    unitModel.UnitStatus = unitSerialNumber.UnitStatus.Status;
                    unitModel.SoftwareLevel = unitSerialNumber.SoftwareLevel;
                    unitModel.IsSoftwareLevelApplicable = unitSerialNumber.PurchaseOrder.PartNumberMaster.IsSoftwareLevelApplicable ?? false;
                    unitModel.Id = unitSerialNumber.Id;
                    unitModel.BoardSerialNumber = unitSerialNumber.BoardSerialNumber;
                    unitModel.NewPartNumberAssigned = string.IsNullOrEmpty(unitSerialNumber.NewPartNumber) ? unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber : (unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber != unitSerialNumber.NewPartNumber ? unitSerialNumber.NewPartNumber : unitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber);
                    unitModel.NewModelNumberAssigned = string.IsNullOrEmpty(unitSerialNumber.NewModelNumber) ? unitSerialNumber.PurchaseOrder.PartNumberMaster.ModelNumber : (unitSerialNumber.PurchaseOrder.PartNumberMaster.ModelNumber != unitSerialNumber.NewModelNumber ? unitSerialNumber.NewModelNumber : unitSerialNumber.PurchaseOrder.PartNumberMaster.ModelNumber);
                    unitModel.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                    unitModel.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
                }
                else
                {
                    unitModel.ResponseCode = ((int)Enumerations.Unit.InvalidRequest).ToString();
                    unitModel.ResponseMessage = Enumerations.Unit.InvalidRequest.GetStringValue();
                    return unitModel;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return unitModel;
        }

        public List<SelectListItem> GetStatus()
        {
            var context = new GuardianAvionicsEntities();
            List<SelectListItem> statusList = context.UnitStatus.Select(s => new SelectListItem
            {
                Text = s.Status,
                Value = s.StatusId.ToString()
            }).ToList();
            return statusList;
        }

        public GeneralResponse UpdateUnit(UnitModel unitModel)
        {
            var context = new GuardianAvionicsEntities();
            var response = new GeneralResponse();
            UnitSerialNumber unitSerialNumber = context.UnitSerialNumbers.FirstOrDefault(f => f.Id == unitModel.Id);
            int partNumberId = unitSerialNumber.PurchaseOrder.PartNumberId;
            if (unitSerialNumber != null)
            {
                if (unitSerialNumber.SerialNumber != unitModel.SerialNumber)
                {
                    UnitSerialNumber unit = context.UnitSerialNumbers.FirstOrDefault(f => f.SerialNumber == unitModel.SerialNumber);

                    // its when we are using quantity not count of value
                 
                    var partnumber = context.PartNumberMasters.Where(x => x.PartNumber == unitModel.NewPartNumberAssigned).FirstOrDefault();
                    if (partnumber == null)
                    {
                        response.ResponseCode = ((int)Enumerations.Unit.PartNumbernotExist).ToString();
                        response.ResponseMessage = Enumerations.Unit.PartNumbernotExist.GetStringValue();
                        return response;
                    }
                    else
                    {
                        var POavail = context.PurchaseOrders.Where(x => x.PartNumberId == partnumber.Id).ToList();
                        if (POavail.Count == 0)
                        {
                            response.ResponseCode = ((int)Enumerations.Unit.PoNotExist).ToString();
                            response.ResponseMessage = Enumerations.Unit.PoNotExist.GetStringValue();
                        }
                        unitSerialNumber.PartNumberId = partnumber.Id;
                    }
                   

                    if (unit != null)
                    {
                        response.ResponseCode = ((int)Enumerations.Unit.SerialNumberAlreadyAssign).ToString();
                        response.ResponseMessage = Enumerations.Unit.SerialNumberAlreadyAssign.GetStringValue();
                        return response;
                    }

                    if (unitSerialNumber.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold && unitModel.StatusId == (int)Enumerations.UnitStatus.AvailableForSale)
                    {

                        context.SalesReturn.Add(new SalesReturn
                        {
                            InvoiceId = unitSerialNumber.InvoiceId ?? 0,
                            Reason = unitModel.ReasonForSalesReturn,
                            ReturnDate = DateTime.UtcNow,
                            UnitId = unitModel.Id
                        });
                        unitSerialNumber.InvoiceId = null;
                    }
                    if (unitModel.StatusId == (int)Enumerations.UnitStatus.AvailableForSale)
                    {
                        if (unitModel.PartNumber != unitModel.NewPartNumberAssigned)
                        {
                            unitSerialNumber.NewPartNumber = unitModel.NewPartNumberAssigned;
                            unitSerialNumber.NewModelNumber = unitModel.NewModelNumberAssigned;
                        }
                    }
                    unitSerialNumber.SerialNumber = unitModel.SerialNumber;
                    unitSerialNumber.UnitCurrentStatusId = unitModel.StatusId;
                    if (unitSerialNumber.PurchaseOrder.PartNumberMaster.IsSoftwareLevelApplicable == true)
                    {
                        unitSerialNumber.BoardSerialNumber = unitModel.BoardSerialNumber;
                        unitSerialNumber.SoftwareLevel = unitModel.SoftwareLevel;
                    }

                    if (unitModel.StatusId != unitSerialNumber.UnitCurrentStatusId)
                    {
                        UnitStatusHistory unitStatusHistory = new UnitStatusHistory
                        {
                            DateOfStatusChanged = DateTime.UtcNow,
                            StatusId = unitModel.StatusId,
                            UnitSerialNumberId = unitModel.Id
                        };
                        context.UnitStatusHistories.Add(unitStatusHistory);
                    }
                    context.SaveChanges();

                }
                else
                {
                    var partnumber = context.PartNumberMasters.Where(x => x.PartNumber == unitModel.NewPartNumberAssigned).FirstOrDefault();
                    if (partnumber == null)
                    {
                        response.ResponseCode = ((int)Enumerations.Unit.PartNumbernotExist).ToString();
                        response.ResponseMessage = Enumerations.Unit.PartNumbernotExist.GetStringValue();
                        return response;
                    }
                    else
                    {
                        unitSerialNumber.PartNumberId = partnumber.Id;
                    }
                    if (unitSerialNumber.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Sold && unitModel.StatusId == (int)Enumerations.UnitStatus.AvailableForSale)
                    {
                        context.SalesReturn.Add(new SalesReturn
                        {
                            InvoiceId = unitSerialNumber.InvoiceId ?? 0,
                            Reason = unitModel.ReasonForSalesReturn,
                            ReturnDate = DateTime.UtcNow,
                            UnitId = unitModel.Id
                        });
                        unitSerialNumber.InvoiceId = null;
                    }

                    if (unitModel.StatusId == (int)Enumerations.UnitStatus.AvailableForSale)
                    {
                        if (unitModel.PartNumber != unitModel.NewPartNumberAssigned)
                        {
                            unitSerialNumber.NewPartNumber = unitModel.NewPartNumberAssigned;
                            unitSerialNumber.NewModelNumber = unitModel.NewModelNumberAssigned;
                        }
                    }

                    if (unitSerialNumber.PurchaseOrder.PartNumberMaster.IsSoftwareLevelApplicable == true)
                    {
                        unitSerialNumber.BoardSerialNumber = unitModel.BoardSerialNumber;
                        unitSerialNumber.SoftwareLevel = unitModel.SoftwareLevel;
                    }

                    unitSerialNumber.UnitCurrentStatusId = unitModel.StatusId;
                    if (unitModel.StatusId != unitSerialNumber.UnitCurrentStatusId)
                    {
                        UnitStatusHistory unitStatusHistory = new UnitStatusHistory
                        {
                            DateOfStatusChanged = DateTime.UtcNow,
                            StatusId = unitModel.StatusId,
                            UnitSerialNumberId = unitModel.Id
                        };
                        context.UnitStatusHistories.Add(unitStatusHistory);
                    }
                    context.SaveChanges();
                }
                response.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                response.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
            }
            else
            {
                response.ResponseCode = ((int)Enumerations.Unit.InvalidRequest).ToString();
                response.ResponseMessage = Enumerations.Unit.InvalidRequest.GetStringValue();
            }
            return response;
        }

        public GeneralResponse UpdateMultipleUnits(List<Int32> idList, int statusId)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            try
            {
                var unitList = context.UnitSerialNumbers.Where(w => idList.Contains(w.Id)).ToList();
                unitList.ForEach(f => { f.UnitCurrentStatusId = statusId; });
                List<UnitStatusHistory> unitStatusHistory = new List<UnitStatusHistory>();
                foreach (var unit in unitList)
                {
                    unitStatusHistory.Add(new UnitStatusHistory { DateOfStatusChanged = DateTime.UtcNow, StatusId = statusId, UnitSerialNumberId = unit.Id });
                }
                context.UnitStatusHistories.AddRange(unitStatusHistory);
                context.SaveChanges();
                response.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                response.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public GeneralResponse UpdateInvoice(List<int> serialNoList, string invoiceNumber, DateTime invoiceDate, int customerId, string customerEmailId, int invoiceId, string CustomerName)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                Invoice invoice = context.Invoice.FirstOrDefault(f => f.Id == invoiceId);
                if (invoice == null)
                {
                    response.ResponseCode = ((int)Enumerations.Invoice.InvalidUpdateRequest).ToString();
                    response.ResponseMessage = Enumerations.Invoice.InvalidUpdateRequest.GetStringValue();
                    return response;
                }

                invoice = context.Invoice.FirstOrDefault(f => f.Id != invoiceId && f.InvoiceNumber == invoiceNumber);
                if (invoice != null)
                {
                    response.ResponseCode = ((int)Enumerations.Unit.InvoiceNumberAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.Unit.InvoiceNumberAlreadyExist.GetStringValue();
                    return response;
                }
                invoice = context.Invoice.FirstOrDefault(f => f.Id == invoiceId);
                List<UnitSerialNumber> invoiceUnitList = invoice.MappingInvoiceAndUnit.ToList().Select(s => s.UnitSerialNumber).ToList();

                //List<UnitSerialNumber> invoiceUnitList = context.UnitSerialNumbers.Where(w => w.InvoiceId == invoice.Id).ToList();
                List<int> unitIdList = invoiceUnitList.Select(s => s.Id).ToList();
                context.UnitStatusHistories.RemoveRange(context.UnitStatusHistories.Where(w => unitIdList.Contains(w.UnitSerialNumberId)).ToList());
                invoiceUnitList.ForEach(f => { f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.AvailableForSale; f.InvoiceId = null; });
                context.MappingInvoiceAndUnit.RemoveRange(context.MappingInvoiceAndUnit.Where(w => w.InvoiceId == invoice.Id));
                context.SaveChanges();




                List<UnitSerialNumber> unitList = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();

                invoice.CreateDate = DateTime.UtcNow;
                invoice.InvoiceDate = invoiceDate;
                invoice.InvoiceNumber = invoiceNumber;
                invoice.CustomerId = customerId;
                invoice.CustomerName = CustomerName;
                context.SaveChanges();

                var mapUnitAndInvoiceList = unitList.Select(s => new MappingInvoiceAndUnit
                {
                    InvoiceId = invoice.Id,
                    UnitId = s.Id
                }).ToList();
                context.MappingInvoiceAndUnit.AddRange(mapUnitAndInvoiceList);

                unitList.ForEach(f => { f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.Sold; });
                context.SaveChanges();
                List<UnitStatusHistory> unitStatusHistoryList = new List<UnitStatusHistory>();
                foreach (var unit in unitList)
                {
                    unitStatusHistoryList.Add(new UnitStatusHistory
                    {
                        DateOfStatusChanged = DateTime.UtcNow,
                        StatusId = (int)Enumerations.UnitStatus.Sold,
                        UnitSerialNumberId = unit.Id

                    });
                }
                context.UnitStatusHistories.AddRange(unitStatusHistoryList);
                context.SaveChanges();

                List<PartNumberMaster> partNumberMasterList = unitList.Select(s => s.PurchaseOrder.PartNumberMaster).Distinct().ToList();

                string zipPath = ConfigurationReader.PartZipFilePath;
                List<string> zipFilePaths = new List<string>();
                foreach (var part in partNumberMasterList)
                {
                    zipFilePaths.Add(zipPath + part.PartNumber + ".zip");
                }

                if (!string.IsNullOrEmpty(customerEmailId))
                {

                    Task.Factory.StartNew(() =>
                    {
                        CreateZipForPartDocs(partNumberMasterList);
                        SendInvoiceToCustomer(invoice, unitList, zipFilePaths, customerEmailId);
                    });
                }
                response.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                response.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }


        public GeneralResponse SaleMultipleUnit(List<int> serialNoList, string invoiceNumber, DateTime invoiceDate, int customerId, string customerEmailId, string customerName)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                Invoice invoice = context.Invoice.FirstOrDefault(f => f.InvoiceNumber == invoiceNumber);
                if (invoice != null)
                {
                    response.ResponseCode = ((int)Enumerations.Unit.InvoiceNumberAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.Unit.InvoiceNumberAlreadyExist.GetStringValue();
                    return response;
                }

                List<UnitSerialNumber> unitList = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();
                invoice = context.Invoice.Create();
                invoice.CreateDate = DateTime.UtcNow;
                invoice.InvoiceDate = invoiceDate;
                invoice.InvoiceNumber = invoiceNumber;
                invoice.CustomerId = customerId;
                invoice.CustomerName = customerName;
                context.Invoice.Add(invoice);
                context.SaveChanges();
                unitList.ForEach(f => { f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.Sold; });
                context.SaveChanges();

                List<MappingInvoiceAndUnit> mapInvoiceAndUnitList = unitList.Select(s => new MappingInvoiceAndUnit
                {
                    InvoiceId = invoice.Id,
                    UnitId = s.Id
                }).ToList();
                context.MappingInvoiceAndUnit.AddRange(mapInvoiceAndUnitList);
                List<UnitStatusHistory> unitStatusHistoryList = new List<UnitStatusHistory>();
                foreach (var unit in unitList)
                {
                    unitStatusHistoryList.Add(new UnitStatusHistory
                    {
                        DateOfStatusChanged = DateTime.UtcNow,
                        StatusId = (int)Enumerations.UnitStatus.Sold,
                        UnitSerialNumberId = unit.Id

                    });
                }
                context.UnitStatusHistories.AddRange(unitStatusHistoryList);
                context.SaveChanges();

                List<PartNumberMaster> partNumberMasterList = unitList.Select(s => s.PurchaseOrder.PartNumberMaster).Distinct().ToList();

                string zipPath = ConfigurationReader.PartZipFilePath;
                List<string> zipFilePaths = new List<string>();
                foreach (var part in partNumberMasterList)
                {
                    zipFilePaths.Add(zipPath + part.PartNumber + ".zip");
                }

                if (!string.IsNullOrEmpty(customerEmailId))
                {

                    Task.Factory.StartNew(() =>
                    {
                        CreateZipForPartDocs(partNumberMasterList);
                        SendInvoiceToCustomer(invoice, unitList, zipFilePaths, customerEmailId);
                    });
                }
                response.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                response.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public GeneralResponse DeleteInvoice(int invoiceId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                Invoice invoice = context.Invoice.FirstOrDefault(f => f.Id == invoiceId);
                if (invoice == null)
                {
                    response.ResponseCode = ((int)Enumerations.Invoice.InvalidDeleteRequest).ToString();
                    response.ResponseMessage = Enumerations.Invoice.InvalidDeleteRequest.GetStringValue();
                    return response;
                }

                List<MappingInvoiceAndUnit> mapInvoiceAndUnitList = context.MappingInvoiceAndUnit.Where(w => w.InvoiceId == invoice.Id).ToList();
                List<int> unitIdList = mapInvoiceAndUnitList.Select(s => s.UnitId).ToList();
                context.UnitStatusHistories.RemoveRange(context.UnitStatusHistories.Where(w => unitIdList.Contains(w.UnitSerialNumberId)).ToList());
                context.SalesReturn.RemoveRange(context.SalesReturn.Where(w => w.InvoiceId == invoiceId).ToList());
                context.UnitSerialNumbers.Where(w => unitIdList.Contains(w.Id)).ToList().ForEach(f => { f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.AvailableForSale; });
                context.MappingInvoiceAndUnit.RemoveRange(mapInvoiceAndUnitList);
                context.Invoice.Remove(invoice);
                context.SaveChanges();

                response.ResponseCode = ((int)Enumerations.Invoice.Success).ToString();
                response.ResponseMessage = Enumerations.Invoice.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Invoice.Exception).ToString();
                response.ResponseMessage = Enumerations.Invoice.Exception.GetStringValue();
            }
            return response;
        }

        public void CreateZipForPartDocs(List<PartNumberMaster> partNumberList)
        {
            Misc misc = new Misc();
            string s3Path = misc.GetS3FolderName("EngineeringDocs");
            string zipPath = ConfigurationReader.PartZipFilePath;

            AWS.AWS aws = new AWS.AWS();
            foreach (var part in partNumberList)
            {
                if (System.IO.File.Exists((zipPath + part.PartNumber + ".zip")))
                {
                    System.IO.File.Delete(zipPath + part.PartNumber + ".zip");

                }
                aws.CreateZipFromS3Directory((zipPath + part.PartNumber + ".zip"), (s3Path + part.Id.ToString() + "/Customer"));
            }
        }

        public void SendInvoiceToCustomer(Invoice invoice, List<UnitSerialNumber> unitList, List<string> zipPathList, string emailId)
        {
            var pathHtmlFile = ConfigurationReader.EmailTemplatePath + "InvoiceEmail.html";

            string serverUrl = ConfigurationReader.ServerUrl;
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");

            string cid = "https://guardianavionicsdata.com/Images/logo_smart_document.png";// ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "smart_plane.png";
            string cidFBImageIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "facebook.png";
            string cidTwitterIcon = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "twitter.png";

            StringBuilder strTable = new StringBuilder();
            int sno = 0;
            foreach (var unit in unitList)
            {
                strTable.Append("<tr " + (sno % 2 == 0 ? "bgcolor=\"#c8e2eb\"" : "") + " >");
                strTable.Append("<td>" + unit.PurchaseOrder.PartNumberMaster.PartNumber + "</td>");
                strTable.Append("<td>" + unit.PurchaseOrder.PartNumberMaster.ModelNumber + "</td>");
                strTable.Append("<td>" + unit.PurchaseOrder.PartNumberMaster.Description + "</td>");
                strTable.Append("<td>" + unit.SerialNumber + "</td>");
                strTable.Append("</tr>");
                sno++;

            }

            string h1 = string.Format(htmlBody,
                 cid,  //0  
                "Thank you for purchasing from Guardian Avionics, Please see the attached installation files for the items purchased.", //1
                  invoice.InvoiceNumber, //2
                   String.Format("{0:dd MMM yyyy}", invoice.InvoiceDate), //3
                                                                          // invoice.Customer.Name, //4
                   strTable.ToString() //5
                   );
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);

            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);
            mail.To.Add(emailId);
            mail.Subject = "Invoice Details";
            mail.Body = h1;
            foreach (string zipPath in zipPathList)
            {
                mail.Attachments.Add(new Attachment(zipPath));
            }
            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["Host"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailIdForSendingEmail"], ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);
        }

        public GeneralResponse DeleteUnit(int unitId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                var unit = context.UnitSerialNumbers.FirstOrDefault(f => f.Id == unitId);
                if (unit != null)
                {
                    int statusId = 0;
                    string status = "";

                    statusId = unit.UnitCurrentStatusId;
                    status = ((Enumerations.UnitStatus)statusId).ToString();
                    if (unit.UnitCurrentStatusId == (int)Enumerations.UnitStatus.AvailableForSale || unit.UnitCurrentStatusId == (int)Enumerations.UnitStatus.Received)
                    {
                        context.SalesReturn.RemoveRange(context.SalesReturn.Where(w => w.UnitId == unitId).ToList());
                        context.UnitSerialNumbers.Remove(unit);
                        context.SaveChanges();
                        response.ResponseCode = ((int)Enumerations.Unit.Success).ToString();
                        response.ResponseMessage = Enumerations.Unit.Success.GetStringValue();
                    }
                    else
                    {
                        response.ResponseCode = ((int)Enumerations.Unit.CannotDeleteUnitWithChangedStatus).ToString();
                        response.ResponseMessage = string.Format(Enumerations.Unit.CannotDeleteUnitWithChangedStatus.GetStringValue(), status, unit.UnitStatus.Status);
                    }
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.Unit.InvalidDeleteRequest).ToString();
                    response.ResponseMessage = Enumerations.Unit.InvalidDeleteRequest.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Unit.Exception).ToString();
                response.ResponseMessage = Enumerations.Unit.Exception.GetStringValue();
            }
            return response;
        }

        public List<InvoiceModel> GetInvoiceList(string key, string value)
        {
            var context = new GuardianAvionicsEntities();
            List<InvoiceModel> invoiceModelList = new List<InvoiceModel>();
            switch (key)
            {
                case "":
                    {
                        invoiceModelList = context.Invoice.OrderByDescending(o => o.Id).Take(50).Select(s => new InvoiceModel
                        {
                            InvoiceId = s.Id.ToString(),
                            InvoiceDate = s.InvoiceDate,
                            CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                            ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                            InvoiceNumber = s.InvoiceNumber,
                            CustomerName = s.CustomerName ?? ""
                        }).ToList();
                        break;
                    }
                case "InvoiceNumber":
                    {
                        invoiceModelList = context.Invoice.Where(w => w.InvoiceNumber.Contains(value)).OrderByDescending(o => o.Id).Select(s => new InvoiceModel
                        {
                            InvoiceId = s.Id.ToString(),
                            InvoiceDate = s.InvoiceDate,
                            CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                            ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                            InvoiceNumber = s.InvoiceNumber,
                            CustomerName = s.CustomerName ?? ""
                        }).ToList();
                        break;
                    }
                case "InvoiceDate":
                    {
                        DateTime invoiceDate = Convert.ToDateTime(value);
                        invoiceModelList = context.Invoice.Where(w => SqlFunctions.DateDiff("DAY", w.InvoiceDate, invoiceDate) == 0).OrderByDescending(o => o.Id).Select(s => new InvoiceModel
                        {
                            InvoiceId = s.Id.ToString(),
                            InvoiceDate = s.InvoiceDate,
                            CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                            ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                            InvoiceNumber = s.InvoiceNumber,
                            CustomerName = s.CustomerName ?? ""
                        }).ToList();
                        break;
                    }
                case "CustomerName":
                    {
                        int unitStatus = (int)Enumerations.UnitStatus.Sold;
                        List<int> invoiceIdList = context.UnitSerialNumbers.Where(w => w.PurchaseOrder.PartNumberMaster.PartNumber == value && w.UnitCurrentStatusId == unitStatus && w.InvoiceId != null).Select(s => s.InvoiceId ?? 0).Distinct().ToList();
                        invoiceModelList = context.Invoice.Where(w => w.CustomerType.Name.Contains(value)).OrderByDescending(o => o.Id).Select(s => new InvoiceModel
                        {
                            InvoiceId = s.Id.ToString(),
                            InvoiceDate = s.InvoiceDate,
                            CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                            ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                            InvoiceNumber = s.InvoiceNumber,
                            CustomerName = s.CustomerName ?? ""
                        }).ToList();
                        break;
                    }
                case "All":
                    {
                        invoiceModelList = context.Invoice.OrderByDescending(o => o.Id).Select(s => new InvoiceModel
                        {
                            InvoiceId = s.Id.ToString(),
                            InvoiceDate = s.InvoiceDate,
                            CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                            ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                            InvoiceNumber = s.InvoiceNumber,
                            CustomerName = s.CustomerName ?? ""
                        }).ToList();
                        break;
                    }
                case "SerialNumber":
                    {
                        int? serialNo = Convert.ToInt32(value);
                        var unitSerialNo = context.UnitSerialNumbers.FirstOrDefault(f => f.SerialNumber == serialNo && f.InvoiceId != null);
                        if (unitSerialNo != null)
                        {
                            invoiceModelList.Add(new InvoiceModel
                            {
                                InvoiceId = unitSerialNo.Invoice.Id.ToString(),
                                InvoiceDate = unitSerialNo.Invoice.InvoiceDate,
                                CustomerType = unitSerialNo.Invoice.CustomerId == null ? "" : unitSerialNo.Invoice.CustomerType.Name,
                                ItemCount = unitSerialNo.Invoice.MappingInvoiceAndUnit.Count() + unitSerialNo.Invoice.SalesReturn.Count(),
                                InvoiceNumber = unitSerialNo.Invoice.InvoiceNumber,
                                CustomerName = unitSerialNo.Invoice.CustomerName ?? ""
                            });
                        }
                        break;
                    }
            }
            int sno = 1;
            Misc misc = new Misc();
            invoiceModelList.ForEach(f => { f.SNO = sno++; f.InvoiceId = misc.Encrypt(f.InvoiceId); });
            return invoiceModelList;
        }

        public List<InvoiceModel> GetInvoiceInChunk(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.Invoice.Count();

            IQueryable<Invoice> invoiceList;
            invoiceList = context.Invoice;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                invoiceList = invoiceList.Where(w => w.InvoiceNumber.ToLower().Contains(search) ||
                                                    w.CustomerType.Name.ToLower().Contains(search) ||
                                                    w.CustomerName.ToLower().Contains(search));
            }

            totalRowsAfterFilter = invoiceList.Count();

            //Sorting
            switch (sortColumnName)
            {
                case "SNO":
                    {
                        invoiceList = sortDirection == "asc" ? invoiceList.OrderBy(o => o.Id).Skip(start).Take(length) : invoiceList.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "InvoiceNumber":
                    {
                        invoiceList = sortDirection == "asc" ? invoiceList.OrderBy(o => o.InvoiceNumber).Skip(start).Take(length) : invoiceList.OrderByDescending(o => o.InvoiceNumber).Skip(start).Take(length);
                        break;
                    }
                case "CustomerType":
                    {
                        invoiceList = sortDirection == "asc" ? invoiceList.OrderBy(o => (o.CustomerType.Name)).Skip(start).Take(length) : invoiceList.OrderByDescending(o => (o.CustomerType.Name)).Skip(start).Take(length);
                        break;
                    }
                case "CustomerName":
                    {
                        invoiceList = sortDirection == "asc" ? invoiceList.OrderBy(o => o.CustomerName).Skip(start).Take(length) : invoiceList.OrderByDescending(o => o.CustomerName).Skip(start).Take(length);
                        break;
                    }
            }

            Misc misc = new Misc();

            //Paging
            List<InvoiceModel> response = invoiceList.Select(s => new InvoiceModel
            {
                InvoiceId = s.Id.ToString(),
                InvoiceDate = s.InvoiceDate,
                CustomerType = s.CustomerId == null ? "" : s.CustomerType.Name,
                ItemCount = s.MappingInvoiceAndUnit.Count() + s.SalesReturn.Count(),
                InvoiceNumber = s.InvoiceNumber,
                CustomerName = s.CustomerName ?? ""
            }).ToList();

            int SerialNo = response.Count;
            if (sortDirection == "asc")
            {
                response.ForEach(f => { f.SNO = ++start; f.InvoiceCreatedOn = f.InvoiceDate.ToString("MM/dd/yyyy"); });
            }
            else
            {
                start = totalRecord - start;
                response.ForEach(f => { f.SNO = start--; f.InvoiceCreatedOn = f.InvoiceDate.ToString("MM/dd/yyyy"); });
            }
            return response;
        }



        public InvoiceDetailsModel GetInventoryDetailsById(int id)
        {
            InvoiceDetailsModel invoiceDetailsModel = new InvoiceDetailsModel
            {
                InvoiceModel = new InvoiceModel(),
                UnitModelList = new List<UnitModel>()
            };
            var context = new GuardianAvionicsEntities();
            Invoice invoice = context.Invoice.FirstOrDefault(w => w.Id == id);
            if (invoice != null)
            {
                invoiceDetailsModel.InvoiceModel.InvoiceDate = invoice.InvoiceDate;
                invoiceDetailsModel.InvoiceModel.CustomerType = invoice.CustomerType.Name;
                invoiceDetailsModel.InvoiceModel.CustomerName = invoice.CustomerName;
                invoiceDetailsModel.InvoiceModel.CustomerId = new Misc().Encrypt(invoice.CustomerId.ToString() ?? "0");
                invoiceDetailsModel.InvoiceModel.ItemCount = invoice.UnitSerialNumbers.Count();
                invoiceDetailsModel.InvoiceModel.InvoiceNumber = invoice.InvoiceNumber;
                invoiceDetailsModel.InvoiceModel.InvoiceId = new Misc().Encrypt(invoice.Id.ToString());

                invoiceDetailsModel.UnitModelList = invoice.MappingInvoiceAndUnit.Select(s => new UnitModel
                {
                    PurchaseOrderNumber = s.UnitSerialNumber.PurchaseOrder.PurchaseOrderNumber,
                    //PartNumber = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber,
                    PartNumber = s.UnitSerialNumber.NewPartNumber == null ? s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber : s.UnitSerialNumber.NewPartNumber,
                    Description = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.Description,
                    SerialNumber = s.UnitSerialNumber.SerialNumber,
                    UnitStatus = s.UnitSerialNumber.UnitStatus.Status,
                    SoftwareLevel = s.UnitSerialNumber.PurchaseOrder.SoftwareLevel,
                    BoardSerialNumber = s.UnitSerialNumber.BoardSerialNumber
                }).ToList();
                string status = Enumerations.UnitStatus.SalesReturn.GetStringValue();
                invoiceDetailsModel.UnitModelList.AddRange(context.SalesReturn.Where(w => w.InvoiceId == id).Select(s => new UnitModel
                {
                    PurchaseOrderNumber = s.UnitSerialNumber.PurchaseOrder.PurchaseOrderNumber,
                    //PartNumber = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber,
                    PartNumber = s.UnitSerialNumber.NewPartNumber==null? s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber : s.UnitSerialNumber.NewPartNumber,
                    Description = s.UnitSerialNumber.PurchaseOrder.PartNumberMaster.Description,
                    SerialNumber = s.UnitSerialNumber.SerialNumber,
                    UnitStatus = status,
                    SoftwareLevel = s.UnitSerialNumber.PurchaseOrder.SoftwareLevel,
                    BoardSerialNumber = s.UnitSerialNumber.BoardSerialNumber,
                    ReasonForSalesReturn = s.Reason
                }));

            }
            int count = 1;
            invoiceDetailsModel.UnitModelList.ForEach(f => f.SNO = count++);
            return invoiceDetailsModel;
        }

        public List<UnitDetailedSheetModel> GetAvailableForSaleReport(int statusId)
        {
            var context = new GuardianAvionicsEntities();
            List<UnitDetailedSheetModel> partIdListList = context.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == statusId && w.PurchaseOrderId != null).GroupBy(p => new { p.PurchaseOrder.PartNumberMaster.Id }).Select(g => new UnitDetailedSheetModel { UnitCount = g.Count(), Id = g.Key.Id }).ToList();

            List<PartNumberMaster> partListTemp = context.PartNumberMasters.ToList();

            var partList = (from p in partListTemp
                            join plist in partIdListList on new { Id = p.Id } equals new { Id = plist.Id } into t
                            from rt in t.DefaultIfEmpty()
                            select new UnitDetailedSheetModel
                            {
                                PartNumber = p.PartNumber,
                                Description = p.Description,
                                UnitCount = rt == null ? 0 : rt.UnitCount,
                                RecomendedQty = p.RecommendQty ?? 0,
                                CostPrice = p.CostPrice ?? 0,
                                SalesPrice = p.SalesPrice ?? 0
                            }).ToList();
            return partList;
        }


        public List<UnitDetailedSheetModel> GetReport(int? partId, int? serialNumber, int statusId, bool isDetailedSheet, DateTime? startDate, DateTime? endDate)
        {
            var context = new GuardianAvionicsEntities();
            IQueryable<UnitSerialNumber> units;
            units = context.UnitSerialNumbers.Where(w => w.PurchaseOrderId != null);
            if (partId != null)
            {
                units = units.Where(w => w.PurchaseOrder.PartNumberMaster.Id == partId);
            }
            if (serialNumber != null)
            {
                units = units.Where(w => w.SerialNumber == serialNumber);
            }
            if (statusId != 0)
            {
                if (statusId == (int)Enumerations.UnitStatus.AvailableForSale)
                {
                    //apply the date filter on purchase order table
                    units = units.Where(w => w.UnitCurrentStatusId == statusId && w.PurchaseOrder.CreateDate >= startDate && w.PurchaseOrder.CreateDate <= endDate);
                }
                else
                {
                    List<int> unitIdList = context.UnitStatusHistories.Where(w => w.StatusId == statusId && w.DateOfStatusChanged >= startDate && w.DateOfStatusChanged <= endDate).Select(s => s.UnitSerialNumberId).ToList();
                    units = units.Where(w => w.UnitCurrentStatusId == statusId && unitIdList.Contains(w.Id));
                }
            }

            if (!isDetailedSheet)
            {
                var summary = units.GroupBy(p => new { p.PurchaseOrder.PartNumberMaster.PartNumber, p.UnitStatus.Status })
                            .Select(g => new UnitDetailedSheetModel
                            {
                                UnitCount = g.Count(),
                                PartNumber = g.FirstOrDefault().PurchaseOrder.PartNumberMaster.PartNumber,
                                RecStatus = g.FirstOrDefault().UnitStatus.Status,
                                CostPrice = g.FirstOrDefault().PurchaseOrder.PartNumberMaster.CostPrice ?? 0,
                                SalesPrice = g.FirstOrDefault().PurchaseOrder.PartNumberMaster.SalesPrice ?? 0,
                                Description = g.FirstOrDefault().PurchaseOrder.PartNumberMaster.Description
                            }).OrderBy(o => o.PartNumber).ThenBy(o => o.RecStatus).ToList();
                summary.ForEach(f => f.Margin = (f.CostPrice == 0 ? 0 : Math.Floor(((f.SalesPrice - f.CostPrice) * 100) / f.CostPrice)));
                return summary;
            }

            var detailedSheetList = units.OrderBy(o => o.PurchaseOrder.PartNumberMaster.PartNumber).Select(s => new UnitDetailedSheetModel
            {
                Id = s.Id,
                PartNumber = s.PurchaseOrder.PartNumberMaster.PartNumber,
                Description = s.PurchaseOrder.PartNumberMaster.Description,
                SerialNumber = s.SerialNumber,
                SoftwareLevel = s.PurchaseOrder.SoftwareLevel,
                RecStatus = "R",
                StatusId = s.UnitCurrentStatusId,
                RecDate = s.ReceivedDate ?? s.PurchaseOrder.CreateDate
            }).ToList();
            var arrSerialNoId = detailedSheetList.Select(s => s.Id).ToList();

            var UnitStatusHistoryList = context.UnitStatusHistories.Where(w => arrSerialNoId.Contains(w.UnitSerialNumberId)).ToList();

            detailedSheetList = (from pd in detailedSheetList
                                 join od in UnitStatusHistoryList on new { Id = pd.Id, StatusId = pd.StatusId } equals new { Id = od.UnitSerialNumberId, StatusId = od.StatusId } into t
                                 from rt in t.DefaultIfEmpty()

                                 orderby pd.Id
                                 select new UnitDetailedSheetModel
                                 {
                                     Id = pd.Id,
                                     PartNumber = pd.PartNumber,
                                     Description = pd.Description,
                                     SerialNumber = pd.SerialNumber,
                                     SoftwareLevel = pd.SoftwareLevel,
                                     StatusId = pd.StatusId,
                                     RecStatus = "R",
                                     RecDate = pd.RecDate,
                                     AvailableForSaleStatus = "A",
                                     AvailableForSaleDate = rt == null ? (DateTime?)null : rt.DateOfStatusChanged
                                 }).ToList();

            detailedSheetList = (from pd in detailedSheetList
                                 join od in UnitStatusHistoryList on new { Id = pd.Id, StatusId = pd.StatusId } equals new { Id = od.UnitSerialNumberId, StatusId = od.StatusId } into t
                                 from rt in t.DefaultIfEmpty()

                                 orderby pd.Id
                                 select new UnitDetailedSheetModel
                                 {
                                     Id = pd.Id,
                                     PartNumber = pd.PartNumber,
                                     Description = pd.Description,
                                     SerialNumber = pd.SerialNumber,
                                     SoftwareLevel = pd.SoftwareLevel,
                                     RecStatus = "R",
                                     RecDate = pd.RecDate,
                                     StatusId = pd.StatusId,
                                     AvailableForSaleStatus = "A",
                                     AvailableForSaleDate = pd.AvailableForSaleDate,
                                     SoldStatus = "S",
                                     SoldDate = rt == null ? (DateTime?)null : rt.DateOfStatusChanged
                                 }).ToList();
            return detailedSheetList;
        }

        #region Vendor
        public List<SelectListItem> GetVendorNameAndId()
        {
            var context = new GuardianAvionicsEntities();
            var response = context.Vendor.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString()
            }).ToList();
            Misc misc = new Misc();
            response.ForEach(f => f.Value = misc.Encrypt(f.Value));
            response.Insert(0, new SelectListItem { Text = "Select Vendor", Value = "" });
            return response;
        }

        public List<VendorModel> GetVendorList()
        {
            var context = new GuardianAvionicsEntities();
            List<VendorModel> vendorList = context.Vendor.Select((s) => new VendorModel
            {
                Id = s.Id,
                Name = s.Name,
                ContactName = s.ContactName,
                ContactLastName = s.ContactLastName,
                Email = s.Email,
                PhoneNumber = s.PhoneNumber,
                Comments = s.Comments,
                Website = s.WebsiteUrl,
                WebUrlPrefix = s.WebUrlPrefix
            }).ToList();
            Misc misc = new Misc();
            int SNO = 1;
            vendorList.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; f.SNO = SNO++; });
            return vendorList;
        }

        public GeneralResponse AddVendor(VendorModel vendorModel)
        {
            GeneralResponse response;
            var context = new GuardianAvionicsEntities();
            try
            {
                var vendor = context.Vendor.FirstOrDefault(f => f.Name == vendorModel.Name);
                if (vendor == null)
                {
                    context.Vendor.Add(new Vendor
                    {
                        Name = vendorModel.Name,
                        ContactName = vendorModel.ContactName,
                        ContactLastName = vendorModel.ContactLastName,
                        Email = vendorModel.Email,
                        PhoneNumber = vendorModel.PhoneNumber,
                        Comments = vendorModel.Comments,
                        WebsiteUrl = vendorModel.Website,
                        WebUrlPrefix = vendorModel.WebUrlPrefix
                    });
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Vendor.Success).ToString(),
                        ResponseMessage = Enumerations.Vendor.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Vendor.VendorAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Vendor.VendorAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Vendor.Exception).ToString(),
                    ResponseMessage = Enumerations.Vendor.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse DeleteVendor(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var vendor = context.Vendor.FirstOrDefault(f => f.Id == id);
                if (vendor != null)
                {
                    if (context.PartNumberMasters.Where(w => w.DefaultVendorId == vendor.Id).Any())
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Vendor.CannotDeletVendor_InUse).ToString(),
                            ResponseMessage = Enumerations.Vendor.CannotDeletVendor_InUse.GetStringValue()
                        };
                        return response;
                    }
                    else if (context.PurchaseOrders.Where(w => w.VendorId == vendor.Id).Any())
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Vendor.CannotDeletVendor_InUse_RecOrder).ToString(),
                            ResponseMessage = Enumerations.Vendor.CannotDeletVendor_InUse_RecOrder.GetStringValue()
                        };
                        return response;
                    }
                    context.Vendor.Remove(vendor);
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Vendor.Success).ToString(),
                        ResponseMessage = Enumerations.Vendor.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Vendor.InvalidDeleteRequest).ToString(),
                        ResponseMessage = Enumerations.Vendor.InvalidDeleteRequest.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Vendor.Exception).ToString(),
                    ResponseMessage = Enumerations.Vendor.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse EditVendor(VendorModel vendorModel)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var vendor = context.Vendor.FirstOrDefault(f => f.Id != vendorModel.Id && f.Name == vendorModel.Name);
                if (vendor == null)
                {
                    vendor = context.Vendor.FirstOrDefault(f => f.Id == vendorModel.Id);
                    if (vendor != null)
                    {
                        vendor.Name = vendorModel.Name;
                        vendor.ContactName = vendorModel.ContactName;
                        vendor.ContactLastName = vendorModel.ContactLastName;
                        vendor.Email = vendorModel.Email;
                        vendor.PhoneNumber = vendorModel.PhoneNumber;
                        vendor.Comments = vendorModel.Comments;
                        vendor.WebsiteUrl = vendorModel.Website;
                        vendor.WebUrlPrefix = vendorModel.WebUrlPrefix;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Vendor.Success).ToString(),
                            ResponseMessage = Enumerations.Vendor.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Vendor.InvalidRequest).ToString(),
                            ResponseMessage = Enumerations.Vendor.InvalidRequest.GetStringValue()
                        };
                    }

                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Vendor.VendorAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Vendor.VendorAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Vendor.Exception).ToString(),
                    ResponseMessage = Enumerations.Vendor.Exception.GetStringValue()
                };
            }
            return response;
        }

        #endregion Vendor

        #region Receiver

        public List<ReceiverModel> GetReceiverList()
        {
            var context = new GuardianAvionicsEntities();
            List<ReceiverModel> receiverList = context.InventoryReceivers.Select((s) => new ReceiverModel
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            Misc misc = new Misc();
            int SNO = 1;
            receiverList.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; f.SNO = SNO++; });
            return receiverList;
        }

        public GeneralResponse AddReceiver(string name)
        {
            GeneralResponse response;
            var context = new GuardianAvionicsEntities();
            try
            {
                var receiver = context.InventoryReceivers.FirstOrDefault(f => f.Name == name);
                if (receiver == null)
                {
                    context.InventoryReceivers.Add(new InventoryReceiver { Name = name });
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Receiver.Success).ToString(),
                        ResponseMessage = Enumerations.Receiver.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Receiver.ReceiverAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Receiver.ReceiverAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Receiver.Exception).ToString(),
                    ResponseMessage = Enumerations.Receiver.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse DeleteReceiver(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var receiver = context.InventoryReceivers.FirstOrDefault(f => f.Id == id);
                if (receiver != null)
                {
                    if (context.PurchaseOrders.FirstOrDefault(f => f.InventoryReceiverId == id) != null)
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Receiver.ReceiverBelongsToPO).ToString(),
                            ResponseMessage = Enumerations.Receiver.ReceiverBelongsToPO.GetStringValue()
                        };
                    }
                    else
                    {
                        context.InventoryReceivers.Remove(receiver);
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Receiver.Success).ToString(),
                            ResponseMessage = Enumerations.Receiver.Success.GetStringValue()
                        };
                    }
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Receiver.InvalidDeleteRequest).ToString(),
                        ResponseMessage = Enumerations.Receiver.InvalidDeleteRequest.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Receiver.Exception).ToString(),
                    ResponseMessage = Enumerations.Receiver.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse EditReceiver(string receiverName, int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var receiver = context.InventoryReceivers.FirstOrDefault(f => f.Id != id && f.Name == receiverName);
                if (receiver == null)
                {
                    receiver = context.InventoryReceivers.FirstOrDefault(f => f.Id == id);
                    if (receiver != null)
                    {
                        receiver.Name = receiverName;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Receiver.Success).ToString(),
                            ResponseMessage = Enumerations.Receiver.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Receiver.InvalidRequest).ToString(),
                            ResponseMessage = Enumerations.Receiver.InvalidRequest.GetStringValue()
                        };
                    }

                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Receiver.ReceiverAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Receiver.ReceiverAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Receiver.Exception).ToString(),
                    ResponseMessage = Enumerations.Receiver.Exception.GetStringValue()
                };
            }
            return response;
        }

        #endregion Receiver

        #region Customer

        public List<CustomerModel> GetCustomerList()
        {
            var context = new GuardianAvionicsEntities();
            List<CustomerModel> customerList = context.CustomerType.OrderByDescending(o => o.Id).Select((s) => new CustomerModel
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            Misc misc = new Misc();
            int SNO = customerList.Count;
            customerList.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; f.SNO = SNO--; });
            return customerList;
        }



        public GeneralResponse AddCustomer(string name)
        {

            var response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                CustomerType customer = context.CustomerType.FirstOrDefault(f => f.Name == name);
                if (customer != null)
                {
                    response.ResponseCode = ((int)Enumerations.Customer.CustomerAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.Customer.CustomerAlreadyExist.GetStringValue();
                    return response;
                }
                customer = new CustomerType();
                customer.Name = name;
                context.CustomerType.Add(customer);
                context.SaveChanges();
                response.ResponseCode = ((int)Enumerations.Customer.Success).ToString();
                response.ResponseMessage = Enumerations.Customer.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Customer.Exception).ToString();
                response.ResponseMessage = Enumerations.Customer.Exception.GetStringValue();
                return response;
            }
            return response;
        }

        public GeneralResponse DeleteCustomerType(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var customer = context.CustomerType.FirstOrDefault(f => f.Id == id);
                if (customer != null)
                {

                    var inventoryCustomerList = context.InventoryCustomer.Where(w => w.CustomerTypeId == customer.Id).ToList();
                    if (inventoryCustomerList.Count() > 0)
                    {
                        inventoryCustomerList.ForEach(f => f.CustomerTypeId = null);
                    }

                    if (context.Invoice.FirstOrDefault(f => f.CustomerId == id) != null)
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Customer.CannotDeleteCustomer_InUse).ToString(),
                            ResponseMessage = Enumerations.Customer.CannotDeleteCustomer_InUse.GetStringValue()
                        };
                        return response;
                    }

                    context.CustomerType.Remove(customer);
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Customer.Success).ToString(),
                        ResponseMessage = Enumerations.Customer.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Customer.InvalidRequestToDelete).ToString(),
                        ResponseMessage = Enumerations.Customer.InvalidRequestToDelete.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Customer.Exception).ToString(),
                    ResponseMessage = Enumerations.Customer.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse EditCustomer(string customerName, int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var customer = context.CustomerType.FirstOrDefault(f => f.Id != id && f.Name == customerName);
                if (customer == null)
                {
                    customer = context.CustomerType.FirstOrDefault(f => f.Id == id);
                    if (customer != null)
                    {
                        customer.Name = customerName;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Customer.Success).ToString(),
                            ResponseMessage = Enumerations.Customer.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Customer.InvalidRequestToEdit).ToString(),
                            ResponseMessage = Enumerations.Customer.InvalidRequestToEdit.GetStringValue()
                        };
                    }
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Customer.CustomerAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Customer.CustomerAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Receiver.Exception).ToString(),
                    ResponseMessage = Enumerations.Receiver.Exception.GetStringValue()
                };
            }
            return response;
        }
        #endregion

        #region EngineeringDocs

        public EngineeringDocument GetEngineeringDocs(string partNo)
        {

            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();

            PartNumberMaster partNoMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNo);
            EngineeringDocs engDocs = context.EngineeringDocs.FirstOrDefault(w => w.ParentId == 0 && w.PartId == partNoMaster.Id);
            if (engDocs == null)
            {
                engDocs = new EngineeringDocs
                {
                    PartId = partNoMaster.Id,
                    IsFolder = true,
                    ParentId = 0,
                    IsDefault = true,
                    DocumentName = "Documents"
                };
                context.EngineeringDocs.Add(engDocs);
                context.SaveChanges();
            }
            EngineeringDocument engDoc = GetAllEngineeringDocsById(engDocs.Id);
            return engDoc;

        }

        public EngineeringDocument GetAllEngineeringDocsById(int engDocId)
        {
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            EngineeringDocument engDoc = new EngineeringDocument
            {
                documentList = new List<EngineeringDocsModel>(),
                parentFolderList = new List<EngDocListItem>()
            };
            List<EngineeringDocs> engDocList = context.EngineeringDocs.Where(w => w.ParentId == engDocId).ToList();
            string folderPath = GetParentFolderPath(engDocId);
            engDoc.documentList = engDocList.Select(s => new EngineeringDocsModel
            {
                Id = misc.Encrypt(s.Id.ToString()),
                DocumentName = s.DocumentName,
                IsFolder = s.IsFolder,
                IsDefault = s.IsDefault ?? true,
                DocUrl = s.IsFolder ? "" : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs + folderPath + "/" + s.DocumentName)
            }).ToList();
            engDoc.parentFolderList = GetAllParentDirectory(engDocId);
            return engDoc;
        }

        public List<EngDocListItem> GetAllParentDirectory(int engDocId)
        {
            int parentId = 1;
            int sno = 1;
            List<EngDocListItem> parentList = new List<EngDocListItem>();
            EngineeringDocs engDoc;
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();

            while (engDocId != 0)
            {
                engDoc = context.EngineeringDocs.FirstOrDefault(f => f.Id == engDocId);
                parentList.Add(new EngDocListItem { SNO = sno, Text = engDoc.DocumentName, Value = misc.Encrypt(engDoc.Id.ToString()) });
                sno++;
                engDocId = engDoc.ParentId;

            }
            return parentList;
        }

        public string GetParentFolderPath(int engDocId)
        {
            int sno = 1;
            List<EngDocListItem> parentList = new List<EngDocListItem>();
            EngineeringDocs engDoc;
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            int partId = context.EngineeringDocs.FirstOrDefault(f => f.Id == engDocId).PartId;
            while (engDocId != 0)
            {
                engDoc = context.EngineeringDocs.FirstOrDefault(f => f.Id == engDocId);
                parentList.Add(new EngDocListItem { SNO = sno, Text = engDoc.DocumentName, Value = misc.Encrypt(engDoc.Id.ToString()) });
                sno++;
                engDocId = engDoc.ParentId;

            }
            if (parentList.Count == 1)
            {
                return partId.ToString();
            }
            string[] arrParentList = parentList.OrderByDescending(o => o.SNO).Skip(1).Select(s => s.Text).ToArray();
            return partId.ToString() + "/" + string.Join("/", arrParentList);
        }


        public GeneralResponse AddEngineeringDirectory(string directoryName, int parentId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            EngineeringDocs engineeringDocs = context.EngineeringDocs.FirstOrDefault(w => w.ParentId == parentId && w.DocumentName == directoryName);
            if (engineeringDocs != null)
            {
                response.ResponseMessage = "Folder name already exists.";
                response.ResponseCode = "1";
                return response;
            }
            engineeringDocs = context.EngineeringDocs.FirstOrDefault(w => w.Id == parentId);
            context.EngineeringDocs.Add(new EngineeringDocs
            {
                DocumentName = directoryName,
                IsFolder = true,
                ParentId = parentId,
                PartId = engineeringDocs.PartId,
                IsDefault = false
            });
            context.SaveChanges();
            response.ResponseMessage = "Folder added successfully.";
            response.ResponseCode = "0";
            string FolderPath = GetParentFolderPath(parentId);
            //S3BucketUpdates s3Updates = new S3BucketUpdates();
            //s3Updates.CreateFolderOns3(directoryName, FolderPath);
            AWS.AWS aws = new AWS.AWS();
            aws.CreateFolderOns3(directoryName, FolderPath);


            return response;
        }

        public GeneralResponse UploadEngineeringFiles(List<HttpPostedFile> fileList, int parentId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            List<EngineeringDocs> docList = new List<EngineeringDocs>();
            S3BucketUpdates s3Updates = new S3BucketUpdates();
            string FolderPath = "";
            var engDoc = context.EngineeringDocs.FirstOrDefault(f => f.Id == parentId);
            List<string> fileNameList = fileList.Select(s => s.FileName).ToList();
            var existingDocs = context.EngineeringDocs.Where(w => fileNameList.Contains(w.DocumentName) && w.ParentId == parentId).Select(s => s.DocumentName).ToList();
            if (existingDocs.Count > 0)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = string.Join(",", existingDocs) + " already exist into the folder.";
                return response;
            }

            foreach (var file in fileList)
            {
                docList.Add(new EngineeringDocs
                {
                    DocumentName = file.FileName,
                    IsFolder = false,
                    ParentId = parentId,
                    PartId = engDoc.PartId,
                    IsDefault = false
                });
                FolderPath = GetParentFolderPath(parentId);
                s3Updates.SaveFileToS3BucketEngDocs(file.FileName, file.InputStream, FolderPath);
            }
            context.EngineeringDocs.AddRange(docList);
            context.SaveChanges();

            response.ResponseCode = "0";
            response.ResponseMessage = (fileList.Count == 1 ? "File" : "Files") + " uploaded successfully.";
            return response;
        }


        public GeneralResponse RenameFileOrFolder(int id, string name)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            try
            {
                EngineeringDocs engDocs = context.EngineeringDocs.FirstOrDefault(f => f.Id == id);
                if (engDocs == null)
                {
                    response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.InvalidRequest).ToString();
                    response.ResponseMessage = Enumerations.EngineeringDocEnum.InvalidRequest.GetStringValue();
                    return response;
                }

                var engDocsWithSameName = context.EngineeringDocs.FirstOrDefault(f => f.ParentId == engDocs.ParentId && f.Id != engDocs.Id && f.DocumentName == name);
                if (engDocsWithSameName != null)
                {
                    response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.InvalidRequest).ToString();
                    if (engDocsWithSameName.IsFolder)
                    {
                        response.ResponseMessage = "Folder name - " + name + " already exist!";
                    }
                    else
                    {
                        response.ResponseMessage = "file name - " + name + " already exist!";
                    }
                    return response;
                }
                string oldFileName = engDocs.DocumentName;
                engDocs.DocumentName = name;

                Misc misc = new Misc();
                string directoryPath = "";
                AWS.AWS aws = new AWS.AWS();
                if (engDocs.IsFolder)
                {
                    directoryPath = misc.GetS3FolderName("EngineeringDocs") + GetParentFolderPath(engDocs.Id);
                    aws.RenameS3Directory(directoryPath, name);
                }
                else
                {
                    directoryPath = misc.GetS3FolderName("EngineeringDocs") + GetParentFolderPath(engDocs.ParentId);
                    aws.RenameFile(directoryPath, oldFileName, name);
                }
                context.SaveChanges();

                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Success).ToString();
                response.ResponseMessage = (engDocs.IsFolder ? "Folder" : "File") + " name changed successfully.";
            }
            catch (Exception ex)
            {
                throw;
            }
            return response;
        }

        #endregion EngineeringDocs




        public List<RecursiveObject> FillRecursive(List<FlatObject> flatObjects, int parentId)
        {
            List<RecursiveObject> recursiveObjects = new List<RecursiveObject>();
            foreach (var item in flatObjects.Where(x => x.parentId.Equals(parentId)))
            {
                recursiveObjects.Add(new RecursiveObject
                {
                    label = item.label,
                    id = item.id,
                    parentId = item.parentId,
                    children = FillRecursive(flatObjects, item.id)

                });
            }
            return recursiveObjects;
        }

        public void test()
        {
            AWS.AWS obj = new AWS.AWS();
            //obj.Test();
            //obj.RenameFile();
            //obj.MoveAllFolder("GuardianTest/EngineeringDocs/96", "Atul");
            //obj.CreateFolderOns3("HelloTest", "GuardianTest/EngineeringDocs");
        }

        public List<RecursiveObject> GetFolderList(string partNo)
        {
            var context = new GuardianAvionicsEntities();
            var partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == partNo);
            var flatObjects = context.EngineeringDocs.Where(w => w.PartId == partNumberMaster.Id && w.IsFolder).Select(s => new FlatObject
            {
                label = s.DocumentName,
                id = s.Id,
                parentId = s.ParentId
            }).ToList();

            return FillRecursive(flatObjects, flatObjects.FirstOrDefault(f => f.parentId == 0).id);
        }

        public GeneralResponse MoveFileAndFolder(List<int> idList, int destinationId)
        {
            Misc misc = new Misc();
            GeneralResponse response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                List<EngineeringDocs> docList = context.EngineeringDocs.Where(w => idList.Contains(w.Id)).ToList();

                int parentId = docList.FirstOrDefault().ParentId;
                string sourcePath = misc.GetS3FolderName("EngineeringDocs") + GetParentFolderPath(parentId);
                string destinationPath = misc.GetS3FolderName("EngineeringDocs") + GetParentFolderPath(destinationId);
                AWS.AWS aws = new AWS.AWS();
                Dictionary<string, bool> dict = new Dictionary<string, bool>();
                docList.ForEach(f => dict.Add(f.DocumentName, f.IsFolder));
                response = aws.MoveFolder(sourcePath, destinationPath, dict);

                docList.ForEach(f => f.ParentId = destinationId);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = "Exception occurs while moving the folder.";
            }
            return response;
        }

        public GeneralResponse DeleteFileOrDirectory(int engDocId, bool isFolder)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                string sourcePath = new Misc().GetS3FolderName("EngineeringDocs") + GetParentFolderPath(engDocId);
                AWS.AWS aws = new AWS.AWS();
                response = aws.DeleteFileOrDirectory(sourcePath, isFolder);
                if (response.ResponseCode == "0")
                {
                    List<int> idList = new List<int>();
                    idList.Add(engDocId);
                    if (isFolder)
                    {
                        List<int> parentIdList = new List<int>();
                        parentIdList.Add(engDocId);
                        while (parentIdList.Count > 0)
                        {
                            parentIdList = context.EngineeringDocs.Where(w => parentIdList.Contains(w.ParentId)).Select(s => s.Id).ToList();
                            idList.AddRange(parentIdList);
                        }
                    }
                    context.EngineeringDocs.RemoveRange(context.EngineeringDocs.Where(w => idList.Contains(w.Id)).ToList());
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = "Exception occurs while deleting the folder!!!";
            }
            return response;
        }

        public List<KitAssemblyModel> GetKitAssembly()
        {
            var context = new GuardianAvionicsEntities();
            var kitAssemblyList = context.KitAssembly.OrderByDescending(o => o.Id).Select(s => new KitAssemblyModel
            {
                Id = s.Id,
                PartName = s.PartNumber,
                Description = s.Description,
                SuggestedSalesPrice = s.SuggestedSalesPrice ?? 0.00m,
                ImageName = s.KitImage,
                Comment = s.Comment,
                PartList = s.KitAssemblyItem.Select(p => new PartNumberModel
                {
                    Id = p.Id,
                    PartNumberId = p.PartNumberMaster.Id.ToString(),
                    PartName = p.PartNumberMaster.PartNumber,
                    ModelNumber = p.PartNumberMaster.ModelNumber,
                    Description = p.PartNumberMaster.Description,
                    IsSoftwareLevelApplicable = p.PartNumberMaster.IsSoftwareLevelApplicable ?? false,
                    CostPrice = p.PartNumberMaster.CostPrice ?? 0,
                    SalesPrice = p.PartNumberMaster.SalesPrice ?? 0,
                    RecommendQuantity = p.PartNumberMaster.RecommendQty ?? 0,
                    Comment = p.PartNumberMaster.Comment,
                    VendorName = p.PartNumberMaster.Vendor == null ? "" : p.PartNumberMaster.Vendor.Name,
                    Quantity = p.Quantity,
                    TotalUnitsAvailableForSale = 0
                }).ToList()
            }).ToList();

            List<int> idList = context.EngineeringDocs.Where(w => w.DocumentName == "Image").Select(s => s.Id).ToList();
            var partWithImageNameList = context.EngineeringDocs.Where(w => idList.Contains(w.ParentId) && !w.IsFolder).GroupBy(g => g.ParentId).Where(w => w.FirstOrDefault() != null).Select(s => new { partId = s.FirstOrDefault().PartId, ImageName = s.FirstOrDefault().DocumentName }).ToList();

            string kitPath = ConfigurationReader.s3BucketURL + ConfigurationReader.KitAssembly;
            string engDocPath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketEngineeringDocs;
            string image = "";
            foreach (var kitAssembly in kitAssemblyList)
            {
                kitAssembly.ImageName = string.IsNullOrEmpty(kitAssembly.ImageName) ? "" : kitPath + kitAssembly.ImageName;
                foreach (var kitItem in kitAssembly.PartList)
                {
                    if (partWithImageNameList.FirstOrDefault(f => f.partId == Convert.ToInt32(kitItem.PartNumberId)) != null)
                    {
                        image = partWithImageNameList.FirstOrDefault(f => f.partId == Convert.ToInt32(kitItem.PartNumberId)).ImageName;
                        kitItem.ImageName = string.IsNullOrEmpty(image) ? "../Images/NoImage.png" : engDocPath + kitItem.PartNumberId.ToString() + "/Image/" + image;
                    }
                }
            }
            kitAssemblyList.ForEach(f => f.PartList.ForEach(a => a.ImageName = string.IsNullOrEmpty(a.ImageName) ? "../Images/NoImage.png" : a.ImageName));
            return kitAssemblyList;
        }

        public GeneralResponse DeleteKitAssembly(int id)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            var kitAssembly = context.KitAssembly.FirstOrDefault(f => f.Id == id);
            if (kitAssembly == null)
            {
                response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.InValidDeleteRequest).ToString();
                response.ResponseMessage = Enumerations.KitAssemblyEnum.InValidDeleteRequest.GetStringValue();
                return response;
            }

            if (!string.IsNullOrEmpty(kitAssembly.KitImage))
            {
                new Misc().DeleteFileFromS3Bucket(kitAssembly.KitImage, "KitAssembly");
            }
            context.KitAssemblyItem.RemoveRange(context.KitAssemblyItem.Where(w => w.KitAssemblyId == kitAssembly.Id).ToList());
            context.KitAssembly.Remove(kitAssembly);
            context.SaveChanges();

            response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.Success).ToString();
            response.ResponseMessage = Enumerations.KitAssemblyEnum.Success.GetStringValue();
            return response;

        }

        public GeneralResponse UpdateKitAssembly(KitAssemblyModel kitAssemblyModel)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            var kitAssembly = context.KitAssembly.FirstOrDefault(f => f.PartNumber == kitAssemblyModel.PartName && f.Id != kitAssemblyModel.Id);
            if (kitAssembly != null)
            {
                response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.KitAssemblyAlreadyExist).ToString();
                response.ResponseMessage = string.Format(Enumerations.KitAssemblyEnum.KitAssemblyAlreadyExist.GetStringValue(), kitAssembly.PartNumber);
            }
            else
            {
                kitAssembly = context.KitAssembly.FirstOrDefault(f => f.Id == kitAssemblyModel.Id);
                string existingFileName = kitAssembly.KitImage;
                kitAssembly.PartNumber = kitAssemblyModel.PartName;
                kitAssembly.Description = kitAssemblyModel.Description;
                kitAssembly.SuggestedSalesPrice = kitAssemblyModel.SuggestedSalesPrice;
                kitAssembly.Comment = kitAssemblyModel.Comment;

                if (!string.IsNullOrEmpty(kitAssemblyModel.ImageName))
                {
                    kitAssembly.KitImage = kitAssemblyModel.ImageName;
                }
                context.SaveChanges();


                context.KitAssemblyItem.RemoveRange(context.KitAssemblyItem.Where(w => w.KitAssemblyId == kitAssemblyModel.Id).ToList());
                List<KitAssemblyItem> itemList = kitAssemblyModel.PartList.Select(s => new KitAssemblyItem
                {
                    KitAssemblyId = kitAssembly.Id,
                    KitPartId = s.Id,
                    Quantity = s.Quantity
                }).ToList();
                context.KitAssemblyItem.AddRange(itemList);
                context.SaveChanges();

                if (kitAssemblyModel.File != null)
                {
                    if (!string.IsNullOrEmpty(existingFileName))
                    {
                        new Misc().DeleteFileFromS3Bucket(existingFileName, "KitAssembly");
                    }
                    UploadLitAssemblyImage(kitAssemblyModel.File, kitAssemblyModel.ImageName);
                }
                response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.Success).ToString();
                response.ResponseMessage = Enumerations.KitAssemblyEnum.Success.GetStringValue();
            }
            return response;
        }

        public GeneralResponse AddKitAssembly(KitAssemblyModel kitAssemblyModel)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            var kitAssembly = context.KitAssembly.FirstOrDefault(f => f.PartNumber == kitAssemblyModel.PartName);
            try
            {
                if (kitAssembly != null)
                {
                    response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.KitAssemblyAlreadyExist).ToString();
                    response.ResponseMessage = string.Format(Enumerations.KitAssemblyEnum.KitAssemblyAlreadyExist.GetStringValue(), kitAssembly.PartNumber);
                }
                else
                {
                    kitAssembly = new KitAssembly
                    {
                        //PartId = kitAssemblyModel.PartId,
                        PartNumber = kitAssemblyModel.PartName,
                        Description = kitAssemblyModel.Description,
                        SuggestedSalesPrice = kitAssemblyModel.SuggestedSalesPrice,
                        KitImage = kitAssemblyModel.ImageName,
                        CreateDate = DateTime.UtcNow,
                        Comment = kitAssemblyModel.Comment
                    };
                    context.KitAssembly.Add(kitAssembly);
                    context.SaveChanges();


                    List<KitAssemblyItem> itemList = kitAssemblyModel.PartList.Select(s => new KitAssemblyItem
                    {
                        KitAssemblyId = kitAssembly.Id,
                        KitPartId = s.Id,
                        Quantity = s.Quantity
                    }).ToList();
                    context.KitAssemblyItem.AddRange(itemList);
                    context.SaveChanges();

                    if (kitAssemblyModel.File != null)
                    {
                        UploadLitAssemblyImage(kitAssemblyModel.File, kitAssemblyModel.ImageName);
                    }
                    response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.Success).ToString();
                    response.ResponseMessage = Enumerations.KitAssemblyEnum.Success.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.KitAssemblyEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.KitAssemblyEnum.Exception.GetStringValue();
            }
            return response;
        }

        public void UploadLitAssemblyImage(HttpPostedFile file, string filename)
        {
            S3BucketUpdates s3Updates = new S3BucketUpdates();

            s3Updates.SaveFileToS3BucketKitAssembly(filename, file.InputStream);
        }

        public KitAssemblyModel GetKitAssemblyById(int id)
        {
            var context = new GuardianAvionicsEntities();
            var kitAssemblyModel = GetKitAssembly().Where(w => w.Id == id).FirstOrDefault();
            return kitAssemblyModel;
        }

        public List<StatusRMA> GetStatusRMA()
        {
            var context = new GuardianAvionicsEntities();
            return context.StatusRMA.ToList();
        }

        public int GetMaxRMANo()
        {
            var context = new GuardianAvionicsEntities();
            return context.RMA.Max(m => m.Id);
        }

        public List<RMAStatusMOdel> GetStatusHistoryByRMAId(int id)
        {
            var context = new GuardianAvionicsEntities();


            List<RMAStatusMOdel> statusList = context.StatusRMA.Select(s => new RMAStatusMOdel
            {
                Status = s.Status,
                StatusId = s.Id,
                Date = "",
                Receiver = ""
            }).ToList();

            var historyList = context.RMAStatusHistory.Where(w => w.RMAId == id).Select(s => new
            {
                StatusId = s.StatusId,
                Date = s.StatusUpdateDate,
                Receiver = s.ReceiverId == null ? "" : s.InventoryReceiver.Name
            }).ToList();

            statusList.Join(historyList, (allStatus) => allStatus.StatusId, (history) => history.StatusId, (allStatus, history) =>
           {
               allStatus.Date = (history.Date == null) ? "" : history.Date.ToString("MM/dd/yyyy");
               allStatus.Receiver = history.Receiver;
               return allStatus;
           }).ToList();
            return statusList;
        }

        public List<RMAModel> GetRMAInChunk(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter, out int maxRMANo)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            maxRMANo = 1;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.RMA.Count();
            if (totalRecord > 0)
            {
                maxRMANo = context.RMA.Max(m => m.Id) + 1;
            }
            IQueryable<RMA> rmaList;
            rmaList = context.RMA;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                rmaList = rmaList.Where(w => w.InventoryCustomer.FirstName.ToLower().Contains(search) ||
                                                    w.InventoryCustomer.LastName.ToLower().Contains(search) ||
                                                    w.InventoryCustomer.Email.ToLower().Contains(search) ||
                                                    w.ModelNumber.ToLower().Contains(search) ||
                                                    w.SerialNumber.ToLower().Contains(search) ||
                                                    w.InventoryCustomer.PhoneNo.ToLower().Contains(search) ||
                                                    w.TrackingNumber.ToLower().Contains(search) ||
                                                    w.Id.ToString().Contains(search) ||
                                                    (w.InventoryCustomer.FirstName + " " + w.InventoryCustomer.LastName).Contains(search));
            }

            totalRowsAfterFilter = rmaList.Count();

            //Sorting
            switch (sortColumnName)
            {
                case "Id":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.Id).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "CreateDate":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.CreateDate).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.CreateDate).Skip(start).Take(length);
                        break;
                    }
                case "CustomerName":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => (o.InventoryCustomer.FirstName + " " + o.InventoryCustomer.LastName)).Skip(start).Take(length) : rmaList.OrderByDescending(o => (o.InventoryCustomer.FirstName + " " + o.InventoryCustomer.LastName)).Skip(start).Take(length);
                        break;
                    }
                case "Email":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.InventoryCustomer.Email).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.InventoryCustomer.Email).Skip(start).Take(length);
                        break;
                    }
                case "PhoneNo":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.InventoryCustomer.PhoneNo).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.InventoryCustomer.PhoneNo).Skip(start).Take(length);
                        break;
                    }
                case "ModelNo":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.ModelNumber).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.ModelNumber).Skip(start).Take(length);
                        break;
                    }
                case "Discrepancy":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.Discrepancy.DiscrepancyDetail).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.Discrepancy.DiscrepancyDetail).Skip(start).Take(length);
                        break;
                    }
                case "SerialNo":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.SerialNumber).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.SerialNumber).Skip(start).Take(length);
                        break;
                    }
                case "Status":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.StatusRMA.Status).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.StatusRMA.Status).Skip(start).Take(length);
                        break;
                    }
                case "Receiver":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.InventoryReceiver.Name).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.InventoryReceiver.Name).Skip(start).Take(length);
                        break;
                    }
                case "TrackingNo":
                    {
                        rmaList = sortDirection == "asc" ? rmaList.OrderBy(o => o.TrackingNumber).Skip(start).Take(length) : rmaList.OrderByDescending(o => o.TrackingNumber).Skip(start).Take(length);
                        break;
                    }
            }


            Misc misc = new Misc();

            var list = rmaList.ToList();
            //Paging
            List<RMAModel> response = list.Select(s => new RMAModel
            {
                CustomerName = s.InventoryCustomer.FirstName + " " + s.InventoryCustomer.LastName,
                SerialNo = s.SerialNumber,
                //DiscrepancyId = s.DiscrepancyId == null ? "" : misc.Encrypt(s.DiscrepancyId.ToString()),
                Discrepancy = s.DiscrepancyId == null ? "" : (s.Discrepancy.DiscrepancyDetail),
                Status = s.StatusRMA.Status,
                CreatedDate = s.CreateDate.ToString("MM/dd/yyyy"),
                Receiver = s.InventoryReceiver.Name,
                // ResolutionId = s.ResolutionId == null ? "" : misc.Encrypt(s.ResolutionId.ToString()),
                Resolution = s.ResolutionId == null ? "" : (s.RMAResolution.ResolutionDetail),
                TrackingNo = s.TrackingNumber,
                ModelNo = s.ModelNumber,
                // CustomerId = s.CustomerId.ToString(),
                // StatusId = s.StatusRMAId.ToString(),
                // ReceiverId = s.ReceiverId.ToString(),
                Id = s.Id,
                EmailId = s.InventoryCustomer.Email,
                PhoneNo = s.InventoryCustomer.PhoneNo,
                tId = misc.Encrypt(s.Id.ToString()),
                UnitCount = s.RMAUnit.Count() + s.AddonUnits.Count(),
                Comments = s.Comments
            }).ToList();
            return response;
        }

        public List<RMAModel> GetRMAList()
        {
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            var objRMAList = context.RMA.ToList();
            if (objRMAList.Count == 0)
            {
                return new List<RMAModel>();
            }
            List<RMAModel> rmaList = objRMAList.Select(s => new RMAModel
            {
                CustomerName = s.InventoryCustomer.FirstName + " " + s.InventoryCustomer.LastName,
                SerialNo = s.SerialNumber,
                DiscrepancyId = s.DiscrepancyId == null ? "" : misc.Encrypt(s.DiscrepancyId.ToString()),
                Discrepancy = s.DiscrepancyId == null ? "" : (s.Discrepancy.DiscrepancyDetail),
                Status = s.StatusRMA.Status,
                CreateDate = s.CreateDate,
                Receiver = s.InventoryReceiver.Name,
                ResolutionId = s.ResolutionId == null ? "" : misc.Encrypt(s.ResolutionId.ToString()),
                Resolution = s.ResolutionId == null ? "" : (s.RMAResolution.ResolutionDetail),
                TrackingNo = s.TrackingNumber,
                ModelNo = s.ModelNumber,
                CustomerId = s.CustomerId.ToString(),
                StatusId = s.StatusRMAId.ToString(),
                ReceiverId = s.ReceiverId.ToString(),
                Id = s.Id,
                EmailId = s.InventoryCustomer.Email,
                PhoneNo = s.InventoryCustomer.PhoneNo
            }).ToList();
            rmaList.ForEach(f => { f.StatusId = misc.Encrypt(f.StatusId); f.ReceiverId = misc.Encrypt(f.ReceiverId); f.CustomerId = misc.Encrypt(f.CustomerId); f.tId = misc.Encrypt(f.Id.ToString()); });
            return rmaList;
        }

        public RMAModel GetRMADetailsById(int id)
        {
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            RMA objRM = context.RMA.FirstOrDefault(f => f.Id == id);
            RMAModel rmaModel = new RMAModel
            {
                CustomerName = objRM.InventoryCustomer.FirstName + " " + objRM.InventoryCustomer.LastName,
                CompanyName = objRM.InventoryCustomer.CompanyName ?? "",
                SerialNo = objRM.SerialNumber,
                Description = objRM.Description,
                DiscrepancyId = objRM.DiscrepancyId == null ? "" : misc.Encrypt(objRM.DiscrepancyId.ToString()),
                Discrepancy = objRM.DiscrepancyId == null ? "" : (objRM.Discrepancy.DiscrepancyDetail),
                Status = objRM.StatusRMA.Status,
                CreateDate = objRM.CreateDate,
                Receiver = objRM.InventoryReceiver.Name,
                ResolutionId = objRM.ResolutionId == null ? "" : misc.Encrypt(objRM.ResolutionId.ToString()),
                Resolution = objRM.ResolutionId == null ? "" : (objRM.RMAResolution.ResolutionDetail),
                TrackingNo = objRM.TrackingNumber,
                ModelNo = objRM.ModelNumber,
                CustomerId = misc.Encrypt(objRM.CustomerId.ToString()),
                StatusId = misc.Encrypt(objRM.StatusRMAId.ToString()),
                ReceiverId = misc.Encrypt(objRM.ReceiverId.ToString()),
                tId = misc.Encrypt(objRM.Id.ToString()),
                Id = objRM.Id,
                EmailId = objRM.InventoryCustomer.Email,
                PhoneNo = objRM.InventoryCustomer.PhoneNo,
                Comments = objRM.Comments,
                UnitModelList = GetAllUnitsByRMAId(id)
            };
            return rmaModel;
        }



        public GeneralResponse EditRMA(RMAModel rmaModel)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                var rma = context.RMA.FirstOrDefault(f => f.Id == rmaModel.Id);

                int statusId = Convert.ToInt32(rmaModel.StatusId);
                var rmaStatus = context.StatusRMA.FirstOrDefault(f => f.Id == statusId);
                if (rmaStatus != null && !string.IsNullOrEmpty(rmaModel.SerialNo))
                {
                    List<Int32> serialNoList = rmaModel.SerialNo.Split(',').Select(s => Convert.ToInt32(s)).ToList();
                    if (rmaStatus.UnitStatusId != null)
                    {
                        var unit = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();
                        if (unit != null)
                        {
                            unit.ForEach(f => f.UnitCurrentStatusId = (rmaStatus.UnitStatusId ?? 0));
                            //unit.UnitCurrentStatusId = rmaStatus.UnitStatusId ?? 0;
                            context.SaveChanges();
                        }
                    }
                }

                if (rma.StatusRMAId != Convert.ToInt32(rmaModel.StatusId))
                {
                    RMAStatusHistory statusHistory = context.RMAStatusHistory.Create();
                    statusHistory.RMAId = rma.Id;
                    statusHistory.ReceiverId = Convert.ToInt32(rmaModel.ReceiverId);
                    statusHistory.StatusId = Convert.ToInt32(rmaModel.StatusId);
                    statusHistory.StatusUpdateDate = DateTime.UtcNow;
                    context.RMAStatusHistory.Add(statusHistory);
                }
                rma.DiscrepancyId = string.IsNullOrEmpty(rmaModel.DiscrepancyId) ? (int?)null : Convert.ToInt32(rmaModel.DiscrepancyId);
                rma.ResolutionId = string.IsNullOrEmpty(rmaModel.ResolutionId) ? (int?)null : Convert.ToInt32(rmaModel.ResolutionId);
                rma.Description = rmaModel.Description;
                rma.CustomerId = Convert.ToInt32(rmaModel.CustomerId);
                rma.ModelNumber = rmaModel.ModelNo;
                rma.ReceiverId = Convert.ToInt32(rmaModel.ReceiverId);
                rma.SerialNumber = rmaModel.SerialNo;
                rma.StatusRMAId = Convert.ToInt32(rmaModel.StatusId);
                rma.TrackingNumber = rmaModel.TrackingNo;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Exception occurs while updating the data."
                };
            }
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }

        public GeneralResponse AddRMA(RMAModel rmaModel, List<SelectListItem> unitAndRMAStatusList, List<UnitModel> newSerialNumberList)
        {

            var context = new GuardianAvionicsEntities();
            RMA rma = context.RMA.Create();
            rma.CreateDate = DateTime.UtcNow;
            rma.CustomerId = Convert.ToInt32(rmaModel.CustomerId);
            rma.DiscrepancyId = string.IsNullOrEmpty(rmaModel.DiscrepancyId) ? (int?)null : Convert.ToInt32(rmaModel.DiscrepancyId);
            rma.ResolutionId = string.IsNullOrEmpty(rmaModel.ResolutionId) ? (int?)null : Convert.ToInt32(rmaModel.ResolutionId);
            rma.ReceiverId = Convert.ToInt32(rmaModel.ReceiverId);
            rma.StatusRMAId = 1;
            rma.TrackingNumber = rmaModel.TrackingNo;
            rma.Comments = rmaModel.Comments;
            context.RMA.Add(rma);

            context.SaveChanges();

            if (unitAndRMAStatusList != null)
            {
                List<int> serialNoList = unitAndRMAStatusList.Select(s => Convert.ToInt32(s.Text)).ToList();
                List<UnitSerialNumber> unitList = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();
                List<RMAUnit> rmaUnitList = unitAndRMAStatusList.Select(s => new RMAUnit
                {
                    RMAId = rma.Id,
                    RMAStatusId = Convert.ToInt32(s.Value),
                    UnitId = unitList.FirstOrDefault(f => f.SerialNumber == (int?)(Convert.ToInt32(s.Text))).Id
                }).ToList();

                context.RMAUnit.AddRange(rmaUnitList);
                context.SaveChanges();

                var rmaList = context.RMAUnit.Where(w => w.RMAId == rma.Id && w.StatusRMA.UnitStatusId != null).ToList();
                //rmaList.ForEach(f => f.UnitSerialNumber.UnitCurrentStatusId = f.StatusRMA.UnitStatusId ?? 0);
                var rmaStatusList = context.StatusRMA.ToList();
                foreach (var rmaUnit in rmaList)
                {
                    if (rmaStatusList.FirstOrDefault(f => f.Id == rmaUnit.RMAStatusId).UnitStatusId != null)
                    {

                        context.UnitSerialNumbers.FirstOrDefault(f => f.Id == rmaUnit.UnitId).UnitCurrentStatusId = rmaStatusList.FirstOrDefault(f => f.Id == rmaUnit.RMAStatusId).UnitStatusId ?? 0;
                        context.SaveChanges();
                    }
                }
                context.SaveChanges();
            }
            if (newSerialNumberList != null)
            {
                //Now add addons unit
                List<AddonUnits> addonUnitList = newSerialNumberList.Select(s => new AddonUnits
                {
                    Description = s.Description,
                    PartNumber = s.PartNumber,
                    RMAId = rma.Id,
                    RMAStatusId = s.RMAStatusId,
                    SerialNumber = s.SerialNumber ?? 0
                }).ToList();
                context.AddonUnits.AddRange(addonUnitList);
                context.SaveChanges();
            }
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "RMA record added successfully."
            };
        }



        public GeneralResponse EditRMA(RMAModel rmaModel, List<SelectListItem> unitAndRMAStatusList, List<UnitModel> newSerialNumberList)
        {
            var context = new GuardianAvionicsEntities();
            var rma = context.RMA.FirstOrDefault(f => f.Id == rmaModel.Id);
            rma.DiscrepancyId = string.IsNullOrEmpty(rmaModel.DiscrepancyId) ? (int?)null : Convert.ToInt32(rmaModel.DiscrepancyId);
            rma.ResolutionId = string.IsNullOrEmpty(rmaModel.ResolutionId) ? (int?)null : Convert.ToInt32(rmaModel.ResolutionId);
            rma.Description = rmaModel.Description;
            rma.CustomerId = Convert.ToInt32(rmaModel.CustomerId);
            rma.ReceiverId = Convert.ToInt32(rmaModel.ReceiverId);
            rma.TrackingNumber = rmaModel.TrackingNo;
            rma.Comments = rmaModel.Comments;
            context.SaveChanges();

            context.RMAUnit.RemoveRange(context.RMAUnit.Where(w => w.RMAId == rmaModel.Id).ToList());
            context.AddonUnits.RemoveRange(context.AddonUnits.Where(w => w.RMAId == rmaModel.Id).ToList());
            context.SaveChanges();

            if (unitAndRMAStatusList != null)
            {
                List<int> serialNoList = unitAndRMAStatusList.Select(s => Convert.ToInt32(s.Text)).ToList();
                List<UnitSerialNumber> unitList = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();
                List<RMAUnit> rmaUnitList = unitAndRMAStatusList.Select(s => new RMAUnit
                {
                    RMAId = rma.Id,
                    RMAStatusId = Convert.ToInt32(s.Value),
                    UnitId = unitList.FirstOrDefault(f => f.SerialNumber == (int?)(Convert.ToInt32(s.Text))).Id
                }).ToList();

                context.RMAUnit.AddRange(rmaUnitList);
                context.SaveChanges();

                var rmaList = context.RMAUnit.Where(w => w.RMAId == rma.Id && w.StatusRMA.UnitStatusId != null).ToList();
                //rmaList.ForEach(f => f.UnitSerialNumber.UnitCurrentStatusId = f.StatusRMA.UnitStatusId ?? 0);
                var rmaStatusList = context.StatusRMA.ToList();
                foreach (var rmaUnit in rmaList)
                {
                    if (rmaStatusList.FirstOrDefault(f => f.Id == rmaUnit.RMAStatusId).UnitStatusId != null)
                    {

                        context.UnitSerialNumbers.FirstOrDefault(f => f.Id == rmaUnit.UnitId).UnitCurrentStatusId = rmaStatusList.FirstOrDefault(f => f.Id == rmaUnit.RMAStatusId).UnitStatusId ?? 0;
                        context.SaveChanges();
                    }
                }
                context.SaveChanges();
            }

            if (newSerialNumberList != null)
            {
                List<AddonUnits> addonUnitList = newSerialNumberList.Select(s => new AddonUnits
                {
                    Description = s.Description,
                    PartNumber = s.PartNumber,
                    RMAId = rma.Id,
                    RMAStatusId = s.RMAStatusId,
                    SerialNumber = s.SerialNumber ?? 0
                }).ToList();
                context.AddonUnits.AddRange(addonUnitList);
                context.SaveChanges();
            }

            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "RMA record updated successfully."
            };
        }

        public GeneralResponse AddRMA(RMAModel rmaModel)
        {
            try
            {
                var context = new GuardianAvionicsEntities();

                int statusId = Convert.ToInt32(rmaModel.StatusId);
                var rmaStatus = context.StatusRMA.FirstOrDefault(f => f.Id == statusId);
                if (rmaStatus != null && !string.IsNullOrEmpty(rmaModel.SerialNo))
                {
                    List<Int32> serialNoList = rmaModel.SerialNo.Split(',').Select(s => Convert.ToInt32(s)).ToList();


                    // int serialNo = Convert.ToInt32(rmaModel.SerialNo);
                    if (rmaStatus.UnitStatusId != null)
                    {
                        var unit = context.UnitSerialNumbers.Where(w => serialNoList.Contains(w.SerialNumber ?? 0)).ToList();
                        if (unit != null)
                        {
                            unit.ForEach(f => f.UnitCurrentStatusId = (rmaStatus.UnitStatusId ?? 0));
                            //unit.UnitCurrentStatusId = rmaStatus.UnitStatusId ?? 0;
                            context.SaveChanges();
                        }
                    }
                }

                RMA rma = context.RMA.Create();
                rma.CreateDate = DateTime.UtcNow;
                rma.Description = rmaModel.Description;
                rma.CustomerId = Convert.ToInt32(rmaModel.CustomerId);
                rma.DiscrepancyId = string.IsNullOrEmpty(rmaModel.DiscrepancyId) ? (int?)null : Convert.ToInt32(rmaModel.DiscrepancyId);
                rma.ResolutionId = string.IsNullOrEmpty(rmaModel.ResolutionId) ? (int?)null : Convert.ToInt32(rmaModel.ResolutionId);
                rma.ModelNumber = rmaModel.ModelNo;
                rma.ReceiverId = Convert.ToInt32(rmaModel.ReceiverId);
                rma.SerialNumber = rmaModel.SerialNo;
                rma.StatusRMAId = Convert.ToInt32(rmaModel.StatusId);
                rma.TrackingNumber = rmaModel.TrackingNo;
                context.RMA.Add(rma);
                context.SaveChanges();

                RMAStatusHistory statusHistory = context.RMAStatusHistory.Create();
                statusHistory.RMAId = rma.Id;
                statusHistory.StatusId = rma.StatusRMAId;
                statusHistory.StatusUpdateDate = DateTime.UtcNow;
                context.RMAStatusHistory.Add(statusHistory);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Exception occurs while inserting the record."
                };
            }
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }

        public GeneralResponse deleteRMA(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                context.RMAStatusHistory.RemoveRange(context.RMAStatusHistory.Where(w => w.RMAId == id).ToList());
                context.RMA.Remove(context.RMA.FirstOrDefault(f => f.Id == id));
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Exception occurs while deleting the record."
                };
            }
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }

        public List<UnitModel> GetAllUnitsByRMAId(int rmaId)
        {
            List<UnitModel> unitModelList = new List<UnitModel>();
            var context = new GuardianAvionicsEntities();
            var rmaUnitList = context.RMAUnit.Where(w => w.RMAId == rmaId).ToList();
            foreach (var rmaunit in rmaUnitList)
            {
                unitModelList.Add(new UnitModel
                {
                    PurchaseOrderNumber = rmaunit.UnitSerialNumber.PurchaseOrder.PurchaseOrderNumber,
                    PartNumber = rmaunit.UnitSerialNumber.PurchaseOrder.PartNumberMaster.PartNumber,
                    Description = rmaunit.UnitSerialNumber.PurchaseOrder.PartNumberMaster.Description,
                    SerialNumber = rmaunit.UnitSerialNumber.SerialNumber,
                    UnitStatus = rmaunit.UnitSerialNumber.UnitStatus.Status,
                    Id = rmaunit.UnitSerialNumber.Id,
                    RMAStatusId = rmaunit.RMAStatusId,
                    OldOrNewUnit = "OldSerialNo"
                });
            }
            var addonUnitList = context.AddonUnits.Where(w => w.RMAId == rmaId).ToList();
            foreach (var addonUnit in addonUnitList)
            {
                unitModelList.Add(new UnitModel
                {
                    PartNumber = addonUnit.PartNumber,
                    Description = addonUnit.Description,
                    SerialNumber = addonUnit.SerialNumber,
                    UnitStatus = addonUnit.StatusRMA == null ? "Sold" : (addonUnit.StatusRMA.UnitStatusId == null ? "Sold" : addonUnit.StatusRMA.UnitStatus.Status),
                    RMAStatusId = addonUnit.RMAStatusId ?? 0,
                    OldOrNewUnit = "NewSerialNo"
                });
            }

            return unitModelList;
        }

        public List<InventoryCustomerModel> SearchInventoryCustomer(string value)
        {
            var context = new GuardianAvionicsEntities();
            List<InventoryCustomerModel> response = context.InventoryCustomer.Where(w => w.FirstName.Contains(value) || w.LastName.Contains(value) || (w.FirstName + " " + w.LastName).ToString().Contains(value) || w.CompanyName.Contains(value)).Select(s => new InventoryCustomerModel
            {
                FirstName = string.IsNullOrEmpty(s.FirstName) ? "" : s.FirstName.Trim(),
                LastName = string.IsNullOrEmpty(s.LastName) ? "" : s.LastName.Trim(),
                Email = s.Email,
                Id = s.Id,
                PhoneNo = s.PhoneNo,
                CompanyName = string.IsNullOrEmpty(s.CompanyName) ? "" : s.CompanyName
            }).Take(10).ToList();
            Misc misc = new Misc();
            response.ForEach(f => f.EncryptId = misc.Encrypt(f.Id.ToString()));
            return response;
        }

        public List<InventoryCustomerModel> GetInventoryCustomers()
        {
            var context = new GuardianAvionicsEntities();
            List<InventoryCustomerModel> response = context.InventoryCustomer.Select(s => new InventoryCustomerModel
            {
                CustomerTypeId = s.CustomerTypeId == null ? "" : s.CustomerTypeId.ToString(),
                CustomerType = s.CustomerTypeId == null ? "" : s.CustomerType.Name,
                FirstName = string.IsNullOrEmpty(s.FirstName) ? "" : s.FirstName.Trim(),
                LastName = string.IsNullOrEmpty(s.LastName) ? "" : s.LastName.Trim(),
                Email = s.Email,
                PhoneNo = s.PhoneNo,
                Id = s.Id,
                CompanyName = string.IsNullOrEmpty(s.CompanyName) ? "" : s.CompanyName
            }).ToList();
            Misc misc = new Misc();
            response.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.CustomerTypeId = (f.CustomerTypeId == "") ? "" : misc.Encrypt(f.CustomerTypeId.ToString()); });
            return response;
        }

        public InventoryCustomerModel GetInventoryCustomerById(int id)
        {
            var context = new GuardianAvionicsEntities();
            InventoryCustomerModel custModel = new InventoryCustomerModel();
            Misc misc = new Misc();
            InventoryCustomer invCustomer = context.InventoryCustomer.FirstOrDefault(f => f.Id == id);
            custModel.CustomerTypeId = invCustomer.CustomerTypeId == null ? "" : misc.Encrypt(invCustomer.CustomerTypeId.ToString());
            custModel.CustomerType = invCustomer.CustomerTypeId == null ? "" : invCustomer.CustomerType.Name;
            custModel.FirstName = string.IsNullOrEmpty(invCustomer.FirstName) ? "" : invCustomer.FirstName.Trim();
            custModel.LastName = string.IsNullOrEmpty(invCustomer.LastName) ? "" : invCustomer.LastName.Trim();
            custModel.Email = invCustomer.Email;
            custModel.PhoneNo = invCustomer.PhoneNo;
            custModel.CompanyName = string.IsNullOrEmpty(invCustomer.CompanyName) ? "" : invCustomer.CompanyName;
            custModel.EncryptId = misc.Encrypt(invCustomer.Id.ToString());
            return custModel;
        }


        public List<InventoryCustomerModel> GetInventoryCustomersByTypeId(int customerTypeId)
        {
            var context = new GuardianAvionicsEntities();
            List<InventoryCustomerModel> response = context.InventoryCustomer.Where(w => w.CustomerTypeId == customerTypeId).Select(s => new InventoryCustomerModel
            {
                CustomerType = s.CustomerTypeId == null ? "" : s.CustomerType.Name,
                FirstName = s.FirstName,
                LastName = s.LastName,
                Email = s.Email,
                PhoneNo = s.PhoneNo,
                Id = s.Id,
                CompanyName = s.CompanyName
            }).ToList();
            Misc misc = new Misc();
            response.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; });
            return response;
        }

        public GeneralResponse DeleteInventoryCustomer(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                var customer = context.InventoryCustomer.FirstOrDefault(f => f.Id == id);
                if (customer == null)
                {
                    response.ResponseCode = ((int)Enumerations.Customer.InvalidRequestToDelete).ToString();
                    response.ResponseMessage = Enumerations.Customer.InvalidRequestToDelete.GetStringValue();
                    return response;
                }

                var rma = context.RMA.FirstOrDefault(f => f.CustomerId == customer.Id);
                if (rma != null)
                {
                    response.ResponseCode = ((int)Enumerations.Customer.CannotDeleteCustomer_RMALinked).ToString();
                    response.ResponseMessage = Enumerations.Customer.CannotDeleteCustomer_RMALinked.GetStringValue();
                    return response;
                }
                context.InventoryCustomer.Remove(customer);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Customer.Exception).ToString();
                response.ResponseMessage = Enumerations.Customer.Exception.GetStringValue();
            }
            response.ResponseCode = ((int)Enumerations.Customer.Success).ToString();
            response.ResponseMessage = Enumerations.Customer.Success.GetStringValue();
            return response;
        }

        public GeneralResponse EditInventoryCustomer(InventoryCustomerModel inventoryCustomerModel)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                InventoryCustomer invCustomer = context.InventoryCustomer.FirstOrDefault(f => f.Id == inventoryCustomerModel.Id);
                if (invCustomer == null)
                {
                    response.ResponseCode = ((int)Enumerations.Customer.InvalidRequestToEdit).ToString();
                    response.ResponseMessage = Enumerations.Customer.InvalidRequestToEdit.GetStringValue();
                    return response;
                }

                string namewithemailid = inventoryCustomerModel.FirstName.Trim() + " " + inventoryCustomerModel.LastName.Trim() + " " + inventoryCustomerModel.Email.Trim();
                InventoryCustomer inventoryCustomer = context.InventoryCustomer.FirstOrDefault(f => f.Id != inventoryCustomerModel.Id && (f.FirstName.Trim() + " " + f.LastName.Trim() + " " + f.Email.Trim()) == namewithemailid);
                if (inventoryCustomer != null)
                {
                    response.ResponseCode = "9999";
                    response.ResponseMessage = "Customer record already exist.";
                    return response;
                }

                invCustomer.CustomerTypeId = (string.IsNullOrEmpty(inventoryCustomerModel.CustomerTypeId)) ? (int?)null : Convert.ToInt32(inventoryCustomerModel.CustomerTypeId);
                invCustomer.FirstName = inventoryCustomerModel.FirstName.Trim();
                invCustomer.LastName = inventoryCustomerModel.LastName.Trim();
                invCustomer.Email = inventoryCustomerModel.Email.Trim();
                invCustomer.PhoneNo = inventoryCustomerModel.PhoneNo;
                invCustomer.CompanyName = inventoryCustomerModel.CompanyName;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception occurs while updating the record.";
            }
            response.ResponseCode = ((int)Enumerations.Customer.Success).ToString();
            response.ResponseMessage = Enumerations.Customer.Success.GetStringValue();
            return response;
        }

        public GeneralResponse AddInventoryCustomer(InventoryCustomerModel inventoryCustomerModel)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                string namewithemailid = inventoryCustomerModel.FirstName.Trim() + " " + inventoryCustomerModel.LastName.Trim() + " " + inventoryCustomerModel.Email.Trim();
                InventoryCustomer inventoryCustomer = context.InventoryCustomer.FirstOrDefault(f => (f.FirstName.Trim() + " " + f.LastName.Trim() + " " + f.Email.Trim()) == namewithemailid);
                if (inventoryCustomer != null)
                {
                    response.ResponseCode = "9999";
                    response.ResponseMessage = "Customer record already exist.";
                    return response;
                }

                InventoryCustomer invCustomer = context.InventoryCustomer.Create();
                invCustomer.CustomerTypeId = (string.IsNullOrEmpty(inventoryCustomerModel.CustomerTypeId)) ? (int?)null : Convert.ToInt32(inventoryCustomerModel.CustomerTypeId);
                invCustomer.FirstName = inventoryCustomerModel.FirstName.Trim();
                invCustomer.LastName = inventoryCustomerModel.LastName.Trim();
                invCustomer.Email = inventoryCustomerModel.Email.Trim();
                invCustomer.PhoneNo = inventoryCustomerModel.PhoneNo;
                invCustomer.CompanyName = inventoryCustomerModel.CompanyName ?? "";
                context.InventoryCustomer.Add(invCustomer);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception occurs while inserting the record.";
            }
            response.ResponseCode = "0";
            response.ResponseMessage = "Record inserted successfully.";
            return response;
        }

        public List<DiscrepancyModel> GetDiscrepancyList()
        {
            var context = new GuardianAvionicsEntities();
            List<DiscrepancyModel> discrepancyList = context.Discrepancy.OrderByDescending(o => o.Id).Select((s) => new DiscrepancyModel
            {
                Id = s.Id,
                DiscrepancyDetail = s.DiscrepancyDetail
            }).ToList();
            Misc misc = new Misc();
            int SNO = discrepancyList.Count;
            discrepancyList.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; f.SNO = SNO--; });
            return discrepancyList;
        }

        public GeneralResponse AddDiscrepancy(string discrepancyDetail)
        {

            var response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                Discrepancy discrepancy = context.Discrepancy.FirstOrDefault(f => f.DiscrepancyDetail == discrepancyDetail);
                if (discrepancy != null)
                {
                    response.ResponseCode = ((int)Enumerations.Discrepancy.DiscrepancyAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.Discrepancy.DiscrepancyAlreadyExist.GetStringValue();
                    return response;
                }
                discrepancy = new Discrepancy();
                discrepancy.DiscrepancyDetail = discrepancyDetail;
                context.Discrepancy.Add(discrepancy);
                context.SaveChanges();
                response.ResponseCode = ((int)Enumerations.Discrepancy.Success).ToString();
                response.ResponseMessage = Enumerations.Discrepancy.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Discrepancy.Exception).ToString();
                response.ResponseMessage = Enumerations.Discrepancy.Exception.GetStringValue();
                return response;
            }
            return response;
        }

        public GeneralResponse EditDiscrepancy(string discrepancyDetail, int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var discrepancy = context.Discrepancy.FirstOrDefault(f => f.Id != id && f.DiscrepancyDetail == discrepancyDetail);
                if (discrepancy == null)
                {
                    discrepancy = context.Discrepancy.FirstOrDefault(f => f.Id == id);
                    if (discrepancy != null)
                    {
                        discrepancy.DiscrepancyDetail = discrepancyDetail;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Discrepancy.Success).ToString(),
                            ResponseMessage = Enumerations.Discrepancy.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Discrepancy.InvalidRequestToEdit).ToString(),
                            ResponseMessage = Enumerations.Discrepancy.InvalidRequestToEdit.GetStringValue()
                        };
                    }
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Discrepancy.DiscrepancyAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Discrepancy.DiscrepancyAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Discrepancy.Exception).ToString(),
                    ResponseMessage = Enumerations.Discrepancy.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse DeleteDiscrepancy(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var discrepancy = context.Discrepancy.FirstOrDefault(f => f.Id == id);
                if (discrepancy != null)
                {
                    context.Discrepancy.Remove(discrepancy);
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Discrepancy.Success).ToString(),
                        ResponseMessage = Enumerations.Discrepancy.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Discrepancy.InvalidRequestToDelete).ToString(),
                        ResponseMessage = Enumerations.Discrepancy.InvalidRequestToDelete.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Discrepancy.Exception).ToString(),
                    ResponseMessage = Enumerations.Discrepancy.Exception.GetStringValue()
                };
            }
            return response;
        }

        public List<ResolutionModel> GetResolutionList()
        {
            var context = new GuardianAvionicsEntities();
            List<ResolutionModel> resolutionList = context.RMAResolution.OrderByDescending(o => o.Id).Select((s) => new ResolutionModel
            {
                Id = s.Id,
                ResolutionDetail = s.ResolutionDetail
            }).ToList();
            Misc misc = new Misc();
            int SNO = resolutionList.Count;
            resolutionList.ForEach(f => { f.EncryptId = misc.Encrypt(f.Id.ToString()); f.Id = 0; f.SNO = SNO--; });
            return resolutionList;
        }

        public GeneralResponse AddResolution(string resolutionDetail)
        {

            var response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                RMAResolution resolution = context.RMAResolution.FirstOrDefault(f => f.ResolutionDetail == resolutionDetail);
                if (resolution != null)
                {
                    response.ResponseCode = ((int)Enumerations.Resolution.ResolutionAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.Resolution.ResolutionAlreadyExist.GetStringValue();
                    return response;
                }
                resolution = new RMAResolution();
                resolution.ResolutionDetail = resolutionDetail;
                context.RMAResolution.Add(resolution);
                context.SaveChanges();
                response.ResponseCode = ((int)Enumerations.Resolution.Success).ToString();
                response.ResponseMessage = Enumerations.Resolution.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.Resolution.Exception).ToString();
                response.ResponseMessage = Enumerations.Resolution.Exception.GetStringValue();
                return response;
            }
            return response;
        }

        public GeneralResponse EditResolution(string resolutionDetail, int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var resolution = context.RMAResolution.FirstOrDefault(f => f.Id != id && f.ResolutionDetail == resolutionDetail);
                if (resolution == null)
                {
                    resolution = context.RMAResolution.FirstOrDefault(f => f.Id == id);
                    if (resolution != null)
                    {
                        resolution.ResolutionDetail = resolutionDetail;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Resolution.Success).ToString(),
                            ResponseMessage = Enumerations.Resolution.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Resolution.InvalidRequestToEdit).ToString(),
                            ResponseMessage = Enumerations.Resolution.InvalidRequestToEdit.GetStringValue()
                        };
                    }
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Resolution.ResolutionAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.Resolution.ResolutionAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Resolution.Exception).ToString(),
                    ResponseMessage = Enumerations.Resolution.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse DeleteResolution(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var resolution = context.RMAResolution.FirstOrDefault(f => f.Id == id);
                if (resolution != null)
                {
                    context.RMAResolution.Remove(resolution);
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Resolution.Success).ToString(),
                        ResponseMessage = Enumerations.Resolution.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Resolution.InvalidRequestToDelete).ToString(),
                        ResponseMessage = Enumerations.Resolution.InvalidRequestToDelete.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Resolution.Exception).ToString(),
                    ResponseMessage = Enumerations.Resolution.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse AddRMAStatus(string status, int unitStatusId)
        {
            var response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                StatusRMA statusRMA = context.StatusRMA.FirstOrDefault(f => f.Status == status);
                if (statusRMA != null)
                {
                    response.ResponseCode = ((int)Enumerations.EnumRMAStatus.RMAStatusAlreadyExist).ToString();
                    response.ResponseMessage = Enumerations.EnumRMAStatus.RMAStatusAlreadyExist.GetStringValue();
                    return response;
                }
                statusRMA = new StatusRMA();
                statusRMA.Status = status;
                statusRMA.UnitStatusId = unitStatusId == 0 ? (int?)null : unitStatusId;
                context.StatusRMA.Add(statusRMA);
                context.SaveChanges();
                response.ResponseCode = ((int)Enumerations.EnumRMAStatus.Success).ToString();
                response.ResponseMessage = Enumerations.EnumRMAStatus.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EnumRMAStatus.Exception).ToString();
                response.ResponseMessage = Enumerations.EnumRMAStatus.Exception.GetStringValue();
                return response;
            }
            return response;
        }

        public GeneralResponse EditRMAStatus(string status, int id, int unitStatusId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var statusRMA = context.StatusRMA.FirstOrDefault(f => f.Id != id && f.Status == status);
                if (statusRMA == null)
                {
                    statusRMA = context.StatusRMA.FirstOrDefault(f => f.Id == id);
                    if (statusRMA != null)
                    {
                        statusRMA.Status = status;
                        statusRMA.UnitStatusId = unitStatusId == 0 ? (int?)null : unitStatusId;
                        context.SaveChanges();
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.EnumRMAStatus.Success).ToString(),
                            ResponseMessage = Enumerations.EnumRMAStatus.Success.GetStringValue()
                        };
                    }
                    else
                    {
                        response = new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.EnumRMAStatus.InvalidRequestToEdit).ToString(),
                            ResponseMessage = Enumerations.EnumRMAStatus.InvalidRequestToEdit.GetStringValue()
                        };
                    }
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.EnumRMAStatus.RMAStatusAlreadyExist).ToString(),
                        ResponseMessage = Enumerations.EnumRMAStatus.RMAStatusAlreadyExist.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.EnumRMAStatus.Exception).ToString(),
                    ResponseMessage = Enumerations.EnumRMAStatus.Exception.GetStringValue()
                };
            }
            return response;
        }

        public GeneralResponse DeleteRMAStatus(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response;
            try
            {
                var statusRMA = context.StatusRMA.FirstOrDefault(f => f.Id == id);
                if (statusRMA != null)
                {
                    context.StatusRMA.Remove(statusRMA);
                    context.SaveChanges();
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.EnumRMAStatus.Success).ToString(),
                        ResponseMessage = Enumerations.EnumRMAStatus.Success.GetStringValue()
                    };
                }
                else
                {
                    response = new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.EnumRMAStatus.InvalidRequestToDelete).ToString(),
                        ResponseMessage = Enumerations.EnumRMAStatus.InvalidRequestToDelete.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                response = new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.EnumRMAStatus.Exception).ToString(),
                    ResponseMessage = Enumerations.EnumRMAStatus.Exception.GetStringValue()
                };
            }
            return response;
        }


        #region GavData
        public GAVDocument GetGAVDocs(int docId)
        {

            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            GAVDocument engDoc = GetAllGAVDocsById(docId);
            return engDoc;

        }

        public GAVDocument GetAllGAVDocsById(int docId)
        {
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            GAVDocument gavDoc = new GAVDocument
            {
                documentList = new List<GavDocsModel>(),
                parentFolderList = new List<GavDocsListItem>()
            };
            List<GAVBigDog> docList = context.GAVBigDog.Where(w => w.ParentId == docId).ToList();
            string folderPath = GetParentFolderPathForGavData(docId);
            gavDoc.documentList = docList.Select(s => new GavDocsModel
            {
                Id = misc.Encrypt(s.Id.ToString()),
                DocumentName = s.DocumentName,
                IsFolder = s.IsFolder,
                ParentId = s.ParentId,
                DocUrl = s.IsFolder ? "" : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketGAVDocs + folderPath + "/" + s.DocumentName),
                IsRootFolder = s.IsRootFolder ?? false
            }).ToList();
            gavDoc.parentFolderList = GetAllParentDirectoryForGAVDocs(docId);
            return gavDoc;
        }

        public string GetParentFolderPathForGavData(int docId)
        {
            int sno = 1;
            List<GavDocsListItem> parentList = new List<GavDocsListItem>();
            GAVBigDog gavDoc;
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();
            while (docId != 0)
            {
                gavDoc = context.GAVBigDog.FirstOrDefault(f => f.Id == docId);
                parentList.Add(new GavDocsListItem { SNO = sno, Text = gavDoc.DocumentName, Value = misc.Encrypt(gavDoc.Id.ToString()) });
                sno++;
                docId = gavDoc.ParentId;
            }
            string[] arrParentList = parentList.OrderByDescending(o => o.SNO).Skip(1).Select(s => s.Text).ToArray();
            return string.Join("/", arrParentList);
        }

        public List<GavDocsListItem> GetAllParentDirectoryForGAVDocs(int gavDocId)
        {
            int parentId = 1;
            int sno = 1;
            List<GavDocsListItem> parentList = new List<GavDocsListItem>();
            GAVBigDog gavDoc;
            Misc misc = new Misc();
            var context = new GuardianAvionicsEntities();

            while (gavDocId != 0)
            {
                gavDoc = context.GAVBigDog.FirstOrDefault(f => f.Id == gavDocId);
                parentList.Add(new GavDocsListItem { SNO = sno, Text = gavDoc.DocumentName, Value = misc.Encrypt(gavDoc.Id.ToString()) });
                sno++;
                gavDocId = gavDoc.ParentId;

            }
            return parentList;
        }

        public List<RecursiveObject> GetFolderListGAV()
        {
            var context = new GuardianAvionicsEntities();
            var flatObjects = context.GAVBigDog.Where(w => w.IsFolder).Select(s => new FlatObject
            {
                label = s.DocumentName,
                id = s.Id,
                parentId = s.ParentId
            }).ToList();

            return FillRecursive(flatObjects, flatObjects.FirstOrDefault(f => f.parentId == 0).id);
        }

        public GeneralResponse UploadGAVFiles(List<HttpPostedFile> fileList, int parentId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            List<GAVBigDog> docList = new List<GAVBigDog>();
            S3BucketUpdates s3Updates = new S3BucketUpdates();
            string FolderPath = "";
            var gavDoc = context.GAVBigDog.FirstOrDefault(f => f.Id == parentId);
            List<string> fileNameList = fileList.Select(s => s.FileName).ToList();
            var existingDocs = context.GAVBigDog.Where(w => fileNameList.Contains(w.DocumentName) && w.ParentId == parentId).Select(s => s.DocumentName).ToList();
            if (existingDocs.Count > 0)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = string.Join(",", existingDocs) + " already exist into the folder.";
                return response;
            }

            foreach (var file in fileList)
            {
                docList.Add(new GAVBigDog
                {
                    DocumentName = file.FileName,
                    IsFolder = false,
                    ParentId = parentId
                });
                FolderPath = GetParentFolderPathForGavData(parentId);
                s3Updates.SaveFileToS3BucketGAVDocs(file.FileName, file.InputStream, FolderPath);
            }
            context.GAVBigDog.AddRange(docList);
            context.SaveChanges();

            response.ResponseCode = "0";
            response.ResponseMessage = (fileList.Count == 1 ? "File" : "Files") + " uploaded successfully.";
            return response;
        }

        public GeneralResponse AddGAVDirectory(string directoryName, int parentId)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            GAVBigDog gavgDocs = context.GAVBigDog.FirstOrDefault(w => w.ParentId == parentId && w.DocumentName == directoryName);
            if (gavgDocs != null)
            {
                response.ResponseMessage = "Folder name already exists.";
                response.ResponseCode = "1";
                return response;
            }
            gavgDocs = context.GAVBigDog.FirstOrDefault(w => w.Id == parentId);
            context.GAVBigDog.Add(new GAVBigDog
            {
                DocumentName = directoryName,
                IsFolder = true,
                ParentId = parentId
            });
            context.SaveChanges();
            response.ResponseMessage = "Folder added successfully.";
            response.ResponseCode = "0";
            string FolderPath = GetParentFolderPathForGavData(parentId);
            //S3BucketUpdates s3Updates = new S3BucketUpdates();
            //s3Updates.CreateFolderOns3(directoryName, FolderPath);
            AWS.AWS aws = new AWS.AWS();
            aws.CreateFolderOns3ForGAV(directoryName, FolderPath);
            return response;
        }

        public GeneralResponse DeleteFileOrDirectoryGAV(int engDocId, bool isFolder)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                string sourcePath = new Misc().GetS3FolderName("GAVDocs") + GetParentFolderPathForGavData(engDocId);
                AWS.AWS aws = new AWS.AWS();
                response = aws.DeleteFileOrDirectory(sourcePath, isFolder);
                if (response.ResponseCode == "0")
                {
                    List<int> idList = new List<int>();
                    idList.Add(engDocId);
                    if (isFolder)
                    {
                        List<int> parentIdList = new List<int>();
                        parentIdList.Add(engDocId);
                        while (parentIdList.Count > 0)
                        {
                            parentIdList = context.GAVBigDog.Where(w => parentIdList.Contains(w.ParentId)).Select(s => s.Id).ToList();
                            idList.AddRange(parentIdList);
                        }
                    }
                    context.GAVBigDog.RemoveRange(context.GAVBigDog.Where(w => idList.Contains(w.Id)).ToList());
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Exception).ToString();
                response.ResponseMessage = "Exception occurs while deleting the folder!!!";
            }
            return response;
        }

        public bool validateAccessForGAVDocs(string password, string docName)
        {
            password = new Misc().Encrypt(password);
            var context = new GuardianAvionicsEntities();
            var gavDoc = context.GAVBigDog.FirstOrDefault(f => f.IsRootFolder == true && f.Password == password && f.DocumentName == docName);
            if (gavDoc == null)
            {
                return false;
            }
            return true;
        }
        #endregion GavData

        #region PartIndex

        public PartIndexModel GetPartIndexById(int id)
        {
            PartIndexModel response = new PartIndexModel();
            var context = new GuardianAvionicsEntities();
            var partIndex = context.PartIndex.FirstOrDefault(f => f.Id == id);
            if (partIndex != null)
            {
                response.Id = new Misc().Encrypt(partIndex.Id.ToString());
                response.PartIndex = partIndex.PartNumberIndex;
                response.Description = partIndex.Description;
                response.LastAddedPartNumber = partIndex.LastAddedPartNo;
            }
            return response;
        }

        public List<PartIndexModel> GetPartIndexInChunk(int length, int start, string search, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.PartIndex.Count();


            IQueryable<PartIndex> partIndexList;
            partIndexList = context.PartIndex;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                partIndexList = partIndexList.Where(w => w.PartNumberIndex.ToLower().Contains(search) ||
                                                    w.Description.ToLower().Contains(search));
            }

            totalRowsAfterFilter = partIndexList.Count();




            List<PartIndexModel> response = partIndexList.Select(s => new PartIndexModel
            {
                Id = s.Id.ToString(),
                PartIndex = s.PartNumberIndex,
                Description = s.Description,
                LastAddedPartNumber = s.LastAddedPartNo
            }).ToList();

            response = response.OrderBy(o => Convert.ToInt32(o.PartIndex.Replace("-", ""))).ToList();
            int SerialNo = response.Count;
            Misc misc = new Misc();
            response.ForEach(f =>
            {
                f.Id = misc.Encrypt(f.Id.ToString());
                f.SNO = ++start;
            });
            return response;
        }

        public GeneralResponse AddPartIndex(string index, string description)
        {
            GeneralResponse response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex == index);
            if (partIndex != null)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Part index is already exist";
                return response;
            }
            partIndex = context.PartIndex.Create();

            var partnumberMaster = context.PartNumberMasters.OrderByDescending(o => o.Id).FirstOrDefault(f => f.PartNumber.StartsWith(index));
            if (partnumberMaster != null)
            {
                partIndex.LastAddedPartNo = partnumberMaster.PartNumber;
            }
            partIndex.PartNumberIndex = index;
            partIndex.Description = description;
            context.PartIndex.Add(partIndex);
            context.SaveChanges();
            response.ResponseCode = "0";
            response.ResponseMessage = "Part index added successfully";
            return response;
        }

        public GeneralResponse EditPartIndex(string index, string description, int id)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var context = new GuardianAvionicsEntities();
                var partIndex = context.PartIndex.FirstOrDefault(f => f.PartNumberIndex == index && f.Id != id);
                if (partIndex != null)
                {
                    response.ResponseCode = "9999";
                    response.ResponseMessage = "Part index is already exist.";
                    return response;
                }
                partIndex = context.PartIndex.FirstOrDefault(f => f.Id == id);
                partIndex.PartNumberIndex = index;
                partIndex.Description = description;

                string partNo = GetMaxPartNumberByIndex(index);
                partIndex.LastAddedPartNo = partNo;
                context.SaveChanges();

                response.ResponseCode = "0";
                response.ResponseMessage = "Part index updated successfully.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Error occurs while updating the record.";
            }
            return response;
        }

        public GeneralResponse DeletePartIndex(int id)
        {
            var response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            var partIndex = context.PartIndex.FirstOrDefault(f => f.Id == id);
            context.PartIndex.Remove(partIndex);
            context.SaveChanges();
            response.ResponseCode = "0";
            response.ResponseMessage = "Record deleted successfully.";
            return response;
        }

        public string GetMaxPartNumberByIndex(string index)
        {
            string maxPartNo = "";
            var context = new GuardianAvionicsEntities();
            List<string> partNoList = context.PartNumberMasters.Where(w => w.PartNumber.StartsWith(index)).Select(s => s.PartNumber).ToList();
            if (partNoList.Count > 0)
            {
                maxPartNo = index + partNoList.Max(m => Convert.ToInt32(m.Split('-')[1])).ToString();
            }
            return maxPartNo ?? "";
        }

        #endregion PartIndex

        #region InventoryReduction

        public List<InventoryReductionModel> GetInventoryReductionInChunk(int length, int start, string search, string sortColumnName, string sortDirection, out int totalRecord, out int totalRowsAfterFilter)
        {
            totalRowsAfterFilter = 0;
            totalRecord = 0;
            var context = new GuardianAvionicsEntities();
            totalRecord = context.InventoryReduction.Count();


            IQueryable<InventoryReduction> reductionList;
            reductionList = context.InventoryReduction;
            search = search.ToLower();
            if (!string.IsNullOrEmpty(search))
            {
                reductionList = reductionList.Where(w => w.PartNumberMaster.PartNumber.ToLower().Contains(search) ||
                                                    w.Comments.ToLower().Contains(search) ||
                                                    w.Quantity.ToString().Contains(search) ||
                                                    w.InventoryUsedByMaster.Type.ToString().Contains(search));
            }

            totalRowsAfterFilter = reductionList.Count();

            switch (sortColumnName)
            {
                case "SNO":
                    {
                        reductionList = sortDirection == "asc" ? reductionList.OrderBy(o => o.Id).Skip(start).Take(length) : reductionList.OrderByDescending(o => o.Id).Skip(start).Take(length);
                        break;
                    }
                case "PartNumber":
                    {
                        reductionList = sortDirection == "asc" ? reductionList.OrderBy(o => o.PartNumberMaster.PartNumber).Skip(start).Take(length) : reductionList.OrderByDescending(o => o.PartNumberMaster.PartNumber).Skip(start).Take(length);
                        break;
                    }
                case "Quantity":
                    {
                        reductionList = sortDirection == "asc" ? reductionList.OrderBy(o => o.Quantity).Skip(start).Take(length) : reductionList.OrderByDescending(o => o.Quantity).Skip(start).Take(length);
                        break;
                    }
                case "UsedBy":
                    {
                        reductionList = sortDirection == "asc" ? reductionList.OrderBy(o => o.InventoryUsedByMaster.Type).Skip(start).Take(length) : reductionList.OrderByDescending(o => o.InventoryUsedByMaster.Type).Skip(start).Take(length);
                        break;
                    }
            }

            List<InventoryReductionModel> response = reductionList.Select(s => new InventoryReductionModel
            {
                PartNo = s.PartNumberMaster.PartNumber,
                UsedBy = s.InventoryUsedByMaster.Type,
                Quantity = s.Quantity,
                UsedOnDatetime = s.ReductionDate,
                Comment = s.Comments,
                Id = s.Id
            }).ToList();

            Misc misc = new Misc();

            if (sortDirection == "asc")
            {
                response.ForEach(f =>
                {
                    f.encId = misc.Encrypt(f.Id.ToString());
                    f.SNO = ++start;
                    f.UsedOn = f.UsedOnDatetime.ToString("MM/dd/yyyy");
                    f.Id = 0;
                });
            }
            else
            {
                start = totalRecord - start;
                response.ForEach(f =>
                {
                    f.encId = misc.Encrypt(f.Id.ToString());
                    f.SNO = start--;
                    f.UsedOn = f.UsedOnDatetime.ToString("dd/MM/yyyy");
                    f.Id = 0;
                });
            }

            return response;
        }

        public InventoryReductionModel GetInventoryReductionById(int id, out GeneralResponse generalResponse)
        {
            generalResponse = new GeneralResponse();
            InventoryReductionModel response = new InventoryReductionModel();
            var context = new GuardianAvionicsEntities();
            try
            {

                var invReduction = context.InventoryReduction.FirstOrDefault(f => f.Id == id);
                if (invReduction != null)
                {
                    response.PartNo = invReduction.PartNumberMaster.PartNumber;
                    response.UsedOn = invReduction.ReductionDate.ToString("MM/dd/yyyy");
                    response.UsedById = invReduction.UsedBy;
                    response.Quantity = invReduction.Quantity;
                    response.Comment = invReduction.Comments;
                    response.encId = new Misc().Encrypt(invReduction.Id.ToString());
                }
                else
                {

                    generalResponse.ResponseCode = ((int)Enumerations.InventoryReduction.InvalidEditRequest).ToString();
                    generalResponse.ResponseMessage = Enumerations.InventoryReduction.InvalidEditRequest.GetStringValue();

                }
            }
            catch (Exception ex)
            {
                generalResponse.ResponseCode = ((int)Enumerations.InventoryReduction.Exception).ToString();
                generalResponse.ResponseMessage = Enumerations.InventoryReduction.Exception.GetStringValue();
            }
            generalResponse.ResponseCode = ((int)Enumerations.InventoryReduction.Success).ToString();
            generalResponse.ResponseMessage = Enumerations.InventoryReduction.Success.GetStringValue();

            return response;
        }

        public GeneralResponse UpdateInventoryReduction(InventoryReductionModel invReductionModel)
        {
            var response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();
            try
            {
                var invReduction = context.InventoryReduction.FirstOrDefault(f => f.Id == invReductionModel.Id);
                if (invReduction != null)
                {
                    List<UnitSerialNumber> unitList = new List<UnitSerialNumber>();
                    if (invReductionModel.Quantity > invReduction.Quantity)
                    {
                        int qtyDifference = invReductionModel.Quantity - invReduction.Quantity;
                        int statusId = (int)Enumerations.UnitStatus.AvailableForSale;

                        context.PartNumberMasters.FirstOrDefault(f => f.Id == invReduction.PartNumberMaster.Id).PurchaseOrders.ToList().ForEach(f => unitList.AddRange(f.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == statusId).ToList()));

                        if (unitList.Count() < qtyDifference)
                        {
                            return new GeneralResponse
                            {
                                ResponseCode = ((int)Enumerations.InventoryReduction.InvalidReductionQuantity).ToString(),
                                ResponseMessage = Enumerations.InventoryReduction.InvalidReductionQuantity.GetStringValue()
                            };
                        }

                    }

                    if (invReductionModel.Quantity > invReduction.Quantity)
                    {
                        unitList.OrderBy(o => o.Id).Take((invReductionModel.Quantity - invReduction.Quantity)).ToList().ForEach(f => f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.UsedForProduction);
                        context.SaveChanges();
                    }
                    else if (invReductionModel.Quantity < invReduction.Quantity)
                    {
                        int statusId = (int)Enumerations.UnitStatus.UsedForProduction;
                        context.PartNumberMasters.FirstOrDefault(f => f.Id == invReduction.PartNumberMaster.Id).PurchaseOrders.ToList().ForEach(f => unitList.AddRange(f.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == statusId).ToList()));
                        unitList.OrderByDescending(o => o.Id).Take((invReduction.Quantity - invReductionModel.Quantity)).ToList().ForEach(f => f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.AvailableForSale);
                        context.SaveChanges();
                    }
                    invReduction.ReductionDate = Convert.ToDateTime(invReductionModel.UsedOn);
                    invReduction.UsedBy = invReductionModel.UsedById;
                    invReduction.Quantity = invReductionModel.Quantity;
                    invReduction.Comments = invReductionModel.Comment;
                    context.SaveChanges();

                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.InventoryReduction.InvalidEditRequest).ToString();
                    response.ResponseMessage = Enumerations.InventoryReduction.InvalidEditRequest.GetStringValue();

                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.InventoryReduction.Exception).ToString();
                response.ResponseMessage = Enumerations.InventoryReduction.Exception.GetStringValue();
            }
            response.ResponseCode = ((int)Enumerations.InventoryReduction.Success).ToString();
            response.ResponseMessage = Enumerations.InventoryReduction.Success.GetStringValue();
            return response;

        }

        public GeneralResponse DeleteInventoryReduction(int id)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var invReduction = context.InventoryReduction.FirstOrDefault(f => f.Id == id);
                if (invReduction != null)
                {
                    int statusId = (int)Enumerations.UnitStatus.UsedForProduction;
                    List<UnitSerialNumber> unitList = new List<UnitSerialNumber>();
                    context.PartNumberMasters.FirstOrDefault(f => f.Id == invReduction.PartId).PurchaseOrders.ToList().ForEach(f => unitList.AddRange(f.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == statusId).ToList()));
                    unitList.OrderBy(o => o.Id).Take(invReduction.Quantity).ToList().ForEach(f => f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.AvailableForSale);
                    context.InventoryReduction.Remove(invReduction);
                    context.SaveChanges();
                }
                else
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.InventoryReduction.InvalidDeleteRequest).ToString(),
                        ResponseMessage = Enumerations.InventoryReduction.InvalidDeleteRequest.GetStringValue()
                    };

                }
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString(),
                    ResponseMessage = Enumerations.PartNumber.Exception.GetStringValue()
                };
            }
            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.PartNumber.Success).ToString(),
                ResponseMessage = Enumerations.PartNumber.Success.GetStringValue()
            };
        }

        public GeneralResponse AddInventoryReduction(InventoryReductionModel reductionModel)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                var partNumberMaster = context.PartNumberMasters.FirstOrDefault(f => f.PartNumber == reductionModel.PartNo);
                if (partNumberMaster == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.PartNumber.PartNoNotExist).ToString(),
                        ResponseMessage = Enumerations.PartNumber.PartNoNotExist.GetStringValue()
                    };
                }
                int statusId = (int)Enumerations.UnitStatus.AvailableForSale;
                List<UnitSerialNumber> unitList = new List<UnitSerialNumber>();
                context.PartNumberMasters.FirstOrDefault(f => f.Id == partNumberMaster.Id).PurchaseOrders.ToList().ForEach(f => unitList.AddRange(f.UnitSerialNumbers.Where(w => w.UnitCurrentStatusId == statusId).ToList()));
                int redCount = reductionModel.Quantity;
                if (unitList.Count() < reductionModel.Quantity)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.InventoryReduction.InvalidReductionQuantity).ToString(),
                        ResponseMessage = Enumerations.InventoryReduction.InvalidReductionQuantity.GetStringValue()
                    };
                }

                var inventoryReduction = context.InventoryReduction.Create();
                inventoryReduction.PartId = partNumberMaster.Id;
                inventoryReduction.ReductionDate = Convert.ToDateTime(reductionModel.UsedOn);
                inventoryReduction.Quantity = reductionModel.Quantity;
                inventoryReduction.Comments = reductionModel.Comment;
                inventoryReduction.UsedBy = reductionModel.UsedById;
                context.InventoryReduction.Add(inventoryReduction);
                context.SaveChanges();

                unitList.OrderBy(o => o.Id).Take(reductionModel.Quantity).ToList().ForEach(f => f.UnitCurrentStatusId = (int)Enumerations.UnitStatus.UsedForProduction);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.PartNumber.Exception).ToString(),
                    ResponseMessage = Enumerations.PartNumber.Exception.GetStringValue()
                };
            }
            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.PartNumber.Success).ToString(),
                ResponseMessage = Enumerations.PartNumber.Success.GetStringValue()
            };
        }
        #endregion InventoryReduction
    }
}
