﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataTransfer.Classes_for_Web;
using GA.DataLayer;
using System.Threading.Tasks;
using GA.Common;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;
//using NLog;

namespace GA.ApplicationLayer
{
    public class PaypalPaymentHelper
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public string SetSubscriptionPurchase(PaypalResponse objResponse)
        {

            ExceptionHandler.ReportError(new Exception(), "C= SetSubscriptionPurchase Method Call");
            var context = new GuardianAvionicsEntities();

            var mapSubscriptionAndUser = context.MapSubscriptionAndUSers.Create();
            try
            {
                mapSubscriptionAndUser.ProfileId = objResponse.ProfileId;
                mapSubscriptionAndUser.PaypalSubscriptionProfileId = objResponse.SubProfileId;
                mapSubscriptionAndUser.SubscriptionSourceId = (int)Enumerations.SubscriptionSourceEnum.Paypal;
                mapSubscriptionAndUser.PaypalStatusId = (int)Enumerations.PaypalStatus.Active;
                mapSubscriptionAndUser.InappOriginalTransactionId = "00";
                mapSubscriptionAndUser.SubId = context.SubscriptionPaymentDefinitions.FirstOrDefault(f => f.PayPalProductId == objResponse.SubscriptionItemId).Id;
                mapSubscriptionAndUser.InappPurchaseDate = DateTime.UtcNow;
                context.MapSubscriptionAndUSers.Add(mapSubscriptionAndUser);
                context.SaveChanges();
                context.Profiles.FirstOrDefault(f => f.Id == objResponse.ProfileId).CurrentSubscriptionId = mapSubscriptionAndUser.Id;
                context.SaveChanges();

            }
            catch (Exception ex)
            {
            }
            var transDetails = context.SubscriptionTransactions.Create();
            transDetails.MapSubAndUserId = mapSubscriptionAndUser.Id;
            transDetails.ReceiptId = objResponse.ReceiptId;
            transDetails.TransactionId = objResponse.TransactionId;
            transDetails.PayerId = objResponse.PayerId;
            transDetails.ReceiverId = objResponse.ReceiverId;
            transDetails.PayerAddressCountry = objResponse.PayerAddressCountry;
            transDetails.PayerAddressState = objResponse.PayerAddressState;
            transDetails.PayerAddressCity = objResponse.PayerAddressCity;
            transDetails.PayerAddressStreet = objResponse.PayerAddressStreet;
            transDetails.PayerAddressZip = objResponse.PayerAddressZip;
            transDetails.PayerFirstName = objResponse.PayerFirstName;
            transDetails.PayerLastName = objResponse.PayerLastName;
            transDetails.PayerEmail = objResponse.PayerEmail;
            transDetails.TransactionType = objResponse.TransactionType;
            transDetails.PaymentType = objResponse.PaymentType;
            transDetails.PaymentStatus = objResponse.PaymentStatus;
            transDetails.PaymentGross = Convert.ToDecimal(objResponse.GrossTotal);
            transDetails.PaymentFee = Convert.ToDecimal(objResponse.PaymentFee);
            transDetails.Currency = objResponse.Currency;

            string correctedTZ = objResponse.PaymentDate.Replace("PDT", "-0700");
            transDetails.PaymentDate = DateTime.UtcNow;
            context.SubscriptionTransactions.Add(transDetails);
            context.SaveChanges();
            ExceptionHandler.ReportError(new Exception(), "D= Save into SubscriptionTransactions");
            return "";

        }

        /// <summary>
        /// Send email to the user for Inapp receipt
        /// </summary>
        /// <param name="inAppReceipt"></param>
        /// <param name="subName"></param>
        /// <param name="subDetails"></param>
        public void SendEmail(InappReceipt inAppReceipt, int subId, int profileId, string subStatus)
        {
            var context = new GuardianAvionicsEntities();
            var pathHtmlFile = ConfigurationReader.InappEmailTemplatePath;
            string stylesheet = System.IO.File.ReadAllText(pathHtmlFile);
            string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");

            string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "smart_plane.png";
            string msg = "";
            string userName = "";
            var subscription = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == subId);
            msg = "Recurring amount has been received for subscription plan - " + subscription.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName + " (" + subscription.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails + ")";
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            if (profile != null)
            {
                if (profile.UserDetail != null)
                {
                    userName = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;
                }
            }
            string html = "";
            string subject = "Inapp purchase subscription receipt";
            string trStyle = "style=\"display:block\"";
            if (subStatus == Enumerations.PaypalStatus.Cancelled.ToString())
            {
                trStyle = "style=\"display:none\"";
                subject = "Subscription cancelled";
            }
            html = string.Format(htmlBody,
                     cid, //0
                     userName, //1
                     msg, //2
                     inAppReceipt.TransactionID, //3
                     inAppReceipt.PurchaseDate.ToString("g"), //4
                     inAppReceipt.ExpireDate.ToString("g"), //5
                     subscription.InAppProductId, //6
                     subscription.SubscriptionPaymentDefinition.Amount, //7
                     trStyle //8
                     );
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(avHtml);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);

            mail.To.Add(profile.EmailId);
            mail.Subject = subject;
            mail.Body = html;

            mail.IsBodyHtml = true;
            SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["Host"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
            smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            smpt.UseDefaultCredentials = false;
            smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailIdForSendingEmail"], ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
            smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smpt.Send(mail);

        }


        public void SendEmail(PaypalResponse objResponse)
        {
            try
            {
                // logger.Fatal("SendEmail method call" + Environment.NewLine);

                var pathHtmlFile = ConfigurationReader.paypalEmailTemplatePath;
                string stylesheet = System.IO.File.ReadAllText(pathHtmlFile);
                string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");

                var context = new GuardianAvionicsEntities();

                if (objResponse.TransactionType == "recurring_payment_suspended" || objResponse.TransactionType == "subscr_eot" || objResponse.TransactionType == "subscr_cancel")
                {
                    var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.PaypalSubscriptionProfileId == objResponse.SubProfileId);
                    if (mapSubAndUser != null)
                    {
                        objResponse.ProfileId = mapSubAndUser.ProfileId;
                        objResponse.PaymentDate = DateTime.UtcNow.ToString();
                    }

                }
                var userdetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == objResponse.ProfileId);
                if (userdetails != null)
                {
                    var subscriptionPaymentDefinitions = context.SubscriptionPaymentDefinitions.FirstOrDefault(f => f.PayPalProductId == objResponse.SubscriptionItemId);
                    var subMaster = context.SubscriptionMasters.FirstOrDefault(f => f.SubItemId == subscriptionPaymentDefinitions.SubscriptionItemId);


                    string username = userdetails.FirstName;
                    string cid = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "smart_plane.png";
                    string msg = "";
                    if (objResponse.TransactionType == "subscr_signup")
                    {
                        msg = "Thanks for purchasing the subscription plan - " + subMaster.SubItemName + " (" + subMaster.SubDetails + ")";
                    }
                    else if (objResponse.TransactionType == "subscr_payment")
                    {
                        var subAndUserMapping = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.ProfileId == objResponse.ProfileId && f.SubId == subscriptionPaymentDefinitions.Id);
                        subAndUserMapping.PaypalStatusId = (int)Enumerations.PaypalStatus.Active;
                        context.SaveChanges();
                        msg = "Recurring amount has been received for subscription plan - " + subMaster.SubItemName + " (" + subMaster.SubDetails + ")";

                    }
                    string correctedTZ = objResponse.PaymentDate.Replace("PDT", "-0700");


                    string h1 = string.Format(htmlBody,
                        cid, //0
                        username, //1
                        msg, //2
                        objResponse.ReceiptId, //3
                        objResponse.TransactionId, //4
                        objResponse.SubProfileId, //5
                        Convert.ToDateTime(correctedTZ).ToShortDateString(), //6
                        objResponse.PaymentStatus, //7
                        subMaster.SubItemName, //8
                        objResponse.PayerFirstName + " " + objResponse.PayerLastName, //9
                        objResponse.RecieverEmail, //10
                        objResponse.GrossTotal, //11
                        objResponse.Currency

                        );
                    AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);
                    MailMessage mail = new MailMessage();
                    mail.AlternateViews.Add(avHtml);
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);

                    mail.To.Add(userdetails.Profile.EmailId);
                    mail.Subject = "Paypal subscription receipt";
                    mail.Body = h1;

                    mail.IsBodyHtml = true;
                    SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["Host"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
                    smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                    smpt.UseDefaultCredentials = false;
                    smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailIdForSendingEmail"], ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
                    smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smpt.Send(mail);
                    // Task.Factory.StartNew(() => { smpt.Send(mail); });
                }
            }

            catch (Exception ex)
            {
                ExceptionHandler.ReportError(new Exception(), "Exception in email method");
                ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                //   logger.Fatal("Exception in paypal email - " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + Environment.NewLine);
            }

        }


        public void SetTransaction(PaypalResponse objResponse)
        {
            try
            {
                var context = new GuardianAvionicsEntities();

                if (objResponse.TransactionId != null)
                {
                    var objSubTransaction = context.SubscriptionTransactions.FirstOrDefault(f => f.TransactionId == objResponse.TransactionId);
                    if (objSubTransaction != null)
                    {
                        //Transaction details already inserted into the database
                        return;
                    }
                }
                var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.PaypalSubscriptionProfileId == objResponse.SubProfileId);
                if (objResponse.TransactionType == "recurring_payment_suspended" || objResponse.TransactionType == "subscr_eot" || objResponse.TransactionType == "subscr_cancel")
                {
                    if (mapSubAndUser != null)
                    {
                        objResponse.ProfileId = mapSubAndUser.ProfileId;
                        objResponse.PaymentDate = DateTime.UtcNow.ToString();

                        if (objResponse.TransactionType == "recurring_payment_suspended")
                        {
                            mapSubAndUser.PaypalStatusId = (int)Enumerations.PaypalStatus.Suspended;
                            //profile may be suspended by paypal or user can set the profile auto renewal as off, when usr set auto renewal off then we suspended the profile
                            //if user suspend there profile then we first check the expire date. if expired date is greater then we will not set the status a false.
                            //var lastReceipt = context.SubscriptionTransactions.OrderByDescending(o => o.PaymentDate).FirstOrDefault(f => f.MapSubAndUserId == mapSubAndUser.Id);
                            // if (lastReceipt != null)
                            // {
                            //    var regularFrequency =  mapSubAndUser.SubscriptionPaymentDefinition.RegularFrequency;
                            //     var recExpDate = DateTime.UtcNow;
                            //     if (regularFrequency == "Monthly")
                            //     {
                            //         recExpDate = lastReceipt.PaymentDate.Value.AddMonths(1);
                            //     }
                            //     else if (regularFrequency == "Yearly")
                            //     {
                            //         recExpDate = lastReceipt.PaymentDate.Value.AddYears(1);
                            //     }
                            //     if ((DateTime.UtcNow - recExpDate).TotalDays > 0)
                            //     {
                            //         mapSubAndUser.Status = false;
                            //     }
                            //     else {
                            //         mapSubAndUser.Status =  true;
                            //     }
                            // }
                            // else {
                            //     mapSubAndUser.Status = false;
                            // }
                        }
                        else if (objResponse.TransactionType == "subscr_cancel")
                        {
                            mapSubAndUser.PaypalStatusId = (int)Enumerations.PaypalStatus.Cancelled;
                        }
                        context.SaveChanges();
                    }
                }

                var transDetails = context.SubscriptionTransactions.Create();
                transDetails.MapSubAndUserId = mapSubAndUser.Id;
                transDetails.ReceiptId = objResponse.ReceiptId;
                transDetails.TransactionId = objResponse.TransactionId;
                transDetails.PayerId = objResponse.PayerId;
                transDetails.ReceiverId = objResponse.ReceiverId;
                transDetails.PayerAddressCountry = objResponse.PayerAddressCountry;
                transDetails.PayerAddressState = objResponse.PayerAddressState;
                transDetails.PayerAddressCity = objResponse.PayerAddressCity;
                transDetails.PayerAddressStreet = objResponse.PayerAddressStreet;
                transDetails.PayerAddressZip = objResponse.PayerAddressZip;
                transDetails.PayerFirstName = objResponse.PayerFirstName;
                transDetails.PayerLastName = objResponse.PayerLastName;
                transDetails.PayerEmail = objResponse.PayerEmail;
                transDetails.TransactionType = objResponse.TransactionType;
                transDetails.PaymentType = objResponse.PaymentType;
                transDetails.PaymentStatus = objResponse.PaymentStatus;
                transDetails.PaymentGross = Convert.ToDecimal(objResponse.GrossTotal);
                transDetails.PaymentFee = Convert.ToDecimal(objResponse.PaymentFee);
                transDetails.Currency = objResponse.Currency;

                string correctedTZ = objResponse.PaymentDate.Replace("PDT", "-0700");
                transDetails.PaymentDate = Convert.ToDateTime(correctedTZ);
                context.SubscriptionTransactions.Add(transDetails);
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                //  logger.Fatal("Exception while inserting transaction for object =" + Newtonsoft.Json.JsonConvert.SerializeObject(objResponse) + Environment.NewLine);
                return;
            }
            return;
        }

        public TransactionModel GetTransactionDetails(string transactionId)
        {
            var context = new GuardianAvionicsEntities();

            var objTransaction = context.SubscriptionTransactions.FirstOrDefault(f => f.TransactionId == transactionId);

            if (objTransaction == null)
            {
                return new TransactionModel();
            }
            return new TransactionModel
            {
                ReceiptId = objTransaction.ReceiptId,
                TransactionId = objTransaction.TransactionId,
                SubProfileId = objTransaction.MapSubscriptionAndUSer.PaypalSubscriptionProfileId,
                Date = objTransaction.PaymentDate,
                PaymentStatus = objTransaction.PaymentStatus,
                ItemName = objTransaction.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName,
                PayerFirstName = objTransaction.PayerFirstName,
                PayerLastName = objTransaction.PayerLastName,
                PayerEmailId = objTransaction.PayerEmail,
                PaymentGross = objTransaction.PaymentGross,
                Currency = objTransaction.Currency
            };
        }

        public InappReceiptModel GetInAppTransactionDetails(string transactionId)
        {
            var context = new GuardianAvionicsEntities();

            var objTransaction = context.InappReceipts.FirstOrDefault(f => f.TransactionID == transactionId);

            if (objTransaction == null)
            {
                return new InappReceiptModel();
            }
            return new InappReceiptModel
            {
                TransactionID = objTransaction.TransactionID,
                PurchaseDate = objTransaction.PurchaseDate,
                ProductId = objTransaction.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName,
                Amount = objTransaction.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.Amount,
                ExpireDate = objTransaction.ExpireDate,
                InappOriginalTransactionId = objTransaction.MapSubscriptionAndUSer.InappOriginalTransactionId
            };
        }

        public List<UserSubscription> GetSubscriptionListAllUser()
        {
            var context = new GuardianAvionicsEntities();
            var profileIdList = context.Profiles.Where(w => w.CurrentSubscriptionId != null).Select(s => s.Id).ToList();
            var subAndUserMappingList = context.MapSubscriptionAndUSers
                                    .OrderByDescending(o => o.InappPurchaseDate)
                                    .GroupBy(m => m.ProfileId)
                                    .Select(g => new
                                    {
                                        userSubscription = g.FirstOrDefault(f => f.Id == f.Profile.CurrentSubscriptionId) // g.OrderByDescending(o => o.Id).FirstOrDefault()
                                    }).ToList();
          

            var userSubList = subAndUserMappingList.Select(s => new UserSubscription
            {
                UserName = s.userSubscription.Profile.UserDetail.FirstName + " " + s.userSubscription.Profile.UserDetail.LastName,
                emailId = s.userSubscription.Profile.EmailId,
                //Status = s.userSubscription.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal ? s.userSubscription.PaypalStatusMaster.PaypalStatus : (s.userSubscription.Status == true ? Enumerations.PaypalStatus.Active.ToString() : Enumerations.PaypalStatus.Cancelled.ToString()),
                SubProfileId = s.userSubscription.PaypalSubscriptionProfileId,
                SubscriptionDetails = s.userSubscription.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails,
                SubscriptionName = s.userSubscription.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName,
                ProfileId = s.userSubscription.ProfileId.ToString(),
                SubId = s.userSubscription.Id.ToString(),
                PurchaseFrom = s.userSubscription.SubscriptionSourceMaster.PurchaseBy
            }).ToList();
            int i = 1;

            var mapSubAndUser = new MapSubscriptionAndUSer();
            var lastReceipt = new SubscriptionTransaction();
            var lastInappReceipt = new InappReceipt();
            var expDate = DateTime.UtcNow;
            SubscriptionHelper subHelper = new SubscriptionHelper();
            foreach (var userSub in userSubList)
            {
                userSub.SNO = i;
                i = i + 1;
                mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id.ToString() == userSub.SubId);
                if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
                {
                    userSub.Status = mapSubAndUser.PaypalStatusMaster.PaypalStatus;
                    lastReceipt = mapSubAndUser.SubscriptionTransactions.OrderByDescending(f => f.PaymentDate).FirstOrDefault();
                    if (lastReceipt != null)
                    {
                        switch (mapSubAndUser.SubscriptionPaymentDefinition.RegularFrequency)
                        {
                            case "Monthly":
                                {
                                    expDate = lastReceipt.PaymentDate.Value.AddMonths(1);
                                    break;
                                }
                            case "Yearly":
                                {
                                    expDate = lastReceipt.PaymentDate.Value.AddYears(1);
                                    break;
                                }
                        }
                        if ((expDate - DateTime.UtcNow).TotalMinutes >= 0)
                        {
                            userSub.IsExpired = false;
                        }
                        else
                        {
                            userSub.IsExpired = true;
                        }
                    }
                    else
                    {
                        userSub.IsExpired = true;
                    }
                }
                else if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
                {
                    lastInappReceipt = mapSubAndUser.InappReceipts.OrderByDescending(o => o.ExpireDate).FirstOrDefault();
                    if (lastInappReceipt != null)
                    {
                        userSub.Status = ((lastInappReceipt.ExpireDate - DateTime.UtcNow).TotalMinutes >= 0) ? Enumerations.PaypalStatus.Active.ToString() : Enumerations.PaypalStatus.Cancelled.ToString();
                    }
                    else
                    {
                        userSub.Status = Enumerations.PaypalStatus.Cancelled.ToString();
                    }
                }
                else
                {
                    userSub.Status = ((subHelper.GetExpiryDateForFreePlan(mapSubAndUser.InappPurchaseDate) - DateTime.UtcNow).TotalMinutes >= 0) ? Enumerations.PaypalStatus.Active.ToString() : Enumerations.PaypalStatus.Cancelled.ToString();
                }
            }

            return userSubList;
        }


        public List<SubscriptionViewModel> GetAllReceiptBySubProfileId(int profileId)
        {
            var context = new GuardianAvionicsEntities();

            var inappSubIdList = context.MapSubscriptionAndUSers.Where(w => w.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase && w.ProfileId == profileId).Select(s => s.Id).ToList();
            var inappReceipts = context.InappReceipts.Where(w => inappSubIdList.Contains(w.MapSubAndUserId ?? 0)).OrderBy(o => o.PurchaseDate).ToList();

            var list = new List<Tuple<int, bool>>();
            SubscriptionHelper subHelper = new SubscriptionHelper();
            foreach (var subId in inappSubIdList)
            {
                list.Add(Tuple.Create(subId, subHelper.IsSubscriptionActive(subId)));
            }

            var prevReceipt = new InappReceipt();
            prevReceipt = null;
            List<SubscriptionViewModel> subViewModelList = new List<SubscriptionViewModel>();
            SubscriptionViewModel subscriptionViewModel = new SubscriptionViewModel();
            subscriptionViewModel.Subscription = new UserSubscription();
            subscriptionViewModel.InAppReceiptList = new List<InappReceiptModel>();
            if (inappReceipts.Count > 0)
            {
                var firstReceipt = inappReceipts.FirstOrDefault();
                subscriptionViewModel.Subscription.UserName = firstReceipt.MapSubscriptionAndUSer.Profile.UserDetail.FirstName + " " + firstReceipt.MapSubscriptionAndUSer.Profile.UserDetail.LastName;
                subscriptionViewModel.Subscription.emailId = firstReceipt.MapSubscriptionAndUSer.Profile.EmailId;
                if (list.FirstOrDefault(f => f.Item1 == firstReceipt.MapSubscriptionAndUSer.Id).Item2 == true)
                {
                    subscriptionViewModel.Subscription.Status = Enumerations.PaypalStatus.Active.ToString();
                }
                else if (firstReceipt.MapSubscriptionAndUSer.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
                {
                    subscriptionViewModel.Subscription.Status = firstReceipt.MapSubscriptionAndUSer.PaypalStatusMaster.PaypalStatus;
                }
                else
                {
                    subscriptionViewModel.Subscription.Status = Enumerations.PaypalStatus.Cancelled.ToString();
                }
                subscriptionViewModel.Subscription.SubId = firstReceipt.MapSubAndUserId.ToString();
                subscriptionViewModel.Subscription.SubscriptionDetails = firstReceipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails;
                subscriptionViewModel.Subscription.SubscriptionName = firstReceipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName;
                subscriptionViewModel.Subscription.CreateDate = firstReceipt.PurchaseDate;
                subscriptionViewModel.Subscription.PurchaseFrom = firstReceipt.MapSubscriptionAndUSer.SubscriptionSourceMaster.PurchaseBy;

                prevReceipt = inappReceipts.FirstOrDefault();

                foreach (var receipt in inappReceipts)
                {
                    if ((receipt.PurchaseDate - prevReceipt.ExpireDate).TotalHours > 24 || receipt.MapSubscriptionAndUSer.InAppProductId != prevReceipt.MapSubscriptionAndUSer.InAppProductId)
                    {
                        //Then this is the new slot of subscription
                        //First set the end date
                        subscriptionViewModel.Subscription.EndDate = prevReceipt.ExpireDate;
                        subViewModelList.Add(subscriptionViewModel);
                        subscriptionViewModel = new SubscriptionViewModel();
                        subscriptionViewModel.Subscription = new UserSubscription();
                        subscriptionViewModel.InAppReceiptList = new List<InappReceiptModel>();

                        subscriptionViewModel.Subscription.UserName = receipt.MapSubscriptionAndUSer.Profile.UserDetail.FirstName + " " + receipt.MapSubscriptionAndUSer.Profile.UserDetail.LastName;
                        subscriptionViewModel.Subscription.emailId = receipt.MapSubscriptionAndUSer.Profile.EmailId;
                        subscriptionViewModel.Subscription.Status = list.FirstOrDefault(f => f.Item1 == receipt.MapSubscriptionAndUSer.Id).Item2 == true ? Enumerations.PaypalStatus.Active.ToString() : Enumerations.PaypalStatus.Cancelled.ToString();
                        subscriptionViewModel.Subscription.SubId = receipt.MapSubAndUserId.ToString();
                        subscriptionViewModel.Subscription.SubscriptionDetails = receipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails;
                        subscriptionViewModel.Subscription.SubscriptionName = receipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName;
                        subscriptionViewModel.Subscription.CreateDate = receipt.PurchaseDate;
                        subscriptionViewModel.Subscription.PurchaseFrom = receipt.MapSubscriptionAndUSer.SubscriptionSourceMaster.PurchaseBy;

                        subscriptionViewModel.InAppReceiptList.Add(new InappReceiptModel
                        {
                            Amount = receipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.Amount,
                            Id = receipt.Id,
                            PurchaseDate = receipt.PurchaseDate,
                            ExpireDate = receipt.ExpireDate,
                            TransactionID = receipt.TransactionID,
                            InappOriginalTransactionId = receipt.MapSubscriptionAndUSer.InappOriginalTransactionId,
                            ProductId = receipt.MapSubscriptionAndUSer.InAppProductId
                        });

                    }
                    else
                    {
                        // Add receipt into list
                        subscriptionViewModel.InAppReceiptList.Add(new InappReceiptModel
                        {
                            Amount = receipt.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.Amount,
                            Id = receipt.Id,
                            PurchaseDate = receipt.PurchaseDate,
                            ExpireDate = receipt.ExpireDate,
                            TransactionID = receipt.TransactionID,
                            InappOriginalTransactionId = receipt.MapSubscriptionAndUSer.InappOriginalTransactionId,
                            ProductId = receipt.MapSubscriptionAndUSer.InAppProductId
                        });
                    }
                    prevReceipt = receipt;
                }
                subscriptionViewModel.Subscription.EndDate = prevReceipt.ExpireDate;
                subViewModelList.Add(subscriptionViewModel);
            }

            // List<SubscriptionViewModel> subViewModelList = new List<SubscriptionViewModel>();
            var userSubList = context.MapSubscriptionAndUSers.Where(w => w.ProfileId == profileId && w.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal).OrderByDescending(o => o.Id).Select(s => new SubscriptionViewModel
            {
                Subscription = new UserSubscription
                {
                    UserName = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                    emailId = s.Profile.EmailId,
                    Status = s.PaypalStatusMaster.PaypalStatus,
                    SubProfileId = s.PaypalSubscriptionProfileId,
                    SubscriptionDetails = s.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails,
                    SubscriptionName = s.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName,
                    CreateDate = s.InappPurchaseDate,
                    PurchaseFrom = s.SubscriptionSourceMaster.PurchaseBy,
                    PaypalButtonId = s.SubscriptionPaymentDefinition.PaypalButtonId,
                    SubId = s.Id.ToString()

                },
                ReceiptList = s.SubscriptionTransactions.OrderByDescending(o => o.Id).Select(t => new TransactionModel
                {
                    ReceiptId = t.ReceiptId,
                    TransactionId = t.TransactionId,
                    TransactionType = t.TransactionType,
                    PaymentGross = t.PaymentGross,
                    Date = t.PaymentDate
                }).ToList()
            }).ToList();
            userSubList.ForEach(f => f.Subscription.IsExpired = !subHelper.IsSubscriptionActive(Convert.ToInt32(f.Subscription.SubId)));
            subViewModelList.AddRange(userSubList);

            userSubList = context.MapSubscriptionAndUSers.Where(w => w.ProfileId == profileId && w.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Free).OrderByDescending(o => o.Id).Select(s => new SubscriptionViewModel
            {
                Subscription = new UserSubscription
                {
                    UserName = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                    emailId = s.Profile.EmailId,
                    //Status = s.Status == true ? "Active" : "Cancelled" ,
                    SubProfileId = s.PaypalSubscriptionProfileId,
                    SubscriptionDetails = s.SubscriptionPaymentDefinition.SubscriptionMaster.SubDetails,
                    SubscriptionName = s.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName,
                    CreateDate = s.InappPurchaseDate,
                    PurchaseFrom = s.SubscriptionSourceMaster.PurchaseBy,
                    PaypalButtonId = ""
                }
            }).ToList();
            userSubList.ForEach(f => { f.ReceiptList = new List<TransactionModel>(); f.Subscription.Status = ((subHelper.GetExpiryDateForFreePlan(f.Subscription.CreateDate) - DateTime.UtcNow).TotalMinutes >= 0) ? Enumerations.PaypalStatus.Active.ToString() : Enumerations.PaypalStatus.Cancelled.ToString(); });
            subViewModelList.AddRange(userSubList);

            int i = 1;
            foreach (var subViewModel in subViewModelList.Where(w => w.Subscription.PurchaseFrom == Enumerations.SubscriptionSourceEnum.InappPurchase.ToString()))
            {
                subViewModel.InAppReceiptList = subViewModel.InAppReceiptList.OrderByDescending(o => o.PurchaseDate).ToList();
                i = 1;
                subViewModel.InAppReceiptList.ForEach(f => f.SNO = i++);
            }

            var latestSub = subViewModelList.OrderByDescending(o => o.Subscription.CreateDate).FirstOrDefault(w => w.Subscription.PurchaseFrom == Enumerations.SubscriptionSourceEnum.InappPurchase.ToString() && w.Subscription.Status == Enumerations.PaypalStatus.Active.ToString());
            if (latestSub != null)
            {
                var listVM = subViewModelList.OrderByDescending(o => o.Subscription.CreateDate).Where(w => w.Subscription.CreateDate < latestSub.Subscription.CreateDate).ToList();
                if (listVM.Count > 0)
                {
                    listVM.ForEach(f => f.Subscription.Status = Enumerations.PaypalStatus.Cancelled.ToString());
                }
            }
            foreach (var subViewModel in subViewModelList.Where(w => w.Subscription.PurchaseFrom == Enumerations.SubscriptionSourceEnum.Paypal.ToString()))
            {
                subViewModel.Subscription.EndDate = GetEndDateOfSubscription(subViewModel.Subscription.SubProfileId) ?? DateTime.UtcNow;
                i = 1;
                subViewModel.ReceiptList.ForEach(f => f.SNO = i++);

            }

            foreach (var subViewModel in subViewModelList.Where(w => w.Subscription.PurchaseFrom == Enumerations.SubscriptionSourceEnum.Free.ToString()))
            {
                subViewModel.Subscription.EndDate = new SubscriptionHelper().GetExpiryDateForFreePlan(subViewModel.Subscription.CreateDate);
            }
            subViewModelList = subViewModelList.OrderByDescending(o => o.Subscription.CreateDate).ToList();
            return subViewModelList;
        }

        public DateTime? GetEndDateOfSubscription(string subProfileId)
        {
            var context = new GuardianAvionicsEntities();
            

            var mapSunAndUSer = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.PaypalSubscriptionProfileId == subProfileId);
            string subscriptionStatus = mapSunAndUSer.PaypalStatusMaster.PaypalStatus;
            var lastTransaction = context.SubscriptionTransactions.Where(w=>w.MapSubAndUserId == mapSunAndUSer.Id).OrderByDescending(o => o.Id).FirstOrDefault();
            DateTime? endDate = DateTime.UtcNow;
           
            var subFrequency = context.SubscriptionPaymentDefinitions.FirstOrDefault(o => o.Id == lastTransaction.MapSubscriptionAndUSer.SubscriptionPaymentDefinition.Id).RegularFrequency;
            switch (subFrequency.ToUpper())
            {
                case "DAILY":
                    {
                        endDate = lastTransaction.PaymentDate.Value.AddDays(1);
                        break;
                    }
                case "WEEKLY":
                    {
                        //endDate = lastTransaction.PaymentDate.Value.AddMinutes(10);
                        endDate = lastTransaction.PaymentDate.Value.AddDays(7);
                        break;
                    }
                case "MONTHLY":
                    {
                        //endDate = lastTransaction.PaymentDate.Value.AddMinutes(10);
                        endDate = lastTransaction.PaymentDate.Value.AddMonths(1);
                        break;
                    }
                case "YEARLY":
                    {
                        endDate = lastTransaction.PaymentDate.Value.AddYears(1);
                        break;
                    }
            }
            return endDate;
        }

        public void ReactivatePaypalProfile(string subProfileId)
        {
            var context = new GuardianAvionicsEntities();
            var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.PaypalSubscriptionProfileId == subProfileId);
            if (mapSubAndUser != null)
            {
                mapSubAndUser.PaypalStatusId = (int)Enumerations.PaypalStatus.Active;
                context.SaveChanges();
            }
        }

        public void SuspendPaypalProfile(string subProfileId)
        {
            var context = new GuardianAvionicsEntities();
            var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.PaypalSubscriptionProfileId == subProfileId);
            if (mapSubAndUser != null)
            {
                mapSubAndUser.PaypalStatusId = (int)Enumerations.PaypalStatus.Suspended;
                context.SaveChanges();
            }
        }
    }


}
