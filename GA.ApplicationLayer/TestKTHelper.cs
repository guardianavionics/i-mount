﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.DataLayer;
using GA.DataTransfer.Classes_for_Web;

namespace GA.ApplicationLayer
{
    public class TestKTHelper
    {
        public TestKTModel GetTestKT()
        {
            TestKTModel testKTmodel = new TestKTModel();
            return testKTmodel;
        }

        public TestKTModel AddTestKT(TestKTModel testKTmodel)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                if (testKTmodel != null)
                {
                    TestKT model = new TestKT();
                    model.Id = testKTmodel.Id;
                    model.Name = testKTmodel.Name;
                    model.Description = testKTmodel.Description;
                    context.TestKT.Add(model);
                    context.Entry(model).State = EntityState.Added;
                    context.SaveChanges();

                }
                return testKTmodel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TestKTModel DetailTestKT(int id)
        {
            var context = new GuardianAvionicsEntities();
            TestKTModel testKTobj = new TestKTModel();
            try
            {
                if (id > 0)
                {
                    var model = context.TestKT.Where(x => x.Id == id).Select(x => x).FirstOrDefault();
                    testKTobj.Id = model.Id;
                    testKTobj.Name = model.Name;
                    testKTobj.Description = model.Description;
                }
                return testKTobj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<TestKTModel> ListTestKT()
        {
            var context = new GuardianAvionicsEntities();
            List<TestKTModel> ListTestKT = new List<TestKTModel>();
            try
            {
                var list = context.TestKT.Select(x => x).ToList();
                foreach (var item in list)
                {
                    TestKTModel model = new TestKTModel();
                    model.Id = item.Id;
                    model.Name = item.Name;
                    model.Description = item.Description;
                    ListTestKT.Add(model);
                }

                return ListTestKT;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TestKTModel EditTestKT(TestKTModel testKTModel)
        {
            var context = new GuardianAvionicsEntities();
            TestKTModel testKTobj = new TestKTModel();
            try
            {
                var oldModel = context.TestKT.Where(x => x.Id == testKTModel.Id).Select(x => x).FirstOrDefault();
                if (testKTModel != null && oldModel != null)
                {
                    oldModel.Id = testKTModel.Id;
                    oldModel.Name = testKTModel.Name;
                    oldModel.Description = testKTModel.Description;
                    context.Entry(oldModel).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return testKTModel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool DeleteTestKT(int id)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                if (id > 0)
                {
                    var model = context.TestKT.Where(x => x.Id == id).Select(x => x).FirstOrDefault();
                    context.TestKT.Remove(model);
                    context.Entry(model).State = EntityState.Deleted;
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
