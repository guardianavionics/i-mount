﻿using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
//using NLog;

namespace GA.ApplicationLayer
{
    public class IPassengerHelper
    {
        public IPassengerMediaDetails GetIPassengerMediaDetails(int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };

            try
            {
                IQueryable<IPassenger> objIPassengerDetails;

                var objMediaTypeMaster = context.IPassengerMediaTypeMasters.ToList();
                if(aircraftId == 0)
                {
                    objIPassengerDetails = context.IPassengers.OrderByDescending(o => o.Id).Where(w => w.IsForAll == true);
                }
                else
                {
                    List<int> deletedIPassengerByAircraftid = context.IPassengerDetailsDeletedByUsers.Where(w => w.AircraftId == aircraftId).Select(s => s.IPassengerId).ToList();
                    objIPassengerDetails = context.IPassengers.OrderByDescending(o => o.Id).Where(w => !deletedIPassengerByAircraftid.Contains(w.Id) && (w.AircraftId == aircraftId || w.IsForAll == true));
                }
                Misc objMisc = new Misc();
                int mediaTypeId = 0;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Video").Id;
                response.VideoList.AddRange(
                        objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                        {
                            Title = s.Title,
                            Id = s.Id,
                            FilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassVideo + s.FileName,
                            ThumbnailImagePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassVideo + s.ThumbnailImage,
                             IsForAll = s.IsForAll ?? false
                        }).ToList()
                    );
                response.VideoList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Image").Id;
                response.ImageList.AddRange(
                      objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                      {
                          Title = s.Title,
                          
                          Id = s.Id,
                          FilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassImage + s.FileName,
                          ThumbnailImagePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassImage + s.ThumbnailImage,
                          IsForAll = s.IsForAll ?? false
                      }).ToList()
                  );
                response.ImageList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });


                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "PDF").Id;
                response.PDFList.AddRange(
                     objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                     {
                         Title = s.Title,
                         Id = s.Id,
                         FilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassPDF + s.FileName,
                         ThumbnailImagePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassPDF + s.ThumbnailImage,
                         IsForAll = s.IsForAll ?? false
                     }).ToList()
                 );
                response.PDFList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Instruction").Id;
                response.InstructionList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                    {
                        Title = s.Title,
                        Id = s.Id,
                        FilePath = "",
                        IsForAll = s.IsForAll ?? false
                    }).ToList()
                );
                response.InstructionList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Warning").Id;
                response.WarningList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                    {
                        Title = s.Title,
                        Id = s.Id,
                        FilePath = "",
                        IsForAll = s.IsForAll ?? false
                    }).ToList()
                );
                response.WarningList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "Web-API";
                errorLog.ErrorSource = "GetIPassengerMediaDetails";
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Profile Id = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;
        }

        public IPassengerResponseModel GetIPassengerMediaDetailsAPI(int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new IPassengerResponseModel
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };

            try
            {
                var objMediaTypeMaster = context.IPassengerMediaTypeMasters.ToList();
                string bucketPath = ConfigurationReader.s3BucketURL +   ConfigurationReader.BucketIPassVideo;
                var objIPassengerDetails = context.IPassengers.Where(w => w.AircraftId == aircraftId);

                int mediaTypeId = 0;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Video").Id;
                response.VideoList.AddRange(
                        objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                        {
                            Title = s.Title,
                            Id = s.Id,
                            //FilePath = ConfigurationReader.ServerUrl + "/IPassengerVideos/" + s.FileName,
                            //ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : ConfigurationReader.ServerUrl + "/IPassengerVideos/" + s.ThumbnailImage,
                            FilePath = bucketPath  + s.FileName,
                            ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath +  s.ThumbnailImage,

                        }).ToList()
                    );
                bucketPath = ConfigurationReader.s3BucketURL +   ConfigurationReader.BucketIPassImage;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Image").Id;
                response.ImageList.AddRange(
                      objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                      {
                          Title = s.Title,
                          Id = s.Id,
                          //FilePath = ConfigurationReader.ServerUrl + "/IPassengerImages/" + s.FileName,
                          //ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : ConfigurationReader.ServerUrl + "/IPassengerImages/" + s.ThumbnailImage,
                          FilePath = bucketPath + s.FileName,
                          ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath + s.ThumbnailImage,
                      }).ToList()
                  );
                bucketPath = ConfigurationReader.s3BucketURL +   ConfigurationReader.BucketIPassPDF;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "PDF").Id;
                response.PDFList.AddRange(
                     objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                     {
                         Title = s.Title,
                         Id = s.Id,
                         //FilePath = ConfigurationReader.ServerUrl + "/IPassengerPDF/" + s.FileName,
                         //ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : ConfigurationReader.ServerUrl + "/IPassengerPDF/" + s.ThumbnailImage,
                         FilePath = bucketPath + s.FileName,
                         ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath+ s.ThumbnailImage,
                     }).ToList()
                 );
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Instruction").Id;
                response.InstructionList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                    {
                        Title = s.Title,
                        Id = s.Id,
                        FilePath = "",
                        ThumbnailImagePath = ""
                    }).ToList()
                );

                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Warning").Id;
                response.WarningList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new MediaDetails
                    {
                        Title = s.Title,
                        Id = s.Id,
                        FilePath = "",
                        ThumbnailImagePath = ""
                    }).ToList()
                );

            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "API";
                errorLog.ErrorSource = "GetIPassengerMediaDetailsAPI";
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Profile Id = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerResponseModel
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }
            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;
        }

        public PassengerProIpassegerDetails GetIPassengerProMediaDetailsAPI(int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new PassengerProIpassegerDetails
            {
                ImageList = new List<FileDetails>(),
                InstructionList = new List<DocumentDetails>(),
                PDFList = new List<FileDetails>(),
                VideoList = new List<FileDetails>(),
                WarningList = new List<DocumentDetails>()
            };

            try
            {
                var objMediaTypeMaster = context.IPassengerMediaTypeMasters.ToList();
                string bucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassVideo;
                List<int> ipassengerIdList = context.IPassengerDetailsDeletedByUsers.Where(w => w.AircraftId == aircraftId).Select(s => s.IPassengerId).ToList();
                var objIPassengerDetails = context.IPassengers.Where(w => !ipassengerIdList.Contains(w.Id) && (w.AircraftId == aircraftId || w.IsForAll == true));

                int mediaTypeId = 0;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Video").Id;
                response.VideoList.AddRange(
                        objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new FileDetails
                        {
                            Id = s.Id,
                            Title = s.Title,
                            FilePath = bucketPath + s.FileName,
                            ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath + s.ThumbnailImage,
                            FileSizeInMB = s.FileSize ?? 0
                        }).ToList()
                    );
                bucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassImage;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Image").Id;
                response.ImageList.AddRange(
                      objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new FileDetails
                      {
                          Id = s.Id,
                          Title = s.Title,
                          FilePath = bucketPath + s.FileName,
                          ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath + s.ThumbnailImage,
                          FileSizeInMB = s.FileSize ?? 0
                      }).ToList()
                  );
                bucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketIPassPDF;
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "PDF").Id;
                response.PDFList.AddRange(
                     objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new FileDetails
                     {
                         Id = s.Id,
                         Title = s.Title,
                         FilePath = bucketPath + s.FileName,
                         ThumbnailImagePath = string.IsNullOrEmpty(s.ThumbnailImage) ? "" : bucketPath + s.ThumbnailImage,
                         FileSizeInMB = s.FileSize ?? 0
                     }).ToList()
                 );
                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Instruction").Id;
                response.InstructionList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new DocumentDetails
                    {
                        Id = s.Id,
                        Title = s.Title,
                    }).ToList()
                );

                mediaTypeId = objMediaTypeMaster.FirstOrDefault(o => o.MediaType == "Warning").Id;
                response.WarningList.AddRange(
                    objIPassengerDetails.Where(v => v.MediaType == mediaTypeId).Select(s => new DocumentDetails
                    {
                        Id = s.Id,
                        Title = s.Title,
                    }).ToList()
                );
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "API";
                errorLog.ErrorSource = "GetIPassengerMediaDetailsAPI";
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Profile Id = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
            }
            return response;
        }
    }
}
