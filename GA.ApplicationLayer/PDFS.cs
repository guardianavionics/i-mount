﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using GA.DataLayer;
//using NLog;
using System.Configuration;
//using NLog.Internal;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;


namespace GA.ApplicationLayer
{
    public class JPIHeaderCalculation
    {
        public int sno { get; set; }
        public int index { get; set; }
        public int value { get; set; }
        public string convertTo { get; set; }
        public bool IsRoundOf { get; set; }
        public string KeyName { get; set; }
    }

   public class PDFS
    {
        //public readonly Logger logger = LogManager.GetCurrentClassLogger();

        StringBuilder strLogger = new StringBuilder();
        StringBuilder strMissingIds = new StringBuilder();
        //int tempFlightId = 1;
        public static List<int?> aircraftIdList = new List<int?>();
        public List<JPIHeaderCalculation> jPIHeaderCalculation;
        public List<JPIHeaderCalculation> jPIHeaderCalculationULP;
        public List<JPIHeaderCalculation> GarminAltitudeAirDataValues;
        public PilotLogRawData g_pilotLogRawData { get; set; }
        public AircraftProfile aircraftProfile;
        public List<string> g_commandArray;
        public DataTable dt_AirframeDataLog;
        public PilotLog g_PilotLog;
        public List<PilotLog> g_PilotLogList = new List<PilotLog>();
        public AirframeDatalog g_FirstDataLog;
        public AirframeDatalog g_LastDataLog;
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        //public string ConnectionString = @"data source=10.10.3.7\SQL2012RTM;initial catalog=GuardianAvionics;user id=sa;password=ideavate@123;";

        string JPIHeader = "OILP,OILT,RPM,MAP,FF,FP,FQL,FQR,CalculatedFuelRemaining,VOLTS,VOLTS2,AMP,TotalAircraftTime,EngineTime,Cht6,Egt6,Cht5,Egt5,Cht4,Egt4,Cht3,Egt3,Cht2,Egt2,Cht1,Egt1,TIT1,TIT2,ElevatorTrimPosition,UnitsIndicator,FlapPosition,UnitsIndicator2,CarbTemp,UnitsIndicator3,CoolantPressure,UnitsIndicator4,CoolantTemperature,UnitsIndicator5,AMP2,UnitsIndicator6,AileronTrimPosition,UnitsIndicator7,RubberTrimPosition,UnitsIndicator8,FuelQty3,UnitsIndicator9,FuelQty4,UnitsIndicator10,DiscreteInput1,DiscreteInput2,DiscreteInput3,DiscreteInput4";
        string ULPHeader = "IgnStatus,SensorStatus,RPM,ThrottlePosition,Baro,Airtemp,OILP,OILT,FP,FF,EcuTemp,Batteryvoltage,CHT1,CHT2,CHT3,CHT4,CHT5,CHT6,EGT1,EGT2,EGT3,EGT4,EGT5,EGT6,Sen1,Sen2,Sen3,Sen4,Sen5,MAP";

        Dictionary<string, string> GarminEngineDataMessage;
        Dictionary<string, string> GarminAltitudeAirDataMessage;

        public void ParseDataFiles(int? parseFilesForaircraftId)
        {
            jPIHeaderCalculation = new List<JPIHeaderCalculation>();
            jPIHeaderCalculationULP = new List<JPIHeaderCalculation>();
            GarminAltitudeAirDataValues = new List<JPIHeaderCalculation>();
            g_pilotLogRawData = new PilotLogRawData();
            aircraftProfile = new AircraftProfile();
            g_commandArray = new List<string>();
            dt_AirframeDataLog = new DataTable();

            g_PilotLog = new PilotLog();


            GarminAltitudeAirDataMessage = new Dictionary<string, string>();
            GarminEngineDataMessage = new Dictionary<string, string>();

            GarminAltitudeAirDataMessage.Add("Pitch", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 1, index = 11, value = 4, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("Roll", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 2, index = 15, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("VSpd", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 3, index = 45, value = 4, convertTo = "CelciusToF", IsRoundOf = false });
            GarminAltitudeAirDataMessage.Add("OAT", "");
            GarminAltitudeAirDataValues.Add(new JPIHeaderCalculation { sno = 4, index = 49, value = 3, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false });



            GarminEngineDataMessage.Add("OILP", ""); //OILP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 1, index = 11, value = 3, convertTo = "", IsRoundOf = false, KeyName = "OILP" });

            GarminEngineDataMessage.Add("OILT", ""); //OILT
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 2, index = 14, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "OILT" });
            GarminEngineDataMessage.Add("RPM", ""); //RPM
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 3, index = 18, value = 4, convertTo = "", IsRoundOf = false, KeyName = "RPM" });
            GarminEngineDataMessage.Add("MAP", ""); //MAP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 4, index = 26, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "MAP" });
            GarminEngineDataMessage.Add("FF", ""); //FF
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 5, index = 29, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FF" });
            GarminEngineDataMessage.Add("FP", ""); //FP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 6, index = 35, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FP" });
            GarminEngineDataMessage.Add("FQL", ""); //FQL
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 7, index = 38, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FQL" });
            GarminEngineDataMessage.Add("FQR", ""); //FQR
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 8, index = 41, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "FQR" });
            GarminEngineDataMessage.Add("CalculatedFuelRemaining", ""); //CalculatedFuelRemaining
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 9, index = 44, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "CalculatedFuelRemaining" });
            GarminEngineDataMessage.Add("Volts", ""); //Volts
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 10, index = 47, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "Volts" });
            GarminEngineDataMessage.Add("Volts2", ""); //Volts2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 11, index = 50, value = 3, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "Volts2" });
            GarminEngineDataMessage.Add("AMP", ""); //AMP
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 12, index = 53, value = 4, convertTo = "OneTenthOfValue", IsRoundOf = true, KeyName = "AMP" });
            GarminEngineDataMessage.Add("TotalAircraftTime", ""); //TotalAircraftTime
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 13, index = 57, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "TotalAircraftTime" });
            GarminEngineDataMessage.Add("EngineTime", ""); //EngineTime
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 14, index = 62, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "EngineTime" });
            GarminEngineDataMessage.Add("Cht6", ""); //Cht6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 15, index = 67, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht6" });
            GarminEngineDataMessage.Add("Egt6", ""); //Egt6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 16, index = 71, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt6" });
            GarminEngineDataMessage.Add("Cht5", ""); //Cht5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 17, index = 75, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht5" });
            GarminEngineDataMessage.Add("Egt5", ""); //Egt5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 18, index = 79, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt5" });
            GarminEngineDataMessage.Add("Cht4", ""); //Cht4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 19, index = 83, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht4" });
            GarminEngineDataMessage.Add("Egt4", ""); //Egt4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 20, index = 87, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt4" });
            GarminEngineDataMessage.Add("Cht3", ""); //Cht3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 21, index = 91, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht3" });
            GarminEngineDataMessage.Add("Egt3", ""); //Egt3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 22, index = 95, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt3" });
            GarminEngineDataMessage.Add("Cht2", ""); //Cht2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 23, index = 99, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht2" });
            GarminEngineDataMessage.Add("Egt2", ""); //Egt2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 24, index = 103, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt2" });
            GarminEngineDataMessage.Add("Cht1", ""); //Cht1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 25, index = 107, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Cht1" });
            GarminEngineDataMessage.Add("Egt1", ""); //Egt1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 26, index = 111, value = 4, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "Egt1" });
            GarminEngineDataMessage.Add("TIT1", ""); //TIT1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 27, index = 115, value = 4, convertTo = "", IsRoundOf = false, KeyName = "TIT1" });
            GarminEngineDataMessage.Add("TIT2", ""); //TIT2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 28, index = 119, value = 4, convertTo = "", IsRoundOf = false, KeyName = "TIT2" });
            GarminEngineDataMessage.Add("ElevatorTrimPosition", ""); //ElevatorTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 29, index = 159, value = 5, convertTo = "", IsRoundOf = false, KeyName = "ElevatorTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator", ""); //UnitsIndicator
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 30, index = 164, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator" });
            GarminEngineDataMessage.Add("FlapPosition", ""); //FlapPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 31, index = 177, value = 5, convertTo = "CelciusToF", IsRoundOf = false, KeyName = "FlapPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator2", ""); //UnitsIndicator2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 32, index = 182, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator2" });
            GarminEngineDataMessage.Add("CarbTemp", ""); //CarbTemp
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 33, index = 123, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false, KeyName = "CarbTemp" });
            GarminEngineDataMessage.Add("UnitsIndicator3", ""); //UnitsIndicator3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 34, index = 128, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator3" });
            GarminEngineDataMessage.Add("CoolantPressure", ""); //CoolantPressure
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 35, index = 153, value = 5, convertTo = "OneHundredOfValue", IsRoundOf = false, KeyName = "CoolantPressure" });
            GarminEngineDataMessage.Add("UnitsIndicator4", ""); //UnitsIndicator4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 36, index = 158, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator4" });
            GarminEngineDataMessage.Add("CoolantTemperature", ""); //CoolantTemperature
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 37, index = 147, value = 5, convertTo = "CelciusToFAndOneTenth", IsRoundOf = false, KeyName = "CoolantTemperature" });
            GarminEngineDataMessage.Add("UnitsIndicator5", ""); //UnitsIndicator5
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 38, index = 152, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator5" });
            GarminEngineDataMessage.Add("AMP2", ""); //AMP2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 39, index = 129, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "AMP2" });
            GarminEngineDataMessage.Add("UnitsIndicator6", ""); //UnitsIndicator6
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 40, index = 134, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator6" });
            GarminEngineDataMessage.Add("AileronTrimPosition", ""); //AileronTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 41, index = 165, value = 5, convertTo = "", IsRoundOf = false, KeyName = "AileronTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator7", ""); //UnitsIndicator7
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 42, index = 170, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator7" });
            GarminEngineDataMessage.Add("RubberTrimPosition", ""); //RubberTrimPosition
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 43, index = 171, value = 5, convertTo = "", IsRoundOf = false, KeyName = "RubberTrimPosition" });
            GarminEngineDataMessage.Add("UnitsIndicator8", ""); //UnitsIndicator8
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 44, index = 176, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator8" });
            GarminEngineDataMessage.Add("FuelQty3", ""); //FuelQty3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 45, index = 135, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "FuelQty3" });
            GarminEngineDataMessage.Add("UnitsIndicator9", ""); //UnitsIndicator9
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 46, index = 140, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator9" });
            GarminEngineDataMessage.Add("FuelQty4", ""); //FuelQty4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 47, index = 141, value = 5, convertTo = "OneTenthOfValue", IsRoundOf = false, KeyName = "FuelQty4" });
            GarminEngineDataMessage.Add("UnitsIndicator10", ""); //UnitsIndicator10
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 48, index = 146, value = 1, convertTo = "", IsRoundOf = false, KeyName = "UnitsIndicator10" });
            GarminEngineDataMessage.Add("DiscreteInput1", ""); //DiscreteInput1
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 49, index = 201, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput1" });
            GarminEngineDataMessage.Add("DiscreteInput2", ""); //DiscreteInput2
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 50, index = 202, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput2" });
            GarminEngineDataMessage.Add("DiscreteInput3", ""); //DiscreteInput3
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 51, index = 203, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput3" });
            GarminEngineDataMessage.Add("DiscreteInput4", ""); //DiscreteInput4
            jPIHeaderCalculation.Add(new JPIHeaderCalculation { sno = 52, index = 204, value = 1, convertTo = "", IsRoundOf = false, KeyName = "DiscreteInput4" });


            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 1, index = 0, value = 4, convertTo = "", IsRoundOf = false }); //Ignstatus
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 2, index = 5, value = 4, convertTo = "", IsRoundOf = false }); //SensorStatus
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 3, index = 10, value = 4, convertTo = "", IsRoundOf = false }); //RPM
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 4, index = 15, value = 4, convertTo = "", IsRoundOf = false }); //Throttleposition
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 5, index = 20, value = 4, convertTo = "", IsRoundOf = false }); //Baro
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 6, index = 25, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //Airtemp
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 7, index = 31, value = 5, convertTo = "BarToPressurePerSquareInch", IsRoundOf = true }); //OILP
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 8, index = 37, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //OILT
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 9, index = 43, value = 5, convertTo = "BarToPressurePerSquareInch", IsRoundOf = true }); //FP
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 10, index = 49, value = 5, convertTo = "LitreToGallonPerHrs", IsRoundOf = true }); //FF
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 11, index = 55, value = 5, convertTo = "CelciusToF", IsRoundOf = false }); //Ecu temp
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 12, index = 61, value = 5, convertTo = "", IsRoundOf = false }); //Battery voltage
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 13, index = 67, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 14, index = 72, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 15, index = 77, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 16, index = 82, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 17, index = 87, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 18, index = 92, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //CHT6
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 19, index = 97, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 20, index = 102, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 21, index = 107, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 22, index = 112, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 23, index = 117, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 24, index = 122, value = 4, convertTo = "CelciusToF", IsRoundOf = false }); //EGT6
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 25, index = 127, value = 4, convertTo = "", IsRoundOf = false }); //Sensor1
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 26, index = 132, value = 4, convertTo = "", IsRoundOf = false }); //Sensor2
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 27, index = 137, value = 4, convertTo = "", IsRoundOf = false }); //Sensor3
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 28, index = 142, value = 4, convertTo = "", IsRoundOf = false }); //Sensor4
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 29, index = 147, value = 4, convertTo = "", IsRoundOf = false }); //Sensor5
            jPIHeaderCalculationULP.Add(new JPIHeaderCalculation { sno = 30, index = 152, value = 4, convertTo = "OneTenthOfValue", IsRoundOf = true }); //MAP

            dt_AirframeDataLog.Columns.Add("Id", typeof(int));
            dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
            dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
            dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));
            int? at = null;
            ////logger.Fatal("Parsing Started at " + DateTime.Now);
            var context = new GuardianAvionicsEntities();
            FileStream fileStream;
            BinaryReader binaryReader;
            int status = 0;
            Int32 transactionId = 0;
            Int32 lastTransactionId = -1;
            int messageLength = 0;
            int messageType = 0;
            string content = string.Empty;
            long aircraftId = 0;
            int? aeroUnitId = null;
            List<string> commandList = new List<string>();



            //List<string> filenames = new List<string>();
            //filenames.Add("0635898497212804000.data");
            var parseDataFileList = context.ParseDataFiles.Where(p => p.StatusId == 3 && p.AircraftId == parseFilesForaircraftId && p.AircraftId != null).OrderBy(o => o.Id).ToList();
            if (parseDataFileList.Count == 0)
            {
                return;
            }
            //logger.Fatal("Parsing started For Testing for aircraft  " + parseFilesForaircraftId);
            //for (int t = 1; t < 50; t++)
            //{
            //tempFlightId = t;
            foreach (var parseDataFile in parseDataFileList)
            {
                try
                {
                    fileStream = new FileStream(GA.Common.ConfigurationReader.DataFilePath + parseDataFile.FileName, FileMode.Open);
                    if (fileStream.Length <= 0)
                    {
                        parseDataFile.StatusId = 1;
                        context.SaveChanges();
                        continue;
                    }
                    binaryReader = new BinaryReader(fileStream);
                    byte[] bin = binaryReader.ReadBytes(Convert.ToInt32(fileStream.Length));

                    Encoding enc = Encoding.ASCII;
                    // binaryReader.BaseStream.Position = 12; // 12 is set because the file has --(Null)-- string in the begining of the file
                    binaryReader.BaseStream.Position = 0;
                    aircraftId = binaryReader.ReadInt64();
                    aircraftId = (long)parseFilesForaircraftId;
                    //  strLogger.Append("aircraftId = " + aircraftId + " FileName : " + parseDataFile.FileName + Environment.NewLine);
                    //logger.Fatal("aircraftId = " + aircraftId + " FileName : " + parseDataFile.FileName + Environment.NewLine);
                    long count = bin.Count();

                    lastTransactionId = -1;
                    aircraftProfile = null;
                    aircraftProfile = context.AircraftProfiles.FirstOrDefault(a => a.Id == aircraftId && !a.Deleted);

                    if (aircraftProfile == null)
                    {
                        parseDataFile.StatusId = 2;
                        parseDataFile.Remark = "Aircraft Id = " + aircraftId + " is not Exists. File Name = " + parseDataFile;
                        context.SaveChanges();
                        strLogger.Append("Execption :  Aircraft Id = " + aircraftId + " is not Exists. File Name = " + parseDataFile.FileName + Environment.NewLine);
                        ////logger.Fatal("Execption :  Aircraft Id = " + aircraftId + " is not Exists. File Name = " + parseDataFile.FileName);
                        continue;
                    }
                    g_PilotLog = new PilotLog();
                    g_pilotLogRawData = new PilotLogRawData();
                    g_FirstDataLog = new AirframeDatalog();
                    g_LastDataLog = new AirframeDatalog();
                    dt_AirframeDataLog.Rows.Clear();
                    g_PilotLogList = context.PilotLogs.Include(p => p.AirframeDatalogs).Include(p => p.PilotLogRawDatas).Where(p => p.AircraftId == aircraftId)
                                               .OrderByDescending(p => p.Id).ToList();
                    g_PilotLog = g_PilotLogList.FirstOrDefault();

                    if (g_PilotLog != null)
                    {
                        var objDataLog = g_PilotLogList.FirstOrDefault().AirframeDatalogs;
                        g_pilotLogRawData = g_PilotLogList.FirstOrDefault().PilotLogRawDatas.FirstOrDefault();
                        if (objDataLog != null)
                        {
                            g_FirstDataLog = objDataLog.FirstOrDefault();
                            g_LastDataLog = objDataLog.LastOrDefault();
                        }
                    }
                    else
                    {
                        g_pilotLogRawData.JPIHD = aircraftProfile.JpiHeader;
                    }

                    var aeroUnitMaster = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraftProfile.Id && !a.Blocked && !a.Deleted);
                    if (aeroUnitMaster == null)
                    {
                        aeroUnitId = null;
                        ////logger.Fatal("Execption :  AeroUnit not exists for the AircraftId = " + aircraftProfile.Id);
                        strLogger.Append("Execption :  AeroUnit not exists for the AircraftId = " + aircraftProfile.Id + Environment.NewLine);
                    }
                    aeroUnitId = aeroUnitMaster.Id;

                    lastTransactionId = Convert.ToInt32(aircraftProfile.LastTransactionId);
                    int temp1 = 0;
                    int temp2 = 0;
                    while (binaryReader.PeekChar() != -1 && binaryReader.BaseStream.Position <= count - 18)
                    {
                        status = binaryReader.ReadByte();
                        transactionId = binaryReader.ReadInt32();
                        temp1 = binaryReader.ReadByte();
                        temp2 = binaryReader.ReadByte();
                        messageLength = ((temp1 << 8) | temp2);

                        //messageLength = binaryReader.ReadInt16();
                        messageType = binaryReader.ReadByte();
                        ////logger.Fatal("Array Count = " + bin.Length + " , Pointer Position = " + binaryReader.BaseStream.Position + " , Message Length = " + (messageLength - 9));
                        content = enc.GetString(bin, Convert.ToInt32(binaryReader.BaseStream.Position), messageLength - 11); //-8-1

                        binaryReader.BaseStream.Position = binaryReader.BaseStream.Position + (messageLength - 6);
                        strLogger.Append("Test TransactionId = " + transactionId + " Thread Id = " + parseFilesForaircraftId + " " + DateTime.Now + Environment.NewLine);
                        ////logger.Fatal("Status Id = " + status + Environment.NewLine + "TransactionId = " + transactionId + Environment.NewLine + "Message Length = " + messageLength + Environment.NewLine + "Message Type = " + messageType + Environment.NewLine + "MessageContent = " + content);
                        strLogger.Append("Status Id = " + status + Environment.NewLine + "TransactionId = " + transactionId + Environment.NewLine + "Message Length = " + messageLength + Environment.NewLine + "Message Type = " + messageType + Environment.NewLine + "MessageContent = " + content + Environment.NewLine);
                        ////logger.Fatal("Last Transaction Id = " + lastTransactionId + " and current Transaction Id = " + transactionId);
                        strLogger.Append("Last Transaction Id = " + lastTransactionId + " and current Transaction Id = " + transactionId + Environment.NewLine);

                        if (lastTransactionId > transactionId)
                        {
                            ////logger.Fatal("Command Reject : Data for TransactionId = " + transactionId + " is already inserted");
                            strLogger.Append("Command Reject : Data for TransactionId = " + transactionId + " is already inserted" + Environment.NewLine);
                        }
                        else
                        {
                            if (transactionId - lastTransactionId > 100)
                            {
                                strLogger.Append("Invalid New Transaction Id, Id = " + transactionId + Environment.NewLine);
                            }
                            else
                            {
                                if (transactionId - lastTransactionId > 1)
                                {
                                    for (int i = 1; i < (transactionId - lastTransactionId); i++)
                                    {
                                        strMissingIds.Append("Missing Id = " + (lastTransactionId + i));
                                    }
                                }
                            }

                            lastTransactionId = transactionId;
                            // move to parse command
                            commandList.Clear();
                            //commandList = content.Replace("\\r\\n", "\r\n").Split("\r\n").ToList();
                            commandList = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                            strLogger.Append(commandList.Count.ToString());

                            for (int q = 0; q < commandList.Count; q++)
                            {
                                strLogger.Append("CommandNo. " + q + commandList[q] + Environment.NewLine);
                                ParsingCommands(transactionId + "," + commandList[q], aircraftId, aeroUnitId);
                            }
                        }
                    }
                    parseDataFile.StatusId = 1;
                    //aircraftProfile.LastTransactionId = Convert.ToString(transactionId);
                    aircraftProfile.LastTransactionId = Convert.ToString(lastTransactionId);
                    aircraftProfile.LastUpdated = DateTime.UtcNow;

                    if (g_PilotLog != null)
                    {
                        var PilotLogtoBeUpdate = context.PilotLogs.Include(f => f.PilotLogRawDatas).FirstOrDefault(f => f.Id == g_PilotLog.Id);

                        PilotLogtoBeUpdate.Actual = g_PilotLog.Actual;
                        PilotLogtoBeUpdate.AeroUnitMasterId = g_PilotLog.AeroUnitMasterId;
                        PilotLogtoBeUpdate.AircraftId = g_PilotLog.AircraftId;
                        PilotLogtoBeUpdate.CoPilotId = g_PilotLog.CoPilotId;
                        PilotLogtoBeUpdate.CrossCountry = g_PilotLog.CrossCountry;
                        PilotLogtoBeUpdate.Date = g_PilotLog.Date;
                        PilotLogtoBeUpdate.DayPIC = g_PilotLog.DayPIC;
                        PilotLogtoBeUpdate.Deleted = g_PilotLog.Deleted;
                        PilotLogtoBeUpdate.Finished = g_PilotLog.Finished;
                        PilotLogtoBeUpdate.FlightId = g_PilotLog.FlightId;
                        PilotLogtoBeUpdate.Hood = g_PilotLog.Hood;
                        PilotLogtoBeUpdate.IsEmailSent = g_PilotLog.IsEmailSent;
                        PilotLogtoBeUpdate.IsSavedOnDropbox = g_PilotLog.IsSavedOnDropbox;
                        PilotLogtoBeUpdate.LastUpdated = DateTime.UtcNow;
                        PilotLogtoBeUpdate.NightPIC = g_PilotLog.NightPIC;
                        PilotLogtoBeUpdate.PilotId = g_PilotLog.PilotId;
                        PilotLogtoBeUpdate.CommandRecFrom = g_PilotLog.CommandRecFrom;
                        var PilotLogRawDataToUpdate = PilotLogtoBeUpdate.PilotLogRawDatas.FirstOrDefault();
                        PilotLogRawDataToUpdate.GPGGA = g_pilotLogRawData.GPGGA;
                        PilotLogRawDataToUpdate.GPRMC = g_pilotLogRawData.GPRMC;
                        PilotLogRawDataToUpdate.JPIDT = g_pilotLogRawData.JPIDT;
                        PilotLogRawDataToUpdate.JPIHD = g_pilotLogRawData.JPIHD;
                        PilotLogRawDataToUpdate.PGAVF = g_pilotLogRawData.PGAVF;
                        PilotLogRawDataToUpdate.PGAV2 = g_pilotLogRawData.PGAV2;
                        PilotLogRawDataToUpdate.PGAVW = g_pilotLogRawData.PGAVW;
                        PilotLogRawDataToUpdate.RPYL = g_pilotLogRawData.RPYL;

                        context.SaveChanges();
                        SqlParameter[] param = new SqlParameter[1];
                        param[0] = new SqlParameter();
                        param[0].ParameterName = "tbAirframeDatalog";
                        param[0].SqlDbType = SqlDbType.Structured;
                        param[0].Value = dt_AirframeDataLog;

                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLog", param));
                        dt_AirframeDataLog.Rows.Clear();
                        fileStream.Close();
                        binaryReader.Close();
                    }
                    else
                    {
                        dt_AirframeDataLog.Rows.Clear();
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    parseDataFile.StatusId = 2;
                    parseDataFile.Remark = "Execption  while parsing the file " + parseDataFile.FileName + ". Transaction Id = " + transactionId + "Error Message : " + Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                    //aircraftProfile.LastTransactionId = Convert.ToString(transactionId);
                    aircraftProfile.LastTransactionId = Convert.ToString(lastTransactionId);
                    aircraftProfile.LastUpdated = DateTime.UtcNow;

                    if (g_PilotLog != null)
                    {
                        var PilotLogtoBeUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == g_PilotLog.Id);

                        PilotLogtoBeUpdate.Actual = g_PilotLog.Actual;
                        PilotLogtoBeUpdate.AeroUnitMasterId = g_PilotLog.AeroUnitMasterId;
                        PilotLogtoBeUpdate.AircraftId = g_PilotLog.AircraftId;
                        PilotLogtoBeUpdate.CoPilotId = g_PilotLog.CoPilotId;
                        PilotLogtoBeUpdate.CrossCountry = g_PilotLog.CrossCountry;
                        PilotLogtoBeUpdate.Date = g_PilotLog.Date;
                        PilotLogtoBeUpdate.DayPIC = g_PilotLog.DayPIC;
                        PilotLogtoBeUpdate.Deleted = g_PilotLog.Deleted;
                        PilotLogtoBeUpdate.Finished = g_PilotLog.Finished;
                        PilotLogtoBeUpdate.FlightId = g_PilotLog.FlightId;
                        PilotLogtoBeUpdate.Hood = g_PilotLog.Hood;
                        PilotLogtoBeUpdate.IsEmailSent = g_PilotLog.IsEmailSent;
                        PilotLogtoBeUpdate.IsSavedOnDropbox = g_PilotLog.IsSavedOnDropbox;
                        PilotLogtoBeUpdate.LastUpdated = DateTime.UtcNow;
                        PilotLogtoBeUpdate.NightPIC = g_PilotLog.NightPIC;
                        PilotLogtoBeUpdate.PilotId = g_PilotLog.PilotId;
                        PilotLogtoBeUpdate.CommandRecFrom = g_PilotLog.CommandRecFrom;
                        context.SaveChanges();

                        SqlParameter[] param = new SqlParameter[1];
                        param[0] = new SqlParameter();
                        param[0].ParameterName = "tbAirframeDatalog";
                        param[0].SqlDbType = SqlDbType.Structured;
                        param[0].Value = dt_AirframeDataLog;

                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLog", param));
                        dt_AirframeDataLog.Rows.Clear();
                        ////logger.Fatal("Execption  while parsing the file " + parseDataFile.FileName + " " + ex.StackTrace);
                        strLogger.Append("Execption  while parsing the file " + parseDataFile.FileName + " " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + Environment.NewLine);
                        //logger.Fatal(strLogger.ToString());
                        //logger.Fatal(strMissingIds.ToString());
                        strLogger.Clear();
                        strMissingIds.Clear();
                    }
                    else
                    {
                        dt_AirframeDataLog.Rows.Clear();

                    }
                }
                finally
                {

                }
                //logger.Fatal("Parsing End for file " + parseDataFile.FileName + " on " + DateTime.Now);
                // strLogger.Append("Parsing End for file " + parseDataFile + " on " + DateTime.Now + Environment.NewLine);
                //logger.Fatal(strLogger.ToString());
                strLogger.Clear();
                strMissingIds.Clear();
            }

            //}
            aircraftIdList.Remove(parseFilesForaircraftId);
            //logger.Fatal("Parsing End For Testing for aircraft  " + parseFilesForaircraftId);
        }

        public void ParsingCommands(string storedDataCommand, long storedDataAircraftId, int? aeroUnitId)
        {
            try
            {
                #region ParseCommand
                g_commandArray.Clear();
                g_commandArray = storedDataCommand.Split(',').ToList();
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                strLogger.Append("g_commandArray[1] = " + g_commandArray[1]);
                var context = new GuardianAvionicsEntities();

                //string[] rawData = storedDataCommand.Split(new[] { "/n" }, StringSplitOptions.RemoveEmptyEntries);
                //var aircraftProfile = context.AircraftProfiles.FirstOrDefault(a => a.Id == storedDataAircraftId);

                //foreach (var command in rawData)
                //{
                //aircraftProfile.LastTransactionId = (storeedDataArray[0]);
                //aircraftProfile.LastUpdated = DateTime.UtcNow;
                //context.SaveChanges();

                //var pilotLog = context.PilotLogs.Where(p => p.AircraftId == storedDataAircraftId)
                //                                      .OrderByDescending(p => p.Id)
                //                                      .Include(p => p.PilotLogRawDatas)
                //                                      .FirstOrDefault();
                switch (g_commandArray[1])
                {
                    case "$PGAVF":
                        {
                            //check the command recieved for the previous flight or new flight
                            var commandDataList = storedDataCommand.Split(',').ToList();
                            if (commandDataList.Count == 3)
                            {
                                if (storedDataCommand.Contains("END") && g_PilotLog != null)
                                {
                                    if (g_PilotLog.Finished == false)
                                    {
                                        g_PilotLog.Finished = true;

                                        double flightTimeInMinute = 0;
                                        flightTimeInMinute = ConvertHHMMToMinutes(g_PilotLog.DayPIC);

                                        // comma seperated string hobbsTime,hobbsTimeOffset,TachTime,TachTimeOffset,flightTime.
                                        string emailData = (aircraftProfile.HobbsTime ?? 0).ToString() + "," + (aircraftProfile.HobbsTimeOffset ?? 0).ToString() + "," + (aircraftProfile.TachTime ?? 0).ToString() + "," + (aircraftProfile.TachTimeOffset ?? 0) + "," + flightTimeInMinute.ToString();
                                        var emailDataForFinishedFlights = context.EmailDataForFinishedFlights.Create();
                                        emailDataForFinishedFlights.PilotLogId = g_PilotLog.Id;
                                        emailDataForFinishedFlights.EmailData = emailData;
                                        context.EmailDataForFinishedFlights.Add(emailDataForFinishedFlights);


                                        aircraftProfile.HobbsTime = flightTimeInMinute + aircraftProfile.HobbsTime;
                                        aircraftProfile.TachTime = flightTimeInMinute + aircraftProfile.TachTime;
                                        aircraftProfile.CurrentEngineTime = flightTimeInMinute + aircraftProfile.CurrentEngineTime;
                                        aircraftProfile.Prop1Time = flightTimeInMinute + aircraftProfile.Prop1Time;
                                        aircraftProfile.Engine2Time = flightTimeInMinute + aircraftProfile.Engine2Time;
                                        aircraftProfile.Prop2Time = flightTimeInMinute + aircraftProfile.Prop2Time;
                                        aircraftProfile.LastTransactionId = (g_commandArray[0]);
                                        aircraftProfile.LastUpdated = DateTime.UtcNow;
                                        var PilotLogtoBeUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == g_PilotLog.Id);

                                        PilotLogtoBeUpdate.Actual = g_PilotLog.Actual;
                                        PilotLogtoBeUpdate.AeroUnitMasterId = g_PilotLog.AeroUnitMasterId;
                                        PilotLogtoBeUpdate.AircraftId = g_PilotLog.AircraftId;
                                        PilotLogtoBeUpdate.CoPilotId = g_PilotLog.CoPilotId;
                                        PilotLogtoBeUpdate.CrossCountry = g_PilotLog.CrossCountry;
                                        PilotLogtoBeUpdate.Date = g_PilotLog.Date;
                                        PilotLogtoBeUpdate.DayPIC = g_PilotLog.DayPIC;
                                        PilotLogtoBeUpdate.Deleted = g_PilotLog.Deleted;
                                        PilotLogtoBeUpdate.Finished = g_PilotLog.Finished;
                                        PilotLogtoBeUpdate.FlightId = g_PilotLog.FlightId;
                                        PilotLogtoBeUpdate.Hood = g_PilotLog.Hood;
                                        PilotLogtoBeUpdate.IsEmailSent = g_PilotLog.IsEmailSent;
                                        PilotLogtoBeUpdate.IsSavedOnDropbox = g_PilotLog.IsSavedOnDropbox;
                                        PilotLogtoBeUpdate.LastUpdated = DateTime.UtcNow;
                                        PilotLogtoBeUpdate.NightPIC = g_PilotLog.NightPIC;
                                        PilotLogtoBeUpdate.PilotId = g_PilotLog.PilotId;
                                        PilotLogtoBeUpdate.CommandRecFrom = g_PilotLog.CommandRecFrom;
                                        context.SaveChanges();


                                        SqlParameter[] param = new SqlParameter[1];
                                        param[0] = new SqlParameter();
                                        param[0].ParameterName = "tbAirframeDatalog";
                                        param[0].SqlDbType = SqlDbType.Structured;
                                        param[0].Value = dt_AirframeDataLog;

                                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLog", param));
                                        dt_AirframeDataLog.Rows.Clear();
                                        //logger.Fatal(strLogger.ToString());
                                        //logger.Fatal(strMissingIds.ToString());
                                        strLogger.Clear();
                                        strMissingIds.Clear();

                                        var itemToRemove = g_PilotLogList.Single(r => r.Id == g_PilotLog.Id);
                                        g_PilotLogList.Remove(itemToRemove);
                                        g_PilotLogList.Add(PilotLogtoBeUpdate);

                                        g_pilotLogRawData = new PilotLogRawData();

                                    }
                                    break;
                                }
                                else
                                {
                                    ////logger.Fatal("Exception : Invalid PGAVF Command " + storedDataCommand);
                                    strLogger.Append("Exception : Invalid PGAVF Command " + storedDataCommand + Environment.NewLine);
                                    break;
                                }
                            }
                            else
                            {
                                if (commandDataList.Count != 8 && commandDataList.Count != 6)
                                {
                                    ////logger.Fatal("Exception : Invalid PGAVF Command " + storedDataCommand);
                                    strLogger.Append("Exception : Invalid PGAVF Command " + storedDataCommand + Environment.NewLine);
                                    break;
                                }
                            }
                            int lastIndex = commandDataList.Count - 1;
                            int index = commandDataList[lastIndex].IndexOf('*');
                            commandDataList[lastIndex] = commandDataList[lastIndex].Substring(0, index);

                            //check that the record is available for the aircraftid and flightId
                            int aircraftId = Convert.ToInt32(storedDataAircraftId);
                            int flightId = Convert.ToInt32(commandDataList[3]);


                            if (!g_PilotLogList.Where(p => p.AircraftId == aircraftId && p.FlightId == flightId && p.AeroUnitMasterId == aeroUnitId).Any())
                            {
                                //Now first check that the previous flight exist or not. If exist then check that the finished status is true or not
                                //int prevFlightId = flightId - 1;
                                var prevPilotLog = g_PilotLogList.OrderByDescending(p => p.FlightId).FirstOrDefault();//context.PilotLogs.Include(p => p.AirframeDatalogs).Include(p => p.PilotLogRawDatas).FirstOrDefault(p => p.AircraftId == aircraftId && p.FlightId == prevFlightId && p.AeroUnitMasterId == aeroUnitId);
                                if (prevPilotLog != null)
                                {
                                    if (prevPilotLog.Finished == false && prevPilotLog.DayPIC == "00:00:00")
                                    {
                                        context.AirframeDatalogs.RemoveRange(prevPilotLog.AirframeDatalogs);
                                        context.PilotLogRawDatas.RemoveRange(prevPilotLog.PilotLogRawDatas);
                                        context.PilotLogs.Remove(prevPilotLog);
                                        g_PilotLogList.Remove(prevPilotLog);

                                    }
                                    else if (prevPilotLog.Finished == false)
                                    {
                                        g_PilotLogList.Remove(prevPilotLog);
                                        if (g_PilotLog.Id == prevPilotLog.Id)
                                        {
                                            var PilotLogtoBeUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == g_PilotLog.Id);

                                            PilotLogtoBeUpdate.Actual = g_PilotLog.Actual;
                                            PilotLogtoBeUpdate.AeroUnitMasterId = g_PilotLog.AeroUnitMasterId;
                                            PilotLogtoBeUpdate.AircraftId = g_PilotLog.AircraftId;
                                            PilotLogtoBeUpdate.CoPilotId = g_PilotLog.CoPilotId;
                                            PilotLogtoBeUpdate.CrossCountry = g_PilotLog.CrossCountry;
                                            PilotLogtoBeUpdate.Date = g_PilotLog.Date;
                                            PilotLogtoBeUpdate.DayPIC = g_PilotLog.DayPIC;
                                            PilotLogtoBeUpdate.Deleted = g_PilotLog.Deleted;
                                            PilotLogtoBeUpdate.Finished = true;
                                            PilotLogtoBeUpdate.FlightId = g_PilotLog.FlightId;
                                            PilotLogtoBeUpdate.Hood = g_PilotLog.Hood;
                                            PilotLogtoBeUpdate.IsEmailSent = g_PilotLog.IsEmailSent;
                                            PilotLogtoBeUpdate.IsSavedOnDropbox = g_PilotLog.IsSavedOnDropbox;
                                            PilotLogtoBeUpdate.LastUpdated = DateTime.UtcNow;
                                            PilotLogtoBeUpdate.NightPIC = g_PilotLog.NightPIC;
                                            PilotLogtoBeUpdate.PilotId = g_PilotLog.PilotId;
                                            PilotLogtoBeUpdate.CommandRecFrom = g_PilotLog.CommandRecFrom;
                                            if (dt_AirframeDataLog.Rows.Count > 0)
                                            {
                                                if (Convert.ToInt32(dt_AirframeDataLog.Rows[0]["PilotLogId"]) == g_PilotLog.Id)
                                                {
                                                    SqlParameter[] param = new SqlParameter[1];
                                                    param[0] = new SqlParameter();
                                                    param[0].ParameterName = "tbAirframeDatalog";
                                                    param[0].SqlDbType = SqlDbType.Structured;
                                                    param[0].Value = dt_AirframeDataLog;

                                                    int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLog", param));
                                                    dt_AirframeDataLog.Rows.Clear();
                                                }
                                            }
                                        }
                                        prevPilotLog.Finished = true;
                                        g_PilotLogList.Add(prevPilotLog);
                                    }
                                }
                                //Here insert data into pilot logs
                                var pilotLogData = context.PilotLogs.Create();
                                pilotLogData.AircraftId = Convert.ToInt32(storedDataAircraftId);
                                int? pilotId = (commandDataList[4] == "default" || commandDataList[4] == "noname") ? (int?)null : Convert.ToInt32(commandDataList[4]);

                                if (pilotId != null)
                                {
                                    //check that the pilot is exists or not
                                    var userDetail = context.Profiles.FirstOrDefault(u => u.Id == pilotId);
                                    pilotLogData.PilotId = userDetail == null ? null : pilotId;
                                }
                                else
                                {
                                    pilotLogData.PilotId = null;
                                }

                                pilotLogData.LastUpdated = Convert.ToDateTime("1/1/1990");
                                pilotLogData.Date = Convert.ToDateTime("1/1/1990");
                                pilotLogData.LastUpdated = DateTime.UtcNow;
                                pilotLogData.Deleted = false;
                                pilotLogData.Finished = false;
                                pilotLogData.IsSavedOnDropbox = false;
                                pilotLogData.Actual = "00:00:00";
                                pilotLogData.CrossCountry = "00:00:00";
                                pilotLogData.DayPIC = "00:00:00";
                                pilotLogData.NightPIC = "00:00:00";
                                pilotLogData.Hood = "00:00:00";
                                pilotLogData.IsEmailSent = false;
                                pilotLogData.AeroUnitMasterId = aeroUnitId;

                                int? coPilotId = null;
                                if (commandDataList.Count > 6)
                                {
                                    coPilotId = commandDataList[6] == "default" ? (int?)null : Convert.ToInt32(commandDataList[6]);
                                }
                                else
                                {
                                    coPilotId = null;
                                }
                                if (coPilotId != null)
                                {
                                    //check that the pilot is exists or not
                                    var userDetail = context.Profiles.FirstOrDefault(u => u.Id == coPilotId);
                                    pilotLogData.CoPilotId = userDetail == null ? null : coPilotId;
                                }
                                else
                                {
                                    pilotLogData.CoPilotId = null;
                                }
                                pilotLogData.FlightId = flightId;
                                var pilotLogRawData = context.PilotLogRawDatas.Create();
                                commandDataList.RemoveRange(0, 2);
                                var temp = string.Join(",", commandDataList);
                                pilotLogRawData.PGAVF = temp;

                                pilotLogData.PilotLogRawDatas.Add(pilotLogRawData);
                                context.PilotLogs.Add(pilotLogData);

                                ////logger.Fatal("PilotId : " + pilotLogData.PilotId);
                                ////logger.Fatal("CoPilotId : " + pilotLogData.CoPilotId);

                                strLogger.Append("PilotId : " + pilotLogData.PilotId + " " + "CoPilotId : " + pilotLogData.CoPilotId + Environment.NewLine);
                                context.SaveChanges();
                                g_PilotLog = pilotLogData;
                                g_FirstDataLog = pilotLogData.AirframeDatalogs.FirstOrDefault();
                                g_LastDataLog = pilotLogData.AirframeDatalogs.LastOrDefault();
                                //g_PilotLogList = context.PilotLogs.Include(p => p.AirframeDatalogs).Include(p => p.PilotLogRawDatas).Where(p => p.AircraftId == aircraftId)
                                //               .OrderByDescending(p => p.Id).ToList();
                                g_PilotLogList.Add(pilotLogData);

                                g_pilotLogRawData = pilotLogRawData;
                            }
                            else
                            {
                                if (g_PilotLog.Finished == true)
                                {
                                    //record already inserted this flight

                                    //If pilotid is not null then only we will update the pilotid  
                                    int? pilotId = commandDataList[4] == "default" ? (int?)null : Convert.ToInt32(commandDataList[4]);
                                    if (pilotId != null)
                                    {
                                        //check that the pilot id exists or not
                                        var objProfile = context.Profiles.FirstOrDefault(u => u.Id == pilotId);
                                        if (objProfile != null)
                                        {
                                            g_PilotLog.PilotId = pilotId;
                                        }
                                        //pilotLog.PilotId = objProfile == null ? null : pilotId;
                                    }

                                    g_PilotLog.LastUpdated = DateTime.UtcNow;
                                    int? coPilotId = commandDataList[6] == "default" ? (int?)null : Convert.ToInt32(commandDataList[6]);

                                    if (coPilotId != null)
                                    {
                                        //check that the pilot is exists or not
                                        var objProfile = context.Profiles.FirstOrDefault(u => u.Id == coPilotId);
                                        if (objProfile != null)
                                        {
                                            g_PilotLog.CoPilotId = coPilotId;
                                        }
                                        //pilotLog.CoPilotId = objProfile == null ? null : coPilotId;
                                    }
                                    //else
                                    //{
                                    //    pilotLog.CoPilotId = null;
                                    //}

                                    //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                    commandDataList.RemoveRange(0, 2);
                                    var temp = string.Join(",", commandDataList);
                                    g_pilotLogRawData.PGAVF = temp;

                                    var PilotLogtoBeUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == g_PilotLog.Id);

                                    PilotLogtoBeUpdate.Actual = g_PilotLog.Actual;
                                    PilotLogtoBeUpdate.AeroUnitMasterId = g_PilotLog.AeroUnitMasterId;
                                    PilotLogtoBeUpdate.AircraftId = g_PilotLog.AircraftId;
                                    PilotLogtoBeUpdate.CoPilotId = g_PilotLog.CoPilotId;
                                    PilotLogtoBeUpdate.CrossCountry = g_PilotLog.CrossCountry;
                                    PilotLogtoBeUpdate.Date = g_PilotLog.Date;
                                    PilotLogtoBeUpdate.DayPIC = g_PilotLog.DayPIC;
                                    PilotLogtoBeUpdate.Deleted = g_PilotLog.Deleted;
                                    PilotLogtoBeUpdate.Finished = g_PilotLog.Finished;
                                    PilotLogtoBeUpdate.FlightId = g_PilotLog.FlightId;
                                    PilotLogtoBeUpdate.Hood = g_PilotLog.Hood;
                                    PilotLogtoBeUpdate.IsEmailSent = g_PilotLog.IsEmailSent;
                                    PilotLogtoBeUpdate.IsSavedOnDropbox = g_PilotLog.IsSavedOnDropbox;
                                    PilotLogtoBeUpdate.LastUpdated = DateTime.UtcNow;
                                    PilotLogtoBeUpdate.NightPIC = g_PilotLog.NightPIC;
                                    PilotLogtoBeUpdate.PilotId = g_PilotLog.PilotId;
                                    PilotLogtoBeUpdate.CommandRecFrom = g_PilotLog.CommandRecFrom;
                                    context.SaveChanges();
                                }
                            }
                            break;
                        }
                    case "$GPRMC":
                        {
                            if (g_PilotLog != null)
                            {
                                if (!g_PilotLog.Finished)
                                {
                                    //Here we have to get all the commands which are previously saved in the PilotLogRawData by aircraftid
                                    //var oldPilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                    var oldGprmc = string.IsNullOrEmpty(g_pilotLogRawData.GPRMC) ? new List<string>() : g_pilotLogRawData.GPRMC.Split(',').ToList();
                                    var oldJPIHD = string.IsNullOrEmpty(g_pilotLogRawData.JPIHD) ? (string.IsNullOrEmpty(aircraftProfile.JpiHeader) ? new List<string>() : aircraftProfile.JpiHeader.Split(',').ToList()) : g_pilotLogRawData.JPIHD.Split(',').ToList();
                                    var oldJpiDT = string.IsNullOrEmpty(g_pilotLogRawData.JPIDT) ? new List<string>() : g_pilotLogRawData.JPIDT.Split(',').ToList();
                                    var oldGpgga = string.IsNullOrEmpty(g_pilotLogRawData.GPGGA) ? new List<string>() : g_pilotLogRawData.GPGGA.Split(',').ToList();
                                    var oldRPYL = string.IsNullOrEmpty(g_pilotLogRawData.RPYL) ? new List<string>() : g_pilotLogRawData.RPYL.Split(',').ToList();
                                    dictionary.Clear();

                                    if (oldGprmc.Count > 0)
                                    {
                                        // dictionary.Add("Time", oldGprmc[0]);
                                        dictionary.Add("Latitude", oldGprmc[2]);
                                        dictionary.Add("Longitude", oldGprmc[3]);
                                        dictionary.Add("Speed", oldGprmc[4]);
                                        dictionary.Add("Date", FormatedDate(oldGprmc[6], oldGprmc[0]).ToString());

                                        if (g_PilotLog.Date.ToShortDateString() == "1/1/1990")
                                        {
                                            g_PilotLog.Date = FormatedDate(oldGprmc[4], oldGprmc[0]);
                                            g_PilotLog.LastUpdated = g_PilotLog.Date;
                                        }

                                        // //logger.Fatal("OldJPIDT Count = " + oldJpiDT.Count);
                                        // //logger.Fatal("oldJPIHD Count = " + oldJPIHD.Count);

                                        strLogger.Append("OldJPIDT Count = " + oldJpiDT.Count + " " + "oldJPIHD Count = " + oldJPIHD.Count + Environment.NewLine);

                                        if (g_PilotLog.CommandRecFrom == "Garmin")
                                        {
                                            foreach (var obj in GarminEngineDataMessage)
                                            {
                                                dictionary.Add(obj.Key, obj.Value);
                                            }

                                            foreach (var obj in GarminAltitudeAirDataMessage)
                                            {
                                                dictionary.Add(obj.Key, obj.Value);
                                            }
                                        }
                                        else
                                        {
                                            if (oldJpiDT.Count > 0 && oldJPIHD.Count > 0)
                                            {
                                                if (oldJPIHD.Count == oldJpiDT.Count)
                                                {
                                                    for (int i = 0; i < oldJPIHD.Count; i++)
                                                    {
                                                        if (!dictionary.ContainsKey(oldJPIHD[i]))
                                                        {
                                                            dictionary.Add(oldJPIHD[i], oldJpiDT[i]);
                                                        }

                                                    }
                                                }
                                            }
                                        }


                                        if (oldGpgga.Count > 0)
                                        {
                                            dictionary.Add("Altitude", oldGpgga[8]);
                                        }

                                        if (oldRPYL.Count > 0)
                                        {
                                            dictionary.Add("Roll", oldRPYL[0]);
                                            dictionary.Add("Pitch", oldRPYL[1]);
                                            dictionary.Add("Yaw", oldRPYL[4]);
                                        }

                                        if (dictionary.Count > 0)
                                        {
                                            //Insert json data in Airframe datalog
                                            DataRow dr = dt_AirframeDataLog.NewRow();
                                            dr[0] = 0;
                                            dr[1] = g_PilotLog.Id;
                                            dr[2] = dictionary["Date"];
                                            dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                            dt_AirframeDataLog.Rows.Add(dr);
                                            g_FirstDataLog = g_FirstDataLog ?? new AirframeDatalog();
                                            g_LastDataLog = g_LastDataLog ?? new AirframeDatalog();
                                            if (string.IsNullOrEmpty(g_FirstDataLog.DataLog))
                                            {
                                                g_FirstDataLog.DataLog = dt_AirframeDataLog.Rows[0]["DataLog"].ToString();
                                            }
                                            g_LastDataLog.DataLog = dt_AirframeDataLog.Rows[dt_AirframeDataLog.Rows.Count - 1]["DataLog"].ToString();



                                            dictionary.Clear();
                                            //context.SaveChanges();
                                        }
                                    }
                                    g_pilotLogRawData.GPRMC = string.Empty;
                                    //oldPilotLogRawData.JPIDT = string.Empty;
                                    //g_pilotLogRawData.GPGGA = string.Empty;

                                    //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                    var newGRRMC = storedDataCommand.Split(',').ToList();

                                    if (newGRRMC.Count != 14)
                                    {
                                        context.SaveChanges();
                                        break;
                                    }

                                    int lastIndex = newGRRMC.Count - 1;
                                    int index = newGRRMC[lastIndex].IndexOf('*');
                                    newGRRMC[lastIndex] = newGRRMC[lastIndex].Substring(0, index);
                                    newGRRMC.RemoveRange(0, 2);

                                    //Now update the Day Pic. Select the First and Last Airframe datalog
                                    //var firstAirframeDatalog = g_PilotLog.AirframeDatalogs.FirstOrDefault();
                                    //var lastAirframeDatalog = g_PilotLog.AirframeDatalogs.LastOrDefault();

                                    string firstAirframeDatalog = g_FirstDataLog == null ? "" : g_FirstDataLog.DataLog;
                                    string lastAirframeDatalog = g_LastDataLog == null ? "" : g_LastDataLog.DataLog;
                                    if (dt_AirframeDataLog.Rows.Count > 0)
                                    {
                                        if (firstAirframeDatalog == "")
                                        {
                                            firstAirframeDatalog = dt_AirframeDataLog.Rows[0]["DataLog"].ToString();
                                        }

                                        lastAirframeDatalog = dt_AirframeDataLog.Rows[dt_AirframeDataLog.Rows.Count - 1]["Datalog"].ToString();
                                    }

                                    if (firstAirframeDatalog != "" && lastAirframeDatalog != "")
                                    {
                                        var startDate = "";
                                        var endDate = "";
                                        var dictonaryFirstLogData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(firstAirframeDatalog);

                                        dictonaryFirstLogData.TryGetValue("Date", out startDate);

                                        var dictonaryLastLogData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(lastAirframeDatalog);
                                        dictonaryLastLogData.TryGetValue("Date", out endDate);
                                        if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && (Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).Ticks > 0)
                                        {
                                            var datetimediff = new DateTime((Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).Ticks);
                                            g_PilotLog.DayPIC = datetimediff.ToString("HH:mm:ss");
                                        }
                                    }

                                    //var arrRemoveZeroAfterDecimal = newGRRMC[2].Split('.');
                                    //if (arrRemoveZeroAfterDecimal[1].Substring(0, 3) == "000")
                                    //{
                                    //    arrRemoveZeroAfterDecimal[1] = arrRemoveZeroAfterDecimal[1].Substring(3, arrRemoveZeroAfterDecimal[1].Length - 3);
                                    //    newGRRMC[2] = arrRemoveZeroAfterDecimal[0] + "." + arrRemoveZeroAfterDecimal[1];
                                    //}

                                    //calculate the Latitude and longitude
                                    double degree = Convert.ToDouble(newGRRMC[2].Substring(0, 2));
                                    double minute = Convert.ToDouble(newGRRMC[2].Substring(2, newGRRMC[2].Length - 2));
                                    string direction = newGRRMC[3];
                                    double latitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                                    ////logger.Fatal("Lat Log Calculation - GPRMC : " + storedDataCommand + " Lat Degree : " + degree + " Lat Minute : " + minute + "Lat Direction : " + direction + " Calculated Latitude : " + latitude);
                                    strLogger.Append("Lat Log Calculation - GPRMC : " + storedDataCommand + " Lat Degree : " + degree + " Lat Minute : " + minute + "Lat Direction : " + direction + " Calculated Latitude : " + latitude + Environment.NewLine);


                                    //arrRemoveZeroAfterDecimal = newGRRMC[4].Split('.');
                                    //if (arrRemoveZeroAfterDecimal[1].Substring(0, 3) == "000")
                                    //{
                                    //    arrRemoveZeroAfterDecimal[1] = arrRemoveZeroAfterDecimal[1].Substring(3, arrRemoveZeroAfterDecimal[1].Length - 3);
                                    //    newGRRMC[4] = arrRemoveZeroAfterDecimal[0] + "." + arrRemoveZeroAfterDecimal[1];
                                    //}
                                    degree = Convert.ToDouble(newGRRMC[4].Substring(0, 3));
                                    minute = Convert.ToDouble(newGRRMC[4].Substring(3, newGRRMC[4].Length - 3));
                                    direction = newGRRMC[5];
                                    double longitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                                    ////logger.Fatal("Lat Log Calculation - GPRMC : " + storedDataCommand + " Log Degree : " + degree + " Log Minute : " + minute + "Log Direction : " + direction + " Calculated Longititude : " + longitude);
                                    strLogger.Append("Lat Log Calculation - GPRMC : " + storedDataCommand + " Log Degree : " + degree + " Log Minute : " + minute + "Log Direction : " + direction + " Calculated Longititude : " + longitude + Environment.NewLine);

                                    newGRRMC[2] = latitude.ToString();
                                    newGRRMC[4] = longitude.ToString();

                                    newGRRMC.RemoveAt(5); // remove the longitude Direction
                                    newGRRMC.RemoveAt(3); // remove the latitude direction

                                    if (g_PilotLog.Date.ToShortDateString() == "1/1/1990")
                                    {
                                        g_PilotLog.Date = FormatedDate(newGRRMC[6], newGRRMC[0]);
                                    }
                                    g_pilotLogRawData.GPRMC = string.Join(",", newGRRMC);
                                }

                                //Check if there is a data in dictionary
                                //context.SaveChanges();
                            }
                            break;
                        }
                    case "$JPIHD":
                        {
                            storedDataCommand = storedDataCommand.Replace("TIT-L", "TIT1").Replace("TIT-R", "TIT2").Replace("OIL-T", "OILT").Replace("OIL-P", "OILP").Replace("OAT-C", "OAT").Replace("WP REQ", "REQ").Replace("H:M", "HM").Replace("BAT", "VOLTS").Replace("FUEL-F", "FF").Replace(",TIT,", ",TIT1,");
                            var jpiHeader = storedDataCommand.Split(',').ToList();
                            jpiHeader.RemoveAt(jpiHeader.Count - 1);
                            jpiHeader.RemoveRange(0, 2);

                            //if (string.IsNullOrEmpty(aircraftProfile.JpiHeader))
                            //{
                            aircraftProfile.JpiHeader = string.Join(",", jpiHeader);
                            //context.SaveChanges();
                            //}

                            if (g_PilotLog != null)
                            {
                                //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                if (!g_PilotLog.Finished)
                                {
                                    g_pilotLogRawData.JPIHD = string.Join(",", jpiHeader);
                                    //context.SaveChanges();
                                }
                            }
                            break;
                        }
                    case "$JPIDT":
                        {

                            strLogger.Append("Parse $JPIDT " + ((g_PilotLog == null) ? " g_Pilot is null " : " g_pilot is not null ") + Environment.NewLine);

                            if (g_PilotLog != null)
                            {
                                g_PilotLog.CommandRecFrom = "JPI";
                                //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();

                                var jpiData = storedDataCommand.Split(',').ToList();

                                jpiData.RemoveAt(jpiData.Count - 1);
                                jpiData.RemoveRange(0, 2);
                                ////logger.Fatal("JPIDT Command" + string.Join(",", jpiData));

                                int jpiHeaderCount = 0;
                                //Check that JPIHD is available in the Aircraft Profile Table 
                                if (!string.IsNullOrEmpty(aircraftProfile.JpiHeader))
                                {
                                    jpiHeaderCount = aircraftProfile.JpiHeader.Count(f => f == ',') + 1;

                                    //Now check that header count and JPIData count id equal or not
                                    if (jpiHeaderCount == jpiData.Count)
                                    {
                                        double tempValue = 0.0;
                                        //first check that all the values be in double
                                        for (int a = 0; a < jpiData.Count; a++)
                                        {
                                            try
                                            {
                                                tempValue = Convert.ToDouble(jpiData[a]);
                                            }
                                            catch
                                            {
                                                strLogger.Append("Exception : JPIDT has invalid value " + jpiData[a] + Environment.NewLine);
                                                jpiData[a] = jpiData[a] == "NA" ? "" : "0.0";
                                            }
                                        }

                                        g_pilotLogRawData.JPIDT = string.Join(",", jpiData);
                                    }
                                    else
                                    {
                                        var prevJpiData = g_pilotLogRawData.JPIDT.Split(',').ToList();
                                        if (jpiHeaderCount == prevJpiData.Count)
                                        {
                                            g_pilotLogRawData.JPIDT = string.Join(",", prevJpiData);
                                        }
                                    }
                                    //check that the flight is finished or not
                                    if (!g_PilotLog.Finished)
                                    {
                                        //pilotLogRawData.JPIDT = string.Join(",", jpiData);
                                        //context.SaveChanges();
                                    }
                                }
                            }
                            //dictionary.Add();
                            break;
                        }
                    case "$GPGGA":
                        {
                            if (g_PilotLog != null)
                            {
                                //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                var gpgga = storedDataCommand.Split(',').ToList();

                                //if (gpgga.Count != 16)
                                if (gpgga.Count < 11)
                                {
                                    // //logger.Fatal("Command Reject : Incorrect GPGGA command = " + storedDataCommand + ". Incorrect columns count");
                                    strLogger.Append("Command Reject : Incorrect GPGGA command = " + storedDataCommand + ". Incorrect columns count" + Environment.NewLine);
                                    break;
                                }

                                //int lastIndex = gpgga.Count - 1;
                                //int index = gpgga[lastIndex].IndexOf('*');
                                //gpgga[lastIndex] = gpgga[lastIndex].Substring(0, index - 1);
                                gpgga.RemoveRange(0, 2);

                                var flag = false;

                                //Now check the value of altitude is correct or not ;
                                try
                                {
                                    double altitude = Convert.ToDouble(gpgga[8]);
                                    flag = true;
                                    gpgga[8] = (Math.Ceiling(altitude * 3.28084)).ToString();
                                }
                                catch (Exception)
                                {
                                    //Value of the altitude is not correct now we fetch the previously inserted value
                                    var prevGpgga = g_pilotLogRawData.GPGGA.Split(',').ToList();

                                    if (prevGpgga.Count >= 9)
                                    {
                                        gpgga[8] = prevGpgga[8];
                                        flag = true;
                                    }
                                }

                                //check that the flight is finished or not
                                if (!g_PilotLog.Finished && flag)
                                {
                                    g_pilotLogRawData.GPGGA = string.Join(",", gpgga);
                                    //context.SaveChanges();
                                }
                            }
                            break;
                        }
                    case "$RPYL":
                        {
                            if (g_PilotLog != null)
                            {
                                var rpyl = storedDataCommand.Split(',').ToList();
                                if (rpyl.Count >= 6)
                                {
                                    rpyl.RemoveRange(0, 2);
                                }
                                else
                                {
                                    strLogger.Append("Invalid RPYL command");
                                }

                                if (!g_PilotLog.Finished)
                                {
                                    g_pilotLogRawData.RPYL = string.Join(",", rpyl);
                                }
                            }

                            break;
                        }
                    default:
                        {
                            strLogger.Append("In the Default Section of Switch case" + Environment.NewLine);

                            strLogger.Append("g_commandArray[1].Substring(0,2) = " + g_commandArray[1].Substring(0, 2) + Environment.NewLine);


                            // Parsing JPI command For Garmin
                            if (g_commandArray[1].Substring(0, 2) == "=3")
                            {
                                strLogger.Append("Command Length for Garmin = " + g_commandArray[1].Length + Environment.NewLine);

                                strLogger.Append("Garmin Command =  " + g_commandArray[1].ToString() + Environment.NewLine);
                                aircraftProfile.JpiHeader = JPIHeader;

                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);

                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in jPIHeaderCalculation.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        strLogger.Append(dict.index.ToString() + ", " + dict.value.ToString() + " = " + strValue + Environment.NewLine);

                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    // strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "CelciusToFAndOneTenth":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToFAndOneTenth(Convert.ToDouble(strValue));
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        // strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneHundredOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneHundredOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminEngineDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminEngineDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    GarminEngineDataMessage[dict.KeyName] = strValue;
                                                    //strJPIDT.Append(strValue);
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        strLogger.Append("Exception while reading the =3 command with dictionary " + ex.Message + Environment.NewLine);
                                    }
                                }
                                //strJPIDT.Remove(strJPIDT.Length - 1, 1);

                                if (g_PilotLog != null)
                                {
                                    //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                    if (!g_PilotLog.Finished)
                                    {
                                        //g_pilotLogRawData.JPIHD = JPIHeader;
                                        g_PilotLog.CommandRecFrom = "Garmin";

                                        //context.SaveChanges();
                                    }
                                    //g_pilotLogRawData.JPIDT = strJPIDT.ToString();

                                }
                            }
                            else if (g_commandArray[1].Substring(0, 2) == "=1")
                            {

                                aircraftProfile.JpiHeader = JPIHeader;

                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);

                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in GarminAltitudeAirDataValues.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        strLogger.Append(dict.index.ToString() + ", " + dict.value.ToString() + " = " + strValue + Environment.NewLine);

                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    // strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "CelciusToFAndOneTenth":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToFAndOneTenth(Convert.ToDouble(strValue));
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        // strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "OneHundredOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneHundredOfValue(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                        //strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        GarminAltitudeAirDataMessage[dict.KeyName] = "";
                                                        //strJPIDT.Append("");
                                                    }
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    GarminAltitudeAirDataMessage[dict.KeyName] = strValue;
                                                    //strJPIDT.Append(strValue);
                                                    //strJPIDT.Append(",");
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        strLogger.Append("Exception while reading the =3 command with dictionary " + ex.Message + Environment.NewLine);
                                    }
                                }
                                //strJPIDT.Remove(strJPIDT.Length - 1, 1);

                                if (g_PilotLog != null)
                                {
                                    if (!g_PilotLog.Finished)
                                    {
                                        //g_pilotLogRawData.JPIHD = JPIHeader;
                                        g_PilotLog.CommandRecFrom = "Garmin";
                                    }
                                }
                            }

                            //check for ULP command 
                            strLogger.Append("g_commandArray[1].Length = " + g_commandArray[1].Length + Environment.NewLine);
                            strLogger.Append("g_commandArray[1].Count = " + g_commandArray[1].Count() + Environment.NewLine);
                            if (g_commandArray[1].Length == 4 && g_commandArray.Count == 31)
                            {
                                strLogger.Append("ULP Command =  " + g_commandArray[1].ToString() + Environment.NewLine);
                                aircraftProfile.JpiHeader = ULPHeader;

                                g_commandArray.RemoveAt(0);

                                string jpiCommand = string.Join(",", g_commandArray);
                                StringBuilder strJPIDT = new StringBuilder();
                                string strValue = "";
                                foreach (var dict in jPIHeaderCalculationULP.OrderBy(o => o.sno).ToList())
                                {
                                    try
                                    {
                                        strValue = jpiCommand.Substring(dict.index, dict.value);
                                        strLogger.Append(dict.index.ToString() + ", " + dict.value.ToString() + " = " + strValue + Environment.NewLine);

                                        switch (dict.convertTo)
                                        {
                                            case "CelciusToF":
                                                {
                                                    try
                                                    {
                                                        strValue = CelciusToF(Convert.ToDouble(strValue));
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }

                                            case "OneTenthOfValue":
                                                {
                                                    try
                                                    {
                                                        strValue = OneTenthOfValue(Convert.ToDouble(strValue), true);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "LitreToGallonPerHrs":
                                                {
                                                    try
                                                    {
                                                        strValue = LitreToGallonPerHrs(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            case "BarToPressurePerSquareInch":
                                                {
                                                    try
                                                    {
                                                        strValue = BarToPressurePerSquareInch(Convert.ToDouble(strValue), dict.IsRoundOf);
                                                        strJPIDT.Append(strValue);
                                                    }
                                                    catch (Exception)
                                                    {
                                                        strJPIDT.Append("");
                                                    }
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                            default:
                                                {
                                                    strJPIDT.Append(strValue);
                                                    strJPIDT.Append(",");
                                                    break;
                                                }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        strLogger.Append("Exception while reading the ULP command with dictionary " + ex.Message + Environment.NewLine);
                                    }
                                }
                                strJPIDT.Remove(strJPIDT.Length - 1, 1); // remove ',' from the last index

                                if (g_PilotLog != null)
                                {
                                    //var pilotLogRawData = g_PilotLog.PilotLogRawDatas.FirstOrDefault();
                                    if (!g_PilotLog.Finished)
                                    {
                                        g_pilotLogRawData.JPIHD = ULPHeader;
                                        g_PilotLog.CommandRecFrom = "ULP";
                                        //context.SaveChanges();
                                    }
                                    g_pilotLogRawData.JPIDT = strJPIDT.ToString();
                                }
                            }
                            break;
                        }
                }
                //  }
                #endregion ParseCommand
            }
            catch (Exception e)
            {
                //  throw;
            }
        }


        public double CalculateLatitudeAndLongitude(double degree, double minute, string strDirection)
        {
            int latsign = 1;

            double second = 0.0;
            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (second / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }

        public DateTime FormatedDate(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(0, 2));
                dd = (dd > 31 || dd < 1) ? 1 : dd;

                var mm = Convert.ToInt32(date.Substring(2, 2));
                mm = (mm > 12 || mm < 1) ? 1 : mm;

                var yy = 0;
                if (date.Length == 6)
                {
                    yy = Convert.ToInt32(date.Substring(4, 2));
                    yy = (yy > 7999 || yy < 1) ? 1 : yy;
                    yy += 2000;
                }
                else
                {
                    yy = Convert.ToInt32(date.Substring(4, 4));
                }


                var h = Convert.ToInt32(time.Substring(0, 2));
                h = (h > 23 || h < 0) ? 1 : h;

                var m = Convert.ToInt32(time.Substring(2, 2));
                m = (m > 59 || m < 0) ? 1 : m;

                var s = Convert.ToInt32(time.Substring(4, 2));
                s = (s > 59 || s < 1) ? 1 : s;

                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));

        }


        public string OneTenthOfValue(double value, bool isroundOf)
        {
            if (isroundOf)
            {
                return Convert.ToString(Math.Round((value * 0.1)));
            }
            else
            {
                return Convert.ToString(Math.Round((value * 0.1), 1, MidpointRounding.ToEven));

            }
        }

        public string OneHundredOfValue(double value, bool isroundOf)
        {
            if (isroundOf)
            {
                return Convert.ToString(Math.Round((value * 0.01)));
            }
            else
            {
                return Convert.ToString(Math.Round((value * 0.01), 1, MidpointRounding.ToEven));

            }
        }

        public string CelciusToF(double value)
        {
            return Convert.ToString(((value * 1.8) + 32));
        }

        public string CelciusToFAndOneTenth(double value)
        {
            return Convert.ToString(((value * 1.8) + 32) * 0.1);
        }

        public string LitreToGallonPerHrs(double value, bool isRoundOf)
        {
            if (isRoundOf)
            {
                return Convert.ToString(Math.Round((value * 0.26), 1, MidpointRounding.ToEven));
            }
            else
            {
                return Convert.ToString(value * 0.26);
            }
        }

        public string BarToPressurePerSquareInch(double value, bool isRoundOf)
        {
            if (isRoundOf)
            {
                return Convert.ToString(Math.Round((value * 14.5037738), 1, MidpointRounding.ToEven));
            }
            else
            {
                return Convert.ToString(value * 14.5037738);
            }
        }
    }

}
