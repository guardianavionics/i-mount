﻿using GA.DataTransfer.Classes_for_Web;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace GA.ApplicationLayer
{
    public class QuickBloxHelper
    {
        public readonly string Email;
        public readonly string Password;
        public readonly string ApplicationId;
        public readonly string AuthKey;
        public readonly string SecretKey;

        public QuickBloxHelper(string email)
        {
            Email = email;
            Password = ConfigurationManager.AppSettings["QuickBloxPassword"];
            ApplicationId = ConfigurationManager.AppSettings["QuickBloxApplicationId"];
            AuthKey = ConfigurationManager.AppSettings["QuickBloxAuthKey"];
            SecretKey = ConfigurationManager.AppSettings["QuickBloxSecretKey"];
        }

        public int GetToken()
        {
            try
            {
                //string emailId = QuickBloxDetailModel.Email;
                //string password = QuickBloxDetailModel.Password;
                //string applicationId = QuickBloxDetailModel.ApplicationId;
                //string authKey = QuickBloxDetailModel.AuthKey;
                //string secretKey = QuickBloxDetailModel.SecretKey;

                string emailId = Email;
                string password = Password;
                string applicationId = ApplicationId;
                string authKey = AuthKey;
                string secretKey = SecretKey;

                string timestamp = GetTimeStamp();
                string nonce = GenerateNonce();
                string message = "application_id=" + applicationId + "&auth_key=" + authKey + "&nonce=" + nonce + "&timestamp=" + timestamp + "&user[email]=" + emailId + "&user[password]=" + password;

                string hmac1 = "";
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] keyByte = encoding.GetBytes(secretKey);

                HMACMD5 hmacmd5 = new HMACMD5(keyByte);
                HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);

                byte[] messageBytes = encoding.GetBytes(message);
                byte[] hashmessage = hmacmd5.ComputeHash(messageBytes);
                hmac1 = ByteToString(hashmessage);

                hashmessage = hmacsha1.ComputeHash(messageBytes);
                string signature = ByteToString(hashmessage);

                var client = new RestSharp.RestClient("https://api.quickblox.com/session.json");
                var request = new RestSharp.RestRequest(Method.POST);

                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("quickblox-rest-api-version", "0.1.0");
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", "{\"application_id\": \"" + applicationId + "\", \"auth_key\": \"" + authKey + "\", \"nonce\": \"" + nonce + "\",\"signature\": \"" + signature.ToLower() + "\", \"timestamp\": \"" + timestamp + "\", \"user\": {\"email\": \"" + emailId + "\", \"password\": \"" + password + "\"}}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.StatusCode.ToString() == "Unauthorized")
                {
                    return 0;
                }

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var quickBloxTokenResponse = serializer.Deserialize<QuickBloxTokenResponse>(response.Content);
                var clientTemp = new RestSharp.RestClient("https://api.quickblox.com/chat/Message/unread.json");
                var requestTemp = new RestSharp.RestRequest(Method.GET);
                requestTemp.AddHeader("postman-token", "74aea2fb-e9d4-6bcb-8a5e-66b4c56464ea");
                requestTemp.AddHeader("cache-control", "no-cache");
                requestTemp.AddHeader("qb-token", quickBloxTokenResponse.Session.Token);
                IRestResponse responses = clientTemp.Execute(requestTemp);
                var msgCountJsonString = JObject.Parse(responses.Content);
                string unreadChatMessageCount = msgCountJsonString["total"].ToString();
                return Convert.ToInt32(unreadChatMessageCount);
            }
            catch
            {
                return 0;
            }
        }

        public string GetTimeStamp()
        {
            return ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }

        public static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        public string GenerateNonce()
        {
            Random generator = new Random();
            string nonce = generator.Next(0, 999999).ToString("D10");
            return nonce;
        }

    }
}
