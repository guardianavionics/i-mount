﻿using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;
using ImageResizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Linq;
using System.Net;
//using NLog;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Globalization;
using System.Web.Script.Serialization;
//using Newtonsoft.Json;

namespace GA.ApplicationLayer
{
    /// <summary>
    /// Contains all the functions used for aircraft on website and aircraft service
    /// </summary>
    public class AircraftHelper
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Creates and updates aircrafts , send updated aircrafts in response after the last update date excluding the current updates
        /// </summary>
        /// <param name="profileDetail">List of aircrafts, other parameters</param>
        /// <returns>updated aircrafts after the last update date excluding the current updates</returns>
        public AircraftProfileResponseModel UpdateAircraftProfile(AircraftProfileUpdateModel profileDetail)
        {
            try
            {
                var response = new AircraftProfileResponseModel { AircraftList = new List<AircraftModel>() };
                // validate aircraft profile list
                var genRes = VaildateUpdateAircraftProfile(profileDetail);

                if (genRes.ResponseCode == ((int)Enumerations.AircraftCodes.Exception).ToString())
                    return response.Exception();

                //response.LastUpdateDateTime = Misc.GetStringOfDate(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1)));
                response.LastUpdateDateTime = Misc.GetStringOfDate(DateTime.UtcNow);

                using (var context = new GuardianAvionicsEntities())
                {
                    if (profileDetail.DAircraftList != default(List<int>))
                    {
                        foreach (var aircraft in profileDetail.DAircraftList
                            .Where(a => a != default(int))
                            .Select(a => context.AircraftProfiles.FirstOrDefault(ar => ar.Id == a))
                            .Where(aircraft => aircraft != default(AircraftProfile)))
                        {
                            var objMapping =
                                context.MappingAircraftAndPilots.FirstOrDefault(
                                    a => a.AircraftId == aircraft.Id && a.ProfileId == profileDetail.ProfileId);
                            if (objMapping != default(MappingAircraftAndPilot))
                            {
                                objMapping.Deleted = true;

                            }
                            //aircraft.Deleted = true;
                            aircraft.LastUpdated = DateTime.UtcNow;
                        }
                        context.SaveChanges();
                    }

                    foreach (var aircraft in profileDetail.AircraftList)
                    {
                        var rAircraft = new AircraftModel();
                        var AircraftToUpdate =
                             context.AircraftProfiles.FirstOrDefault(ap => ap.UniqueId == aircraft.UniqueId);
                        #region update

                        // update aircraft

                        if (aircraft.IsUpdateAvailableForGeneral)
                        {
                            if (aircraft.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                            {
                                // save aircraft image
                                if (!string.IsNullOrEmpty(aircraft.ImageName))
                                {
                                    // assuming image is base64 encoded image
                                    try
                                    {
                                        byte[] bytes = Convert.FromBase64String(aircraft.ImageName);
                                        string path = ConfigurationReader.ImagePathKey;
                                        string fileName = GenerateUniqueImageName();
                                        string fullPath = path + "/" + fileName;
                                        using (var imageFile = new FileStream(fullPath, FileMode.Create))
                                        {
                                            imageFile.Write(bytes, 0, bytes.Length);
                                            imageFile.Flush();
                                        }

                                        if (File.Exists(fullPath))
                                        {
                                            new Misc().UploadFile(fullPath, fileName, "Image");
                                            File.Delete(fullPath);
                                        }
                                        AircraftToUpdate.ImageUrl = fileName;
                                    }
                                    catch
                                    {
                                        AircraftToUpdate.ImageUrl = string.Empty;
                                    }
                                }
                                else
                                {
                                    AircraftToUpdate.ImageUrl = string.Empty;
                                }
                            }
                            else if (aircraft.ImageOption == Enumerations.ImageOptions.DeleteImage.GetStringValue())
                            {
                                AircraftToUpdate.ImageUrl = string.Empty;
                            }
                            //AircraftToUpdate.OwnerProfileId = newOwnerProfileId;
                            AircraftToUpdate.Capacity = aircraft.Capacity;
                            AircraftToUpdate.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType 
                            AircraftToUpdate.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType
                            AircraftToUpdate.FuelUnit = aircraft.FuelUnit;
                            if (aircraft.IsUpdateAvailableForGeneral)
                            {
                                AircraftToUpdate.InstalledEngine = aircraft.InstalledEngine;
                            }
                            AircraftToUpdate.WeightUnit = aircraft.WeightUnit;
                            AircraftToUpdate.SpeedUnit = aircraft.SpeedUnit;
                            AircraftToUpdate.VerticalSpeed = aircraft.VerticalSpeed;
                            AircraftToUpdate.IsAmericaAircraft = (aircraft.IsAmericanAircraft != default(bool)) &&
                                                                 aircraft.IsAmericanAircraft;
                            AircraftToUpdate.LastUpdated = DateTime.UtcNow;
                            AircraftToUpdate.Make = aircraft.Make;
                            AircraftToUpdate.Model = aircraft.Model;
                            AircraftToUpdate.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                            AircraftToUpdate.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                            AircraftToUpdate.Color = aircraft.Color;
                            AircraftToUpdate.HomeBase = aircraft.HomeBase;
                            //AircraftToUpdate.MissingFlight = aircraft.MissingFlight;
                            //AircraftToUpdate.AeroUnitNo = aircraft.AeroUnitNo;
                            AircraftToUpdate.EngineType = GetEngineTypeFromCodes((aircraft.EngineType).ToString());
                            AircraftToUpdate.AircraftType = aircraft.AircraftType;
                            AircraftToUpdate.AircraftSerialNo = aircraft.AircraftSerialNumber;
                            AircraftToUpdate.AircraftYear = aircraft.AircraftYear;
                            AircraftToUpdate.HobbsTime = aircraft.CurrentHobbsTime;
                            AircraftToUpdate.HobbsTimeOffset = aircraft.HobbsTimeOffset;
                            AircraftToUpdate.TachTime = aircraft.CurrentTachTime;
                            AircraftToUpdate.TachTimeOffset = aircraft.TachTimeOffset;

                            AircraftToUpdate.EngineMFGType = aircraft.EngineMFGType;
                            AircraftToUpdate.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;
                            AircraftToUpdate.EngineTBO = aircraft.EngineTBO;

                            AircraftToUpdate.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
                            AircraftToUpdate.CurrentEngineTime = aircraft.CurrentEngineTime1;

                            AircraftToUpdate.PropMFG = aircraft.PropMFG;
                            AircraftToUpdate.OtherPropManufacturer = aircraft.OtherPropManufactureType;
                            AircraftToUpdate.Prop1TBO = aircraft.Prop1TBO;
                            AircraftToUpdate.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);

                            AircraftToUpdate.Prop1Time = aircraft.CurrentPropTime1;
                            AircraftToUpdate.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
                            AircraftToUpdate.Engine2Time = aircraft.CurrentEngineTime2;

                            AircraftToUpdate.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
                            AircraftToUpdate.Prop2Time = aircraft.CurrentPropTime2;
                            AircraftToUpdate.Comm = aircraft.Comm;
                            AircraftToUpdate.EngineMonitor = aircraft.EngineMonitor;
                            AircraftToUpdate.Transponder = aircraft.Transponder;
                            //AircraftToUpdate.ChargeBy = aircraft.ChargeBy;

                            var objMappingAircraftAndPilots = context.MappingAircraftAndPilots.FirstOrDefault(a => a.AircraftId == aircraft.AircraftId && a.ProfileId == aircraft.ProfileId);
                            if (objMappingAircraftAndPilots != null)
                            {
                                if (objMappingAircraftAndPilots.Deleted == true)
                                {
                                    objMappingAircraftAndPilots.Deleted = false;
                                }
                            }


                            var temp = aircraft.AircraftComponentMakeAndModels.Select(s => s.aircraftComponentId).ToList();
                            var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(cm => cm.AircraftId == aircraft.AircraftId && temp.Contains(cm.AircraftComponentId)).ToList();
                            if (aircraftComponentMakeAndModels.Count > 0)
                            {
                                aircraftComponentMakeAndModels.ForEach(delegate (AircraftComponentMakeAndModel item)
                                {

                                    var componentModel =
                                        aircraft.AircraftComponentMakeAndModels.FirstOrDefault(
                                            cm => cm.aircraftComponentId == item.AircraftComponentId);
                                    Document oldDocument = null;
                                    if (item.AircraftComponentModel != null)
                                    {
                                        oldDocument =
                                            context.Documents.FirstOrDefault(
                                                d => d.Url == item.AircraftComponentModel.UserMannual);
                                    }
                                    //var oldDocument = context.Documents.FirstOrDefault(d => d.Url == item.AircraftComponentModel.UserMannual);
                                    if (componentModel != null && componentModel.isShowInFlightBag &&
                                    (componentModel.componentModelId != item.ComponentModelId || oldDocument == null))
                                    {
                                        if (oldDocument != null)
                                        {
                                            context.Documents.Remove(oldDocument);
                                        }
                                        var usermannual =
                                            context.AircraftComponentModels.FirstOrDefault(
                                                f => f.Id == componentModel.componentModelId);
                                        var document = context.Documents.Create();
                                        document.Url = usermannual == null ? "" : usermannual.UserMannual;
                                        document.ProfileId = Convert.ToInt32(AircraftToUpdate.OwnerProfileId);
                                        document.IsForAll = false;
                                        document.Deleted = false;
                                        document.LastUpdated = DateTime.UtcNow;
                                        document.AircraftId = aircraft.AircraftId;

                                        context.Documents.Add(document);
                                    }
                                    else
                                    {
                                        if (!componentModel.isShowInFlightBag && oldDocument != null)
                                        {
                                            context.Documents.Remove(oldDocument);
                                        }
                                    }
                                    item.ComponentManufacturerId = componentModel.componentManufacturerId;
                                    item.ComponentModelId = componentModel.componentModelId;
                                    item.ComponentModelFreetext = componentModel.modelSerialNo;
                                    item.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                });
                            }

                            foreach (var componentModel in aircraft.AircraftComponentMakeAndModels.Where(a => !aircraftComponentMakeAndModels.Select(s => s.AircraftComponentId).ToList().Contains(a.aircraftComponentId)).ToList())
                            {
                                var cm = context.AircraftComponentMakeAndModels.Create();
                                cm.AircraftComponentId = componentModel.aircraftComponentId;
                                cm.ComponentManufacturerId = componentModel.componentManufacturerId;
                                cm.ComponentModelId = componentModel.componentModelId;
                                cm.AircraftId = aircraft.AircraftId;
                                cm.ComponentModelFreetext = componentModel.modelSerialNo;
                                cm.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                AircraftToUpdate.AircraftComponentMakeAndModels.Add(cm);

                                if (componentModel.isShowInFlightBag)
                                {
                                    var document = context.Documents.Create();
                                    document.Url =
                                        context.AircraftComponentModels.FirstOrDefault(
                                            f => f.Id == componentModel.componentModelId).UserMannual;
                                    document.LastUpdated = DateTime.UtcNow;
                                    document.Deleted = false;
                                    document.ProfileId = AircraftToUpdate.OwnerProfileId ?? 0;
                                    document.IsForAll = false;
                                    document.AircraftId = aircraft.AircraftId;
                                    AircraftToUpdate.Documents.Add(document);

                                }
                            }

                            var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.AircraftId);
                            if (mappingComponentManufacturerAndAircrafts != null)
                            {
                                mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft ?? "";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
                                {
                                    var obj = context.MappingComponentManufacturerAndAircrafts.Create();
                                    obj.AircraftId = aircraft.AircraftId;
                                    obj.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
                                    context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
                                }
                            }

                            AircraftToUpdate.LastUpdated = DateTime.UtcNow;

                        }

                        if (aircraft.IsUpdateAvailableForPerformance)
                        {
                            AircraftToUpdate.Power = (aircraft.Power == default(float)) ? 0 : aircraft.Power;
                            AircraftToUpdate.RPM = (aircraft.RPM == default(double)) ? 0 : aircraft.RPM;
                            AircraftToUpdate.MP = (aircraft.MP == default(double)) ? 0 : aircraft.MP;
                            AircraftToUpdate.Altitude = (aircraft.Altitude == default(double))
                                ? 0
                                : aircraft.Altitude;
                            AircraftToUpdate.FuelBurnCruise = (aircraft.FuelBurnCruise == default(double))
                                ? 0
                                : aircraft.FuelBurnCruise;
                            AircraftToUpdate.TASCruise = (aircraft.TASCruise == default(double))
                                ? 0
                                : aircraft.TASCruise;
                            AircraftToUpdate.RateOfClimb = (aircraft.RateOfClimb == default(double))
                                ? 0
                                : aircraft.RateOfClimb;
                            AircraftToUpdate.TASClimb = (aircraft.TASClimb == default(double))
                                ? 0
                                : aircraft.TASClimb;
                            AircraftToUpdate.RateOfDescent = (aircraft.RateOfDescent == default(double))
                                ? 0
                                : aircraft.RateOfDescent;
                            AircraftToUpdate.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == default(double))
                                ? 0
                                : aircraft.FuelBurnRateOfDescent;
                            AircraftToUpdate.TASDescent = (aircraft.TASDescent == default(double))
                                ? 0
                                : aircraft.TASDescent;
                            AircraftToUpdate.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
                            AircraftToUpdate.LastUpdateDatePerformance = DateTime.UtcNow;

                        }

                        if (aircraft.IsUpdateAvailableForEngine)
                        {
                            if (!string.IsNullOrEmpty(aircraft.EngineSettings))
                            {
                                AircraftToUpdate.LastUpdateDateEngineSetting = DateTime.UtcNow;
                                if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml"))
                                {
                                    File.Delete(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml");
                                }
                                XmlDocument doc = new XmlDocument();
                                try
                                {
                                    doc.LoadXml(aircraft.EngineSettings);
                                }
                                catch (Exception ex)
                                {
                                    //logger.Fatal("Error while saving the XML file (Update Aircraft) " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                                }
                                doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId.ToString() + ".xml");
                            }
                        }
                        updateCategoryItem(aircraft.CategoryList);
                        context.SaveChanges();


                        #endregion update
                        // update aircraft completed

                    } // for each completed


                    // send updated aircraft in response

                    #region resposne

                    var udate = Misc.GetDate(profileDetail.LastUpdateDateTime) ?? DateTime.MinValue;

                    var keyValueTable = context.KeyValueTables.FirstOrDefault();

                    var aircraftIdMapWithPilot = context.MappingAircraftAndPilots.Where(map => map.ProfileId == profileDetail.ProfileId && map.Deleted == false)
                                                                          .Select(s => s.AircraftId)
                                                                          .ToList();
                    var deletedAircraftList = context.MappingAircraftAndPilots.Where(w => w.ProfileId == profileDetail.ProfileId && w.Deleted).Select(s => s.AircraftId).ToList();
                    var aircraftIdLogs = context.PilotLogs.Where(w => w.PilotId == profileDetail.ProfileId || w.CoPilotId == profileDetail.ProfileId).Select(s => s.AircraftId).Distinct().ToList();

                    var aList =
                        context.AircraftProfiles.Where(a => (a.OwnerProfileId == profileDetail.ProfileId || aircraftIdMapWithPilot.Contains(a.Id) || aircraftIdLogs.Contains(a.Id)) && !deletedAircraftList.Contains(a.Id) && !a.Deleted && (a.LastUpdated >= udate || a.LastUpdateDatePerformance >= udate || a.LastUpdateDateEngineSetting >= udate || a.LastUpdateDateMaintenanceUser >= udate || a.LastUpdateDateIssue >= udate || keyValueTable.IpassengerAdminLastUpdateDate >= udate)).ToList();

                    if (aList.Count > 0)
                    {
                        if (profileDetail.DAircraftList != default(List<int>))
                            foreach (var a in profileDetail.DAircraftList)
                                aList.RemoveAll(al => al.Id == a);

                        // Remove ProfileId From Table
                        var deletedAircraft =
                            context.AircraftProfiles.Where(
                                al => al.Deleted && context.MappingAircraftAndPilots
                                    .Where(map => map.ProfileId == profileDetail.ProfileId && map.Deleted == true)
                                    .Select(s => s.AircraftId)
                                    .ToList()
                                    .Contains(al.Id) && (al.LastUpdated >= udate || al.LastUpdateDatePerformance >= udate || al.LastUpdateDateEngineSetting >= udate || al.LastUpdateDateMaintenanceUser >= udate || al.LastUpdateDateIssue >= udate))
                                .Select(aId => aId.Id)
                                .ToList();

                        if (profileDetail.DAircraftList != default(List<int>))
                            foreach (var a in profileDetail.DAircraftList)
                                deletedAircraft.Remove(a);

                        response.DAircraftList = new List<int>();
                        if (deletedAircraft.Count > 0)
                            foreach (var a in deletedAircraft)
                                response.DAircraftList.Add(a);
                        foreach (var a in aList)
                        {
                            var objAircraft = GetAircraftModelyAircraftProfile(a, udate, profileDetail.ProfileId);
                            //var objAircraft =  AircraftProfileResponseModel.Create(a, (Misc.GetDate(profileDetail.LastUpdateDateTime) ?? DateTime.MinValue));
                            objAircraft.ManufacturerName = selectMakeNameById(objAircraft.Make);
                            var objFuelQuantity = getfueldetail(a.Id);
                            objAircraft.FuelQuantityModels = objFuelQuantity;
                            //objAircraft.FuelQuantityModels.Add(objFuelQuantity);
                            //not required to send ipassenger details as the API already exist for ipassenger and device calls API everytime when it needs to access ipassenger details
                            //objAircraft.IPassengerList = new IPassengerHelper().GetIPassengerMediaDetailsAPI(objAircraft.AircraftId);
                            response.AircraftList.Add(objAircraft);
                        }
                    }
                    //Add categories with item


                    if (response.AircraftList != null)
                    {
                        if (response.AircraftList.Count > 0)
                        {
                            response.AircraftList.ForEach(f => f.IsDefaultAircraft = false);
                            int aircraftId = GetFirstAircraftRegisteredByProfileId(profileDetail.ProfileId);
                            if (response.AircraftList.FirstOrDefault(f => f.AircraftId == aircraftId) != null)
                            {
                                response.AircraftList.FirstOrDefault(f => f.AircraftId == aircraftId).IsDefaultAircraft = true;
                            }
                        }
                    }
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                    return response;

                    #endregion resposne

                } // using

            }
            catch (Exception e)
            {
                string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(profileDetail.AircraftList.FirstOrDefault().AircraftId);
                string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                if (File.Exists(aircraftLogPath))
                {
                    using (StreamWriter sw = File.AppendText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - UpdateAircraftProfile)");
                        sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                else
                {
                    //using (StreamWriter sw = File.CreateText(aircraftLogPath))
                    //{
                    //    sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                    //    sw.WriteLine("Exception while Update aircraft (API - UpdateAircraftProfile)");
                    //    sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
                    //    sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                    //    sw.WriteLine("=====================================================================================");
                    //    sw.WriteLine("");
                    //}
                }
                return new AircraftProfileResponseModel().Exception();
            }

        }
        public void updateCategoryItem(List<CategoryModel> catgoryitem)
        {
            List<CategoryModel> modelList = new List<CategoryModel>();
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    int aircraftid = Convert.ToInt32(catgoryitem[0].AircraftId);
                    var aircraft = context.AircraftProfiles.Where(x => x.Id == aircraftid).FirstOrDefault(); //Where(x => x.IsActive == true).
                    foreach (var cat in catgoryitem)
                    {
                        var categry = context.Category.Where(x => x.IOSCategoryId == cat.Id).FirstOrDefault();
                        if (categry != null)
                        {
                            //categry.Id = cat.Id;
                            categry.AircraftId = cat.AircraftId;
                            categry.CategoryName = cat.CategoryName;
                            categry.CreatedBy = aircraft.OwnerProfileId;
                            categry.CreatedDate = DateTime.Now;
                            categry.UpdatedDate = DateTime.Now;
                            categry.UpdatedBy = aircraft.Id;
                            categry.IsActive = cat.IsActive;
                            categry.TabColor = cat.TabColor;
                            categry.CategoryType = cat.CategoryType;
                            categry.Priority = cat.Priority;
                            foreach (var item in cat.ItemList)
                            {
                                var itemset = context.CategoryItems.Where(x => x.IOSCategoryItemId == item.Id).FirstOrDefault();
                                //itemmodel.Id = item.Id;
                                if (itemset != null)
                                {
                                    itemset.CategoryItemName = item.CategoryItemName;
                                    itemset.CreatedBy = aircraft.OwnerProfileId;
                                    itemset.UpdatedBy = aircraft.OwnerProfileId;
                                    itemset.CreatedDate = DateTime.Now;
                                    itemset.UpdatedDate = DateTime.Now;
                                    itemset.IsActive = item.IsActive;
                                    itemset.IOSCategoryItemId = item.Id;
                                    itemset.Condition = item.Condition;
                                    itemset.RowIndex = item.RowIndex;
                                }
                                else
                                {
                                    CategoryItems itemmodel = new CategoryItems();
                                    itemmodel.CategoryId = categry.Id;
                                    itemmodel.CategoryItemName = item.CategoryItemName;
                                    itemmodel.CreatedBy = aircraft.OwnerProfileId;
                                    itemmodel.UpdatedBy = aircraft.OwnerProfileId;
                                    itemmodel.CreatedDate = DateTime.Now;
                                    itemmodel.UpdatedDate = DateTime.Now;
                                    itemmodel.IsActive = item.IsActive;
                                    itemmodel.IOSCategoryItemId = item.Id;
                                    itemmodel.Condition = item.Condition;
                                    itemmodel.RowIndex = item.RowIndex;
                                    context.CategoryItems.Add(itemmodel);
                                }
                            }
                            context.SaveChanges();

                        }
                        else
                        {
                            Category model = new Category();
                            //model.Id = cat.Id;
                            model.AircraftId = cat.AircraftId;
                            model.CategoryName = cat.CategoryName;
                            model.CreatedBy = aircraft.OwnerProfileId;
                            model.UpdatedBy = aircraft.OwnerProfileId;
                            model.CreatedDate = DateTime.Now;
                            model.UpdatedDate = DateTime.Now;
                            model.IsActive = cat.IsActive;
                            model.TabColor = cat.TabColor;
                            model.Priority = cat.Priority;
                            model.CategoryType = cat.CategoryType;
                            model.IOSCategoryId = cat.Id;//cat.IOSCategoryId;
                            context.Category.Add(model);
                            context.SaveChanges();
                            //var Items = context.CategoryItems.Where(x => x.CategoryId == cat.Id).ToList(); // && x.IsActive == true
                            foreach (var item in cat.ItemList)
                            {
                                CategoryItems itemmodel = new CategoryItems();
                                itemmodel.CategoryId = item.Id;
                                itemmodel.CategoryItemName = item.CategoryItemName;
                                itemmodel.CreatedBy = aircraft.OwnerProfileId;
                                itemmodel.UpdatedBy = aircraft.OwnerProfileId;
                                itemmodel.CreatedDate = DateTime.Now;
                                itemmodel.UpdatedDate = DateTime.Now;
                                itemmodel.IsActive = item.IsActive;
                                itemmodel.IOSCategoryItemId = item.Id;
                                itemmodel.Condition = item.Condition;
                                itemmodel.RowIndex = item.RowIndex;
                                context.CategoryItems.Add(itemmodel);
                            }
                            context.SaveChanges();
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        public List<CategoryModel> setCategoryItem(int aircraftId)
        {
            List<CategoryModel> modelList = new List<CategoryModel>();
            using (var context = new GuardianAvionicsEntities())
            {
                var categry = context.Category.Where(x => x.AircraftId == aircraftId).ToList(); //Where(x => x.IsActive == true).
                foreach (var cat in categry)
                {
                    CategoryModel model = new CategoryModel();
                    //model.Id = cat.Id;
                    model.AircraftId = cat.AircraftId;
                    model.CategoryName = cat.CategoryName;
                    model.CreatedDate = cat.CreatedDate;
                    model.UpdatedDate = cat.UpdatedDate;
                    model.IsActive = cat.IsActive;
                    model.TabColor = cat.TabColor;
                    model.Priority = cat.Priority;
                    model.CategoryType = cat.CategoryType;
                    model.Id= Convert.ToInt32(cat.IOSCategoryId);
                    //model.IOSCategoryId = cat.IOSCategoryId;
                    var Items = context.CategoryItems.Where(x => x.CategoryId == cat.Id).ToList(); // && x.IsActive == true
                    List<CategoryItemModel> itemList = new List<CategoryItemModel>();
                    foreach (var item in Items)
                    {
                        CategoryItemModel itemmodel = new CategoryItemModel();
                        //itemmodel.Id = item.Id;
                        itemmodel.CategoryItemName = item.CategoryItemName;
                        itemmodel.CreatedBy = item.CreatedBy;
                        itemmodel.CreatedDate = item.CreatedDate;
                        itemmodel.UpdatedDate = item.UpdatedDate;
                        itemmodel.IsActive = item.IsActive;
                        itemmodel.Id = Convert.ToInt32(item.IOSCategoryItemId);
                        itemmodel.Condition = item.Condition;
                        itemmodel.RowIndex = item.RowIndex;
                        //itemmodel.IOSCategoryItemId = item.IOSCategoryItemId;
                        itemList.Add(itemmodel);
                    }
                    model.ItemList = itemList;
                    modelList.Add(model);
                }

            }
            return modelList;
        }


        /// <summary>
        /// This method checks the mapping for aircraft and pilot. If exist then update IsActive to true else create the new mapping.
        /// </summary>
        /// <param name="aircraftId"></param>
        /// <param name="profileId"></param>
        public void MappingAircraftAndPilot(int aircraftId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var objMap = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == aircraftId && f.ProfileId == profileId);
            if (objMap == null)
            {
                var mappingAircraftAndPilots = new MappingAircraftAndPilot()
                {
                    AircraftId = aircraftId,
                    ProfileId = profileId,
                    Deleted = false,
                    CreateDate = DateTime.UtcNow,
                    IsActive = true
                };
                context.MappingAircraftAndPilots.Add(mappingAircraftAndPilots);
            }
            else
            {
                objMap.IsActive = true;
                objMap.Deleted = false;
            }
            context.SaveChanges();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="aircraft"></param>
        /// <param name="lastUpdateDate"></param>
        /// <returns></returns>
        public AircraftModel GetAircraftModelyAircraftProfile(AircraftProfile aircraft, DateTime? lastUpdateDate, int profileId)
        {

            var context = new GuardianAvionicsEntities();
            var objAircraftProfileUpdateModel = new AircraftModel();

            objAircraftProfileUpdateModel.LastTransactionId = aircraft.LastTransactionId;
            objAircraftProfileUpdateModel.AircraftId = aircraft.Id;
            objAircraftProfileUpdateModel.UniqueId = aircraft.UniqueId ?? default(long);

            objAircraftProfileUpdateModel.IsUpdateAvailableForEngine = false;
            objAircraftProfileUpdateModel.IsUpdateAvailableForGeneral = false;
            objAircraftProfileUpdateModel.IsUpdateAvailableForPerformance = false;

            if (aircraft.LastUpdateDatePerformance > lastUpdateDate)
            {
                objAircraftProfileUpdateModel.IsUpdateAvailableForPerformance = true;
                objAircraftProfileUpdateModel.Power = (aircraft.Power == null) ? 0 : (double)aircraft.Power;
                objAircraftProfileUpdateModel.RPM = (aircraft.RPM == null) ? 0 : (double)aircraft.RPM;
                objAircraftProfileUpdateModel.MP = (aircraft.MP == null) ? 0 : (double)aircraft.MP;
                objAircraftProfileUpdateModel.Altitude = (aircraft.Altitude == null) ? 0 : (double)aircraft.Altitude;
                objAircraftProfileUpdateModel.FuelBurnCruise = (aircraft.FuelBurnCruise == null) ? 0 : (double)aircraft.FuelBurnCruise;
                objAircraftProfileUpdateModel.TASCruise = (aircraft.TASCruise == null) ? 0 : (double)aircraft.TASCruise;
                objAircraftProfileUpdateModel.RateOfClimb = (aircraft.RateOfClimb == null) ? 0 : (double)aircraft.RateOfClimb;
                objAircraftProfileUpdateModel.TASClimb = (aircraft.TASClimb == null) ? 0 : (double)aircraft.TASClimb;
                objAircraftProfileUpdateModel.RateOfDescent = (aircraft.RateOfDescent == null) ? 0 : (double)aircraft.RateOfDescent;
                objAircraftProfileUpdateModel.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == null) ? 0 : (double)aircraft.FuelBurnRateOfDescent;
                objAircraftProfileUpdateModel.TASDescent = (aircraft.TASDescent == null) ? 0 : (double)aircraft.TASDescent;
            }

            if (aircraft.LastUpdated > lastUpdateDate)
            {
                objAircraftProfileUpdateModel.IsUpdateAvailableForGeneral = true;
                objAircraftProfileUpdateModel.Registration = aircraft.Registration ?? string.Empty;
                objAircraftProfileUpdateModel.Capacity = aircraft.Capacity ?? string.Empty;
                objAircraftProfileUpdateModel.TaxiFuel = aircraft.TaxiFuel == null ? string.Empty : aircraft.TaxiFuel.ToString(); //ChangeDataType
                objAircraftProfileUpdateModel.AdditionalFuel = aircraft.AdditionalFuel == null ? string.Empty : aircraft.AdditionalFuel.ToString();//ChangeDataType
                objAircraftProfileUpdateModel.FuelUnit = aircraft.FuelUnit ?? string.Empty;
                objAircraftProfileUpdateModel.WeightUnit = aircraft.WeightUnit ?? string.Empty;
                objAircraftProfileUpdateModel.SpeedUnit = aircraft.SpeedUnit ?? string.Empty;
                objAircraftProfileUpdateModel.VerticalSpeed = aircraft.VerticalSpeed ?? string.Empty;

                objAircraftProfileUpdateModel.IsAmericanAircraft = (aircraft.IsAmericaAircraft != null) && (bool)aircraft.IsAmericaAircraft;
                objAircraftProfileUpdateModel.Color = aircraft.Color ?? string.Empty;
                objAircraftProfileUpdateModel.FuelBurnRateClimb = (aircraft.FuelBurnRateClimb == null) ? 0 : (double)aircraft.FuelBurnRateClimb;
                objAircraftProfileUpdateModel.HomeBase = aircraft.HomeBase ?? string.Empty;
                objAircraftProfileUpdateModel.Make = aircraft.Make;
                objAircraftProfileUpdateModel.Model = aircraft.Model;
                objAircraftProfileUpdateModel.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                objAircraftProfileUpdateModel.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                objAircraftProfileUpdateModel.ManufacturerName = (aircraft.Make != null) ? aircraft.AircraftManufacturer.Name : string.Empty;
                objAircraftProfileUpdateModel.ModelName = (aircraft.Model != null) ? aircraft.AircraftModelList.ModelName : string.Empty;
                objAircraftProfileUpdateModel.AircraftType = aircraft.AircraftType ?? string.Empty;
                objAircraftProfileUpdateModel.AircraftSerialNumber = aircraft.AircraftSerialNo ?? string.Empty;
                objAircraftProfileUpdateModel.AircraftYear = aircraft.AircraftYear ?? default(int);
                objAircraftProfileUpdateModel.CurrentHobbsTime = aircraft.HobbsTime ?? default(double);
                objAircraftProfileUpdateModel.HobbsTimeOffset = aircraft.HobbsTimeOffset ?? default(double);
                objAircraftProfileUpdateModel.CurrentTachTime = aircraft.TachTime ?? default(double);
                objAircraftProfileUpdateModel.TachTimeOffset = aircraft.TachTimeOffset ?? default(double);
                objAircraftProfileUpdateModel.EngineMFGType = aircraft.EngineMFGType;
                objAircraftProfileUpdateModel.OtherEngineManufactureType = aircraft.OtherEngineManufacturerType;
                objAircraftProfileUpdateModel.EngineTBO = aircraft.EngineTBO ?? default(double);
                objAircraftProfileUpdateModel.Engine1LastMOHDate = (aircraft.Engine1LastMOHDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Engine1LastMOHDate);

                objAircraftProfileUpdateModel.CurrentEngineTime1 = aircraft.CurrentEngineTime ?? 0;
                objAircraftProfileUpdateModel.EngineTimeOffset1 = aircraft.EngineTimeOffset ?? 0;
                objAircraftProfileUpdateModel.PropMFG = aircraft.PropMFG;
                objAircraftProfileUpdateModel.OtherPropManufactureType = aircraft.OtherPropManufacturer;
                objAircraftProfileUpdateModel.Prop1TBO = (aircraft.Prop1TBO == null) ? default(int) : (int)aircraft.Prop1TBO;
                objAircraftProfileUpdateModel.Prop1OHDueDate = (aircraft.Prop1OHDueDate == null) ? string.Empty : Misc.GetStringOnlyDate(aircraft.Prop1OHDueDate);

                objAircraftProfileUpdateModel.CurrentPropTime1 = aircraft.Prop1Time ?? 0;
                objAircraftProfileUpdateModel.PropTimeOffset1 = aircraft.Prop1TimeOffset ?? 0;
                objAircraftProfileUpdateModel.Engine2LastMOHDate = (aircraft.Engine2LastMOHDate == null)
                    ? string.Empty
                    : Misc.GetStringOnlyDate(aircraft.Engine2LastMOHDate);

                objAircraftProfileUpdateModel.CurrentEngineTime2 = aircraft.Engine2Time ?? 0;
                objAircraftProfileUpdateModel.EngineTimeOffset2 = (aircraft.Engine2TimeOffset ?? 0);


                objAircraftProfileUpdateModel.Prop2OHDueDate = (aircraft.Prop2OHDueDate == null)
                    ? string.Empty
                    : Misc.GetStringOnlyDate(aircraft.Prop2OHDueDate);

                objAircraftProfileUpdateModel.CurrentPropTime2 = (aircraft.Prop2Time ?? 0);
                objAircraftProfileUpdateModel.PropTimeOffset2 = (aircraft.Prop2Timeoffset ?? 0);


                var aeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Deleted);
                objAircraftProfileUpdateModel.IsAeroUnitBlocked = (aeroUnit == null) ? true : aeroUnit.Blocked;

                objAircraftProfileUpdateModel.AeroUnitNo = aircraft.AeroUnitNo ?? string.Empty;
                objAircraftProfileUpdateModel.IsRegisterWithoutUnit = string.IsNullOrEmpty(aircraft.AeroUnitNo) ? true : false;
                objAircraftProfileUpdateModel.ChargeBy = aircraft.ChargeBy;


                // converting Engine Type to codes.
                if (aircraft.EngineType == Enumerations.EngineType.MultipleEngine.ToString())
                    objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.MultipleEngine.GetStringValue());
                else if (aircraft.EngineType == Enumerations.EngineType.SingleEngine.ToString())
                    objAircraftProfileUpdateModel.EngineType = Convert.ToInt32(Enumerations.EngineType.SingleEngine.GetStringValue());
                else
                    objAircraftProfileUpdateModel.EngineType = 0;

                // send aircraft profile image
                if (!String.IsNullOrEmpty(aircraft.ImageUrl))
                {
                    objAircraftProfileUpdateModel.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + aircraft.ImageUrl;
                }
                else
                {
                    objAircraftProfileUpdateModel.ImageName = aircraft.ImageUrl ?? string.Empty;
                }

                objAircraftProfileUpdateModel.Comm = aircraft.Comm;
                objAircraftProfileUpdateModel.Transponder = aircraft.Transponder;
                objAircraftProfileUpdateModel.EngineMonitor = aircraft.EngineMonitor;

                var documentList = context.Documents.Where(d => !d.Deleted && d.AircraftId == aircraft.Id).Select(d => d.Url).ToList();
                objAircraftProfileUpdateModel.AircraftComponentMakeAndModels =
                context.AircraftComponentMakeAndModels.Where(a => a.AircraftId == aircraft.Id).Select(s => new AircraftComponentMakeAndModelApp()
                {
                    aircraftComponentId = s.AircraftComponentId,
                    componentManufacturerId = s.ComponentManufacturerId,
                    componentModelId = s.ComponentModelId,
                    isShowInFlightBag = documentList.Contains(s.AircraftComponentModel.UserMannual),
                    modelSerialNo = s.ComponentModelFreetext,
                    isVisibleOnMFDScreen = s.IsVisibleOnMFDScreen ?? false
                }).ToList();


                //var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.Id);

                //var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraft.Id && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
                //objAircraftProfileUpdateModel.ShareDataWithManufacturer = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
                //if (mappingAircraftManufacturerAndUsers != null)
                //{
                //    List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                //    objAircraftProfileUpdateModel.ShareDataWithManufacturer.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
                //}
                //objAircraftProfileUpdateModel.ShareDataWithManufacturer = objAircraftProfileUpdateModel.ShareDataWithManufacturer.OrderBy(o => o.ManufacturerId).ToList();


                var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.Id);

                var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraft.Id && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
                List<ManufacturerAuthForDataLog> manufacturerAuthForDataLog = new List<ManufacturerAuthForDataLog>();
                manufacturerAuthForDataLog = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
                if (mappingAircraftManufacturerAndUsers != null)
                {
                    List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                    manufacturerAuthForDataLog.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
                }
                var manuIds = manufacturerAuthForDataLog.Where(w => w.ISAuthenticateForDatalog).OrderBy(o => o.ManufacturerId).Select(s => s.ManufacturerId).ToList();
                objAircraftProfileUpdateModel.ShareDataWithManufacturer = string.Join(",", manuIds);


                objAircraftProfileUpdateModel.OwnerEmailId =
                    context.Profiles.FirstOrDefault(p => p.Id == aircraft.OwnerProfileId).EmailId;

                var mappingAircraftAndPilot = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == aircraft.Id && f.ProfileId == profileId);
                if (mappingAircraftAndPilot != null)
                {
                    objAircraftProfileUpdateModel.IsActiveForUser = mappingAircraftAndPilot.IsActive ?? true;
                }
                else
                {
                    if (string.IsNullOrEmpty(aircraft.AeroUnitNo))
                    {
                        objAircraftProfileUpdateModel.IsActiveForUser = false;
                    }
                    else
                    {
                        objAircraftProfileUpdateModel.IsActiveForUser = true;
                    }
                }
            }
            objAircraftProfileUpdateModel.IPassengerList = new IPassengerHelper().GetIPassengerProMediaDetailsAPI(aircraft.Id);
            objAircraftProfileUpdateModel.InstalledEngine = aircraft.InstalledEngine;

            if (aircraft.LastUpdateDateEngineSetting > lastUpdateDate)
            {
                objAircraftProfileUpdateModel.IsUpdateAvailableForEngine = true;
                if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml"))
                {

                    XDocument loaded = XDocument.Load(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.Id.ToString() + ".xml");
                    objAircraftProfileUpdateModel.EngineSettings = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + loaded.ToString().Replace("[]", "");
                }
                else
                {
                    objAircraftProfileUpdateModel.EngineSettings = "";
                }
            }
            var MissingflightList = (from pilotlog in context.PilotLogs
                                     where pilotlog.AircraftId == aircraft.Id && pilotlog.IsMissingData == true
                                     select new { pilotlog.FlightId, pilotlog.IsMissingData }).ToList();

            List<missingFightData> MissingFlightDataList = new List<missingFightData>();
            foreach (var item in MissingflightList)
            {
                missingFightData Missingflight = new missingFightData();
                Missingflight.isMissingData = item.IsMissingData;
                Missingflight.flightId = item.FlightId;
                MissingFlightDataList.Add(Missingflight);
            }
            objAircraftProfileUpdateModel.CategoryList = setCategoryItem(aircraft.Id);
            objAircraftProfileUpdateModel.MissingFlight = aircraft.MissingFlight;
            objAircraftProfileUpdateModel.missingFightData = MissingFlightDataList;
            return objAircraftProfileUpdateModel;
        }


        public string selectMakeNameById(int? makeId)
        {
            var context = new GuardianAvionicsEntities();
            var makeName = context.AircraftManufacturers.FirstOrDefault(n => n.Id == makeId);

            return makeName == null ? string.Empty : makeName.Name;
        }

        public string selectModelNameByModelId(int? modelId)
        {
            var context = new GuardianAvionicsEntities();
            var obj = context.AircraftModelLists.FirstOrDefault(n => n.Id == modelId);

            return obj == null ? string.Empty : obj.ModelName;
        }

        /// <summary>
        /// Set DefaultAircraft field for all the aircraft as false except current aircraft.  
        /// </summary>
        /// <param name="profileId">Profile Id</param>
        /// <param name="aircraftId">Aircraft Id</param>
        /// <param name="context">Context of GuardianAvionicsEntities</param>
        //public void SetForAllAircraftDefaultAircraft(int profileId, int aircraftId, GuardianAvionicsEntities context)
        //{
        //    if (context != null && aircraftId != default(int) && profileId != default(int))
        //    {
        //        var aList =
        //            context.AircraftProfiles.Where(a => a.ProfileId == profileId && a.Id != aircraftId && !a.Deleted)
        //                .ToList();
        //        //if (aList.Count() > 0)
        //        //{
        //        foreach (var a in aList.Where(a => a.DefaultAircraft == true))
        //        {
        //            a.DefaultAircraft = false;
        //            a.LastUpdated = DateTime.UtcNow;
        //            context.SaveChanges();
        //        }
        //        //}
        //    }
        //}

        /// <summary>
        /// Decodes engine type code into actual values.
        /// </summary>
        /// <param name="engineType">Code for type of engine</param>
        /// <returns>Type of Engine</returns>
        private string GetEngineTypeFromCodes(string engineType)
        {

            if (engineType == Enumerations.EngineType.SingleEngine.GetStringValue())
                return Enumerations.EngineType.SingleEngine.ToString();
            else
                return Enumerations.EngineType.MultipleEngine.ToString();
        }

        /// <summary>
        /// Validates profile and aircraft details
        /// </summary>
        /// <param name="profileDetail">aircraft list , profile Id</param>
        /// <returns>object of GeneralResponse class</returns>
        private GeneralResponse VaildateUpdateAircraftProfile(AircraftProfileUpdateModel profileDetail)
        {
            if (profileDetail.ProfileId < 1)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.InvalidProfileId).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.InvalidProfileId.GetStringValue()
                };
            }

            using (var context = new GuardianAvionicsEntities())
            {
                var userProfile = context.Profiles.FirstOrDefault(p => p.Id == profileDetail.ProfileId);

                if (userProfile == default(Profile))
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.ProfileIdNotFound).ToString(),
                        ResponseMessage = Enumerations.AircraftCodes.ProfileIdNotFound.GetStringValue()
                    };
                }


            }

            foreach (var a in profileDetail.AircraftList)
            {
                if (a.ProfileId < 1)
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                        ResponseMessage = "Invalid profile id for aircraft id = " + a.AircraftId.ToString()
                    };

                if (a.UniqueId == default(long))
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                        ResponseMessage = "Invalid Unique id from aircraft id = " + a.AircraftId.ToString()
                    };


            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue()
            };
        }

        /// <summary>
        /// Generates Unique name for Image , starting with gaImage_ , ending with .png.
        /// </summary>
        /// <returns>Unique name</returns>
        public string GenerateUniqueImageName()
        {
            var fileName = "gaImage_" + DateTime.UtcNow.Ticks + ".png";
            return fileName;
        }

        /// <summary>
        /// Fetch aircraft from database and converts it into AircraftProfileModelWeb.
        /// </summary>
        /// <param name="aircraftId">Id of Aircraft</param>
        /// <returns>AircraftProfileModelWeb Object</returns>
        public AircraftProfileModelWeb GetAircraftDetails(int aircraftId, int profileId = 0)
        {
            var aircraftModel = new AircraftProfileModelWeb();
            // make the list for all the unit's
            aircraftModel = SetSelectListForAllUnits(aircraftModel);

            if (aircraftId == default(int))
                return aircraftModel;

            using (var context = new GuardianAvionicsEntities())
            {
                var aircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == aircraftId);

                if (aircraft == default(AircraftProfile)) return aircraftModel;
                var objProfile = context.Profiles.FirstOrDefault(f => f.Id == profileId);

                //aircraftModel.AdditionalFuel = aircraft.AdditionalFuel ?? string.Empty;
                aircraftModel.AdditionalFuel = aircraft.AdditionalFuel == null ? string.Empty : aircraft.AdditionalFuel.ToString();
                aircraftModel.Altitude = aircraft.Altitude ?? default(double);
                aircraftModel.Capacity = aircraft.Capacity ?? string.Empty;
                aircraftModel.Color = aircraft.Color ?? string.Empty;
                aircraftModel.IsAmericanAircraft = aircraft.IsAmericaAircraft ?? default(bool);
                aircraftModel.FuelBurnCruise = aircraft.FuelBurnCruise ?? default(double);
                aircraftModel.FuelBurnRateClimb = aircraft.FuelBurnRateClimb ?? default(double);
                aircraftModel.FuelBurnRateOfDescent = aircraft.FuelBurnRateOfDescent ?? default(double);
                aircraftModel.FuelUnit = aircraft.FuelUnit ?? string.Empty;
                aircraftModel.HomeBase = aircraft.HomeBase ?? string.Empty;
                aircraftModel.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                aircraftModel.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                aircraftModel.Make = aircraft.Make ??
                                     (string.IsNullOrEmpty(aircraft.OtherAircraftManufacturer) ? 0 : -1);
                aircraftModel.Model = aircraft.Model ?? (string.IsNullOrEmpty(aircraft.OtheAircraftrModel) ? 0 : -1);


                if (aircraftModel.Make != -1)
                {
                    aircraftModel.OtherAircraftManufacturer = "";
                }

                if (aircraftModel.Model != -1)
                {
                    aircraftModel.OtherAircraftManufacturer = "";
                }

                aircraftModel.MP = aircraft.MP ?? default(double);
                aircraftModel.Power = aircraft.Power ?? default(double);
                aircraftModel.RateOfClimb = aircraft.RateOfClimb ?? default(double);
                aircraftModel.RateOfDescent = aircraft.RateOfDescent ?? default(double);
                aircraftModel.Registration = aircraft.Registration ?? string.Empty;
                aircraftModel.RPM = aircraft.RPM ?? default(double);
                aircraftModel.SpeedUnit = aircraft.SpeedUnit ?? string.Empty;
                aircraftModel.TASClimb = aircraft.TASClimb ?? default(double);
                aircraftModel.TASCruise = aircraft.TASCruise ?? default(double);
                aircraftModel.TASDescent = aircraft.TASDescent ?? default(double);
                //aircraftModel.TaxiFuel = aircraft.TaxiFuel ?? string.Empty;
                aircraftModel.TaxiFuel = aircraft.TaxiFuel == null ? string.Empty : aircraft.TaxiFuel.ToString();//ChangeDataType
                aircraftModel.VerticalSpeed = aircraft.VerticalSpeed ?? string.Empty;
                aircraftModel.WeightUnit = aircraft.WeightUnit ?? string.Empty;

                aircraftModel.AreoUnitNo = aircraft.AeroUnitNo ?? string.Empty;
                aircraftModel.IsSingleEngine = aircraft.EngineType == Enumerations.EngineType.SingleEngine.ToString();
                aircraftModel.Part = (aircraft.Part == "Part 91");
                //aircraftModel.AircraftType = aircraft.AircraftType ?? string.Empty;
                aircraftModel.AircraftSerialNumber = aircraft.AircraftSerialNo ?? string.Empty;
                aircraftModel.AircraftYear = aircraft.AircraftYear;
                GA.Common.Misc misc = new GA.Common.Misc();


                aircraftModel.CurrentHobbsTime = Misc.ConvertMinToOneTenthOFTime(aircraft.HobbsTime ?? 0);// TimeSpan.FromMinutes(aircraft.HobbsTime ?? 0).ToString();
                aircraftModel.HobbsTimeOffset = Misc.ConvertMinToOneTenthOFTime(aircraft.HobbsTimeOffset ?? 0); //TimeSpan.FromMinutes(aircraft.HobbsTimeOffset ?? 0).ToString();
                aircraftModel.TotalHobbsTime = Misc.ConvertMinToOneTenthOFTime((aircraft.HobbsTime ?? 0) + (aircraft.HobbsTimeOffset ?? 0)); // TimeSpan.FromMinutes((aircraft.HobbsTime ?? 0) + (aircraft.HobbsTimeOffset ?? 0)).ToString();
                aircraftModel.CurrentTachTime = Misc.ConvertMinToOneTenthOFTime(aircraft.TachTime ?? 0); // TimeSpan.FromMinutes(aircraft.TachTime ?? 0).ToString();
                aircraftModel.TachTimeOffset = Misc.ConvertMinToOneTenthOFTime(aircraft.TachTimeOffset ?? 0); // TimeSpan.FromMinutes(aircraft.TachTimeOffset ?? 0).ToString();
                aircraftModel.TotalTechTime = Misc.ConvertMinToOneTenthOFTime((aircraft.TachTime ?? 0) + (aircraft.TachTimeOffset ?? 0)); // TimeSpan.FromMinutes((aircraft.TachTime ?? 0) + (aircraft.TachTimeOffset ?? 0)).ToString();

                aircraftModel.EngineMFGType = aircraft.EngineMFGType ?? (string.IsNullOrEmpty(aircraft.OtherEngineManufacturerType) ? 0 : -1);
                aircraftModel.OtherEngineManufactureType = aircraft.OtherEngineManufacturerType;
                aircraftModel.EngineTBO = aircraft.EngineTBO ?? default(double);
                aircraftModel.Engine1LastMOHDate = (aircraft.Engine1LastMOHDate == null)
                    ? string.Empty
                    : ((DateTime)aircraft.Engine1LastMOHDate).ToShortDateString();
                // Misc.GetStringOnlyDate(aircraft.Engine1LastMOHDate);

                aircraftModel.CurrentEngineTime1 = Misc.ConvertMinToOneTenthOFTime(aircraft.CurrentEngineTime ?? 0); // TimeSpan.FromMinutes(aircraft.CurrentEngineTime ?? 0).ToString();
                aircraftModel.EngineTimeOffset1 = Misc.ConvertMinToOneTenthOFTime(aircraft.EngineTimeOffset ?? 0); // TimeSpan.FromMinutes(aircraft.EngineTimeOffset ?? 0).ToString();
                aircraftModel.TotalEngineTime1 = Misc.ConvertMinToOneTenthOFTime((aircraft.CurrentEngineTime ?? 0) + (aircraft.EngineTimeOffset ?? 0)); // TimeSpan.FromMinutes((aircraft.CurrentEngineTime ?? 0) + (aircraft.EngineTimeOffset ?? 0)).ToString();

                aircraftModel.PropMFG = aircraft.PropMFG ?? (string.IsNullOrEmpty(aircraft.OtherPropManufacturer) ? 0 : -1);
                aircraftModel.OtherPropManufactureType = aircraft.OtherPropManufacturer;

                aircraftModel.Prop1TBO = (aircraft.Prop1TBO == null) ? default(int) : (int)aircraft.Prop1TBO;
                aircraftModel.Prop1OHDueDate = (aircraft.Prop1OHDueDate == null)
                    ? string.Empty
                    : ((DateTime)aircraft.Prop1OHDueDate).ToShortDateString();

                aircraftModel.CurrentPropTime1 = Misc.ConvertMinToOneTenthOFTime(aircraft.Prop1Time ?? 0); // TimeSpan.FromMinutes(aircraft.Prop1Time ?? 0).ToString();
                aircraftModel.PropTimeOffset1 = Misc.ConvertMinToOneTenthOFTime(aircraft.Prop1TimeOffset ?? 0); // TimeSpan.FromMinutes(aircraft.Prop1TimeOffset ?? 0).ToString();
                aircraftModel.TotalPropTime1 = Misc.ConvertMinToOneTenthOFTime((aircraft.Prop1Time ?? 0) + (aircraft.Prop1TimeOffset ?? 0)); // TimeSpan.FromMinutes((aircraft.Prop1Time ?? 0) + (aircraft.Prop1TimeOffset ?? 0)).ToString();

                aircraftModel.Engine2LastMOHDate = (aircraft.Engine2LastMOHDate == null)
                    ? string.Empty
                    : ((DateTime)aircraft.Engine2LastMOHDate).ToShortDateString();

                aircraftModel.CurrentEngineTime2 = Misc.ConvertMinToOneTenthOFTime(aircraft.Engine2Time ?? 0);// TimeSpan.FromMinutes(aircraft.Engine2Time ?? 0).ToString();
                aircraftModel.EngineTimeOffset2 = Misc.ConvertMinToOneTenthOFTime(aircraft.Engine2TimeOffset ?? 0);// TimeSpan.FromMinutes(aircraft.Engine2TimeOffset ?? 0).ToString();
                aircraftModel.TotalEngineTime2 = Misc.ConvertMinToOneTenthOFTime((aircraft.Engine2Time ?? 0) + (aircraft.Engine2TimeOffset ?? 0));// TimeSpan.FromMinutes( (aircraft.Engine2Time ?? 0) + (aircraft.Engine2TimeOffset ?? 0)).ToString();

                aircraftModel.Prop2OHDueDate = (aircraft.Prop2OHDueDate == null)
                    ? string.Empty
                    : ((DateTime)aircraft.Prop2OHDueDate).ToShortDateString();

                aircraftModel.CurrentPropTime2 = Misc.ConvertMinToOneTenthOFTime(aircraft.Prop2Time ?? 0);// TimeSpan.FromMinutes(aircraft.Prop2Time ?? 0).ToString();
                aircraftModel.PropTimeOffset2 = Misc.ConvertMinToOneTenthOFTime(aircraft.Prop2Timeoffset ?? 0);// TimeSpan.FromMinutes(aircraft.Prop2Timeoffset ?? 0).ToString();
                aircraftModel.TotalPropTime2 = Misc.ConvertMinToOneTenthOFTime((aircraft.Prop2Timeoffset ?? 0) + (aircraft.Prop2Time ?? 0));// TimeSpan.FromMinutes( (aircraft.Prop2Timeoffset ?? 0) + (aircraft.Prop2Time ?? 0)).ToString();


                aircraftModel.AircraftId = aircraft.Id;
                aircraftModel.Comm = aircraft.Comm;
                aircraftModel.Transponder = aircraft.Transponder;
                aircraftModel.EngineMonitor = aircraft.EngineMonitor;
                aircraftModel.ChargeBy = aircraft.ChargeBy ?? 0;
                aircraftModel.aircraftSalesDetailList = new List<AircraftSalesDetailWeb>();
                aircraftModel.aircraftSalesDetailList = context.AircraftSalesDetails
                                                                .Where(s => s.AircraftId == aircraftId)
                                                                .Select(s => new AircraftSalesDetailWeb
                                                                {
                                                                    Id = s.Id,
                                                                    SalesDetailtype = s.SalesDetailMaster.DetailType,
                                                                    Price = (s.Price),
                                                                    GLAccountNumber = s.GLAccountNumber
                                                                }).ToList();
                // set the values for All the unit's id.
                aircraftModel = SetIdForAllUnits(aircraftModel);

                //aircraftModel = SetAllComponentDetails(aircraftModel);

                var documentList = context.Documents.Where(d => !d.Deleted && d.AircraftId == aircraftId).Select(d => d.Url).ToList();

                aircraftModel.AircraftComponentMakeAndModels = (from c in context.AircraftComponents
                                                                join cm in context.AircraftComponentMakeAndModels on new { AircraftComponentId = c.Id, AircraftId = aircraftId } equals new { AircraftComponentId = (int)cm.AircraftComponentId, AircraftId = cm.AircraftId } into cm_join
                                                                from cm in cm_join.DefaultIfEmpty()
                                                                where c.ComponentName != "Transponder"
                                                                select new AircraftComponentMakeAndModelWeb
                                                                {
                                                                    Id = cm != null ? cm.Id : 0,
                                                                    AircraftId = aircraftId,
                                                                    AircraftComponentId = c.Id,
                                                                    AircraftComponentName = c.ComponentName,
                                                                    ComponentManufacturerId = cm != null ? cm.ComponentManufacturerId : null,
                                                                    ComponentManufacturerName = cm != null && cm.ComponentManufacturerId != null ? cm.AircraftComponentManufacturer.Name : string.Empty,
                                                                    ComponentModelId = cm != null ? cm.ComponentModelId : null,
                                                                    ComponentModelName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.Name : string.Empty,
                                                                    ComponentModelFileName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.UserMannual : string.Empty,
                                                                    IsShowInFlightBag = cm != null && cm.ComponentModelId != null && documentList.Contains(cm.AircraftComponentModel.UserMannual),
                                                                    ModelFreetext = cm.ComponentModelFreetext,
                                                                    IsVisibleOnMFDScreen = cm.IsVisibleOnMFDScreen ?? false
                                                                }).ToList();

                var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraftId);

                var distinctManuId = context.AircraftComponentMakeAndModels.Where(s => s.AircraftId == aircraftId && s.ComponentManufacturerId != null).Select(s => s.ComponentManufacturerId).Distinct().ToList();
                aircraftModel.manufacturerAuthForDataLogList = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id)).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
                if (mappingAircraftManufacturerAndUsers != null)
                {
                    List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                    aircraftModel.manufacturerAuthForDataLogList.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
                }


                aircraftModel.manufacturerAuthForDataLogList = aircraftModel.manufacturerAuthForDataLogList.OrderBy(o => o.ManufacturerId).ToList();

                aircraftModel.AircraftModel = GetAircraftModelsByManufactureId(aircraftModel.Make);

                aircraftModel.OwnerEmailId =
                    context.Profiles.FirstOrDefault(p => p.Id == aircraft.OwnerProfileId).EmailId;
                aircraftModel.IsOwner = (aircraft.OwnerProfileId == profileId);

                IPassengerHelper objPassengerHelper = new IPassengerHelper();
                aircraftModel.iPassengerMediaDetails = objPassengerHelper.GetIPassengerMediaDetails(aircraftId);

                var fileName = aircraft.ImageUrl;
                //string path = ConfigurationReader.ImageServerPathKey;
                string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                {
                    aircraftModel.AircraftImagePath = (path + fileName);
                }
                else
                {
                    aircraftModel.AircraftImagePath = Constants.AircraftDefaultImage;
                }
                aircraftModel.Maintenance = new MaintenanceModelWeb();
                aircraftModel.Maintenance.MaintenanceUserList = new List<MaintenanceUserModel>();
                aircraftModel.Maintenance.MaintenanceUserList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraft.Id).Select(s => new MaintenanceUserModel
                {
                    EmailId = s.Profile.EmailId,
                    Id = s.Profile.Id,
                    FirstName = (s.Profile.UserDetail == null) ? "" : s.Profile.UserDetail.FirstName,
                    LastName = (s.Profile.UserDetail == null) ? "" : s.Profile.UserDetail.LastName,
                    IsRegister = (s.Profile.UserDetail == null) ? false : (string.IsNullOrEmpty(s.Profile.UserDetail.FirstName) ? false : true)
                }).ToList();
                if (aircraftModel.Maintenance.MaintenanceUserList == null)
                {
                    aircraftModel.Maintenance.MaintenanceUserList = new List<MaintenanceUserModel>();
                }

                var issueList = context.Issues.Where(f => f.AircraftId == aircraft.Id);

                bool isMaintenanceUser = (objProfile.UserTypeId == (int)Enumerations.UserType.Maintenance);

                if (isMaintenanceUser)
                {
                    issueList = issueList.Where(p => p.Assignee == profileId);
                }

                var tempIssue = issueList.Select(s => new
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.Assigner,
                    AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                    AssigneeId = s.Assignee,
                    AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedBy,
                    ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                    CreateDate = (s.CreateDate),
                    UpdateDate = (s.UpdateDate),
                    RegistrationNo = s.AircraftProfile.Registration,
                    PriorityId = s.PriorityId,
                    IsOwner = s.AircraftProfile.OwnerProfileId == profileId,
                    Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail

                }).ToList();

                aircraftModel.Maintenance.SquawkListModel = new SquawkListModelWeb();
                aircraftModel.Maintenance.SquawkListModel.IsOwner = (aircraft.OwnerProfileId == profileId);
                aircraftModel.Maintenance.SquawkListModel.UserType = objProfile.UserMaster.UserType;
                aircraftModel.Maintenance.SquawkListModel.IssueList = new List<IssueModel>();

                tempIssue.ForEach(s => aircraftModel.Maintenance.SquawkListModel.IssueList.Add(new IssueModel
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.AssignerId,
                    AssignerName = s.AssignerName,
                    AssigneeId = s.AssigneeId,
                    AssigneeName = s.AssigneeName,
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedById,
                    ClosedByName = s.ClosedByName,
                    CreateDate = (s.CreateDate.ToShortDateString()),
                    UpdateDate = (s.UpdateDate.ToShortDateString()),
                    RegistrationNo = s.RegistrationNo,
                    PriorityId = s.PriorityId,
                    IsOwner = s.IsOwner,
                    Comment = s.Comment
                }));

                if (aircraftModel.Maintenance.SquawkListModel.IssueList != null)
                {
                    aircraftModel.Maintenance.SquawkListModel.IssueList = aircraftModel.Maintenance.SquawkListModel.IssueList.OrderByDescending(o => o.Id).ToList();
                }

                aircraftModel.Maintenance.SquawkListModel.aircraftList = context.AircraftProfiles.Where(f => f.OwnerProfileId == profileId).Select(s => new ListItems { text = s.Registration, value = s.Id }).ToList();
                aircraftModel.Maintenance.SquawkListModel.aircraftList.Add(new ListItems() { text = "Select", value = 0 });
                aircraftModel.Maintenance.SquawkListModel.aircraftList = aircraftModel.Maintenance.SquawkListModel.aircraftList.OrderBy(o => o.value).ToList();
                aircraftModel.Maintenance.SquawkListModel.aircraftIdForIssue = 0;

                aircraftModel.Maintenance.SquawkListModel.UserList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraft.Id && f.Profile.UserDetail != null && !string.IsNullOrEmpty(f.Profile.UserDetail.FirstName)).Select(s => new ListItems
                {
                    text = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                    value = s.Profile.Id
                }).ToList();
                aircraftModel.Maintenance.SquawkListModel.UserList.Add(new ListItems() { text = "Select", value = 0 });
                aircraftModel.Maintenance.SquawkListModel.UserList = aircraftModel.Maintenance.SquawkListModel.UserList.OrderBy(o => o.value).ToList();
                aircraftModel.Maintenance.SquawkListModel.userIdForIssue = 0;
                return aircraftModel;
            }
        }

        public AircraftProfileModelWeb SetAllComponentDetails(AircraftProfileModelWeb aircraftModel)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                aircraftModel.CommManufacturerId = 0;
                aircraftModel.CommModelId = 0;
                aircraftModel.TransponderManufacturerId = 0;
                aircraftModel.TransponderModelId = 0;
                aircraftModel.EngineMonitorManufacturerId = 0;
                aircraftModel.EngineMonitorModelId = 0;


                var ComponentList = context.AircraftComponents.Where(a => !a.Deleted).ToList();

                aircraftModel.CommId = ComponentList.FirstOrDefault(c => c.ComponentName == "Comm").Id;
                aircraftModel.TransponderId = ComponentList.FirstOrDefault(c => c.ComponentName == "Transponder").Id;
                aircraftModel.EngineMonitorId = ComponentList.FirstOrDefault(c => c.ComponentName == "EngineMonitor").Id;

                var componentMakeAndModelList =
                    context.AircraftComponentMakeAndModels.Where(a => a.AircraftId == aircraftModel.AircraftId).ToList();

                if (componentMakeAndModelList.Any())
                {
                    var comm = componentMakeAndModelList.FirstOrDefault(c => c.AircraftComponentId == aircraftModel.CommId);
                    if (comm != null)
                    {
                        aircraftModel.CommManufacturerId = comm.ComponentManufacturerId;
                        aircraftModel.CommModelId = comm.ComponentModelId;
                        aircraftModel.commFileName = GetFileNamebyComponentModelId(aircraftModel.CommModelId ?? 0);
                    }

                    var transponder = componentMakeAndModelList.FirstOrDefault(c => c.AircraftComponentId == aircraftModel.TransponderId);
                    if (transponder != null)
                    {
                        aircraftModel.TransponderManufacturerId = transponder.ComponentManufacturerId;
                        aircraftModel.TransponderModelId = transponder.ComponentModelId;
                        aircraftModel.TransponderFileName = GetFileNamebyComponentModelId(aircraftModel.TransponderModelId ?? 0);
                    }

                    var engineMonitor = componentMakeAndModelList.FirstOrDefault(c => c.AircraftComponentId == aircraftModel.EngineMonitorId);
                    if (engineMonitor != null)
                    {
                        aircraftModel.EngineMonitorManufacturerId = engineMonitor.ComponentManufacturerId;
                        aircraftModel.EngineMonitorModelId = engineMonitor.ComponentModelId;
                        aircraftModel.EngineMonitorFileNAme = GetFileNamebyComponentModelId(aircraftModel.EngineMonitorModelId ?? 0);
                    }
                    //aircraftModel.CommModelList = GetComponentModelsByManufactureId(aircraftModel.CommManufacturerId, ConfigurationReader.Comm);
                    //aircraftModel.TransponderModelList = GetComponentModelsByManufactureId(aircraftModel.TransponderManufacturerId, ConfigurationReader.Transponder);
                    //aircraftModel.EngineMonitorModelList = GetComponentModelsByManufactureId(aircraftModel.EngineMonitorManufacturerId, ConfigurationReader.EngineMonitor);
                }
                return aircraftModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ListItems> GetAircraftModelsByManufactureId(int? manufactureId)
        {
            var context = new GuardianAvionicsEntities();
            var objAircraftModel = new List<ListItems>();
            if (manufactureId != -1)
            {
                objAircraftModel.Add(new ListItems() { text = "Select", value = 0 });
            }

            if (manufactureId != 0)
            {
                objAircraftModel.AddRange((from m in context.AircraftModelLists
                                           where m.AircraftManufacturerId == manufactureId && !m.Deleted
                                           orderby m.Id
                                           select new ListItems
                                           {
                                               text = m.ModelName,
                                               value = m.Id
                                           }).ToList());

                objAircraftModel = objAircraftModel.OrderBy(o => o.value).ToList();
                objAircraftModel.Add(new ListItems() { text = "Other", value = -1 });
            }

            return objAircraftModel;
        }


        public List<ListItems> GetComponentModelsByManufactureId(int? manufactureId, int componentId)
        {
            var context = new GuardianAvionicsEntities();

            var componentModels = context.AircraftComponentModels.Where(w => w.AircraftComponentId == componentId && w.AircraftComponentManufacturerId == manufactureId && !w.Deleted)
                                    .Select(cm => new ListItems
                                    {
                                        text = cm.Name,
                                        value = cm.Id
                                    }).ToList();

            componentModels.Insert(0, new ListItems() { text = "Select", value = 0 });
            return componentModels;
        }

        public string GetFileNamebyComponentModelId(int componentModelId)
        {
            var context = new GuardianAvionicsEntities();
            string fileName = string.Empty;
            try
            {
                var aircraftComponentModels = context.AircraftComponentModels.FirstOrDefault(m => m.Id == componentModelId);
                fileName = aircraftComponentModels == null ? string.Empty : aircraftComponentModels.UserMannual;
            }
            catch (Exception)
            {
                fileName = "";
            }
            return fileName;
        }

        /// <summary>
        /// Set Id for dropdown list for all the units
        /// </summary>
        /// <param name="aircraftModel">AircraftProfileModelWeb object</param>
        /// <returns>AircraftProfileModelWeb object</returns>
        private AircraftProfileModelWeb SetIdForAllUnits(AircraftProfileModelWeb aircraftModel)
        {
            if (aircraftModel == null)
                return null;

            using (var context = new GuardianAvionicsEntities())
            {
                var speedId =
                    context.MeasurementUnits.FirstOrDefault(s => s.MeasurementUnitName == aircraftModel.SpeedUnit);
                var verticalId =
                    context.MeasurementUnits.FirstOrDefault(v => v.MeasurementUnitName == aircraftModel.VerticalSpeed);
                var fuelId =
                    context.MeasurementUnits.FirstOrDefault(f => f.MeasurementUnitName == aircraftModel.FuelUnit);
                var weightId =
                    context.MeasurementUnits.FirstOrDefault(w => w.MeasurementUnitName == aircraftModel.WeightUnit);

                if (speedId != default(MeasurementUnit))
                    aircraftModel.SpeedUnitId = speedId.Id;

                if (verticalId != default(MeasurementUnit))
                    aircraftModel.VerticalSpeedUnitId = verticalId.Id;

                if (fuelId != default(MeasurementUnit))
                    aircraftModel.FuelUnitId = fuelId.Id;

                if (weightId != default(MeasurementUnit))
                    aircraftModel.WeightUnitId = weightId.Id;
            }
            return aircraftModel;
        }

        /// <summary>
        /// Set the content for dropdown list.
        /// </summary>
        /// <param name="aircraftModel">AircraftProfileModelWeb object</param>
        /// <returns>AircraftProfileModelWeb object</returns>
        public AircraftProfileModelWeb SetSelectListForAllUnits(AircraftProfileModelWeb aircraftModel)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                aircraftModel.VerticalSpeedList = new List<ListItems>();
                aircraftModel.SpeedList = new List<ListItems>();
                aircraftModel.FuelList = new List<ListItems>();
                aircraftModel.WeightList = new List<ListItems>();

                var speedDb = context.MeasurementUnits.Where(s => s.Measurement.MeasurementName == "SpeedUnit").ToList();

                foreach (var spd in speedDb)
                {
                    aircraftModel.SpeedList.Add(new ListItems
                    {
                        text = spd.MeasurementUnitName,
                        value = spd.Id,
                    });
                }

                var fuelDb = context.MeasurementUnits.Where(f => f.Measurement.MeasurementName == "FuelUnit").ToList();

                foreach (var fl in fuelDb)
                {
                    aircraftModel.FuelList.Add(new ListItems
                    {
                        text = fl.MeasurementUnitName,
                        value = fl.Id,
                    });
                }

                var weightDb =
                    context.MeasurementUnits.Where(f => f.Measurement.MeasurementName == "WeightUnit").ToList();

                foreach (var wt in weightDb)
                {
                    aircraftModel.WeightList.Add(new ListItems
                    {
                        text = wt.MeasurementUnitName,
                        value = wt.Id,
                    });
                }

                var verticalDb =
                    context.MeasurementUnits.Where(v => v.Measurement.MeasurementName == "VerticalSpeedUnit").ToList();

                foreach (var vspd in verticalDb)
                {
                    aircraftModel.VerticalSpeedList.Add(new ListItems
                    {
                        text = vspd.MeasurementUnitName,
                        value = vspd.Id,
                    });
                }

                aircraftModel.Manufacturer = (from m in context.AircraftManufacturers
                                              where m.Deleted == false
                                              select new ListItems
                                              {
                                                  text = m.Name,
                                                  value = m.Id
                                              }).ToList();

                aircraftModel.Manufacturer.Add(new ListItems() { text = "Select", value = 0 });
                aircraftModel.Manufacturer = aircraftModel.Manufacturer.OrderBy(m => m.value).ToList();
                aircraftModel.Manufacturer.Add(new ListItems() { text = "Other", value = -1 });
                aircraftModel.AircraftModel = new List<ListItems>();


                //var ComponentList = context.AircraftComponents.Where(a => !a.Deleted).ToList();

                //var CommId = ComponentList.FirstOrDefault(c => c.ComponentName == "Comm").Id;
                //var TransponderId = ComponentList.FirstOrDefault(c => c.ComponentName == "Transponder").Id;
                //var EngineMonitorId = ComponentList.FirstOrDefault(c => c.ComponentName == "EngineMonitor").Id;

                aircraftModel.ComponentModelList = context.AircraftComponentModels.Where(cm => !cm.Deleted).ToList();

                aircraftModel.AircraftComponentManufacturerList = aircraftModel.ComponentModelList.GroupBy(cm => new
                {
                    AircraftComponentManufacturerId = cm.AircraftComponentManufacturer.Id,
                    AircraftComponentManufacturerName = cm.AircraftComponentManufacturer.Name,
                    AircraftComponentId = cm.AircraftComponent.Id,
                    AircraftComponentName = cm.AircraftComponent.ComponentName
                }).Select(g => new AircraftComponentManufacturerModel
                {
                    AircraftComponentManufacturerId = g.Key.AircraftComponentManufacturerId,
                    AircraftComponentManufacturerName = g.Key.AircraftComponentManufacturerName,
                    AircraftComponentId = g.Key.AircraftComponentId,
                    AircraftComponentName = g.Key.AircraftComponentName
                }).ToList();

                aircraftModel.AircraftComponentMakeAndModels = (from c in context.AircraftComponents
                                                                join cm in context.AircraftComponentMakeAndModels on new { AircraftComponentId = c.Id, AircraftId = 0 } equals new { AircraftComponentId = (int)cm.AircraftComponentId, AircraftId = cm.AircraftId } into cm_join
                                                                from cm in cm_join.DefaultIfEmpty()
                                                                where c.ComponentName != "Transponder"
                                                                select new AircraftComponentMakeAndModelWeb
                                                                {
                                                                    Id = cm != null ? cm.Id : 0,
                                                                    AircraftId = 0,
                                                                    AircraftComponentId = c.Id,
                                                                    AircraftComponentName = c.ComponentName,
                                                                    ComponentManufacturerId = cm != null ? cm.ComponentManufacturerId : null,
                                                                    ComponentManufacturerName = cm != null && cm.ComponentManufacturerId != null ? cm.AircraftComponentManufacturer.Name : string.Empty,
                                                                    ComponentModelId = cm != null ? cm.ComponentModelId : null,
                                                                    ComponentModelName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.Name : string.Empty,
                                                                    ComponentModelFileName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.UserMannual : string.Empty,
                                                                    IsShowInFlightBag = false,
                                                                    ModelFreetext = cm.ComponentModelFreetext,
                                                                    IsVisibleOnMFDScreen = cm.IsVisibleOnMFDScreen ?? false
                                                                }).ToList();


                //aircraftModel.CommManufacturerList =
                //    componentManufacturers.Where(cm => cm.AircraftComponentName == ConfigurationReader.Comm)
                //        .Select(
                //            cm =>
                //                new ListItems()
                //                {
                //                    Text = cm.AircraftComponentManufacturerName,
                //                    Value = cm.AircraftComponentManufacturerId
                //                }).ToList();
                //aircraftModel.CommManufacturerList.Insert(0, new ListItems() { Text = "Select", Value = 0 });

                //aircraftModel.TransponderManufacturerList =
                //    componentManufacturers.Where(cm => cm.AircraftComponentName == ConfigurationReader.Transponder)
                //        .Select(
                //            cm =>
                //                new ListItems()
                //                {
                //                    Text = cm.AircraftComponentManufacturerName,
                //                    Value = cm.AircraftComponentManufacturerId
                //                }).ToList();
                //aircraftModel.TransponderManufacturerList.Insert(0, new ListItems() { Text = "Select", Value = 0 });

                //aircraftModel.EngineMonitorManufacturerList =
                //    componentManufacturers.Where(cm => cm.AircraftComponentName == ConfigurationReader.EngineMonitor)
                //        .Select(
                //            cm =>
                //                new ListItems()
                //                {
                //                    Text = cm.AircraftComponentManufacturerName,
                //                    Value = cm.AircraftComponentManufacturerId
                //                })
                //        .ToList();
                //aircraftModel.EngineMonitorManufacturerList.Insert(0, new ListItems() { Text = "Select", Value = 0 });

                aircraftModel.CommModelList = new List<ListItems>();
                aircraftModel.TransponderModelList = new List<ListItems>();
                aircraftModel.EngineMonitorModelList = new List<ListItems>();
                aircraftModel.EngineMFGTypeList = new List<ListItems>();
                aircraftModel.PropellerManufacturrList = new List<ListItems>();



                aircraftModel.EngineMFGTypeList = (from m in context.Engines
                                                   where m.Deleted == false
                                                   select new ListItems
                                                   {
                                                       text = m.Name,
                                                       value = m.Id
                                                   }).ToList();
                aircraftModel.EngineMFGTypeList.Add(new ListItems() { text = "Select", value = 0 });
                aircraftModel.EngineMFGTypeList = aircraftModel.EngineMFGTypeList.OrderBy(m => m.value).ToList();
                aircraftModel.EngineMFGTypeList.Add(new ListItems() { text = "Other", value = -1 });


                aircraftModel.PropellerManufacturrList = (from m in context.Propellers
                                                          where m.Deleted == false
                                                          select new ListItems
                                                          {
                                                              text = m.Name,
                                                              value = m.Id
                                                          }).ToList();
                aircraftModel.PropellerManufacturrList.Add(new ListItems() { text = "Select", value = 0 });
                aircraftModel.PropellerManufacturrList = aircraftModel.PropellerManufacturrList.OrderBy(m => m.value).ToList();
                aircraftModel.PropellerManufacturrList.Add(new ListItems() { text = "Other", value = -1 });
            }
            return aircraftModel;
        }

        /// <summary>
        /// Updates the given aircraft profile object into database
        /// </summary>
        /// <param name="obj">AircraftProfileModelWeb Object</param>
        /// <returns>Boolean variable , for success or failure</returns>
        public bool UpdateAircraftProfile(AircraftProfileModelWeb obj)
        {

            using (var context = new GuardianAvionicsEntities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {


                    try
                    {
                        if (obj == default(AircraftProfileModelWeb) || obj.AircraftId <= 0) return false;
                        var aircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == obj.AircraftId);

                        if (aircraft == default(AircraftProfile)) return false;
                        // set values for dropdown list
                        aircraft = SetValuesForAllIds(aircraft, obj);

                        var objProfile = GetOwnerProfileByAircraftId(obj.AircraftId);
                        if (objProfile.EmailId != obj.OwnerEmailId)
                        {
                            objProfile = new UserHelper().GetProfileDetailByEmailId(obj.OwnerEmailId);
                            aircraft.OwnerProfileId = objProfile.Id;
                            if (context.MappingAircraftAndPilots.FirstOrDefault(f => f.ProfileId == objProfile.Id && f.AircraftId == obj.AircraftId) == null)
                            {
                                context.MappingAircraftAndPilots.Add(new DataLayer.MappingAircraftAndPilot
                                {
                                    AircraftId = obj.AircraftId,
                                    CreateDate = DateTime.UtcNow,
                                    Deleted = false,
                                    IsActive = true,
                                    ProfileId = objProfile.Id
                                });
                            }

                        }

                        if (obj.UpdateModuleName == "General")
                        {
                            aircraft.LastUpdated = DateTime.UtcNow;
                        }
                        else if (obj.UpdateModuleName == "Performance")
                        {
                            aircraft.LastUpdateDatePerformance = DateTime.UtcNow;
                        }

                        aircraft.IsAmericaAircraft = (obj.IsAmericanAircraft != null) && (bool)obj.IsAmericanAircraft;
                        aircraft.Registration = obj.Registration;
                        aircraft.Make = obj.Make;
                        aircraft.Model = obj.Model;
                        aircraft.OtherAircraftManufacturer = obj.OtherAircraftManufacturer;
                        aircraft.OtheAircraftrModel = obj.OtheAircraftrModel;

                        if (obj.Make == 0 || obj.Make == -1)
                        {
                            aircraft.Make = null;
                        }
                        if (obj.Make != -1)
                        {
                            aircraft.OtherAircraftManufacturer = "";
                        }

                        if (obj.Model == 0 || obj.Model == -1)
                        {
                            aircraft.Model = null;
                        }
                        if (obj.Model != -1)
                        {
                            aircraft.OtheAircraftrModel = "";
                        }

                        aircraft.Color = obj.Color;
                        aircraft.HomeBase = obj.HomeBase;
                        aircraft.Capacity = obj.Capacity;
                        //aircraft.TaxiFuel = obj.TaxiFuel;
                        aircraft.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(obj.TaxiFuel) ? null : obj.TaxiFuel); //ChangeDataType
                        //aircraft.AdditionalFuel = obj.AdditionalFuel;
                        aircraft.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(obj.AdditionalFuel) ? null : obj.AdditionalFuel);//ChangeDataType
                        aircraft.Power = obj.Power;
                        aircraft.RPM = obj.RPM;
                        aircraft.MP = obj.MP;
                        aircraft.Altitude = obj.Altitude;
                        aircraft.FuelBurnCruise = obj.FuelBurnCruise;
                        aircraft.TASCruise = obj.TASCruise;

                        aircraft.RateOfClimb = obj.RateOfClimb;
                        aircraft.FuelBurnRateClimb = obj.FuelBurnRateClimb;
                        aircraft.TASClimb = obj.TASClimb;

                        aircraft.FuelBurnRateOfDescent = obj.FuelBurnRateOfDescent;
                        aircraft.RateOfDescent = obj.RateOfDescent;
                        aircraft.TASDescent = obj.TASDescent;

                        aircraft.AeroUnitNo = obj.AreoUnitNo;
                        aircraft.EngineType = obj.IsSingleEngine
                            ? Enumerations.EngineType.SingleEngine.ToString()
                            : Enumerations.EngineType.MultipleEngine.ToString();
                        GA.Common.Misc misc = new GA.Common.Misc();

                        aircraft.Part = obj.Part ? "Part 91" : "Part 135";

                        //aircraft.AircraftType = obj.AircraftType ?? string.Empty;
                        aircraft.AircraftSerialNo = obj.AircraftSerialNumber ?? string.Empty;
                        aircraft.AircraftYear = obj.AircraftYear;
                        aircraft.HobbsTime = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentHobbsTime);  //   TimeSpan.Parse(obj.CurrentHobbsTime).TotalMinutes ; // ?? default(double);
                        aircraft.HobbsTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.HobbsTimeOffset);  //  TimeSpan.Parse(obj.HobbsTimeOffset).TotalMinutes;
                        aircraft.TachTime = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentTachTime);  //  TimeSpan.Parse(obj.CurrentTachTime).TotalMinutes;
                        aircraft.TachTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.TachTimeOffset);  //  TimeSpan.Parse(obj.TachTimeOffset).TotalMinutes;
                        aircraft.EngineMFGType = obj.EngineMFGType;
                        aircraft.OtherEngineManufacturerType = obj.OtherEngineManufactureType;
                        if (obj.EngineMFGType == 0 || obj.EngineMFGType == -1)
                        {
                            aircraft.EngineMFGType = null;
                        }
                        if (obj.EngineMFGType != -1)
                        {
                            aircraft.OtherEngineManufacturerType = "";
                        }



                        aircraft.EngineTBO = obj.EngineTBO;
                        //aircraft.Engine1LastMOHDate = Misc.GetDate(obj.Engine1LastMOHDate);
                        aircraft.Engine1LastMOHDate = Misc.GetDateUS(obj.Engine1LastMOHDate); //DateFormateChange

                        aircraft.CurrentEngineTime = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentEngineTime1); // TimeSpan.Parse(obj.CurrentEngineTime1).TotalMinutes;
                        aircraft.EngineTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.EngineTimeOffset1); // TimeSpan.Parse(obj.EngineTimeOffset1).TotalMinutes;
                        aircraft.Prop1Time = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentPropTime1); // TimeSpan.Parse(obj.CurrentPropTime1).TotalMinutes;
                        aircraft.Prop1TimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.PropTimeOffset1); // TimeSpan.Parse(obj.PropTimeOffset1).TotalMinutes;

                        aircraft.Engine2Time = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentEngineTime2); //TimeSpan.Parse(obj.CurrentEngineTime2).TotalMinutes;
                        aircraft.Engine2TimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.EngineTimeOffset2);  //  TimeSpan.Parse(obj.EngineTimeOffset2).TotalMinutes;

                        aircraft.Prop2Time = Misc.ConvertOneTenthOfTimeToMinutes(obj.CurrentPropTime2);  // TimeSpan.Parse(obj.CurrentPropTime2).TotalMinutes;
                        aircraft.Prop2Timeoffset = Misc.ConvertOneTenthOfTimeToMinutes(obj.PropTimeOffset2);  // TimeSpan.Parse(obj.PropTimeOffset2).TotalMinutes;

                        aircraft.PropMFG = obj.PropMFG;
                        aircraft.OtherPropManufacturer = obj.OtherPropManufactureType;
                        if (obj.PropMFG == 0 || obj.PropMFG == -1)
                        {
                            aircraft.PropMFG = null;
                        }
                        if (obj.PropMFG != -1)
                        {
                            aircraft.OtherPropManufacturer = "";
                        }
                        aircraft.Prop1TBO = obj.Prop1TBO;
                        //aircraft.Prop1OHDueDate = Misc.GetDate(obj.Prop1OHDueDate);
                        aircraft.Prop1OHDueDate = Misc.GetDateUS(obj.Prop1OHDueDate); //DateFormateChange
                        //== null) ? string.Empty : ((DateTime)aircraft.Prop1OHDueDate).ToShortDateString();

                        //aircraft.Engine2LastMOHDate = Misc.GetDate(obj.Engine2LastMOHDate);
                        aircraft.Engine2LastMOHDate = Misc.GetDateUS(obj.Engine2LastMOHDate); //DateFormateChange

                        //aircraft.Prop2OHDueDate = Misc.GetDate(obj.Prop2OHDueDate); 
                        aircraft.Prop2OHDueDate = Misc.GetDateUS(obj.Prop2OHDueDate); //DateFormateChange
                        aircraft.Comm = obj.Comm;
                        aircraft.Transponder = obj.Transponder;
                        aircraft.EngineMonitor = obj.EngineMonitor;
                        aircraft.LastUpdated = DateTime.UtcNow;
                        aircraft.ChargeBy = obj.ChargeBy;

                        var mappingCompManuAndAircraft = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == obj.AircraftId);
                        if (obj.manufacturerAuthForDataLogList != null)
                        {
                            var manuIdlist = obj.manufacturerAuthForDataLogList.Where(s => s.ISAuthenticateForDatalog).Select(s => s.ManufacturerId.ToString()).ToList();

                            string manufacturerIds = string.Join(",", manuIdlist);

                            //  mappingCompManuAndAircraft => MappintComponentManufacturerAndAircraft


                            if (mappingCompManuAndAircraft == null)
                            {
                                var objMappingCompManuAndAircraft = context.MappingComponentManufacturerAndAircrafts.Create();
                                objMappingCompManuAndAircraft.AircraftId = obj.AircraftId;
                                objMappingCompManuAndAircraft.ComponentManufacturers = manufacturerIds;
                                context.MappingComponentManufacturerAndAircrafts.Add(objMappingCompManuAndAircraft);
                            }
                            else
                            {
                                mappingCompManuAndAircraft.ComponentManufacturers = manufacturerIds;

                            }
                        }
                        else
                        {
                            if (mappingCompManuAndAircraft != null)
                            {
                                mappingCompManuAndAircraft.ComponentManufacturers = "";
                            }
                        }

                        //get the comp manu. id that is already stored in the database
                        var AircraftComponentMakeAndModelsTemp = context.AircraftComponentMakeAndModels.FirstOrDefault(f => f.AircraftComponentId == 3 && f.AircraftId == obj.AircraftId);
                        int? prevComponentManuId = null;
                        int? CurrentComponentManuId = null;
                        if (AircraftComponentMakeAndModelsTemp != null)
                        {
                            prevComponentManuId = AircraftComponentMakeAndModelsTemp.ComponentManufacturerId;
                        }

                        CurrentComponentManuId = obj.AircraftComponentMakeAndModels.FirstOrDefault(f => f.AircraftComponentId == 3).ComponentManufacturerId;

                        //Check that the user has selected a new Component manufacturer or not
                        //If user select other manufacturer then needs to reset the plist file for engine setting
                        if (prevComponentManuId != CurrentComponentManuId)
                        {
                            //Now reset the Engine Setting file
                            var aircraftComponentManufacturers = context.AircraftComponentManufacturers.FirstOrDefault(f => f.Id == CurrentComponentManuId);
                            string componentManufacturerName = string.Empty;
                            if (aircraftComponentManufacturers != null)
                            {
                                componentManufacturerName = aircraftComponentManufacturers.Name;
                            }
                            else
                            {
                                componentManufacturerName = "JPI";
                            }
                            new CommonHelper().ResetEngineSetting(obj.AircraftId, componentManufacturerName);
                        }


                        var temp = obj.AircraftComponentMakeAndModels.Select(s => s.AircraftComponentId).ToList();
                        var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(cm => cm.AircraftId == obj.AircraftId && temp.Contains(cm.AircraftComponentId)).ToList();

                        if (aircraftComponentMakeAndModels.Count > 0)
                        {
                            aircraftComponentMakeAndModels.ForEach(delegate (AircraftComponentMakeAndModel item)
                            {
                                var componentModel =
                                    obj.AircraftComponentMakeAndModels.FirstOrDefault(
                                        cm => cm.AircraftComponentId == item.AircraftComponentId);
                                Document oldDocument = null;
                                if (item.AircraftComponentModel != null)
                                {
                                    oldDocument =
                                        context.Documents.FirstOrDefault(
                                            d => d.Url == item.AircraftComponentModel.UserMannual);
                                }
                                //var oldDocument =
                                //    context.Documents.FirstOrDefault(d => d.Url == item.AircraftComponentModel.UserMannual);
                                if (componentModel != null && componentModel.IsShowInFlightBag &&
                                    (componentModel.ComponentModelId != item.ComponentModelId || oldDocument == null))
                                {
                                    if (oldDocument != null)
                                    {
                                        context.Documents.Remove(oldDocument);
                                    }

                                    var document = context.Documents.Create();

                                    var docComponentModel = context.AircraftComponentModels.FirstOrDefault(f => f.Id == componentModel.ComponentModelId);

                                    document.Url = componentModel.ComponentModelFileName;
                                    document.ProfileId = Convert.ToInt32(aircraft.OwnerProfileId);
                                    document.IsForAll = false;
                                    document.Deleted = false;
                                    document.LastUpdated = DateTime.UtcNow;
                                    document.AircraftId = aircraft.Id;

                                    if (docComponentModel != null)
                                    {
                                        document.Url = docComponentModel.UniqueName;
                                        document.FileName = docComponentModel.UserMannual;
                                        document.FileSize = docComponentModel.FileSize;
                                        document.MimeType = docComponentModel.MimeType;
                                    }

                                    context.Documents.Add(document);
                                }
                                else
                                {
                                    if (!componentModel.IsShowInFlightBag && oldDocument != null)
                                    {
                                        context.Documents.Remove(oldDocument);
                                    }
                                }

                                item.ComponentManufacturerId = componentModel.ComponentManufacturerId == 0 ? null : componentModel.ComponentManufacturerId;
                                item.ComponentModelId = componentModel.ComponentModelId == 0 ? null : componentModel.ComponentModelId;
                                item.ComponentModelFreetext = componentModel.ModelFreetext;
                                item.IsVisibleOnMFDScreen = componentModel.IsVisibleOnMFDScreen;
                            });
                        }



                        foreach (var componentModel in obj.AircraftComponentMakeAndModels.Where(a => !aircraftComponentMakeAndModels.Select(s => s.AircraftComponentId).ToList().Contains(a.AircraftComponentId)).ToList())
                        {
                            var cm = context.AircraftComponentMakeAndModels.Create();
                            cm.AircraftComponentId = componentModel.AircraftComponentId;
                            cm.ComponentManufacturerId = componentModel.ComponentManufacturerId;
                            cm.ComponentModelId = componentModel.ComponentModelId;
                            cm.AircraftId = obj.AircraftId;
                            cm.ComponentModelFreetext = componentModel.ModelFreetext;
                            cm.IsVisibleOnMFDScreen = componentModel.IsVisibleOnMFDScreen;
                            aircraft.AircraftComponentMakeAndModels.Add(cm);

                            if (componentModel.IsShowInFlightBag)
                            {
                                var document = context.Documents.Create();

                                var docComponentModel = context.AircraftComponentModels.FirstOrDefault(f => f.Id == componentModel.ComponentModelId);
                                document.Url = docComponentModel.UniqueName;
                                document.LastUpdated = DateTime.UtcNow;
                                document.Deleted = false;
                                document.ProfileId = aircraft.OwnerProfileId ?? 0;
                                document.IsForAll = false;
                                document.AircraftId = obj.AircraftId;
                                document.FileName = docComponentModel.UserMannual;
                                document.FileSize = docComponentModel.FileSize;
                                document.MimeType = docComponentModel.MimeType;
                                aircraft.Documents.Add(document);
                            }
                        }

                        context.SaveChanges();
                        transaction.Commit();

                        //if (aircraft.DefaultAircraft == true)
                        //    // set other aircraft as not default
                        //    SetForAllAircraftDefaultAircraft(aircraft.ProfileId, aircraft.Id, context);

                        return true;

                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {

                        transaction.Rollback();
                        Exception raise = dbEx;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                // raise a new exception nesting  
                                // the current instance as InnerException  
                                raise = new InvalidOperationException(message, raise);
                            }
                        }
                        string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(obj.AircraftId);
                        string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                        if (System.IO.File.Exists(aircraftLogPath))
                        {
                            using (StreamWriter sw = System.IO.File.AppendText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Parsing the Engine setting file from WEB ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(dbEx));
                                sw.WriteLine("Aircraft Prodile Data - ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(obj));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        else
                        {
                            using (StreamWriter sw = System.IO.File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Parsing the Engine setting file from WEB ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(dbEx));
                                sw.WriteLine("Aircraft Prodile Data - ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(obj));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        throw raise;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(obj.AircraftId);
                        string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                        if (System.IO.File.Exists(aircraftLogPath))
                        {
                            using (StreamWriter sw = System.IO.File.AppendText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Updating the Aircraft profile from WEB ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("Aircraft Prodile Data - ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(obj));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        else
                        {
                            using (StreamWriter sw = System.IO.File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Updating the Aircraft profile from WEB ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("Aircraft Prodile Data - ");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(obj));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }


                        //ExceptionHandler.ReportError(e);
                        //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(obj), e);
                        return false;
                    }
                }
            }



        }

        /// <summary>
        /// Get Id's from AircraftProfileModelWeb and convert them into unit Name.
        /// </summary>
        /// <param name="aircraft">AircraftProfile Object</param>
        /// <param name="obj">AircraftProfileModelWeb Object</param>
        /// <returns>AircraftProfile Object</returns>
        private AircraftProfile SetValuesForAllIds(AircraftProfile aircraft, AircraftProfileModelWeb obj)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                var speed = context.MeasurementUnits.FirstOrDefault(d => d.Id == obj.SpeedUnitId);
                var vSpeed = context.MeasurementUnits.FirstOrDefault(d => d.Id == obj.VerticalSpeedUnitId);
                var fuel = context.MeasurementUnits.FirstOrDefault(d => d.Id == obj.FuelUnitId);
                var weight = context.MeasurementUnits.FirstOrDefault(d => d.Id == obj.WeightUnitId);

                if (speed != default(MeasurementUnit))
                    aircraft.SpeedUnit = speed.MeasurementUnitName;

                if (vSpeed != default(MeasurementUnit))
                    aircraft.VerticalSpeed = vSpeed.MeasurementUnitName;

                if (fuel != default(MeasurementUnit))
                    aircraft.FuelUnit = fuel.MeasurementUnitName;

                if (weight != default(MeasurementUnit))
                    aircraft.WeightUnit = weight.MeasurementUnitName;
            }
            return aircraft;
        }

        /// <summary>
        /// Convert Fuel from one unit to another. 
        /// </summary>
        /// <param name="fuelUnitName">Changed fuel unit</param>
        /// <param name="aircraftData">Aircraft profile Data to change</param>
        /// <returns></returns>
        public AircraftProfileModelWeb ChangeFuelUnit(string fuelUnitName, AircraftProfileModelWeb aircraftData)
        {
            if (fuelUnitName == Enumerations.FuelUnit.GAL.GetStringValue())
            {
                if (aircraftData.FuelUnit != Enumerations.FuelUnit.GAL.GetStringValue())
                {
                    // converted all the values to GAL

                    // convert Additional fuel
                    Double fuel;
                    var isSuccess = Double.TryParse(aircraftData.AdditionalFuel, out fuel);

                    if (isSuccess)
                    {
                        fuel = Math.Round(Constants.LiterToGallon * fuel, Constants.RoundOffDigit);
                        aircraftData.AdditionalFuel = fuel.ToString();
                    }

                    // convert taxi fuel
                    isSuccess = Double.TryParse(aircraftData.TaxiFuel, out fuel);
                    if (isSuccess)
                    {
                        fuel = Math.Round(Constants.LiterToGallon * fuel, Constants.RoundOffDigit);
                        aircraftData.TaxiFuel = fuel.ToString();
                    }

                    // convert capacity
                    isSuccess = Double.TryParse(aircraftData.Capacity, out fuel);
                    if (isSuccess)
                    {
                        fuel = Math.Round(Constants.LiterToGallon * fuel, Constants.RoundOffDigit);
                        aircraftData.Capacity = fuel.ToString();
                    }

                    // convert fuel burn rate

                    if (aircraftData.FuelBurnCruise != default(double))
                        aircraftData.FuelBurnCruise = Math.Round(
                            Constants.LiterToGallon * aircraftData.FuelBurnCruise, Constants.RoundOffDigit);

                    if (aircraftData.FuelBurnRateClimb != default(double))
                        aircraftData.FuelBurnRateClimb =
                            Math.Round(Constants.LiterToGallon * aircraftData.FuelBurnRateClimb,
                                Constants.RoundOffDigit);

                    if (aircraftData.FuelBurnRateOfDescent != default(double))
                        aircraftData.FuelBurnRateOfDescent =
                            Math.Round(Constants.LiterToGallon * aircraftData.FuelBurnRateOfDescent,
                                Constants.RoundOffDigit);

                    aircraftData.FuelUnit = Enumerations.FuelUnit.GAL.GetStringValue();

                    SaveAircraftProfileAfterUnitChange(aircraftData);

                    aircraftData = SetIdForAllUnits(aircraftData);
                }
            }
            else if (fuelUnitName == Enumerations.FuelUnit.LIT.GetStringValue())
            {
                if (aircraftData.FuelUnit != Enumerations.FuelUnit.LIT.GetStringValue())
                {
                    // converted all the values to LIT

                    // convert Additional fuel
                    Double fuel;
                    var IsSuccess = Double.TryParse(aircraftData.AdditionalFuel, out fuel);

                    if (IsSuccess)
                    {
                        fuel = Math.Round(Constants.GalonToLiter * fuel, Constants.RoundOffDigit);
                        aircraftData.AdditionalFuel = fuel.ToString();
                    }

                    // convert taxi fuel
                    IsSuccess = Double.TryParse(aircraftData.TaxiFuel, out fuel);
                    if (IsSuccess)
                    {
                        fuel = Math.Round(Constants.GalonToLiter * fuel, Constants.RoundOffDigit);
                        aircraftData.TaxiFuel = fuel.ToString();
                    }

                    // convert capacity
                    IsSuccess = Double.TryParse(aircraftData.Capacity, out fuel);
                    if (IsSuccess)
                    {
                        fuel = Math.Round(Constants.GalonToLiter * fuel, Constants.RoundOffDigit);
                        aircraftData.Capacity = fuel.ToString();
                    }

                    // convert fuel burn rate

                    if (aircraftData.FuelBurnCruise != default(double))
                        aircraftData.FuelBurnCruise = Math.Round(
                            Constants.GalonToLiter * aircraftData.FuelBurnCruise, Constants.RoundOffDigit);

                    if (aircraftData.FuelBurnRateClimb != default(double))
                        aircraftData.FuelBurnRateClimb =
                            Math.Round(Constants.GalonToLiter * aircraftData.FuelBurnRateClimb,
                                Constants.RoundOffDigit);

                    if (aircraftData.FuelBurnRateOfDescent != default(double))
                        aircraftData.FuelBurnRateOfDescent =
                            Math.Round(Constants.GalonToLiter * aircraftData.FuelBurnRateOfDescent,
                                Constants.RoundOffDigit);

                    aircraftData.FuelUnit = Enumerations.FuelUnit.LIT.GetStringValue();

                    SaveAircraftProfileAfterUnitChange(aircraftData);

                    aircraftData = SetIdForAllUnits(aircraftData);
                }
            } // else
            return aircraftData;
        }

        /// <summary>
        /// Saves the changes in database
        /// </summary>
        /// <param name="AircraftData">Aircraft to update</param>
        private void SaveAircraftProfileAfterUnitChange(AircraftProfileModelWeb AircraftData)
        {
            if (AircraftData == default(AircraftProfileModelWeb))
                return;

            if (AircraftData.AircraftId == default(int))
                return;

            using (var context = new GuardianAvionicsEntities())
            {
                var aircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == AircraftData.AircraftId);

                if (aircraft == default(AircraftProfile))
                    return;

                // we have an aircraft profile according to current AircraftProfileModelWeb saved in db. Now we save changes.
                //aircraft.AdditionalFuel = AircraftData.AdditionalFuel;
                aircraft.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(AircraftData.AdditionalFuel) ? null : AircraftData.AdditionalFuel);//ChangeDataType
                aircraft.Altitude = AircraftData.Altitude;
                aircraft.Capacity = AircraftData.Capacity;
                aircraft.Color = AircraftData.Color;
                aircraft.IsAmericaAircraft = (AircraftData.IsAmericanAircraft != null) &&
                                             ((bool)AircraftData.IsAmericanAircraft);
                aircraft.FuelBurnCruise = AircraftData.FuelBurnCruise;
                aircraft.FuelBurnRateClimb = AircraftData.FuelBurnRateClimb;
                aircraft.FuelBurnRateOfDescent = AircraftData.FuelBurnRateOfDescent;
                aircraft.FuelUnit = AircraftData.FuelUnit;
                aircraft.HomeBase = AircraftData.HomeBase;
                aircraft.LastUpdated = DateTime.UtcNow;
                aircraft.Make = AircraftData.Make;
                aircraft.Model = AircraftData.Model;
                aircraft.MP = AircraftData.MP;
                aircraft.Power = AircraftData.Power;
                aircraft.RateOfClimb = AircraftData.RateOfClimb;
                aircraft.RateOfDescent = AircraftData.RateOfDescent;
                aircraft.Registration = AircraftData.Registration;
                aircraft.RPM = AircraftData.RPM;
                aircraft.SpeedUnit = AircraftData.SpeedUnit;
                aircraft.TASClimb = AircraftData.TASClimb;
                aircraft.TASCruise = AircraftData.TASCruise;
                aircraft.TASDescent = AircraftData.TASDescent;
                //aircraft.TaxiFuel = AircraftData.TaxiFuel;
                aircraft.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(AircraftData.TaxiFuel) ? null : AircraftData.TaxiFuel);//ChangeDataType
                aircraft.VerticalSpeed = AircraftData.VerticalSpeed;
                aircraft.WeightUnit = AircraftData.WeightUnit;

                //aircraft.AircraftType = AircraftData.AircraftType ?? string.Empty;
                aircraft.AircraftSerialNo = AircraftData.AircraftSerialNumber ?? string.Empty;
                aircraft.AircraftYear = AircraftData.AircraftYear;
                aircraft.HobbsTime = TimeSpan.Parse(AircraftData.HobbsTimeOffset).TotalMinutes;
                aircraft.HobbsTimeOffset = TimeSpan.Parse(AircraftData.HobbsTimeOffset).TotalMinutes;
                aircraft.TachTime = TimeSpan.Parse(AircraftData.TachTimeOffset).TotalMinutes;
                aircraft.TachTimeOffset = TimeSpan.Parse(AircraftData.TachTimeOffset).TotalMinutes;
                aircraft.EngineMFGType = AircraftData.EngineMFGType;
                aircraft.EngineTBO = AircraftData.EngineTBO;
                aircraft.Engine1LastMOHDate = Misc.GetDate(AircraftData.Engine1LastMOHDate);

                aircraft.CurrentEngineTime = TimeSpan.Parse(AircraftData.CurrentEngineTime1).TotalMinutes;
                aircraft.PropMFG = AircraftData.PropMFG;

                aircraft.Prop1TBO = AircraftData.Prop1TBO;
                aircraft.Prop1OHDueDate = Misc.GetDate(AircraftData.Prop1OHDueDate);

                aircraft.Prop1Time = TimeSpan.Parse(AircraftData.CurrentPropTime1).TotalMinutes;
                aircraft.Engine2LastMOHDate = Misc.GetDate(AircraftData.Engine2LastMOHDate);

                aircraft.Engine2Time = TimeSpan.Parse(AircraftData.CurrentEngineTime2).TotalMinutes; //== null)

                aircraft.Prop2OHDueDate = Misc.GetDate(AircraftData.Prop2OHDueDate);
                aircraft.Prop2Time = TimeSpan.Parse(AircraftData.CurrentPropTime2).TotalMinutes;


                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets the first aircraft from aircraft list
        /// </summary>
        /// <param name="profileId"> User profile Id</param>
        /// <returns>aircraft id</returns>
        public int getAircraftIdAircraftProfilePage(int profileId)
        {
            if (profileId == default(int))
                return default(int);

            using (var context = new GuardianAvionicsEntities())
            {
                //var aircraftIdList =
                //    context.AircraftProfiles.Where(a => a.ProfileId == profileId && !a.Deleted).Select(a => a.Id).ToList();
                //Remove ProfileId From Table
                List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                aircraftMappedWithManufacturerUserList = new UnitDataHelper().GetAircraftListForManufacturerUser(profileId);


                var aircraftIdList =
                    context.AircraftProfiles.Where(a => aircraftMappedWithManufacturerUserList.Contains(a.Id) || context.MappingAircraftAndPilots
                        .Where(map => map.ProfileId == profileId && map.Deleted == false)
                        .Select(s => s.AircraftId)
                        .ToList()
                        .Contains(a.Id) && !a.Deleted).Select(a => a.Id).ToList();

                return aircraftIdList.Count > 0 ? (aircraftIdList.FirstOrDefault()) : 0;
            }
        }

        public List<AircraftSummary> GetAircraftList(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<AircraftSummary>();


            string userType = context.Profiles.FirstOrDefault(f => f.Id == profileId).UserMaster.UserType;
            var aList = new List<AircraftProfile>();
            if (userType == "Maintenance")
            {
                var aircraftIdList = context.MappingMaintenanceUserAndAircrafts.Where(a => a.ProfileId == profileId).Select(s => s.AircraftId).Distinct().ToList();
                response = context.AircraftProfiles.Where(ap => aircraftIdList.Contains(ap.Id)).Select(a => new AircraftSummary
                {
                    aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + a.ImageUrl),
                    //aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.ImageServerPathKey + @"/" + a.ImageUrl),
                    aircraftNNumber = a.Registration ?? string.Empty,
                    aircraftType = a.Make == null ? "N/A" : a.AircraftManufacturer.Name,
                    AircraftId = a.Id
                    //totalTachTime = (a.TachTime == null ? 0 : (double)a.TachTime) + (a.TachTimeOffset == null ? 0 : (double)a.TachTimeOffset),
                }).ToList();
            }
            else if (userType == "Admin")
            {
                response = context.AircraftProfiles.Select(a => new AircraftSummary
                {
                    aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + a.ImageUrl),
                    aircraftNNumber = a.Registration ?? string.Empty,
                    AircraftId = a.Id,
                    UnitSerialNo = a.AeroUnitNo
                }).ToList();
            }
            else
            {
                var aircraftIdMapWithPilot = context.MappingAircraftAndPilots.Where(map => map.ProfileId == profileId && map.Deleted == false)
                                                                            .Select(s => s.AircraftId)
                                                                            .ToList();
                var deletedAircraftList = context.MappingAircraftAndPilots.Where(w => w.ProfileId == profileId && w.Deleted).Select(s => s.AircraftId).ToList();
                var aircraftIdLogs = context.PilotLogs.Where(w => w.PilotId == profileId || w.CoPilotId == profileId).Select(s => s.AircraftId).Distinct().ToList();
                response = context.AircraftProfiles
                                    .Where(ap => (ap.OwnerProfileId == profileId || aircraftIdMapWithPilot.Contains(ap.Id) || aircraftIdLogs.Contains(ap.Id)) && !deletedAircraftList.Contains(ap.Id))
                                                        .Select(a => new AircraftSummary
                                                        {
                                                            aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + a.ImageUrl),
                                                            //aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.ImageServerPathKey + @"/" + a.ImageUrl),
                                                            aircraftNNumber = a.Registration ?? string.Empty,
                                                            aircraftType = a.Make == null ? "N/A" : a.AircraftManufacturer.Name,
                                                            AircraftId = a.Id,
                                                            //TId = "1",
                                                            OwnerProfileId = a.OwnerProfileId
                                                            //totalTachTime = (a.TachTime == null ? 0 : (double)a.TachTime) + (a.TachTimeOffset == null ? 0 : (double)a.TachTimeOffset),
                                                        }).ToList();
            }

            if (response == null)
            {
                return new List<AircraftSummary>();
            }
            return response;
        }

        /// <summary>
        /// Converts Vertical speed unit form one unit to another
        /// </summary>
        /// <param name="VerticalSpeedUnitName">Changed Unit</param>
        /// <param name="AircraftData">Aircraft Data</param>
        /// <returns>Updated Aircraft Data</returns>
        public AircraftProfileModelWeb ChangeVerticalSpeedUnit(string VerticalSpeedUnitName,
            AircraftProfileModelWeb AircraftData)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                if (VerticalSpeedUnitName == Enumerations.VerticalSpeedUnit.FTMIN.GetStringValue())
                {
                    if (AircraftData.VerticalSpeed != Enumerations.VerticalSpeedUnit.FTMIN.GetStringValue())
                    {
                        // converted all the values to FTMIN

                        if (AircraftData.RateOfClimb != default(double))
                            AircraftData.RateOfClimb = Math.Round(Constants.MsToFtmin * AircraftData.RateOfClimb,
                                Constants.RoundOffDigit);

                        if (AircraftData.RateOfDescent != default(double))
                            AircraftData.RateOfDescent = Math.Round(Constants.MsToFtmin * AircraftData.RateOfDescent,
                                Constants.RoundOffDigit);

                        AircraftData.VerticalSpeed = Enumerations.VerticalSpeedUnit.FTMIN.GetStringValue();

                        SaveAircraftProfileAfterUnitChange(AircraftData);

                        AircraftData = SetIdForAllUnits(AircraftData);
                    }
                }
                else if (VerticalSpeedUnitName == Enumerations.VerticalSpeedUnit.MS.GetStringValue())
                {
                    if (AircraftData.VerticalSpeed != Enumerations.VerticalSpeedUnit.MS.GetStringValue())
                    {
                        // converted all the values to MS

                        if (AircraftData.RateOfClimb != default(double))
                            AircraftData.RateOfClimb = Math.Round(Constants.FtminToMs * AircraftData.RateOfClimb,
                                Constants.RoundOffDigit);

                        if (AircraftData.RateOfDescent != default(double))
                            AircraftData.RateOfDescent = Math.Round(Constants.FtminToMs * AircraftData.RateOfDescent,
                                Constants.RoundOffDigit);

                        AircraftData.VerticalSpeed = Enumerations.VerticalSpeedUnit.MS.GetStringValue();

                        SaveAircraftProfileAfterUnitChange(AircraftData);

                        AircraftData = SetIdForAllUnits(AircraftData);
                    }
                }
            }
            return AircraftData;
        }

        /// <summary>
        /// Converts Speed unit form one unit to another
        /// </summary>
        /// <param name="SpeedUnitName">Changed Uni</param>
        /// <param name="AircraftData">Aircraft Data</param>
        /// <returns>Updated Aircraft Data</returns>
        public AircraftProfileModelWeb ChangeSpeedUnit(string SpeedUnitName, AircraftProfileModelWeb AircraftData)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                if (SpeedUnitName == Enumerations.SpeedUnit.KMH.GetStringValue())
                {
                    if (AircraftData.SpeedUnit != Enumerations.SpeedUnit.KMH.GetStringValue())
                    {
                        if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.KTS.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.KtsToKmh * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.KtsToKmh * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.KtsToKmh * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }
                        else if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.MPH.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.MphToKmh * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.MphToKmh * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.MphToKmh * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }

                        AircraftData.SpeedUnit = Enumerations.SpeedUnit.KMH.GetStringValue();

                        SaveAircraftProfileAfterUnitChange(AircraftData);
                        AircraftData = SetIdForAllUnits(AircraftData);
                    }
                }
                else if (SpeedUnitName == Enumerations.SpeedUnit.KTS.GetStringValue())
                {
                    if (AircraftData.SpeedUnit != Enumerations.SpeedUnit.KTS.GetStringValue())
                    {
                        if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.KMH.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.KmhToKts * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.KmhToKts * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.KmhToKts * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }
                        else if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.MPH.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.MphToKts * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.MphToKts * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.MphToKts * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }

                        AircraftData.SpeedUnit = Enumerations.SpeedUnit.KTS.GetStringValue();

                        SaveAircraftProfileAfterUnitChange(AircraftData);
                        AircraftData = SetIdForAllUnits(AircraftData);
                    }
                }
                else if (SpeedUnitName == Enumerations.SpeedUnit.MPH.GetStringValue())
                {
                    if (AircraftData.SpeedUnit != Enumerations.SpeedUnit.MPH.GetStringValue())
                    {
                        if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.KMH.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.KmhToMph * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.KmhToMph * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.KmhToMph * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }
                        else if (AircraftData.SpeedUnit == Enumerations.SpeedUnit.KTS.GetStringValue())
                        {
                            if (AircraftData.TASClimb != default(double))
                                AircraftData.TASClimb = Math.Round(Constants.KtsToMph * AircraftData.TASClimb,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASCruise != default(double))
                                AircraftData.TASCruise = Math.Round(Constants.KtsToMph * AircraftData.TASCruise,
                                    Constants.RoundOffDigit);

                            if (AircraftData.TASDescent != default(double))
                                AircraftData.TASDescent = Math.Round(Constants.KtsToMph * AircraftData.TASDescent,
                                    Constants.RoundOffDigit);
                        }

                        AircraftData.SpeedUnit = Enumerations.SpeedUnit.MPH.GetStringValue();

                        SaveAircraftProfileAfterUnitChange(AircraftData);
                        AircraftData = SetIdForAllUnits(AircraftData);
                    }
                }
            } // using


            return AircraftData;
        }

        /// <summary>
        /// Saves Aircraft image on server
        /// </summary>
        /// <param name="obj">Aircraft Data</param>
        /// <returns>Image Name</returns>
        public string SaveAircraftImage(AircraftProfileModelWeb obj)
        {
            if (obj != null && obj.AircraftImage != null && obj.AircraftId != default(int))
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var objAircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == obj.AircraftId);
                    if (objAircraft != default(AircraftProfile))
                    {
                        // generate path for saving image
                        string path = ConfigurationReader.ImagePathKey;
                        if (!string.IsNullOrEmpty(path))
                        {
                            string fileName = GenerateUniqueImageName();
                            string fullPath = path + "/" + fileName;

                            // convert httpPostedFileBase to bytes array
                            new Misc().SaveFileToS3Bucket(fileName, obj.AircraftImage.InputStream, "Image");
                            try
                            {
                                objAircraft.ImageUrl = fileName;
                                context.SaveChanges();
                                return Boolean.TrueString;
                            }
                            catch (Exception e)
                            {
                                return e.Message;
                            }
                        }
                    }
                }
            }

            return Boolean.FalseString;
        } // SaveAircraftImage

        /// <summary>
        /// Fetch Aircraft Image from database and returns path of image as on server
        /// </summary>
        /// <param name="aircraftId">Id for aircraft</param>
        /// <returns>Image source</returns>
        public dynamic setAircraftImageSource(int aircraftId)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                var objAircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == aircraftId);

                if (objAircraft != default(AircraftProfile))
                {
                    var fileName = objAircraft.ImageUrl;
                    string path = ConfigurationReader.ImageServerPathKey;

                    if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                        return (path + "/" + fileName);
                }
            }

            return Constants.AircraftDefaultImage;
        }

        /// <summary>
        /// Sets text for showing on Aircraft profile page , in aircraft listing.
        /// </summary>
        /// <param name="profileId">Profile Id</param>
        /// <returns>text for aircraft listing</returns>
        public List<AircraftDetail> setViewBagAircraftListing(int profileId)
        {
            var detailList = new List<AircraftDetail>();

            if (profileId > 0)
                using (var context = new GuardianAvionicsEntities())
                {
                    Misc objMisc = new Misc();
                    string userType = context.Profiles.FirstOrDefault(f => f.Id == profileId).UserMaster.UserType;
                    var aList = new List<AircraftProfile>();
                    if (userType == "Maintenance")
                    {
                        var aircraftIdList = context.MappingMaintenanceUserAndAircrafts.Where(a => a.ProfileId == profileId).Select(s => s.AircraftId).Distinct().ToList();
                        detailList = context.AircraftProfiles.Where(ap => aircraftIdList.Contains(ap.Id)).Select(s => new AircraftDetail
                        {
                            Id = s.Id,
                            Text = s.Registration,


                        }).ToList();
                    }
                    else
                    {
                        List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                        aircraftMappedWithManufacturerUserList = new UnitDataHelper().GetAircraftListForManufacturerUser(profileId);


                        detailList = context.AircraftProfiles.Where(ap => (aircraftMappedWithManufacturerUserList.Contains(ap.Id) || ap.OwnerProfileId == profileId || context.MappingAircraftAndPilots
                          .Where(map => map.ProfileId == profileId && !map.Deleted)
                          .Select(s => s.AircraftId)
                          .ToList()
                          .Contains(ap.Id)) && !ap.Deleted && !context.MappingAircraftAndPilots.Where(m => m.ProfileId == profileId && m.Deleted).Select(s => s.AircraftId).ToList().Contains(ap.Id)).Select(s => new AircraftDetail
                          {
                              Id = s.Id,
                              Text = s.Registration
                          }).ToList(); ;
                    }

                } // using  

            return detailList;
        }

        /// <summary>
        /// Sets Default values for aircraft
        /// </summary>
        /// <returns>Aircraft profile with default values</returns>
        public AircraftProfileModelWeb GetEmptyAircraftDetail(int profileId)
        {
            var model = new AircraftProfileModelWeb
            {
                IsSingleEngine = true,
                AdditionalFuel = "5",
                Capacity = "20",
                TaxiFuel = "5",
                Power = 65,
                RPM = 2400,
                MP = 24,
                Altitude = 2500,
                FuelBurnCruise = 34.00,
                TASCruise = 115,
                RateOfClimb = 650,
                FuelBurnRateClimb = 60,
                TASClimb = 73,
                RateOfDescent = 500,
                FuelBurnRateOfDescent = 5,
                TASDescent = 125,
                AircraftId = 0,

                CurrentHobbsTime = "0.0",
                TotalHobbsTime = "0.0",
                HobbsTimeOffset = "0.0",

                CurrentTachTime = "0.0",
                TachTimeOffset = "0.0",
                TotalTechTime = "0.0",


                CurrentEngineTime1 = "0.0",
                EngineTimeOffset1 = "0.0",
                TotalEngineTime1 = "0.0",

                CurrentEngineTime2 = "0.0",
                EngineTimeOffset2 = "0.0",
                TotalEngineTime2 = "0.0",

                CurrentPropTime1 = "0.0",
                PropTimeOffset1 = "0.0",
                TotalPropTime1 = "0.0",

                CurrentPropTime2 = "0.0",
                PropTimeOffset2 = "0.0",
                TotalPropTime2 = "0.0",


            };

            if (profileId != default(int))
            {
                // get the vaules of default units from user preference
                using (var context = new GuardianAvionicsEntities())
                {
                    var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);
                    if (preference != null)
                    {
                        // preference can not be null
                        model.FuelUnit = preference.FuelUnit;
                        model.WeightUnit = preference.WeightUnit;
                        model.SpeedUnit = preference.SpeedUnit;
                        model.VerticalSpeed = preference.VerticalSpeedUnit;
                    }
                    else
                    {
                        model.FuelUnit = Enumerations.FuelUnit.LIT.ToString();
                        model.WeightUnit = Enumerations.WeightUnit.LBS.ToString();
                        model.SpeedUnit = Enumerations.SpeedUnit.KTS.ToString();
                        model.VerticalSpeed = Enumerations.VerticalSpeedUnit.FTMIN.ToString();
                    }
                }
            }
            else
            {
                model.FuelUnit = Enumerations.FuelUnit.LIT.ToString();
                model.WeightUnit = Enumerations.WeightUnit.LBS.ToString();
                model.SpeedUnit = Enumerations.SpeedUnit.KTS.ToString();
                model.VerticalSpeed = Enumerations.VerticalSpeedUnit.FTMIN.ToString();
            }
            model = SetSelectListForAllUnits(model);
            model = SetIdForAllUnits(model);
            model.IsAmericanAircraft = true;
            model.Engine1LastMOHDate = string.Empty;
            model.Prop1OHDueDate = string.Empty;
            model.Engine2LastMOHDate = string.Empty;
            model.Prop2OHDueDate = string.Empty;
            model.Comm = true;
            model.Transponder = true;
            model.EngineMonitor = true;
            model.OtherAircraftManufacturer = string.Empty;
            model.OtheAircraftrModel = string.Empty;
            //*123
            var li = new List<ListItems> { new ListItems { text = "Select", value = 0 } };
            model.AircraftModel = li;
            model.Make = 0;
            model.Model = 0;
            model.aircraftSalesDetailList = new List<AircraftSalesDetailWeb>();
            model.iPassengerMediaDetails = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };

            model.Maintenance = new MaintenanceModelWeb();
            model.Maintenance.MaintenanceUserList = new List<MaintenanceUserModel>();
            model.Maintenance.SquawkListModel = new SquawkListModelWeb
            {
                IsOwner = false,
                IssueList = new List<IssueModel>(),
                UserList = new List<ListItems>(),
                aircraftList = new List<ListItems>(),
                userIdForIssue = 0
            };
            model.Maintenance.SquawkListModel.aircraftList.Add(new ListItems() { text = "Select", value = 0 });
            model.Maintenance.SquawkListModel.UserList.Add(new ListItems() { text = "Select", value = 0 });

            var dbContext = new GuardianAvionicsEntities();
            model.ComponentModelList = dbContext.AircraftComponentModels.Where(cm => !cm.Deleted).ToList();

            model.AircraftComponentManufacturerList = model.ComponentModelList.GroupBy(cm => new
            {
                AircraftComponentManufacturerId = cm.AircraftComponentManufacturer.Id,
                AircraftComponentManufacturerName = cm.AircraftComponentManufacturer.Name,
                AircraftComponentId = cm.AircraftComponent.Id,
                AircraftComponentName = cm.AircraftComponent.ComponentName
            }).Select(g => new AircraftComponentManufacturerModel
            {
                AircraftComponentManufacturerId = g.Key.AircraftComponentManufacturerId,
                AircraftComponentManufacturerName = g.Key.AircraftComponentManufacturerName,
                AircraftComponentId = g.Key.AircraftComponentId,
                AircraftComponentName = g.Key.AircraftComponentName
            }).ToList();

            model.AircraftComponentMakeAndModels = (from c in dbContext.AircraftComponents
                                                    join cm in dbContext.AircraftComponentMakeAndModels on new { AircraftComponentId = c.Id, AircraftId = 0 } equals new { AircraftComponentId = (int)cm.AircraftComponentId, AircraftId = cm.AircraftId } into cm_join
                                                    from cm in cm_join.DefaultIfEmpty()
                                                    where c.ComponentName != "Transponder"
                                                    select new AircraftComponentMakeAndModelWeb
                                                    {
                                                        Id = cm != null ? cm.Id : 0,
                                                        AircraftId = 0,
                                                        AircraftComponentId = c.Id,
                                                        AircraftComponentName = c.ComponentName,
                                                        ComponentManufacturerId = cm != null ? cm.ComponentManufacturerId : null,
                                                        ComponentManufacturerName = cm != null && cm.ComponentManufacturerId != null ? cm.AircraftComponentManufacturer.Name : string.Empty,
                                                        ComponentModelId = cm != null ? cm.ComponentModelId : null,
                                                        ComponentModelName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.Name : string.Empty,
                                                        ComponentModelFileName = cm != null && cm.ComponentModelId != null ? cm.AircraftComponentModel.UserMannual : string.Empty,
                                                        IsShowInFlightBag = false,
                                                        ModelFreetext = cm.ComponentModelFreetext,
                                                        IsVisibleOnMFDScreen = cm.IsVisibleOnMFDScreen ?? false
                                                    }).ToList();
            return model;
        }

        /// <summary>
        /// Deletes aircraft from aircraft table
        /// </summary>
        /// <param name="aircraftId">Aircraft Id</param>
        /// <returns>Code for confirmation of deletion</returns>
        public int DeleteAircraft(int aircraftId, int profileId)
        {
            if (aircraftId < 1)
                return 0;
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    // add include here to count pilot logs in one querry
                    var aircraft = context.AircraftProfiles.FirstOrDefault(ap => ap.Id == aircraftId);
                    if (aircraft == default(AircraftProfile))
                        return 0;

                    var objMapping =
                        context.MappingAircraftAndPilots.FirstOrDefault(
                            a => a.AircraftId == aircraftId && a.ProfileId == profileId);
                    if (objMapping != null)
                    {
                        objMapping.Deleted = true;
                    }
                    aircraft.LastUpdated = DateTime.UtcNow;

                    context.SaveChanges();
                    return 1;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception AircraftId = " + aircraftId + " Profile Id = " + profileId, e);
                return 3;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public GeneralResponse CreateAircraft(AircraftProfileModelWeb aircraft, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            #region create
            //logger.Fatal("Create Aircraft Method Call ");


            var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AreoUnitNo);
            if (objAeroUnit == default(AeroUnitMaster))
                return new AircraftModifyNnumberResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitNumberNotExist).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.AeroUnitNumberNotExist.GetStringValue()
                };

            if (objAeroUnit.AircraftId != null)
                return new AircraftModifyNnumberResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft.GetStringValue()
                };


            // check if N-Number is unique or not

            if (context.AircraftProfiles.Any(ap => ap.Registration == aircraft.Registration))
            {
                return new AircraftModifyNnumberResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.NNumberIsNotUnique).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.NNumberIsNotUnique.GetStringValue(),
                };
            }

            var profile = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId);
            if (profile != null)
            {
                if (profile.CurrentSubscriptionId != null)
                {
                    var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(profile.Id);

                    int totalRegAircraft = context.AircraftProfiles.Count(c => c.OwnerProfileId == profile.Id);

                    if (!isCurrSubscriptionIsPlan5AndActive)
                    {
                        if (totalRegAircraft > 0)
                        {
                            return new AircraftModifyNnumberResponse
                            {
                                ResponseCode = ((int)Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription).ToString(),
                                ResponseMessage = Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription.GetStringValue()
                            };
                        }
                    }
                }
            }

            // create new aircraft
            var objaircraft = new AircraftProfile();

            // when aircraft is created then ImageOptions.DoNothing option will be invalid
            objaircraft.ImageUrl = string.Empty;
            if (aircraft.AircraftImage != null)
            {
                string path = ConfigurationReader.ImagePathKey;
                string fileName = GenerateUniqueImageName();
                string fullPath = path + "/" + fileName;

                // convert httpPostedFileBase to bytes array
                new Misc().SaveFileToS3Bucket(fileName, aircraft.AircraftImage.InputStream, "Image");
                objaircraft.ImageUrl = fileName;
            }


            //objaircraft.AdditionalFuel = aircraft.AdditionalFuel;
            objaircraft.LastUpdateDateEngineSetting = DateTime.UtcNow;
            objaircraft.LastUpdateDateIpassenger = DateTime.UtcNow;
            objaircraft.LastUpdateDatePerformance = DateTime.UtcNow;
            objaircraft.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType 
            objaircraft.Altitude = aircraft.Altitude;
            objaircraft.Capacity = aircraft.Capacity;
            objaircraft.Color = aircraft.Color;
            objaircraft.FuelBurnCruise = aircraft.FuelBurnCruise;
            objaircraft.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
            objaircraft.FuelBurnRateOfDescent = aircraft.FuelBurnRateOfDescent;
            objaircraft.FuelUnit = aircraft.FuelUnit;
            objaircraft.HomeBase = aircraft.HomeBase;
            objaircraft.LastUpdated = DateTime.UtcNow;
            objaircraft.Make = aircraft.Make;
            objaircraft.Model = aircraft.Model;
            objaircraft.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
            objaircraft.OtheAircraftrModel = aircraft.OtheAircraftrModel;

            objaircraft.MP = aircraft.MP;
            objaircraft.Power = aircraft.Power;
            // Remove ProfileId From Table 
            //objaircraft.ProfileId = aircraft.ProfileId;
            objaircraft.RateOfClimb = aircraft.RateOfClimb;
            objaircraft.RateOfDescent = aircraft.RateOfDescent;
            objaircraft.Registration = aircraft.Registration;
            objaircraft.RPM = aircraft.RPM;
            objaircraft.SpeedUnit = aircraft.SpeedUnit;
            objaircraft.TASClimb = aircraft.TASClimb;
            objaircraft.TASCruise = aircraft.TASCruise;
            objaircraft.TASDescent = aircraft.TASDescent;
            //objaircraft.TaxiFuel = aircraft.TaxiFuel;
            objaircraft.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType
            objaircraft.VerticalSpeed = aircraft.VerticalSpeed;
            objaircraft.WeightUnit = aircraft.WeightUnit;
            objaircraft.AeroUnitNo = aircraft.AreoUnitNo;
            objaircraft.EngineType = aircraft.IsSingleEngine
                           ? Enumerations.EngineType.SingleEngine.ToString()
                           : Enumerations.EngineType.MultipleEngine.ToString();
            //objaircraft.AircraftType = aircraft.AircraftType;
            objaircraft.AircraftSerialNo = aircraft.AircraftSerialNumber;
            objaircraft.AircraftYear = aircraft.AircraftYear;
            objaircraft.HobbsTime = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentHobbsTime);  //   TimeSpan.Parse(obj.CurrentHobbsTime).TotalMinutes ; // ?? default(double);
            objaircraft.HobbsTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.HobbsTimeOffset);  //  TimeSpan.Parse(obj.HobbsTimeOffset).TotalMinutes;
            objaircraft.TachTime = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentTachTime);  //  TimeSpan.Parse(obj.CurrentTachTime).TotalMinutes;
            objaircraft.TachTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.TachTimeOffset);  //  TimeSpan.Parse(obj.TachTimeOffset).TotalMinutes;
            objaircraft.EngineMFGType = aircraft.EngineMFGType;
            objaircraft.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;
            if (aircraft.EngineMFGType == 0 || aircraft.EngineMFGType == -1)
            {
                objaircraft.EngineMFGType = null;
            }
            if (aircraft.EngineMFGType != -1)
            {
                objaircraft.OtherEngineManufacturerType = "";
            }

            objaircraft.EngineTBO = aircraft.EngineTBO;
            objaircraft.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
            objaircraft.CurrentEngineTime = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentEngineTime1); // TimeSpan.Parse(obj.CurrentEngineTime1).TotalMinutes;
            objaircraft.EngineTimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.EngineTimeOffset1); // TimeSpan.Parse(obj.EngineTimeOffset1).TotalMinutes;
            objaircraft.ChargeBy = 1;
            objaircraft.PropMFG = aircraft.PropMFG;
            objaircraft.OtherPropManufacturer = aircraft.OtherPropManufactureType;
            if (aircraft.PropMFG == 0 || aircraft.PropMFG == -1)
            {
                objaircraft.PropMFG = null;
            }
            if (aircraft.PropMFG != -1)
            {
                objaircraft.OtherPropManufacturer = "";
            }
            objaircraft.Prop1TBO = aircraft.Prop1TBO;
            objaircraft.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);
            objaircraft.Prop1Time = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentPropTime1); // TimeSpan.Parse(obj.CurrentPropTime1).TotalMinutes;
            objaircraft.Prop1TimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.PropTimeOffset1); // TimeSpan.Parse(obj.PropTimeOffset1).TotalMinutes;
            objaircraft.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
            objaircraft.Engine2Time = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentEngineTime2); //TimeSpan.Parse(obj.CurrentEngineTime2).TotalMinutes;
            objaircraft.Engine2TimeOffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.EngineTimeOffset2);  //  TimeSpan.Parse(obj.EngineTimeOffset2).TotalMinutes;
            objaircraft.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
            objaircraft.Prop2Time = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.CurrentPropTime2);  // TimeSpan.Parse(obj.CurrentPropTime2).TotalMinutes;
            objaircraft.Prop2Timeoffset = Misc.ConvertOneTenthOfTimeToMinutes(aircraft.PropTimeOffset2);  // TimeSpan.Parse(obj.PropTimeOffset2).TotalMinutes;
            objaircraft.UniqueId = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));
            objaircraft.IsAmericaAircraft = (aircraft.IsAmericanAircraft != null) &&
                                            (bool)aircraft.IsAmericanAircraft;
            objaircraft.Comm = aircraft.Comm;
            objaircraft.Transponder = aircraft.Transponder;
            objaircraft.EngineMonitor = aircraft.EngineMonitor;
            objaircraft.LastTransactionId = "-1";
            objaircraft.OwnerProfileId =
                context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id;
            if (aircraft.AircraftComponentMakeAndModels != null)
            {
                foreach (var componentModel in aircraft.AircraftComponentMakeAndModels)
                {
                    var cm = context.AircraftComponentMakeAndModels.Create();
                    cm.AircraftComponentId = componentModel.AircraftComponentId;
                    cm.ComponentManufacturerId = componentModel.ComponentManufacturerId;
                    cm.ComponentModelId = componentModel.ComponentModelId;
                    cm.ComponentModelFreetext = componentModel.ModelFreetext;
                    cm.IsVisibleOnMFDScreen = componentModel.IsVisibleOnMFDScreen;
                    objaircraft.AircraftComponentMakeAndModels.Add(cm);

                    if (componentModel.IsShowInFlightBag)
                    {
                        var document = context.Documents.Create();
                        document.Url =
                                                    context.AircraftComponentModels.FirstOrDefault(
                                                        f => f.Id == componentModel.ComponentModelId).UserMannual;
                        document.LastUpdated = DateTime.UtcNow;
                        document.Deleted = false;
                        document.ProfileId = objaircraft.OwnerProfileId ?? 0;
                        document.IsForAll = false;
                        objaircraft.Documents.Add(document);

                    }
                }
            }
            context.AircraftProfiles.Add(objaircraft);
            context.SaveChanges();
            //if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
            //{
            //    var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.Create();
            //    mappingComponentManufacturerAndAircrafts.AircraftId = objaircraft.Id;
            //    mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
            //    context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
            //}
            var aircraftsalesdetail = context.SalesDetailMasters.Where(s => s.DetailType == "Rental" || s.DetailType == "Instruction").Select(a => new { Id = a.Id, Type = a.DetailType }).ToList();
            foreach (var obj in aircraftsalesdetail)
            {
                var salesdetail = context.AircraftSalesDetails.Create();

                salesdetail.AircraftId = objaircraft.Id;
                if (obj.Type == "Rental")
                {
                    salesdetail.GLAccountNumber = "4060.00";
                    salesdetail.Price = Convert.ToDecimal(67.50);
                }
                else
                {
                    salesdetail.GLAccountNumber = "4140.00";
                    salesdetail.Price = Convert.ToDecimal(35.00);
                }

                salesdetail.SalesDetailId = obj.Id;

                context.AircraftSalesDetails.Add(salesdetail);
            }
            var objAeroUnitMaster =
                context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AreoUnitNo && !a.Deleted);
            objAeroUnitMaster.AircraftId = objaircraft.Id;

            var mapAircraftAndOwner = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == objaircraft.Id && f.ProfileId == objaircraft.OwnerProfileId);
            if (mapAircraftAndOwner == null)
            {

                var objMappingowner = new MappingAircraftAndPilot()
                {
                    AircraftId = objaircraft.Id,
                    ProfileId = objaircraft.OwnerProfileId ?? 0,
                    Deleted = false,
                    CreateDate = DateTime.UtcNow,
                    IsActive = true
                };
                context.MappingAircraftAndPilots.Add(objMappingowner);
            }
            else
            {
                mapAircraftAndOwner.Deleted = false;
                mapAircraftAndOwner.IsActive = true;
            }
            // MappingAircraftAndPilot(objaircraft.Id, objaircraft.OwnerProfileId ?? 0);

            context.SaveChanges();

            //if (!string.IsNullOrEmpty(aircraft.EngineSettings))
            //{
            //    XmlDocument doc = new XmlDocument();
            //    try
            //    {
            //        doc.LoadXml(aircraft.EngineSettings);
            //    }
            //    catch (Exception ex)
            //    {
            //        //logger.Fatal("Error while saving the XML file (Create Aircraft)" + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
            //    }
            //    doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + objaircraft.Id.ToString() + ".xml");
            //}

            // send aircraft id with tag and image url in response.
            GeneralResponse response = new GeneralResponse();
            response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
            response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();

            System.Threading.Tasks.Task.Run(() =>
            {
                string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "aircraftId";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = objaircraft.Id;
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spCreateAircraftTable", param);

                string message = "<p>Date - " + DateTime.Now.ToString() + "</p><p>Congratulation, new aircraft has been registered form " + ConfigurationReader.ServerUrl + ".</p><table border=\"1\" style=\"width:50%\"> <tr><td>Registartion No.</td><td>" + aircraft.Registration + "</td></tr><tr><td>AeroUnitNo</td><td>" + aircraft.AreoUnitNo + "</td></tr><tr><td>Owner Email Id</td><td>" + aircraft.OwnerEmailId + "</td></tr></table>";
                Misc.SendEmailCommon("<p>Date - " + DateTime.Now.ToString() + "</p>" + message, "Aircraft Registration");

                string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(objaircraft.Id);
                string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                using (StreamWriter sw = File.CreateText(aircraftLogPath))
                {
                    sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                    sw.WriteLine("Congratulation, new aircraft has been registered");
                    sw.WriteLine("Registration No " + aircraft.Registration);
                    sw.WriteLine("AeroUnitNo - " + aircraft.AreoUnitNo);
                    sw.WriteLine("Owner Email Id - " + aircraft.OwnerEmailId);
                    sw.WriteLine("Creation Date - " + DateTime.UtcNow.ToShortDateString());
                    sw.WriteLine("=====================================================================================");
                    sw.WriteLine("");
                }
            });

            return response;

            #endregion create
        }

        /// <summary>
        /// Querries aircraft master file and reference file for given n number
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns>If aircraft is not found then null else aircraft object with fields prefilled from FAA's database</returns>
        public AircraftProfileModelWeb GetAircraftDetailsFromFaa(string NNumber)
        {
            var response = new AircraftProfileModelWeb();
            var context = new GuardianAvionicsEntities();
            try
            {
                // if N-Number is prefixed by n then remove it
                var startsWithN = NNumber.StartsWith("N") || NNumber.StartsWith("n");
                if (startsWithN)
                    NNumber = NNumber.Substring(1);

                var master = context.AircraftMasterFiles.Where(am => am.NNumber == NNumber).ToList();

                if (master.Count == 0)
                    return null;

                var masterObj = master.FirstOrDefault();

                if (!String.IsNullOrEmpty(masterObj.MFR_MDL_CODE))
                {
                    var refer = context.AircraftReferenceFiles.Where(ar => ar.MFR_MDL_CODE == masterObj.MFR_MDL_CODE).ToList();

                    if (refer.Count == 0)
                        return null;

                    var referObj = refer.FirstOrDefault();

                    //*123
                    var manufacturer =
                        context.AircraftManufacturers.FirstOrDefault(man => man.Name == referObj.ManufacturerName && !man.Deleted);
                    var madel = context.AircraftModelLists.FirstOrDefault(mod => mod.ModelName == referObj.ModelName && !mod.Deleted);

                    response.Make = manufacturer == default(AircraftManufacturer) ? 1 : manufacturer.Id;
                    response.Model = madel == default(AircraftModelList) ? 1 : madel.Id;
                    response.IsSingleEngine = referObj.NumberOfEngine == "1" ? true : false;
                    response.NumberOfEngine = Convert.ToInt16(referObj.NumberOfEngine ?? string.Empty);

                    // from master file
                    response.AircraftYear = Convert.ToInt32(masterObj.ManufactureYear);
                    response.AircraftSerialNumber = masterObj.SERIALNUMBER ?? string.Empty;
                    response.Registration = masterObj.NNumber;
                    return response;
                }
                return response;
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        /// <summary>
        /// Checks if there is any other aircraft created with this nNmber.
        /// </summary>
        /// <param name="NNumber"></param>
        /// <param name="profileId"></param>
        /// <param name="aircraftId"></param>
        /// <returns>If no other aircraft created with this nNumber then true else false</returns>
        public bool IsNNumberUniqueForUser(string NNumber, int profileId, int aircraftId)
        {
            try
            {
                if (!string.IsNullOrEmpty(NNumber) && profileId != default(int))
                    using (var context = new GuardianAvionicsEntities())
                    {
                        //return !context.AircraftProfiles.Any(ap => ap.ProfileId == profileId && !ap.Deleted && ap.Id != aircraftId && ap.Registration == NNumber);
                        // Remove ProfileId From Table
                        return
                            !context.AircraftProfiles.Any(
                                ap => !ap.Deleted && ap.Id != aircraftId && ap.Registration == NNumber);
                    }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Searches Aircraft details 
        /// </summary>
        /// <param name="aircraft"></param>
        /// <returns></returns>
        public AircraftSearchResponse SearchAircraft(AircraftSearchRequest aircraft)
        {
            //var context = new GuardianAvionicsEntities();
            var response = new AircraftSearchResponse();
            try
            {
                if (string.IsNullOrEmpty(aircraft.Registration)) // profile == default(Profile) || 
                    return new AircraftSearchResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftSearch.RegistrationNoCannotNullOrEmpty).ToString(),
                        ResponseMessage = Enumerations.AircraftSearch.RegistrationNoCannotNullOrEmpty.GetStringValue(),
                    };

                var aircraftProfile = GetAircraftDetailsFromFaa(aircraft.Registration);

                if (aircraftProfile == null)
                    return new AircraftSearchResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftSearch.NNumberDoNotExist).ToString(),
                        ResponseMessage = Enumerations.AircraftSearch.NNumberDoNotExist.GetStringValue(),
                    };

                response.AircraftSerialNumber = aircraftProfile.AircraftSerialNumber;
                response.AircraftYear = aircraftProfile.AircraftYear ?? 0;
                response.Make = aircraftProfile.Make;
                response.Model = aircraftProfile.Model;
                response.ManufacturerName = selectMakeNameById(aircraftProfile.Make);
                response.ModelName = selectMakeNameById(aircraftProfile.Model);
                response.NumbersOfEngine = aircraftProfile.NumberOfEngine;
                response.Registration = aircraftProfile.Registration;
                response.ResponseCode = ((int)Common.Enumerations.AircraftCodes.Success).ToString();
                response.ResponseMessage = Common.Enumerations.AircraftCodes.Success.GetStringValue();

                return response;
            }
            catch (Exception e)
            {
                return response.Exception();
            }
        }

        public AircraftModifyNnumberResponse ModifyAircraftNNumberTest(AircraftModifyNnumberRequest aircraft)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var response = new AircraftModifyNnumberResponse();
                        // validate aircraft profile list
                        var genRes = VaildateAircraftModifyNnumberRequest(aircraft, context);
                        if (genRes.ResponseMessage != Enumerations.AircraftCodes.Success.GetStringValue())
                            return new AircraftModifyNnumberResponse
                            {
                                ResponseCode = genRes.ResponseCode,
                                ResponseMessage = genRes.ResponseMessage
                            };
                        // if N-Number is prefixed by n then remove it
                        //var startsWithN = aircraft.Registration.StartsWith("N") || aircraft.Registration.StartsWith("n");
                        //if (startsWithN)
                        //    aircraft.Registration = aircraft.Registration.Substring(1);

                        var AircraftToUpdate =
                            context.AircraftProfiles.FirstOrDefault(ap => ap.UniqueId == aircraft.UniqueId);

                        if (AircraftToUpdate == default(AircraftProfile))
                        {
                            #region create
                            //logger.Fatal("Create Aircraft Method Call ");

                            if (!aircraft.IsRegisterWithoutUnit)
                            {
                                var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AeroUnitNo);
                                if (objAeroUnit == default(AeroUnitMaster))
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitNumberNotExist).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.AeroUnitNumberNotExist.GetStringValue()
                                    };

                                if (objAeroUnit.AircraftId != null)
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft.GetStringValue()
                                    };
                            }

                            // check if N-Number is unique or not

                            if (context.AircraftProfiles.Any(ap => ap.Registration == aircraft.Registration))
                            {
                                if (aircraft.IsRegisterWithoutUnit)
                                {
                                    var objAircraft = context.AircraftProfiles.FirstOrDefault(f => f.Registration == aircraft.Registration);

                                    if (objAircraft.OwnerProfileId == context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id)
                                    {
                                        response.ResponseCode = ((int)Enumerations.AircraftCodes.OwnerEmailIdIsAlreadyRegisteredWithTailNo).ToString();
                                        response.ResponseMessage = Enumerations.AircraftCodes.OwnerEmailIdIsAlreadyRegisteredWithTailNo.GetStringValue();
                                        return response;
                                    }

                                    response.AircraftId = objAircraft.Id;
                                    response.UniqueId = objAircraft.UniqueId ?? default(long);
                                    response.ImageName = "";
                                    response.OwnerName = (objAircraft.Profile == null) ? "" : ((objAircraft.Profile.UserDetail == null) ? "" : objAircraft.Profile.UserDetail.FirstName + " " + objAircraft.Profile.UserDetail.LastName);
                                    response.ProfileId = context.Profiles.FirstOrDefault(f => f.EmailId == aircraft.OwnerEmailId).Id;
                                    response.ResponseCode = ((int)Enumerations.AircraftCodes.TailNoAlreadyExistForRegisterAircraftWithoutUnit).ToString();
                                    response.ResponseMessage = Enumerations.AircraftCodes.TailNoAlreadyExistForRegisterAircraftWithoutUnit.GetStringValue();
                                    return response;
                                }
                                else
                                {
                                    return response.DuplicateNNumber();
                                }

                            }

                            var profile = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId);
                            if (profile != null)
                            {
                                if (profile.CurrentSubscriptionId != null)
                                {
                                    var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(profile.Id);

                                    int totalRegAircraft = context.AircraftProfiles.Count(c => c.OwnerProfileId == profile.Id);

                                    if (!isCurrSubscriptionIsPlan5AndActive)
                                    {
                                        if (totalRegAircraft > 0)
                                        {
                                            response.ResponseCode = ((int)Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription).ToString();
                                            response.ResponseMessage = Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription.GetStringValue();
                                        }
                                    }
                                }
                            }

                            // create new aircraft
                            var objaircraft = new AircraftProfile();

                            // when aircraft is created then ImageOptions.DoNothing option will be invalid
                            objaircraft.ImageUrl = objaircraft.ImageUrl = string.Empty;

                            if (aircraft.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                            {
                                // save aircraft image
                                if (!string.IsNullOrEmpty(aircraft.ImageName))
                                {
                                    // assuming image is base64 encoded image
                                    try
                                    {
                                        byte[] bytes = Convert.FromBase64String(aircraft.ImageName);
                                        string path = ConfigurationReader.ImagePathKey;
                                        string fileName = GenerateUniqueImageName();
                                        string fullPath = path + "/" + fileName;
                                        using (var imageFile = new FileStream(fullPath, FileMode.Create))
                                        {
                                            imageFile.Write(bytes, 0, bytes.Length);
                                            imageFile.Flush();
                                        }
                                        if (File.Exists(fullPath))
                                        {
                                            new Misc().UploadFile(fullPath, fileName, "Image");
                                            File.Delete(fullPath);
                                        }
                                        objaircraft.ImageUrl = fileName;
                                    }
                                    catch
                                    {
                                        objaircraft.ImageUrl = string.Empty;
                                    }
                                }
                                else
                                {
                                    objaircraft.ImageUrl = string.Empty;
                                }
                            }

                            //objaircraft.AdditionalFuel = aircraft.AdditionalFuel;
                            objaircraft.LastUpdateDateEngineSetting = DateTime.UtcNow;
                            objaircraft.LastUpdateDateIpassenger = DateTime.UtcNow;
                            objaircraft.LastUpdateDatePerformance = DateTime.UtcNow;
                            objaircraft.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType 
                            objaircraft.Altitude = aircraft.Altitude;
                            objaircraft.Capacity = aircraft.Capacity;
                            objaircraft.Color = aircraft.Color;
                            objaircraft.FuelBurnCruise = aircraft.FuelBurnCruise;
                            objaircraft.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
                            objaircraft.FuelBurnRateOfDescent = aircraft.FuelBurnRateOfDescent;
                            objaircraft.FuelUnit = aircraft.FuelUnit;
                            objaircraft.HomeBase = aircraft.HomeBase;
                            objaircraft.LastUpdated = DateTime.UtcNow;
                            objaircraft.Make = aircraft.Make;
                            objaircraft.Model = aircraft.Model;
                            objaircraft.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                            objaircraft.OtheAircraftrModel = aircraft.OtheAircraftrModel;

                            objaircraft.MP = aircraft.MP;
                            objaircraft.Power = aircraft.Power;
                            // Remove ProfileId From Table 
                            //objaircraft.ProfileId = aircraft.ProfileId;
                            objaircraft.RateOfClimb = aircraft.RateOfClimb;
                            objaircraft.RateOfDescent = aircraft.RateOfDescent;
                            objaircraft.Registration = aircraft.Registration;
                            objaircraft.RPM = aircraft.RPM;
                            objaircraft.SpeedUnit = aircraft.SpeedUnit;
                            objaircraft.TASClimb = aircraft.TASClimb;
                            objaircraft.TASCruise = aircraft.TASCruise;
                            objaircraft.TASDescent = aircraft.TASDescent;
                            //objaircraft.TaxiFuel = aircraft.TaxiFuel;
                            objaircraft.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType
                            objaircraft.VerticalSpeed = aircraft.VerticalSpeed;
                            objaircraft.WeightUnit = aircraft.WeightUnit;
                            objaircraft.AeroUnitNo = aircraft.IsRegisterWithoutUnit ? "" : aircraft.AeroUnitNo;
                            objaircraft.EngineType = GetEngineTypeFromCodes(aircraft.EngineType.ToString());
                            objaircraft.AircraftType = aircraft.AircraftType;
                            objaircraft.AircraftSerialNo = aircraft.AircraftSerialNumber;
                            objaircraft.AircraftYear = aircraft.AircraftYear;
                            objaircraft.HobbsTime = aircraft.CurrentHobbsTime;
                            objaircraft.HobbsTimeOffset = aircraft.HobbsTimeOffset;
                            objaircraft.TachTime = aircraft.CurrentTachTime;
                            objaircraft.TachTimeOffset = aircraft.TachTimeOffset;
                            objaircraft.EngineMFGType = aircraft.EngineMFGType;
                            objaircraft.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;

                            objaircraft.EngineTBO = aircraft.EngineTBO;
                            objaircraft.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
                            objaircraft.CurrentEngineTime = aircraft.CurrentEngineTime1;
                            objaircraft.EngineTimeOffset = aircraft.EngineTimeOffset1;
                            objaircraft.ChargeBy = 1;
                            objaircraft.PropMFG = aircraft.PropMFG;
                            objaircraft.OtherPropManufacturer = aircraft.OtherPropManufactureType;

                            objaircraft.Prop1TBO = aircraft.Prop1TBO;
                            objaircraft.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);
                            objaircraft.Prop1Time = aircraft.CurrentPropTime1;
                            objaircraft.Prop1TimeOffset = aircraft.PropTimeOffset1;
                            objaircraft.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
                            objaircraft.Engine2Time = aircraft.CurrentEngineTime2;
                            objaircraft.Engine2TimeOffset = aircraft.EngineTimeOffset2;
                            objaircraft.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
                            objaircraft.Prop2Time = aircraft.CurrentPropTime2;
                            objaircraft.Prop2Timeoffset = aircraft.PropTimeOffset2;
                            objaircraft.UniqueId = aircraft.UniqueId;
                            objaircraft.IsAmericaAircraft = (aircraft.IsAmericanAircraft != null) &&
                                                            (bool)aircraft.IsAmericanAircraft;
                            objaircraft.Comm = aircraft.Comm;
                            objaircraft.Transponder = aircraft.Transponder;
                            objaircraft.EngineMonitor = aircraft.EngineMonitor;
                            objaircraft.LastTransactionId = "-1";
                            objaircraft.OwnerProfileId =
                                context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id;
                            if (aircraft.AircraftComponentMakeAndModels != null)
                            {
                                foreach (var componentModel in aircraft.AircraftComponentMakeAndModels)
                                {
                                    var cm = context.AircraftComponentMakeAndModels.Create();
                                    cm.AircraftComponentId = componentModel.aircraftComponentId;
                                    cm.ComponentManufacturerId = componentModel.componentManufacturerId;
                                    cm.ComponentModelId = componentModel.componentModelId;
                                    cm.ComponentModelFreetext = componentModel.modelSerialNo;
                                    cm.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    objaircraft.AircraftComponentMakeAndModels.Add(cm);

                                    if (componentModel.isShowInFlightBag)
                                    {
                                        var document = context.Documents.Create();
                                        document.Url =
                                            context.AircraftComponentModels.FirstOrDefault(
                                                f => f.Id == componentModel.componentModelId).UserMannual;
                                        document.LastUpdated = DateTime.UtcNow;
                                        document.Deleted = false;
                                        document.ProfileId = objaircraft.OwnerProfileId ?? 0;
                                        document.IsForAll = false;
                                        objaircraft.Documents.Add(document);

                                    }
                                }
                            }
                            context.AircraftProfiles.Add(objaircraft);
                            context.SaveChanges();
                            if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
                            {
                                var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.Create();
                                mappingComponentManufacturerAndAircrafts.AircraftId = objaircraft.Id;
                                mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
                                context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
                            }
                            var aircraftsalesdetail = context.SalesDetailMasters.Where(s => s.DetailType == "Rental" || s.DetailType == "Instruction").Select(a => new { Id = a.Id, Type = a.DetailType }).ToList();
                            foreach (var obj in aircraftsalesdetail)
                            {
                                var salesdetail = context.AircraftSalesDetails.Create();

                                salesdetail.AircraftId = objaircraft.Id;
                                if (obj.Type == "Rental")
                                {
                                    salesdetail.GLAccountNumber = "4060.00";
                                    salesdetail.Price = Convert.ToDecimal(67.50);
                                }
                                else
                                {
                                    salesdetail.GLAccountNumber = "4140.00";
                                    salesdetail.Price = Convert.ToDecimal(35.00);
                                }

                                salesdetail.SalesDetailId = obj.Id;

                                context.AircraftSalesDetails.Add(salesdetail);
                            }

                            if (!aircraft.IsRegisterWithoutUnit)
                            {

                                var objAeroUnitMaster =
                                    context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AeroUnitNo && !a.Deleted);
                                objAeroUnitMaster.AircraftId = objaircraft.Id;

                            }
                            var mapAircraftAndOwner = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == objaircraft.Id && f.ProfileId == objaircraft.OwnerProfileId);
                            if (mapAircraftAndOwner == null)
                            {

                                var objMappingowner = new MappingAircraftAndPilot()
                                {
                                    AircraftId = objaircraft.Id,
                                    ProfileId = objaircraft.OwnerProfileId ?? 0,
                                    Deleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    IsActive = true
                                };
                                context.MappingAircraftAndPilots.Add(objMappingowner);
                            }
                            else
                            {
                                mapAircraftAndOwner.Deleted = false;
                                mapAircraftAndOwner.IsActive = true;
                            }
                            // MappingAircraftAndPilot(objaircraft.Id, objaircraft.OwnerProfileId ?? 0);


                            if (aircraft.IsPilot && aircraft.ProfileId != objaircraft.OwnerProfileId)
                            {
                                var mapAircraftAndPilot = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == objaircraft.Id && f.ProfileId == aircraft.ProfileId);
                                if (mapAircraftAndPilot == null)
                                {
                                    //Now Insert into PilotAircraftMapping Table
                                    var objMapping = new MappingAircraftAndPilot()
                                    {
                                        AircraftId = objaircraft.Id,
                                        ProfileId = aircraft.ProfileId,
                                        Deleted = false,
                                        CreateDate = DateTime.UtcNow
                                    };
                                    context.MappingAircraftAndPilots.Add(objMapping);
                                }
                                else
                                {
                                    mapAircraftAndPilot.Deleted = false;
                                    mapAircraftAndPilot.IsActive = true;
                                }
                                // MappingAircraftAndPilot(objaircraft.Id, aircraft.ProfileId);
                            }
                            //logger.Fatal("Create Aircraft Method Call FFF");
                            context.SaveChanges();

                            transaction.Commit();
                            //logger.Fatal("Create Aircraft Method Call GGG");

                            if (!string.IsNullOrEmpty(aircraft.EngineSettings))
                            {
                                XmlDocument doc = new XmlDocument();
                                try
                                {
                                    doc.LoadXml(aircraft.EngineSettings);
                                }
                                catch (Exception ex)
                                {
                                    //logger.Fatal("Error while saving the XML file (Create Aircraft)" + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                                }
                                doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + objaircraft.Id.ToString() + ".xml");
                            }

                            // send aircraft id with tag and image url in response.
                            response.UniqueId = objaircraft.UniqueId ?? default(long);
                            response.AircraftId = objaircraft.Id;
                            response.ImageName = objaircraft.ImageUrl;
                            response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                            response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();

                            string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
                            SqlParameter[] param = new SqlParameter[1];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "aircraftId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = objaircraft.Id;
                            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spCreateAircraftTable", param);

                            string message = "<p>Date - " + DateTime.Now.ToString() + "</p><p>Congratulation, new aircraft has been registered form " + ConfigurationReader.ServerUrl + ".</p><table border=\"1\" style=\"width:50%\"> <tr><td>Registartion No.</td><td>" + aircraft.Registration + "</td></tr><tr><td>AeroUnitNo</td><td>" + aircraft.AeroUnitNo + "</td></tr><tr><td>Owner Email Id</td><td>" + aircraft.OwnerEmailId + "</td></tr></table>";
                            Misc.SendEmailCommon("<p>Date - " + DateTime.Now.ToString() + "</p>" + message, "Aircraft Registration");

                            string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(objaircraft.Id);
                            string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                            using (StreamWriter sw = File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Congratulation, new aircraft has been registered");
                                sw.WriteLine("Registration No " + aircraft.Registration);
                                sw.WriteLine("AeroUnitNo - " + aircraft.AeroUnitNo);
                                sw.WriteLine("Owner Email Id - " + aircraft.OwnerEmailId);
                                sw.WriteLine("Creation Date - " + DateTime.UtcNow.ToShortDateString());
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }

                            return response;

                            #endregion create
                        } // create aircraft 
                        else
                        {
                            //logger.Fatal("Update Aircraft");
                            #region update

                            // update aircraft
                            if (aircraft.IsUpdateAvailableForGeneral)
                            {

                                int newOwnerProfileId = 0;

                                if (string.IsNullOrEmpty(aircraft.OwnerEmailId))
                                {
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.EmailIdCannotNullOrEmpty).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.EmailIdCannotNullOrEmpty.GetStringValue()
                                    };
                                }

                                if (AircraftToUpdate.Profile.EmailId != aircraft.OwnerEmailId)
                                {
                                    var chkProfileExist = context.Profiles.FirstOrDefault(p => p.EmailId.ToLower() == aircraft.OwnerEmailId.ToLower());
                                    if (chkProfileExist == null)
                                    {
                                        return new AircraftModifyNnumberResponse
                                        {
                                            ResponseCode = ((int)Enumerations.AircraftCodes.EmailIdDoNotExist).ToString(),
                                            ResponseMessage = Enumerations.AircraftCodes.EmailIdDoNotExist.GetStringValue()
                                        };
                                    }
                                    newOwnerProfileId = chkProfileExist.Id;
                                }

                                if (aircraft.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                                {
                                    // save aircraft image
                                    if (!string.IsNullOrEmpty(aircraft.ImageName))
                                    {
                                        // assuming image is base64 encoded image
                                        try
                                        {
                                            byte[] bytes = Convert.FromBase64String(aircraft.ImageName);
                                            string path = ConfigurationReader.ImagePathKey;
                                            string fileName = GenerateUniqueImageName();
                                            string fullPath = path + "/" + fileName;
                                            using (var imageFile = new FileStream(fullPath, FileMode.Create))
                                            {
                                                imageFile.Write(bytes, 0, bytes.Length);
                                                imageFile.Flush();
                                            }
                                            if (File.Exists(fullPath))
                                            {
                                                new Misc().UploadFile(fullPath, fileName, "Image");
                                            }
                                            AircraftToUpdate.ImageUrl = fileName;
                                        }
                                        catch
                                        {
                                            AircraftToUpdate.ImageUrl = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        AircraftToUpdate.ImageUrl = string.Empty;
                                    }
                                }
                                else if (aircraft.ImageOption == Enumerations.ImageOptions.DeleteImage.GetStringValue())
                                {
                                    AircraftToUpdate.ImageUrl = string.Empty;
                                }
                                AircraftToUpdate.LastUpdated = DateTime.UtcNow;
                                AircraftToUpdate.OwnerProfileId = newOwnerProfileId;
                                //AircraftToUpdate.Registration = aircraft.Registration;
                                AircraftToUpdate.Capacity = aircraft.Capacity;
                                //AircraftToUpdate.TaxiFuel = aircraft.TaxiFuel;
                                AircraftToUpdate.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType
                                                                                                                                                   //AircraftToUpdate.AdditionalFuel = aircraft.AdditionalFuel;
                                AircraftToUpdate.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType
                                AircraftToUpdate.FuelUnit = aircraft.FuelUnit;
                                AircraftToUpdate.WeightUnit = aircraft.WeightUnit;
                                AircraftToUpdate.SpeedUnit = aircraft.SpeedUnit;
                                AircraftToUpdate.VerticalSpeed = aircraft.VerticalSpeed;




                                AircraftToUpdate.IsAmericaAircraft = (aircraft.IsAmericanAircraft != null) &&
                                                                     (bool)aircraft.IsAmericanAircraft;
                                AircraftToUpdate.LastUpdated = DateTime.UtcNow;

                                AircraftToUpdate.Make = aircraft.Make;
                                AircraftToUpdate.Model = aircraft.Model;
                                AircraftToUpdate.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                                AircraftToUpdate.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                                AircraftToUpdate.Color = aircraft.Color;
                                AircraftToUpdate.HomeBase = aircraft.HomeBase;
                                AircraftToUpdate.EngineType = GetEngineTypeFromCodes((aircraft.EngineType).ToString());
                                AircraftToUpdate.AircraftType = aircraft.AircraftType;
                                //AircraftToUpdate.AircraftSerialNo = aircraft.AircraftSerialNumber;
                                AircraftToUpdate.AircraftYear = aircraft.AircraftYear;
                                AircraftToUpdate.HobbsTime = aircraft.CurrentHobbsTime;
                                AircraftToUpdate.HobbsTimeOffset = aircraft.HobbsTimeOffset;
                                AircraftToUpdate.TachTime = aircraft.CurrentTachTime;
                                AircraftToUpdate.TachTimeOffset = aircraft.TachTimeOffset;
                                AircraftToUpdate.EngineMFGType = aircraft.EngineMFGType;
                                AircraftToUpdate.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;

                                AircraftToUpdate.EngineTBO = aircraft.EngineTBO;
                                AircraftToUpdate.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
                                AircraftToUpdate.CurrentEngineTime = aircraft.CurrentEngineTime1;
                                AircraftToUpdate.EngineTimeOffset = aircraft.EngineTimeOffset1;

                                AircraftToUpdate.PropMFG = aircraft.PropMFG;
                                AircraftToUpdate.OtherPropManufacturer = aircraft.OtherPropManufactureType;

                                AircraftToUpdate.Prop1TBO = aircraft.Prop1TBO;
                                AircraftToUpdate.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);
                                AircraftToUpdate.Prop1Time = aircraft.CurrentPropTime1;
                                AircraftToUpdate.Prop1TimeOffset = aircraft.PropTimeOffset1;
                                AircraftToUpdate.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
                                AircraftToUpdate.Engine2Time = aircraft.CurrentEngineTime2;
                                AircraftToUpdate.Engine2TimeOffset = aircraft.EngineTimeOffset2;
                                AircraftToUpdate.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
                                AircraftToUpdate.Prop2Time = aircraft.CurrentPropTime2;
                                AircraftToUpdate.Prop2Timeoffset = aircraft.PropTimeOffset2;
                                AircraftToUpdate.Comm = aircraft.Comm;
                                AircraftToUpdate.Transponder = aircraft.Transponder;
                                AircraftToUpdate.EngineMonitor = aircraft.EngineMonitor;
                                AircraftToUpdate.OwnerProfileId = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id;
                                //AircraftToUpdate.ChargeBy = aircraft.ChargeBy;


                                var temp = aircraft.AircraftComponentMakeAndModels.Select(s => s.aircraftComponentId).ToList();
                                var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(cm => cm.AircraftId == aircraft.AircraftId && temp.Contains(cm.AircraftComponentId)).ToList();
                                if (aircraftComponentMakeAndModels.Count > 0)
                                {
                                    aircraftComponentMakeAndModels.ForEach(delegate (AircraftComponentMakeAndModel item)
                                    {

                                        var componentModel =
                                            aircraft.AircraftComponentMakeAndModels.FirstOrDefault(
                                                cm => cm.aircraftComponentId == item.AircraftComponentId);
                                        Document oldDocument = null;
                                        if (item.AircraftComponentModel != null)
                                        {
                                            oldDocument =
                                                context.Documents.FirstOrDefault(
                                                    d => d.Url == item.AircraftComponentModel.UserMannual);
                                        }
                                        //var oldDocument = context.Documents.FirstOrDefault(d => d.Url == item.AircraftComponentModel.UserMannual);
                                        if (componentModel != null && componentModel.isShowInFlightBag &&
                                    (componentModel.componentModelId != item.ComponentModelId || oldDocument == null))
                                        {
                                            if (oldDocument != null)
                                            {
                                                context.Documents.Remove(oldDocument);
                                            }
                                            var usermannual =
                                                context.AircraftComponentModels.FirstOrDefault(
                                                    f => f.Id == componentModel.componentModelId);
                                            var document = context.Documents.Create();
                                            document.Url = usermannual == null ? "" : usermannual.UserMannual;
                                            document.ProfileId = Convert.ToInt32(AircraftToUpdate.OwnerProfileId);
                                            document.IsForAll = false;
                                            document.Deleted = false;
                                            document.LastUpdated = DateTime.UtcNow;
                                            document.AircraftId = aircraft.AircraftId;
                                            context.Documents.Add(document);
                                        }
                                        else
                                        {
                                            if (!componentModel.isShowInFlightBag && oldDocument != null)
                                            {
                                                context.Documents.Remove(oldDocument);
                                            }
                                        }
                                        item.ComponentManufacturerId = componentModel.componentManufacturerId;
                                        item.ComponentModelId = componentModel.componentModelId;
                                        item.ComponentModelFreetext = componentModel.modelSerialNo;
                                        item.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    });
                                }

                                foreach (var componentModel in aircraft.AircraftComponentMakeAndModels.Where(a => !aircraftComponentMakeAndModels.Select(s => s.AircraftComponentId).ToList().Contains(a.aircraftComponentId)).ToList())
                                {
                                    var cm = context.AircraftComponentMakeAndModels.Create();
                                    cm.AircraftComponentId = componentModel.aircraftComponentId;
                                    cm.ComponentManufacturerId = componentModel.componentManufacturerId;
                                    cm.ComponentModelId = componentModel.componentModelId;
                                    cm.AircraftId = aircraft.AircraftId;
                                    cm.ComponentModelFreetext = componentModel.modelSerialNo;
                                    cm.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    AircraftToUpdate.AircraftComponentMakeAndModels.Add(cm);

                                    if (componentModel.isShowInFlightBag)
                                    {
                                        var document = context.Documents.Create();
                                        document.Url =
                                            context.AircraftComponentModels.FirstOrDefault(
                                                f => f.Id == componentModel.componentModelId).UserMannual;
                                        document.LastUpdated = DateTime.UtcNow;
                                        document.Deleted = false;
                                        document.ProfileId = AircraftToUpdate.OwnerProfileId ?? 0;
                                        document.IsForAll = false;
                                        document.AircraftId = aircraft.AircraftId;
                                        AircraftToUpdate.Documents.Add(document);

                                    }
                                }

                                var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.AircraftId);
                                if (mappingComponentManufacturerAndAircrafts != null)
                                {
                                    mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft ?? "";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
                                    {
                                        var obj = context.MappingComponentManufacturerAndAircrafts.Create();
                                        obj.AircraftId = aircraft.AircraftId;
                                        obj.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
                                        context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
                                    }
                                }
                            }

                            if (aircraft.IsUpdateAvailableForPerformance)
                            {
                                AircraftToUpdate.LastUpdateDatePerformance = DateTime.UtcNow;
                                AircraftToUpdate.Power = (aircraft.Power == default(float)) ? 0 : aircraft.Power;
                                AircraftToUpdate.RPM = (aircraft.RPM == default(double)) ? 0 : aircraft.RPM;
                                AircraftToUpdate.MP = (aircraft.MP == default(double)) ? 0 : aircraft.MP;
                                AircraftToUpdate.Altitude = (aircraft.Altitude == default(double))
                                    ? 0
                                    : aircraft.Altitude;

                                AircraftToUpdate.FuelBurnCruise = (aircraft.FuelBurnCruise == default(double))
                                    ? 0
                                    : aircraft.FuelBurnCruise;
                                AircraftToUpdate.TASCruise = (aircraft.TASCruise == default(double))
                                    ? 0
                                    : aircraft.TASCruise;
                                AircraftToUpdate.RateOfClimb = (aircraft.RateOfClimb == default(double))
                                    ? 0
                                    : aircraft.RateOfClimb;
                                AircraftToUpdate.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
                                AircraftToUpdate.TASClimb = (aircraft.TASClimb == default(double))
                                    ? 0
                                    : aircraft.TASClimb;
                                AircraftToUpdate.RateOfDescent = (aircraft.RateOfDescent == default(double))
                                    ? 0
                                    : aircraft.RateOfDescent;
                                AircraftToUpdate.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == default(double))
                                    ? 0
                                    : aircraft.FuelBurnRateOfDescent;
                                AircraftToUpdate.TASDescent = (aircraft.TASDescent == default(double))
                                    ? 0
                                    : aircraft.TASDescent;
                            }

                            context.SaveChanges();
                            transaction.Commit();

                            if (aircraft.IsUpdateAvailableForEngine)
                            {
                                if (!string.IsNullOrEmpty(aircraft.EngineSettings))
                                {
                                    AircraftToUpdate.LastUpdateDateEngineSetting = DateTime.UtcNow;
                                    if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml"))
                                    {
                                        File.Delete(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml");
                                    }
                                    XmlDocument doc = new XmlDocument();
                                    try
                                    {
                                        doc.LoadXml(aircraft.EngineSettings);
                                    }
                                    catch (Exception ex)
                                    {
                                        //logger.Fatal("Error while saving the XML file (Update Aircraft) " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                                    }
                                    doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId.ToString() + ".xml");
                                }
                            }
                            //updateCategoryItem(aircraft.CategoryList);
                            // this will send all the fields back to calling end.    in case of image updation.
                            ////    rAircraft = AircraftProfileResponseModel.Create(AircraftToUpdate);
                            response.AircraftId = AircraftToUpdate.Id;
                            response.UniqueId = aircraft.UniqueId;
                            response.ImageName = AircraftToUpdate.ImageUrl;
                            response.ProfileId = aircraft.ProfileId;
                            response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                            response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                            return response;

                            #endregion update
                        } // update aircraft completed

                        // send updated aircraft in response

                    }
                    catch (Exception e)
                    {
                        string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(aircraft.AircraftId);
                        string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                        if (File.Exists(aircraftLogPath))
                        {
                            using (StreamWriter sw = File.AppendText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Create Update aircraft (API - CreateUpdateAircraftNNumber)");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        else
                        {
                            using (StreamWriter sw = File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Create Update aircraft (API - CreateUpdateAircraftNNumber)");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }



                        var errorLog = context.ErrorLogs.Create();
                        errorLog.ModuleName = "Web-API";
                        errorLog.ErrorSource = "API - CreateUpdateAircraftNNumber, MethodName - ModifyAircraftNNumber()";
                        errorLog.ErrorDate = DateTime.UtcNow;
                        errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(e);
                        errorLog.ErrorDate = DateTime.UtcNow;
                        errorLog.LastUpdateDate = DateTime.UtcNow;
                        errorLog.ResolvedStatus = false;
                        errorLog.RequestParameters = Newtonsoft.Json.JsonConvert.SerializeObject(aircraft);
                        context.ErrorLogs.Add(errorLog);
                        context.SaveChanges();
                        return new AircraftModifyNnumberResponse().Exception();
                    }
                }
            } // using
        }

        public AircraftModifyNnumberResponse ModifyAircraftNNumber(AircraftModifyNnumberRequest aircraft)
        {
            using (var context = new GuardianAvionicsEntities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var response = new AircraftModifyNnumberResponse();
                        // validate aircraft profile list
                        var genRes = VaildateAircraftModifyNnumberRequest(aircraft, context);
                        if (genRes.ResponseMessage != Enumerations.AircraftCodes.Success.GetStringValue())
                            return new AircraftModifyNnumberResponse
                            {
                                ResponseCode = genRes.ResponseCode,
                                ResponseMessage = genRes.ResponseMessage
                            };
                        // if N-Number is prefixed by n then remove it
                        //var startsWithN = aircraft.Registration.StartsWith("N") || aircraft.Registration.StartsWith("n");
                        //if (startsWithN)
                        //    aircraft.Registration = aircraft.Registration.Substring(1);

                        var AircraftToUpdate =
                            context.AircraftProfiles.FirstOrDefault(ap => ap.UniqueId == aircraft.UniqueId);

                        if (AircraftToUpdate == default(AircraftProfile))
                        {
                            #region create
                            //logger.Fatal("Create Aircraft Method Call ");

                            if (!aircraft.IsRegisterWithoutUnit)
                            {
                                var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AeroUnitNo);
                                if (objAeroUnit == default(AeroUnitMaster))
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitNumberNotExist).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.AeroUnitNumberNotExist.GetStringValue()
                                    };

                                if (objAeroUnit.AircraftId != null)
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.AeroUnitAlreadyLinkWithAnotherAircraft.GetStringValue()
                                    };
                            }

                            // check if N-Number is unique or not

                            if (context.AircraftProfiles.Any(ap => ap.Registration == aircraft.Registration))
                            {
                                if (aircraft.IsRegisterWithoutUnit)
                                {
                                    var objAircraft = context.AircraftProfiles.FirstOrDefault(f => f.Registration == aircraft.Registration);

                                    if (objAircraft.OwnerProfileId == context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id)
                                    {
                                        response.ResponseCode = ((int)Enumerations.AircraftCodes.OwnerEmailIdIsAlreadyRegisteredWithTailNo).ToString();
                                        response.ResponseMessage = Enumerations.AircraftCodes.OwnerEmailIdIsAlreadyRegisteredWithTailNo.GetStringValue();
                                        return response;
                                    }

                                    response.AircraftId = objAircraft.Id;
                                    response.UniqueId = objAircraft.UniqueId ?? default(long);
                                    response.ImageName = "";
                                    response.OwnerName = (objAircraft.Profile == null) ? "" : ((objAircraft.Profile.UserDetail == null) ? "" : objAircraft.Profile.UserDetail.FirstName + " " + objAircraft.Profile.UserDetail.LastName);
                                    response.ProfileId = context.Profiles.FirstOrDefault(f => f.EmailId == aircraft.OwnerEmailId).Id;
                                    response.ResponseCode = ((int)Enumerations.AircraftCodes.TailNoAlreadyExistForRegisterAircraftWithoutUnit).ToString();
                                    response.ResponseMessage = Enumerations.AircraftCodes.TailNoAlreadyExistForRegisterAircraftWithoutUnit.GetStringValue();
                                    return response;
                                }
                                else
                                {
                                    return response.DuplicateNNumber();
                                }

                            }

                            var profile = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId);
                            if (profile != null)
                            {
                                if (profile.CurrentSubscriptionId != null)
                                {
                                    var isCurrSubscriptionIsPlan5AndActive = new SubscriptionHelper().IsCurrSubscriptionIsPlan5AndActive(profile.Id);

                                    int totalRegAircraft = context.AircraftProfiles.Count(c => c.OwnerProfileId == profile.Id);

                                    if (!isCurrSubscriptionIsPlan5AndActive)
                                    {
                                        if (totalRegAircraft > 0)
                                        {
                                            response.ResponseCode = ((int)Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription).ToString();
                                            response.ResponseMessage = Enumerations.AircraftCodes.CannotCreateAircraftForCurrentSubscription.GetStringValue();
                                        }
                                    }
                                }
                            }

                            // create new aircraft
                            var objaircraft = new AircraftProfile();

                            // when aircraft is created then ImageOptions.DoNothing option will be invalid
                            objaircraft.ImageUrl = objaircraft.ImageUrl = string.Empty;

                            if (aircraft.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                            {
                                // save aircraft image
                                if (!string.IsNullOrEmpty(aircraft.ImageName))
                                {
                                    // assuming image is base64 encoded image
                                    try
                                    {
                                        byte[] bytes = Convert.FromBase64String(aircraft.ImageName);
                                        string path = ConfigurationReader.ImagePathKey;
                                        string fileName = GenerateUniqueImageName();
                                        string fullPath = path + "/" + fileName;
                                        using (var imageFile = new FileStream(fullPath, FileMode.Create))
                                        {
                                            imageFile.Write(bytes, 0, bytes.Length);
                                            imageFile.Flush();
                                        }
                                        if (File.Exists(fullPath))
                                        {
                                            new Misc().UploadFile(fullPath, fileName, "Image");
                                            File.Delete(fullPath);
                                        }
                                        objaircraft.ImageUrl = fileName;
                                    }
                                    catch
                                    {
                                        objaircraft.ImageUrl = string.Empty;
                                    }
                                }
                                else
                                {
                                    objaircraft.ImageUrl = string.Empty;
                                }
                            }

                            //objaircraft.AdditionalFuel = aircraft.AdditionalFuel;
                            objaircraft.LastUpdateDateEngineSetting = DateTime.UtcNow;
                            objaircraft.LastUpdateDateIpassenger = DateTime.UtcNow;
                            objaircraft.LastUpdateDatePerformance = DateTime.UtcNow;
                            objaircraft.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType 
                            objaircraft.Altitude = aircraft.Altitude;
                            objaircraft.Capacity = aircraft.Capacity;
                            objaircraft.Color = aircraft.Color;
                            objaircraft.FuelBurnCruise = aircraft.FuelBurnCruise;
                            objaircraft.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
                            objaircraft.FuelBurnRateOfDescent = aircraft.FuelBurnRateOfDescent;
                            objaircraft.FuelUnit = aircraft.FuelUnit;
                            objaircraft.HomeBase = aircraft.HomeBase;
                            objaircraft.LastUpdated = DateTime.UtcNow;
                            objaircraft.Make = aircraft.Make;
                            objaircraft.Model = aircraft.Model;
                            objaircraft.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                            objaircraft.OtheAircraftrModel = aircraft.OtheAircraftrModel;

                            objaircraft.MP = aircraft.MP;
                            objaircraft.Power = aircraft.Power;
                            // Remove ProfileId From Table 
                            //objaircraft.ProfileId = aircraft.ProfileId;
                            objaircraft.RateOfClimb = aircraft.RateOfClimb;
                            objaircraft.RateOfDescent = aircraft.RateOfDescent;
                            objaircraft.Registration = aircraft.Registration;
                            objaircraft.RPM = aircraft.RPM;
                            objaircraft.SpeedUnit = aircraft.SpeedUnit;
                            objaircraft.TASClimb = aircraft.TASClimb;
                            objaircraft.TASCruise = aircraft.TASCruise;
                            objaircraft.TASDescent = aircraft.TASDescent;
                            //objaircraft.TaxiFuel = aircraft.TaxiFuel;
                            objaircraft.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType
                            objaircraft.VerticalSpeed = aircraft.VerticalSpeed;
                            objaircraft.WeightUnit = aircraft.WeightUnit;
                            objaircraft.AeroUnitNo = aircraft.IsRegisterWithoutUnit ? "" : aircraft.AeroUnitNo;
                            objaircraft.EngineType = GetEngineTypeFromCodes(aircraft.EngineType.ToString());
                            objaircraft.AircraftType = aircraft.AircraftType;
                            objaircraft.AircraftSerialNo = aircraft.AircraftSerialNumber;
                            objaircraft.AircraftYear = aircraft.AircraftYear;
                            objaircraft.HobbsTime = aircraft.CurrentHobbsTime;
                            objaircraft.HobbsTimeOffset = aircraft.HobbsTimeOffset;
                            objaircraft.TachTime = aircraft.CurrentTachTime;
                            objaircraft.TachTimeOffset = aircraft.TachTimeOffset;
                            objaircraft.EngineMFGType = aircraft.EngineMFGType;
                            objaircraft.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;

                            objaircraft.EngineTBO = aircraft.EngineTBO;
                            objaircraft.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
                            objaircraft.CurrentEngineTime = aircraft.CurrentEngineTime1;
                            objaircraft.EngineTimeOffset = aircraft.EngineTimeOffset1;
                            objaircraft.ChargeBy = 1;
                            objaircraft.PropMFG = aircraft.PropMFG;
                            objaircraft.OtherPropManufacturer = aircraft.OtherPropManufactureType;

                            objaircraft.Prop1TBO = aircraft.Prop1TBO;
                            objaircraft.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);
                            objaircraft.Prop1Time = aircraft.CurrentPropTime1;
                            objaircraft.Prop1TimeOffset = aircraft.PropTimeOffset1;
                            objaircraft.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
                            objaircraft.Engine2Time = aircraft.CurrentEngineTime2;
                            objaircraft.Engine2TimeOffset = aircraft.EngineTimeOffset2;
                            objaircraft.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
                            objaircraft.Prop2Time = aircraft.CurrentPropTime2;
                            objaircraft.Prop2Timeoffset = aircraft.PropTimeOffset2;
                            objaircraft.UniqueId = aircraft.UniqueId;
                            objaircraft.IsAmericaAircraft = (aircraft.IsAmericanAircraft != null) &&
                                                            (bool)aircraft.IsAmericanAircraft;
                            objaircraft.Comm = aircraft.Comm;
                            objaircraft.Transponder = aircraft.Transponder;
                            objaircraft.EngineMonitor = aircraft.EngineMonitor;
                            objaircraft.LastTransactionId = "-1";
                            objaircraft.OwnerProfileId =
                                context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id;
                            if (aircraft.AircraftComponentMakeAndModels != null)
                            {
                                foreach (var componentModel in aircraft.AircraftComponentMakeAndModels)
                                {
                                    var cm = context.AircraftComponentMakeAndModels.Create();
                                    cm.AircraftComponentId = componentModel.aircraftComponentId;
                                    cm.ComponentManufacturerId = componentModel.componentManufacturerId;
                                    cm.ComponentModelId = componentModel.componentModelId;
                                    cm.ComponentModelFreetext = componentModel.modelSerialNo;
                                    cm.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    objaircraft.AircraftComponentMakeAndModels.Add(cm);

                                    if (componentModel.isShowInFlightBag)
                                    {
                                        var document = context.Documents.Create();
                                        document.Url =
                                            context.AircraftComponentModels.FirstOrDefault(
                                                f => f.Id == componentModel.componentModelId).UserMannual;
                                        document.LastUpdated = DateTime.UtcNow;
                                        document.Deleted = false;
                                        document.ProfileId = objaircraft.OwnerProfileId ?? 0;
                                        document.IsForAll = false;
                                        objaircraft.Documents.Add(document);
                                    }
                                }
                            }
                            context.AircraftProfiles.Add(objaircraft);
                            context.SaveChanges();
                            if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
                            {
                                var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.Create();
                                mappingComponentManufacturerAndAircrafts.AircraftId = objaircraft.Id;
                                mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
                                context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
                            }

                            var aircraftsalesdetail = context.SalesDetailMasters.Where(s => s.DetailType == "Rental" || s.DetailType == "Instruction").Select(a => new { Id = a.Id, Type = a.DetailType }).ToList();
                            foreach (var obj in aircraftsalesdetail)
                            {
                                var salesdetail = context.AircraftSalesDetails.Create();

                                salesdetail.AircraftId = objaircraft.Id;
                                if (obj.Type == "Rental")
                                {
                                    salesdetail.GLAccountNumber = "4060.00";
                                    salesdetail.Price = Convert.ToDecimal(67.50);
                                }
                                else
                                {
                                    salesdetail.GLAccountNumber = "4140.00";
                                    salesdetail.Price = Convert.ToDecimal(35.00);
                                }

                                salesdetail.SalesDetailId = obj.Id;

                                context.AircraftSalesDetails.Add(salesdetail);
                            }

                            if (!aircraft.IsRegisterWithoutUnit)
                            {

                                var objAeroUnitMaster =
                                    context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aircraft.AeroUnitNo && !a.Deleted);
                                objAeroUnitMaster.AircraftId = objaircraft.Id;

                            }
                            var mapAircraftAndOwner = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == objaircraft.Id && f.ProfileId == objaircraft.OwnerProfileId);
                            if (mapAircraftAndOwner == null)
                            {

                                var objMappingowner = new MappingAircraftAndPilot()
                                {
                                    AircraftId = objaircraft.Id,
                                    ProfileId = objaircraft.OwnerProfileId ?? 0,
                                    Deleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    IsActive = true
                                };
                                context.MappingAircraftAndPilots.Add(objMappingowner);
                            }
                            else
                            {
                                mapAircraftAndOwner.Deleted = false;
                                mapAircraftAndOwner.IsActive = true;
                            }
                            // MappingAircraftAndPilot(objaircraft.Id, objaircraft.OwnerProfileId ?? 0);


                            if (aircraft.IsPilot && aircraft.ProfileId != objaircraft.OwnerProfileId)
                            {
                                var mapAircraftAndPilot = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == objaircraft.Id && f.ProfileId == aircraft.ProfileId);
                                if (mapAircraftAndPilot == null)
                                {
                                    //Now Insert into PilotAircraftMapping Table
                                    var objMapping = new MappingAircraftAndPilot()
                                    {
                                        AircraftId = objaircraft.Id,
                                        ProfileId = aircraft.ProfileId,
                                        Deleted = false,
                                        CreateDate = DateTime.UtcNow
                                    };
                                    context.MappingAircraftAndPilots.Add(objMapping);
                                }
                                else
                                {
                                    mapAircraftAndPilot.Deleted = false;
                                    mapAircraftAndPilot.IsActive = true;
                                }
                                // MappingAircraftAndPilot(objaircraft.Id, aircraft.ProfileId);
                            }
                            //logger.Fatal("Create Aircraft Method Call FFF");
                            context.SaveChanges();

                            transaction.Commit();
                            //logger.Fatal("Create Aircraft Method Call GGG");

                            if (!string.IsNullOrEmpty(aircraft.EngineSettings))
                            {
                                XmlDocument doc = new XmlDocument();
                                try
                                {
                                    doc.LoadXml(aircraft.EngineSettings);
                                }
                                catch (Exception ex)
                                {
                                    //logger.Fatal("Error while saving the XML file (Create Aircraft)" + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                                }
                                doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + objaircraft.Id.ToString() + ".xml");
                            }

                            // send aircraft id with tag and image url in response.
                            response.UniqueId = objaircraft.UniqueId ?? default(long);
                            response.AircraftId = objaircraft.Id;
                            response.ImageName = objaircraft.ImageUrl;
                            response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                            response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();

                            string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
                            SqlParameter[] param = new SqlParameter[1];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "aircraftId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = objaircraft.Id;
                            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spCreateAircraftTable", param);
                            try
                            {
                                SqlParameter[] catparam = new SqlParameter[2];
                                catparam[0] = new SqlParameter();
                                catparam[0].ParameterName = "aircraftId";
                                catparam[0].SqlDbType = SqlDbType.Int;
                                catparam[0].Value = objaircraft.Id;
                                catparam[1] = new SqlParameter();
                                catparam[1].ParameterName = "createdBy";
                                catparam[1].SqlDbType = SqlDbType.Int;
                                catparam[1].Value = objaircraft.OwnerProfileId;
                                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "createCategoryWithAircraft", catparam);
                            }
                            catch (Exception ex)
                            {

                            }

                            string message = "<p>Date - " + DateTime.Now.ToString() + "</p><p>Congratulation, new aircraft has been registered form " + ConfigurationReader.ServerUrl + ".</p><table border=\"1\" style=\"width:50%\"> <tr><td>Registartion No.</td><td>" + aircraft.Registration + "</td></tr><tr><td>AeroUnitNo</td><td>" + aircraft.AeroUnitNo + "</td></tr><tr><td>Owner Email Id</td><td>" + aircraft.OwnerEmailId + "</td></tr></table>";
                            Misc.SendEmailCommon("<p>Date - " + DateTime.Now.ToString() + "</p>" + message, "Aircraft Registration");

                            string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(objaircraft.Id);
                            string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                            using (StreamWriter sw = File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Congratulation, new aircraft has been registered");
                                sw.WriteLine("Registration No " + aircraft.Registration);
                                sw.WriteLine("AeroUnitNo - " + aircraft.AeroUnitNo);
                                sw.WriteLine("Owner Email Id - " + aircraft.OwnerEmailId);
                                sw.WriteLine("Creation Date - " + DateTime.UtcNow.ToShortDateString());
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }

                            return response;

                            #endregion create
                        } // create aircraft 
                        else
                        {
                            //logger.Fatal("Update Aircraft");
                            #region update

                            // update aircraft
                            if (aircraft.IsUpdateAvailableForGeneral)
                            {

                                int newOwnerProfileId = 0;

                                if (string.IsNullOrEmpty(aircraft.OwnerEmailId))
                                {
                                    return new AircraftModifyNnumberResponse
                                    {
                                        ResponseCode = ((int)Enumerations.AircraftCodes.EmailIdCannotNullOrEmpty).ToString(),
                                        ResponseMessage = Enumerations.AircraftCodes.EmailIdCannotNullOrEmpty.GetStringValue()
                                    };
                                }

                                if (AircraftToUpdate.Profile.EmailId != aircraft.OwnerEmailId)
                                {
                                    var chkProfileExist = context.Profiles.FirstOrDefault(p => p.EmailId.ToLower() == aircraft.OwnerEmailId.ToLower());
                                    if (chkProfileExist == null)
                                    {
                                        return new AircraftModifyNnumberResponse
                                        {
                                            ResponseCode = ((int)Enumerations.AircraftCodes.EmailIdDoNotExist).ToString(),
                                            ResponseMessage = Enumerations.AircraftCodes.EmailIdDoNotExist.GetStringValue()
                                        };
                                    }
                                    newOwnerProfileId = chkProfileExist.Id;
                                }

                                if (aircraft.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                                {
                                    // save aircraft image
                                    if (!string.IsNullOrEmpty(aircraft.ImageName))
                                    {
                                        // assuming image is base64 encoded image
                                        try
                                        {
                                            byte[] bytes = Convert.FromBase64String(aircraft.ImageName);
                                            string path = ConfigurationReader.ImagePathKey;
                                            string fileName = GenerateUniqueImageName();
                                            string fullPath = path + "/" + fileName;
                                            using (var imageFile = new FileStream(fullPath, FileMode.Create))
                                            {
                                                imageFile.Write(bytes, 0, bytes.Length);
                                                imageFile.Flush();
                                            }
                                            if (File.Exists(fullPath))
                                            {
                                                new Misc().UploadFile(fullPath, fileName, "Image");
                                            }
                                            AircraftToUpdate.ImageUrl = fileName;
                                        }
                                        catch
                                        {
                                            AircraftToUpdate.ImageUrl = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        AircraftToUpdate.ImageUrl = string.Empty;
                                    }
                                }
                                else if (aircraft.ImageOption == Enumerations.ImageOptions.DeleteImage.GetStringValue())
                                {
                                    AircraftToUpdate.ImageUrl = string.Empty;
                                }
                                AircraftToUpdate.LastUpdated = DateTime.UtcNow;
                                AircraftToUpdate.OwnerProfileId = newOwnerProfileId;
                                //AircraftToUpdate.Registration = aircraft.Registration;
                                AircraftToUpdate.Capacity = aircraft.Capacity;
                                //AircraftToUpdate.TaxiFuel = aircraft.TaxiFuel;
                                AircraftToUpdate.TaxiFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.TaxiFuel) ? null : aircraft.TaxiFuel); //ChangeDataType
                                                                                                                                                   //AircraftToUpdate.AdditionalFuel = aircraft.AdditionalFuel;
                                AircraftToUpdate.AdditionalFuel = Convert.ToDecimal(string.IsNullOrEmpty(aircraft.AdditionalFuel) ? null : aircraft.AdditionalFuel); //ChangeDataType
                                AircraftToUpdate.FuelUnit = aircraft.FuelUnit;
                                AircraftToUpdate.WeightUnit = aircraft.WeightUnit;
                                AircraftToUpdate.SpeedUnit = aircraft.SpeedUnit;
                                AircraftToUpdate.VerticalSpeed = aircraft.VerticalSpeed;




                                AircraftToUpdate.IsAmericaAircraft = (aircraft.IsAmericanAircraft != null) &&
                                                                     (bool)aircraft.IsAmericanAircraft;
                                AircraftToUpdate.LastUpdated = DateTime.UtcNow;

                                AircraftToUpdate.Make = aircraft.Make;
                                AircraftToUpdate.Model = aircraft.Model;
                                AircraftToUpdate.OtherAircraftManufacturer = aircraft.OtherAircraftManufacturer;
                                AircraftToUpdate.OtheAircraftrModel = aircraft.OtheAircraftrModel;
                                AircraftToUpdate.Color = aircraft.Color;
                                AircraftToUpdate.HomeBase = aircraft.HomeBase;
                                AircraftToUpdate.EngineType = GetEngineTypeFromCodes((aircraft.EngineType).ToString());
                                AircraftToUpdate.AircraftType = aircraft.AircraftType;
                                //AircraftToUpdate.AircraftSerialNo = aircraft.AircraftSerialNumber;
                                AircraftToUpdate.AircraftYear = aircraft.AircraftYear;
                                AircraftToUpdate.HobbsTime = aircraft.CurrentHobbsTime;
                                AircraftToUpdate.HobbsTimeOffset = aircraft.HobbsTimeOffset;
                                AircraftToUpdate.TachTime = aircraft.CurrentTachTime;
                                AircraftToUpdate.TachTimeOffset = aircraft.TachTimeOffset;
                                AircraftToUpdate.EngineMFGType = aircraft.EngineMFGType;
                                AircraftToUpdate.OtherEngineManufacturerType = aircraft.OtherEngineManufactureType;

                                AircraftToUpdate.EngineTBO = aircraft.EngineTBO;
                                AircraftToUpdate.Engine1LastMOHDate = Misc.GetDate(aircraft.Engine1LastMOHDate);
                                AircraftToUpdate.CurrentEngineTime = aircraft.CurrentEngineTime1;
                                AircraftToUpdate.EngineTimeOffset = aircraft.EngineTimeOffset1;

                                AircraftToUpdate.PropMFG = aircraft.PropMFG;
                                AircraftToUpdate.OtherPropManufacturer = aircraft.OtherPropManufactureType;

                                AircraftToUpdate.Prop1TBO = aircraft.Prop1TBO;
                                AircraftToUpdate.Prop1OHDueDate = Misc.GetDate(aircraft.Prop1OHDueDate);
                                AircraftToUpdate.Prop1Time = aircraft.CurrentPropTime1;
                                AircraftToUpdate.Prop1TimeOffset = aircraft.PropTimeOffset1;
                                AircraftToUpdate.Engine2LastMOHDate = Misc.GetDate(aircraft.Engine2LastMOHDate);
                                AircraftToUpdate.Engine2Time = aircraft.CurrentEngineTime2;
                                AircraftToUpdate.Engine2TimeOffset = aircraft.EngineTimeOffset2;
                                AircraftToUpdate.Prop2OHDueDate = Misc.GetDate(aircraft.Prop2OHDueDate);
                                AircraftToUpdate.Prop2Time = aircraft.CurrentPropTime2;
                                AircraftToUpdate.Prop2Timeoffset = aircraft.PropTimeOffset2;
                                AircraftToUpdate.Comm = aircraft.Comm;
                                AircraftToUpdate.Transponder = aircraft.Transponder;
                                AircraftToUpdate.EngineMonitor = aircraft.EngineMonitor;
                                AircraftToUpdate.OwnerProfileId = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId).Id;
                                //AircraftToUpdate.ChargeBy = aircraft.ChargeBy;


                                var temp = aircraft.AircraftComponentMakeAndModels.Select(s => s.aircraftComponentId).ToList();
                                var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(cm => cm.AircraftId == aircraft.AircraftId && temp.Contains(cm.AircraftComponentId)).ToList();
                                if (aircraftComponentMakeAndModels.Count > 0)
                                {
                                    aircraftComponentMakeAndModels.ForEach(delegate (AircraftComponentMakeAndModel item)
                                    {

                                        var componentModel =
                                            aircraft.AircraftComponentMakeAndModels.FirstOrDefault(
                                                cm => cm.aircraftComponentId == item.AircraftComponentId);
                                        Document oldDocument = null;
                                        if (item.AircraftComponentModel != null)
                                        {
                                            oldDocument =
                                                context.Documents.FirstOrDefault(
                                                    d => d.Url == item.AircraftComponentModel.UserMannual);
                                        }
                                        //var oldDocument = context.Documents.FirstOrDefault(d => d.Url == item.AircraftComponentModel.UserMannual);
                                        if (componentModel != null && componentModel.isShowInFlightBag &&
                                    (componentModel.componentModelId != item.ComponentModelId || oldDocument == null))
                                        {
                                            if (oldDocument != null)
                                            {
                                                context.Documents.Remove(oldDocument);
                                            }
                                            var usermannual =
                                                context.AircraftComponentModels.FirstOrDefault(
                                                    f => f.Id == componentModel.componentModelId);
                                            var document = context.Documents.Create();
                                            document.Url = usermannual == null ? "" : usermannual.UserMannual;
                                            document.ProfileId = Convert.ToInt32(AircraftToUpdate.OwnerProfileId);
                                            document.IsForAll = false;
                                            document.Deleted = false;
                                            document.LastUpdated = DateTime.UtcNow;
                                            document.AircraftId = aircraft.AircraftId;
                                            context.Documents.Add(document);
                                        }
                                        else
                                        {
                                            if (!componentModel.isShowInFlightBag && oldDocument != null)
                                            {
                                                context.Documents.Remove(oldDocument);
                                            }
                                        }
                                        item.ComponentManufacturerId = componentModel.componentManufacturerId;
                                        item.ComponentModelId = componentModel.componentModelId;
                                        item.ComponentModelFreetext = componentModel.modelSerialNo;
                                        item.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    });
                                }

                                foreach (var componentModel in aircraft.AircraftComponentMakeAndModels.Where(a => !aircraftComponentMakeAndModels.Select(s => s.AircraftComponentId).ToList().Contains(a.aircraftComponentId)).ToList())
                                {
                                    var cm = context.AircraftComponentMakeAndModels.Create();
                                    cm.AircraftComponentId = componentModel.aircraftComponentId;
                                    cm.ComponentManufacturerId = componentModel.componentManufacturerId;
                                    cm.ComponentModelId = componentModel.componentModelId;
                                    cm.AircraftId = aircraft.AircraftId;
                                    cm.ComponentModelFreetext = componentModel.modelSerialNo;
                                    cm.IsVisibleOnMFDScreen = componentModel.isVisibleOnMFDScreen;
                                    AircraftToUpdate.AircraftComponentMakeAndModels.Add(cm);

                                    if (componentModel.isShowInFlightBag)
                                    {
                                        var document = context.Documents.Create();
                                        document.Url =
                                            context.AircraftComponentModels.FirstOrDefault(
                                                f => f.Id == componentModel.componentModelId).UserMannual;
                                        document.LastUpdated = DateTime.UtcNow;
                                        document.Deleted = false;
                                        document.ProfileId = AircraftToUpdate.OwnerProfileId ?? 0;
                                        document.IsForAll = false;
                                        document.AircraftId = aircraft.AircraftId;
                                        AircraftToUpdate.Documents.Add(document);

                                    }
                                }

                                var mappingComponentManufacturerAndAircrafts = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraft.AircraftId);
                                if (mappingComponentManufacturerAndAircrafts != null)
                                {
                                    mappingComponentManufacturerAndAircrafts.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft ?? "";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(aircraft.ManufacturersMappedWithAircraft))
                                    {
                                        var obj = context.MappingComponentManufacturerAndAircrafts.Create();
                                        obj.AircraftId = aircraft.AircraftId;
                                        obj.ComponentManufacturers = aircraft.ManufacturersMappedWithAircraft;
                                        context.MappingComponentManufacturerAndAircrafts.Add(mappingComponentManufacturerAndAircrafts);
                                    }
                                }
                            }

                            if (aircraft.IsUpdateAvailableForPerformance)
                            {
                                AircraftToUpdate.LastUpdateDatePerformance = DateTime.UtcNow;
                                AircraftToUpdate.Power = (aircraft.Power == default(float)) ? 0 : aircraft.Power;
                                AircraftToUpdate.RPM = (aircraft.RPM == default(double)) ? 0 : aircraft.RPM;
                                AircraftToUpdate.MP = (aircraft.MP == default(double)) ? 0 : aircraft.MP;
                                AircraftToUpdate.Altitude = (aircraft.Altitude == default(double))
                                    ? 0
                                    : aircraft.Altitude;

                                AircraftToUpdate.FuelBurnCruise = (aircraft.FuelBurnCruise == default(double))
                                    ? 0
                                    : aircraft.FuelBurnCruise;
                                AircraftToUpdate.TASCruise = (aircraft.TASCruise == default(double))
                                    ? 0
                                    : aircraft.TASCruise;
                                AircraftToUpdate.RateOfClimb = (aircraft.RateOfClimb == default(double))
                                    ? 0
                                    : aircraft.RateOfClimb;
                                AircraftToUpdate.FuelBurnRateClimb = aircraft.FuelBurnRateClimb;
                                AircraftToUpdate.TASClimb = (aircraft.TASClimb == default(double))
                                    ? 0
                                    : aircraft.TASClimb;
                                AircraftToUpdate.RateOfDescent = (aircraft.RateOfDescent == default(double))
                                    ? 0
                                    : aircraft.RateOfDescent;
                                AircraftToUpdate.FuelBurnRateOfDescent = (aircraft.FuelBurnRateOfDescent == default(double))
                                    ? 0
                                    : aircraft.FuelBurnRateOfDescent;
                                AircraftToUpdate.TASDescent = (aircraft.TASDescent == default(double))
                                    ? 0
                                    : aircraft.TASDescent;
                            }

                            context.SaveChanges();
                            transaction.Commit();

                            if (aircraft.IsUpdateAvailableForEngine)
                            {
                                if (!string.IsNullOrEmpty(aircraft.EngineSettings))
                                {
                                    AircraftToUpdate.LastUpdateDateEngineSetting = DateTime.UtcNow;
                                    if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml"))
                                    {
                                        File.Delete(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId + ".xml");
                                    }
                                    XmlDocument doc = new XmlDocument();
                                    try
                                    {
                                        doc.LoadXml(aircraft.EngineSettings);
                                    }
                                    catch (Exception ex)
                                    {
                                        //logger.Fatal("Error while saving the XML file (Update Aircraft) " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                                    }
                                    doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraft.AircraftId.ToString() + ".xml");
                                }
                            }

                            // this will send all the fields back to calling end.    in case of image updation.
                            ////    rAircraft = AircraftProfileResponseModel.Create(AircraftToUpdate);
                            response.AircraftId = AircraftToUpdate.Id;
                            response.UniqueId = aircraft.UniqueId;
                            response.ImageName = AircraftToUpdate.ImageUrl;
                            response.ProfileId = aircraft.ProfileId;
                            response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                            response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                            return response;

                            #endregion update
                        } // update aircraft completed

                        // send updated aircraft in response

                    }
                    catch (Exception e)
                    {
                        string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(aircraft.AircraftId);
                        string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                        if (File.Exists(aircraftLogPath))
                        {
                            using (StreamWriter sw = File.AppendText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Create Update aircraft (API - CreateUpdateAircraftNNumber)");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        else
                        {
                            using (StreamWriter sw = File.CreateText(aircraftLogPath))
                            {
                                sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                                sw.WriteLine("Exception while Create Update aircraft (API - CreateUpdateAircraftNNumber)");
                                sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                                sw.WriteLine("=====================================================================================");
                                sw.WriteLine("");
                            }
                        }
                        return new AircraftModifyNnumberResponse().Exception();
                    }
                }
            } // using
        }

        public GeneralResponse CreateAircraftWithoutUnit(string tailNo, long uniqueId, int profileId, string engineSetting, out int aircraftId, out string ownerName, out int ownerProfileId)
        {
            aircraftId = 0;
            ownerName = "";
            ownerProfileId = 0;

            var context = new GuardianAvionicsEntities();
            using (var transaction = context.Database.BeginTransaction())
            {
                var response = new GeneralResponse();
                try
                {
                    #region create

                    // check if N-Number is unique or not
                    var testAircraftObj = context.AircraftProfiles.FirstOrDefault(ap => ap.Registration == tailNo);
                    if (testAircraftObj != null)
                    {
                        aircraftId = testAircraftObj.Id;
                        ownerName = testAircraftObj.OwnerProfileId == null ? "Not Available" : (testAircraftObj.Profile.UserDetail.FirstName + " " + testAircraftObj.Profile.UserDetail.LastName);
                        ownerProfileId = testAircraftObj.OwnerProfileId == null ? 0 : testAircraftObj.Profile.Id;
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.AircraftCodes.TailNoAlreadyExist).ToString(),
                            ResponseMessage = Enumerations.AircraftCodes.TailNoAlreadyExist.GetStringValue()
                        };
                    }
                    // create new aircraft
                    var objaircraft = new AircraftProfile();

                    // when aircraft is created then ImageOptions.DoNothing option will be invalid
                    objaircraft.ImageUrl = objaircraft.ImageUrl = string.Empty;



                    //objaircraft.AdditionalFuel = aircraft.AdditionalFuel;
                    objaircraft.LastUpdateDateEngineSetting = DateTime.UtcNow;
                    objaircraft.LastUpdateDateIpassenger = DateTime.UtcNow;
                    objaircraft.LastUpdateDatePerformance = DateTime.UtcNow;
                    objaircraft.AdditionalFuel = 0;
                    objaircraft.Altitude = 0;
                    objaircraft.Capacity = "";
                    objaircraft.Color = "";
                    objaircraft.FuelBurnCruise = 0;
                    objaircraft.FuelBurnRateClimb = 0;
                    objaircraft.FuelBurnRateOfDescent = 0;
                    objaircraft.FuelUnit = "GAL";
                    objaircraft.HomeBase = null;
                    objaircraft.LastUpdated = DateTime.UtcNow;
                    objaircraft.Make = null;
                    objaircraft.Model = null;
                    objaircraft.OtherAircraftManufacturer = "";
                    objaircraft.OtheAircraftrModel = "";
                    objaircraft.MP = 0;
                    objaircraft.Power = 0;
                    // Remove ProfileId From Table 
                    //objaircraft.ProfileId = aircraft.ProfileId;
                    objaircraft.RateOfClimb = 0;
                    objaircraft.RateOfDescent = 0;
                    objaircraft.Registration = tailNo;
                    objaircraft.RPM = 0;
                    objaircraft.SpeedUnit = "KTS";
                    objaircraft.TASClimb = 0;
                    objaircraft.TASCruise = 0;
                    objaircraft.TASDescent = 0;
                    //objaircraft.TaxiFuel = aircraft.TaxiFuel;
                    objaircraft.TaxiFuel = 0; //ChangeDataType
                    objaircraft.VerticalSpeed = "FT/MIN";
                    objaircraft.WeightUnit = "LBS";
                    objaircraft.AeroUnitNo = "";
                    objaircraft.EngineType = "SingleEngine";
                    objaircraft.AircraftType = null;
                    objaircraft.AircraftSerialNo = "";
                    objaircraft.AircraftYear = 0;
                    objaircraft.HobbsTime = 0;
                    objaircraft.HobbsTimeOffset = 0;
                    objaircraft.TachTime = 0;
                    objaircraft.TachTimeOffset = 0;
                    objaircraft.EngineMFGType = null;
                    objaircraft.OtherEngineManufacturerType = "";

                    objaircraft.EngineTBO = 0;
                    objaircraft.Engine1LastMOHDate = DateTime.UtcNow;
                    objaircraft.CurrentEngineTime = 0;
                    objaircraft.EngineTimeOffset = 0;
                    objaircraft.ChargeBy = 1;
                    objaircraft.PropMFG = null;
                    objaircraft.OtherPropManufacturer = "";

                    objaircraft.Prop1TBO = 0;
                    objaircraft.Prop1OHDueDate = DateTime.UtcNow;
                    objaircraft.Prop1Time = 0;
                    objaircraft.Prop1TimeOffset = 0;
                    objaircraft.Engine2LastMOHDate = DateTime.UtcNow;
                    objaircraft.Engine2Time = 0;
                    objaircraft.Engine2TimeOffset = 0;
                    objaircraft.Prop2OHDueDate = DateTime.UtcNow;
                    objaircraft.Prop2Time = 0;
                    objaircraft.Prop2Timeoffset = 0;
                    objaircraft.UniqueId = uniqueId;
                    objaircraft.IsAmericaAircraft = false;
                    objaircraft.Comm = false;
                    objaircraft.Transponder = false;
                    objaircraft.EngineMonitor = false;
                    objaircraft.LastTransactionId = "-1";
                    objaircraft.OwnerProfileId = profileId;

                    context.AircraftProfiles.Add(objaircraft);
                    context.SaveChanges();

                    var aircraftsalesdetail = context.SalesDetailMasters.Where(s => s.DetailType == "Rental" || s.DetailType == "Instruction").Select(a => new { Id = a.Id, Type = a.DetailType }).ToList();
                    foreach (var obj in aircraftsalesdetail)
                    {
                        var salesdetail = context.AircraftSalesDetails.Create();

                        salesdetail.AircraftId = objaircraft.Id;
                        if (obj.Type == "Rental")
                        {
                            salesdetail.GLAccountNumber = "4060.00";
                            salesdetail.Price = Convert.ToDecimal(67.50);
                        }
                        else
                        {
                            salesdetail.GLAccountNumber = "4140.00";
                            salesdetail.Price = Convert.ToDecimal(35.00);
                        }

                        salesdetail.SalesDetailId = obj.Id;

                        context.AircraftSalesDetails.Add(salesdetail);
                    }

                    //var objMappingowner = new MappingAircraftAndPilot()
                    //{
                    //    AircraftId = objaircraft.Id,
                    //    ProfileId = objaircraft.OwnerProfileId ?? 0,
                    //    Deleted = false,
                    //    CreateDate = DateTime.UtcNow,
                    //    IsActive = true
                    //};
                    //context.MappingAircraftAndPilots.Add(objMappingowner);
                    MappingAircraftAndPilot(objaircraft.Id, objaircraft.OwnerProfileId ?? 0);

                    if (!string.IsNullOrEmpty(engineSetting))
                    {
                        XmlDocument doc = new XmlDocument();
                        try
                        {
                            doc.LoadXml(engineSetting);
                        }
                        catch (Exception ex)
                        {
                            //logger.Fatal("Error while saving the XML file (Create Aircraft)" + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                        }
                        doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + objaircraft.Id.ToString() + ".xml");
                    }



                    context.SaveChanges();


                    //logger.Fatal("Create Aircraft Method Call GGG");

                    string ownerEmailId = "";
                    var aProfile = context.Profiles.FirstOrDefault(f => f.Id == objaircraft.OwnerProfileId);
                    if (aProfile != null)
                    {
                        ownerEmailId = aProfile.EmailId;
                    }

                    // send aircraft id with tag and image url in response.

                    string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "aircraftId";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = objaircraft.Id;
                    Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spCreateAircraftTable", param);

                    string message = "<p>Date - " + DateTime.Now.ToString() + "</p><p>Congratulation, new aircraft has been registered form " + ConfigurationReader.ServerUrl + ".</p><table border=\"1\" style=\"width:50%\"> <tr><td>Registartion No.</td><td>" + objaircraft.Registration + "</td></tr><tr><td>AeroUnitNo</td><td>" + objaircraft.AeroUnitNo + "</td></tr><tr><td>Owner Email Id</td><td>" + ownerProfileId + "</td></tr></table>";
                    Misc.SendEmailCommon("<p>Date - " + DateTime.Now.ToString() + "</p>" + message, "Aircraft Registration");

                    string aircraftLogFileName = tailNo.Replace(" ", "_");
                    string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                    using (StreamWriter sw = File.CreateText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Congratulation, new aircraft has been registered");
                        sw.WriteLine("Registration No " + tailNo);
                        sw.WriteLine("AeroUnitNo - ");
                        sw.WriteLine("Owner Email Id - " + ownerEmailId);
                        sw.WriteLine("Creation Date - " + DateTime.UtcNow.ToShortDateString());
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                    transaction.Commit();
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();

                    return response;

                    #endregion create
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue();
                    //logger.Fatal("Exception while creating Aircraft without Unit : " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                    return response;
                }
            }
        }



        private GeneralResponse VaildateAircraftModifyNnumberRequest(AircraftModifyNnumberRequest aircraft,
            GuardianAvionicsEntities context)
        {
            var userProfile = context.Profiles.FirstOrDefault(p => p.Id == aircraft.ProfileId);
            if (aircraft.IsPilot)
            {
                if (aircraft.ProfileId < 1 || string.IsNullOrEmpty(Convert.ToString(aircraft.ProfileId)))
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.InvalidProfileId).ToString(),
                        ResponseMessage = Enumerations.AircraftCodes.InvalidProfileId.GetStringValue()
                    };
                }
                if (userProfile == default(Profile))
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.ProfileIdNotFound).ToString(),
                        ResponseMessage = Enumerations.AircraftCodes.ProfileIdNotFound.GetStringValue()
                    };
            }

            if (aircraft.IsUpdateAvailableForGeneral)
            {

                var profile = context.Profiles.FirstOrDefault(p => p.EmailId == aircraft.OwnerEmailId);
                if (profile == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.OwnerEmailIdNotFound).ToString(),
                        ResponseMessage = Enumerations.AircraftCodes.OwnerEmailIdNotFound.GetStringValue()
                    };
                }

                if (string.IsNullOrEmpty(aircraft.Registration))
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                        ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue()
                    };

            }

            if (aircraft.UniqueId == default(long))
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.InvalidUniqueId).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.InvalidUniqueId.GetStringValue()
                };

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString(),
                ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue()
            };
        }

        public ManufacturereModelListResponse GetFieldsForDropDownlist(ManufacturereModelListRequest model)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var lastUpdateDate = Misc.GetDate(model.LastUpdatedDateTime) ?? DateTime.MinValue;
                var response = new ManufacturereModelListResponse
                {
                    AircraftManufacturerAndModelList = new List<AircraftManufacturerAndModel>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    ComponentManufacturerModelList = new List<ComponentManufacturerModel>(),
                    DeletedComponentModelList = new List<ListItems>()
                };

                var manifactureList =
                    context.AircraftManufacturers.Where(x => x.Id != 1 && x.LastUpdated >= lastUpdateDate)
                        .Include(a => a.AircraftModelLists).ToList();

                if (manifactureList.Any())
                {
                    manifactureList = context.AircraftManufacturers.Where(x => x.Id != 1 && !x.Deleted)
                        .Include(a => a.AircraftModelLists).ToList();
                    var objNameID = new List<AircraftManufacturerAndModel>();
                    manifactureList.ForEach(m => objNameID.Add(new AircraftManufacturerAndModel
                    {
                        aircraftManufacturerNameAndId = new ListItems { text = m.Name, value = m.Id },
                        aircraftModelList = (from mo in m.AircraftModelLists
                                             where mo.LastUpdated >= lastUpdateDate
                                             select new ListItems
                                             {
                                                 text = mo.ModelName,
                                                 value = mo.Id
                                             }).ToList()
                    }));

                    response.AircraftManufacturerAndModelList = objNameID;


                    if (context.AircraftComponentModels.Where(cm => cm.LastUpdateDate >= lastUpdateDate || cm.AircraftComponent.LastUpdateDate >= lastUpdateDate || cm.AircraftComponentManufacturer.LastUpdateDate >= lastUpdateDate).Any())
                    {
                        response.ComponentManufacturerModelList = context.AircraftComponentModels.Where(cm => !cm.Deleted)
                                                .Select(cm => new
                                                {
                                                    AircraftComponentModelId = cm.Id,
                                                    AircraftComponentModelName = cm.Name,
                                                    cm.AircraftComponentId,
                                                    AircraftComponentName = cm.AircraftComponent.ComponentName,
                                                    cm.AircraftComponentManufacturerId,
                                                    AircraftComponentManufacturerName = cm.AircraftComponentManufacturer.Name,
                                                    cm.UserMannual,
                                                    FileUrl = ConfigurationReader.DocumentServerPathKey + "/" + cm.UserMannual,
                                                    IsUserAvailable = cm.AircraftComponentManufacturer.MappingAircraftManufacturerAndUsers.Any()
                                                }).GroupBy(cm => new
                                                {
                                                    cm.AircraftComponentId,
                                                    cm.AircraftComponentName
                                                }).Select(g => new ComponentManufacturerModel
                                                {
                                                    componentNameAndId = new ListItems { text = g.Key.AircraftComponentName, value = g.Key.AircraftComponentId },
                                                    manufacturerAndModelList = g.Select(cm => new
                                                    {
                                                        cm.AircraftComponentManufacturerId,
                                                        cm.AircraftComponentManufacturerName,
                                                        cm.AircraftComponentModelId,
                                                        cm.AircraftComponentModelName,
                                                        cm.UserMannual,
                                                        cm.FileUrl,
                                                        cm.IsUserAvailable
                                                    }).GroupBy(cm2 => new
                                                    {
                                                        cm2.AircraftComponentManufacturerId,
                                                        cm2.AircraftComponentManufacturerName
                                                    }).Select(g2 => new ManufacturerAndModel
                                                    {
                                                        manufacturerNameAndId = new ListItems { text = g2.Key.AircraftComponentManufacturerName, value = g2.Key.AircraftComponentManufacturerId },
                                                        isUserAvailable = g2.Select(c => c.IsUserAvailable).FirstOrDefault(),
                                                        componentModelList = g2.Select(c => new ComponentModel
                                                        {
                                                            name = c.AircraftComponentModelName,
                                                            id = c.AircraftComponentModelId,
                                                            fileUrl = string.IsNullOrEmpty(c.UserMannual) ? null : c.FileUrl,
                                                            fileName = string.IsNullOrEmpty(c.UserMannual) ? null : c.UserMannual
                                                        }).ToList()
                                                    }).ToList()
                                                }).OrderBy(cm => cm.componentNameAndId.value).ToList();
                    }
                }

                response.DeletedComponentModelList =
                    context.AircraftComponentModels.Where(a => a.Deleted && a.LastUpdateDate >= lastUpdateDate)
                        .Select(s => new ListItems()
                        {
                            value = s.Id,
                            text = s.Name
                        }).ToList();

                // Rating type
                if (context.RatingTypes.Where(rt => rt.LastUpdated >= lastUpdateDate).Any())
                {
                    response.RatingType.AddRange(
                    context.RatingTypes.Where(rt => !rt.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList());
                }


                // Propeller
                if ((from pro in context.Propellers
                     where pro.LastUpdated >= lastUpdateDate
                     select pro).Any())
                {
                    response.Propeller.AddRange
                        (
                            (from pro in context.Propellers
                             where !pro.Deleted
                             select pro).Select(s => new ListItems
                             {
                                 text = s.Name,
                                 value = s.Id,
                             }).ToList()
                        );
                }

                // Engine
                if ((from eng in context.Engines
                     where eng.LastUpdated >= lastUpdateDate
                     select eng).Any())
                {
                    response.Engine.AddRange
                        (
                            (from eng in context.Engines
                             where !eng.Deleted
                             select eng).Select(s => new ListItems
                             {
                                 value = s.Id,
                                 text = s.Name,
                             }).ToList()
                        );
                }

                // Medical certificate class
                if ((from med in context.MedicalCertClasses
                     where med.LastUpdated >= lastUpdateDate
                     select med).Any())
                {
                    response.MedicalClass.AddRange
                        (
                            (from med in context.MedicalCertClasses
                             where !med.Deleted
                             select med).Select(s => new ListItems
                             {
                                 value = s.Id,
                                 text = s.Name
                             }).ToList()
                        );
                }
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception e)
            {
                return new ManufacturereModelListResponse
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
            finally
            {
                context.Dispose();
            }
        }

        public GetConstants GetEngines()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.Engine.AddRange
                   (
                       context.Engines.Where(rt => !rt.Deleted).Select(s => new ListItems
                       {
                           text = s.Name,
                           value = s.Id,
                       }).ToList()
                   );

                response.ComponentList.AddRange
                  (
                       context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
                       {
                           text = s.ComponentName,
                           value = s.Id,
                       }).ToList()
                  );

                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetRatingTypes()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.RatingType.AddRange(
                     context.RatingTypes.Where(rt => !rt.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id,
                     }).ToList());
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetPropellers()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.Propeller.AddRange(
                     context.Propellers.Where(rt => !rt.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id,
                     }).ToList());
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetMedicalCertificateClass()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.MedicalClass.AddRange
                    (
                         context.MedicalCertClasses.Where(rt => !rt.Deleted).Select(s => new ListItems
                         {
                             text = s.Name,
                             value = s.Id,
                         }).ToList()
                    );
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetFlightBagDocuments()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.Document.AddRange(context.Documents.Where(d => d.IsForAll && !d.Deleted).Select(s => new ListItems
                {
                    text = s.Url,
                    value = s.Id

                }).ToList());
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetAircraftManufacturer()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                response.AircraftManufacturer.AddRange(
                     context.AircraftManufacturers.Where(a => !a.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id
                     }).ToList());
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants GetComponentManufacturer()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };
                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                response.ComponentManufacturerAndUserList = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        public GetConstants ComponentModel()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new GetConstants
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var response = new GetConstants
                {
                    AircraftManufacturer = new List<ListItems>(),
                    AircraftModel = new List<ListItems>(),
                    Engine = new List<ListItems>(),
                    MedicalClass = new List<ListItems>(),
                    Propeller = new List<ListItems>(),
                    RatingType = new List<ListItems>(),
                    //ComponentManufacturer = new List<ListItems>(),
                    ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
                    ComponentModel = new List<ComponentModel_ConfigSetting>(),
                    ComponentList = new List<ListItems>(),
                    Document = new List<ListItems>()
                };


                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                response.ComponentManufacturerAndUserList = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();

                response.ComponentList.AddRange
                 (
                      context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
                      {
                          text = s.ComponentName,
                          value = s.Id,
                      }).ToList()
                 );

                response.ComponentModel =
                    context.AircraftComponentModels.Where(w => !w.Deleted)
                        .Select(s => new ComponentModel_ConfigSetting()
                        {
                            Id = s.Id,
                            componentName = s.AircraftComponent.ComponentName,
                            CompnentId = s.AircraftComponentId,
                            FileName = s.UserMannual,
                            ManufacturerName = s.AircraftComponentManufacturer.Name,
                            ComponentManufacturerId = s.AircraftComponentManufacturerId,
                            ModelName = s.Name
                        }).OrderBy(o => o.CompnentId).ThenBy(a => a.ComponentManufacturerId).ToList();


                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                return new GetConstants
                {
                    ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue()
                };
            }
        }

        //public GetConstants GetConstants()
        //{
        //    var context = new GuardianAvionicsEntities();
        //    try
        //    {
        //        if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
        //            return new GetConstants
        //            {
        //                ResponseCode = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue(),
        //                ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.ToString()
        //            };

        //        var response = new GetConstants
        //        {
        //            AircraftManufacturer = new List<ListItems>(),
        //            AircraftModel = new List<ListItems>(),
        //            Engine = new List<ListItems>(),
        //            MedicalClass = new List<ListItems>(),
        //            Propeller = new List<ListItems>(),
        //            RatingType = new List<ListItems>(),
        //            //ComponentManufacturer = new List<ListItems>(),
        //            ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>(),
        //            ComponentModel = new List<ComponentModel_ConfigSetting>(),
        //            ComponentList = new List<ListItems>(),
        //            Document = new List<ListItems>()
        //        };

        //        //AircraftManufacturer

        //        response.AircraftManufacturer.AddRange(
        //            context.AircraftManufacturers.Where(a => !a.Deleted).Select(s => new ListItems
        //            {
        //                text = s.Name,
        //                value = s.Id
        //            }).ToList());

        //        //ComponentManufacturer

        //        var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
        //        response.ComponentManufacturerAndUserList = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
        //        {
        //            Id = s.Id,
        //            Name = s.ManufacturerName,
        //            EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
        //        }).ToList();

        //        // Rating type

        //        response.RatingType.AddRange(
        //            context.RatingTypes.Where(rt => !rt.Deleted).Select(s => new ListItems
        //            {
        //                text = s.Name,
        //                value = s.Id,
        //            }).ToList());

        //        //Document For all
        //        response.Document.AddRange(context.Documents.Where(d => d.IsForAll && !d.Deleted).Select(s => new ListItems
        //        {
        //            text = s.Url,
        //            value = s.Id

        //        }).ToList());


        //        // Propeller

        //        response.Propeller.AddRange
        //            (
        //                 context.Propellers.Where(rt => !rt.Deleted).Select(s => new ListItems
        //                 {
        //                     text = s.Name,
        //                     value = s.Id,
        //                 }).ToList()
        //            );

        //        // Engine

        //        response.Engine.AddRange
        //            (
        //                context.Engines.Where(rt => !rt.Deleted).Select(s => new ListItems
        //                {
        //                    text = s.Name,
        //                    value = s.Id,
        //                }).ToList()
        //            );

        //        // Medical certificate class

        //        response.MedicalClass.AddRange
        //            (
        //                 context.MedicalCertClasses.Where(rt => !rt.Deleted).Select(s => new ListItems
        //                 {
        //                     text = s.Name,
        //                     value = s.Id,
        //                 }).ToList()
        //            );

        //        response.ComponentList.AddRange
        //           (
        //                context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
        //                {
        //                    text = s.ComponentName,
        //                    value = s.Id,
        //                }).ToList()
        //           );

        //        response.ComponentModel =
        //            context.AircraftComponentModels.Where(w => !w.Deleted)
        //                .Select(s => new ComponentModel_ConfigSetting()
        //                {
        //                    Id = s.Id,
        //                    componentName = s.AircraftComponent.ComponentName,
        //                    CompnentId = s.AircraftComponentId,
        //                    FileName = s.UserMannual,
        //                    ManufacturerName = s.AircraftComponentManufacturer.Name,
        //                    ComponentManufacturerId = s.AircraftComponentManufacturerId,
        //                    ModelName = s.Name
        //                }).OrderBy(o => o.CompnentId).ThenBy(a => a.ComponentManufacturerId).ToList();

        //        response.ResponseCode = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
        //        response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.ToString();
        //        return response;
        //    }
        //    catch (Exception e)
        //    {
        //        //ExceptionHandler.ReportError(e, Newtonsoft.Json.JsonConvert.SerializeObject(model));
        //        return new GetConstants
        //        {
        //            ResponseCode = Enumerations.RegistrationReturnCodes.Exception.GetStringValue(),
        //            ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.ToString()
        //        };
        //    }
        //    finally
        //    {
        //        context.Dispose();
        //    }
        //}

        public string RestoreAircraft(string aircraftIds, int profileId)
        {
            int[] ids = { };
            string response = string.Empty;
            ids = aircraftIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var objMapping = context.MappingAircraftAndPilots.Where(a => ids.ToList().Contains(a.AircraftId) && a.Deleted == true && a.ProfileId == profileId).ToList();
                    objMapping.ForEach(o => { o.Deleted = false; });

                    var objPilotLogsList = context.PilotLogs.Where(a => ids.ToList().Contains(a.AircraftId) && a.Deleted == true).ToList();
                    objPilotLogsList.ForEach(p => { p.LastUpdated = DateTime.UtcNow; p.Deleted = false; });
                    var preference = context.Preferences.FirstOrDefault(f => f.ProfileId == profileId);
                    if (preference != null)
                    {
                        preference.LastUpdateDate = DateTime.UtcNow;
                    }

                    var aircraftList = context.AircraftProfiles.Where(a => ids.ToList().Contains(a.Id)).ToList();
                    aircraftList.ForEach(p => { p.LastUpdated = DateTime.UtcNow; });
                    context.SaveChanges();
                    response = GA.Common.ResourcrFiles.Common.Messages.UpdateRecord.ToString();
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                //logger.Fatal("Exception AircraftId = " + aircraftIds + "ProfileId = " + profileId, ex);
                response = GA.Common.ResourcrFiles.Common.Messages.ErrorWhileProcessing.ToString();
            }
            return response;
        }

        /// <summary>
        /// Gets the AeroUnitNo(FMS) and check that the no. exists or not and return message  as true or false 
        /// </summary>
        /// <param name="FMS">AeroUnit Number</param>
        /// <returns></returns>
        public GeneralResponse IsFmsExists(AeroUnitNumberRequestModel model)
        {
            var response = new GeneralResponse();



            if (string.IsNullOrEmpty(model.FMS))
            {
                response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitNoNotFound).ToString();
                response.ResponseMessage = Enumerations.FMSSearch.AeroUnitNoNotFound.GetStringValue();
                return response;
            }

            using (var context = new GuardianAvionicsEntities())
            {
                //using (var dbContextTransaction = context.Database.BeginTransaction())
                //{
                try
                {
                    var objAircraft = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == model.FMS && a.Deleted == false);
                    if (objAircraft != null)
                    {
                        response.ResponseCode = ((int)Enumerations.FMSSearch.True).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.True.GetStringValue();
                    }
                    else
                    {
                        response.ResponseCode = ((int)Enumerations.FMSSearch.False).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.False.GetStringValue();
                    }
                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(model), ex);
                    var errorLog = context.ErrorLogs.Create();
                    errorLog.ModuleName = "API";
                    errorLog.ErrorSource = "IsFmsExists";
                    errorLog.ErrorDate = DateTime.UtcNow;
                    errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                    errorLog.ErrorDate = DateTime.UtcNow;
                    errorLog.LastUpdateDate = DateTime.UtcNow;
                    errorLog.ResolvedStatus = false;
                    errorLog.RequestParameters = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    context.ErrorLogs.Add(errorLog);
                    context.SaveChanges();
                    response.ResponseCode = ((int)Enumerations.FMSSearch.Exception).ToString();
                    response.ResponseMessage = Enumerations.FMSSearch.Exception.GetStringValue();
                }
                finally
                {
                    context.Dispose();
                }
                //}
            }
            return response;
        }

        /// <summary>
        /// Gets the AeroUnitNo(FMS) and check that the no. exists or not. If Not Exists then insert this AeroUnitNo into AeroUnitMaster Table otherwise return message "NotRegistered"
        /// </summary>
        /// <param name="FMS">AeroUnit Number</param>
        /// <returns></returns>
        public GeneralResponse RegisterFMS(AeroUnitNumberRequestModel model)
        {
            var response = new GeneralResponse();

            if (string.IsNullOrEmpty(model.FMS))
            {
                response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitNoNotFound).ToString();
                response.ResponseMessage = Enumerations.FMSSearch.AeroUnitNoNotFound.GetStringValue();
                return response;
            }

            using (var context = new GuardianAvionicsEntities())
            {
                //using (var dbContextTransaction = context.Database.BeginTransaction())
                //{
                try
                {
                    int FMSNo = Convert.ToInt32(model.FMS);
                    model.FMS = FMSNo.ToString();
                    var objAircraft = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == (model.FMS) && a.Deleted == false);
                    if (objAircraft != null)
                    {
                        response.ResponseCode = ((int)Enumerations.FMSSearch.FMSAlreadyRegistered).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.FMSAlreadyRegistered.GetStringValue();
                    }
                    else
                    {
                        var objAeroUnitMaster = new AeroUnitMaster()
                        {
                            AeroUnitNo = (Convert.ToInt32(model.FMS)).ToString(),
                            AircraftId = null,
                            CreateDate = DateTime.UtcNow,
                            CreatedBy = string.Empty,
                            Deleted = false,
                            Blocked = false,
                            BlockedReason = string.Empty
                        };
                        context.AeroUnitMasters.Add(objAeroUnitMaster);
                        context.SaveChanges();
                        //dbContextTransaction.Commit();
                        response.ResponseCode = ((int)Enumerations.FMSSearch.Success).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.Success.GetStringValue();
                    }
                }
                catch (Exception ex)
                {
                    response.ResponseCode = ((int)Enumerations.FMSSearch.Exception).ToString();
                    response.ResponseMessage = Enumerations.FMSSearch.Exception.GetStringValue();
                }
                finally
                {
                    context.Dispose();
                }
                //}
            }
            return response;
        }

        public GeneralResponse DeleteAeroUnit(AeroUnitNumberRequestModel model)
        {
            var response = new GeneralResponse();
            if (string.IsNullOrEmpty(model.FMS))
            {
                response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitNoNotFound).ToString();
                response.ResponseMessage = Enumerations.FMSSearch.AeroUnitNoNotFound.GetStringValue();
                return response;
            }

            using (var context = new GuardianAvionicsEntities())
            {
                try
                {
                    var objAircraft = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == model.FMS && !a.Deleted);
                    if (objAircraft != null)
                    {
                        if (objAircraft.AircraftId != null)
                        {
                            response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitLinkedWithAircraft).ToString();
                            response.ResponseMessage = Enumerations.FMSSearch.AeroUnitLinkedWithAircraft.GetStringValue();
                        }
                        else
                        {
                            context.AeroUnitMasters.Remove(objAircraft);
                            context.SaveChanges();
                            response.ResponseCode = ((int)Enumerations.FMSSearch.Success).ToString();
                            response.ResponseMessage = Enumerations.FMSSearch.Success.GetStringValue();
                        }
                    }
                    else
                    {
                        response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitNumberNotExist).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.AeroUnitNumberNotExist.GetStringValue();
                    }
                }
                catch (Exception ex)
                {
                    response.ResponseCode = ((int)Enumerations.FMSSearch.Exception).ToString();
                    response.ResponseMessage = Enumerations.FMSSearch.Exception.GetStringValue();
                }
                finally
                {
                    context.Dispose();
                }
            }
            return response;
        }




        /// <summary>
        /// Check if AeroUnit is Register with any Aircraft Or Not, if not then return NotRegistered , if Register then 
        /// check IsPilot = true then check that this pilot is link with aircraft or not , if linked then return the aircraft details else map the aircraft with the Pilot
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AircraftProfileResponseModel ISFMSRegisterWithAircraft(AeroUnitRequestModel model)
        {
            var response = new AircraftProfileResponseModel { AircraftList = new List<AircraftModel>() };
            using (var context = new GuardianAvionicsEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //Select record if there is an aircraft register with the AeroUnitNo
                        var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == model.FMS && a.Deleted == false);
                        //Check AeroUnit is available or Not
                        if (objAeroUnit != null)
                        {
                            //Aero Unit is Available , Now Check there is an Aircraft is mapped or not
                            if (objAeroUnit.AircraftId != null)
                            {
                                //Finds that Aircraft is mapped with AeroUnit, Now check the pilot flag is true or not
                                if (model.IsPilot == true)
                                {
                                    var aircraftProfile = objAeroUnit.AircraftProfile;

                                    //Validate the Profile object.
                                    var objProfile = context.Profiles.Where(p => p.Id == model.ProfileId).FirstOrDefault();
                                    if (objProfile != null)
                                    {
                                        if (objProfile.Deleted)
                                        {
                                            response.ResponseCode = ((int)Enumerations.FMSSearch.CannotLinkWithDeletedPilot).ToString();
                                            response.ResponseMessage = Enumerations.FMSSearch.CannotLinkWithDeletedPilot.GetStringValue();
                                            return response;
                                        }
                                    }
                                    else
                                    {
                                        response.ResponseCode = ((int)Enumerations.FMSSearch.ProfileIdNotFound).ToString();
                                        response.ResponseMessage = Enumerations.FMSSearch.ProfileIdNotFound.GetStringValue();
                                        return response;
                                    }


                                    MappingAircraftAndPilot(objAeroUnit.AircraftId ?? 0, model.ProfileId);
                                    response.AircraftList.Add(AircraftProfileResponseModel.Create(aircraftProfile, null));
                                    response.ResponseCode = ((int)Common.Enumerations.FMSSearch.Success).ToString();
                                    response.ResponseMessage = Common.Enumerations.FMSSearch.Success.GetStringValue();
                                }
                                else
                                {
                                    var aircraftProfile = objAeroUnit.AircraftProfile;
                                    response.AircraftList.Add(AircraftProfileResponseModel.Create(aircraftProfile, null));
                                    response.ResponseCode = ((int)Common.Enumerations.FMSSearch.Success).ToString();
                                    response.ResponseMessage = Common.Enumerations.FMSSearch.Success.GetStringValue();
                                }
                            }
                            else
                            {

                                //Aero Unit Is Not Registered With Any Aircraft
                                response.ResponseCode = ((int)Enumerations.FMSSearch.NotRegisterWithAircraft).ToString();
                                response.ResponseMessage = Enumerations.FMSSearch.NotRegisterWithAircraft.GetStringValue();
                            }
                        }
                        else
                        {
                            // Invalid Aero Unit No Is Passed
                            response.ResponseCode = ((int)Enumerations.FMSSearch.AeroUnitNumberNotExist).ToString();
                            response.ResponseMessage = Enumerations.FMSSearch.AeroUnitNumberNotExist.GetStringValue();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        response.ResponseCode = ((int)Enumerations.FMSSearch.Exception).ToString();
                        response.ResponseMessage = Enumerations.FMSSearch.Exception.GetStringValue();
                    }
                    finally
                    {
                        context.Dispose();
                    }
                }
            }
            return response;
        }

        public string GetUserMannual(int modelId)
        {
            string fileName = string.Empty;
            var context = new GuardianAvionicsEntities();
            try
            {
                var componentModel = context.AircraftComponentModels.FirstOrDefault(f => f.Id == modelId);
                fileName = componentModel.UserMannual;
            }
            catch (Exception)
            {
                throw;
            }
            return fileName;
        }


        public GeneralResponse SetLastTransactionId(int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new GeneralResponse();
            try
            {
                var aircraft = context.AircraftProfiles.FirstOrDefault(a => a.Id == aircraftId);

                if (aircraft != null)
                {
                    aircraft.LastTransactionId = "-1";
                    context.SaveChanges();
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.AircraftIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.AircraftIdNotFound.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue()
                };
            }
            return response;
        }

        public AircraftProfileModelWeb DeleteAircraftSalesDetailType(int id, int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var aircraftSalesDetail = context.AircraftSalesDetails.FirstOrDefault(f => f.Id == id);

            context.AircraftSalesDetails.Remove(aircraftSalesDetail);
            context.SaveChanges();

            var response = new AircraftProfileModelWeb();

            response.aircraftSalesDetailList = context.AircraftSalesDetails.Where(s => s.AircraftId == aircraftId)
                                                                .Select(s => new AircraftSalesDetailWeb
                                                                {
                                                                    Id = s.Id,
                                                                    SalesDetailtype = s.SalesDetailMaster.DetailType,
                                                                    Price = (s.Price),
                                                                    GLAccountNumber = s.GLAccountNumber
                                                                }).ToList();
            return response;
        }

        public List<ManufacturerAuthForDataLog> GetCompManuListByIds(string manuIds, int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<ManufacturerAuthForDataLog>();

            if (manuIds == "")
            {
                return response;
            }
            var mappingAircraftManufacturerAndUsers = context.MappingComponentManufacturerAndAircrafts.FirstOrDefault(f => f.AircraftId == aircraftId);
            var distinctManuId = manuIds.Split(',').Distinct().ToList();
            response = context.AircraftComponentManufacturers.Where(s => distinctManuId.Contains(s.Id.ToString())).Select(s => new ManufacturerAuthForDataLog { ManufacturerId = s.Id, ISAuthenticateForDatalog = false, ManufacturerName = s.Name }).ToList();
            if (mappingAircraftManufacturerAndUsers != null)
            {
                List<string> manuIdlist = mappingAircraftManufacturerAndUsers.ComponentManufacturers.Split(',').ToList();
                response.Where(s => manuIdlist.Contains(s.ManufacturerId.ToString())).ToList().ForEach(f => f.ISAuthenticateForDatalog = true);
            }

            return response;
        }

        public ResponseModel SendPushMessageAfterFlightStart(int profileId, int aircraftId, string tokenId ,string startdate)
        {
            var context = new GuardianAvionicsEntities();
            ResponseModel response = new ResponseModel();
            //logger.Fatal("SendPushMessageAfterFlightStart Call : profile Id = " + profileId + " AircraftId = " + aircraftId);
            try
            {
                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);
                if (aircraft == null)
                {
                    return new ResponseModel
                    {
                        ResponseCode = ((int)Enumerations.SendPushMessageAfterFlightStart.AircraftIdNotExist).ToString(),
                        ResponseMessage = Enumerations.SendPushMessageAfterFlightStart.AircraftIdNotExist.GetStringValue()
                    };
                }

                var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                if (profile == null)
                {
                    return new ResponseModel
                    {
                        ResponseCode = ((int)Enumerations.SendPushMessageAfterFlightStart.ProfileIdNotExist).ToString(),
                        ResponseMessage = Enumerations.SendPushMessageAfterFlightStart.ProfileIdNotExist.GetStringValue()
                    };
                }

                string[] tokenIds = new string[] { };
                string message = "";
                DateTime dt = DateTime.UtcNow;
                tokenIds = context.PushNotifications.Where(p => p.ProfileId == aircraft.OwnerProfileId && p.TokenId != null).Select(s => s.TokenId).Distinct().ToArray();
                //Remove tokenId Passsed from the array
                tokenIds = tokenIds.Where(w => w != tokenId).ToArray();
                //logger.Fatal("Token Ids for notification = " + Newtonsoft.Json.JsonConvert.SerializeObject(tokenId));
                if (startdate!=null&& startdate != "")
                {
                    message = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName + " " + "has started a flight on " + String.Format("{0:MM/dd/yyyy HH:mm:ss}", startdate) + " with the aircraft - " + aircraft.Registration;
                }
                else
                {
                    message = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName + " " + "has started a flight on " + String.Format("{0:MM/dd/yyyy HH:mm:ss}", dt.ToString()) + " with the aircraft - " + aircraft.Registration;
                }
                if (tokenIds.Length > 0)
                {
                    //PushNotificationIPhone objPushMessage = new Common.PushNotificationIPhone();
                    //objPushMessage.Push(tokenIds, message);
                    IphonePushNotification objPushMessage = new IphonePushNotification();
                    foreach (var id in tokenIds)
                    {
                        objPushMessage.PushToiPhone(id, message, "");
                    }
                }
                else
                {
                    return new ResponseModel
                    {
                        ResponseCode = ((int)Enumerations.SendPushMessageAfterFlightStart.DeviceTokenIdNotAvailable).ToString(),
                        ResponseMessage = Enumerations.SendPushMessageAfterFlightStart.DeviceTokenIdNotAvailable.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseModel
                {
                    ResponseCode = ((int)Enumerations.SendPushMessageAfterFlightStart.Exception).ToString(),
                    ResponseMessage = Enumerations.SendPushMessageAfterFlightStart.Exception.GetStringValue()
                };
            }

            return new ResponseModel
            {
                ResponseCode = ((int)Enumerations.SendPushMessageAfterFlightStart.Success).ToString(),
                ResponseMessage = Enumerations.SendPushMessageAfterFlightStart.Success.GetStringValue()
            };
        }

        public List<MediaDetails> GetIPassengerDetails(bool isForAll, int mediaTypeMasterId, int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            List<MediaDetails> response = new List<MediaDetails>();
            string ipassengerMediaType = "";
            switch (mediaTypeMasterId)
            {
                case 1:
                    {
                        ipassengerMediaType = ConfigurationReader.BucketIPassVideo;
                        break;
                    }
                case 2:
                    {
                        ipassengerMediaType = ConfigurationReader.BucketIPassImage;
                        break;
                    }
                case 5:
                    {
                        ipassengerMediaType = ConfigurationReader.BucketIPassPDF;
                        break;
                    }

            }
            if (isForAll)
            {
                response.AddRange(
                  context.IPassengers.OrderByDescending(o => o.Id).Where(v => v.MediaType == mediaTypeMasterId && v.IsForAll == true).Select(s => new MediaDetails
                  {
                      Title = s.Title,
                      Id = s.Id,
                      FilePath = ConfigurationReader.s3BucketURL + ipassengerMediaType + s.FileName,
                      ThumbnailImagePath = ConfigurationReader.s3BucketURL + ipassengerMediaType + s.ThumbnailImage,
                      IsForAll = s.IsForAll ?? false
                  }).ToList()
              );
            }
            else
            {
                List<int> deletedIPassengerByAircraftid = context.IPassengerDetailsDeletedByUsers.Where(w => w.AircraftId == aircraftId).Select(s => s.IPassengerId).ToList();
                response.AddRange(
                       context.IPassengers.OrderByDescending(o => o.Id).Where(v => v.MediaType == mediaTypeMasterId && (v.AircraftId == aircraftId || v.IsForAll == true) && !deletedIPassengerByAircraftid.Contains(v.Id)).Select(s => new MediaDetails
                       {
                           Title = s.Title,
                           Id = s.Id,
                           IsForAll = s.IsForAll ?? false,
                           FilePath = ConfigurationReader.s3BucketURL + ipassengerMediaType + s.FileName,
                           ThumbnailImagePath = ConfigurationReader.s3BucketURL + ipassengerMediaType + s.ThumbnailImage
                       }).ToList()
                   );
            }
            return response;
        }

        public IPassengerMediaDetails AddIPassengerVideo(string title, string fileName, int aircraftId, decimal fileSize, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };


            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Video").Id;
            try
            {
                var iPassenger = context.IPassengers.Create();
                iPassenger.AircraftId = aircraftId == 0 ? null : (int?)aircraftId;
                iPassenger.CreateDate = DateTime.UtcNow;
                iPassenger.FileName = fileName;
                iPassenger.LastUpdateDate = DateTime.UtcNow;
                iPassenger.MediaType = mediaTypeMasterId;
                iPassenger.Title = title;
                iPassenger.ThumbnailImage = fileName.Split('.')[0] + "thumbnail.jpg";
                iPassenger.IsForAll = isForAll;
                iPassenger.FileSize = fileSize;
                context.IPassengers.Add(iPassenger);
                if (isForAll)
                {
                    var keyValueTable = context.KeyValueTables.FirstOrDefault();
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                if (aircraftId != 0)
                {
                    iPassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.VideoList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.VideoList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "WEB";
                errorLog.ErrorSource = "AddIPassengerVideo";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Title = " + title + " FileNAme = " + fileName + " AircraftID = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }

        public IPassengerMediaDetails UpdateDeleteIPassengerVideo(string title, int id, bool isUpdate, int aircraftId, bool isForAll)
        {

            var context = new GuardianAvionicsEntities();

            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };
            try
            {
                var ipassenger = context.IPassengers.FirstOrDefault(f => f.Id == id);
                ipassenger.LastUpdateDate = DateTime.UtcNow;
                var keyValueTable = context.KeyValueTables.FirstOrDefault();
                if (isForAll)
                {
                    //Update or delete by Admin
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                else
                {
                    if (ipassenger.AircraftProfile != null)
                    {
                        ipassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                    }
                }
                if (isUpdate)
                {
                    ipassenger.Title = title;
                }
                else
                {
                    if (isForAll)
                    {
                        var deletedIpassengerList = context.IPassengerDetailsDeletedByUsers.Where(w => w.IPassengerId == id);
                        context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedIpassengerList);
                        context.IPassengers.Remove(ipassenger);

                        new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassVideo");
                        new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassVideo");
                    }
                    else
                    {
                        if (ipassenger.IsForAll == true)
                        {
                            var iPassengerDetailsDeletedByUsers = context.IPassengerDetailsDeletedByUsers.Create();
                            iPassengerDetailsDeletedByUsers.IPassengerId = ipassenger.Id;
                            iPassengerDetailsDeletedByUsers.AircraftId = aircraftId;
                            context.IPassengerDetailsDeletedByUsers.Add(iPassengerDetailsDeletedByUsers);
                        }
                        else
                        {
                            context.IPassengers.Remove(ipassenger);
                            new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassVideo");
                            new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassVideo");
                        }

                    }

                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Video").Id;
                response.VideoList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.VideoList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                return response;
            }
            return response;
        }

        public IPassengerMediaDetails AddIPassengerImage(string title, string fileName, int aircraftId, decimal fileSize, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };

            Misc objMisc = new Misc();
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Image").Id;
            try
            {
                var iPassenger = context.IPassengers.Create();
                iPassenger.AircraftId = aircraftId == 0 ? null : (int?)aircraftId;
                iPassenger.CreateDate = DateTime.UtcNow;
                iPassenger.FileName = fileName;
                iPassenger.LastUpdateDate = DateTime.UtcNow;
                iPassenger.MediaType = mediaTypeMasterId;
                iPassenger.Title = title;
                iPassenger.ThumbnailImage = fileName.Split('.')[0] + "thumbnail.jpg";
                iPassenger.IsForAll = isForAll;
                iPassenger.FileSize = fileSize;
                context.IPassengers.Add(iPassenger);
                if (isForAll)
                {
                    var keyValueTable = context.KeyValueTables.FirstOrDefault();
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                if (aircraftId != 0)
                {
                    iPassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();
                response.ImageList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.ImageList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "WEB";
                errorLog.ErrorSource = "AddIPassengerImage";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Title = " + title + " FileNAme = " + fileName + " AircraftID = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }

        public IPassengerMediaDetails AddIPassengerPDF(string title, string fileName, int aircraftId, decimal fileSize, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };

            Misc objMisc = new Misc();
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "PDF").Id;
            try
            {
                var iPassenger = context.IPassengers.Create();
                iPassenger.AircraftId = aircraftId == 0 ? null : (int?)aircraftId;
                iPassenger.CreateDate = DateTime.UtcNow;
                iPassenger.FileName = fileName;
                iPassenger.LastUpdateDate = DateTime.UtcNow;
                iPassenger.MediaType = mediaTypeMasterId;
                iPassenger.Title = title;
                iPassenger.ThumbnailImage = fileName.Split('.')[0] + "thumbnail.jpg";
                iPassenger.IsForAll = isForAll;
                iPassenger.FileSize = fileSize;
                context.IPassengers.Add(iPassenger);

                if (isForAll)
                {
                    var keyValueTable = context.KeyValueTables.FirstOrDefault();
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                if (aircraftId != 0)
                {
                    iPassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();
                response.PDFList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.PDFList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "WEB";
                errorLog.ErrorSource = "AddIPassengerPDF";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Title = " + title + " FileNAme = " + fileName + " AircraftID = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }
        public IPassengerMediaDetails AddIPassengerWarning(string title, int aircraftId, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };


            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Warning").Id;
            try
            {
                var iPassenger = context.IPassengers.Create();
                iPassenger.AircraftId = aircraftId == 0 ? null : (int?)aircraftId;
                iPassenger.CreateDate = DateTime.UtcNow;
                iPassenger.FileName = "";
                iPassenger.LastUpdateDate = DateTime.UtcNow;
                iPassenger.MediaType = mediaTypeMasterId;
                iPassenger.Title = title;
                iPassenger.IsForAll = isForAll;
                iPassenger.FileSize = null;
                context.IPassengers.Add(iPassenger);
                if (isForAll)
                {
                    var keyValueTable = context.KeyValueTables.FirstOrDefault();
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                if (aircraftId != 0)
                {
                    iPassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.WarningList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.WarningList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "WEB";
                errorLog.ErrorSource = "AddIPassengerVideo";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Title = " + title + " AircraftID = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }

        public IPassengerMediaDetails UpdateDeleteIPassengerWarning(string title, int id, int aircraftId, bool isUpdate, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Warning").Id;
            try
            {
                var ipassenger = context.IPassengers.FirstOrDefault(f => f.Id == id);
                ipassenger.LastUpdateDate = DateTime.UtcNow;
                var keyValueTable = context.KeyValueTables.FirstOrDefault();
                if (isForAll)
                {
                    //Update or delete by Admin
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                else
                {
                    ipassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }

                if (isUpdate)
                {
                    ipassenger.Title = title;
                }
                else
                {
                    if (isForAll)
                    {
                        var deletedIpassengerList = context.IPassengerDetailsDeletedByUsers.Where(w => w.IPassengerId == id);
                        context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedIpassengerList);
                        context.IPassengers.Remove(ipassenger);
                    }
                    else
                    {
                        if (ipassenger.IsForAll == true)
                        {
                            var iPassengerDetailsDeletedByUsers = context.IPassengerDetailsDeletedByUsers.Create();
                            iPassengerDetailsDeletedByUsers.IPassengerId = ipassenger.Id;
                            iPassengerDetailsDeletedByUsers.AircraftId = aircraftId;
                            context.IPassengerDetailsDeletedByUsers.Add(iPassengerDetailsDeletedByUsers);
                        }
                        else
                        {
                            context.IPassengers.Remove(ipassenger);
                        }
                    }
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.WarningList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.WarningList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                return response = new IPassengerMediaDetails
                {
                    ImageList = new List<MediaDetails>(),
                    InstructionList = new List<MediaDetails>(),
                    PDFList = new List<MediaDetails>(),
                    VideoList = new List<MediaDetails>(),
                    WarningList = new List<MediaDetails>(),
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }
            response.ResponseCode = "1";
            response.ResponseMessage = "Success";
            return response;
        }

        public IPassengerMediaDetails AddIPassengerInstruction(string title, int aircraftId, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };


            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Instruction").Id;
            try
            {
                var iPassenger = context.IPassengers.Create();
                iPassenger.AircraftId = aircraftId == 0 ? null : (int?)aircraftId;
                iPassenger.CreateDate = DateTime.UtcNow;
                iPassenger.FileName = "";
                iPassenger.LastUpdateDate = DateTime.UtcNow;
                iPassenger.MediaType = mediaTypeMasterId;
                iPassenger.Title = title;
                iPassenger.IsForAll = isForAll;
                iPassenger.FileSize = null;
                context.IPassengers.Add(iPassenger);
                if (isForAll)
                {
                    var keyValueTable = context.KeyValueTables.FirstOrDefault();
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                if (aircraftId != 0)
                {
                    iPassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.InstructionList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.InstructionList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });
            }
            catch (Exception ex)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "WEB";
                errorLog.ErrorSource = "AddIPassengerInstruction";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = "Title = " + title + " AircraftID = " + aircraftId;
                context.ErrorLogs.Add(errorLog);
                context.SaveChanges();
                return new IPassengerMediaDetails
                {
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }

            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }

        public IPassengerMediaDetails UpdateDeleteIPassengerInstruction(string title, int id, int aircraftId, bool isUpdate, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Instruction").Id;
            try
            {
                var ipassenger = context.IPassengers.FirstOrDefault(f => f.Id == id);
                ipassenger.LastUpdateDate = DateTime.UtcNow;
                var keyValueTable = context.KeyValueTables.FirstOrDefault();
                if (isForAll)
                {
                    //Update or delete by Admin
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                else
                {
                    ipassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                if (isUpdate)
                {
                    ipassenger.Title = title;
                }
                else
                {
                    if (isForAll)
                    {
                        var deletedIpassengerList = context.IPassengerDetailsDeletedByUsers.Where(w => w.IPassengerId == id);
                        context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedIpassengerList);
                        context.IPassengers.Remove(ipassenger);
                    }
                    else
                    {
                        if (ipassenger.IsForAll == true)
                        {
                            var iPassengerDetailsDeletedByUsers = context.IPassengerDetailsDeletedByUsers.Create();
                            iPassengerDetailsDeletedByUsers.IPassengerId = ipassenger.Id;
                            iPassengerDetailsDeletedByUsers.AircraftId = aircraftId;
                            context.IPassengerDetailsDeletedByUsers.Add(iPassengerDetailsDeletedByUsers);
                        }
                        else
                        {
                            context.IPassengers.Remove(ipassenger);
                        }
                    }
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.InstructionList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.InstructionList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

            }
            catch (Exception ex)
            {
                return response = new IPassengerMediaDetails
                {
                    ImageList = new List<MediaDetails>(),
                    InstructionList = new List<MediaDetails>(),
                    PDFList = new List<MediaDetails>(),
                    VideoList = new List<MediaDetails>(),
                    WarningList = new List<MediaDetails>(),
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }
            response.ResponseCode = "1";
            response.ResponseMessage = "Success";
            return response;
        }

        public IPassengerMediaDetails UpdateDeleteIPassengerImage(string title, int id, int aircraftId, bool isUpdate, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "Image").Id;
            try
            {
                var ipassenger = context.IPassengers.FirstOrDefault(f => f.Id == id);
                ipassenger.LastUpdateDate = DateTime.UtcNow;
                var keyValueTable = context.KeyValueTables.FirstOrDefault();
                if (isForAll)
                {
                    //Update or delete by Admin
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                else
                {
                    ipassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                if (isUpdate)
                {
                    ipassenger.Title = title;
                }
                else
                {
                    if (isForAll)
                    {
                        var deletedIpassengerList = context.IPassengerDetailsDeletedByUsers.Where(w => w.IPassengerId == id);
                        context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedIpassengerList);
                        context.IPassengers.Remove(ipassenger);
                        new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassImage");
                        new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassImage");
                    }
                    else
                    {
                        if (ipassenger.IsForAll == true)
                        {
                            var iPassengerDetailsDeletedByUsers = context.IPassengerDetailsDeletedByUsers.Create();
                            iPassengerDetailsDeletedByUsers.IPassengerId = ipassenger.Id;
                            iPassengerDetailsDeletedByUsers.AircraftId = aircraftId;
                            context.IPassengerDetailsDeletedByUsers.Add(iPassengerDetailsDeletedByUsers);
                        }
                        else
                        {
                            context.IPassengers.Remove(ipassenger);
                            new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassImage");
                            new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassImage");
                        }
                    }
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.ImageList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.ImageList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

            }
            catch (Exception ex)
            {
                return response = new IPassengerMediaDetails
                {
                    ImageList = new List<MediaDetails>(),
                    InstructionList = new List<MediaDetails>(),
                    PDFList = new List<MediaDetails>(),
                    VideoList = new List<MediaDetails>(),
                    WarningList = new List<MediaDetails>(),
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }
            response.ResponseCode = "1";
            response.ResponseMessage = "Success";
            return response;
        }

        public IPassengerMediaDetails UpdateDeleteIPassengerPDF(string title, int id, int aircraftId, bool isUpdate, bool isForAll)
        {
            var response = new IPassengerMediaDetails
            {
                ImageList = new List<MediaDetails>(),
                InstructionList = new List<MediaDetails>(),
                PDFList = new List<MediaDetails>(),
                VideoList = new List<MediaDetails>(),
                WarningList = new List<MediaDetails>()
            };
            var context = new GuardianAvionicsEntities();
            int mediaTypeMasterId = context.IPassengerMediaTypeMasters.FirstOrDefault(f => f.MediaType == "PDF").Id;
            try
            {
                var ipassenger = context.IPassengers.FirstOrDefault(f => f.Id == id);
                ipassenger.LastUpdateDate = DateTime.UtcNow;
                var keyValueTable = context.KeyValueTables.FirstOrDefault();
                if (isForAll)
                {
                    //Update or delete by Admin
                    keyValueTable.IpassengerAdminLastUpdateDate = DateTime.UtcNow;
                }
                else
                {
                    ipassenger.AircraftProfile.LastUpdated = DateTime.UtcNow;
                }
                if (isUpdate)
                {
                    ipassenger.Title = title;
                }
                else
                {
                    if (isForAll)
                    {
                        var deletedIpassengerList = context.IPassengerDetailsDeletedByUsers.Where(w => w.IPassengerId == id);
                        context.IPassengerDetailsDeletedByUsers.RemoveRange(deletedIpassengerList);
                        context.IPassengers.Remove(ipassenger);
                        new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassPDF");
                        new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassPDF");
                    }
                    else
                    {
                        if (ipassenger.IsForAll == true)
                        {
                            var iPassengerDetailsDeletedByUsers = context.IPassengerDetailsDeletedByUsers.Create();
                            iPassengerDetailsDeletedByUsers.IPassengerId = ipassenger.Id;
                            iPassengerDetailsDeletedByUsers.AircraftId = aircraftId;
                            context.IPassengerDetailsDeletedByUsers.Add(iPassengerDetailsDeletedByUsers);
                        }
                        else
                        {
                            context.IPassengers.Remove(ipassenger);
                            new Misc().DeleteFileFromS3Bucket(ipassenger.FileName, "IPassPDF");
                            new Misc().DeleteFileFromS3Bucket(ipassenger.ThumbnailImage, "IPassPDF");
                        }
                    }
                }
                context.SaveChanges();
                Misc objMisc = new Misc();
                response.PDFList = GetIPassengerDetails(isForAll, mediaTypeMasterId, aircraftId);
                response.PDFList.ForEach(f => { f.TId = objMisc.Encrypt(f.Id.ToString()); f.Id = 0; });

            }
            catch (Exception ex)
            {
                return response = new IPassengerMediaDetails
                {
                    ImageList = new List<MediaDetails>(),
                    InstructionList = new List<MediaDetails>(),
                    PDFList = new List<MediaDetails>(),
                    VideoList = new List<MediaDetails>(),
                    WarningList = new List<MediaDetails>(),
                    ResponseCode = "1",
                    ResponseMessage = "Exception"
                };
            }
            response.ResponseCode = "1";
            response.ResponseMessage = "Success";
            return response;
        }

        public AircraftProfileModelWeb UpdateAircraftSalesDetailType(int id, decimal price, string GLAccNo, int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var aircraftSalesDetail = context.AircraftSalesDetails.FirstOrDefault(f => f.Id == id && f.AircraftId == aircraftId);

            aircraftSalesDetail.Price = price;
            aircraftSalesDetail.GLAccountNumber = GLAccNo;
            context.SaveChanges();

            var response = new AircraftProfileModelWeb();

            response.aircraftSalesDetailList = context.AircraftSalesDetails.Where(s => s.AircraftId == aircraftId)
                                                                .Select(s => new AircraftSalesDetailWeb
                                                                {
                                                                    Id = s.Id,
                                                                    SalesDetailtype = s.SalesDetailMaster.DetailType,
                                                                    Price = (s.Price),
                                                                    GLAccountNumber = s.GLAccountNumber
                                                                }).ToList();
            return response;

        }

        public Profile GetOwnerProfileByAircraftId(int aircraftId)
        {
            var context = new GuardianAvionicsEntities();
            var profile = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId).Profile;
            return profile;
        }

        public void UpdateAircraftLastUpdateDate(int aircraftId, string moduleName)
        {
            var context = new GuardianAvionicsEntities();
            var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);
            if (aircraft != null)
            {
                if (moduleName == "Engine")
                {
                    aircraft.LastUpdateDateEngineSetting = DateTime.UtcNow;
                }
                else if (moduleName == "General")
                {
                    aircraft.LastUpdated = DateTime.UtcNow;
                }
                else if (moduleName == "Performance")
                {
                    aircraft.LastUpdateDatePerformance = DateTime.UtcNow;
                }
                else if (moduleName == "Ipassenger")
                {
                    aircraft.LastUpdateDateIpassenger = DateTime.UtcNow;
                }
                context.SaveChanges();
            }
        }

        public string GetAircraftRegAndSerialNo(int aircraftId)
        {
            var msg = string.Empty;
            var context = new GuardianAvionicsEntities();
            var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);
            if (aircraft != null)
            {
                msg = aircraft.Registration.Replace(" ", "_") + "-" + aircraft.AircraftSerialNo;
            }
            return msg;
        }

        public GeneralResponse SendRequestToOwnerForAircraft(RequestToOwnerForAircraftModel objModel)
        {
            //Validate request
            var context = new GuardianAvionicsEntities();
            int ownerProfileId = 0;
            GeneralResponse response = new GeneralResponse();
            try
            {
                //Check the user profile exist or not.
                var profile = context.Profiles.FirstOrDefault(f => f.Id == objModel.ProfileId);
                if (profile == null)
                {
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.ProfileIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.ProfileIdNotFound.GetStringValue();
                    return response;
                }

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objModel.AircraftId);
                if (aircraft == null)
                {
                    response.ResponseCode = ((int)Enumerations.AircraftCodes.AircraftIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.AircraftCodes.AircraftIdNotFound.GetStringValue();
                    return response;
                }
                else
                {

                    ownerProfileId = aircraft.OwnerProfileId ?? 0;
                }

                //Now Insert the Details into notification table and send push notification to the aircraft owner
                var notification = context.Notifications.Create();
                notification.NotificationFrom = objModel.ProfileId;
                notification.NotificationTo = ownerProfileId;
                notification.Type = context.NotificationTypes.FirstOrDefault(f => f.Type == "AccessAircraftRequest").Id;
                notification.Status = context.NotificationStatus.FirstOrDefault(f => f.Status == "Pending").StatusId;
                string msg = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName + " wants to access the aircraft with tail Number - " + aircraft.Registration;
                notification.Message = msg;
                notification.AircraftId = aircraft.Id;
                notification.LastUpdateDate = DateTime.UtcNow;
                notification.CreateDate = DateTime.UtcNow;
                context.Notifications.Add(notification);

                //var objMappingowner = new MappingAircraftAndPilot()
                //{
                //    AircraftId = aircraft.Id,
                //    ProfileId = objModel.ProfileId,
                //    Deleted = false,
                //    CreateDate = DateTime.UtcNow,
                //    IsActive = false
                //};
                //context.MappingAircraftAndPilots.Add(objMappingowner);
                MappingAircraftAndPilot(aircraft.Id, objModel.ProfileId);

                aircraft.LastUpdated = DateTime.UtcNow;
                context.SaveChanges();

                var objPushNotification = context.PushNotifications.Where(p => p.ProfileId == ownerProfileId && p.IsValid && !string.IsNullOrEmpty(p.TokenId)).Select(p => p.TokenId).ToArray();
                if (objPushNotification.Any())
                {
                    IphonePushNotification obj = new IphonePushNotification();
                    foreach (var tokenId in objPushNotification)
                    {
                        obj.PushToiPhone(tokenId, msg, "NotificationList");
                    }
                }
                response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.AircraftCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.AircraftCodes.Exception.GetStringValue();
                return response;
            }
            return new GeneralResponse();
        }

        public NotificationResponse GetNotification(int profileId, string lastUpdateDate)
        {
            var context = new GuardianAvionicsEntities();
            var udate = Misc.GetDate(lastUpdateDate) ?? DateTime.MinValue;

            NotificationResponse objNotificationResponse = new NotificationResponse();
            objNotificationResponse.NotificationList = new List<NotificationModel>();

            try
            {
                var temp = context.Notifications.Where(w => w.NotificationTo == profileId && w.LastUpdateDate > udate).Select(s => new
                {
                    Id = s.Id,
                    Message = s.Message,
                    Status = s.Status,
                    Type = s.Type,
                    CreatedDate = s.CreateDate,
                    AircraftId = s.AircraftId
                }).ToList();

                temp.ForEach(s => objNotificationResponse.NotificationList.Add(new NotificationModel
                {

                    Id = s.Id,
                    Message = s.Message,
                    Status = s.Status ?? 0,
                    Type = s.Type ?? 0,
                    CreatedDate = Misc.GetStringOfDate(s.CreatedDate),
                    AircraftId = s.AircraftId
                }));
                objNotificationResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);
                objNotificationResponse.ResponseMessage = "Success";
                objNotificationResponse.ResponseCode = "0";
            }
            catch (Exception ex)
            {
                return new NotificationResponse
                {
                    NotificationList = new List<NotificationModel>(),
                    ResponseCode = "9999",
                    ResponseMessage = "Exception"
                };
            }

            return objNotificationResponse;
        }

        public UpdateNotificationStatusResponse UpdateNotificationStatus(UpdateNotificationStatusRequest objRequest)
        {
            var context = new GuardianAvionicsEntities();

            try
            {
                //Check notification exist or not
                var notification = context.Notifications.FirstOrDefault(f => f.Id == objRequest.NotificationId);
                if (notification == null)
                {
                    return new UpdateNotificationStatusResponse
                    {
                        LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow),
                        ResponseCode = ((int)Enumerations.NotificationCodes.InvalidNotificationId).ToString(),
                        ResponseMessage = Enumerations.NotificationCodes.InvalidNotificationId.GetStringValue()
                    };
                }

                var status = context.NotificationStatus.FirstOrDefault(f => f.StatusId == objRequest.StatusId && f.Status != "Pending");
                if (status == null)
                {
                    return new UpdateNotificationStatusResponse
                    {
                        LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow),
                        ResponseCode = ((int)Enumerations.NotificationCodes.InvalidStatusId).ToString(),
                        ResponseMessage = Enumerations.NotificationCodes.InvalidStatusId.GetStringValue()
                    };
                }

                notification.LastUpdateDate = DateTime.UtcNow;
                notification.Status = objRequest.StatusId;


                var newNotification = context.Notifications.Create();
                newNotification.NotificationFrom = null;
                newNotification.NotificationTo = notification.NotificationFrom;
                newNotification.Status = objRequest.StatusId;
                newNotification.Type = context.NotificationTypes.FirstOrDefault(f => f.Type == "AccessAircraftResponse").TypeId;
                string message = "Your request has been " + status.Status + " to access the aircraft with tail number - " + notification.AircraftProfile.Registration;
                newNotification.Message = message;
                newNotification.AircraftId = notification.AircraftId;
                newNotification.LastUpdateDate = DateTime.UtcNow;
                newNotification.CreateDate = DateTime.UtcNow;
                context.Notifications.Add(newNotification);


                var allStatus = context.NotificationStatus;

                string notificationStatus = allStatus.FirstOrDefault(f => f.StatusId == objRequest.StatusId).Status;

                var mappingAircraftAndPilots = context.MappingAircraftAndPilots.FirstOrDefault(f => f.AircraftId == notification.AircraftId && f.ProfileId == notification.NotificationFrom);

                if (mappingAircraftAndPilots != null)
                {
                    if (notificationStatus == "Approved")
                    {
                        mappingAircraftAndPilots.IsActive = true;
                    }
                    else
                    {
                        context.MappingAircraftAndPilots.Remove(mappingAircraftAndPilots);
                    }
                }

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == notification.AircraftId);
                if (aircraft != null)
                {
                    aircraft.LastUpdated = DateTime.UtcNow;
                }
                context.SaveChanges();

                try
                {
                    var objPushNotification = context.PushNotifications.Where(p => p.ProfileId == notification.NotificationFrom && p.IsValid && !string.IsNullOrEmpty(p.TokenId)).Select(p => p.TokenId).Distinct().ToArray();
                    if (objPushNotification.Any())
                    {
                        //PushNotificationIPhone obj = new PushNotificationIPhone();
                        //obj.Push(objPushNotification, message);

                        IphonePushNotification obj = new IphonePushNotification();
                        foreach (var tokenId in objPushNotification)
                        {
                            obj.PushToiPhone(tokenId, message, "NotificationList");
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {
                return new UpdateNotificationStatusResponse
                {
                    LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow),
                    ResponseCode = ((int)Enumerations.NotificationCodes.Exception).ToString(),
                    ResponseMessage = Enumerations.NotificationCodes.Exception.GetStringValue()
                };
            }
            return new UpdateNotificationStatusResponse
            {
                LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow),
                ResponseCode = ((int)Enumerations.NotificationCodes.Success).ToString(),
                ResponseMessage = Enumerations.NotificationCodes.Success.GetStringValue()
            };
        }

        public GenerateIssueResponseModel GenerateIssue(IssueModel objIssue)
        {
            var context = new GuardianAvionicsEntities();
            var response = new GenerateIssueResponseModel();
            GeneralResponse genRes = ValidateIssue(objIssue);

            if (genRes.ResponseMessage != Enumerations.AircraftIssue.Success.GetStringValue())
            {
                response.ResponseCode = genRes.ResponseCode;
                response.ResponseMessage = genRes.ResponseMessage;
                return response;
            }

            try
            {
                Issue addIssue = new Issue();
                addIssue.Title = objIssue.Title;
                addIssue.Assigner = objIssue.AssignerId;
                if (objIssue.AssigneeId != null)
                {
                    addIssue.Assignee = objIssue.AssigneeId;
                }
                addIssue.StatusId = context.IssueStatusMasters.FirstOrDefault(f => f.Status == "Open").Id;
                addIssue.CreateDate = DateTime.UtcNow;
                addIssue.UpdateDate = DateTime.UtcNow;
                addIssue.AircraftId = objIssue.AircraftId;
                addIssue.PriorityId = objIssue.PriorityId;
                context.Issues.Add(addIssue);

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objIssue.AircraftId);
                if (aircraft != null)
                {
                    aircraft.LastUpdateDateIssue = DateTime.UtcNow;
                }
                context.SaveChanges();

                response.Id = addIssue.Id;
                //Now send the push notification to the maintenence user

                if (objIssue.AssigneeId != null && aircraft != null)
                {
                    var userDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == objIssue.AssignerId);
                    string message = userDetails.FirstName + " " + userDetails.LastName + "  has assigned an issue on aircraft Tail " + aircraft.Registration + ", you can view complete detail about an issue by logging to " + ConfigurationReader.ServerUrlToResetPassword;
                    string IssueDetail = "<br /><p>Issue - </p><br /><p>" + objIssue.Title + "</p>";
                    var tokenIdList = context.PushNotifications.Where(f => f.ProfileId == objIssue.AssigneeId).Select(s => s.TokenId).Distinct().ToList();

                    IphonePushNotification objPush = new Common.IphonePushNotification();

                    foreach (var tokenId in tokenIdList)
                    {
                        objPush.PushToiPhone(tokenId, message, "");
                    }

                    try
                    {
                        var assignee = context.UserDetails.FirstOrDefault(f => f.ProfileId == objIssue.AssigneeId);
                        if (assignee != null)
                        {
                            new UserHelper().SendEmailToMaintenanceUser(assignee.FirstName + " " + assignee.LastName, message, assignee.Profile.EmailId, IssueDetail);
                        }
                    }
                    catch (Exception e)
                    {

                    }

                }
                return new GenerateIssueResponseModel
                {
                    Id = addIssue.Id,
                    ResponseCode = ((int)Enumerations.AircraftIssue.Success).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Success.GetStringValue()
                };
            }
            catch (Exception ex)
            {
                return new GenerateIssueResponseModel
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Exception).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Exception.GetStringValue()
                };
            }
        }

        public GeneralResponse ValidateIssue(IssueModel objIssue)
        {
            var context = new GuardianAvionicsEntities();

            if (string.IsNullOrEmpty(objIssue.Title))
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.TitleNotFound).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.TitleNotFound.GetStringValue(),
                };
            }

            var profile = context.Profiles.FirstOrDefault(f => f.Id == objIssue.AssignerId);
            if (profile == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.InvalidAssignerId).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.InvalidAssignerId.GetStringValue(),
                };

            }

            if (objIssue.AssigneeId != null)
            {

                profile = context.Profiles.FirstOrDefault(f => f.Id == objIssue.AssigneeId && f.UserTypeId == context.UserMasters.FirstOrDefault(u => u.UserType == "Maintenance").Id);
                if (profile == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.InvalidAssigneeId).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.InvalidAssigneeId.GetStringValue(),
                    };
                }
            }

            if (objIssue.AircraftId != null)
            {

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objIssue.AircraftId);
                if (aircraft == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.InvalidAircraftId).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.InvalidAircraftId.GetStringValue(),
                    };
                }
            }
            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.AircraftIssue.Success).ToString(),
                ResponseMessage = Enumerations.AircraftIssue.Success.GetStringValue(),
            };
        }

        public GeneralResponse AssignIssueToOtherUser(ReassignIssueRequestModel objRequest)
        {
            var response = new GeneralResponse();

            var context = new GuardianAvionicsEntities();

            var issue = context.Issues.FirstOrDefault(f => f.Id == objRequest.IssueId);

            #region ValidateIssue
            if (issue == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.InvalidIssueId).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.InvalidIssueId.GetStringValue(),
                };
            }

            var profile = context.Profiles.FirstOrDefault(f => f.Id == objRequest.Assigner);
            if (profile == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.InvalidAssignerId).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.InvalidAssignerId.GetStringValue(),
                };

            }

            profile = context.Profiles.FirstOrDefault(f => f.Id == objRequest.Assignee && f.UserTypeId == context.UserMasters.FirstOrDefault(u => u.UserType == "Maintenance").Id);
            if (profile == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.InvalidAssigneeId).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.InvalidAssigneeId.GetStringValue(),
                };
            }

            #endregion ValidateIssue

            try
            {
                issue.Assigner = objRequest.Assigner;
                issue.Assignee = objRequest.Assignee;

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == issue.AircraftId);
                aircraft.LastUpdateDateIssue = DateTime.UtcNow;

                context.SaveChanges();

                var tokenIdList = context.PushNotifications.Where(f => f.ProfileId == objRequest.Assignee).Select(s => s.TokenId).Distinct().ToList();

                IphonePushNotification objPush = new Common.IphonePushNotification();


                var userDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == objRequest.Assigner);
                string message = userDetails.FirstName + " " + userDetails.LastName + " has assign you an issue of the aircrat with tail No. - " + aircraft.Registration;

                foreach (var tokenId in tokenIdList)
                {
                    objPush.PushToiPhone(tokenId, message, "");
                }

            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Exception).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Exception.GetStringValue(),
                };
            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.AircraftIssue.Success).ToString(),
                ResponseMessage = Enumerations.AircraftIssue.Success.GetStringValue(),
            };

        }

        public GeneralResponse UpdateIssue(IssueModel objRequest)
        {
            var response = new GeneralResponse();
            var context = new GuardianAvionicsEntities();

            var issue = context.Issues.FirstOrDefault(f => f.Id == objRequest.Id);

            if (issue == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.InvalidIssueId).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.InvalidIssueId.GetStringValue(),
                };
            }

            var profile = new Profile();
            if (objRequest.ClosedById != null)
            {
                profile = context.Profiles.FirstOrDefault(f => f.Id == objRequest.ClosedById);
                if (profile == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.CloseByUserIdNotFound).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.CloseByUserIdNotFound.GetStringValue(),
                    };
                }

                if (!(profile.Id == issue.Assigner || profile.Id == issue.AircraftProfile.OwnerProfileId))
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.UserMustBeMaintenanceUserOrOwner).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.UserMustBeMaintenanceUserOrOwner.GetStringValue(),
                    };
                }
            }

            try
            {
                int? existingAssignee = null;
                existingAssignee = issue.Assignee;
                if (objRequest.ClosedById != null)
                {
                    issue.StatusId = context.IssueStatusMasters.FirstOrDefault(f => f.Status == "Closed").Id;
                }

                issue.ClosedBy = objRequest.ClosedById;
                //issue.Title = objRequest.Title;
                issue.Assignee = objRequest.AssigneeId;
                issue.PriorityId = objRequest.PriorityId;
                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == issue.AircraftId);
                aircraft.LastUpdateDateIssue = DateTime.UtcNow;

                if (objRequest.StatusId == (int)Enumerations.IssueStatus.Closed)
                {
                    var comment = context.IssueComments.FirstOrDefault(f => f.IssueId == objRequest.Id);
                    if (comment == null)
                    {
                        comment = new IssueComment();
                        comment.CommentBy = objRequest.ClosedById ?? 0;
                        comment.CreateDate = DateTime.UtcNow;
                        comment.IssueId = objRequest.Id;
                        context.IssueComments.Add(comment);
                    }
                    else
                    {
                        comment.CommentDetail = objRequest.Comment;
                    }
                }
                context.SaveChanges();

                var assignerUserDetails = new UserDetail();
                var assigneeDetail = new UserDetail();
                IphonePushNotification objPush = new Common.IphonePushNotification();
                var tokenIdList = new List<string>();
                string message = string.Empty;
                string IssueDetail = string.Empty;

                if (issue.Assigner == null)
                {
                    assignerUserDetails = null;
                }
                else
                {
                    assignerUserDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == issue.Assigner);
                }

                if (issue.Assignee == null)
                {
                    assigneeDetail = null;
                }
                else
                {
                    assigneeDetail = context.UserDetails.FirstOrDefault(f => f.ProfileId == issue.Assignee);
                }

                //Now check the issue is reassign to other maintainance user or not
                if (issue.Assignee != null)
                {
                    if (existingAssignee != issue.Assignee)
                    {
                        //issue assign to other maintainance user
                        message = assignerUserDetails.FirstName + " " + assignerUserDetails.LastName + " has assigned an issue on aircraft Tail " + issue.AircraftProfile.Registration + ", you can view complete detail about an issue by logging to " + ConfigurationReader.ServerUrlToResetPassword;
                        IssueDetail = "<br /><p>Issue - </p><br /><p>" + issue.Title + "</p>";
                        tokenIdList = context.PushNotifications.Where(f => f.ProfileId == issue.Assignee).Select(s => s.TokenId).Distinct().ToList();

                        foreach (var tokenId in tokenIdList)
                        {
                            objPush.PushToiPhone(tokenId, message, "");
                        }
                        new UserHelper().SendEmailToMaintenanceUser(assigneeDetail.FirstName + " " + assigneeDetail.LastName, message, assigneeDetail.Profile.EmailId, IssueDetail);
                    }
                }

                //Now check that the issue is closed or not
                if (issue.ClosedBy != null && issue.Assignee != null)
                {
                    //Now check that issue closed by aircraft owner or maintenance user
                    if (issue.ClosedBy == issue.Assigner)
                    {
                        //Issue closed By the owner of the aircraft 
                        //Now send push message and email to maintenance user

                        message = assignerUserDetails.FirstName + " " + assignerUserDetails.LastName + " has closed an issue of the aircrat with tail No. - " + issue.AircraftProfile.Registration;
                        IssueDetail = "<br /><p>Issue - </p><br /><p>" + issue.Title + "</p>";
                        tokenIdList = context.PushNotifications.Where(f => f.ProfileId == issue.Assignee).Select(s => s.TokenId).Distinct().ToList();

                        new UserHelper().SendEmailToMaintenanceUser(assigneeDetail.FirstName + " " + assigneeDetail.LastName, message, assigneeDetail.Profile.EmailId, IssueDetail, "Resolved aircraft Issue with tail No. - " + issue.AircraftProfile.Registration);
                    }
                    else
                    {
                        message = assigneeDetail.FirstName + " " + assigneeDetail.LastName + " has closed an issue of the aircrat with tail No. - " + issue.AircraftProfile.Registration;
                        IssueDetail = "<br /><p>Issue - </p><br /><p>" + issue.Title + "</p>";
                        tokenIdList = context.PushNotifications.Where(f => f.ProfileId == issue.Assigner).Select(s => s.TokenId).Distinct().ToList();

                        new UserHelper().SendEmailToMaintenanceUser(assignerUserDetails.FirstName + " " + assignerUserDetails.LastName, message, assignerUserDetails.Profile.EmailId, IssueDetail, "Resolved aircraft Issue with tail No. - " + issue.AircraftProfile.Registration);
                    }

                    foreach (var tokenId in tokenIdList)
                    {
                        objPush.PushToiPhone(tokenId, message, "");
                    }
                }

                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Success).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Success.GetStringValue(),
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Exception).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Exception.GetStringValue(),
                };
            }
        }

        public MaintenanceModelWeb GetSquwakListByAircraftId(int aircraftId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            MaintenanceModelWeb objMaintenanceModelWeb = new MaintenanceModelWeb();
            var objProfile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            var isMaintenanceUser = objProfile.UserMaster.UserType == "Maintenance" ? true : false;

            var tempList = context.Issues.Where(f => f.AircraftId == aircraftId).Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                AircraftId = s.AircraftId,
                AssignerId = s.Assigner,
                AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                AssigneeId = s.Assignee,
                AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                StatusId = s.StatusId,
                ClosedById = s.ClosedBy,
                ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                CreateDate = s.CreateDate,
                UpdateDate = s.UpdateDate,
                PriorityId = s.PriorityId,
                RegistrationNo = s.AircraftProfile.Registration,
                Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail,
                IsOwner = (s.AircraftProfile.OwnerProfileId == profileId)
            }).OrderByDescending(o => o.Id).ToList();

            objMaintenanceModelWeb.SquawkListModel = new SquawkListModelWeb();
            objMaintenanceModelWeb.SquawkListModel.UserType = objProfile.UserMaster.UserType;
            var aircraftProfile = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId && f.OwnerProfileId == profileId);
            objMaintenanceModelWeb.SquawkListModel.IsOwner = (aircraftProfile == null) ? false : true;

            objMaintenanceModelWeb.SquawkListModel.IssueList = new List<IssueModel>();
            tempList.ForEach(s => objMaintenanceModelWeb.SquawkListModel.IssueList.Add(new IssueModel
            {
                Id = s.Id,
                Title = s.Title,
                AircraftId = s.AircraftId,
                AssignerId = s.AssignerId,
                AssignerName = s.AssignerName,
                AssigneeId = s.AssigneeId,
                AssigneeName = s.AssigneeName,
                StatusId = s.StatusId,
                ClosedById = s.ClosedById,
                ClosedByName = s.ClosedByName,
                CreateDate = s.CreateDate.ToShortDateString(),
                UpdateDate = s.UpdateDate.ToShortDateString(),
                PriorityId = s.PriorityId,
                RegistrationNo = s.RegistrationNo,
                Comment = s.Comment,
                IsOwner = s.IsOwner
            }));


            if (objMaintenanceModelWeb.SquawkListModel.IssueList != null)
            {
                objMaintenanceModelWeb.SquawkListModel.IssueList = objMaintenanceModelWeb.SquawkListModel.IssueList.OrderByDescending(o => o.Id).ToList();
            }


            objMaintenanceModelWeb.SquawkListModel.aircraftList = context.AircraftProfiles.Where(f => f.OwnerProfileId == profileId).Select(s => new ListItems { text = s.Registration, value = s.Id }).ToList();
            objMaintenanceModelWeb.SquawkListModel.aircraftList.Add(new ListItems() { text = "Select", value = 0 });
            objMaintenanceModelWeb.SquawkListModel.aircraftList = objMaintenanceModelWeb.SquawkListModel.aircraftList.OrderBy(o => o.value).ToList();
            objMaintenanceModelWeb.SquawkListModel.aircraftIdForIssue = 0;

            objMaintenanceModelWeb.SquawkListModel.UserList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraftId && f.Profile.UserDetail != null && !string.IsNullOrEmpty(f.Profile.UserDetail.FirstName)).Select(s => new ListItems
            {
                text = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                value = s.Profile.Id
            }).ToList();
            objMaintenanceModelWeb.SquawkListModel.UserList.Add(new ListItems() { text = "Select", value = 0 });
            objMaintenanceModelWeb.SquawkListModel.UserList = objMaintenanceModelWeb.SquawkListModel.UserList.OrderBy(o => o.value).ToList();
            objMaintenanceModelWeb.SquawkListModel.userIdForIssue = 0;

            return objMaintenanceModelWeb;
        }

        //Call from API
        public SquawkListResponse GetSquawkList(int profileId, string lastUpdateDate = "")
        {

            SquawkListResponse response = new SquawkListResponse();
            var context = new GuardianAvionicsEntities();

            try
            {
                var udate = string.IsNullOrEmpty(lastUpdateDate) ? DateTime.MinValue : (Misc.GetDate(lastUpdateDate) ?? DateTime.MinValue);
                string userType = context.Profiles.FirstOrDefault(f => f.Id == profileId).UserMaster.UserType;


                var aircraftIdList = new List<int>();

                if (userType != "Maintenance")
                {
                    var mappedAircraftList = GetAllAircraftIdMappedWithProfile(profileId);
                    aircraftIdList = context.AircraftProfiles.Where(a => (mappedAircraftList.Contains(a.Id) || a.OwnerProfileId == profileId || context.PilotLogs.Where(p => p.PilotId == profileId || p.CoPilotId == profileId).Select(s => s.AircraftId).Distinct().ToList().Contains(a.Id)) && a.LastUpdateDateIssue >= udate).Select(s => s.Id).Distinct().ToList();

                    var tempList = context.Issues.Where(f => aircraftIdList.Contains(f.AircraftId ?? 0)).Select(s => new
                    {
                        Id = s.Id,
                        Title = s.Title,
                        AircraftId = s.AircraftId,
                        AssignerId = s.Assigner,
                        AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                        AssigneeId = s.Assignee,
                        AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                        StatusId = s.StatusId,
                        ClosedById = s.ClosedBy,
                        ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                        CreateDate = s.CreateDate,
                        UpdateDate = s.UpdateDate,
                        PriorityId = s.PriorityId,
                        RegistrationNo = s.AircraftProfile.Registration,
                        Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail,
                        IsOwner = (s.AircraftProfile.OwnerProfileId == profileId)
                    }).ToList();

                    response.SquawkList = new List<IssueModel>();
                    tempList.ForEach(s => response.SquawkList.Add(new IssueModel
                    {
                        Id = s.Id,
                        Title = s.Title,
                        AircraftId = s.AircraftId,
                        AssignerId = s.AssignerId,
                        AssignerName = s.AssignerName,
                        AssigneeId = s.AssigneeId,
                        AssigneeName = s.AssigneeName,
                        StatusId = s.StatusId,
                        ClosedById = s.ClosedById,
                        ClosedByName = s.ClosedByName,
                        CreateDate = s.CreateDate.ToShortDateString(),
                        UpdateDate = s.UpdateDate.ToShortDateString(),
                        PriorityId = s.PriorityId,
                        RegistrationNo = s.RegistrationNo,
                        Comment = s.Comment,
                        IsOwner = s.IsOwner
                    }));
                }
                else
                {
                    aircraftIdList = context.MappingMaintenanceUserAndAircrafts
                                      .Where(map => map.ProfileId == profileId)
                                      .Select(s => s.AircraftId)
                                      .ToList();
                    var tempList = context.Issues.Where(f => (f.Assignee == profileId) && f.UpdateDate >= udate).Select(s => new
                    //var tempList = context.Issues.Where(f => (f.Assignee == profileId || aircraftIdList.Contains(f.AircraftId ?? 0)) && f.UpdateDate >= udate).Select(s => new
                    {
                        Id = s.Id,
                        Title = s.Title,
                        AircraftId = s.AircraftId,
                        AssignerId = s.Assigner,
                        AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                        AssigneeId = s.Assignee,
                        AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                        StatusId = s.StatusId,
                        ClosedById = s.ClosedBy,
                        ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                        CreateDate = s.CreateDate,
                        UpdateDate = s.UpdateDate,
                        PriorityId = s.PriorityId,
                        RegistrationNo = s.AircraftProfile.Registration,
                        Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail,
                        IsOwner = (s.AircraftProfile.OwnerProfileId == profileId)
                    }).ToList();

                    response.SquawkList = new List<IssueModel>();
                    tempList.ForEach(s => response.SquawkList.Add(new IssueModel
                    {
                        Id = s.Id,
                        Title = s.Title,
                        AircraftId = s.AircraftId,
                        AssignerId = s.AssignerId,
                        AssignerName = s.AssignerName,
                        AssigneeId = s.AssigneeId,
                        AssigneeName = s.AssigneeName,
                        StatusId = s.StatusId,
                        ClosedById = s.ClosedById,
                        ClosedByName = s.ClosedByName,
                        CreateDate = s.CreateDate.ToShortDateString(),
                        UpdateDate = s.UpdateDate.ToShortDateString(),
                        PriorityId = s.PriorityId,
                        RegistrationNo = s.RegistrationNo,
                        Comment = s.Comment,
                        IsOwner = s.IsOwner
                    }));
                }

                response.ResponseCode = "0";
                response.ResponseMessage = "Success";
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception";
            }
            return response;
        }

        public GeneralResponse CloseIssueAPI(int profileId, int issueId, string comment)
        {
            var context = new GuardianAvionicsEntities();
            var issue = context.Issues.FirstOrDefault(f => f.Id == issueId);
            try
            {

                if (issue == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.InvalidIssueId).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.InvalidIssueId.GetStringValue()
                    };
                }

                if (string.IsNullOrEmpty(comment))
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.CommentCanNotBeEmpty).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.CommentCanNotBeEmpty.GetStringValue()
                    };
                }

                var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                if (profile == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.AircraftIssue.InvalidProfileId).ToString(),
                        ResponseMessage = Enumerations.AircraftIssue.InvalidProfileId.GetStringValue()
                    };
                }

                issue.ClosedBy = profileId;
                issue.StatusId = context.IssueStatusMasters.FirstOrDefault(f => f.Status == "Closed").Id;

                var objComment = context.IssueComments.Create();
                objComment.CommentBy = profileId;
                objComment.CreateDate = DateTime.UtcNow;
                objComment.IssueId = issueId;
                objComment.CommentDetail = comment;
                context.IssueComments.Add(objComment);
                context.SaveChanges();

                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == issue.AircraftId);
                aircraft.LastUpdateDateIssue = DateTime.UtcNow;

                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Success).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Success.GetStringValue()
                };
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.AircraftIssue.Exception).ToString(),
                    ResponseMessage = Enumerations.AircraftIssue.Exception.GetStringValue()
                };
            }
        }

        public MaintenanceModelWeb CloseIssue(int profileId, int issueId, string comment)
        {
            var context = new GuardianAvionicsEntities();
            var issue = context.Issues.FirstOrDefault(f => f.Id == issueId);
            List<IssueModel> response = new List<IssueModel>();

            try
            {
                issue.ClosedBy = profileId;
                issue.StatusId = context.IssueStatusMasters.FirstOrDefault(f => f.Status == "Closed").Id;

                var objComment = context.IssueComments.Create();
                objComment.CommentBy = profileId;
                objComment.CreateDate = DateTime.UtcNow;
                objComment.IssueId = issueId;
                objComment.CommentDetail = comment;
                context.IssueComments.Add(objComment);
                context.SaveChanges();
                return GetSquawkListTemp(profileId);
            }
            catch (Exception ex)
            {
                return GetSquawkListTemp(profileId);
            }
        }

        public MaintenanceModelWeb CloseIssueFromAircraftModule(int issueId, string comment, int aircraftId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var issue = context.Issues.FirstOrDefault(f => f.Id == issueId);

            try
            {
                issue.ClosedBy = profileId;
                issue.StatusId = context.IssueStatusMasters.FirstOrDefault(f => f.Status == "Closed").Id;

                var objComment = context.IssueComments.Create();
                objComment.CommentBy = profileId;
                objComment.CreateDate = DateTime.UtcNow;
                objComment.IssueId = issueId;
                objComment.CommentDetail = comment;
                context.IssueComments.Add(objComment);

                context.SaveChanges();
                return GetSquwakListByAircraftId(aircraftId, profileId);
            }
            catch (Exception ex)
            {
                return GetSquwakListByAircraftId(aircraftId, profileId);
            }
        }

        public void GetAircraftListByMaintenanceUserId(int profileId)
        {
            var context = new GuardianAvionicsEntities();
        }

        public List<int> GetAllAircraftIdMappedWithProfile(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            return context.MappingAircraftAndPilots
                                   .Where(
                                       map =>
                                           map.ProfileId == profileId && map.Deleted == false &&
                                           !map.Profile.Deleted)
                                   .Select(s => s.AircraftId)
                                   .ToList();
        }

        public MaintenanceModelWeb GetMaintenanceDetails(int aircraftId, int profileId)
        {
            var response = new MaintenanceModelWeb();
            var context = new GuardianAvionicsEntities();
            response.MaintenanceUserList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraftId).Select(s => new MaintenanceUserModel
            {
                EmailId = s.Profile.EmailId,
                Id = s.Profile.Id,
                FirstName = (s.Profile.UserDetail == null) ? "" : s.Profile.UserDetail.FirstName,
                LastName = (s.Profile.UserDetail == null) ? "" : s.Profile.UserDetail.LastName,
                IsRegister = (s.Profile.UserDetail == null) ? false : (string.IsNullOrEmpty(s.Profile.UserDetail.FirstName) ? false : true)
            }).ToList();
            if (response.MaintenanceUserList == null)
            {
                response.MaintenanceUserList = new List<MaintenanceUserModel>();
            }

            var issueList = context.Issues.Where(f => f.AircraftId == aircraftId);

            bool isMaintenanceUser = (context.Profiles.FirstOrDefault(f => f.Id == profileId).UserTypeId == (int)Enumerations.UserType.Maintenance);

            if (isMaintenanceUser)
            {
                issueList = issueList.Where(p => p.Assignee == profileId);
            }

            var tempIssue = issueList.Select(s => new
            {
                Id = s.Id,
                Title = s.Title,
                AircraftId = s.AircraftId,
                AssignerId = s.Assigner,
                AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                AssigneeId = s.Assignee,
                AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                StatusId = s.StatusId,
                ClosedById = s.ClosedBy,
                ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                CreateDate = (s.CreateDate),
                UpdateDate = (s.UpdateDate),
                RegistrationNo = s.AircraftProfile.Registration,
                IsOwner = (s.AircraftProfile.OwnerProfileId == profileId),
                PriorityId = s.PriorityId
            }).ToList();

            response.SquawkListModel = new SquawkListModelWeb();
            response.SquawkListModel.IssueList = new List<IssueModel>();
            response.SquawkListModel.userIdForIssue = 0;
            tempIssue.ForEach(s => response.SquawkListModel.IssueList.Add(new IssueModel
            {
                Id = s.Id,
                Title = s.Title,
                AircraftId = s.AircraftId,
                AssignerId = s.AssignerId,
                AssignerName = s.AssignerName,
                AssigneeId = s.AssigneeId,
                AssigneeName = s.AssigneeName,
                StatusId = s.StatusId,
                ClosedById = s.ClosedById,
                ClosedByName = s.ClosedByName,
                CreateDate = (s.CreateDate.ToShortDateString()),
                UpdateDate = (s.UpdateDate.ToShortDateString()),
                RegistrationNo = s.RegistrationNo,
                IsOwner = s.IsOwner,
                PriorityId = s.PriorityId
            }));

            response.SquawkListModel.aircraftList = context.AircraftProfiles.Where(f => f.OwnerProfileId == profileId).Select(s => new ListItems { text = s.Registration, value = s.Id }).ToList();
            response.SquawkListModel.aircraftList.Add(new ListItems() { text = "Select", value = 0 });
            response.SquawkListModel.aircraftList = response.SquawkListModel.aircraftList.OrderBy(o => o.value).ToList();
            response.SquawkListModel.aircraftIdForIssue = 0;

            response.SquawkListModel.UserList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraftId && f.Profile.UserDetail != null && !string.IsNullOrEmpty(f.Profile.UserDetail.FirstName) && !f.IsDeleted).Select(s => new ListItems
            {
                text = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                value = s.Profile.Id
            }).ToList();
            response.SquawkListModel.UserList.Add(new ListItems() { text = "Select", value = 0 });
            response.SquawkListModel.UserList = response.SquawkListModel.UserList.OrderBy(o => o.value).ToList();
            response.SquawkListModel.userIdForIssue = 0;
            response.SquawkListModel.UserType = context.Profiles.FirstOrDefault(f => f.Id == profileId).UserMaster.UserType;
            var aircraftProfile = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId && f.OwnerProfileId == profileId);
            response.SquawkListModel.IsOwner = (aircraftProfile == null) ? false : true;


            return response;
        }

        public MaintenanceModelWeb GetSquawkListTemp(int profileId)
        {
            var response = new MaintenanceModelWeb();
            response.SquawkListModel = new SquawkListModelWeb();
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            response.SquawkListModel.UserType = profile.UserMaster.UserType;
            var aircraftIdList = new List<int>();
            var issueList = context.Issues;
            if (response.SquawkListModel.UserType != "Maintenance")
            {
                var mappedAircraftList = GetAllAircraftIdMappedWithProfile(profileId);
                aircraftIdList = context.AircraftProfiles.Where(a => (mappedAircraftList.Contains(a.Id) || a.OwnerProfileId == profileId || context.PilotLogs.Where(p => p.PilotId == profileId || p.CoPilotId == profileId).Select(s => s.AircraftId).Distinct().ToList().Contains(a.Id))).Select(s => s.Id).Distinct().ToList();

                var tempList = context.Issues.Where(f => aircraftIdList.Contains(f.AircraftId ?? 0) || f.Assigner == profileId).Select(s => new
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.Assigner,
                    AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                    AssigneeId = s.Assignee,
                    AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedBy,
                    ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                    CreateDate = s.CreateDate,
                    UpdateDate = s.UpdateDate,
                    PriorityId = s.PriorityId,
                    RegistrationNo = s.AircraftProfile.Registration,
                    Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail,
                    IsOwner = (s.AircraftProfile.OwnerProfileId == profileId)
                }).ToList();

                response.SquawkListModel.IssueList = new List<IssueModel>();
                tempList.ForEach(s => response.SquawkListModel.IssueList.Add(new IssueModel
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.AssignerId,
                    AssignerName = s.AssignerName,
                    AssigneeId = s.AssigneeId,
                    AssigneeName = s.AssigneeName,
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedById,
                    ClosedByName = s.ClosedByName,
                    CreateDate = s.CreateDate.ToShortDateString(),
                    UpdateDate = s.UpdateDate.ToShortDateString(),
                    PriorityId = s.PriorityId,
                    RegistrationNo = s.RegistrationNo,
                    Comment = s.Comment,
                    IsOwner = s.IsOwner
                }));
            }
            else
            {
                aircraftIdList = context.MappingMaintenanceUserAndAircrafts
                                  .Where(map => map.ProfileId == profileId)
                                  .Select(s => s.AircraftId)
                                  .ToList();
                //var tempList = context.Issues.Where(f => (f.Assignee == profileId || aircraftIdList.Contains(f.AircraftId ?? 0))).Select(s => new
                var tempList = context.Issues.Where(f => (f.Assignee == profileId)).Select(s => new
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.Assigner,
                    AssignerName = s.AssignerProfile.UserDetail.FirstName + " " + s.AssignerProfile.UserDetail.LastName,
                    AssigneeId = s.Assignee,
                    AssigneeName = s.Assignee == null ? "" : (s.AssigneeProfile.UserDetail.FirstName + " " + s.AssigneeProfile.UserDetail.LastName),
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedBy,
                    ClosedByName = s.ClosedBy == null ? "" : (s.ClosedByProfile.UserDetail.FirstName + " " + s.ClosedByProfile.UserDetail.LastName),
                    CreateDate = s.CreateDate,
                    UpdateDate = s.UpdateDate,
                    PriorityId = s.PriorityId,
                    RegistrationNo = s.AircraftProfile.Registration,
                    Comment = s.IssueComments.FirstOrDefault() == null ? "" : s.IssueComments.FirstOrDefault().CommentDetail,
                    IsOwner = (s.AircraftProfile.OwnerProfileId == profileId)
                }).ToList();

                response.SquawkListModel.IssueList = new List<IssueModel>();
                tempList.ForEach(s => response.SquawkListModel.IssueList.Add(new IssueModel
                {
                    Id = s.Id,
                    Title = s.Title,
                    AircraftId = s.AircraftId,
                    AssignerId = s.AssignerId,
                    AssignerName = s.AssignerName,
                    AssigneeId = s.AssigneeId,
                    AssigneeName = s.AssigneeName,
                    StatusId = s.StatusId,
                    ClosedById = s.ClosedById,
                    ClosedByName = s.ClosedByName,
                    CreateDate = s.CreateDate.ToShortDateString(),
                    UpdateDate = s.UpdateDate.ToShortDateString(),
                    PriorityId = s.PriorityId,
                    RegistrationNo = s.RegistrationNo,
                    Comment = s.Comment,
                    IsOwner = s.IsOwner
                }));
            }

            if (response.SquawkListModel.IssueList != null)
            {
                response.SquawkListModel.IssueList = response.SquawkListModel.IssueList.OrderByDescending(o => o.Id).ToList();
            }

            response.SquawkListModel.aircraftList = context.AircraftProfiles.Where(a => aircraftIdList.Contains(a.Id)).Select(s => new ListItems { text = s.Registration, value = s.Id }).ToList();
            response.SquawkListModel.aircraftList.Add(new ListItems() { text = "Select", value = 0 });
            response.SquawkListModel.aircraftList = response.SquawkListModel.aircraftList.OrderBy(o => o.value).ToList();
            response.SquawkListModel.aircraftIdForIssue = 0;

            response.SquawkListModel.UserList = new List<ListItems>();
            response.SquawkListModel.UserList.Add(new ListItems() { text = "Select", value = 0 });
            response.SquawkListModel.userIdForIssue = 0;

            return response;
        }

        public List<ListItems> GetMaintenanceUserByAIrcraftId(int aircraftId, int profileId, out bool isOwner)
        {
            isOwner = false;
            var context = new GuardianAvionicsEntities();
            var response = context.MappingMaintenanceUserAndAircrafts.Where(f => f.AircraftId == aircraftId && f.Profile.UserDetail != null && !string.IsNullOrEmpty(f.Profile.UserDetail.FirstName)).Select(s => new ListItems
            {
                text = s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                value = s.Profile.Id
            }).ToList();
            response.Add(new ListItems() { text = "Select", value = 0 });
            response = response.OrderBy(o => o.value).ToList();
            var aircraftProfile = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId && f.OwnerProfileId == profileId);
            if (aircraftProfile != null)
            {
                isOwner = true;
            }
            return response;
        }

        public int GetFirstAircraftRegisteredByProfileId(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var aircraft = context.AircraftProfiles.OrderBy(o => o.Id).FirstOrDefault(f => f.OwnerProfileId == profileId);
            if (aircraft == null)
            {
                return 0;
            }
            return aircraft.Id;
        }



        public PassengerProLoginResponse PassengerProInFlightContent(int profileId)
        {
            PassengerProLoginResponse response = new PassengerProLoginResponse
            {
                AircraftAndIpassengerList = new List<AircraftAndIpassengerDetailModel>()
            };
            var context = new GuardianAvionicsEntities();
            try
            {
                Profile profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);

                if (profile.UserDetail != null)
                {
                    response.UserName = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;
                }

                IPassengerHelper iPassengerHelper = new IPassengerHelper();
                var aircraftList =
                           context.AircraftProfiles.Where(
                               a => context.MappingAircraftAndPilots
                                   .Where(
                                       map =>
                                           map.ProfileId == profile.Id && map.Deleted == false &&
                                           !map.Profile.Deleted)
                                   .Select(s => s.AircraftId)
                                   .ToList()
                                   .Contains(a.Id) && !a.Deleted).Select(s => new
                                   {
                                       AircraftImagePath = s.ImageUrl,
                                       TailNumber = s.Registration,
                                       Id = s.Id
                                   }).ToList();
                response.AircraftAndIpassengerList = aircraftList.Select(s => new AircraftAndIpassengerDetailModel
                {
                    TailNumber = s.TailNumber,
                    AircraftImagePath = String.IsNullOrEmpty(s.AircraftImagePath) ? "" : ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + s.AircraftImagePath,
                    IPassengerList = iPassengerHelper.GetIPassengerProMediaDetailsAPI(s.Id)
                }).ToList();
                response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue();

            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue();
            }
            return response;
        }


        //gp
        public GeneralResponse InsertAircraftDetails(FuelQuantityModel fuelDetails)
        {
            try
            {
                var response = new GeneralResponse();
                //var response = new FuelQuantityModel();// { fuelLevelFrequency = new List<FuelLevelFrequency>() };

                using (var context = new GuardianAvionicsEntities())
                {
                    var IsSerialNoExists = context.FuelDetails.Where(x => x.aircraftId == fuelDetails.aircraftId).ToList();
                    if (IsSerialNoExists.Count == 0)
                    {

                        if (fuelDetails.right != null)
                        {
                            FuelDetails fuel = new FuelDetails();
                            fuel.fuelsideType = "right";
                            fuel.aircraftId = fuelDetails.aircraftId;
                            fuel.fuelValue = fuelDetails.right.fuelValue;
                            context.FuelDetails.Add(fuel);
                            context.SaveChanges();
                            //List<FuelQuantityModel.Frequencies> levelright = new List<FuelQuantityModel.Frequencies>();
                            for (var i = 0; i < fuelDetails.right.frequencies.Count; i++)
                            {
                                //FuelQuantityModel.Frequencies getreturn = new FuelQuantityModel.Frequencies();
                                FuelLevelFrequency level = new FuelLevelFrequency();
                                level.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                level.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                level.frequency = fuelDetails.right.frequencies[i].frequency;
                                level.fuelType = fuel.fuelTypeId;
                                context.FuelLevelFrequency.Add(level);
                                context.SaveChanges();
                                //getreturn.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                //getreturn.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                //getreturn.frequency = fuelDetails.right.frequencies[i].frequency;
                                //levelright.Add(getreturn);
                            }
                            //response.right.frequencies = levelright;
                            //response.right.fuelValue = fuelDetails.right.fuelValue;

                        }
                        if (fuelDetails.left != null)
                        {
                            FuelDetails fuel = new FuelDetails();
                            fuel.fuelsideType = "left";
                            fuel.aircraftId = fuelDetails.aircraftId;
                            fuel.fuelValue = fuelDetails.left.fuelValue;
                            context.FuelDetails.Add(fuel);
                            context.SaveChanges();
                            //List<FuelQuantityModel.Frequencies> levelleft = new List<FuelQuantityModel.Frequencies>();
                            for (var i = 0; i < fuelDetails.left.frequencies.Count; i++)
                            {
                                //FuelQuantityModel.Frequencies getreturn = new FuelQuantityModel.Frequencies();
                                FuelLevelFrequency level = new FuelLevelFrequency();
                                level.gallonLevel = fuelDetails.left.frequencies[i].gallonLevel;
                                level.frequencyAtLevel = fuelDetails.left.frequencies[i].frequencyAtLevel;
                                level.frequency = fuelDetails.left.frequencies[i].frequency;
                                level.fuelType = fuel.fuelTypeId;
                                context.FuelLevelFrequency.Add(level);
                                context.SaveChanges();
                                //getreturn.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                //getreturn.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                //getreturn.frequency = fuelDetails.right.frequencies[i].frequency;
                                //levelleft.Add(getreturn);
                            }
                            //response.left.frequencies = levelleft;
                            //response.left.fuelValue = fuelDetails.left.fuelValue;
                        }
                    }
                    else
                    {
                        List<FuelDetails> fuelList = context.FuelDetails.Where(x => x.aircraftId == fuelDetails.aircraftId).ToList();
                        for (int j = 0; j < fuelList.Count; j++)
                        {
                            int fT = fuelList[j].fuelTypeId;
                            List<FuelLevelFrequency> DeleteFrequencies = context.FuelLevelFrequency.Where(x => x.fuelType == fT).ToList();
                            context.FuelLevelFrequency.RemoveRange(DeleteFrequencies);
                            context.SaveChanges();
                        }

                        if (fuelDetails.right != null)
                        {
                            FuelDetails fuel = context.FuelDetails.Where(x => x.aircraftId == fuelDetails.aircraftId && x.fuelsideType == "right").FirstOrDefault();
                            fuel.fuelsideType = "right";
                            fuel.aircraftId = fuelDetails.aircraftId;
                            fuel.fuelValue = fuelDetails.right.fuelValue;
                            context.SaveChanges();
                            //List<FuelQuantityModel.Frequencies> levelright = new List<FuelQuantityModel.Frequencies>();
                            for (var i = 0; i < fuelDetails.right.frequencies.Count; i++)
                            {
                                //FuelQuantityModel.Frequencies getreturn = new FuelQuantityModel.Frequencies();
                                FuelLevelFrequency level = new FuelLevelFrequency();
                                level.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                level.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                level.frequency = fuelDetails.right.frequencies[i].frequency;
                                level.fuelType = fuel.fuelTypeId;
                                context.FuelLevelFrequency.Add(level);
                                context.SaveChanges();
                                //getreturn.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                //getreturn.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                //getreturn.frequency = fuelDetails.right.frequencies[i].frequency;
                                //levelright.Add(getreturn);
                            }
                            //response.right.frequencies = levelright;
                            //response.right.fuelValue = fuelDetails.right.fuelValue;
                        }
                        if (fuelDetails.left != null)
                        {
                            FuelDetails fuel = context.FuelDetails.Where(x => x.aircraftId == fuelDetails.aircraftId && x.fuelsideType == "left").FirstOrDefault();
                            fuel.fuelsideType = "left";
                            fuel.aircraftId = fuelDetails.aircraftId;
                            fuel.fuelValue = fuelDetails.left.fuelValue;
                            context.SaveChanges();
                            //List<FuelQuantityModel.Frequencies> levelleft = new List<FuelQuantityModel.Frequencies>();
                            for (var i = 0; i < fuelDetails.left.frequencies.Count; i++)
                            {
                                // FuelQuantityModel.Frequencies getreturn = new FuelQuantityModel.Frequencies();
                                FuelLevelFrequency level = new FuelLevelFrequency();
                                level.gallonLevel = fuelDetails.left.frequencies[i].gallonLevel;
                                level.frequencyAtLevel = fuelDetails.left.frequencies[i].frequencyAtLevel;
                                level.frequency = fuelDetails.left.frequencies[i].frequency;
                                level.fuelType = fuel.fuelTypeId;
                                context.FuelLevelFrequency.Add(level);
                                context.SaveChanges();
                                //getreturn.gallonLevel = fuelDetails.right.frequencies[i].gallonLevel;
                                //getreturn.frequencyAtLevel = fuelDetails.right.frequencies[i].frequencyAtLevel;
                                //getreturn.frequency = fuelDetails.right.frequencies[i].frequency;
                                //levelleft.Add(getreturn);
                            }
                            //response.left.frequencies = levelleft;
                            //response.right.fuelValue = fuelDetails.right.fuelValue;
                        }


                    }
                    //response.aircraftId = fuelDetails.aircraftId;
                    var lastupdate = context.AircraftProfiles.Where(x => x.Id == fuelDetails.aircraftId).FirstOrDefault();
                    lastupdate.LastUpdated = DateTime.Now;
                    context.SaveChanges();
                }
                response.ResponseCode = ((int)Enumerations.AircraftCodes.Success).ToString();
                response.ResponseMessage = Enumerations.AircraftCodes.Success.GetStringValue();
                return response;
            }
            catch (Exception e)
            {
                string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(fuelDetails.aircraftId);
                string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                if (File.Exists(aircraftLogPath))
                {
                    using (StreamWriter sw = File.AppendText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - InsertAircraftDetails)");
                        sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(fuelDetails));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - InsertAircraftDetails)");
                        sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(fuelDetails));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                return new FuelQuantityModel().Exception();
            }

        }

        public FuelQuantityModel getfueldetail(int aircraftId)
        {
            try
            {
                int SNO = aircraftId;
                FuelQuantityModel model = new FuelQuantityModel();
                using (var context = new GuardianAvionicsEntities())
                {

                    var list = (from FD in context.FuelDetails
                                join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                where FD.aircraftId == SNO
                                select new { FD, FL }).ToList();
                    if (list.Count > 0)
                    {
                        List<FuelQuantityModel.Frequencies> mfListright = new List<FuelQuantityModel.Frequencies>();
                        FuelQuantityModel.Right a = new FuelQuantityModel.Right();
                        List<FuelQuantityModel.Frequencies> mfListleft = new List<FuelQuantityModel.Frequencies>();
                        for (var i = 0; i < list.Count; i++)
                        {
                            if (list[i].FD.fuelsideType.ToLower() == "right")
                            {
                                a.fuelValue = Convert.ToDouble(list[i].FD.fuelValue);
                                FuelQuantityModel.Frequencies mf = new FuelQuantityModel.Frequencies();
                                mf.frequencyAtLevel = Convert.ToInt16(list[i].FL.frequencyAtLevel);
                                mf.gallonLevel = Convert.ToDouble(list[i].FL.gallonLevel);
                                mf.frequency = Convert.ToDouble(list[i].FL.frequency);
                                mfListright.Add(mf);
                                a.frequencies = mfListright;
                            }
                            model.aircraftId = Convert.ToInt32(list[i].FD.aircraftId);
                        }
                        FuelQuantityModel.Left ab = new FuelQuantityModel.Left();
                        for (var i = 0; i < list.Count; i++)
                        {
                            if (list[i].FD.fuelsideType.ToLower() == "left")
                            {
                                ab.fuelValue = Convert.ToDouble(list[i].FD.fuelValue);

                                FuelQuantityModel.Frequencies mf = new FuelQuantityModel.Frequencies();
                                mf.frequencyAtLevel = Convert.ToInt16(list[i].FL.frequencyAtLevel);
                                mf.gallonLevel = Convert.ToDouble(list[i].FL.gallonLevel);
                                mf.frequency = Convert.ToDouble(list[i].FL.frequency);
                                mfListleft.Add(mf);
                                ab.frequencies = mfListleft;
                            }
                            model.aircraftId = Convert.ToInt32(list[i].FD.aircraftId);
                        }
                        model.right = a;
                        model.left = ab;
                    }
                    else
                    {
                        model = null;
                    }
                }

                return model;
            }
            catch (Exception e)
            {
                string aircraftLogFileName = new AircraftHelper().GetAircraftRegAndSerialNo(1);
                string aircraftLogPath = ConfigurationReader.AircraftLogFilePath + aircraftLogFileName + ".txt";
                if (File.Exists(aircraftLogPath))
                {
                    using (StreamWriter sw = File.AppendText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - InsertAircraftDetails)");
                        //sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(aircraftLogPath))
                    {
                        sw.WriteLine("<p>Date - " + DateTime.Now.ToString() + "</p>");
                        sw.WriteLine("Exception while Update aircraft (API - InsertAircraftDetails)");
                        sw.WriteLine("Request Data - " + Newtonsoft.Json.JsonConvert.SerializeObject(1));
                        sw.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));
                        sw.WriteLine("=====================================================================================");
                        sw.WriteLine("");
                    }
                }
                return new FuelQuantityModel().Exception();
            }
        }
    }
}