﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.DataLayer;
using GA.Common;
using GA.DataTransfer.Classes_for_Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace GA.ApplicationLayer
{
  public  class ErrorHelper
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ErrorLogConnection"];

        public void SerErrorLog(ErrorLogModel errorLogModel)
        {
            
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter();
            param[0].ParameterName = "EncryptRequest";
            param[0].SqlDbType = SqlDbType.NVarChar;
            param[0].Value = errorLogModel.EncryptRequest;

            param[1] = new SqlParameter();
            param[1].ParameterName = "JsonRequest";
            param[1].SqlDbType = SqlDbType.NVarChar;
            param[1].Value = errorLogModel.JsonRequest;

            param[2] = new SqlParameter();
            param[2].ParameterName = "RequestId";
            param[2].SqlDbType = SqlDbType.Int;
            param[2].Value = errorLogModel.RequestId;

            param[3] = new SqlParameter();
            param[3].ParameterName = "Response";
            param[3].SqlDbType = SqlDbType.NVarChar;
            param[3].Value = errorLogModel.Response;

            param[4] = new SqlParameter();
            param[4].ParameterName = "EncryptedResponse";
            param[4].SqlDbType = SqlDbType.NVarChar;
            param[4].Value = errorLogModel.EncryptedResponse;

            param[5] = new SqlParameter();
            param[5].ParameterName = "JsonResponse";
            param[5].SqlDbType = SqlDbType.NVarChar;
            param[5].Value = errorLogModel.JsonResponse;

            param[6] = new SqlParameter();
            param[6].ParameterName = "Module";
            param[6].SqlDbType = SqlDbType.NVarChar;
            param[6].Value = errorLogModel.Module;

            param[7] = new SqlParameter();
            param[7].ParameterName = "MethodName";
            param[7].SqlDbType = SqlDbType.NVarChar;
            param[7].Value = errorLogModel.MethodName;

            param[8] = new SqlParameter();
            param[8].ParameterName = "ServerName";
            param[8].SqlDbType = SqlDbType.NVarChar;
            param[8].Value = errorLogModel.ServerName;

            param[9] = new SqlParameter();
            param[9].ParameterName = "Date";
            param[9].SqlDbType = SqlDbType.DateTime;
            param[9].Value = DateTime.UtcNow;

            param[10] = new SqlParameter();
            param[10].ParameterName = "IsResolved";
            param[10].SqlDbType = SqlDbType.Bit;
            param[10].Value = 0;

            param[11] = new SqlParameter();
            param[11].ParameterName = "Remark";
            param[11].SqlDbType = SqlDbType.NVarChar;
            param[11].Value = errorLogModel.Remark;

            int result = SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "spSetErrorLog", param);
        }

    }
}
