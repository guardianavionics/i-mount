﻿using System.Activities.Statements;
using System.Web.Script.Serialization;
using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Threading;
using System.Web;
using EntityFramework.BulkInsert.Extensions;
using GA.CommonForParseDataFile;

//using SharpKml;
//using SharpKml.Dom;
using SharpKml.Base;
using SharpKml.Dom;
using Document = GA.DataLayer.Document;
using TimeSpan = System.TimeSpan;
using SharpKml.Engine;
//using NLog;
using System.Globalization;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using Microsoft.ApplicationBlocks.Data;


namespace GA.ApplicationLayer
{
    /// <summary>
    /// All the fuctions on JPI unit data for service and website
    /// </summary>
    public class UnitDataHelper
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        //private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Saves Jpi data in Database , data received in string form is converted to double.
        /// Key are mapped to proper units.
        /// </summary>
        /// <param name="key">Key received from device end</param>
        /// <param name="value">Value of that key</param>
        /// <param name="objJPIData">Jpi data object for which key is to be saved</param>
        private void SaveInDb(string key, string value, JPIUnitData objJPIData)
        {
            try
            {
                switch (key)
                {
                    case "CHT1":
                        {
                            objJPIData.Cht1 = Convert.ToDouble(value);
                        }
                        break;
                    case "CHT2":
                        {
                            objJPIData.Cht2 = Convert.ToDouble(value);
                        }
                        break;
                    case "CHT3":
                        {
                            objJPIData.Cht3 = Convert.ToDouble(value);
                        }
                        break;
                    case "CHT4":
                        {
                            objJPIData.Cht4 = Convert.ToDouble(value);
                        }
                        break;
                    case "CHT5":
                        {
                            objJPIData.Cht5 = Convert.ToDouble(value);
                        }
                        break;
                    case "CHT6":
                        {
                            objJPIData.Cht6 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT1":
                        {
                            objJPIData.Egt1 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT2":
                        {
                            objJPIData.Egt2 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT3":
                        {
                            objJPIData.Egt3 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT4":
                        {
                            objJPIData.Egt4 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT5":
                        {
                            objJPIData.Egt5 = Convert.ToDouble(value);
                        }
                        break;
                    case "EGT6":
                        {
                            objJPIData.Egt6 = Convert.ToDouble(value);
                        }
                        break;
                    case "TIT":
                        {
                            objJPIData.Tit = Convert.ToDouble(value);
                        }
                        break;
                    case "OIL-P":
                        {
                            objJPIData.OilP = Convert.ToDouble(value);
                        }
                        break;
                    case "OIL-T":
                        {
                            objJPIData.OilT = Convert.ToDouble(value);
                        }
                        break;
                    case "RPM":
                        {
                            objJPIData.RPM = Convert.ToDouble(value);
                        }
                        break;
                    case "MAP":
                        {
                            objJPIData.MAP = Convert.ToDouble(value);
                        }
                        break;
                    case "FF":
                        {
                            objJPIData.FF = Convert.ToDouble(value);
                        }
                        break;
                    case "OAT":
                        {
                            objJPIData.Oat = Convert.ToDouble(value);
                        }
                        break;

                    case "AMP":
                        {
                            objJPIData.AMP1 = Convert.ToDouble(value);
                        }
                        break;
                    case "FQL":
                        {
                            objJPIData.FQL = Convert.ToDouble(value);
                        }
                        break;
                    case "FQR":
                        {
                            objJPIData.FQR = Convert.ToDouble(value);
                        }
                        break;
                    case "BAT":
                        {
                            objJPIData.Volt1 = Convert.ToDouble(value);
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception Key =" + key + " Value =" + value + " " + Newtonsoft.Json.JsonConvert.SerializeObject(objJPIData), e);
            }
        }

        /// <summary>
        /// Converts date and time in String to object of DateTime
        /// </summary>
        /// <param name="date">Date in string</param>
        /// <param name="time">Time in string</param>
        /// <returns>Date in Date Time object</returns>
        public DateTime FormatedDate(string date, string time)
        {
            // format of date will be dd mm yy

            try
            {
                var dd = Convert.ToInt32(date.Substring(0, 2));
                dd = (dd > 31 || dd < 1) ? 1 : dd;

                var mm = Convert.ToInt32(date.Substring(2, 2));
                mm = (mm > 12 || mm < 1) ? 1 : mm;

                var yy = 0;
                if (date.Length == 6)
                {
                    yy = Convert.ToInt32(date.Substring(4, 2));
                    yy = (yy > 7999 || yy < 1) ? 1 : yy;
                    yy += 2000;
                }
                else
                {
                    yy = Convert.ToInt32(date.Substring(4, 4));

                }


                var h = Convert.ToInt32(time.Substring(0, 2));
                h = (h > 23 || h < 0) ? 1 : h;

                var m = Convert.ToInt32(time.Substring(2, 2));
                m = (m > 59 || m < 0) ? 1 : m;

                var s = Convert.ToInt32(time.Substring(4, 2));
                s = (s > 59 || s < 1) ? 1 : s;

                return new DateTime(yy, mm, dd, h, m, s);
            }
            catch (Exception)
            {
                return (DateTime)SqlDateTime.MinValue;
            }
        }

        public double CalculateLatitudeAndLongitude(double degree, double minute, string strDirection)
        {
            int latsign = 1;

            double second = 0.0;
            if (degree < 0)
            {
                latsign = -1;
            }
            else
            {
                latsign = 1;
            }
            double dd = (degree + (latsign * (minute / 60.0)) + (latsign * (second / 3600.0)));

            if (strDirection.ToUpper() == "S" || strDirection.ToUpper() == "W")
            {
                dd = dd * (-1);
            }
            return dd;
        }


        public GeneralResponse GetDemoFlightDataFile(Stream file)
        {
            if (File.Exists(ConfigurationReader.KmlFilePath + @"\log.txt"))
            {
                File.Delete(ConfigurationReader.KmlFilePath + @"\log.txt");
            }

            using (var fs = new FileStream(ConfigurationReader.KmlFilePath + @"\log.txt", FileMode.Create, FileAccess.Write))
            {
                file.CopyTo(fs);
            }

            new ParseDemoFlightData().ParseDataFiles();
            new UnitDataHelper().SaveKmlFileOnSeverDemo();
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }


        public GeneralResponse GetDataFile(Stream fileContent, int? aircraftId, int profileId, Int64 uniqueId, out string encryptedFileName)
        {
            encryptedFileName = "";
            try
            {
                string fileName = DateTime.UtcNow.Ticks.ToString();
                encryptedFileName = fileName + ".data";
                string EncryptedFilePath = ConfigurationReader.DataFilePath + fileName + ".data";
                string originalFilePath = ConfigurationReader.DataFilePath + "0" + fileName + ".data";
                //using (var fs = new FileStream(ConfigurationReader.DataFilePath + fileName + ".data", FileMode.Create, FileAccess.Write))
                using (var fs = new FileStream(EncryptedFilePath, FileMode.Create, FileAccess.Write))
                {
                    fileContent.CopyTo(fs);
                }

                using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
                {
                    aes.KeySize = 128;
                    aes.Key = System.Text.Encoding.UTF8.GetBytes(Constants.GlobalEncryptionKey);
                    byte[] iv = new byte[16];
                    aes.IV = iv;
                    ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                    using (FileStream plain = File.Open(originalFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        using (FileStream encrypted = File.Open(EncryptedFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            using (CryptoStream cs = new CryptoStream(plain, decryptor, CryptoStreamMode.Write))
                            {
                                Stream stream = new ConversionHelper().convertToByteArray(encrypted);

                                stream.CopyTo(cs);

                            }
                        }
                    }
                }
                ExceptionHandler.ReportError(new Exception(), "Aircraft Id = " + aircraftId.ToString() + " profileid = " + profileId.ToString() + " uniqueId = " + uniqueId.ToString() + Environment.NewLine);
                var context = new GuardianAvionicsEntities();
                var parseDataFile = new ParseDataFile();
                parseDataFile.FileName = "0" + fileName + ".data";
                parseDataFile.AircraftId = aircraftId;
                parseDataFile.StatusId = Convert.ToInt32(Enumerations.Status.Pending);
                parseDataFile.CreateDate = DateTime.UtcNow;
                parseDataFile.Remark = null;
                parseDataFile.ProfileId = profileId;
                parseDataFile.UniqueId = uniqueId;
                context.ParseDataFiles.Add(parseDataFile);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new GeneralResponse()
            {
                ResponseCode = ((int)Enumerations.GetDataFile.Success).ToString(),
                ResponseMessage = Enumerations.GetDataFile.Success.GetStringValue()
            };
        }

        public GeneralResponse GetAudioFile(Stream fileContent, string fileName)
        {
            try
            {
                //string filePath = ConfigurationReader.AudioFilePath + fileName + ".m4a";
                //using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                //{

                //    fileContent.CopyTo(fs);
                //}

                HttpMultipartParser parser = new HttpMultipartParser(fileContent, "audio");
                if (parser.Success)
                {
                    ExceptionHandler.ReportError(new Exception(), "Parser success");
                    //if (File.Exists(ConfigurationReader.AudioFilePath + parser.Filename))
                    //{
                    //    File.Delete(ConfigurationReader.AudioFilePath + parser.Filename);
                    //}
                    //File.WriteAllBytes(ConfigurationReader.AudioFilePath + parser.Filename, parser.FileContents);
                    bool result = new Misc().SaveFileToS3Bucket(parser.Filename, new MemoryStream(parser.FileContents), "AudioFile");
                    if (result == false)
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.GetAudioFile.Exception).ToString(),
                            ResponseMessage = Enumerations.GetAudioFile.Exception.GetStringValue()
                        };
                    }
                }
                else
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.GetAudioFile.ParserFailed).ToString(),
                        ResponseMessage = Enumerations.GetAudioFile.ParserFailed.GetStringValue()
                    };
                }
            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.GetAudioFile.Exception).ToString(),
                    ResponseMessage = Enumerations.GetAudioFile.Exception.GetStringValue()
                };
            }
            return new GeneralResponse()
            {
                ResponseCode = ((int)Enumerations.GetAudioFile.Success).ToString(),
                ResponseMessage = Enumerations.GetAudioFile.Success.GetStringValue()
            };
        }


        public void ParsingCommands(string storedDataCommand, long storedDataAircraftId)
        {
            #region ParseCommand
            string[] storeedDataArray = storedDataCommand.Split(',');
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            List<string> gprmcHeader = new List<string>() { "Time", "Latitude", "Longitude", "Speed", "Date" };

            var context = new GuardianAvionicsEntities();

            //string[] rawData = storedDataCommand.Split(new[] { "/n" }, StringSplitOptions.RemoveEmptyEntries);
            var aircraftProfile = context.AircraftProfiles.FirstOrDefault(a => a.Id == storedDataAircraftId);

            //foreach (var command in rawData)
            //{
            aircraftProfile.LastTransactionId = (storeedDataArray[0]);
            aircraftProfile.LastUpdated = DateTime.UtcNow;
            context.SaveChanges();
            string commandName = storeedDataArray[1];

            var pilotLog = context.PilotLogs.Where(p => p.AircraftId == storedDataAircraftId)
                                                  .OrderByDescending(p => p.Id)
                                                  .Include(p => p.PilotLogRawDatas)
                                                  .FirstOrDefault();
            switch (commandName)
            {
                case "$PGAVF":
                    {
                        //check the command recieved for the previous flight or new flight
                        var commandDataList = storedDataCommand.Split(',').ToList();
                        int lastIndex = commandDataList.Count - 1;
                        int index = commandDataList[lastIndex].IndexOf('*');
                        commandDataList[lastIndex] = commandDataList[lastIndex].Substring(0, index);

                        //check that the record is available for the aircraftid and flightId
                        int aircraftId = Convert.ToInt32(storedDataAircraftId);
                        int flightId = Convert.ToInt32(commandDataList[3]);
                        pilotLog = context.PilotLogs.Where(p => p.AircraftId == aircraftId && p.FlightId == flightId)
                                            .OrderByDescending(o => o.Id)
                                            .FirstOrDefault();
                        if (pilotLog == null)
                        {
                            //Here insert data into pilot logs
                            var pilotLogData = context.PilotLogs.Create();
                            pilotLogData.AircraftId = Convert.ToInt32(storedDataAircraftId);
                            pilotLogData.PilotId = commandDataList[4] == "default" ? (int?)null : Convert.ToInt32(commandDataList[4]);
                            pilotLogData.LastUpdated = Convert.ToDateTime("1/1/1990");
                            pilotLogData.Date = Convert.ToDateTime("1/1/1990");
                            pilotLogData.Deleted = false;
                            pilotLogData.Finished = false;
                            pilotLogData.IsSavedOnDropbox = false;
                            pilotLogData.CoPilotId = commandDataList[6] == "default" ? (int?)null : Convert.ToInt32(commandDataList[6]);
                            pilotLogData.FlightId = flightId;
                            var pilotLogRawData = context.PilotLogRawDatas.Create();
                            commandDataList.RemoveRange(0, 2);
                            var temp = string.Join(",", commandDataList);
                            pilotLogRawData.PGAVF = temp;

                            pilotLogData.PilotLogRawDatas.Add(pilotLogRawData);
                            context.PilotLogs.Add(pilotLogData);

                            context.SaveChanges();
                        }
                        else
                        {
                            //record already inserted this flight
                            pilotLog.AircraftId = Convert.ToInt32(storedDataAircraftId);
                            pilotLog.PilotId = Convert.ToInt32(commandDataList[4]);
                            pilotLog.LastUpdated = DateTime.UtcNow;
                            pilotLog.Deleted = false;
                            pilotLog.Finished = false;
                            pilotLog.IsSavedOnDropbox = false;
                            pilotLog.CoPilotId = Convert.ToInt32(commandDataList[6]);

                            var pilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                            commandDataList.RemoveRange(0, 2);
                            var temp = string.Join(",", commandDataList);
                            pilotLogRawData.PGAVF = temp;
                            context.SaveChanges();
                        }
                        break;
                    }
                case "$GPRMC":
                    {
                        if (pilotLog != null)
                        {
                            if (!pilotLog.Finished)
                            {
                                //Here we have to get all the commands which are previously saved in the PilotLogRawData by aircraftid
                                var oldPilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                                var oldGprmc = string.IsNullOrEmpty(oldPilotLogRawData.GPRMC) ? new List<string>() : oldPilotLogRawData.GPRMC.Split(',').ToList();
                                var oldJPIHD = string.IsNullOrEmpty(oldPilotLogRawData.JPIHD) ? (string.IsNullOrEmpty(aircraftProfile.JpiHeader) ? new List<string>() : aircraftProfile.JpiHeader.Split(',').ToList()) : oldPilotLogRawData.JPIHD.Split(',').ToList();
                                var oldJpiDT = string.IsNullOrEmpty(oldPilotLogRawData.JPIDT) ? new List<string>() : oldPilotLogRawData.JPIDT.Split(',').ToList();
                                var oldGpgga = string.IsNullOrEmpty(oldPilotLogRawData.GPGGA) ? new List<string>() : oldPilotLogRawData.GPGGA.Split(',').ToList();
                                dictionary.Clear();

                                if (oldGprmc.Count > 0)
                                {
                                    // dictionary.Add("Time", oldGprmc[0]);
                                    dictionary.Add("Latitude", oldGprmc[2]);
                                    dictionary.Add("Longitude", oldGprmc[3]);
                                    dictionary.Add("Speed", oldGprmc[4]);
                                    dictionary.Add("Date", FormatedDate(oldGprmc[6], oldGprmc[0]).ToString());

                                    if (pilotLog.Date.ToShortDateString() == "1/1/1990")
                                    {
                                        pilotLog.Date = FormatedDate(oldGprmc[4], oldGprmc[0]);
                                        pilotLog.LastUpdated = pilotLog.Date;
                                    }
                                }

                                if (oldJpiDT.Count > 0 && oldJPIHD.Count > 0)
                                {
                                    if (oldJPIHD.Count == oldJpiDT.Count)
                                    {
                                        for (int i = 0; i < oldJPIHD.Count; i++)
                                        {
                                            dictionary.Add(oldJPIHD[i], oldJpiDT[i]);
                                        }
                                    }
                                }

                                if (oldGpgga.Count > 0)
                                {
                                    dictionary.Add("Altitude", oldGpgga[8]);
                                }

                                if (dictionary.Count > 0)
                                {
                                    //Insert json data in Airframe datalog
                                    var airframeData = context.AirframeDatalogs.Create();
                                    airframeData.DataLog = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                    pilotLog.AirframeDatalogs.Add(airframeData);
                                    dictionary.Clear();
                                }

                                oldPilotLogRawData.GPRMC = string.Empty;
                                oldPilotLogRawData.JPIDT = string.Empty;
                                oldPilotLogRawData.GPGGA = string.Empty;

                                var pilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                                var newGRRMC = storedDataCommand.Split(',').ToList();

                                if (newGRRMC.Count != 14)
                                {
                                    context.SaveChanges();
                                    break;
                                }

                                int lastIndex = newGRRMC.Count - 1;
                                int index = newGRRMC[lastIndex].IndexOf('*');
                                newGRRMC[lastIndex] = newGRRMC[lastIndex].Substring(0, index);
                                newGRRMC.RemoveRange(0, 2);

                                //Now update the Day Pic. Select the First and Last Airframe datalog
                                var firstAirframeDatalog = pilotLog.AirframeDatalogs.OrderBy(o => o.Id).FirstOrDefault();
                                var lastAirframeDatalog = pilotLog.AirframeDatalogs.OrderByDescending(o => o.Id).FirstOrDefault();

                                if (firstAirframeDatalog != null && lastAirframeDatalog != null)
                                {
                                    var startDate = "";
                                    var endDate = "";
                                    var dictonaryFirstLogData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(firstAirframeDatalog.DataLog);
                                    dictonaryFirstLogData.TryGetValue("Date", out startDate);

                                    var dictonaryLastLogData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(lastAirframeDatalog.DataLog);
                                    dictonaryLastLogData.TryGetValue("Date", out endDate);
                                    if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && (Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).Ticks > 0)
                                    {
                                        var datetimediff = new DateTime((Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).Ticks);
                                        pilotLog.DayPIC = datetimediff.ToString("HH:mm:ss");
                                    }
                                }

                                //calculate the Latitude and longitude
                                double degree = Convert.ToDouble(newGRRMC[2].Substring(0, 3));
                                double minute = Convert.ToDouble(newGRRMC[2].Substring(3, newGRRMC[2].Length - 3));
                                string direction = newGRRMC[3];
                                double latitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                                degree = Convert.ToDouble(newGRRMC[4].Substring(0, 2));
                                minute = Convert.ToDouble(newGRRMC[4].Substring(2, newGRRMC[4].Length - 2));
                                direction = newGRRMC[5];
                                double longitude = CalculateLatitudeAndLongitude(degree, minute, direction);

                                newGRRMC[2] = latitude.ToString();
                                newGRRMC[4] = longitude.ToString();

                                newGRRMC.RemoveAt(5); // remove the longitude Direction
                                newGRRMC.RemoveAt(3); // remove the latitude direction

                                if (pilotLog.Date.ToShortDateString() == "1/1/1990")
                                {
                                    pilotLog.Date = FormatedDate(newGRRMC[6], newGRRMC[0]);
                                }

                                pilotLogRawData.GPRMC = string.Join(",", newGRRMC);

                            }
                            //Check if there is a data in dictionary
                            context.SaveChanges();
                        }
                        break;
                    }
                case "$JPIHD":
                    {
                        var jpiHeader = storedDataCommand.Split(',').ToList();
                        jpiHeader.RemoveAt(jpiHeader.Count - 1);
                        jpiHeader.RemoveRange(0, 2);

                        if (string.IsNullOrEmpty(aircraftProfile.JpiHeader))
                        {
                            aircraftProfile.JpiHeader = string.Join(",", jpiHeader);
                            context.SaveChanges();
                        }

                        if (pilotLog != null)
                        {
                            var pilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                            if (!pilotLog.Finished)
                            {
                                pilotLogRawData.JPIHD = string.Join(",", jpiHeader);
                                context.SaveChanges();
                            }
                        }
                        break;
                    }
                case "$JPIDT":
                    {
                        if (pilotLog != null)
                        {
                            var pilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                            var jpiData = storedDataCommand.Split(',').ToList();

                            jpiData.RemoveAt(jpiData.Count - 1);
                            jpiData.RemoveRange(0, 2);

                            //check that the flight is finished or not
                            if (!pilotLog.Finished)
                            {
                                pilotLogRawData.JPIDT = string.Join(",", jpiData);
                                context.SaveChanges();
                            }
                        }
                        //dictionary.Add();
                        break;
                    }
                case "$GPGGA":
                    {
                        if (pilotLog != null)
                        {
                            var pilotLogRawData = pilotLog.PilotLogRawDatas.FirstOrDefault();
                            var gpgga = storedDataCommand.Split(',').ToList();

                            if (gpgga.Count != 16)
                            {
                                //logger.Fatal("Command Reject : Incorrect GPGGA command = " + storedDataCommand + ". Incorrect columns count");
                                break;
                            }

                            //int lastIndex = gpgga.Count - 1;
                            //int index = gpgga[lastIndex].IndexOf('*');
                            //gpgga[lastIndex] = gpgga[lastIndex].Substring(0, index - 1);
                            gpgga.RemoveRange(0, 2);

                            //check that the flight is finished or not
                            if (!pilotLog.Finished)
                            {
                                pilotLogRawData.GPGGA = string.Join(",", gpgga);
                                context.SaveChanges();
                            }
                        }
                        break;
                    }
            }
            //  }
            #endregion ParseCommand
        }

        /// <summary>
        /// Gets data in string form from device end , parses the data and saves in database.
        /// </summary>
        /// <param name="dataAsString">Object of FileuploadRequestModel </param>
        /// <returns>Object of FileUploadResponseModel , with message for success or failure</returns>
        public FileUploadResponseModel UploadStringData(FileUploadRequestModel dataAsString)
        {
            var context = new GuardianAvionicsEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            var respnse = new FileUploadResponseModel();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new FileUploadResponseModel
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                string dataStream = dataAsString.DataAsString;
                string[] dataArray = dataStream.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                var headerData = dataArray[0].Split(',');

                var profileId = Convert.ToInt32(headerData[3]);
                var profile = context.Profiles.FirstOrDefault(p => p.Id == profileId && !p.Deleted);

                #region validations

                if (profile == default(Profile))
                {
                    // return error
                    respnse.ResponseCode = ((int)Enumerations.GetPreference.ProfileIdNotFound).ToString();
                    respnse.ResponseMessage = Enumerations.GetPreference.ProfileIdNotFound.GetStringValue();
                    return respnse;
                }

                if (dataAsString.PilotLogId == default(int))
                {
                    respnse.ResponseCode = ((int)Enumerations.Logbook.LoogBookTagAndIdNotFound).ToString();
                    respnse.ResponseMessage = Enumerations.Logbook.LoogBookTagAndIdNotFound.GetStringValue();
                    return respnse;
                }

                var pilotLog = context.PilotLogs.FirstOrDefault(pl => pl.Id == dataAsString.PilotLogId);

                if (pilotLog == default(PilotLog))
                {
                    respnse.ResponseCode = ((int)Enumerations.Logbook.LoogBookTagAndIdNotFound).ToString();
                    respnse.ResponseMessage = Enumerations.Logbook.LoogBookTagAndIdNotFound.GetStringValue();
                    return respnse;
                }

                if (pilotLog.PilotId != profile.Id)
                {
                    respnse.ResponseCode = ((int)Enumerations.Logbook.LoogBookTagAndIdNotFound).ToString();
                    respnse.ResponseMessage = Enumerations.Logbook.LoogBookTagAndIdNotFound.GetStringValue();
                    return respnse;
                }

                #endregion validations

                #region save unit data object

                UnitData unitData = context.UnitDatas.Create();//new UnitData();

                unitData.UnitSerialNumber = headerData[0];
                unitData.AccountNumber = headerData[1];
                unitData.AircraftId = Convert.ToInt32(headerData[2]);

                unitData.ProfileId = profileId;
                unitData.LastUpdated = DateTime.UtcNow;
                unitData.PilotLogId = dataAsString.PilotLogId;

                #endregion save unit data object

                string[] JPIHeaderArray = new string[100], JPIDataArray, TempArray;

                #region for loop

                var jpiList = new List<JPIUnitData>();

                // parse all the dataArray rows and save geo data and jpi data
                for (long i = 1; i < dataArray.LongLength; i++)
                {
                    var objJPIData = new JPIUnitData();

                    // split each row by #  to get geo data , some fileds of jpi data ; header ; data
                    string[] subArray = dataArray[i].Split('#');

                    try
                    {
                        // each row of subArray has profile data , JPIHeader and JPIData.
                        foreach (var element in subArray)
                        {
                            // split each element by ',' to get individual values.
                            TempArray = element.Split(',');

                            if (TempArray[0] == "JPIHD")
                            {
                                JPIHeaderArray = TempArray;
                            }
                            else if (TempArray[0] == "JPIDT")
                            {
                                // save all the JPI data according to the JPI header
                                JPIDataArray = TempArray;

                                for (int j = 1; j < JPIDataArray.Length; j++)
                                    SaveInDb(JPIHeaderArray[j], JPIDataArray[j], objJPIData);

                                jpiList.Add(objJPIData);
                            }
                            else
                            {
                                // save GeoData
                                var date = FormatedDate(TempArray[0], TempArray[1]);
                                objJPIData.DateTime = date;
                                objJPIData.Latitude = Convert.ToDouble(TempArray[3]);
                                objJPIData.Longitude = Convert.ToDouble(TempArray[4]);
                                objJPIData.WayPoint = TempArray[2];
                                objJPIData.GpsAltitude = Convert.ToDouble(TempArray[5]);
                                objJPIData.GndSpd = Convert.ToDouble(TempArray[6]);

                                // save jpi data
                                objJPIData.VSpd = Convert.ToDouble(TempArray[7]);
                                objJPIData.Pitch = Convert.ToDouble(TempArray[8]);
                                objJPIData.Roll = Convert.ToDouble(TempArray[9]);
                                objJPIData.YAW = Convert.ToDouble(TempArray[10]);
                            }
                        }// foreach (var element in subArray)
                    }
                    catch (Exception e)
                    {
                        //ExceptionHandler.ReportError( new Exception(Newtonsoft.Json.JsonConvert.SerializeObject(dataArray[i])) ,"data row.");
                        //logger.Fatal("Exception data row " + Newtonsoft.Json.JsonConvert.SerializeObject(dataAsString), e);
                        //ExceptionHandler.ReportError(e);
                    }
                }

                var jpiUnitDataList = jpiList.AsEnumerable();

                foreach (var objJpiData in jpiUnitDataList)
                {
                    unitData.JPIUnitDatas.Add(objJpiData);
                }

                context.UnitDatas.Add(unitData);
                context.SaveChanges();

                #endregion for loop

                var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);

                #region dropbox
                try
                {
                    if (dataAsString.IsLastDataForFlight && preference != default(Preference) && preference.IsDropboxSync == true)
                    {
                        // upload on dropbox

                        var pthred = new ParameterizedThreadStart(DropbBoxParameteres);
                        var th = new Thread(pthred);

                        var logbook = new DropboxParameterisedThread
                        {
                            ProfileId = profileId,
                            Id = dataAsString.PilotLogId,
                            identity = System.Security.Principal.WindowsIdentity.GetCurrent(),
                        };

                        GC.KeepAlive(pthred);
                        GC.KeepAlive(th);

                        th.Start(logbook);

                    }
                }
                catch (Exception e)
                {
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(dataAsString), e);
                    //ExceptionHandler.ReportError(e);
                }

                #endregion dropbox

                context.SaveChanges();

                respnse.ResponseCode = ((int)Enumerations.GetPreference.Success).ToString();
                respnse.ResponseMessage = Enumerations.GetPreference.Success.GetStringValue();
                return respnse;
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex, Newtonsoft.Json.JsonConvert.SerializeObject(dataAsString));
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(dataAsString), ex);
                return new FileUploadResponseModel
                {
                    ResponseCode = ((int)Enumerations.GetPreference.Exception).ToString(),
                    ResponseMessage = Enumerations.GetPreference.Exception.GetStringValue()
                };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        private void DropbBoxParameteres(object obj)
        {
            var logbook = (DropboxParameterisedThread)obj;
            try
            {
                UpdateDropBox(logbook.Id, logbook.ProfileId, new GuardianAvionicsEntities(), logbook.identity);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(obj), e);
            }
        }

        /// <summary>
        /// Fetches JPI data for pilot log and converts it into .xlsx sheets and saves on dropbox
        /// </summary>
        /// <param name="pilotLogId">Pilotlog's Id ,of pilot log to be saved on dropbox</param>
        /// <param name="profileId">User's profile Id</param>
        /// <param name="context">Database of Project</param>
        public void UpdateDropBox(int pilotLogId, int profileId, GuardianAvionicsEntities context,
            System.Security.Principal.WindowsIdentity identity)
        {

            identity.Impersonate();
            try
            {
                # region select JPI data as Enumerable collection

                var count = context.JPIUnitDatas.Count(jd => jd.UnitData.ProfileId == profileId && jd.UnitData.PilotLogId == pilotLogId);
                if (count < 1)
                { return; }

                var list = context.JPIUnitDatas.Where(jd => jd.UnitData.ProfileId == profileId && jd.UnitData.PilotLogId == pilotLogId);

                var collection = list.AsEnumerable().Select(s => new JPIDataExcelSheets
                {
                    Date = Misc.GetStringOnlyDate(s.DateTime),
                    Time = s.DateTime.HasValue ? s.DateTime.Value.ToString("hh:mm:ss") : string.Empty,
                    utcOfst = Misc.GetStringOnlyDate(s.UTCOfst),
                    WayPoint = s.WayPoint ?? string.Empty,
                    Latitude = Convert.ToDouble(s.Latitude),
                    Longitude = Convert.ToDouble(s.Longitude),
                    AltB = Convert.ToString(s.AltB),
                    BaroA = Convert.ToString(s.BravoA),
                    AltMSL = Convert.ToString(s.AltMSL),
                    Oat = Convert.ToString(s.Oat),
                    IAS = Convert.ToString(s.IAS),
                    GndSpd = Convert.ToString(s.GndSpd),
                    VSpd = Convert.ToString(s.VSpd),
                    Pitch = Convert.ToString(s.Pitch),
                    Roll = Convert.ToString(s.Roll),
                    LatAc = Convert.ToString(s.LatAc),
                    NormAc = Convert.ToString(s.NormAc),
                    HDG = Convert.ToString(s.HDG),
                    TRK = Convert.ToString(s.TRK),

                    Volt1 = Convert.ToString(s.Volt1),
                    Volt2 = Convert.ToString(s.Volt2),

                    AMP1 = Convert.ToString(s.AMP1),
                    AMP2 = Convert.ToString(s.AMP2),

                    FQL = Convert.ToString(s.FQL),
                    FQR = Convert.ToString(s.FQR),

                    FF = (s.FF == null ? "" : Convert.ToString(s.FF)),
                    OilP = s.OilP.ToString(),
                    OilT = s.OilT.ToString(),

                    MAP = (s.MAP == null ? "" : Convert.ToString(s.MAP)),
                    RPM = (s.RPM == null ? "" : Convert.ToString(s.RPM)),

                    Cht1 = (s.Cht1 == null ? "" : Convert.ToString(s.Cht1)),
                    Cht2 = (s.Cht2 == null ? "" : Convert.ToString(s.Cht2)),
                    Cht3 = (s.Cht3 == null ? "" : Convert.ToString(s.Cht3)),
                    Cht4 = (s.Cht4 == null ? "" : Convert.ToString(s.Cht4)),
                    Cht5 = (s.Cht5 == null ? "" : Convert.ToString(s.Cht5)),
                    Cht6 = (s.Cht6 == null ? "" : Convert.ToString(s.Cht6)),
                    Egt1 = (s.Egt1 == null ? "" : Convert.ToString(s.Egt1)),
                    Egt2 = (s.Egt2 == null ? "" : Convert.ToString(s.Egt2)),
                    Egt3 = (s.Egt3 == null ? "" : Convert.ToString(s.Egt3)),
                    Egt4 = (s.Egt4 == null ? "" : Convert.ToString(s.Egt4)),
                    Egt5 = (s.Egt5 == null ? "" : Convert.ToString(s.Egt5)),
                    Egt6 = (s.Egt6 == null ? "" : Convert.ToString(s.Egt6)),

                    Tit = (s.Tit == null ? "" : Convert.ToString(s.Tit)),
                    GpsAltitude = (s.GpsAltitude == null ? "" : Convert.ToString(s.GpsAltitude)),
                    TAS = Convert.ToString(s.TAS),

                    HSIS = Convert.ToString(s.HSIS),
                    CRS = Convert.ToString(s.CRS),
                    NAV1 = Convert.ToString(s.NAV1),
                    NAV2 = Convert.ToString(s.NAV2),
                    COM1 = Convert.ToString(s.COM1),
                    COM2 = Convert.ToString(s.COM2),
                    HCDI = Convert.ToString(s.HCDI),

                    VCDI = Convert.ToString(s.VCDI),
                    WndSpd = Convert.ToString(s.WndSpd),

                    WndDr = Convert.ToString(s.WndDr),

                    WptDst = Convert.ToString(s.WptDst),
                    WptBrg = Convert.ToString(s.WptBrg),

                    MagVar = Convert.ToString(s.MagVar),
                    AfcsOn = Convert.ToString(s.AfcsOn),

                    RollM = Convert.ToString(s.RollM),
                    PitchM = Convert.ToString(s.PitchM),

                    RollC = Convert.ToString(s.RollC),
                    PichC = Convert.ToString(s.PitchC),

                    VSpdG = Convert.ToString(s.VSpdG),
                    GPSfix = Convert.ToString(s.GPSfix),
                    HAL = Convert.ToString(s.HAL),
                    VAL = Convert.ToString(s.VAL),

                    HPLwas = Convert.ToString(s.HPLwas),
                    HPLfd = Convert.ToString(s.HPLft),

                    VPLwas = Convert.ToString(s.VPLwas),

                }).ToList();
                #endregion

                if (collection == null || collection == default(List<JPIDataExcelSheets>))
                    return;

                var pilotlog = context.PilotLogs.Include(a => a.AircraftProfile).FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);

                if (pilotlog != default(PilotLog) && userDetails != default(UserDetail))
                {
                    // fetch other details for file from db.
                    var flightNumber = context.PilotLogs.Count(p => p.Id < pilotLogId
                                                                 && p.PilotId == profileId
                                                                 && !p.Deleted);

                    // ReSharper disable once PossibleNullReferenceException
                    var fileName =
                        GetFlightName(pilotlog.AircraftProfile.Registration,
                        flightNumber, userDetails.FirstName, userDetails.LastName, pilotlog.Date);

                    //ExceptionHandler.ReportError(new Exception(fileName), "File name of dropbox file upload .                    ..          .");
                    //logger.Fatal("Exception pilotLogId = " + pilotLogId + " , profileId =" + profileId);

                    // generate excel sheets and save on machine

                    //var isSuccess = DumpExcel(collection, pilotlog.AircraftProfile.Registration, pilotlog.AircraftProfile.AircraftType,
                    //    pilotlog.AircraftProfile.AeroUnitNo, userDetails.FirstName, fileName,"",identity);

                    var isSuccess = false;
                    if (!isSuccess)
                        return;


                    // upload on dropbox

                    // check proper credentials
                    var userPreference =
                        context.Preferences.FirstOrDefault(ud => ud.ProfileId == profileId);

                    if (userPreference == default(Preference) || userPreference.IsDropboxSync != true)
                    {
                        // delete file and return
                        var fpath = ConfigurationReader.DropboxExcelSheetPath + @"\" + fileName;
                        try
                        {
                            if (File.Exists(fpath))
                                File.Delete(fpath);
                        }
                        catch (Exception e)
                        {
                            //ExceptionHandler.ReportError(e);
                            //logger.Fatal("Exception pilotLogId = " + pilotLogId + " , profileId =" + profileId, e);
                        }
                        // return as not allowed to upload on dropbox
                        return;
                    }

                    // save file on user's dropbox
                    var filepath = ConfigurationReader.DropboxExcelSheetPath + @"\" + fileName;

                    //new DropboxFileUpload().UploadFileDropbox("g75o0ap9wsprg6cw", "pis7zov6smj7j6n", @"C:\GALogs\07-13-2014.txt", @"07-13-2014.txt");


                    var response = new DropboxFileUpload().UploadFileDropbox(userPreference.DropBoxToken, userPreference.DropBoxTokenSecret, filepath, fileName);

                    // update database
                    if (response.ToLower() == Boolean.TrueString.ToLower())
                    {
                        var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId);

                        if (plog != default(PilotLog))
                        {
                            plog.IsSavedOnDropbox = true;
                            context.SaveChanges();
                        }
                    }
                    // delete the excel file form machine
                    try
                    {
                        if (File.Exists(filepath))
                            File.Delete(filepath);
                    }
                    catch (Exception e)
                    {
                        //logger.Fatal("Exception pilotLogId = " + pilotLogId + " , profileId =" + profileId, e);
                    }
                }
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception pilotLogId = " + pilotLogId + " , profileId =" + profileId, e);
            }
        }

        /// <summary>
        /// Creates flight name for flight , spaces are replaced with '_'
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="DayPIC"></param>
        /// <returns></returns>
        public string GetFlightName(DateTime Date, string DayPIC)
        {
            //var plogs = context.PilotLogs.Where(pl => pl.ProfileId == profileId && !pl.Deleted).ToList();
            //// we get zero based index so add 1 to get actual index
            //var index = plogs.IndexOf(pilotlog) + 1;

            DateTime date = UserHelper.getEndDateTime(Date, DayPIC);

            var endDate = Misc.GetStringOnlyDateFormat2(date);
            var endTime = date.ToShortTimeString().Replace(" ", "").Replace(":", "-");

            var startD = Misc.GetStringOnlyDateFormat2(Date);
            var startT = Date.ToShortTimeString().Replace(" ", "").Replace(":", "-");

            //var fileName = "Flight_" + index + "_from_" + startD + "_" + startT + "_to_" + endDate + "_" + endTime + ".xlsx";

            var fileName = "Flight" + "_from_" + startD + "_" + startT + "_to_" + endDate + "_" + endTime;

            return fileName;
        }



        public string GetFlightName(DateTime Date, string DayPIC, int plotId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var pilotLogList2 = (context.PilotLogs.
                        Where(ap => ap.PilotId == profileId
                        && !ap.Deleted
                        && !ap.Profile.Deleted && ap.Date != null)).ToList();


                var abc1 = pilotLogList2.Select(a => new { date = a.Date.ToShortDateString(), log = a, time = a.Date.ToShortTimeString() }).ToList();

                var p4 = (abc1.OrderByDescending(a => a.date).ThenByDescending(a => a.time)).ToList();

                //var test1 = (abc1.OrderByDescending(a => a.date).ThenByDescending(a => a.time)).ToList();
                //var test2 = (abc1.OrderByDescending(a => a.date)).ToList();

                var p5 = p4.GroupBy(g => g.date)
                    .Select(t => new { date = t.Key, list = t.Where(a => true).ToList() }).ToList();

                if (p5.Any())
                {
                    var flightList = new List<DataLogListing>();
                    foreach (var group in p5)
                    {
                        var countList = group.list.Count;
                        if (countList <= 0) continue;

                        int i = 0;
                        foreach (var plog in @group.list)
                        {
                            i++;

                            flightList.Add(new DataLogListing
                            {
                                pilotLogId = plog.log.Id,
                                Text = String.Format("Flight {0} on {1}", i, plog.date),
                            });
                        }
                    }

                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception pilotLogId = " + plotId + " , profileId =" + profileId, e);
            }
            finally
            {
                context.Dispose();
            }

            //var plogs = context.PilotLogs.Where(pl => pl.ProfileId == profileId && !pl.Deleted).ToList();
            //// we get zero based index so add 1 to get actual index
            //var index = plogs.IndexOf(pilotlog) + 1;

            DateTime date = UserHelper.getEndDateTime(Date, DayPIC);

            var endDate = Misc.GetStringOnlyDateFormat2(date);
            var endTime = date.ToShortTimeString().Replace(" ", "").Replace(":", "-");

            var startD = Misc.GetStringOnlyDateFormat2(Date);
            var startT = Date.ToShortTimeString().Replace(" ", "").Replace(":", "-");

            //var fileName = "Flight_" + index + "_from_" + startD + "_" + startT + "_to_" + endDate + "_" + endTime + ".xlsx";

            var fileName = "Flight" + "_from_" + startD + "_" + startT + "_to_" + endDate + "_" + endTime;

            return fileName;
        }

        /// <summary>
        /// Generates the flight name for flight . 
        /// eg N733MB-_Flight_20_Ash_Vij_9/1/2014
        /// 
        /// </summary>
        /// <param name="NNumber"></param>
        /// <param name="flightNo">Count of flight for the aircraft(independent of date).</param>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public string GetFlightName(string NNumber, int flightNo, string fName, string lName, DateTime date)
        {
            return string.Format(GAResource.ExcelSheetNameForFlight
                                                            , flightNo
                                                            , (NNumber ?? string.Empty).ToUpper()
                                                            , (fName ?? string.Empty)
                                                            , (lName ?? string.Empty)
                                                            , date.ToShortDateString()
                                                            .Replace(" ", "").Replace(":", "-").Replace(@"/", "-").Replace(@"\", "-"));
        }

        /// <summary>
        /// Gets flight name for multiple flights
        /// </summary>
        /// <param name="date"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        public string GetFlightNameMultiplePilotLog(string date, string firstName, string lastName)
        {
            return string.Format(GAResource.ExcelSheetNameForMultipleFlight, date, firstName, lastName);
        }

        /// <summary>
        /// Fetch all the Jpi data for user ( for a profile Id ).
        /// </summary>
        /// <param name="profileId">User profile Id</param>
        /// <param name="pageNo">Pagination page No.</param>
        /// <param name="count">Number of pages</param>
        /// <returns>List of jpi data converted into dataloglisting</returns>
        public List<DataLogListing> ListJpiData(int profileId, int pageNo, out int count)
        {
            try
            {
                count = 0;
                if (profileId < 1) return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    var dataLogList = new List<DataLogListing>();

                    // to check if unit data exist and take Id.
                    var pilotLogList = context.PilotLogs.Where(p => p.PilotId == profileId || p.CoPilotId == profileId || p.AircraftProfile.OwnerProfileId == profileId).Select(s => s.Id).ToList();
                    if (pilotLogList.Count == 0) return dataLogList;

                    //int toSkip = Constants.PageSizeTable * (pageNo - 1);
                    //const int toTake = Constants.PageSizeTable;

                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    var airframedatalog =
                        context.AirframeDatalogs.Where(a => pilotLogList.Contains(a.PilotLogId)).OrderByDescending(o => o.Id).Select(s => s.DataLog).ToList();
                    if (airframedatalog.Count > 0)
                    {
                        foreach (var airframedata in airframedatalog)
                        {
                            dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        }

                        dataLogList.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                        });

                    }
                    return dataLogList;
                } // using
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId = " + profileId + " PageNo = " + pageNo, e);
                count = 0;
                return new List<DataLogListing>();
            }
        }

        /// <summary>
        /// Converts list of JPIUnitData into list of DataLogListing
        /// </summary>
        /// <param name="jpi"></param>
        /// <returns></returns>
        public List<DataLogListing> ConvertJpiToDataLogListing(List<JPIUnitData> jpi)
        {
            try
            {
                return jpi.Select(data => new DataLogListing
                {
                    //AMP1 = data.AMP1 == default(double) ? string.Empty : data.AMP1.ToString(),
                    Cht1 = data.Cht1 == default(double) ? string.Empty : data.Cht1.ToString(),
                    Cht2 = data.Cht2 == default(double) ? string.Empty : data.Cht2.ToString(),
                    Cht3 = data.Cht3 == default(double) ? string.Empty : data.Cht3.ToString(),
                    Cht4 = data.Cht4 == default(double) ? string.Empty : data.Cht4.ToString(),
                    Cht5 = data.Cht5 == default(double) ? string.Empty : data.Cht5.ToString(),
                    Cht6 = data.Cht6 == default(double) ? string.Empty : data.Cht6.ToString(),
                    Egt1 = data.Egt1 == default(double) ? string.Empty : data.Egt1.ToString(),
                    Egt2 = data.Egt2 == default(double) ? string.Empty : data.Egt2.ToString(),
                    Egt3 = data.Egt3 == default(double) ? string.Empty : data.Egt3.ToString(),
                    Egt4 = data.Egt4 == default(double) ? string.Empty : data.Egt4.ToString(),
                    Egt5 = data.Egt5 == default(double) ? string.Empty : data.Egt5.ToString(),
                    Egt6 = data.Egt6 == default(double) ? string.Empty : data.Egt6.ToString(),
                    FF = data.FF == default(double) ? string.Empty : data.FF.ToString(),
                    FQL = data.FQL == default(double) ? string.Empty : data.FQL.ToString(),
                    FQR = data.FQR == default(double) ? string.Empty : data.FQR.ToString(),
                    //IAS = data.IAS == default(double) ? string.Empty : data.IAS.ToString(),
                    MAP = data.MAP == default(double) ? string.Empty : data.MAP.ToString(),
                    Oat = data.Oat == default(double) ? string.Empty : data.Oat.ToString(),
                    //OilP = data.OilP == null ? string.Empty : data.OilP.ToString(),
                    //OilT = data.OilT == null ? string.Empty : data.OilT.ToString(),
                    RPM = data.RPM == default(double) ? string.Empty : data.RPM.ToString(),
                    Tit = data.Tit == default(double) ? string.Empty : data.Tit.ToString(),
                    //Volt1 = data.Volt1 == default(double) ? string.Empty : data.Volt1.ToString(),

                    //GpsAltitude =
                    //    data.GpsAltitude == default(double) ? string.Empty : data.GpsAltitude.ToString(),
                    //WayPoint = String.IsNullOrEmpty(data.WayPoint) ? string.Empty : data.WayPoint.ToString(),
                    Latitude = data.Latitude == default(double) ? string.Empty : data.Latitude.ToString(),
                    Longitude = data.Longitude == default(double) ? string.Empty : data.Longitude.ToString(),

                    Date =
                        data.DateTime.HasValue
                            ? Misc.GetStringOnlyDateFormat2(data.DateTime.Value)
                            : string.Empty,
                    Time = data.DateTime.HasValue ? data.DateTime.Value.ToString("hh:mm:ss") : string.Empty,

                    //GndSpd = data.GndSpd == default(double) ? string.Empty : data.GndSpd.ToString(),

                    //Pitch = data.Pitch == null ? string.Empty : data.Pitch.ToString(),
                    //Roll = data.Roll == null ? string.Empty : data.Roll.ToString(),
                    //Yaw = data.YAW == null ? string.Empty : data.YAW.ToString(),
                    //VSpd = data.VSpd == null ? string.Empty : data.VSpd.ToString(),

                }).ToList();

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(jpi), e);
                return new List<DataLogListing>();
            }
        }

        public List<int> GetAircraftListForManufacturerUser(int profileId)
        {

            var context = new GuardianAvionicsEntities();
            //Check that the user is manufacturer user or not
            var mappingAircraftManufacturerAndUsers = context.MappingAircraftManufacturerAndUsers.FirstOrDefault(f => f.ProfileId == profileId);

            List<int> aircraftMappedWithManufacturerUserList = new List<int>();

            if (mappingAircraftManufacturerAndUsers != null)
            {
                aircraftMappedWithManufacturerUserList = context.MappingComponentManufacturerAndAircrafts.Where(w => w.ComponentManufacturers.Contains(mappingAircraftManufacturerAndUsers.AircraftComponentManufacturerId.ToString())).Select(s => s.AircraftId).ToList();
            }

            return aircraftMappedWithManufacturerUserList;
        }


        public List<DataLogListing> ListJpiDataLastFlight(int profileId, int pageNo, out string LabelText, out int pilotLogIdOut)
        {
            pilotLogIdOut = 0;

            using (var context = new GuardianAvionicsEntities())
            {
                List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                aircraftMappedWithManufacturerUserList = GetAircraftListForManufacturerUser(profileId);

                //Check the pilot is owner or not. If owner then search by aircrafts last flight else direct search by pilotId
                var aircraftList = context.AircraftProfiles.Where(a => a.OwnerProfileId == profileId).Select(s => s.Id).ToList();
                var objPilotLog = new PilotLog();
                if (aircraftList.Count > 0)
                {
                    objPilotLog =
                  context.PilotLogs.Where(pl => (pl.PilotId == profileId || aircraftList.Contains(pl.AircraftId) || pl.CoPilotId == profileId || aircraftMappedWithManufacturerUserList.Contains(pl.AircraftId)) && !pl.Deleted)
                      .OrderByDescending(o => o.Date)
                      .FirstOrDefault();
                }
                else
                {
                    objPilotLog =
                   context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || aircraftMappedWithManufacturerUserList.Contains(pl.AircraftId)) && !pl.Deleted)
                       .OrderByDescending(o => o.Date)
                       .FirstOrDefault();
                }

                if (objPilotLog == default(PilotLog))
                {
                    LabelText = "";
                    return new List<DataLogListing>();
                }
                var totalCount = 0;
                string engineCommandType = "";
                DateTime? lastRecordDateTime = null;
                bool isDataAvailableForFlight = false;
                string aircraftImageURL = "";
                string pilotImageURL = string.Empty;
                string pilotName = string.Empty;
                int flightTimeInterval = 0;
                return ListJpiData(profileId, pageNo, objPilotLog.Id, 0, 5, out totalCount, out LabelText, out pilotLogIdOut, out engineCommandType, out isDataAvailableForFlight, out aircraftImageURL, out pilotImageURL, out pilotName, out flightTimeInterval, null, 0);
            }
        }

        /// <summary>
        /// Fetch All the Jpi data for user profile Id for given pilot log , identified by pilot log id
        /// </summary>
        /// <param name="profileId">User profile Id</param>
        /// <param name="pageNo">Page No. for pagination</param>
        /// <param name="pilotLogId">Id for pilot log</param>
        /// <param name="count">No. of pages</param>
        /// <returns>List of DataLogListing</returns>
        public List<DataLogListing> ListJpiData(int profileId, int pageNo, int pilotLogId, int totalRecordFetch, int timeInterval, out int count, out string labelText, out int pilotLogIdOut, out string engineCommandType, out bool isDataAvailableForFlight, out string aircraftImageURL, out string pilotImageURL, out string pilotName, out int flightTimeInterval, DateTime? StartDateForInnerData, int RequestDataForLevel)
        {
            labelText = string.Empty;
            count = 0;
            pilotLogIdOut = pilotLogId;
            engineCommandType = "JPI";
            isDataAvailableForFlight = false;
            aircraftImageURL = "";
            pilotImageURL = "";
            pilotName = "";
            flightTimeInterval = 0;
            try
            {
                //if (profileId < 1) return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);
                    if (plog != null)
                    {
                        engineCommandType = plog.CommandRecFrom ?? "";
                        var flightName = GetFlightName(plog.Date, plog.DayPIC).Replace("_", " ");
                        labelText = String.Format("Showing engine data for {0} Z time.", flightName);
                        Misc objMisc = new Misc();
                        labelText += "  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(plog.DayPIC));
                    }
                    else
                    {
                        return new List<DataLogListing>();
                    }
                    //zxx
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));

                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                              .AsEnumerable()
                                              .Select((s, index) => new
                                              {
                                                  FlightNo = index + 1,
                                                  RegistrationNo = s.AircraftProfile.Registration,
                                                  pilotLogId = s.Id,
                                                  aircraftImageUrl = s.AircraftProfile.ImageUrl,
                                                  pilotImageURL = (s.Profile == null) ? "" : (s.Profile.UserDetail == null ? "" : s.Profile.UserDetail.ImageUrl),
                                                  pilotName = (s.Profile == null) ? "Not Available" : (s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName)
                                              }).Where(p => p.pilotLogId == pilotLogId).FirstOrDefault();


                    string flightNo = flightList.FlightNo.ToString();
                    string RegNo = flightList.RegistrationNo;
                    string fileName = flightList.aircraftImageUrl;
                    string pilotImageName = flightList.pilotImageURL;
                    pilotName = flightList.pilotName;
                    //string path = ConfigurationReader.ImageServerPathKey;
                    string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                    if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                    {
                        aircraftImageURL = (path + fileName);
                    }
                    else
                    {
                        aircraftImageURL = Constants.AircraftDefaultImage;
                    }

                    if (!String.IsNullOrEmpty(pilotImageName))
                    {
                        //string pilotImagepath = ConfigurationReader.ImageServerPathKey;
                        string pilotImagepath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                        if (!string.IsNullOrEmpty(pilotImagepath))
                            pilotImageURL = pilotImagepath + pilotImageName;
                        else
                            pilotImageURL = string.Empty;
                    }
                    else
                    {
                        pilotImageURL = Constants.UserProfileDefaultImage;
                    }
                    //Comment by gp
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLogId;

                    DataSet dsCount = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spCheckDataLogExistByLogIdCount", param);
                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spCheckDataLogExistByLogId", param);
                    var response = new List<AirframeDatalog>(); // context.AirframeDatalogs.Where(d => d.PilotLogId == pilotLogId);
                    var dataLogListTemp = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airDatalog = response.FirstOrDefault();
                    //if (airDatalog != null)

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        isDataAvailableForFlight = true;
                        DateTime? dateFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["GPRMCDate"].ToString());    // airDatalog.GPRMCDate;
                        decimal sec = Convert.ToDecimal(dateFrom.Value.Second);
                        DateTime endDate = DateTime.UtcNow;
                        if (ds.Tables.Count >= 2)
                        {
                            endDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["GPRMCDate"].ToString());
                        }
                        if (RequestDataForLevel != 0)
                        {
                            sec = StartDateForInnerData.Value.Second;
                            endDate = StartDateForInnerData.Value.AddMinutes(timeInterval);
                            if (RequestDataForLevel == 1)
                            {

                                //Condition 1
                                // response = response.Where(r => r.GPRMCDate >= StartDateForInnerData && r.GPRMCDate < endDate && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id);

                                param = new SqlParameter[7];
                                param[0] = new SqlParameter();
                                param[0].ParameterName = "pilotlogId";
                                param[0].SqlDbType = SqlDbType.Int;
                                param[0].Value = pilotLogId;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "startDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = StartDateForInnerData;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "endDate";
                                param[2].SqlDbType = SqlDbType.DateTime;
                                param[2].Value = endDate;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "sec";
                                param[3].SqlDbType = SqlDbType.Decimal;
                                param[3].Value = sec;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "timeinterval";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 0;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "condition";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 1;

                                param[6] = new SqlParameter();
                                param[6].ParameterName = "totalrecordFetch";
                                param[6].SqlDbType = SqlDbType.Int;
                                param[6].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeIntervalByCount", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();
                            }
                            else
                            {
                                //Condition 2
                                endDate = StartDateForInnerData.Value.AddMinutes(1);
                                // response = response.Where(r => r.GPRMCDate > StartDateForInnerData && r.GPRMCDate < endDate).OrderBy(o => o.Id);

                                param = new SqlParameter[7];
                                param[0] = new SqlParameter();
                                param[0].ParameterName = "pilotlogId";
                                param[0].SqlDbType = SqlDbType.Int;
                                param[0].Value = pilotLogId;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "startDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = StartDateForInnerData;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "endDate";
                                param[2].SqlDbType = SqlDbType.DateTime;
                                param[2].Value = endDate;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "sec";
                                param[3].SqlDbType = SqlDbType.Decimal;
                                param[3].Value = sec;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "timeinterval";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 0;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "condition";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 2;

                                param[6] = new SqlParameter();
                                param[6].ParameterName = "totalrecordFetch";
                                param[6].SqlDbType = SqlDbType.Int;
                                param[6].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeIntervalByCount", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();
                            }
                        }
                        else
                        {
                            float flightTime = Convert.ToInt32(dsCount.Tables[0].Rows[0]["sec"].ToString());
                            flightTime = flightTime / 60;
                            // get the flight time duration
                            //int flightTime = endDate.Subtract(dateFrom ?? DateTime.Now).Minutes;
                            if (flightTime > 10)
                            {
                                flightTimeInterval = 10;
                            }
                            else if (flightTime > 5)
                            {
                                flightTimeInterval = 5;
                            }
                            else if (flightTime > 2)
                            {
                                flightTimeInterval = 2;
                            }
                            else if (flightTime >= 1)
                            {
                                flightTimeInterval = 1;
                            }
                            else
                            {
                                flightTimeInterval = 0;
                            }


                            if (timeInterval > flightTimeInterval)
                            {
                                timeInterval = flightTimeInterval;
                            }
                            //List<int> tempList = new List<int>();
                            //tempList.Add(10);
                            //tempList.Add(5);
                            //tempList.Add(2);
                            //tempList.Add(1);
                            //tempList.Add(0);
                            //while (timeInterval > 0)
                            //{

                            //Condition 3
                            // response = response.Where(r => r.GPRMCDate.Value.Minute % timeInterval == 0 && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id).Skip(totalRecordFetch).Take(60);
                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = dateFrom;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = DateTime.Now;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = timeInterval;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            if (flightTimeInterval == 0)
                            {
                                param[5].Value = 1;
                            }
                            else
                            {
                                param[5].Value = 3;
                            }


                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = totalRecordFetch;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeIntervalByCount", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                PilotLogId = m.Field<int>("PilotLogId"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();
                            //if (response.Count() > 0)
                            //{
                            //    break;
                            //}
                            //timeInterval = tempList.Where(s => s < timeInterval).Max();
                            //}
                        }
                    }
                    else
                    {
                        isDataAvailableForFlight = false;
                    }

                    // context.AirframeDatalogs.Where(d => d.PilotLogId == item.pilotLogId).ToList().ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                    response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));


                    if (dataLogListTemp.Count > 0)
                    {
                        //foreach (var i in dataLogListTemp)
                        //{
                        //    i.Time = new DateTime(DateTime.Parse(i.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                        //    i.Date = (Convert.ToDateTime(i.Date)).ToShortDateString();
                        //    i.AircraftNNumber = RegNo;
                        //    i.FlightNumber = flightNo;
                        //    if (i.FPosition != null) { if (i.FPosition.Contains("__")) { i.FPosition = null; }}
                        //    int flap = Convert.ToInt32(i.FPosition==""?"0": i.FPosition);
                        //    i.FlapPosition = flap.ToString();
                        //    double Right = Convert.ToDouble(i.FQRight == "" ? "0" : i.FQRight);
                        //    double FQLeft = Convert.ToDouble(i.FQleft == "" ? "0" : i.FQleft);
                        //}
                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;
                            if (a.FPosition != null) { if (a.FPosition.Contains("__")) { a.FPosition = null; } }
                            int flap = Convert.ToInt32(a.FPosition == "" ? "0" : a.FPosition);
                            a.FlapPosition = flap.ToString();
                            double Right = Convert.ToDouble(a.FQRight == "" ? "0" : a.FQRight);
                            double FQLeft = Convert.ToDouble(a.FQleft == "" ? "0" : a.FQleft);
                            //GP Fuel quantity calculation

                            var calculateFzLeft = (from FD in context.FuelDetails
                                                   join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                                   where FD.aircraftId == plog.AircraftId && FD.fuelsideType == "left" && FL.frequency >= FQLeft
                                                   orderby FL.frequency
                                                   select new { FD, FL }).FirstOrDefault();

                            var calculateFzRight = (from FD in context.FuelDetails
                                                    join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                                    where FD.aircraftId == plog.AircraftId && FD.fuelsideType == "right" && FL.frequency >= Right
                                                    orderby FL.frequency
                                                    select new { FD, FL }).FirstOrDefault();
                            if (calculateFzLeft != null)
                            {
                                if (calculateFzLeft.FL.frequencyAtLevel != 0)
                                {
                                    var calculateLeft = (from FD in context.FuelDetails
                                                         join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                                         where FD.aircraftId == plog.AircraftId && FD.fuelsideType == "left" && FL.frequencyId != calculateFzLeft.FL.frequencyId
                                                         orderby FL.frequency
                                                         select new { FD, FL }).FirstOrDefault();
                                    var left = (FQLeft - calculateLeft.FL.frequency) /
                                       ((calculateFzLeft.FL.frequency - calculateLeft.FL.frequency) /
                                       (calculateFzLeft.FL.gallonLevel - calculateLeft.FL.gallonLevel))
                                       + calculateLeft.FL.gallonLevel;

                                    a.FQL = left.ToString();
                                }
                            }

                            if (calculateFzRight != null)
                            {
                                if (calculateFzRight.FL.frequencyAtLevel != 0)
                                {
                                    var calculateRight = (from FD in context.FuelDetails
                                                          join FL in context.FuelLevelFrequency on FD.fuelTypeId equals FL.fuelType
                                                          where FD.aircraftId == plog.AircraftId && FD.fuelsideType == "right" && FL.frequencyId != calculateFzRight.FL.frequencyId
                                                          orderby FL.frequency
                                                          select new { FD, FL }).FirstOrDefault();

                                    var right = (Right - calculateRight.FL.frequency) /
                                        ((calculateFzRight.FL.frequency - calculateRight.FL.frequency) /
                                        (calculateFzRight.FL.gallonLevel - calculateRight.FL.gallonLevel))
                                        + calculateRight.FL.gallonLevel;
                                    a.FQR = right.ToString();
                                }

                            }

                        });
                    }

                    CommonHelper objHelper = new CommonHelper();
                    dataLogListTemp = objHelper.ParseJPIColourCode(dataLogListTemp, plog.AircraftId);
                    return dataLogListTemp;
                } // using
            }
            catch (Exception ex)
            {

                return new List<DataLogListing>();
            }

        }


        public List<DataLogWithoutUnit> ListJpiDataForAircraftWithoutUnit(int profileId, int pageNo, int pilotLogId, int totalRecordFetch, int timeInterval, out int count, out string labelText, out int pilotLogIdOut, out string engineCommandType, out bool isDataAvailableForFlight, out string aircraftImageURL, out string pilotImageURL, out string pilotName, out int flightTimeInterval, DateTime? StartDateForInnerData, int RequestDataForLevel)
        {
            labelText = string.Empty;
            count = 0;
            pilotLogIdOut = pilotLogId;
            engineCommandType = "JPI";
            isDataAvailableForFlight = false;
            aircraftImageURL = "";
            pilotImageURL = "";
            pilotName = "";
            flightTimeInterval = 0;
            try
            {
                //if (profileId < 1) return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);
                    if (plog != null)
                    {
                        engineCommandType = plog.CommandRecFrom ?? "";
                        var flightName = GetFlightName(plog.Date, plog.DayPIC).Replace("_", " ");
                        labelText = String.Format("Showing engine data for {0} Z time.", flightName);
                        Misc objMisc = new Misc();
                        labelText += "  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(plog.DayPIC));
                    }
                    else
                    {
                        return new List<DataLogWithoutUnit>();
                    }
                    //zxx
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));

                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                              .AsEnumerable()
                                              .Select((s, index) => new
                                              {
                                                  FlightNo = index + 1,
                                                  RegistrationNo = s.AircraftProfile.Registration,
                                                  pilotLogId = s.Id,
                                                  aircraftImageUrl = s.AircraftProfile.ImageUrl,
                                                  pilotImageURL = (s.Profile == null) ? "" : (s.Profile.UserDetail == null ? "" : s.Profile.UserDetail.ImageUrl),
                                                  pilotName = (s.Profile == null) ? "Not Available" : (s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName)
                                              }).Where(p => p.pilotLogId == pilotLogId).FirstOrDefault();


                    string flightNo = flightList.FlightNo.ToString();
                    string RegNo = flightList.RegistrationNo;
                    string fileName = flightList.aircraftImageUrl;
                    string pilotImageName = flightList.pilotImageURL;
                    pilotName = flightList.pilotName;
                    //string path = ConfigurationReader.ImageServerPathKey;
                    string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                    if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                    {
                        aircraftImageURL = (path + fileName);
                    }
                    else
                    {
                        aircraftImageURL = Constants.AircraftDefaultImage;
                    }

                    if (!String.IsNullOrEmpty(pilotImageName))
                    {
                        //string pilotImagepath = ConfigurationReader.ImageServerPathKey;
                        string pilotImagepath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                        if (!string.IsNullOrEmpty(pilotImagepath))
                            pilotImageURL = pilotImagepath + pilotImageName;
                        else
                            pilotImageURL = string.Empty;
                    }
                    else
                    {
                        pilotImageURL = Constants.UserProfileDefaultImage;
                    }

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLogId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spCheckDataLogExistByLogId", param);


                    var response = new List<AirframeDatalog>(); // context.AirframeDatalogs.Where(d => d.PilotLogId == pilotLogId);
                    var dataLogListTemp = new List<DataLogWithoutUnit>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airDatalog = response.FirstOrDefault();
                    //if (airDatalog != null)
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        isDataAvailableForFlight = true;
                        DateTime? dateFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["GPRMCDate"].ToString());    // airDatalog.GPRMCDate;
                        decimal sec = Convert.ToDecimal(dateFrom.Value.Second);
                        DateTime endDate = DateTime.UtcNow;

                        if (ds.Tables.Count >= 2)
                        {
                            endDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["GPRMCDate"].ToString());
                        }

                        if (RequestDataForLevel != 0)
                        {
                            sec = StartDateForInnerData.Value.Second;
                            endDate = StartDateForInnerData.Value.AddMinutes(timeInterval);
                            if (RequestDataForLevel == 1)
                            {

                                //Condition 1
                                // response = response.Where(r => r.GPRMCDate >= StartDateForInnerData && r.GPRMCDate < endDate && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id);

                                param = new SqlParameter[7];
                                param[0] = new SqlParameter();
                                param[0].ParameterName = "pilotlogId";
                                param[0].SqlDbType = SqlDbType.Int;
                                param[0].Value = pilotLogId;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "startDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = StartDateForInnerData;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "endDate";
                                param[2].SqlDbType = SqlDbType.DateTime;
                                param[2].Value = endDate;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "sec";
                                param[3].SqlDbType = SqlDbType.Decimal;
                                param[3].Value = sec;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "timeinterval";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 0;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "condition";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 1;

                                param[6] = new SqlParameter();
                                param[6].ParameterName = "totalrecordFetch";
                                param[6].SqlDbType = SqlDbType.Int;
                                param[6].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();


                            }
                            else
                            {
                                //Condition 2
                                endDate = StartDateForInnerData.Value.AddMinutes(1);
                                // response = response.Where(r => r.GPRMCDate > StartDateForInnerData && r.GPRMCDate < endDate).OrderBy(o => o.Id);

                                param = new SqlParameter[7];
                                param[0] = new SqlParameter();
                                param[0].ParameterName = "pilotlogId";
                                param[0].SqlDbType = SqlDbType.Int;
                                param[0].Value = pilotLogId;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "startDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = StartDateForInnerData;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "endDate";
                                param[2].SqlDbType = SqlDbType.DateTime;
                                param[2].Value = endDate;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "sec";
                                param[3].SqlDbType = SqlDbType.Decimal;
                                param[3].Value = sec;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "timeinterval";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 0;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "condition";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 2;

                                param[6] = new SqlParameter();
                                param[6].ParameterName = "totalrecordFetch";
                                param[6].SqlDbType = SqlDbType.Int;
                                param[6].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();
                            }
                        }
                        else
                        {
                            //List<int> tempList = new List<int>();
                            //tempList.Add(10);
                            //tempList.Add(5);
                            //tempList.Add(2);
                            //tempList.Add(1);
                            //tempList.Add(0);
                            //while (timeInterval > 0)
                            //{

                            int flightTime = endDate.Subtract(dateFrom ?? DateTime.Now).Minutes;
                            if (flightTime > 10)
                            {
                                flightTimeInterval = 10;
                            }
                            else if (flightTime > 5)
                            {
                                flightTimeInterval = 5;
                            }
                            else if (flightTime > 2)
                            {
                                flightTimeInterval = 2;
                            }
                            else if (flightTime >= 1)
                            {
                                flightTimeInterval = 1;
                            }
                            else
                            {
                                flightTimeInterval = 0;
                            }

                            if (timeInterval > flightTimeInterval)
                            {
                                timeInterval = flightTimeInterval;
                            }

                            //Condition 3
                            // response = response.Where(r => r.GPRMCDate.Value.Minute % timeInterval == 0 && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id).Skip(totalRecordFetch).Take(60);
                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = dateFrom;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = DateTime.Now;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = timeInterval;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            if (flightTimeInterval == 0)
                            {
                                param[5].Value = 1;
                            }
                            else
                            {
                                param[5].Value = 3;
                            }


                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = totalRecordFetch;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                PilotLogId = m.Field<int>("PilotLogId"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();
                            //if (response.Count() > 0)
                            //{
                            //    break;
                            //}
                            //timeInterval = tempList.Where(s => s < timeInterval).Max();
                            //}
                        }
                    }
                    else
                    {
                        isDataAvailableForFlight = false;
                    }

                    // context.AirframeDatalogs.Where(d => d.PilotLogId == item.pilotLogId).ToList().ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                    response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));


                    if (dataLogListTemp.Count > 0)
                    {

                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.DateString, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.DateString = Convert.ToDateTime(a.DateString).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;

                        });
                    }

                    //CommonHelper objHelper = new CommonHelper();
                    //dataLogListTemp = objHelper.ParseJPIColourCode(dataLogListTemp, plog.AircraftId);
                    return dataLogListTemp;
                } // using
            }
            catch (Exception)
            {

                return new List<DataLogWithoutUnit>();
            }

        }

        public List<DataLogWithoutUnit> ListJpiDataForAircraftWithoutUnitLive(int profileId, int pilotLogId, int lastRecordId, int? timeInterval, DateTime? StartDate, int? requestDataForLevel, out int count, out string labelText, out int pilotLogIdOut, out string engineCommandType, out string aircraftImageURL, out string pilotImageURL, out string pilotName, out int flightTimeInterval, out int lastRecordFetchId, out bool isFlightFinished, out List<LatLong> latLongList, out string route, out string wayPoint, out List<MFDSettings> MFDList, out int[] PLHeader)
        {
            labelText = string.Empty;
            count = 0;
            pilotLogIdOut = pilotLogId;
            engineCommandType = "JPI";
            lastRecordFetchId = lastRecordId;
            aircraftImageURL = "";
            pilotImageURL = "";
            pilotName = "";
            isFlightFinished = false;
            flightTimeInterval = 1;
            latLongList = new List<LatLong>();
            route = "";
            wayPoint = "";
            MFDList = new List<MFDSettings>();
            PLHeader = new int[] { };
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);


                    if (plog == null)
                    {
                        isFlightFinished = true;
                        return new List<DataLogWithoutUnit>();
                    }

                    if (plog.Finished)
                    {
                        isFlightFinished = true;
                        return new List<DataLogWithoutUnit>();
                    }

                    route = plog.Route ?? "";
                    wayPoint = plog.WayPoint ?? "";

                    if (plog != null)
                    {
                        engineCommandType = plog.CommandRecFrom ?? "";
                        var flightName = GetFlightName(plog.Date, plog.DayPIC).Replace("_", " ");
                        labelText = String.Format("Showing engine data for {0} Z time.", flightName);
                        Misc objMisc = new Misc();
                        labelText += "  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(plog.DayPIC));
                    }
                    else
                    {
                        return new List<DataLogWithoutUnit>();
                    }
                    //zxx
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));

                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                              .AsEnumerable()
                                              .Select((s, index) => new
                                              {
                                                  FlightNo = index + 1,
                                                  RegistrationNo = s.AircraftProfile.Registration,
                                                  pilotLogId = s.Id,
                                                  aircraftImageUrl = s.AircraftProfile.ImageUrl,
                                                  pilotImageURL = (s.Profile == null) ? "" : (s.Profile.UserDetail == null ? "" : s.Profile.UserDetail.ImageUrl),
                                                  pilotName = (s.Profile == null) ? "Not Available" : (s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName)
                                              }).Where(p => p.pilotLogId == pilotLogId).FirstOrDefault();


                    string flightNo = flightList.FlightNo.ToString();
                    string RegNo = flightList.RegistrationNo;
                    string fileName = flightList.aircraftImageUrl;
                    string pilotImageName = flightList.pilotImageURL;
                    pilotName = flightList.pilotName;
                    string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                    if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                    {
                        aircraftImageURL = (path + fileName);
                    }
                    else
                    {
                        aircraftImageURL = Constants.AircraftDefaultImage;
                    }

                    if (!String.IsNullOrEmpty(pilotImageName))
                    {
                        //string pilotImagepath = ConfigurationReader.ImageServerPathKey;
                        string pilotImagepath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                        if (!string.IsNullOrEmpty(pilotImagepath))
                            pilotImageURL = pilotImagepath + pilotImageName;
                        else
                            pilotImageURL = string.Empty;
                    }
                    else
                    {
                        pilotImageURL = Constants.UserProfileDefaultImage;
                    }

                    PLHeader = plog.Headers.Split(',').Select(Int32.Parse).ToArray();

                    SqlParameter[] param;
                    DataSet ds;

                    var response = new List<AirframeDatalog>(); // context.AirframeDatalogs.Where(d => d.PilotLogId == pilotLogId);
                    var dataLogListTemp = new List<DataLogWithoutUnit>();

                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();


                    if (StartDate == null && lastRecordId != 0)
                    {
                        param = new SqlParameter[2];
                        param[0] = new SqlParameter();
                        param[0].ParameterName = "@pilotLogId";
                        param[0].SqlDbType = SqlDbType.Int;
                        param[0].Value = pilotLogId;

                        param[1] = new SqlParameter();
                        param[1].ParameterName = "@lastRecordId";
                        param[1].SqlDbType = SqlDbType.Int;
                        param[1].Value = lastRecordId;

                        ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetLiveData", param);


                        if (ds.Tables.Count > 0)
                        {
                            int totalRows = ds.Tables[0].Rows.Count;
                            if (totalRows > 0)
                            {
                                if (lastRecordId == 0)
                                {
                                    response = ds.Tables[0].AsEnumerable().OrderBy(o => o.Field<int>("ID")).Select(m => new AirframeDatalog()
                                    {
                                        Id = m.Field<int>("ID"),
                                        DataLog = m.Field<string>("DataLog")
                                    }).ToList();

                                    lastRecordFetchId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                    {
                                        Id = m.Field<int>("ID"),
                                        DataLog = m.Field<string>("DataLog")
                                    }).ToList();

                                    lastRecordFetchId = Convert.ToInt32(ds.Tables[0].Rows[totalRows - 1][0]);
                                }
                                response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                                for (int i = 0; i < response.Count; i++)
                                {
                                    dataLogListTemp[i].Id = response[i].Id;
                                }

                            }
                            else
                            {
                                return new List<DataLogWithoutUnit>();
                            }
                        }
                        else
                        {
                            return new List<DataLogWithoutUnit>();
                        }
                    }
                    else
                    {
                        DateTime endDate = StartDate ?? DateTime.Now;
                        StartDate = StartDate != null ? StartDate.Value.AddMinutes((timeInterval ?? 0) * -1) : DateTime.UtcNow;
                        decimal sec = StartDate.Value.Second;

                        if (requestDataForLevel == 0)
                        {

                            param = new SqlParameter[1];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotLog";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spCheckDataLogExistByLogId", param);
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    StartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["GPRMCDate"].ToString());    // airDatalog.GPRMCDate;
                                    sec = Convert.ToDecimal(StartDate.Value.Second);
                                    endDate = DateTime.UtcNow;

                                    if (ds.Tables.Count >= 2)
                                    {
                                        endDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["GPRMCDate"].ToString());
                                        lastRecordFetchId = Convert.ToInt32(ds.Tables[1].Rows[0]["Id"]);
                                    }

                                    flightTimeInterval = 0;

                                    int flightTime = endDate.Subtract(StartDate ?? DateTime.Now).Minutes;
                                    if (flightTime > 10)
                                    {
                                        flightTimeInterval = 10;
                                    }
                                    else if (flightTime > 5)
                                    {
                                        flightTimeInterval = 5;
                                    }
                                    else if (flightTime >= 1)
                                    {
                                        flightTimeInterval = 1;
                                    }
                                    else
                                    {
                                        flightTimeInterval = 0;
                                    }

                                    timeInterval = flightTimeInterval;


                                    //Condition 3
                                    // response = response.Where(r => r.GPRMCDate.Value.Minute % timeInterval == 0 && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id).Skip(totalRecordFetch).Take(60);
                                    param = new SqlParameter[7];
                                    param[0] = new SqlParameter();
                                    param[0].ParameterName = "pilotlogId";
                                    param[0].SqlDbType = SqlDbType.Int;
                                    param[0].Value = pilotLogId;

                                    param[1] = new SqlParameter();
                                    param[1].ParameterName = "startDate";
                                    param[1].SqlDbType = SqlDbType.DateTime;
                                    param[1].Value = StartDate;

                                    param[2] = new SqlParameter();
                                    param[2].ParameterName = "endDate";
                                    param[2].SqlDbType = SqlDbType.DateTime;
                                    param[2].Value = DateTime.Now;

                                    param[3] = new SqlParameter();
                                    param[3].ParameterName = "sec";
                                    param[3].SqlDbType = SqlDbType.Decimal;
                                    param[3].Value = sec;

                                    param[4] = new SqlParameter();
                                    param[4].ParameterName = "timeinterval";
                                    param[4].SqlDbType = SqlDbType.Int;
                                    param[4].Value = timeInterval;

                                    param[5] = new SqlParameter();
                                    param[5].ParameterName = "condition";
                                    param[5].SqlDbType = SqlDbType.Int;
                                    string isSecData = "false";
                                    if (flightTimeInterval == 0)
                                    {
                                        param[5].Value = 4;
                                        isSecData = "true";
                                    }
                                    else
                                    {
                                        param[5].Value = 3;
                                    }


                                    param[6] = new SqlParameter();
                                    param[6].ParameterName = "totalrecordFetch";
                                    param[6].SqlDbType = SqlDbType.Int;
                                    param[6].Value = 0;

                                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                    int recCount = 0;
                                    if (ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                            {
                                                Id = m.Field<int>("ID"),
                                                GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                                PilotLogId = m.Field<int>("PilotLogId"),
                                                DataLog = m.Field<string>("DataLog")
                                            }).ToList();
                                            lastRecordFetchId = response[response.Count - 1].Id;
                                            // response.RemoveAt(0);

                                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                                            for (int i = 0; i < response.Count; i++)
                                            {
                                                recCount = i;
                                                dataLogListTemp[i].Id = response[i].Id;
                                                lastRecordFetchId = response[i].Id;
                                                dataLogListTemp[recCount].IsSecData = isSecData;
                                            }
                                            //recCount = recCount + 1;
                                        }
                                    }


                                    if (flightTimeInterval != 0)
                                    {
                                        param[5].Value = 5;
                                        param[6].Value = lastRecordFetchId;
                                        ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);

                                        if (ds.Tables.Count > 0)
                                        {
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                var response1 = ds.Tables[0].AsEnumerable().Select(m => new
                                                {
                                                    Id = m.Field<int>("ID"),
                                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                                    PilotLogId = m.Field<int>("PilotLogId"),
                                                    DataLog = m.Field<string>("DataLog"),
                                                    IsSecData = "true"
                                                }).ToList();
                                                lastRecordFetchId = response1[response1.Count - 1].Id;
                                                response1.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                                                for (int i = 0; i < response1.Count; i++)
                                                {

                                                    dataLogListTemp[recCount].Id = response1[i].Id;
                                                    dataLogListTemp[recCount].IsSecData = response1[i].IsSecData;
                                                    recCount = recCount + 1;
                                                }
                                            }

                                        }


                                    }

                                    param[5].Value = 6;
                                    param[6].Value = lastRecordFetchId;
                                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                    if (ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            var datalogForLatLng = ds.Tables[0].AsEnumerable().Select(m => new
                                            {
                                                DataLog = m.Field<string>("DataLog")
                                            }).ToList();
                                            var latLongListTemp = new List<LatLong>();
                                            datalogForLatLng.ForEach(f => latLongListTemp.Add(jsonSerializer.Deserialize<LatLong>(f.DataLog)));
                                            latLongList = latLongListTemp;
                                        }
                                    }
                                }
                                else
                                {
                                    return new List<DataLogWithoutUnit>();
                                }
                            }
                            else
                            {
                                return new List<DataLogWithoutUnit>();
                            }
                        }
                        else if (requestDataForLevel == 1)
                        {

                            //Condition 1
                            // response = response.Where(r => r.GPRMCDate >= StartDateForInnerData && r.GPRMCDate < endDate && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id);

                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = StartDate;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = endDate;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = 0;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            param[5].Value = 1;

                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = 1;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();

                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                            for (int i = 0; i < response.Count; i++)
                            {
                                dataLogListTemp[i].Id = response[i].Id;
                            }


                        }
                        else
                        {
                            //Condition 2
                            endDate = StartDate.Value.AddMinutes(1);
                            // response = response.Where(r => r.GPRMCDate > StartDateForInnerData && r.GPRMCDate < endDate).OrderBy(o => o.Id);

                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = StartDate;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = endDate;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = 0;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            param[5].Value = 2;

                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = 1;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();

                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(f.DataLog.Replace("Date", "DateString"))));

                            for (int i = 0; i < response.Count; i++)
                            {
                                dataLogListTemp[i].Id = response[i].Id;
                            }
                        }
                    }


                    if (dataLogListTemp.Count > 0)
                    {

                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.DateString, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.DateString = Convert.ToDateTime(a.DateString).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;
                        });
                    }


                    MFDList = new CommonHelper().ParsePListForMFD(plog.AircraftId);

                    //CommonHelper objHelper = new CommonHelper();
                    //dataLogListTemp = objHelper.ParseJPIColourCode(dataLogListTemp, plog.AircraftId);
                    return dataLogListTemp;


                } // using
            }
            catch (Exception)
            {

                return new List<DataLogWithoutUnit>();
            }

        }


        public List<DataLogListing> ListJpiDataLive(int profileId, int pilotLogId, int lastRecordId, int? timeInterval, DateTime? StartDate, int? requestDataForLevel, out int count, out string labelText, out int pilotLogIdOut, out string engineCommandType, out string aircraftImageURL, out string pilotImageURL, out string pilotName, out int flightTimeInterval, out int lastRecordFetchId, out bool isFlightFinished, out List<LatLong> latLongList, out string route, out string wayPoint, out List<MFDSettings> MFDList, out int[] PLHeader)
        {
            labelText = string.Empty;
            count = 0;
            pilotLogIdOut = pilotLogId;
            engineCommandType = "JPI";
            aircraftImageURL = "";
            pilotImageURL = "";
            pilotName = "";
            lastRecordFetchId = lastRecordId;
            isFlightFinished = false;
            flightTimeInterval = 0;
            route = "";
            wayPoint = "";
            latLongList = new List<LatLong>();
            MFDList = new List<MFDSettings>();
            PLHeader = new int[] { };
            try
            {
                //if (profileId < 1) return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    string flightNo = "";
                    string RegNo = "";
                    var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);

                    if (plog == null)
                    {
                        isFlightFinished = true;
                        return new List<DataLogListing>();
                    }

                    if (plog.Finished)
                    {
                        isFlightFinished = true;
                        return new List<DataLogListing>();
                    }

                    route = plog.Route ?? "";
                    wayPoint = plog.WayPoint ?? "";

                    if (lastRecordId == 0)
                    {

                        if (plog != null)
                        {
                            engineCommandType = plog.CommandRecFrom ?? "";
                            var flightName = GetFlightName(plog.Date, plog.DayPIC).Replace("_", " ");
                            labelText = String.Format("Showing engine data for {0} Z time.", flightName);
                            Misc objMisc = new Misc();
                            labelText += "  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(plog.DayPIC));
                        }
                        else
                        {
                            return new List<DataLogListing>();
                        }
                        //zxx
                        List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                        var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));

                        var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                                  .AsEnumerable()
                                                  .Select((s, index) => new
                                                  {
                                                      FlightNo = index + 1,
                                                      RegistrationNo = s.AircraftProfile.Registration,
                                                      pilotLogId = s.Id,
                                                      aircraftImageUrl = s.AircraftProfile.ImageUrl,
                                                      pilotImageURL = (s.Profile == null) ? "" : (s.Profile.UserDetail == null ? "" : s.Profile.UserDetail.ImageUrl),
                                                      pilotName = (s.Profile == null) ? "Not Available" : (s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName)
                                                  }).Where(p => p.pilotLogId == pilotLogId).FirstOrDefault();


                        flightNo = flightList.FlightNo.ToString();
                        RegNo = flightList.RegistrationNo;
                        string fileName = flightList.aircraftImageUrl;
                        string pilotImageName = flightList.pilotImageURL;
                        pilotName = flightList.pilotName;
                        string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                        if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                        {
                            aircraftImageURL = (path + fileName);
                        }
                        else
                        {
                            aircraftImageURL = Constants.AircraftDefaultImage;
                        }

                        if (!String.IsNullOrEmpty(pilotImageName))
                        {
                            //string pilotImagepath = ConfigurationReader.ImageServerPathKey;
                            string pilotImagepath = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                            if (!string.IsNullOrEmpty(pilotImagepath))
                                pilotImageURL = pilotImagepath + pilotImageName;
                            else
                                pilotImageURL = string.Empty;
                        }
                        else
                        {
                            pilotImageURL = Constants.UserProfileDefaultImage;
                        }
                    }
                    try
                    {
                        PLHeader = plog.Headers.Split(',').Select(Int32.Parse).ToArray();
                    }
                    catch (Exception ex)
                    {
                    }
                    SqlParameter[] param;
                    DataSet ds;
                    CommonHelper objHelper = new CommonHelper();

                    MFDList = objHelper.ParsePListForMFD(plog.AircraftId);


                    var response = new List<AirframeDatalog>(); // context.AirframeDatalogs.Where(d => d.PilotLogId == pilotLogId);
                    var dataLogListTemp = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airDatalog = response.FirstOrDefault();
                    //if (airDatalog != null)


                    if (StartDate == null && lastRecordId != 0)
                    {
                        param = new SqlParameter[2];
                        param[0] = new SqlParameter();
                        param[0].ParameterName = "@pilotLogId";
                        param[0].SqlDbType = SqlDbType.Int;
                        param[0].Value = pilotLogId;

                        param[1] = new SqlParameter();
                        param[1].ParameterName = "@lastRecordId";
                        param[1].SqlDbType = SqlDbType.Int;
                        param[1].Value = lastRecordId;

                        ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetLiveData", param);
                        if (ds.Tables.Count > 0)
                        {
                            int totalRows = ds.Tables[0].Rows.Count;
                            if (totalRows > 0)
                            {
                                if (lastRecordId == 0)
                                {
                                    response = ds.Tables[0].AsEnumerable().OrderBy(o => o.Field<int>("ID")).Select(m => new AirframeDatalog()
                                    {
                                        Id = m.Field<int>("ID"),
                                        DataLog = m.Field<string>("DataLog")
                                    }).ToList();

                                    lastRecordFetchId = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                    {
                                        Id = m.Field<int>("ID"),
                                        DataLog = m.Field<string>("DataLog")
                                    }).ToList();

                                    lastRecordFetchId = Convert.ToInt32(ds.Tables[0].Rows[totalRows - 1][0]);
                                }
                                response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                                for (int i = 0; i < response.Count; i++)
                                {
                                    dataLogListTemp[i].Id = response[i].Id;
                                }
                            }
                            else
                            {
                                return new List<DataLogListing>();
                            }
                        }
                        else
                        {
                            return new List<DataLogListing>();
                        }
                    }
                    else
                    {
                        DateTime endDate = StartDate ?? DateTime.Now;
                        //  StartDate = StartDate.Value.AddMinutes((timeInterval ?? 0) * -1);
                        StartDate = StartDate != null ? StartDate.Value.AddMinutes((timeInterval ?? 0) * -1) : DateTime.UtcNow;
                        decimal sec = StartDate.Value.Second;

                        if (requestDataForLevel == 0)
                        {
                            param = new SqlParameter[1];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotLog";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spCheckDataLogExistByLogId", param);
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    StartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["GPRMCDate"].ToString());    // airDatalog.GPRMCDate;
                                    sec = Convert.ToDecimal(StartDate.Value.Second);
                                    endDate = DateTime.UtcNow;

                                    if (ds.Tables.Count >= 2)
                                    {
                                        endDate = Convert.ToDateTime(ds.Tables[1].Rows[0]["GPRMCDate"].ToString());
                                        lastRecordFetchId = Convert.ToInt32(ds.Tables[1].Rows[0]["Id"]);
                                    }

                                    flightTimeInterval = 0;

                                    int flightTime = endDate.Subtract(StartDate ?? DateTime.Now).Minutes;
                                    if (flightTime > 10)
                                    {
                                        flightTimeInterval = 10;
                                    }
                                    else if (flightTime > 5)
                                    {
                                        flightTimeInterval = 5;
                                    }
                                    else if (flightTime >= 1)
                                    {
                                        flightTimeInterval = 1;
                                    }
                                    else
                                    {
                                        flightTimeInterval = 0;
                                    }

                                    timeInterval = (flightTimeInterval == 0) ? 1 : flightTimeInterval;


                                    //Condition 3
                                    // response = response.Where(r => r.GPRMCDate.Value.Minute % timeInterval == 0 && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id).Skip(totalRecordFetch).Take(60);
                                    param = new SqlParameter[7];
                                    param[0] = new SqlParameter();
                                    param[0].ParameterName = "pilotlogId";
                                    param[0].SqlDbType = SqlDbType.Int;
                                    param[0].Value = pilotLogId;

                                    param[1] = new SqlParameter();
                                    param[1].ParameterName = "startDate";
                                    param[1].SqlDbType = SqlDbType.DateTime;
                                    param[1].Value = StartDate;

                                    param[2] = new SqlParameter();
                                    param[2].ParameterName = "endDate";
                                    param[2].SqlDbType = SqlDbType.DateTime;
                                    param[2].Value = DateTime.Now;

                                    param[3] = new SqlParameter();
                                    param[3].ParameterName = "sec";
                                    param[3].SqlDbType = SqlDbType.Decimal;
                                    param[3].Value = sec;

                                    param[4] = new SqlParameter();
                                    param[4].ParameterName = "timeinterval";
                                    param[4].SqlDbType = SqlDbType.Int;
                                    param[4].Value = timeInterval;

                                    param[5] = new SqlParameter();
                                    param[5].ParameterName = "condition";
                                    param[5].SqlDbType = SqlDbType.Int;

                                    string isSecData = "false";
                                    if (flightTimeInterval == 0)
                                    {
                                        param[5].Value = 4;
                                        isSecData = "true";
                                    }
                                    else
                                    {
                                        param[5].Value = 3;
                                    }


                                    param[6] = new SqlParameter();
                                    param[6].ParameterName = "totalrecordFetch";
                                    param[6].SqlDbType = SqlDbType.Int;
                                    param[6].Value = 0;
                                    int recCount = 0;
                                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                    if (ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                            {
                                                Id = m.Field<int>("ID"),
                                                GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                                PilotLogId = m.Field<int>("PilotLogId"),
                                                DataLog = m.Field<string>("DataLog")
                                            }).ToList();

                                            response.RemoveAt(0);

                                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                                            for (int i = 0; i < response.Count; i++)
                                            {
                                                recCount = i;
                                                dataLogListTemp[i].Id = response[i].Id;
                                                lastRecordFetchId = response[i].Id;
                                                dataLogListTemp[i].IsSecData = isSecData;
                                            }
                                            // recCount = recCount + 1;
                                        }

                                    }

                                    if (flightTimeInterval != 0)
                                    {
                                        param[5].Value = 5;
                                        param[6].Value = lastRecordFetchId;
                                        ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);

                                        if (ds.Tables.Count > 0)
                                        {

                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                var response1 = ds.Tables[0].AsEnumerable().Select(m => new
                                                {
                                                    Id = m.Field<int>("ID"),
                                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                                    PilotLogId = m.Field<int>("PilotLogId"),
                                                    DataLog = m.Field<string>("DataLog"),
                                                    IsSecData = "true"
                                                }).ToList();
                                                lastRecordFetchId = response1[response1.Count - 1].Id;
                                                response1.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                                                try
                                                {
                                                    for (int i = 0; i < response1.Count; i++)
                                                    {
                                                        dataLogListTemp[recCount].Id = response1[i].Id;
                                                        dataLogListTemp[recCount].IsSecData = response1[i].IsSecData;
                                                        recCount = recCount + 1;
                                                    }

                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }
                                        }
                                    }
                                    param[5].Value = 6;
                                    param[6].Value = lastRecordFetchId;
                                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                                    if (ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            var datalogForLatLng = ds.Tables[0].AsEnumerable().Select(m => new
                                            {
                                                DataLog = m.Field<string>("DataLog")
                                            }).ToList();
                                            var latLongListTemp = new List<LatLong>();
                                            datalogForLatLng.ForEach(f => latLongListTemp.Add(jsonSerializer.Deserialize<LatLong>(f.DataLog)));
                                            latLongList = latLongListTemp;
                                        }
                                    }


                                }
                                else
                                {
                                    return new List<DataLogListing>();
                                }
                            }
                            else
                            {
                                return new List<DataLogListing>();
                            }
                        }
                        else if (requestDataForLevel == 1)
                        {
                            //Condition 1
                            // response = response.Where(r => r.GPRMCDate >= StartDateForInnerData && r.GPRMCDate < endDate && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id);

                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = StartDate;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = endDate;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = 0;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            param[5].Value = 1;

                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = 1;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();

                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                            for (int i = 0; i < response.Count; i++)
                            {
                                dataLogListTemp[i].Id = response[i].Id;
                            }
                        }
                        else
                        {
                            //Condition 2
                            endDate = StartDate.Value.AddMinutes(1);
                            // response = response.Where(r => r.GPRMCDate > StartDateForInnerData && r.GPRMCDate < endDate).OrderBy(o => o.Id);

                            param = new SqlParameter[7];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "pilotlogId";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = pilotLogId;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "startDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = StartDate;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "endDate";
                            param[2].SqlDbType = SqlDbType.DateTime;
                            param[2].Value = endDate;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "sec";
                            param[3].SqlDbType = SqlDbType.Decimal;
                            param[3].Value = sec;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "timeinterval";
                            param[4].SqlDbType = SqlDbType.Int;
                            param[4].Value = 0;

                            param[5] = new SqlParameter();
                            param[5].ParameterName = "condition";
                            param[5].SqlDbType = SqlDbType.Int;
                            param[5].Value = 2;

                            param[6] = new SqlParameter();
                            param[6].ParameterName = "totalrecordFetch";
                            param[6].SqlDbType = SqlDbType.Int;
                            param[6].Value = 1;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();

                            response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));

                            for (int i = 0; i < response.Count; i++)
                            {
                                dataLogListTemp[i].Id = response[i].Id;
                            }
                        }
                    }

                    if (dataLogListTemp.Count > 0)
                    {
                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;
                        });
                    }


                    dataLogListTemp = objHelper.ParseJPIColourCode(dataLogListTemp, plog.AircraftId);
                    return dataLogListTemp;
                } // using
            }
            catch (Exception ex)
            {

                return new List<DataLogListing>();
            }

        }


        public List<DataLogListing> GetDataLog(int profileId, int pilotLogId)
        {
            string engineCommandType = "JPI";
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var plog = context.PilotLogs.FirstOrDefault(pl => pl.Id == pilotLogId && !pl.Deleted);
                    if (plog != null)
                    {
                        engineCommandType = plog.CommandRecFrom ?? "";
                        var flightName = GetFlightName(plog.Date, plog.DayPIC).Replace("_", " ");
                        Misc objMisc = new Misc();
                    }
                    else
                    {
                        return new List<DataLogListing>();
                    }
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));

                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                              .AsEnumerable()
                                              .Select((s, index) => new
                                              {
                                                  FlightNo = index + 1,
                                                  RegistrationNo = s.AircraftProfile.Registration,
                                                  pilotLogId = s.Id,
                                              }).Where(p => p.pilotLogId == pilotLogId).FirstOrDefault();

                    string flightNo = flightList.FlightNo.ToString();
                    string RegNo = flightList.RegistrationNo;

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = pilotLogId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);


                    var response = new List<AirframeDatalog>(); // context.AirframeDatalogs.Where(d => d.PilotLogId == pilotLogId);
                    var dataLogListTemp = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                    response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                    {
                        Id = m.Field<int>("ID"),
                        GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                        PilotLogId = m.Field<int>("PilotLogId"),
                        DataLog = m.Field<string>("DataLog")
                    }).ToList();

                    response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));
                    if (dataLogListTemp.Count > 0)
                    {
                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;
                        });
                    }
                    return dataLogListTemp;
                }
            }
            catch (Exception)
            {
                return new List<DataLogListing>();
            }
        }




        public Byte[] ExportDatalog(int profileId, int pilotlogId, out string outFileName)
        {
            outFileName = string.Empty;

            //outFileName = outFileName.Replace(" ", "").Replace(":", "-");

            var context = new GuardianAvionicsEntities();
            try
            {
                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;

                dataLogList = GetDataLog(profileId, pilotlogId);

                List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));


                var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                          .AsEnumerable()
                                          .Select((s, index) => new
                                          {
                                              FlightNo = index + 1,
                                              RegistrationNo = s.AircraftProfile.Registration,
                                              pilotLogId = s.Id,
                                              CommandRecFrom = s.CommandRecFrom
                                          }).ToList();


                flightList = flightList.Where(p => p.pilotLogId == pilotlogId).ToList();


                string engineCommandType = "JPI";
                var flightNumber = 0;
                var registrationNumber = "";
                foreach (var item in flightList.ToList())
                {
                    flightNumber = item.FlightNo;
                    engineCommandType = item.CommandRecFrom;
                    registrationNumber = item.RegistrationNo;
                }

                if (dataLogList.Count > 0)
                {

                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        FlightNumber = s.FlightNumber,
                        RegistrationNumber = s.AircraftNNumber,
                        Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = (s.Oat),
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = s.Pitch,
                        Roll = s.Roll,
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = s.VOLTS2,
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = s.AMP2,
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = (s.FF),
                        OilP = s.OILP,
                        OilT = s.OILT,
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = s.Yaw,
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD,
                        CalculatedFuelRemaining = s.CalculatedFuelRemaining,
                        TotalAircraftTime = s.TotalAircraftTime,
                        EngineTime = s.EngineTime,
                        ElevatorTrimPosition = s.ElevatorTrimPosition,
                        UnitsIndicator = s.UnitsIndicator,
                        FlapPosition = s.FlapPosition,
                        UnitsIndicator2 = s.UnitsIndicator2,
                        CarbTemp = s.CarbTemp,
                        UnitsIndicator3 = s.UnitsIndicator3,
                        CoolantPressure = s.CoolantPressure,
                        UnitsIndicator4 = s.UnitsIndicator4,
                        CoolantTemperature = s.CoolantTemperature,
                        UnitsIndicator5 = s.UnitsIndicator5,
                        UnitsIndicator6 = s.UnitsIndicator6,
                        AileronTrimPosition = s.AileronTrimPosition,
                        UnitsIndicator7 = s.UnitsIndicator7,
                        RubberTrimPosition = s.RubberTrimPosition,
                        UnitsIndicator8 = s.UnitsIndicator8,
                        FuelQty3 = s.FuelQty3,
                        UnitsIndicator9 = s.UnitsIndicator9,
                        FuelQty4 = s.FuelQty4,
                        UnitsIndicator10 = s.UnitsIndicator10,
                        DiscreteInput1 = s.DiscreteInput1,
                        DiscreteInput2 = s.DiscreteInput2,
                        DiscreteInput3 = s.DiscreteInput3,
                        DiscreteInput4 = s.DiscreteInput4,
                        IgnStatus = s.IgnStatus,
                        SensorStatus = s.SensorStatus,
                        ThrottlePosition = s.ThrottlePosition,
                        Baro = s.Baro,
                        Airtemp = s.Airtemp,
                        EcuTemp = s.EcuTemp,
                        Batteryvoltage = s.Batteryvoltage,
                        Sen1 = s.Sen1,
                        Sen2 = s.Sen2,
                        Sen3 = s.Sen3,
                        Sen4 = s.Sen4,
                        Sen5 = s.Sen5
                    });
                }
                else
                {
                    return new Byte[] { };
                }

                // genrate excel sheet and get byte array.
                byte[] byteArray = { };

                // fetch other details for file from db.
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);
                var pilotLog = context.PilotLogs.Include(a => a.AircraftProfile).FirstOrDefault(pl => pl.Id == pilotlogId);


                bool isSuccess;
                if (pilotLog != null)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    var fileName = GetFlightName(pilotLog.AircraftProfile.Registration, flightNumber, userDetails.FirstName, userDetails.LastName, pilotLog.Date);

                    //var fileName = String.Format("Engine_Data_For_{0}_Z_time.xlsx", flightName).Replace(" ", "").Replace(":", "-").Replace("/", "-"); ;
                    outFileName = fileName;

                    //ExceptionHandler.ReportError(new Exception(fileName), "ExportAllDataToPcPilotLog file name");
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject("ExportAllDataToPcPilotLog file name"), new Exception(fileName));
                    isSuccess = DumpExcel(collection,
                                            pilotLog.AircraftProfile.Registration ?? string.Empty,
                                            pilotLog.AircraftProfile.AircraftType ?? string.Empty,
                                            pilotLog.AircraftProfile.AeroUnitNo ?? string.Empty,
                                            userDetails.FirstName ?? string.Empty,
                                            fileName ?? string.Empty, engineCommandType, out byteArray);
                }
                else
                {
                    isSuccess = DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, "Flight_Data_For_Flight.xlsx", engineCommandType, out byteArray);
                    outFileName = "Flight_Data_For_Flight.xlsx";
                }
                return (isSuccess == true ? byteArray : new byte[] { });
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception pilotlogId =" + pilotlogId + ", profileId= " + profileId, e);
                return new Byte[] { };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        /// <summary>
        /// Fetch All the Jpi data for user profile Id for given pilot log , identified by pilot log id
        /// </summary>
        /// <param name="profileId">User profile Id</param>
        /// <param name="pageNo">Page No. for pagination</param>
        /// <param name="pilotLogId">Id for pilot log</param>
        /// <param name="count">No. of pages</param>
        /// <returns>List of DataLogListing</returns>
        public List<DataLogListing> ListJpiData(int profileId, int pageNo, int[] arrPilotLogId, out int count, out string labelText, out string engineCommandType)
        {
            engineCommandType = "JPI";
            labelText = string.Empty;
            count = 0;
            try
            {
                if (profileId < 1)
                    return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    var objEngineCommandType = context.PilotLogs.Where(p => arrPilotLogId.ToList().Contains(p.Id)).Select(s => s.CommandRecFrom);
                    if (objEngineCommandType != null)
                    {
                        engineCommandType = string.Join(",", objEngineCommandType.ToList());
                    }
                    var dataLogListTemp = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();


                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));
                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                              .AsEnumerable()
                                              .Select((s, index) => new
                                              {
                                                  FlightNo = index + 1,
                                                  RegistrationNo = s.AircraftProfile.Registration,
                                                  pilotLogId = s.Id,
                                                  DayPic = s.DayPIC
                                              })
                                                .ToList();

                    flightList = flightList.Where(p => arrPilotLogId.Contains(p.pilotLogId)).ToList();
                    double totalFlightTime = 0.00;

                    foreach (var item in flightList)
                    {
                        totalFlightTime += new Misc().ConvertHHMMToMinutes(item.DayPic);
                        context.AirframeDatalogs.Where(d => d.PilotLogId == item.pilotLogId).ToList().ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                    }


                    dataLogListTemp.ForEach(a =>
                    {
                        a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                        a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                    });
                    labelText = "Showing Data for multiple flights.  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(totalFlightTime);

                    //var airframedatalog =
                    //    context.AirframeDatalogs.Where(a => arrPilotLogId.ToList().Contains(a.PilotLogId)).Select(s => s.DataLog).ToList();
                    //labelText = "Showing Data for multiple flights.";
                    //if (airframedatalog.Count > 0)
                    //{
                    //    var dataLog = new DataLogListing();
                    //    foreach (var airframedata in airframedatalog)
                    //    {
                    //        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                    //    }

                    //    dataLogList.ForEach(a =>
                    //    {
                    //        a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                    //        a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                    //    });

                    //}
                    //return dataLogList;

                    return dataLogListTemp;
                } // using
            }
            catch (Exception)
            {
                return new List<DataLogListing>();
            }

        }

        /// <summary>
        /// Fetch All the Jpi data for user profile Id for given aircraft, identified by aircaft id
        /// </summary>
        /// <param name="profileId">User profile Id</param>
        /// <param name="pageNo">Page No. for pagination</param>
        /// <param name="aircraftId"></param>
        /// <param name="count">No. of pages</param>
        /// <returns>List of DataLogListing</returns>
        public List<DataLogListing> ListJpiDataAircraft(int profileId, int pageNo, int aircraftId, out int count, out string labelText, out string engineCommandType)
        {
            labelText = string.Empty;
            engineCommandType = "JPI";
            try
            {
                count = 0;
                engineCommandType = "JPI";
                if (profileId < 1)
                    return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    var objEngineCommandType = context.PilotLogs.Where(p => p.Finished && p.AircraftId == aircraftId && p.CommandRecFrom != null).Select(s => s.CommandRecFrom);
                    if (objEngineCommandType != null)
                    {
                        engineCommandType = string.Join(",", objEngineCommandType.ToList());
                    }
                    else
                    {
                        engineCommandType = "JPI";
                    }

                    var aircraft = context.AircraftProfiles.FirstOrDefault(ap => ap.Id == aircraftId && !ap.Deleted);

                    if (aircraft != default(AircraftProfile))
                    {
                        labelText = "Showing engine data for " + "Aircraft " + aircraft.Make + " " + aircraft.Registration;
                    }



                    #region flight Number
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                    var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId) && (pl.AircraftId == aircraftId));


                    var pCount = pilotLogs.Count();
                    var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                                .AsEnumerable()
                                                .Select((s, index) => new
                                                {
                                                    FlightNo = index + 1,
                                                    RegistrationNo = s.AircraftProfile.Registration,
                                                    DataLog = s.AirframeDatalogs.ToList(),
                                                    DayPic = s.DayPIC
                                                })
                                                .ToList();

                    double TotalFlightTime = 0.00;
                    var dataLogListTemp = new List<DataLogListing>();

                    foreach (var item in flightList)
                    {
                        TotalFlightTime += new Misc().ConvertHHMMToMinutes(item.DayPic);
                        item.DataLog.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                    }
                    labelText += "  Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(TotalFlightTime);
                    count = dataLogListTemp.Count;

                    #endregion flight Number


                    dataLogListTemp.ForEach(a =>
                    {
                        a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                        a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                    });


                    //var dataLogList = new List<DataLogListing>();

                    //var airframedatalog =
                    //    context.AirframeDatalogs.Include(s=>s.PilotLog).Include(s=>s.PilotLog.AircraftProfile).Where(a => a.PilotLog.AircraftId == aircraftId && a.PilotLog.Finished).Select(s => new { s.DataLog, s.PilotLog.AircraftProfile.Registration }).ToList();
                    //labelText = "Showing Data for multiple flights.";

                    //if (airframedatalog.Count > 0)
                    //{
                    //    count = airframedatalog.Count;
                    //    var dataLog = new DataLogListing();
                    //    foreach (var airframedata in airframedatalog.ToList())
                    //    {
                    //        //dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                    //        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + airframedata.Registration + "\"}")));
                    //    }

                    //    //dataLogList.ForEach(a =>
                    //    //{
                    //    //    a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                    //    //    a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                    //    //});
                    //    CommonHelper objHelper = new CommonHelper();
                    //    dataLogList = objHelper.ParseJPIColourCode(dataLogList, aircraftId);

                    //}
                    //return dataLogList;
                    return dataLogListTemp;
                } // using

            }

            catch (Exception)
            {
                count = 0;
                return new List<DataLogListing>();
            }
        }

        public List<DataLogListing> ListJpiDataDateTime(int profileId, int pageNo, string toDate, string toTime,
            string fromDate, string fromTime, out int totalCount, out string labelText)
        {
            labelText = String.Format("Showing engine data from {0} {1} to {2} {3}", fromDate, fromTime, toDate, toTime);
            try
            {
                totalCount = 0;

                if (profileId < 1)
                    return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    DateTime objFromDate = DateTime.Parse(Convert.ToString(fromDate));
                    DateTime objToDate = DateTime.Parse(Convert.ToString(toDate));

                    #region Adding Time to date

                    // for adding toTime to toDate
                    if (toTime.Contains("am"))
                    {
                        var t = toTime.Replace("am", "");
                        string[] tarray = t.Split(':');

                        objToDate = objToDate.Add(new TimeSpan(Convert.ToInt16(tarray[0]), Convert.ToInt16(tarray[1]), 0));
                    }
                    else
                    {
                        var t = toTime.Replace("pm", "");
                        string[] tarray = t.Split(':');

                        objToDate = objToDate.Add(new TimeSpan(Convert.ToInt16(tarray[0]) + 12, Convert.ToInt16(tarray[1]), 0));
                    }

                    // for adding fromTime to fromDate
                    if (fromTime.Contains("am"))
                    {
                        var t = fromTime.Replace("am", "");
                        string[] tarray = t.Split(':');

                        objFromDate = objFromDate.Add(new TimeSpan(Convert.ToInt16(tarray[1]), Convert.ToInt16(tarray[0]), 0));
                    }
                    else
                    {
                        var t = fromTime.Replace("pm", "");
                        string[] tarray = t.Split(':');

                        objFromDate = objFromDate.Add(new TimeSpan(Convert.ToInt16(tarray[1]) + 12, Convert.ToInt16(tarray[0]), 0));
                    }

                    #endregion Adding Time to date

                    var dataLogList = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airframedatalog =
                    //    context.AirframeDatalogs.Where(a => a.PilotLog.Date >= objFromDate && a.PilotLog.Date <= objToDate && a.PilotLog.Finished).OrderByDescending(o=>o.Id).Select(s => s.DataLog).ToList();
                    var airframedatalog =
                        context.AirframeDatalogs.Where(a => a.PilotLog.Date >= objFromDate && a.PilotLog.Date <= objToDate).Select(s => s.DataLog).ToList();
                    labelText = "Showing Data for multiple flights.";

                    if (airframedatalog.Count > 0)
                    {
                        totalCount = airframedatalog.Count;
                        var dataLog = new DataLogListing();
                        foreach (var airframedata in airframedatalog)
                        {
                            dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        }

                        dataLogList.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                        });

                    }
                    return dataLogList;
                }// using ends here
            }
            catch (Exception)
            {
                totalCount = 0;
                return new List<DataLogListing>();
            }
        }

        /// <summary>
        /// Flight listing for user profile , contins from and to date .
        /// </summary>
        /// <param name="profileId">proflie Id</param>
        /// <param name="pageNo">page No. for pagination</param>
        /// <param name="count">number of pages</param>
        /// <returns>list of dataloglisting</returns>
        public List<DataLogListing> GetFlightList(int profileId, int pageNo, out int count)
        {
            count = 0;
            try
            {
                if (profileId == default(int))
                    return new List<DataLogListing>();
                Misc objMisc = new Misc();
                //AircraftDetail
                using (var context = new GuardianAvionicsEntities())
                {
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pCount = context.PilotLogs.Count(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId))); //&& pl.Finished   remove this condition
                    var flightList = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId))) //&& pl.Finished   remove this condition
                                                .Include(i => i.AircraftProfile)
                                                .OrderByDescending(o => o.Date)
                                             .AsEnumerable()
                                                .Select((s, index) => new DataLogListing
                                                {
                                                    Text = (pCount - index).ToString() + " Flight " + s.FlightId + " - " + s.AircraftProfile.Registration.ToUpper() + " - " + Misc.GetStringOnlyDateUS(s.Date) + " " + (s.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture) + " (Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(s.DayPIC)) + ")",
                                                    pilotLogId = s.Id,
                                                })
                                                .ToList();



                    return flightList;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId= " + profileId, e);

            }
            return new List<DataLogListing>();
        }

        public List<DataLogListing> GetFlightListByAircraftId(int profileId, int aircraftId, out string aircraftRegNo)
        {
            aircraftRegNo = string.Empty;
            try
            {
                if (profileId == default(int))
                    return new List<DataLogListing>();
                Misc objMisc = new Misc();
                //AircraftDetail
                using (var context = new GuardianAvionicsEntities())
                {
                    List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                    var pCount = context.PilotLogs.Count(pl => pl.AircraftId == aircraftId && (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId))); //&& pl.Finished   remove this condition
                    var flightList = context.PilotLogs.Where(pl => pl.AircraftId == aircraftId && (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId))) //&& pl.Finished   remove this condition
                                                .Include(i => i.AircraftProfile)
                                                .OrderByDescending(o => o.Date)
                                             .AsEnumerable()
                                                .Select((s, index) => new DataLogListing
                                                {
                                                    Text = (pCount - index).ToString() + " Flight " + s.FlightId + " - " + s.AircraftProfile.Registration.ToUpper() + " - " + Misc.GetStringOnlyDateUS(s.Date) + " " + (s.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture) + " (Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(s.DayPIC)) + ")",
                                                    pilotLogId = s.Id,
                                                })
                                                .ToList();

                    var aircraftProfile = context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId);
                    if (aircraftProfile != null)
                    {
                        aircraftRegNo = "Flight List for aircraft = " + aircraftProfile.Registration;
                    }

                    return flightList;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId= " + profileId, e);

            }
            return new List<DataLogListing>();
        }

        /// <summary>
        /// Counts the No. of pages.
        /// </summary>
        /// <param name="totalCount">Total number of records</param>
        /// <returns>No. of pages</returns>
        public int JpiDataNoOfPages(int totalCount)
        {
            int pages = Convert.ToInt32(Math.Ceiling((double)totalCount / Constants.PageSizeTable));
            if (pages == 0)
                return 1;

            return pages;
        }

        /// <summary>
        /// Creates the list of aircraft for given profile Id whith text and Id.
        /// </summary>
        /// <param name="profileId">User Profile id</param>
        /// <param name="pageNo">page No. for pageination</param>
        /// <param name="totalCount">No. of pages</param>
        /// <returns>list of dataloglistng </returns>
        public List<DataLogListing> GetAircraftList(int profileId)
        {
            var dataLogListing = new List<DataLogListing>();
            try
            {
                if (profileId > 0)
                    using (var context = new GuardianAvionicsEntities())
                    {
                        List<int> aircraftListForManufacturerUser = new List<int>();

                        aircraftListForManufacturerUser = GetAircraftListForManufacturerUser(profileId);

                        dataLogListing = context.AircraftProfiles.Where(ap => (context.MappingAircraftAndPilots
                                           .Where(map => map.ProfileId == profileId && !map.Deleted)
                                           .Select(s => s.AircraftId)
                                           .ToList()
                                           .Contains(ap.Id) || ap.OwnerProfileId == profileId || aircraftListForManufacturerUser.Contains(ap.Id) || (context.PilotLogs.Where(p => p.PilotId == profileId || p.CoPilotId == profileId).ToList().Select(ss => ss.AircraftId).Contains(ap.Id))) && !ap.Deleted && !context.MappingAircraftAndPilots.Where(m => m.ProfileId == profileId && m.Deleted).Select(s => s.AircraftId).ToList().Contains(ap.Id)).Select(s => new DataLogListing()
                                           {
                                               pilotLogId = s.Id,
                                               //Text = "Aircraft " + s.Make + " " + s.Registration,
                                               Text = s.Registration,
                                               AircraftSerialNo = s.AircraftSerialNo,
                                               AircraftModelNo = (s.Model == null) ? s.OtheAircraftrModel : s.AircraftModelList.ModelName
                                           }).ToList();
                    }
            }
            catch (Exception)
            {

            }
            return dataLogListing;
        }

        /// <summary>
        /// Creates .xlsx sheets and save it on server
        /// </summary>
        /// <param name="jpi">list of dataloglisting Data for which xlxs sheets have to be made</param>
        /// <param name="pilotLogId">user profile id</param>
        /// <param name="registration">Aircraft registration</param>
        /// <param name="aircraftType">Aircraft  type</param>
        /// <param name="aeroUnitNo">Aircraft Aero unit number</param>
        /// <param name="pilotName">User name</param>
        /// <param name="fileName">File name to be saved on server</param>
        /// <returns></returns>
        private bool DumpExcel(IEnumerable<JPIDataExcelSheets> jpi, string registration,
            string aircraftType, string aeroUnitNo, string pilotName, string fileName, System.Security.Principal.WindowsIdentity identity)
        {
            try
            {
                //string fileName = @"\pilotLogId1" + pilotLogId + ".xlsx";
                //FileInfo newFile = new FileInfo(ConfigurationReader.ImagePathKey + @"\test2.xlsx");

                var filepath = ConfigurationReader.DropboxExcelSheetPath + @"\" + fileName;
                var newFile = new FileInfo(@filepath);

                using (var pck = new ExcelPackage(newFile))
                {

                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Engine Data");

                    #region HeadersAndUnits , Aircraft details

                    ws.Cells["A1"].Value = "Aircraft N-Number" + ":" + registration;
                    ws.Cells["B1"].Value = "Aircraft Type" + ":" + aircraftType;
                    ws.Cells["C1"].Value = "A/C Hardware S/N" + ":" + aeroUnitNo;
                    ws.Cells["D1"].Value = "Pilots Name" + ":" + pilotName;

                    ws.Cells["A3"].Value = "FlightNumber";
                    ws.Cells["B"].Value = "Aircraft N-Number";
                    ws.Cells["C3"].Value = "Lcl Date";
                    ws.Cells["D3"].Value = "Lcl Time";
                    ws.Cells["E3"].Value = "UTCOfst";
                    ws.Cells["F3"].Value = "AtvWpt";
                    ws.Cells["G3"].Value = "Latitude";
                    ws.Cells["H3"].Value = "Longitude";
                    ws.Cells["I3"].Value = "AltB";
                    ws.Cells["J3"].Value = "BaroA";
                    ws.Cells["K3"].Value = "AltMSL";
                    ws.Cells["L3"].Value = "OAT";
                    ws.Cells["M3"].Value = "IAS";

                    ws.Cells["N3"].Value = "GndSpd";
                    ws.Cells["O3"].Value = "VSpd";
                    ws.Cells["P3"].Value = "Pitch";
                    ws.Cells["Q3"].Value = "Roll";
                    ws.Cells["R3"].Value = "LatAc";
                    ws.Cells["S3"].Value = "NormAc";
                    ws.Cells["T3"].Value = "HDG";
                    ws.Cells["U3"].Value = "TRK";
                    ws.Cells["V3"].Value = "Volt1";
                    ws.Cells["W3"].Value = "Volt2";
                    ws.Cells["X3"].Value = "AMP1";
                    ws.Cells["Y3"].Value = "AMP2";
                    ws.Cells["Z3"].Value = "FQtyL";
                    ws.Cells["AA3"].Value = "FQtyR";
                    ws.Cells["AB3"].Value = "E1 FFlow";
                    ws.Cells["AC3"].Value = "E1 OilP";
                    ws.Cells["AD3"].Value = "E1 OilT";
                    ws.Cells["AE3"].Value = "E1 MAP";
                    ws.Cells["AF3"].Value = "E1 RPM";
                    ws.Cells["AG3"].Value = "E1 Cht1";
                    ws.Cells["AH3"].Value = "E1 Cht2";
                    ws.Cells["AI3"].Value = "E1 Cht3";
                    ws.Cells["AJ3"].Value = "E1 Cht4";
                    ws.Cells["AK3"].Value = "E1 Cht5";
                    ws.Cells["AL3"].Value = "E1 Cht6";
                    ws.Cells["AM3"].Value = "E1 Egt1";
                    ws.Cells["AN3"].Value = "E1 Egt2";
                    ws.Cells["AO3"].Value = "E1 Egt3";
                    ws.Cells["AP3"].Value = "E1 Egt4";
                    ws.Cells["AQ3"].Value = "E1 Egt5";
                    ws.Cells["AR3"].Value = "E1 Egt6";
                    ws.Cells["AS3"].Value = "E1 Tit1";
                    ws.Cells["AT3"].Value = "AltGPS";
                    ws.Cells["AU3"].Value = "TAS";
                    ws.Cells["AV3"].Value = "HSIS";
                    ws.Cells["AW3"].Value = "CRS";
                    ws.Cells["AX3"].Value = "NAV1";
                    ws.Cells["AY3"].Value = "NAV2";
                    ws.Cells["AZ3"].Value = "COM1";
                    ws.Cells["BA3"].Value = "COM2";
                    ws.Cells["AB3"].Value = "HCDI";
                    ws.Cells["BC3"].Value = "VCDI";
                    ws.Cells["BD3"].Value = "WndSpd";
                    ws.Cells["BE3"].Value = "WndDr";
                    ws.Cells["BF3"].Value = "WptDst";
                    ws.Cells["BG3"].Value = "WptBrg";
                    ws.Cells["BH3"].Value = "MagVar";
                    ws.Cells["BI3"].Value = "AfcsOn";
                    ws.Cells["BJ3"].Value = "RollM";
                    ws.Cells["BK3"].Value = "PitchM";
                    ws.Cells["BL3"].Value = "RollC";
                    ws.Cells["BM3"].Value = "PichC";
                    ws.Cells["BN3"].Value = "VSpdG";
                    ws.Cells["BO3"].Value = "GPSfix";
                    ws.Cells["BP3"].Value = "HAL";
                    ws.Cells["BQ3"].Value = "VAL";
                    ws.Cells["BR3"].Value = "HPLwas";
                    ws.Cells["BS3"].Value = "HPLfd";
                    ws.Cells["BT3"].Value = "VPLwas";
                    ws.Cells["BU3"].Value = "YAW";


                    // units for all the column
                    ws.Cells["A4"].Value = "";
                    ws.Cells["B4"].Value = "";
                    ws.Cells["C4"].Value = "mm-dd-yyyy";
                    ws.Cells["D4"].Value = "hh:mm:ss";
                    ws.Cells["E4"].Value = "hh:mm";
                    ws.Cells["F4"].Value = "ident";
                    ws.Cells["G4"].Value = "degrees";
                    ws.Cells["H4"].Value = "degrees";
                    ws.Cells["I4"].Value = "ft Baro";
                    ws.Cells["J4"].Value = "inch";
                    ws.Cells["K4"].Value = "ft msl";
                    ws.Cells["L4"].Value = "deg C";
                    ws.Cells["M4"].Value = "kt";

                    ws.Cells["N4"].Value = "kt";
                    ws.Cells["O4"].Value = "fpm";
                    ws.Cells["P4"].Value = "deg";
                    ws.Cells["Q4"].Value = "deg";
                    ws.Cells["R4"].Value = "G";
                    ws.Cells["S4"].Value = "G";
                    ws.Cells["T4"].Value = "deg";
                    ws.Cells["U4"].Value = "deg";
                    ws.Cells["V4"].Value = "volts";
                    ws.Cells["W4"].Value = "volts";
                    ws.Cells["X4"].Value = "amps";
                    ws.Cells["Y4"].Value = "amps";
                    ws.Cells["Z4"].Value = "gals";
                    ws.Cells["AA4"].Value = "gals";
                    ws.Cells["AB4"].Value = "gph";
                    ws.Cells["AC4"].Value = "deg F";
                    ws.Cells["AD4"].Value = "psi";
                    ws.Cells["AE4"].Value = "Hg";
                    ws.Cells["AF4"].Value = "rpm";
                    ws.Cells["AG4"].Value = "deg F";
                    ws.Cells["AH4"].Value = "deg F";
                    ws.Cells["AI4"].Value = "deg F";
                    ws.Cells["AJ4"].Value = "deg F";
                    ws.Cells["AK4"].Value = "deg F";
                    ws.Cells["AL4"].Value = "deg F";
                    ws.Cells["AM4"].Value = "deg F";
                    ws.Cells["AN4"].Value = "deg F";
                    ws.Cells["AO4"].Value = "deg F";
                    ws.Cells["AP4"].Value = "deg F";
                    ws.Cells["AQ4"].Value = "deg F";
                    ws.Cells["AR4"].Value = "deg F";
                    ws.Cells["AS4"].Value = "deg F";
                    ws.Cells["AT4"].Value = "ft wgs";
                    ws.Cells["AU4"].Value = "kt";
                    ws.Cells["AV4"].Value = "enum";
                    ws.Cells["AW4"].Value = "deg";
                    ws.Cells["AX4"].Value = "MHz";
                    ws.Cells["AY4"].Value = "MHz";
                    ws.Cells["AZ4"].Value = "MHz";
                    ws.Cells["BA4"].Value = "MHz";
                    ws.Cells["BB4"].Value = "fsd";
                    ws.Cells["BC4"].Value = "fsd";
                    ws.Cells["BD4"].Value = "kt";
                    ws.Cells["BE4"].Value = "deg";
                    ws.Cells["BF4"].Value = "nm";
                    ws.Cells["BG4"].Value = "deg";
                    ws.Cells["BH4"].Value = "deg";
                    ws.Cells["BI4"].Value = "bool";
                    ws.Cells["BJ4"].Value = "enum";
                    ws.Cells["BK4"].Value = "enum";
                    ws.Cells["BL4"].Value = "deg";
                    ws.Cells["BM4"].Value = "deg";
                    ws.Cells["BN4"].Value = "fpm";
                    ws.Cells["BO4"].Value = "enum";
                    ws.Cells["BP4"].Value = "mt";
                    ws.Cells["BQ4"].Value = "mt";
                    ws.Cells["BR4"].Value = "mt";
                    ws.Cells["BS4"].Value = "mt";
                    ws.Cells["BT4"].Value = "mt";
                    ws.Cells["BU4"].Value = "degrees";

                    #endregion HeadersAndUnits

                    try
                    {
                        //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                        ws.Cells["A5"].LoadFromCollection(jpi);
                    }
                    catch (Exception e)
                    {
                        //ExceptionHandler.ReportError(e, "making excel file");
                        //logger.Fatal("Exception making excel file", e);
                    }
                    identity.Impersonate();

                    pck.Save();

                    return true;
                }// using
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return false;
            }
        }

        public DocumentResponseModel GetDocumentForApp(DocumnetRequestModel document)
        {
            var response = new DocumentResponseModel
            {
                ResponseCode = ((int)Enumerations.Documents.Success).ToString(),
                ResponseMessage = Enumerations.Documents.Success.GetStringValue(),
                ListOfDocuments = new List<DocumentModelForApp>(),
                ProfileId = document.ProfileId,
            };

            var context = new GuardianAvionicsEntities();
            var adminHelper = new AdminHelper();
            try
            {
                //validate document model
                if (!context.Profiles.Any(pr => pr.Id == document.ProfileId && !pr.Deleted))
                    return new DocumentResponseModel
                    {
                        ResponseCode = ((int)Enumerations.Documents.InvalidProfileId).ToString(),
                        ResponseMessage = Enumerations.Documents.InvalidProfileId.GetStringValue()
                    };

                var deletedDocIdList = context.AdminDocsDeletedFromUsers.Where(w => w.ProfileId == document.ProfileId).Select(s => s.DocumentId).ToList();
                if (deletedDocIdList == null)
                {
                    deletedDocIdList = new List<int>();
                }

                var aircraftIds =
                    context.MappingAircraftAndPilots.Where(m => m.ProfileId == document.ProfileId && !m.Deleted)
                        .Select(s => s.AircraftId)
                        .ToList();
                var doc = context.Documents.Where(dc => !deletedDocIdList.Contains(dc.Id) && !dc.Deleted && (dc.ProfileId == document.ProfileId || dc.IsForAll == true || aircraftIds.Contains(dc.AircraftId ?? 0))).ToList();

                doc = doc.GroupBy(x => x.Url).Select(x => x.First()).ToList();

                if (doc.Any())
                {
                    var path = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath;
                    var docForAll = doc.Where(d => d.IsForAll == true).ToList();
                    int count = 1;
                    // send all the docs which are for this user in response
                    foreach (var d in doc)
                    {
                        response.ListOfDocuments.Add(new DocumentModelForApp
                        {
                            //Deleted = d.Deleted,
                            SNO = count,
                            Id = d.Id,
                            LastUpdateDate = Misc.GetStringOfDate(d.LastUpdated),
                            Url = path + d.Url,
                            Name = d.FileName,
                            Title = d.Title,
                            IconURL = adminHelper.GetIconURL(d.FileName, d.MimeType),
                            FileSize = d.FileSize ?? 0,
                            IsForAll = d.IsForAll,
                            CanDeleteDocument = (!d.IsForAll && d.AircraftId == null),
                            MineType = d.MimeType ?? ""
                        });
                        count = count + 1;
                    }
                }
                return response;
            }
            catch (Exception e)
            {

                return response.Exception();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Fetches documents form database which are common for all or for current profile , they should not be deleted
        /// </summary>
        /// <param name="document">DocumnetRequestModel object</param>
        /// <returns>DocumentResponseModel object</returns>
        public DocumentResponseModelWeb GetDocumentForUser(DocumnetRequestModel document)
        {
            var response = new DocumentResponseModelWeb
            {
                ResponseCode = ((int)Enumerations.Logbook.Success).ToString(),
                ResponseMessage = Enumerations.Logbook.Success.GetStringValue(),
                ListOfDocuments = new List<DocumentModelWeb>(),
                ProfileId = document.ProfileId,
            };

            var context = new GuardianAvionicsEntities();

            var adminHelper = new AdminHelper();
            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new DocumentResponseModelWeb
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                //validate document model
                if (!context.Profiles.Any(pr => pr.Id == document.ProfileId && !pr.Deleted))
                    return response.Exception();

                var deletedDocIdList = context.AdminDocsDeletedFromUsers.Where(w => w.ProfileId == document.ProfileId).Select(s => s.DocumentId).ToList();
                if (deletedDocIdList == null)
                {
                    deletedDocIdList = new List<int>();
                }


                var aircraftIds =
                    context.MappingAircraftAndPilots.Where(m => m.ProfileId == document.ProfileId && !m.Deleted)
                        .Select(s => s.AircraftId)
                        .ToList();
                var doc = context.Documents.Where(dc => !deletedDocIdList.Contains(dc.Id) && !dc.Deleted && dc.FileSize != null && (dc.ProfileId == document.ProfileId || dc.IsForAll == true || aircraftIds.Contains(dc.AircraftId ?? 0))).ToList();

                doc = doc.GroupBy(x => x.Url).Select(x => x.First()).ToList();

                if (doc.Any())
                {
                    //var path = ConfigurationReader.DocumentServerPathKey + "/";
                    var path = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath;
                    var docForAll = doc.Where(d => d.IsForAll == true).ToList();
                    int count = doc.Count;
                    // send all the docs which are for this user in response
                    foreach (var d in doc)
                    {
                        response.ListOfDocuments.Add(new DocumentModelWeb
                        {
                            //Deleted = d.Deleted,
                            SNO = count,
                            Id = d.Id,
                            LastUpdateDate = Misc.GetStringOfDate(d.LastUpdated),
                            Url = path + d.Url,
                            Name = d.FileName,
                            Title = d.Title,
                            IconUrl = adminHelper.GetIconURL(d.FileName, d.MimeType),
                            FileSize = adminHelper.ToFileSize(Convert.ToInt64(d.FileSize ?? 0)),
                            IsForAll = d.IsForAll,
                            CanDeleteDocument = (!d.IsForAll && d.AircraftId == null),
                            MineType = d.MimeType ?? ""
                        });
                        count = count - 1;
                    }
                }
                return response;
            }
            catch (Exception e)
            {
                var errorLog = context.ErrorLogs.Create();
                errorLog.ModuleName = "API";
                errorLog.ErrorSource = "API - GetDocument, MethodName - GetDocumentForUser()";
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(e);
                errorLog.ErrorDate = DateTime.UtcNow;
                errorLog.LastUpdateDate = DateTime.UtcNow;
                errorLog.ResolvedStatus = false;
                errorLog.RequestParameters = Newtonsoft.Json.JsonConvert.SerializeObject(document);
                context.ErrorLogs.Add(errorLog);
                return response.Exception();
            }
            finally
            {
                context.Dispose();
            }
        }

        public bool CheckDocumentForDuplicacy(string pDocName, int profileId, out int docId)
        {
            docId = 0;
            var context = new GuardianAvionicsEntities();
            try
            {
                var document = context.Documents.FirstOrDefault(dc => dc.ProfileId == profileId && !dc.Deleted && dc.FileName == pDocName);

                if (document != null)
                {
                    docId = document.Id;
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception pDocName =" + pDocName + " ProfileId= " + profileId, e);
                return false;
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public void DeleteDocument(System.Web.HttpPostedFileBase file, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var fileName = file.FileName;
                var doc = context.Documents.Where(dc => dc.Url == fileName);

                if (doc.Any())
                {
                    foreach (var d in doc)
                    {
                        // delete document
                        d.Deleted = true;

                        var filePath = ConfigurationReader.DocumentPathKey + @"\" + d.Url;
                        if (File.Exists(filePath))
                            File.Delete(filePath);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public DocumentResponseModelWeb CreateNewDocument(System.Web.HttpPostedFile file, int profileId, bool isDuplicate, string title, string uniqueName, int docId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var fileName = file.FileName;
                if (isDuplicate)
                {
                    var doc = context.Documents.Where(dc => dc.Id == docId);

                    if (doc.Any())
                    {
                        foreach (var d in doc)
                        {
                            // delete document
                            d.Deleted = true;

                            //var filePath = ConfigurationReader.DocumentPathKey + @"\" + d.Url;
                            //if (File.Exists(filePath))
                            //    File.Delete(filePath);
                            new Misc().DeleteFileFromS3Bucket(d.Url, "Doc");
                        }
                        context.SaveChanges();
                    }
                }
                int fileSizeInKB = Convert.ToInt32(file.ContentLength);
                //string path = ConfigurationReader.DocumentPathKey + @"\" + uniqueName;
                //file.SaveAs(path);
                new Misc().SaveFileToS3Bucket(uniqueName, file.InputStream, "Doc");
                var newDocument = new Document
                {
                    Deleted = false,
                    IsForAll = false,
                    LastUpdated = DateTime.UtcNow,
                    ProfileId = profileId,
                    Url = uniqueName,
                    Title = title,
                    FileName = fileName,
                    FileSize = fileSizeInKB,
                    MimeType = file.ContentType
                };
                context.Documents.Add(newDocument);
                context.SaveChanges();

                return GetDocumentForUser(new DocumnetRequestModel { ProfileId = profileId });
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
            return new DocumentResponseModelWeb();
        }


        /// <summary>
        /// Gets the url for document Id.
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        public string GetNameOfDocument(int DocumentId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var doc = context.Documents.FirstOrDefault(dc => dc.Id == DocumentId);
                if (doc != null)
                    return doc.Url;
                return string.Empty;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception DocumrntId=" + DocumentId, e);
                return string.Empty;
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        /// <summary>
        /// Deletes document from server , set flag for deleted in db.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentResponseModelWeb DeleteDocumentById(int documentId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var doc = context.Documents.FirstOrDefault(dc => dc.Id == documentId);

                if (doc != default(Document) && !string.IsNullOrEmpty(doc.Url))
                {
                    if (doc.IsForAll)
                    {
                        var adminDecDeleteByUser = context.AdminDocsDeletedFromUsers.Create();
                        adminDecDeleteByUser.DocumentId = documentId;
                        adminDecDeleteByUser.ProfileId = profileId;
                        context.AdminDocsDeletedFromUsers.Add(adminDecDeleteByUser);
                        context.SaveChanges();
                    }
                    else
                    {
                        // delete document
                        doc.Deleted = true;
                        context.SaveChanges();
                        new Misc().DeleteFileFromS3Bucket(doc.Url, "Doc");
                    }
                    return GetDocumentForUser(new DocumnetRequestModel { ProfileId = profileId });
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception documentId= " + documentId, e);
                return new DocumentResponseModelWeb();
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
            return new DocumentResponseModelWeb();
        }
        // fetch list 

        // save documetn

        public byte[] ExportAllDataToPcDateTime(string toDate, string toTime, string fromDate, string fromTime, int profileId, out string outFileName)
        {
            outFileName = String.Format("Engine_Data_For_{0}_{1}_to_{2}_{3}.xlsx", fromDate ?? string.Empty
                                        , fromTime ?? string.Empty, toDate ?? string.Empty, toTime ?? string.Empty).Replace(" ", "").Replace(":", "-").Replace("/", "-");

            var context = new GuardianAvionicsEntities();
            try
            {
                var DataLogList = new List<DataLogListing>();

                DateTime objFromDate = DateTime.Parse(Convert.ToString(fromDate));
                DateTime objToDate = DateTime.Parse(Convert.ToString(toDate));

                #region Adding Time to date

                // for adding toTime to toDate
                if (toTime.Contains("am"))
                {
                    var t = toTime.Replace("am", "");
                    string[] tarray = t.Split(':');

                    objToDate = objToDate.Add(new TimeSpan(Convert.ToInt16(tarray[0]), Convert.ToInt16(tarray[1]), 0));
                }
                else
                {
                    var t = toTime.Replace("pm", "");
                    string[] tarray = t.Split(':');

                    objToDate = objToDate.Add(new TimeSpan(Convert.ToInt16(tarray[0]) + 12, Convert.ToInt16(tarray[1]), 0));
                }

                // for adding fromTime to fromDate
                if (fromTime.Contains("am"))
                {
                    var t = fromTime.Replace("am", "");
                    string[] tarray = t.Split(':');

                    objFromDate = objFromDate.Add(new TimeSpan(Convert.ToInt16(tarray[1]), Convert.ToInt16(tarray[0]), 0));
                }
                else
                {
                    var t = fromTime.Replace("pm", "");
                    string[] tarray = t.Split(':');

                    objFromDate = objFromDate.Add(new TimeSpan(Convert.ToInt16(tarray[1]) + 12, Convert.ToInt16(tarray[0]), 0));
                }

                #endregion Adding Time to date

                var jpiLst = context.JPIUnitDatas.Where(j => j.UnitData.ProfileId == profileId && j.DateTime >= objFromDate && j.DateTime <= objToDate)
                                    .OrderByDescending(j => j.DateTime)
                                    .ToList();

                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;
                var airframedatalog = context.AirframeDatalogs.Where(a => (context.PilotLogs.Where(p => (p.PilotId == profileId || p.CoPilotId == profileId || p.AircraftProfile.OwnerProfileId == profileId) && p.Date >= objFromDate && p.Date <= objToDate && p.Finished).Select(s => s.Id).ToList().Contains(a.PilotLogId))).OrderByDescending(o => o.Id).Select(s => s.DataLog).ToList();
                if (airframedatalog.Count > 0)
                {
                    foreach (var airframedata in airframedatalog)
                    {
                        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                    }

                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = (s.Oat),
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = "",
                        Roll = "",
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = "",
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = "",
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = (s.FF),
                        OilP = "",
                        OilT = "",
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = "",
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD
                    });
                }
                else
                {
                    return new Byte[] { };
                }

                if (!jpiLst.Any())
                    return new Byte[] { };

                // convert to JPIDataExcelSheets
                //List<JPIDataExcelSheets> unitDataLst = ConvertJpiToDataLogExcelSheets(jpiLst);

                // genrate excel sheet and get byte array.
                byte[] byteArray = new byte[] { };

                // fetch other details for file from db.
                //var userName = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId).FirstName ?? string.Empty;

                bool isSuccess = false;
                //ExceptionHandler.ReportError(new Exception(outFileName), "ExportAllDataToPcDateTime file name");
                isSuccess = DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, outFileName, "", out byteArray);

                return (isSuccess == true ? byteArray : new byte[] { });

            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
                return new byte[] { };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        /// <summary>
        /// Fetches Engine data from db , converts them to List of DataLogListing objects.
        /// Saves the document on server , returns the url
        /// </summary>
        /// <param name="aircraftId"></param>
        /// <param name="profileId"></param>
        /// <param name="outFileName"></param>
        /// <returns>url of document on server</returns>
        public byte[] ExportAllDataToPcAircraft(int aircraftId, int profileId, out string outFileName)
        {
            outFileName = string.Empty;
            var context = new GuardianAvionicsEntities();
            try
            {
                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;

                string engineCommandType = "JPI";

                #region flight Number

                var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId) && (pl.AircraftId == aircraftId));
                var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                            .AsEnumerable()
                                            .Select((s, index) => new
                                            {
                                                FlightNo = index + 1,
                                                RegistrationNo = s.AircraftProfile.Registration,
                                                DataLog = s.AirframeDatalogs.ToList(),
                                                DayPic = s.DayPIC,
                                                CommandRecFrom = s.CommandRecFrom
                                            })
                                            .ToList();

                var objEngineCommandType = flightList.Where(p => p.CommandRecFrom != null).Select(s => s.CommandRecFrom).ToList();

                if (objEngineCommandType != null)
                {
                    engineCommandType = string.Join(",", objEngineCommandType.ToList());
                }
                else
                {
                    engineCommandType = "JPI";
                }

                double TotalFlightTime = 0.00;

                foreach (var item in flightList)
                {
                    TotalFlightTime += new Misc().ConvertHHMMToMinutes(item.DayPic);
                    item.DataLog.ForEach(f => dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                }
                #endregion flight Number

                if (flightList.Count > 0)
                {

                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        FlightNumber = s.FlightNumber,
                        RegistrationNumber = s.AircraftNNumber,
                        Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = s.Oat,
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = s.Pitch,
                        Roll = s.Roll,
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = s.VOLTS2,
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = s.AMP2,
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = (s.FF),
                        OilP = s.OILP,
                        OilT = s.OILT,
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = s.Yaw,
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD,
                        CalculatedFuelRemaining = s.CalculatedFuelRemaining,
                        TotalAircraftTime = s.TotalAircraftTime,
                        EngineTime = s.EngineTime,
                        ElevatorTrimPosition = s.ElevatorTrimPosition,
                        UnitsIndicator = s.UnitsIndicator,
                        FlapPosition = s.FlapPosition,
                        UnitsIndicator2 = s.UnitsIndicator2,
                        CarbTemp = s.CarbTemp,
                        UnitsIndicator3 = s.UnitsIndicator3,
                        CoolantPressure = s.CoolantPressure,
                        UnitsIndicator4 = s.UnitsIndicator4,
                        CoolantTemperature = s.CoolantTemperature,
                        UnitsIndicator5 = s.UnitsIndicator5,
                        UnitsIndicator6 = s.UnitsIndicator6,
                        AileronTrimPosition = s.AileronTrimPosition,
                        UnitsIndicator7 = s.UnitsIndicator7,
                        RubberTrimPosition = s.RubberTrimPosition,
                        UnitsIndicator8 = s.UnitsIndicator8,
                        FuelQty3 = s.FuelQty3,
                        UnitsIndicator9 = s.UnitsIndicator9,
                        FuelQty4 = s.FuelQty4,
                        UnitsIndicator10 = s.UnitsIndicator10,
                        DiscreteInput1 = s.DiscreteInput1,
                        DiscreteInput2 = s.DiscreteInput2,
                        DiscreteInput3 = s.DiscreteInput3,
                        DiscreteInput4 = s.DiscreteInput4,
                        IgnStatus = s.IgnStatus,
                        SensorStatus = s.SensorStatus,
                        ThrottlePosition = s.ThrottlePosition,
                        Baro = s.Baro,
                        Airtemp = s.Airtemp,
                        EcuTemp = s.EcuTemp,
                        Batteryvoltage = s.Batteryvoltage,
                        Sen1 = s.Sen1,
                        Sen2 = s.Sen2,
                        Sen3 = s.Sen3,
                        Sen4 = s.Sen4,
                        Sen5 = s.Sen5

                    });
                }
                else
                {
                    return new Byte[] { };
                }

                // genrate excel sheet and get byte array.
                byte[] byteArray = new byte[] { };

                // fetch other details for file from db.
                var aircraft = context.AircraftProfiles.FirstOrDefault(ap => ap.Id == aircraftId);
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);

                bool isSuccess = false;
                if (aircraft != null && userDetails != null)
                {
                    var fileName = "Engine_Data_For_Aicraft_Tail_Number_" + (aircraft.Registration ?? string.Empty) + ".xlsx";
                    fileName = fileName.Replace(" ", "").Replace(":", "-").Replace("/", "-");

                    //ExceptionHandler.ReportError(new Exception(fileName), "ExportAllDataToPcAircraft file name");
                    //logger.Fatal("Exception ExportAllDataToPcAircraft file name", new Exception(fileName));
                    outFileName = fileName;
                    isSuccess = DumpExcel(collection, aircraft.Registration ?? string.Empty, aircraft.AircraftType ?? string.Empty,
                                                    aircraft.AeroUnitNo ?? string.Empty, userDetails.FirstName ??
                                                    string.Empty, fileName ?? string.Empty, engineCommandType, out byteArray);
                }
                else
                {
                    isSuccess = DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, "Flight_Data_For_Aicraft.xlsx", engineCommandType, out byteArray);
                    outFileName = "Flight_Data_For_Aicraft.xlsx";
                }


                return (isSuccess == true ? byteArray : new byte[] { });
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception aircraftId=" + aircraftId + " ,profileId= " + profileId + " ,outFileName =" + outFileName, e);
                return new Byte[] { };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public Byte[] ExportAllDataToPcPilotLog(int pilotlogId, int profileId, out string outFileName)
        {
            outFileName = string.Empty;

            //outFileName = outFileName.Replace(" ", "").Replace(":", "-");

            var context = new GuardianAvionicsEntities();
            try
            {
                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;

                List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));


                var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                          .AsEnumerable()
                                          .Select((s, index) => new
                                          {
                                              FlightNo = index + 1,
                                              RegistrationNo = s.AircraftProfile.Registration,
                                              pilotLogId = s.Id,
                                              CommandRecFrom = s.CommandRecFrom
                                          }).ToList();


                flightList = flightList.Where(p => p.pilotLogId == pilotlogId).ToList();


                string engineCommandType = "JPI";
                var flightNumber = 0;
                var registrationNumber = "";
                foreach (var item in flightList.ToList())
                {
                    flightNumber = item.FlightNo;
                    engineCommandType = item.CommandRecFrom;
                    registrationNumber = item.RegistrationNo;
                    context.AirframeDatalogs.Where(d => d.PilotLogId == item.pilotLogId)
                        .ToList().ForEach(f => dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));


                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = item.pilotLogId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);


                    var response = new List<AirframeDatalog>();


                    response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                    {

                        DataLog = m.Field<string>("DataLog")
                    }).ToList();

                    response.ForEach(f => dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));
                    if (dataLogList.Count > 0)
                    {
                        dataLogList.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                            a.AircraftNNumber = registrationNumber;
                            a.FlightNumber = flightNumber.ToString();
                        });
                    }
                }

                if (flightList.Count > 0)
                {

                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        FlightNumber = s.FlightNumber,
                        RegistrationNumber = registrationNumber,
                        //Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        //Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        Date = s.Date,
                        Time = s.Time,
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = (s.Oat),
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = s.Pitch,
                        Roll = s.Roll,
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = s.VOLTS2,
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = s.AMP2,
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = (s.FF),
                        OilP = s.OILP,
                        OilT = s.OILT,
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = s.Yaw,
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD,
                        CalculatedFuelRemaining = s.CalculatedFuelRemaining,
                        TotalAircraftTime = s.TotalAircraftTime,
                        EngineTime = s.EngineTime,
                        ElevatorTrimPosition = s.ElevatorTrimPosition,
                        UnitsIndicator = s.UnitsIndicator,
                        FlapPosition = s.FlapPosition,
                        UnitsIndicator2 = s.UnitsIndicator2,
                        CarbTemp = s.CarbTemp,
                        UnitsIndicator3 = s.UnitsIndicator3,
                        CoolantPressure = s.CoolantPressure,
                        UnitsIndicator4 = s.UnitsIndicator4,
                        CoolantTemperature = s.CoolantTemperature,
                        UnitsIndicator5 = s.UnitsIndicator5,
                        UnitsIndicator6 = s.UnitsIndicator6,
                        AileronTrimPosition = s.AileronTrimPosition,
                        UnitsIndicator7 = s.UnitsIndicator7,
                        RubberTrimPosition = s.RubberTrimPosition,
                        UnitsIndicator8 = s.UnitsIndicator8,
                        FuelQty3 = s.FuelQty3,
                        UnitsIndicator9 = s.UnitsIndicator9,
                        FuelQty4 = s.FuelQty4,
                        UnitsIndicator10 = s.UnitsIndicator10,
                        DiscreteInput1 = s.DiscreteInput1,
                        DiscreteInput2 = s.DiscreteInput2,
                        DiscreteInput3 = s.DiscreteInput3,
                        DiscreteInput4 = s.DiscreteInput4,
                        IgnStatus = s.IgnStatus,
                        SensorStatus = s.SensorStatus,
                        ThrottlePosition = s.ThrottlePosition,
                        Baro = s.Baro,
                        Airtemp = s.Airtemp,
                        EcuTemp = s.EcuTemp,
                        Batteryvoltage = s.Batteryvoltage,
                        Sen1 = s.Sen1,
                        Sen2 = s.Sen2,
                        Sen3 = s.Sen3,
                        Sen4 = s.Sen4,
                        Sen5 = s.Sen5
                    });
                }
                else
                {
                    return new Byte[] { };
                }

                // genrate excel sheet and get byte array.
                byte[] byteArray = { };

                // fetch other details for file from db.
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);
                var pilotLog = context.PilotLogs.Include(a => a.AircraftProfile).FirstOrDefault(pl => pl.Id == pilotlogId);


                bool isSuccess;
                if (pilotLog != null)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    var fileName = GetFlightName(pilotLog.AircraftProfile.Registration, flightNumber, userDetails.FirstName, userDetails.LastName, pilotLog.Date);

                    //var fileName = String.Format("Engine_Data_For_{0}_Z_time.xlsx", flightName).Replace(" ", "").Replace(":", "-").Replace("/", "-"); ;
                    outFileName = fileName;

                    //ExceptionHandler.ReportError(new Exception(fileName), "ExportAllDataToPcPilotLog file name");
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject("ExportAllDataToPcPilotLog file name"), new Exception(fileName));
                    isSuccess = DumpExcel(collection,
                                            pilotLog.AircraftProfile.Registration ?? string.Empty,
                                            pilotLog.AircraftProfile.AircraftType ?? string.Empty,
                                            pilotLog.AircraftProfile.AeroUnitNo ?? string.Empty,
                                            userDetails.FirstName ?? string.Empty,
                                            fileName ?? string.Empty, engineCommandType, out byteArray);
                }
                else
                {
                    isSuccess = DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, "Flight_Data_For_Flight.xlsx", engineCommandType, out byteArray);
                    outFileName = "Flight_Data_For_Flight.xlsx";
                }
                return (isSuccess == true ? byteArray : new byte[] { });
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception pilotlogId =" + pilotlogId + ", profileId= " + profileId, e);
                return new Byte[] { };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public Byte[] ExportAllDataToPcPilotLog(int[] pilotlogMultipleIds, int profileId, out string outFileName)
        {
            outFileName = string.Empty;

            var context = new GuardianAvionicsEntities();
            try
            {
                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;

                string engineCommandType = "JPI";

                var objEngineCommandType = context.PilotLogs.Where(p => pilotlogMultipleIds.ToList().Contains(p.Id)).Select(s => s.CommandRecFrom);
                if (objEngineCommandType != null)
                {
                    engineCommandType = string.Join(",", objEngineCommandType.ToList());
                }

                List<int> aircraftListOfManufacturerUserList = GetAircraftListForManufacturerUser(profileId);
                var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || aircraftListOfManufacturerUserList.Contains(pl.AircraftId)));
                var flightList = pilotLogs.Include(i => i.AircraftProfile)
                                          .AsEnumerable()
                                          .Select((s, index) => new
                                          {
                                              FlightNo = index + 1,
                                              RegistrationNo = s.AircraftProfile.Registration,
                                              pilotLogId = s.Id,
                                              DayPic = s.DayPIC
                                          })
                                            .ToList();

                flightList = flightList.Where(p => pilotlogMultipleIds.Contains(p.pilotLogId)).ToList();
                double totalFlightTime = 0.00;

                foreach (var item in flightList)
                {
                    totalFlightTime += new Misc().ConvertHHMMToMinutes(item.DayPic);
                    context.AirframeDatalogs.Where(d => d.PilotLogId == item.pilotLogId).ToList().ForEach(f => dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + item.RegistrationNo + "\",\"FlightNumber\":\"" + item.FlightNo + "\"}"))));
                }


                dataLogList.ForEach(a =>
                {
                    a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                    a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                });

                if (flightList.Count > 0)
                {


                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        FlightNumber = s.FlightNumber,
                        RegistrationNumber = s.AircraftNNumber,
                        Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = (s.Oat),
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = s.Pitch,
                        Roll = s.Roll,
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = "",
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = s.AMP2,
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = s.FF,
                        OilP = s.OILP,
                        OilT = s.OILT,
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = s.Yaw,
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD,
                        CalculatedFuelRemaining = s.CalculatedFuelRemaining,
                        TotalAircraftTime = s.TotalAircraftTime,
                        EngineTime = s.EngineTime,
                        ElevatorTrimPosition = s.ElevatorTrimPosition,
                        UnitsIndicator = s.UnitsIndicator,
                        FlapPosition = s.FlapPosition,
                        UnitsIndicator2 = s.UnitsIndicator2,
                        CarbTemp = s.CarbTemp,
                        UnitsIndicator3 = s.UnitsIndicator3,
                        CoolantPressure = s.CoolantPressure,
                        UnitsIndicator4 = s.UnitsIndicator4,
                        CoolantTemperature = s.CoolantTemperature,
                        UnitsIndicator5 = s.UnitsIndicator5,
                        UnitsIndicator6 = s.UnitsIndicator6,
                        AileronTrimPosition = s.AileronTrimPosition,
                        UnitsIndicator7 = s.UnitsIndicator7,
                        RubberTrimPosition = s.RubberTrimPosition,
                        UnitsIndicator8 = s.UnitsIndicator8,
                        FuelQty3 = s.FuelQty3,
                        UnitsIndicator9 = s.UnitsIndicator9,
                        FuelQty4 = s.FuelQty4,
                        UnitsIndicator10 = s.UnitsIndicator10,
                        DiscreteInput1 = s.DiscreteInput1,
                        DiscreteInput2 = s.DiscreteInput2,
                        DiscreteInput3 = s.DiscreteInput3,
                        DiscreteInput4 = s.DiscreteInput4,
                        IgnStatus = s.IgnStatus,
                        SensorStatus = s.SensorStatus,
                        ThrottlePosition = s.ThrottlePosition,
                        Baro = s.Baro,
                        Airtemp = s.Airtemp,
                        EcuTemp = s.EcuTemp,
                        Batteryvoltage = s.Batteryvoltage,
                        Sen1 = s.Sen1,
                        Sen2 = s.Sen2,
                        Sen3 = s.Sen3,
                        Sen4 = s.Sen4,
                        Sen5 = s.Sen5

                    });

                }
                else
                {
                    return new Byte[] { };
                }
                // genrate excel sheet and get byte array.
                byte[] byteArray;

                // fetch other details for file from db.
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);
                var dateArray = context.PilotLogs.Where(pl => pilotlogMultipleIds.Contains(pl.Id))
                                                .AsEnumerable()
                                                .Select(s => Misc.GetStringOnlyDate(s.Date))
                                                .ToList();

                string date = dateArray.Count() > 1 ? string.Join(",", dateArray).Replace(" ", "").Replace(":", "-").Replace("/", "-") : string.Empty;
                bool isSuccess;
                var fileName = GetFlightNameMultiplePilotLog(date, userDetails.FirstName, userDetails.LastName);

                outFileName = fileName;

                //ExceptionHandler.ReportError(new Exception(fileName), "ExportAllDataToPcPilotLog file name");
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject("ExportAllDataToPcPilotLog file name"), new Exception(fileName));
                isSuccess = DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, engineCommandType, out byteArray);
                return (isSuccess == true ? byteArray : new byte[] { });
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new Byte[] { };
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public byte[] ExportAllDataToPc(int profileId, out string outFileName)
        {
            outFileName = string.Empty;
            var context = new GuardianAvionicsEntities();
            try
            {

                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                IEnumerable<JPIDataExcelSheets> collection;
                var airframedatalog = context.AirframeDatalogs.Where(a => (context.PilotLogs.Where(p => p.PilotId == profileId || p.CoPilotId == profileId || p.AircraftProfile.OwnerProfileId == profileId).Select(s => s.Id).ToList().Contains(a.PilotLogId))).OrderByDescending(o => o.PilotLogId).ThenByDescending(t => t.Id).Select(s => new { s.DataLog, s.PilotLog.AircraftProfile.Registration }).ToList();
                string engineCommandType = "JPI";
                var objEngineCommandType = context.PilotLogs.Where(p => (p.PilotId == profileId || p.CoPilotId == profileId || p.AircraftProfile.OwnerProfileId == profileId) && !p.Finished).Select(s => s.CommandRecFrom);
                if (objEngineCommandType != null)
                {
                    engineCommandType = string.Join(",", objEngineCommandType.ToList());
                }
                else
                {
                    engineCommandType = "JPI";
                }

                if (airframedatalog.Count > 0)
                {
                    foreach (var airframedata in airframedatalog.ToList())
                    {
                        //dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata.DataLog.Replace("}", ",\"AircraftNNumber\":\"" + airframedata.Registration + "\"}")));
                    }

                    //JPIDataExcelSheets
                    collection = dataLogList.AsEnumerable().Select(s => new JPIDataExcelSheets
                    {
                        Date = (Convert.ToDateTime(s.Date)).ToShortDateString(),
                        Time = new DateTime(DateTime.Parse(s.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss"),
                        utcOfst = "",
                        WayPoint = "",
                        Latitude = Convert.ToDouble(s.Latitude),
                        Longitude = Convert.ToDouble(s.Longitude),
                        AltB = "",
                        BaroA = "",
                        AltMSL = "",
                        Oat = (s.Oat),
                        IAS = "",
                        GndSpd = (s.Speed),
                        VSpd = "",
                        Pitch = "",
                        Roll = "",
                        LatAc = "",
                        NormAc = "",
                        HDG = "",
                        TRK = "",
                        Volt1 = (s.VOLTS),
                        Volt2 = "",
                        AMP1 = Convert.ToString(s.AMP),
                        AMP2 = "",
                        FQL = Convert.ToString(s.FQL),
                        FQR = Convert.ToString(s.FQR),
                        FF = (s.FF),
                        OilP = "",
                        OilT = "",
                        MAP = (s.MAP),
                        RPM = (s.RPM),
                        Cht1 = (s.Cht1),
                        Cht2 = (s.Cht2),
                        Cht3 = (s.Cht3),
                        Cht4 = (s.Cht4),
                        Cht5 = (s.Cht5),
                        Cht6 = (s.Cht6),
                        Egt1 = (s.Egt1),
                        Egt2 = (s.Egt2),
                        Egt3 = (s.Egt3),
                        Egt4 = (s.Egt4),
                        Egt5 = (s.Egt5),
                        Egt6 = (s.Egt6),
                        Tit = (s.Tit),
                        GpsAltitude = (s.Altitude),
                        TAS = "",
                        HSIS = "",
                        CRS = "",
                        NAV1 = "",
                        NAV2 = "",
                        COM1 = "",
                        COM2 = "",
                        HCDI = "",
                        VCDI = "",
                        WndSpd = "",
                        WndDr = "",
                        WptDst = "",
                        WptBrg = "",
                        MagVar = "",
                        AfcsOn = "",
                        RollM = "",
                        PitchM = "",
                        RollC = "",
                        PichC = "",
                        VSpdG = "",
                        GPSfix = "",
                        HAL = "",
                        VAL = "",
                        HPLwas = Convert.ToString(s.HP),
                        HPLfd = "",
                        VPLwas = "",
                        Yaw = "",
                        CDT = s.CDT,
                        CLD = s.CLD,
                        END = s.END,
                        FP = s.FP,
                        IAT = s.IAT,
                        MPG = s.MPG,
                        REM = s.REM,
                        REQ = s.REQ,
                        RES = s.RES,
                        USD = s.USD

                    });

                }
                else
                {
                    return new Byte[] { };
                }

                //if (!jpiLst.Any())
                //    return new Byte[] { };

                // convert to JPIDataExcelSheets
                //List<JPIDataExcelSheets> unitDataLst = UnitDataHelper.ConvertJpiToDataLogExcelSheets(jpiLst);

                // genrate excel sheet and get byte array.
                byte[] byteArray = new byte[] { };

                // fetch other details for file from db.
                var userDetails = context.UserDetails.FirstOrDefault(pr => pr.ProfileId == profileId);

                var fileName = "Engine_Data.xlsx";
                outFileName = fileName;

                bool isSuccess = userDetails != null ? DumpExcel(collection, string.Empty, string.Empty, string.Empty, userDetails.FirstName ?? string.Empty, fileName, engineCommandType, out byteArray)
                    : DumpExcel(collection, string.Empty, string.Empty, string.Empty, string.Empty, fileName, engineCommandType, out byteArray);


                return (isSuccess ? byteArray : new byte[] { });


                //return new byte[] { };
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new byte[] { };
            }
            finally
            {
                // dispose context
                context.Dispose();
            }
        }

        /// <summary>
        /// Saves the Engine data on server and returns the url
        /// </summary>
        /// <param name="list">List of data to save</param>
        /// <returns>Url of document on server</returns>
        public string SaveListOnServer(List<JPIDataExcelSheets> list, GuardianAvionicsEntities context)
        {
            if (!list.Any())
                return string.Empty;

            return string.Empty;

        }


        /// <summary>
        /// Coverts JPI data list into JPIDataExcelSheets data list
        /// </summary>
        /// <param name="jpiList"></param>
        /// <returns>List of JPIDataExcelSheets</returns>
        public static List<JPIDataExcelSheets> ConvertJpiToDataLogExcelSheets(List<JPIUnitData> jpiList)
        {
            try
            {
                return jpiList.Select(s => new JPIDataExcelSheets
                {
                    Date = Misc.GetStringOnlyDate(s.DateTime),
                    Time = s.DateTime.HasValue ? s.DateTime.Value.ToString("hh:mm:ss") : string.Empty,
                    utcOfst = Misc.GetStringOnlyDate(s.UTCOfst),

                    WayPoint = s.WayPoint ?? string.Empty,
                    Latitude = Convert.ToDouble(s.Latitude),
                    Longitude = Convert.ToDouble(s.Longitude),

                    AltB = Convert.ToString(s.AltB),
                    BaroA = Convert.ToString(s.BravoA),
                    AltMSL = Convert.ToString(s.AltMSL),

                    Oat = (s.Oat == null ? "" : Convert.ToString(s.Oat)),
                    IAS = Convert.ToString(s.IAS),

                    GndSpd = Convert.ToString(s.GndSpd),
                    VSpd = Convert.ToString(s.VSpd),
                    Pitch = Convert.ToString(s.Pitch),
                    Roll = Convert.ToString(s.Roll),
                    LatAc = Convert.ToString(s.LatAc),
                    NormAc = Convert.ToString(s.NormAc),
                    HDG = Convert.ToString(s.HDG),
                    TRK = Convert.ToString(s.TRK),

                    Volt1 = (s.Volt1 == null ? "" : Convert.ToString(s.Volt1)),
                    Volt2 = Convert.ToString(s.Volt2),

                    AMP1 = Convert.ToString(s.AMP1),
                    AMP2 = Convert.ToString(s.AMP2),

                    FQL = Convert.ToString(s.FQL),
                    FQR = Convert.ToString(s.FQR),

                    FF = (s.FF == null ? "" : Convert.ToString(s.Volt1)),
                    OilP = s.OilP.ToString(),
                    OilT = s.OilT.ToString(),

                    MAP = (s.MAP == null ? "" : Convert.ToString(s.MAP)),
                    RPM = (s.RPM == null ? "" : Convert.ToString(s.RPM)),

                    Cht1 = (s.Cht1 == null ? "" : Convert.ToString(s.Cht1)),
                    Cht2 = (s.Cht2 == null ? "" : Convert.ToString(s.Cht2)),
                    Cht3 = (s.Cht3 == null ? "" : Convert.ToString(s.Cht3)),
                    Cht4 = (s.Cht4 == null ? "" : Convert.ToString(s.Cht4)),
                    Cht5 = (s.Cht5 == null ? "" : Convert.ToString(s.Cht5)),
                    Cht6 = (s.Cht6 == null ? "" : Convert.ToString(s.Cht6)),
                    Egt1 = (s.Egt1 == null ? "" : Convert.ToString(s.Egt1)),
                    Egt2 = (s.Egt2 == null ? "" : Convert.ToString(s.Egt2)),
                    Egt3 = (s.Egt3 == null ? "" : Convert.ToString(s.Egt3)),
                    Egt4 = (s.Egt4 == null ? "" : Convert.ToString(s.Egt4)),
                    Egt5 = (s.Egt5 == null ? "" : Convert.ToString(s.Egt5)),
                    Egt6 = (s.Egt6 == null ? "" : Convert.ToString(s.Egt6)),

                    Tit = (s.Tit == null ? "" : Convert.ToString(s.Tit)),
                    GpsAltitude = (s.GpsAltitude == null ? "" : Convert.ToString(s.GpsAltitude)),
                    TAS = Convert.ToString(s.TAS),

                    HSIS = Convert.ToString(s.HSIS),
                    CRS = Convert.ToString(s.CRS),
                    NAV1 = Convert.ToString(s.NAV1),
                    NAV2 = Convert.ToString(s.NAV2),
                    COM1 = Convert.ToString(s.COM1),
                    COM2 = Convert.ToString(s.COM2),
                    HCDI = Convert.ToString(s.HCDI),

                    VCDI = Convert.ToString(s.VCDI),
                    WndSpd = Convert.ToString(s.WndSpd),

                    WndDr = Convert.ToString(s.WndDr),

                    WptDst = Convert.ToString(s.WptDst),
                    WptBrg = Convert.ToString(s.WptBrg),

                    MagVar = Convert.ToString(s.MagVar),
                    AfcsOn = Convert.ToString(s.AfcsOn),

                    RollM = Convert.ToString(s.RollM),
                    PitchM = Convert.ToString(s.PitchM),

                    RollC = Convert.ToString(s.RollC),
                    PichC = Convert.ToString(s.PitchC),

                    VSpdG = Convert.ToString(s.VSpdG),
                    GPSfix = Convert.ToString(s.GPSfix),
                    HAL = Convert.ToString(s.HAL),
                    VAL = Convert.ToString(s.VAL),

                    HPLwas = Convert.ToString(s.HPLwas),
                    HPLfd = Convert.ToString(s.HPLft),

                    VPLwas = Convert.ToString(s.VPLwas),

                    Yaw = Convert.ToString(s.YAW),

                }).ToList();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(jpiList), e);
                return new List<JPIDataExcelSheets>();
            }
        }


        public bool DumpExcel(IEnumerable<JPIDataExcelSheets> jpi, string registration,
            string aircraftType, string aeroUnitNo, string pilotName, string fileName, string engineCommandType, out byte[] bytesArray)
        {

            engineCommandType = engineCommandType ?? "";
            bytesArray = new byte[] { };
            try
            {
                var filepath = ConfigurationReader.DropboxExcelSheetPath + @"\" + @fileName;
                var newFile = new FileInfo(@filepath);

                //using (var pck = new ExcelPackage(newFile))
                //{
                var pck = new ExcelPackage(newFile);
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Engine Data");



                //Do not display the warning 'number stored as text'
                #region HeadersAndUnits , Aircraft details


                ws.Cells["A1"].Value = "Aircraft N-Number" + ":" + registration;
                ws.Cells["B1"].Value = "Aircraft Type" + ":" + aircraftType;
                ws.Cells["C1"].Value = "A/C Hardware S/N" + ":" + aeroUnitNo;
                ws.Cells["D1"].Value = "Pilots Name" + ":" + pilotName;


                ws.Cells["A3"].Value = "FlightNumber";
                ws.Cells["B3"].Value = "Aircraft N-Number";
                ws.Cells["C3"].Value = "Lcl Date";
                ws.Cells["D3"].Value = "Lcl Time";
                ws.Cells["E3"].Value = "UTCOfst";
                ws.Cells["F3"].Value = "AtvWpt";
                ws.Cells["G3"].Value = "Latitude";
                ws.Cells["H3"].Value = "Longitude";
                ws.Cells["I3"].Value = "AltB";
                ws.Cells["J3"].Value = "BaroA";
                ws.Cells["K3"].Value = "AltMSL";
                ws.Cells["L3"].Value = "OAT";
                ws.Cells["M3"].Value = "IAS";

                ws.Cells["N3"].Value = "GndSpd";
                ws.Cells["O3"].Value = "VSpd";
                ws.Cells["P3"].Value = "Pitch";
                ws.Cells["Q3"].Value = "Roll";
                ws.Cells["R3"].Value = "LatAc";
                ws.Cells["S3"].Value = "NormAc";
                ws.Cells["T3"].Value = "HDG";
                ws.Cells["U3"].Value = "TRK";
                ws.Cells["V3"].Value = "Volt1";
                ws.Cells["W3"].Value = "Volt2";
                ws.Cells["X3"].Value = "AMP1";
                ws.Cells["Y3"].Value = "AMP2";
                ws.Cells["Z3"].Value = "FQtyL";
                ws.Cells["AA3"].Value = "FQtyR";
                ws.Cells["AB3"].Value = "E1 FFlow";
                ws.Cells["AC3"].Value = "E1 OilP";
                ws.Cells["AD3"].Value = "E1 OilT";
                ws.Cells["AE3"].Value = "E1 MAP";
                ws.Cells["AF3"].Value = "E1 RPM";
                ws.Cells["AG3"].Value = "E1 Cht1";
                ws.Cells["AH3"].Value = "E1 Cht2";
                ws.Cells["AI3"].Value = "E1 Cht3";
                ws.Cells["AJ3"].Value = "E1 Cht4";
                ws.Cells["AK3"].Value = "E1 Cht5";
                ws.Cells["AL3"].Value = "E1 Cht6";
                ws.Cells["AM3"].Value = "E1 Egt1";
                ws.Cells["AN3"].Value = "E1 Egt2";
                ws.Cells["AO3"].Value = "E1 Egt3";
                ws.Cells["AP3"].Value = "E1 Egt4";
                ws.Cells["AQ3"].Value = "E1 Egt5";
                ws.Cells["AR3"].Value = "E1 Egt6";
                ws.Cells["AS3"].Value = "E1 Tit1";
                ws.Cells["AT3"].Value = "AltGPS";
                ws.Cells["AU3"].Value = "TAS";
                ws.Cells["AV3"].Value = "HSIS";
                ws.Cells["AW3"].Value = "CRS";
                ws.Cells["AX3"].Value = "NAV1";
                ws.Cells["AY3"].Value = "NAV2";
                ws.Cells["AZ3"].Value = "COM1";
                ws.Cells["BA3"].Value = "COM2";
                ws.Cells["BB3"].Value = "HCDI";
                ws.Cells["BC3"].Value = "VCDI";
                ws.Cells["BD3"].Value = "WndSpd";
                ws.Cells["BE3"].Value = "WndDr";
                ws.Cells["BF3"].Value = "WptDst";
                ws.Cells["BG3"].Value = "WptBrg";
                ws.Cells["BH3"].Value = "MagVar";
                ws.Cells["BI3"].Value = "AfcsOn";
                ws.Cells["BJ3"].Value = "RollM";
                ws.Cells["BK3"].Value = "PitchM";
                ws.Cells["BL3"].Value = "RollC";
                ws.Cells["BM3"].Value = "PichC";
                ws.Cells["BN3"].Value = "VSpdG";
                ws.Cells["BO3"].Value = "GPSfix";
                ws.Cells["BP3"].Value = "HAL";
                ws.Cells["BQ3"].Value = "VAL";
                ws.Cells["BR3"].Value = "HPLwas";
                ws.Cells["BS3"].Value = "HPLfd";
                ws.Cells["BT3"].Value = "VPLwas";
                ws.Cells["BU3"].Value = "YAW";
                ws.Cells["BV3"].Value = "CDT";
                ws.Cells["BW3"].Value = "CLD";
                ws.Cells["BX3"].Value = "END";
                ws.Cells["BY3"].Value = "FP";
                ws.Cells["BZ3"].Value = "IAT";
                ws.Cells["CA3"].Value = "MPG";
                ws.Cells["CB3"].Value = "REM";
                ws.Cells["CC3"].Value = "REQ";
                ws.Cells["CD3"].Value = "RES";
                ws.Cells["CE3"].Value = "USD";
                if (engineCommandType.Contains("Garmin"))
                {
                    ws.Cells["CF3"].Value = "CalculatedFuelRemaining";
                    ws.Cells["CG3"].Value = "TotalAircraftTime";
                    ws.Cells["CH3"].Value = "EngineTime";
                    ws.Cells["CI3"].Value = "ElevatorTrimPosition";
                    ws.Cells["CJ3"].Value = "UnitsIndicator";
                    ws.Cells["CK3"].Value = "FlapPosition";
                    ws.Cells["CL3"].Value = "UnitsIndicator2";
                    ws.Cells["CM3"].Value = "CarbTemp";
                    ws.Cells["CN3"].Value = "UnitsIndicator3";
                    ws.Cells["CO3"].Value = "CoolantPressure";
                    ws.Cells["CP3"].Value = "UnitsIndicator4";
                    ws.Cells["CQ3"].Value = "CoolantTemperature";
                    ws.Cells["CR3"].Value = "UnitsIndicator5";
                    ws.Cells["CS3"].Value = "UnitsIndicator6";
                    ws.Cells["CT3"].Value = "AileronTrimPosition";
                    ws.Cells["CU3"].Value = "UnitsIndicator7";
                    ws.Cells["CV3"].Value = "RudderTrimPosition";
                    ws.Cells["CW3"].Value = "UnitsIndicator8";
                    ws.Cells["CX3"].Value = "FuelQty3";
                    ws.Cells["CY3"].Value = "UnitsIndicator9";
                    ws.Cells["CZ3"].Value = "FuelQty4";
                    ws.Cells["DA3"].Value = "UnitsIndicator10";
                    ws.Cells["DB3"].Value = "DiscreteInput1";
                    ws.Cells["DC3"].Value = "DiscreteInput2";
                    ws.Cells["DD3"].Value = "DiscreteInput3";
                    ws.Cells["DE3"].Value = "DiscreteInput4";
                }
                else if (engineCommandType.Contains("ULP"))
                {
                    ws.Cells["CF3"].Value = "IgnStatus";
                    ws.Cells["CG3"].Value = "SensorStatus";
                    ws.Cells["CH3"].Value = "ThrottlePosition";
                    ws.Cells["CI3"].Value = "Baro";
                    ws.Cells["CJ3"].Value = "Airtemp";
                    ws.Cells["CK3"].Value = "EcuTemp";
                    ws.Cells["CL3"].Value = "Batteryvoltage";
                    ws.Cells["CM3"].Value = "Sen1";
                    ws.Cells["CN3"].Value = "Sen2";
                    ws.Cells["CO3"].Value = "Sen3";
                    ws.Cells["CP3"].Value = "Sen4";
                    ws.Cells["CQ3"].Value = "Sen5";

                }

                // units for all the column
                ws.Cells["A4"].Value = "";
                ws.Cells["B4"].Value = "";
                ws.Cells["C4"].Value = "mm-dd-yyyy";
                ws.Cells["D4"].Value = "hh:mm:ss";
                ws.Cells["E4"].Value = "hh:mm";
                ws.Cells["F4"].Value = "ident";
                ws.Cells["G4"].Value = "degrees";
                ws.Cells["H4"].Value = "degrees";
                ws.Cells["I4"].Value = "ft Baro";
                ws.Cells["J4"].Value = "inch";
                ws.Cells["K4"].Value = "ft msl";
                ws.Cells["L4"].Value = "deg C";
                ws.Cells["M4"].Value = "kt";

                ws.Cells["N4"].Value = "kt";
                ws.Cells["O4"].Value = "fpm";
                ws.Cells["P4"].Value = "deg";
                ws.Cells["Q4"].Value = "deg";
                ws.Cells["R4"].Value = "G";
                ws.Cells["S4"].Value = "G";
                ws.Cells["T4"].Value = "deg";
                ws.Cells["U4"].Value = "deg";
                ws.Cells["V4"].Value = "volts";
                ws.Cells["W4"].Value = "volts";
                ws.Cells["X4"].Value = "amps";
                ws.Cells["Y4"].Value = "amps";
                ws.Cells["Z4"].Value = "gals";
                ws.Cells["AA4"].Value = "gals";
                ws.Cells["AB4"].Value = "gph";
                ws.Cells["AC4"].Value = "deg F";
                ws.Cells["AD4"].Value = "psi";
                ws.Cells["AE4"].Value = "Hg";
                ws.Cells["AF4"].Value = "rpm";
                ws.Cells["AG4"].Value = "deg F";
                ws.Cells["AH4"].Value = "deg F";
                ws.Cells["AI4"].Value = "deg F";
                ws.Cells["AJ4"].Value = "deg F";
                ws.Cells["AK4"].Value = "deg F";
                ws.Cells["AL4"].Value = "deg F";
                ws.Cells["AM4"].Value = "deg F";
                ws.Cells["AN4"].Value = "deg F";
                ws.Cells["AO4"].Value = "deg F";
                ws.Cells["AP4"].Value = "deg F";
                ws.Cells["AQ4"].Value = "deg F";
                ws.Cells["AR4"].Value = "deg F";
                ws.Cells["AS4"].Value = "deg F";
                ws.Cells["AT4"].Value = "ft wgs";
                ws.Cells["AU4"].Value = "kt";
                ws.Cells["AV4"].Value = "enum";
                ws.Cells["AW4"].Value = "deg";
                ws.Cells["AX4"].Value = "MHz";
                ws.Cells["AY4"].Value = "MHz";
                ws.Cells["AZ4"].Value = "MHz";
                ws.Cells["BA4"].Value = "MHz";
                ws.Cells["BB4"].Value = "fsd";
                ws.Cells["BC4"].Value = "fsd";
                ws.Cells["BD4"].Value = "kt";
                ws.Cells["BE4"].Value = "deg";
                ws.Cells["BF4"].Value = "nm";
                ws.Cells["BG4"].Value = "deg";
                ws.Cells["BH4"].Value = "deg";
                ws.Cells["BI4"].Value = "bool";
                ws.Cells["BJ4"].Value = "enum";
                ws.Cells["BK4"].Value = "enum";
                ws.Cells["BL4"].Value = "deg";
                ws.Cells["BM4"].Value = "deg";
                ws.Cells["BN4"].Value = "fpm";
                ws.Cells["BO4"].Value = "enum";
                ws.Cells["BP4"].Value = "mt";
                ws.Cells["BQ4"].Value = "mt";
                ws.Cells["BR4"].Value = "mt";
                ws.Cells["BS4"].Value = "mt";
                ws.Cells["BT4"].Value = "mt";
                ws.Cells["BU4"].Value = "degrees";

                ws.Cells["BV4"].Value = "";
                ws.Cells["BW4"].Value = "";
                ws.Cells["BX4"].Value = "";
                ws.Cells["BY4"].Value = "";
                ws.Cells["BZ4"].Value = "";
                ws.Cells["CA4"].Value = "";
                ws.Cells["CB4"].Value = "";
                ws.Cells["CC4"].Value = "";
                ws.Cells["CD4"].Value = "";
                ws.Cells["CE4"].Value = "";
                if (engineCommandType.Contains("Garmin"))
                {
                    ws.Cells["CF4"].Value = "gallon";
                    ws.Cells["CG4"].Value = "hours";
                    ws.Cells["CH4"].Value = "hours";
                    ws.Cells["CI4"].Value = "1 % of Travel";
                    ws.Cells["CJ4"].Value = "";
                    ws.Cells["CK4"].Value = "degree";
                    ws.Cells["CL4"].Value = "";
                    ws.Cells["CM4"].Value = "deg";
                    ws.Cells["CN4"].Value = "";
                    ws.Cells["CO4"].Value = "PSI";
                    ws.Cells["CP4"].Value = "";
                    ws.Cells["CQ4"].Value = "deg";
                    ws.Cells["CR4"].Value = "";
                    ws.Cells["CS4"].Value = "";
                    ws.Cells["CT4"].Value = "1 % of Travel";
                    ws.Cells["CU4"].Value = "";
                    ws.Cells["CV4"].Value = "1 % of Travel";
                    ws.Cells["CW4"].Value = "";
                    ws.Cells["CX4"].Value = "gallon";
                    ws.Cells["CY4"].Value = "";
                    ws.Cells["CZ4"].Value = "gallon";
                    ws.Cells["DA4"].Value = "";
                    ws.Cells["DB4"].Value = "";
                    ws.Cells["DC4"].Value = "";
                    ws.Cells["DD4"].Value = "";
                    ws.Cells["DE4"].Value = "";

                }
                else if (engineCommandType.Contains("ULP"))
                {
                    ws.Cells["CF4"].Value = "";
                    ws.Cells["CG4"].Value = "";
                    ws.Cells["CH4"].Value = "percent";
                    ws.Cells["CI4"].Value = "hPa";
                    ws.Cells["CJ4"].Value = "deg F";
                    ws.Cells["CK4"].Value = "deg F";
                    ws.Cells["CL4"].Value = "volts";
                    ws.Cells["CM4"].Value = "";
                    ws.Cells["CN4"].Value = "";
                    ws.Cells["CO4"].Value = "";
                    ws.Cells["CP4"].Value = "";
                    ws.Cells["CQ4"].Value = "";
                }

                #endregion HeadersAndUnits

                try
                {
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A5"].LoadFromCollection(jpi);

                }
                catch (Exception e)
                {
                    //ExceptionHandler.ReportError(e, "making excel file");
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject("making excel file"), e);
                }
                bytesArray = pck.GetAsByteArray();

                return true;
                //}// using
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return false;
            }
        }


        /// <summary>
        /// Gets the content type of file from extention of file.
        /// </summary>
        /// <param name="extention"></param>
        /// <returns>Content type or default</returns>
        public string GetContextTypeByFileExtention(string extention)
        {
            string contentType = "text/html";
            if (extention != string.Empty)
            {
                switch (extention)
                {
                    case Constants.Pdf: { contentType = "application/pdf"; } break;
                    case Constants.xls: { contentType = "application/vnd.ms-excel"; } break;
                    case Constants.xlsx: { contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; } break;
                    case Constants.doc: { contentType = "application/msword"; } break;
                    case Constants.docx: { contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; } break;
                    case Constants.ppt: { contentType = "application/vnd.ms-powerpoint"; } break;
                    case Constants.pptx: { contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation"; break; }
                    case Constants.rtf: { contentType = "application/rtf"; } break;
                    case Constants.key: { contentType = "application/x-iwork-keynote-sffkey"; } break;
                    case Constants.number: { contentType = "application/x-iwork-numbers-sffnumbers"; } break;
                    case Constants.pages: { contentType = "application/x-iwork-pages-sffpages "; } break;
                    case Constants.jpg: { contentType = "image/jpeg"; } break;
                    case Constants.jpeg: { contentType = "image/jpeg"; } break;
                    case Constants.png: { contentType = "image/png"; } break;
                    case Constants.txt: { contentType = "text/plain"; break; }
                    default: { contentType = "text/html"; break; }
                }
            }
            return contentType;
        }

        /// <summary>
        /// Gets the list of aircraft for user
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public GraphModel GetAircraftListForGraph(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                string userType = context.Profiles.FirstOrDefault(f => f.Id == profileId).UserMaster.UserType;
                List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                // aircraftMappedWithManufacturerUserList = GetAircraftListForManufacturerUser(profileId);

                var resposne = new GraphModel { AircraftNNumberList = new List<ListItems>() };
                resposne.flightList = new List<AircraftDetail>();

                var aircraftList = new AircraftHelper().GetAircraftList(profileId);
                //if (userType == "Maintenance")
                //{
                //    var aircraftIdList = context.MappingMaintenanceUserAndAircrafts.Where(a => a.ProfileId == profileId).Select(s => s.AircraftId).Distinct().ToList();
                //    listItems = context.AircraftProfiles.Where(ap => aircraftIdList.Contains(ap.Id)).Select(s => new ListItems
                //    {
                //        text = s.Registration,
                //        value = s.Id
                //    }).ToList();
                //}
                //else
                //{
                //    listItems = context.AircraftProfiles.Where(ap => (context.MappingAircraftAndPilots
                //                                                                  .Where(map => map.ProfileId == profileId && map.Deleted == false)
                //                                                                  .Select(s => s.AircraftId)
                //                                                                  .ToList()
                //                                                                  .Contains(ap.Id)) && !ap.Deleted).Select(s => new ListItems
                //                                                                  {
                //                                                                      text = s.Registration,
                //                                                                      value = s.Id
                //                                                                  }).ToList();
                //}
                int count = aircraftList.Count;
                if (count > 0)
                {
                    resposne.AircraftNNumberList.Add(new ListItems { text = "Select", value = 0 });
                    aircraftList.ForEach(
                        n => resposne.AircraftNNumberList.Add(new ListItems { text = n.aircraftNNumber, value = n.AircraftId }));
                }
                else
                {
                    resposne.AircraftNNumberList.Add(new ListItems { text = "No aircraft available", value = 0 });
                }
                return resposne;
            }
            catch (Exception e)
            {
                return new GraphModel();
            }
            finally
            {
                context.Dispose();
            }
        }


        /// <summary>
        /// Get the list of flight for selected aircraft for graph
        /// </summary>
        /// <param name="aircraftId"></param>
        /// <returns></returns>
        public GraphModel GetFlightListGraph(int aircraftId, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var resposne = new GraphModel { flightList = new List<AircraftDetail>() };
                var count = 0;

                //Check that the user is owner or not 
                var aircraftProfile =
                    context.AircraftProfiles.FirstOrDefault(a => a.OwnerProfileId == profileId && a.Id == aircraftId);
                if (aircraftProfile != null)
                {
                    count = context.PilotLogs.Count(pl => pl.AircraftId == aircraftId && pl.Finished && pl.FlightDataType == (int)Enumerations.FlightDataType.StoredData);
                    resposne.flightList = context.PilotLogs.Where(pl => pl.AircraftId == aircraftId && pl.Finished && pl.FlightDataType == (int)Enumerations.FlightDataType.StoredData)
                                           .Include(i => i.AircraftProfile)
                                           .OrderByDescending(o => o.Date)
                                           .AsEnumerable()
                                           .Select((s, index) => new AircraftDetail
                                           {
                                               SNO = (count - index),
                                               Text = (count - index) + " Flight " + s.FlightId + " - " + s.AircraftProfile.Registration.ToUpper() + " - " + Misc.GetStringOnlyDateUS(s.Date),
                                               Id = s.Id,
                                           }).ToList();
                }
                else
                {
                    count = context.PilotLogs.Count(pl => pl.AircraftId == aircraftId && (pl.PilotId == profileId || pl.CoPilotId == profileId) && pl.Finished && pl.FlightDataType == (int)Enumerations.FlightDataType.StoredData);
                    resposne.flightList = context.PilotLogs.Where(pl => pl.AircraftId == aircraftId && (pl.PilotId == profileId || pl.CoPilotId == profileId) && pl.Finished && pl.FlightDataType == (int)Enumerations.FlightDataType.StoredData)
                                           .Include(i => i.AircraftProfile)
                                           .OrderByDescending(o => o.Date)
                                           .AsEnumerable()
                                           .Select((s, index) => new AircraftDetail
                                           {
                                               Text = "Flight " + (count - index) + " - " + s.AircraftProfile.Registration.ToUpper() + " - " + Misc.GetStringOnlyDateUS(s.Date),
                                               Id = s.Id,
                                           }).ToList();
                }
                return resposne;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new GraphModel();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// sets select list for IFR Appchs
        /// </summary>
        /// <returns></returns>
        public List<ListItems> setIfrAppchsList()
        {
            var lst = new List<ListItems>();

            for (int i = 0; i <= 25; i++)
            {
                lst.Add(new ListItems
                {
                    text = i.ToString(),
                    value = i
                });
            }
            return lst;
        }

        public long[][][] DynamicGraphRecall()
        {
            var context = new GuardianAvionicsEntities();
            var response = new long[2][][];

            try
            {



                var tempArray = new long[200][];
                int i;
                var jpiDataLoop = new List<DataLogListing>();
                int jpiCountLoop = 0;

                //  CONVERT ARRAYS TO LIST => LIST TO ARRAYS , AS list are more faster and less memory consuming then arrays.
                DateTime dt = DateTime.Now;
                #region CHT 1

                // get cht1 arrays

                i = 0;
                jpiCountLoop = 1;


                tempArray = new long[jpiCountLoop][];

                for (int c = 0; c < jpiCountLoop; c++)
                {
                    tempArray[i] = new long[2];

                    tempArray[i][1] = Convert.ToInt64(new Random().Next(c, 500));
                    tempArray[i][0] = Misc.GetMilliSecondsFrom01011970(dt);

                    i++;
                }


                response[0] = new long[jpiCountLoop][];
                response[0] = tempArray;


                #endregion CHT 1

                #region CHT 2


                // get cht2 arrays

                i = 0;



                tempArray = new long[jpiCountLoop][];

                for (int c = 0; c < jpiCountLoop; c++)
                {
                    tempArray[i] = new long[2];

                    tempArray[i][1] = Convert.ToInt64(new Random().Next(c, 250));
                    tempArray[i][0] = Misc.GetMilliSecondsFrom01011970(dt);

                    i++;
                }


                response[1] = new long[jpiCountLoop][];
                response[1] = tempArray;



                #endregion CHT 2



                return response;


            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new long[][][] { };
            }
            finally
            {
                context.Dispose();
            }
        }

        public long[][][] DynamicGraphArray()
        {
            var context = new GuardianAvionicsEntities();
            var response = new long[2][][];

            try
            {



                var tempArray = new long[200][];
                int i;
                var jpiDataLoop = new List<DataLogListing>();
                int jpiCountLoop = 0;

                //  CONVERT ARRAYS TO LIST => LIST TO ARRAYS , AS list are more faster and less memory consuming then arrays.
                DateTime dt = DateTime.Now.AddSeconds(-200);
                #region CHT 1

                // get cht1 arrays

                i = 0;
                jpiCountLoop = 200;


                tempArray = new long[jpiCountLoop][];

                for (int c = 0; c < jpiCountLoop; c++)
                {
                    tempArray[i] = new long[2];

                    tempArray[i][1] = Convert.ToInt64(new Random().Next(c, 500));
                    tempArray[i][0] = Misc.GetMilliSecondsFrom01011970(dt.AddSeconds(c));

                    i++;
                }


                response[0] = new long[jpiCountLoop][];
                response[0] = tempArray;


                #endregion CHT 1

                #region CHT 2


                // get cht2 arrays

                i = 0;



                tempArray = new long[jpiCountLoop][];

                for (int c = 0; c < jpiCountLoop; c++)
                {
                    tempArray[i] = new long[2];

                    tempArray[i][1] = Convert.ToInt64(new Random().Next(c, 250));
                    tempArray[i][0] = Misc.GetMilliSecondsFrom01011970(dt.AddSeconds(c));

                    i++;
                }


                response[1] = new long[jpiCountLoop][];
                response[1] = tempArray;



                #endregion CHT 2



                return response;


            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new long[][][] { };
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flightids"> List of flight Ids</param>
        /// <param name="dt">get data from -5 to +5 min from the date passed</param>
        /// <returns></returns>
        public List<DataLogListing> GetDatalogForGraph(int[] flightId, DateTime dt, out string engineCommandType)
        {
            engineCommandType = "JPI";
            var context = new GuardianAvionicsEntities();
            try
            {

                List<int> flightIdList = flightId.ToList();

                var objEngineCommandType = context.PilotLogs.Where(p => flightIdList.Contains(p.Id) && p.Finished).Select(s => s.CommandRecFrom);
                if (objEngineCommandType != null)
                {
                    engineCommandType = string.Join(",", objEngineCommandType.ToList());
                }
                else
                {
                    engineCommandType = "JPI";
                }



                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "pilotLog";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = flightId[0];

                DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var airframedatalog = ds.Tables[0].AsEnumerable()
                            .Select(r => r.Field<string>("DataLog"))
                            .ToList();
                        foreach (var airframedata in airframedatalog)
                        {
                            dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        }
                        DateTime dtFrom = dt.AddMinutes(-5);
                        DateTime dtTo = dt.AddMinutes(5);
                        dataLogList = dataLogList.Where(d => Convert.ToDateTime(d.Date) >= dtFrom && Convert.ToDateTime(d.Date) <= dtTo).ToList();
                    }
                }
                return dataLogList;
            }
            catch (Exception ex)
            {
                return new List<DataLogListing>();
            }
        }
        /// <summary>
        /// Gets array of data for flights
        /// </summary>
        /// <returns></returns>
        public double[][][] GetGraphDataDoubleArray(int[] flightId, bool isDemoUser)
        {
            var context = new GuardianAvionicsEntities();
            var response = new double[18][][];

            try
            {
                List<int> flightIdList = flightId.ToList();
                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                //var airframedatalog =
                //    context.AirframeDatalogs.Where(a => flightIdList.Contains(a.PilotLogId)).Select(s => s.DataLog);
                DataSet ds = new DataSet();
                if (!isDemoUser)
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = flightId[0];

                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);
                }
                else
                {

                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, "select * from AirframeDatalogDemo");
                }


                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var airframedatalog = ds.Tables[0].AsEnumerable()
                          .Select(r => r.Field<string>("DataLog"))
                          .ToList();
                        foreach (var airframedata in airframedatalog)
                        {
                            dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        }
                        dataLogList.ForEach(d => d.TickTime = Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(d.Date)));
                    }
                }





                dataLogList = dataLogList.OrderBy(o => o.Date).ToList();
                int jpiCount = dataLogList.Count();

                if (jpiCount > 0)
                {
                    var tempArray = new double[jpiCount][];
                    int i;
                    var jpiDataLoop = new List<DataLogListing>();
                    int jpiCountLoop = 0;

                    //  CONVERT ARRAYS TO LIST => LIST TO ARRAYS , AS list are more faster and less memory consuming then arrays.

                    i = 0;
                    jpiCountLoop = dataLogList.Count();

                    response[0] = new double[jpiCount][];
                    response[1] = new double[jpiCount][];
                    response[2] = new double[jpiCount][];
                    response[3] = new double[jpiCount][];
                    response[4] = new double[jpiCount][];
                    response[5] = new double[jpiCount][];
                    response[6] = new double[jpiCount][];
                    response[7] = new double[jpiCount][];
                    response[8] = new double[jpiCount][];
                    response[9] = new double[jpiCount][];
                    response[10] = new double[jpiCount][];
                    response[11] = new double[jpiCount][];
                    response[12] = new double[jpiCount][];
                    response[13] = new double[jpiCount][];
                    response[14] = new double[jpiCount][];
                    response[15] = new double[jpiCount][];
                    response[16] = new double[jpiCount][];
                    response[17] = new double[jpiCount][];
                    if (jpiCountLoop > 0)
                    {
                        dataLogList.ForEach(f =>
                        {
                            f.Cht1 = string.IsNullOrEmpty(f.Cht1) ? "0" : f.Cht1;
                            f.Cht2 = string.IsNullOrEmpty(f.Cht2) ? "0" : f.Cht2;
                            f.Cht3 = string.IsNullOrEmpty(f.Cht3) ? "0" : f.Cht3;
                            f.Cht4 = string.IsNullOrEmpty(f.Cht4) ? "0" : f.Cht4;
                            f.Cht5 = string.IsNullOrEmpty(f.Cht5) ? "0" : f.Cht5;
                            f.Cht6 = string.IsNullOrEmpty(f.Cht6) ? "0" : f.Cht6;
                            f.Egt1 = string.IsNullOrEmpty(f.Egt1) ? "0" : f.Egt1;
                            f.Egt2 = string.IsNullOrEmpty(f.Egt2) ? "0" : f.Egt2;
                            f.Egt3 = string.IsNullOrEmpty(f.Egt3) ? "0" : f.Egt3;
                            f.Egt4 = string.IsNullOrEmpty(f.Egt4) ? "0" : f.Egt4;
                            f.Egt5 = string.IsNullOrEmpty(f.Egt5) ? "0" : f.Egt5;
                            f.Egt6 = string.IsNullOrEmpty(f.Egt6) ? "0" : f.Egt6;
                            f.TIT1 = string.IsNullOrEmpty(f.TIT1) ? "0" : f.TIT1;
                            f.RPM = string.IsNullOrEmpty(f.RPM) ? "0" : f.RPM;
                            f.FF = string.IsNullOrEmpty(f.FF) ? "0" : f.FF;
                            f.CLD = string.IsNullOrEmpty(f.CLD) ? "0" : f.CLD;
                            f.Speed = string.IsNullOrEmpty(f.Speed) ? "0" : f.Speed;
                            f.MAP = string.IsNullOrEmpty(f.MAP) ? "0" : f.MAP;
                        });

                        var cht1Array = new double[jpiCount][];
                        var cht2Array = new double[jpiCount][];
                        var cht3Array = new double[jpiCount][];
                        var cht4Array = new double[jpiCount][];
                        var cht5Array = new double[jpiCount][];
                        var cht6Array = new double[jpiCount][];
                        var egt1Array = new double[jpiCount][];
                        var egt2Array = new double[jpiCount][];
                        var egt3Array = new double[jpiCount][];
                        var egt4Array = new double[jpiCount][];
                        var egt5Array = new double[jpiCount][];
                        var egt6Array = new double[jpiCount][];
                        var TIT1Array = new double[jpiCount][];
                        var RPMArray = new double[jpiCount][];
                        var FFArray = new double[jpiCount][];
                        var CLDArray = new double[jpiCount][];
                        var SpeedArray = new double[jpiCount][];
                        var MAPArray = new double[jpiCount][];

                        foreach (var jpiRow in dataLogList)
                        {

                            cht1Array[i] = new double[2];
                            try
                            {
                                cht1Array[i][1] = Convert.ToDouble(jpiRow.Cht1);
                                cht1Array[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                            }
                            catch (Exception e)
                            {
                                cht1Array[i][1] = 0;
                                cht1Array[i][0] = jpiRow.TickTime;
                            }


                            cht2Array[i] = new double[2];
                            try
                            {
                                cht2Array[i][1] = Convert.ToDouble(jpiRow.Cht2);
                                cht2Array[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                            }
                            catch (Exception ex)
                            {
                                cht2Array[i][1] = 0;
                                cht2Array[i][0] = jpiRow.TickTime;
                            }


                            cht3Array[i] = new double[2];
                            try
                            {
                                cht3Array[i][1] = Convert.ToDouble(jpiRow.Cht3);
                                cht3Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                cht3Array[i][1] = 0;
                                cht3Array[i][0] = jpiRow.TickTime;
                            }


                            cht4Array[i] = new double[2];
                            try
                            {
                                cht4Array[i][1] = Convert.ToDouble(jpiRow.Cht4);
                                cht4Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                cht4Array[i][1] = 0;
                                cht4Array[i][0] = jpiRow.TickTime;
                            }

                            cht5Array[i] = new double[2];
                            try
                            {
                                cht5Array[i][1] = Convert.ToDouble(jpiRow.Cht5);
                                cht5Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                cht5Array[i][1] = 0;
                                cht5Array[i][0] = jpiRow.TickTime;
                            }


                            cht6Array[i] = new double[2];
                            try
                            {
                                cht6Array[i][1] = Convert.ToDouble(jpiRow.Cht6);
                                cht6Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                cht6Array[i][1] = 0;
                                cht6Array[i][0] = jpiRow.TickTime;
                            }


                            egt1Array[i] = new double[2];
                            try
                            {
                                egt1Array[i][1] = Convert.ToDouble(jpiRow.Egt1);
                                egt1Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                egt1Array[i][1] = 0;
                                egt1Array[i][0] = jpiRow.TickTime;
                            }


                            egt2Array[i] = new double[2];
                            try
                            {
                                egt2Array[i][1] = Convert.ToDouble(jpiRow.Egt2);
                                egt2Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                egt2Array[i][1] = 0;
                                egt2Array[i][0] = jpiRow.TickTime;
                            }


                            egt3Array[i] = new double[2];
                            try
                            {
                                egt3Array[i][1] = Convert.ToDouble(jpiRow.Egt3);
                                egt3Array[i][0] = jpiRow.TickTime;

                            }
                            catch (Exception ex)
                            {
                                egt3Array[i][1] = 0;
                                egt3Array[i][0] = jpiRow.TickTime;
                            }


                            egt4Array[i] = new double[2];
                            try
                            {
                                egt4Array[i][1] = Convert.ToDouble(jpiRow.Egt4);
                                egt4Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                egt4Array[i][1] = 0;
                                egt4Array[i][0] = jpiRow.TickTime;

                            }


                            egt5Array[i] = new double[2];
                            try
                            {
                                egt5Array[i][1] = Convert.ToDouble(jpiRow.Egt5);
                                egt5Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                egt5Array[i][1] = 0;
                                egt5Array[i][0] = jpiRow.TickTime;
                            }


                            egt6Array[i] = new double[2];
                            try
                            {
                                egt6Array[i][1] = Convert.ToDouble(jpiRow.Egt6);
                                egt6Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                egt6Array[i][1] = 0;
                                egt6Array[i][0] = jpiRow.TickTime;
                            }


                            TIT1Array[i] = new double[2];
                            try
                            {
                                TIT1Array[i][1] = Convert.ToDouble(jpiRow.TIT1);
                                TIT1Array[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                TIT1Array[i][1] = 0;
                                TIT1Array[i][0] = jpiRow.TickTime;
                            }


                            RPMArray[i] = new double[2];
                            try
                            {
                                RPMArray[i][1] = Convert.ToDouble(jpiRow.RPM);
                                RPMArray[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                RPMArray[i][1] = 0;
                                RPMArray[i][0] = jpiRow.TickTime;
                            }


                            FFArray[i] = new double[2];
                            try
                            {
                                FFArray[i][1] = Convert.ToDouble(jpiRow.FF);
                                FFArray[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                FFArray[i][1] = 0;
                                FFArray[i][0] = jpiRow.TickTime;
                            }


                            CLDArray[i] = new double[2];
                            try
                            {
                                CLDArray[i][1] = Convert.ToDouble(jpiRow.CLD);
                                CLDArray[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                CLDArray[i][1] = 0;
                                CLDArray[i][0] = jpiRow.TickTime;
                            }


                            SpeedArray[i] = new double[2];
                            try
                            {
                                SpeedArray[i][1] = Convert.ToDouble(jpiRow.Speed);
                                SpeedArray[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                SpeedArray[i][1] = 0;
                                SpeedArray[i][0] = jpiRow.TickTime;
                            }


                            MAPArray[i] = new double[2];
                            try
                            {
                                MAPArray[i][1] = Convert.ToDouble(jpiRow.MAP);
                                MAPArray[i][0] = jpiRow.TickTime;
                            }
                            catch (Exception ex)
                            {
                                MAPArray[i][1] = 0;
                                MAPArray[i][0] = jpiRow.TickTime;
                            }
                            i++;
                        }

                        response[0] = cht1Array;
                        response[1] = cht2Array;
                        response[2] = cht3Array;
                        response[3] = cht4Array;
                        response[4] = cht5Array;
                        response[5] = cht6Array;
                        response[6] = egt1Array;
                        response[7] = egt2Array;
                        response[8] = egt3Array;
                        response[9] = egt4Array;
                        response[10] = egt5Array;
                        response[11] = egt6Array;
                        response[12] = TIT1Array;
                        response[13] = RPMArray;
                        response[14] = FFArray;
                        response[15] = CLDArray;
                        response[16] = SpeedArray;
                        response[17] = MAPArray;
                    }



                    //#region CHT 1

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht1 != null).ToList();

                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    int c = 1;
                    //    tempArray = new double[jpiCountLoop][];
                    //    try
                    //    {
                    //        foreach (var jpiRow in jpiDataLoop)
                    //        {
                    //            c = c + 1;
                    //            tempArray[i] = new double[2];

                    //            tempArray[i][1] = Convert.ToDouble(jpiRow.Cht1);
                    //            tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //            i++;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }

                    //    response[0] = new double[jpiCountLoop][];
                    //    response[0] = tempArray;
                    //}
                    //else
                    //{
                    //    response[0] = new double[jpiCountLoop][];
                    //}

                    //#endregion CHT 1

                    //#region CHT 2

                    //// get cht2 arrays
                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht2 != null && jpiRow.Cht2 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Cht2);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //        i++;
                    //    }

                    //    response[1] = new double[jpiCountLoop][];
                    //    response[1] = tempArray;
                    //}
                    //else
                    //{
                    //    response[1] = new double[jpiCountLoop][];
                    //}


                    //#endregion CHT 2

                    //#region CHT 3

                    //// get cht3 arrays
                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht3 != null && jpiRow.Cht3 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Cht3);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //        i++;
                    //    }
                    //    response[2] = new double[jpiCountLoop][];
                    //    response[2] = tempArray;
                    //}
                    //else
                    //{
                    //    response[2] = new double[jpiCountLoop][];
                    //}
                    //#endregion CHT 3

                    //#region CHT 4

                    //// get cht4 arrays
                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht4 != null && jpiRow.Cht4 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Cht4);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //        i++;
                    //    }
                    //    response[3] = new double[jpiCountLoop][];
                    //    response[3] = tempArray;
                    //}
                    //else
                    //{
                    //    response[3] = new double[jpiCountLoop][];
                    //}
                    //#endregion CHT 4

                    //#region CHT 5

                    //// get cht5 arrays
                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht5 != null && jpiRow.Cht5 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Cht5);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //        i++;
                    //    }
                    //    response[4] = new double[jpiCountLoop][];
                    //    response[4] = tempArray;
                    //}
                    //else
                    //{
                    //    response[4] = new double[jpiCountLoop][];
                    //}

                    //#endregion CHT 5

                    //#region CHT 6

                    //// get cht6 arrays
                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Cht6 != null && jpiRow.Cht6 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Cht6);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));

                    //        i++;
                    //    }
                    //    response[5] = new double[jpiCountLoop][];
                    //    response[5] = tempArray;
                    //}
                    //else
                    //{
                    //    response[5] = new double[jpiCountLoop][];
                    //}

                    //#endregion CHT 6

                    //#region EGT 1

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt1 != null && jpiRow.Egt1 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt1);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[6] = new double[jpiCountLoop][];
                    //    response[6] = tempArray;
                    //}
                    //else
                    //{
                    //    response[6] = new double[jpiCountLoop][];
                    //}

                    //#endregion EGT 1

                    //#region EGT 2

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt2 != null && jpiRow.Egt2 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt2);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[7] = new double[jpiCountLoop][];
                    //    response[7] = tempArray;
                    //}
                    //else
                    //{
                    //    response[7] = new double[jpiCountLoop][];
                    //}

                    //#endregion EGT 2

                    //#region EGT 3

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt3 != null && jpiRow.Egt3 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt3);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[8] = new double[jpiCountLoop][];
                    //    response[8] = tempArray;
                    //}
                    //else
                    //{
                    //    response[8] = new double[jpiCountLoop][];
                    //}

                    //#endregion EGT 3

                    //#region EGT 4

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt4 != null && jpiRow.Egt4 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt4);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[9] = new double[jpiCountLoop][];
                    //    response[9] = tempArray;
                    //}
                    //else
                    //{
                    //    response[9] = new double[jpiCountLoop][];
                    //}
                    //#endregion EGT 4

                    //#region EGT 5

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt5 != null && jpiRow.Egt5 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt5);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[10] = new double[jpiCountLoop][];
                    //    response[10] = tempArray;
                    //}
                    //else
                    //{
                    //    response[10] = new double[jpiCountLoop][];
                    //}

                    //#endregion EGT 5

                    //#region EGT 6

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Egt6 != null && jpiRow.Egt6 != "").ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Egt6);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[11] = new double[jpiCountLoop][];
                    //    response[11] = tempArray;
                    //}
                    //else
                    //{
                    //    response[11] = new double[jpiCountLoop][];
                    //}
                    //#endregion EGT 6

                    //#region TIT1

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Tit != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.Tit);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[12] = new double[jpiCountLoop][];
                    //    response[12] = tempArray;
                    //}
                    //else
                    //{
                    //    response[12] = new double[jpiCountLoop][];
                    //}

                    //#endregion TIT1

                    //#region RPM

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.RPM != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];
                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];
                    //        try
                    //        {
                    //            tempArray[i][1] = Convert.ToDouble(jpiRow.RPM);
                    //            tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        }
                    //        catch (Exception exx)
                    //        {

                    //        }
                    //        i++;
                    //    }
                    //    response[13] = new double[jpiCountLoop][];
                    //    response[13] = tempArray;
                    //}
                    //else
                    //{
                    //    response[13] = new double[jpiCountLoop][];
                    //}

                    //#endregion RPM

                    //#region FF

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.FF != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.FF);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[14] = new double[jpiCountLoop][];
                    //    response[14] = tempArray;
                    //}
                    //else
                    //{
                    //    response[14] = new double[jpiCountLoop][];
                    //}

                    //#endregion FF

                    //#region CLD

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.CLD != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(jpiRow.CLD);
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[15] = new double[jpiCountLoop][];
                    //    response[15] = tempArray;
                    //}
                    //else
                    //{
                    //    response[15] = new double[jpiCountLoop][];
                    //}

                    //#endregion CLD

                    //#region Gnd Speend

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.Speed != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(Convert.ToDouble(jpiRow.Speed));
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[16] = new double[jpiCountLoop][];
                    //    response[16] = tempArray;
                    //}
                    //else
                    //{
                    //    response[16] = new double[jpiCountLoop][];
                    //}
                    //#endregion Gnd Speend

                    //#region MAP

                    //i = 0;
                    //jpiDataLoop = dataLogList.Where(jpiRow => jpiRow.MAP != null).ToList();
                    //jpiCountLoop = jpiDataLoop.Count;

                    //if (jpiCountLoop > 0)
                    //{
                    //    tempArray = new double[jpiCountLoop][];

                    //    foreach (var jpiRow in jpiDataLoop)
                    //    {
                    //        tempArray[i] = new double[2];

                    //        tempArray[i][1] = Convert.ToDouble(Convert.ToDouble(jpiRow.MAP));
                    //        tempArray[i][0] = jpiRow.TickTime; // Misc.GetMilliSecondsFrom01011970(Convert.ToDateTime(jpiRow.Date));
                    //        i++;
                    //    }
                    //    response[17] = new double[jpiCountLoop][];
                    //    response[17] = tempArray;
                    //}
                    //else
                    //{
                    //    response[17] = new double[jpiCountLoop][];
                    //}
                    //#endregion MAP

                    return response;
                }
                return new double[][][] { };
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new double[][][] { };
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Open KML file for a flight in google earth , if file dont exist then create it.
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public string GetUrlFlightPathOnGoogleEarth(int flightId)
        {
            string url = GetKmlDocumentUrl(flightId);

            if (url != string.Empty)
            {
                var fileName = ConfigurationReader.KmlFilePath + @"\" + String.Format(GAResource.KMLFileName, flightId);

                if (File.Exists(fileName))
                    url = ConfigurationReader.KmlFileServerPath + @"/" + String.Format(GAResource.KMLFileName, flightId);
                else
                    url = string.Empty;
            }
            return url;
        }

        /// <summary>
        /// Gets the server url for kml file.
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public string GetKmlDocumentUrl(int flightId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var fileName = ConfigurationReader.KmlFilePath + @"\" + String.Format(GAResource.KMLFileName, flightId);

                if (!File.Exists(fileName))
                {
                    var fName = GenerateKmlDocument(flightId, context);
                }

                if (File.Exists(fileName))
                    return ConfigurationReader.KmlFilePath + @"\" + String.Format(GAResource.KMLFileName, flightId);
                else
                    return ConfigurationReader.KmlFilePath + @"\" + "SamplesInMaps.kml";
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return string.Empty;
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Generate and save KML document on server.
        /// </summary>
        /// <param name="flightId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GenerateKmlDocument(int flightId, GuardianAvionicsEntities context)
        {
            try
            {
                // get the unitdataIds for current flight
                var unitDataIds = context.UnitDatas.Where(u => u.PilotLogId == flightId).Select(u => u.Id).ToList();

                // get jpi data for all the UnitdataIds
                var jpiData = context.JPIUnitDatas.Where(j => unitDataIds.Contains(j.UnitDataId)
                                                                && j.GpsAltitude != null
                                                                && j.Latitude != null
                                                                && j.Longitude != null
                                                                ).ToList();
                if (jpiData.Count == 0)
                    return string.Empty;

                var ls = new LineString();
                ls.Extrude = true;
                ls.Tessellate = false;
                ls.AltitudeMode = AltitudeMode.Absolute;

                var cc = new CoordinateCollection();

                // check for null is done in query
                jpiData.ForEach(a => cc.Add(new Vector((double)a.Latitude
                                                     , (double)a.Longitude
                                                     , (double)a.GpsAltitude)));
                ls.Coordinates = cc;

                // This is the Element we are going to save to the Kml file.
                var placemark = new Placemark();

                // add name
                placemark.Name = "Flight Path - p";

                // add description
                var desc = new Description { Text = "Shows the complete flight path - p" };
                placemark.Description = desc;

                // add style
                var styl = new Style();
                styl.Line = new LineStyle
                {
                    Color = SharpKml.Base.Color32.Parse("7f00ff00"),
                    Width = 4,
                };
                styl.Polygon = new PolygonStyle
                {
                    Color = SharpKml.Base.Color32.Parse("7f00ff00"),
                };
                placemark.AddStyle(styl);
                placemark.Geometry = ls;

                var container = new SharpKml.Dom.Document();
                desc.Text = "MainDoc - d";
                container.Description = desc;
                container.Name = "KML doc - d";
                container.AddFeature(placemark);

                // This allows us to save and Element easily.
                //KmlFile kml = KmlFile.Create(placemark, false);

                KmlFile kml = KmlFile.Create(container, false);

                var fileName = ConfigurationReader.KmlFilePath + @"\" + String.Format(GAResource.KMLFileName, flightId);
                using (var stream = System.IO.File.OpenWrite(fileName))
                {
                    kml.Save(stream);
                }

                return fileName;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return string.Empty;
            }
        }


        /// <summary>
        /// Get server url to download Kml file , if file dont exist then create it.
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public string GetUrlDownloadKmlFileForFlight(int flightId)
        {
            return GetKmlDocumentUrl(flightId);
        }


        public KMLFileResponseModel GetKMLFileURL(int flightId)
        {
            //string fPth = ConfigurationReader.KmlFilePath + @"\FlightPath" + flightId + ".kml";
            //if (File.Exists(fPth))
            //{
            //    File.Delete(fPth);
            //}
            int[] arr = new int[1];
            arr[0] = flightId;


            bool isFileExist = false;
            Stream str = new Misc().ReadFileFromS3("KmlFiles", "FlightPath" + flightId + ".kml", out isFileExist);
            if (isFileExist)
            {
                // logger.Info("File Exist on S3 Server");
                return new KMLFileResponseModel
                {
                    ResponseCode = ((int)Enumerations.KMLFileCodes.Success).ToString(),
                    ResponseMessage = Enumerations.KMLFileCodes.Success.GetStringValue(),
                    URL = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "FlightPath" + flightId + ".kml"
                };
            }
            else
            {
                //  logger.Info("File Not Exist on S3 Server");
                bool response = SaveKmlFileOnSever(arr);
                if (response == false)
                {
                    return new KMLFileResponseModel
                    {
                        ResponseCode = ((int)Enumerations.KMLFileCodes.InvalidFlightId).ToString(),
                        ResponseMessage = Enumerations.KMLFileCodes.InvalidFlightId.GetStringValue(),
                    };

                }
                else
                {
                    return new KMLFileResponseModel
                    {
                        ResponseCode = ((int)Enumerations.KMLFileCodes.Success).ToString(),
                        ResponseMessage = Enumerations.KMLFileCodes.Success.GetStringValue(),
                        URL = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "FlightPath" + flightId + ".kml"
                    };
                }
            }
        }



        /// <summary>
        /// Returns Url for download of kml file.
        /// </summary>
        /// <param name="flightIds"></param>
        /// <returns></returns>
        public string GetUrlForHostedKmlFile(int[] flightIds)
        {

            bool isFileExist = false;
            Stream str = new Misc().ReadFileFromS3("KmlFiles", "FlightPath" + flightIds[0] + ".kml", out isFileExist);
            //if (isFileExist)
            if (false)
            {
                return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "FlightPath" + flightIds[0] + ".kml";
            }
            else
            {
                SaveKmlFileOnSever(flightIds);
            }
            // return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "SamplesInMaps.kml";
            return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "FlightPath" + flightIds[0] + ".kml";
        }

        public string GetUrlForHostedFdrFile(int[] flightIds)
        {

            bool isFileExist = false;
            Stream str = new Misc().ReadFileFromS3("FdrFiles", "FlightPath" + flightIds[0] + ".fdr", out isFileExist);
            //if (isFileExist)
            if (false)
            {
                return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketFdrFile + "FlightPath" + flightIds[0] + ".fdr";
            }
            else
            {
                SaveFdrFileOnSever(flightIds);
            }
            // return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + "SamplesInMaps.kml";
            return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketFdrFile + "FlightPath" + flightIds[0] + ".fdr";
        }


        public double[] GetLatLongForFlight(int[] flightIds, bool isDemoUser)
        {
            // initialse with lat long center of usa.
            var latLong = new double[2] { 40, 98 };

            var context = new GuardianAvionicsEntities();

            try
            {

                var dataLogList = new List<DataLogListing>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                DataSet ds = new DataSet();
                if (!isDemoUser)
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = flightIds.FirstOrDefault();
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);
                }
                else
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, "select * from AirframeDatalogDemo ");
                }

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var airframedatalog = ds.Tables[0].AsEnumerable()
                          .Select(r => r.Field<string>("DataLog"))
                          .ToList();

                        foreach (var airframedata in airframedatalog)
                        {
                            dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                        }

                    }
                }

                var datalog = dataLogList.FirstOrDefault(d => d.Altitude != null && d.Latitude != null && d.Longitude != null);
                if (datalog != null)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    latLong[0] = Math.Round(Convert.ToDouble(datalog.Latitude), 6);
                    // ReSharper disable once PossibleInvalidOperationException
                    latLong[1] = Math.Round(Convert.ToDouble(datalog.Longitude), 6);
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                context.Dispose();
            }


            return latLong;
        }

        /// <summary>
        /// Generates Url for download of KML file from server
        /// </summary>
        /// <param name="flightIds">Id's of flights for which KML file is to be generated</param>
        /// <returns>Url for download of KML file from server</returns>
        public string GetServerHostedUrlKmlFile(IEnumerable<int> flightIds)
        {
            // hosted server url of kml file
            return ConfigurationReader.KmlFileServerPath + @"/" + String.Format(GAResource.KMLFileName, string.Join("-", flightIds));
        }
        public string GetServerHostedUrlFDRFile(IEnumerable<int> flightIds)
        {
            // hosted server url of kml file
            return ConfigurationReader.FDRFileServerPath + @"/" + String.Format(GAResource.FDRFileName, string.Join("-", flightIds));
        }
        

        public void SaveKmlFileOnSeverDemo()
        {
            var context = new GuardianAvionicsEntities();
            bool isFlightWithoutUnit = false;
            try
            {
                var desc = new Description { Text = "Shows flight path for aircrafts." };
                var container = new SharpKml.Dom.Document { Description = desc, Name = "Aircraft flight path" };
                //501400FF 50000000
                string[] colourArray = { "7f00ff00", "50FFFFFF", "5000FF14",
                                         "507800F0", "5014F0B4", "50FF7800",
                                         "50783CF0", "50B40014", "500A7878", "50FFFFFF" };



                var dataLogList = new List<DataLogListing>();
                var dataLogListWithoutUnit = new List<DataLogWithoutUnit>();
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, "select * from AirframeDatalogDemo");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var airframedatalog = ds.Tables[0].AsEnumerable()
                          .Select(r => r.Field<string>("DataLog"))
                          .ToList();
                        if (isFlightWithoutUnit)
                        {
                            foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                            {
                                dataLogListWithoutUnit.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(airframedata));
                            }
                        }
                        else
                        {
                            foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                            {
                                dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                            }
                        }
                    }

                }
                var cc = new CoordinateCollection();
                dataLogList.ForEach(a => cc.Add(new Vector(Convert.ToDouble(a.Latitude), Convert.ToDouble(a.Longitude), Convert.ToDouble(a.Altitude))));

                var ls = new LineString
                {
                    Extrude = true,
                    Tessellate = false,
                    AltitudeMode = AltitudeMode.Absolute,
                    Coordinates = cc
                };

                // add description for placemark
                desc = new Description { Text = "Shows flight path for flight" };

                // This is the Element we are going to save to the Kml file but first in the container either folder or document.
                var placemark = new Placemark { Name = "Flight Path", Description = desc, Geometry = ls };

                // add style
                var styl = new Style
                {
                    Line = new LineStyle
                    {
                        Color = SharpKml.Base.Color32.Parse(colourArray[0]),
                        Width = 8,
                    },
                    Polygon = new PolygonStyle
                    {
                        Color = SharpKml.Base.Color32.Parse(colourArray[0]),
                    }
                };
                placemark.AddStyle(styl);

                container.AddFeature(placemark);


                // This allows us to save and Element easily.
                var kml = KmlFile.Create(container, false);


                using (var stream = System.IO.File.OpenWrite(ConfigurationReader.KmlFilePath + @"\" + "demoFlight.kml"))
                {
                    kml.Save(stream);
                }
                new Misc().UploadFile(ConfigurationReader.KmlFilePath + @"\" + "demoFlight.kml", "demoFlight.kml", "KmlFiles");
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Generates KML file and saves it on server
        /// </summary>
        /// <param name="flightIds"></param>
        /// <returns></returns>
        private bool SaveKmlFileOnSever(int[] flightIds)
        {
            var context = new GuardianAvionicsEntities();
            bool isFlightWithoutUnit = false;
            try
            {
                var desc = new Description { Text = "Shows flight path for aircrafts." };
                var container = new SharpKml.Dom.Document { Description = desc, Name = "Aircraft flight path" };
                //501400FF 50000000
                string[] colourArray = { "7f00ff00", "50FFFFFF", "5000FF14",
                                         "507800F0", "5014F0B4", "50FF7800",
                                         "50783CF0", "50B40014", "500A7878", "50FFFFFF" };

                int i = -1;
                foreach (var flightId in flightIds)
                {
                    i++;

                    var dataLogList = new List<DataLogListing>();
                    var dataLogListWithoutUnit = new List<DataLogWithoutUnit>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = flightId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);

                    var pilotlog = context.PilotLogs.FirstOrDefault(f => f.Id == flightId);
                    if (pilotlog == null)
                    {
                        isFlightWithoutUnit = false;
                    }
                    else if (pilotlog.AeroUnitMasterId == null)
                    {
                        isFlightWithoutUnit = true;
                    }
                    else
                    {
                        isFlightWithoutUnit = false;
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            var airframedatalog = ds.Tables[0].AsEnumerable()
                              .Select(r => r.Field<string>("DataLog"))
                              .ToList();
                            if (isFlightWithoutUnit)
                            {
                                foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                                {
                                    dataLogListWithoutUnit.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(airframedata));
                                }
                            }
                            else
                            {
                                foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                                {
                                    dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    var cc = new CoordinateCollection();

                    if (isFlightWithoutUnit)
                    {
                        dataLogListWithoutUnit.ForEach(a => cc.Add(new Vector(Convert.ToDouble(a.Latitude), Convert.ToDouble(a.Longitude), Convert.ToDouble(a.Altitude))));
                    }
                    else
                    {
                        dataLogList.ForEach(a => cc.Add(new Vector(Convert.ToDouble(a.Latitude), Convert.ToDouble(a.Longitude), Convert.ToDouble(a.Altitude))));
                    }

                    var ls = new LineString
                    {
                        Extrude = true,
                        Tessellate = false,
                        AltitudeMode = AltitudeMode.Absolute,
                        Coordinates = cc
                    };

                    // add description for placemark
                    desc = new Description { Text = "Shows flight path for flight" + i };

                    // This is the Element we are going to save to the Kml file but first in the container either folder or document.
                    var placemark = new Placemark { Name = "Flight Path", Description = desc, Geometry = ls };

                    // add style
                    var styl = new Style
                    {
                        Line = new LineStyle
                        {
                            Color = SharpKml.Base.Color32.Parse(colourArray[i]),
                            Width = 8,
                        },
                        Polygon = new PolygonStyle
                        {
                            Color = SharpKml.Base.Color32.Parse(colourArray[i]),
                        }
                    };
                    placemark.AddStyle(styl);

                    container.AddFeature(placemark);
                }

                // This allows us to save and Element easily.
                var kml = KmlFile.Create(container, false);

                if (File.Exists(GetPathForKmlFile(flightIds)))
                {
                    File.Delete(GetPathForKmlFile(flightIds));
                }
                //if (!File.Exists(GetPathForKmlFile(flightIds)))
                //{
                //    logger.Info("KML file Not Exist On Local Folder");
                //    using (var stream = System.IO.File.OpenWrite(GetPathForKmlFile(flightIds)))
                //    {
                //        kml.Save(stream);
                //    }
                //}
                //else
                //{
                //    logger.Info("KML file Exist On Local Folder");
                //}
                using (var stream = System.IO.File.OpenWrite(GetPathForKmlFile(flightIds)))
                {
                    kml.Save(stream);
                }
                new Misc().UploadFile(GetPathForKmlFile(flightIds), "FlightPath" + flightIds[0] + ".kml", "KmlFiles");

                return true;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(flightIds), e);
                return false;
            }
        }


        private bool SaveFdrFileOnSever(int[] flightIds)
        {
            var context = new GuardianAvionicsEntities();
            bool isFlightWithoutUnit = false;
            try
            {
                string desc = "A"+"\n"+ "1 This is the needed beginning of the file: 'A' or 'I' for 'Apple' or 'IBM' carriage-returns, followed by an IMMEDIATE carriage return, followed by the version number of 1 \n" ;
                desc = desc + "\n" + "COMM,This is a sample FDR file, use it to generate your own Flight Data Recorder files." ;
                desc = desc + "\n" + "COMM,Data entries are as seen in the FDR window in X-Plane.\n\n" ;
                desc = desc + "\n" + "COMM,EVERY LINE BELOW (EXCEPT DATA) MUST END IN A COMMA!." ;
                desc = desc + "\n" + "COMM,WE USE COMMAS TO INDICATE END OF LINE IN THIS FILE, SINCE MANY SPREADHSEETS LIKE TO COMMA-DELIMIT." ;
                desc = desc + "\n" + "COMM,of course, any line that starts with the letters COMM is a comment, obviously.\n" ;

                
                foreach (var flightId in flightIds) 
                {
                    
                    var dataLogList = new List<DataLogListing>();
                    var dataLogListWithoutUnit = new List<DataLogWithoutUnit>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = flightId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetAllDataLogByLogId", param);
                    //desc = desc + "\n" + "ACFT," + "Aircraft/Laminar Research/Boeing B737-800/b738.acf"; // add file name here;
                    var pilotlog = context.PilotLogs.FirstOrDefault(f => f.Id == flightId);
                    var aircraft = context.AircraftProfiles.Where(x => x.Id == pilotlog.AircraftId).FirstOrDefault();
                    desc = desc + "\n" + "TAIL," + aircraft.Registration; //add tail number
                    desc = desc + "\n" + "DATE," + ds.Tables[0].Rows[0]["GPRMCDate"].ToString();
                    desc = desc + "\n" + "PRES," + "29.83";
                    desc = desc + "\n" + "TEMP," + "65";
                    desc = desc + "\n" + "WIND," + "230,16";
                   
                    if (pilotlog == null)
                    {
                        isFlightWithoutUnit = false;
                    }
                    else if (pilotlog.AeroUnitMasterId == null)
                    {
                        isFlightWithoutUnit = true;
                    }
                    else
                    {
                        isFlightWithoutUnit = false;
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            var airframedatalog = ds.Tables[0].AsEnumerable()
                              .Select(r => r.Field<string>("DataLog"))
                              .ToList();
                            if (isFlightWithoutUnit)
                            {
                                foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                                {
                                    dataLogListWithoutUnit.Add(jsonSerializer.Deserialize<DataLogWithoutUnit>(airframedata));
                                    
                                }
                            }
                            else
                            {
                                foreach (var airframedata in airframedatalog.Where(a => a.Contains("Latitude") && a.Contains("Longitude")))
                                {
                                    var dataLogData = new DataLogListing();
                                    dataLogData = jsonSerializer.Deserialize<DataLogListing>(airframedata);
                                    string getsec = Convert.ToDateTime(dataLogData.Date).ToString("HH:mm:ss");
                                    double seconds = TimeSpan.Parse(getsec).TotalSeconds;
                                    dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                                    dataLogData.ElevatorTrimPosition = (Convert.ToInt32(dataLogData.ElevatorTrimPosition==""?"0": dataLogData.ElevatorTrimPosition)).ToString();
                                    dataLogData.TIT1 = (Convert.ToInt32(dataLogData.TIT1==""?"0": dataLogData.TIT1)).ToString();
                                    dataLogData.Pitch=dataLogData.Pitch == "" ? "0" : dataLogData.Pitch;
                                    dataLogData.Roll=dataLogData.Roll == "" ? "0" : dataLogData.Roll;
                                    dataLogData.VSI = dataLogData.VSI == null ? "0" : dataLogData.VSI;
                                    desc = desc + "\n" + "DATA," + seconds + ",";
                                    desc = desc + 0.00 + "," + dataLogData.Longitude + "," + dataLogData.Latitude + "," + 4010 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ",";
                                    desc = desc + dataLogData.Pitch + "," + dataLogData.Roll  + "," + 0 + "," + dataLogData.Speed + "," + dataLogData.VSI + "," + 0 + "," + 0.4 + "," + 0.5 + ",";
                                    desc = desc + 1 + "," + 0 + "," + dataLogData.FlapPosition + "," + 0 + "," + 0 + "," + 0 + "," + 1 + "," + 1 + "," + 1 + "," + 1 + ",";
                                    desc = desc + dataLogData.ElevatorTrimPosition + "," + 11010 + "," + 10930 + "," + 4 + "," + 4 + "," + 90 + "," + 270 + "," + 0 + "," + 0 + "," + 2.5 + "," + 2.5 + ",";
                                    desc = desc + 1 +"," + 1 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 3 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ",";
                                    desc = desc + 1 + "," + 1 + "," + 2.5 + "," + 2.5 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 10 + "," + 10 + "," + 0 + "," + 0 + ",";
                                    desc = desc + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ",";
                                    desc = desc + 500 + "," + 29.9 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ",";
                                    desc = desc + 1 + ","  + dataLogData.RPM + "," + dataLogData.RPM + "," + 30 + ",";
                                    desc = desc + 100.1 + "," + 100.1 + "," + dataLogData.MAP + "," + 0 + "," + 584.1 + "," + dataLogData.FF + "," + dataLogData.TIT1 + "," + dataLogData.Egt1 + "," + dataLogData.Cht1 + ",";
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                // This allows us to save and Element easily.
                if (File.Exists(GetPathForFDRFile(flightIds)))
                {
                    File.Delete(GetPathForFDRFile(flightIds));
                }
                System.IO.File.WriteAllText(GetPathForFDRFile(flightIds), desc);
                new Misc().UploadFile(GetPathForFDRFile(flightIds), "FlightPath" + flightIds[0] + ".fdr", "FdrFiles");

                return true;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(flightIds), e);
                return false;
            }
        }
        /// <summary>
        /// Generates path of physical location of kml file on server.
        /// </summary>
        /// <param name="flightIds">Id's of flights for which KML file is to be generated</param>
        /// <returns></returns>
        private string GetPathForKmlFile(int[] flightIds)
        {
            // server path of file
            return ConfigurationReader.KmlFilePath + @"\" + String.Format(GAResource.KMLFileName, string.Join("-", flightIds));
            //return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + String.Format(GAResource.KMLFileName, string.Join("-", flightIds));
        }
        private string GetPathForFDRFile(int[] flightIds)
        {
            // server path of file
            return ConfigurationReader.FdrFilePath + @"\" + String.Format(GAResource.FDRFileName, string.Join("-", flightIds));
            //return ConfigurationReader.s3BucketURL + ConfigurationReader.BucketKmlFile + String.Format(GAResource.KMLFileName, string.Join("-", flightIds));
        }

        public int GetLastFlightId(int p)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var pList = context.PilotLogs.Where(pilot => pilot.PilotId == p && pilot.Finished).ToList();
                int maxPlogId = 0;
                if (pList.Count > 0)
                {
                    maxPlogId = pList.Max(a => a.Id);
                }

                return (maxPlogId);

                //return 0;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return 0;
            }
            finally
            {
                context.Dispose();
            }
        }

        #region Map files service
        /// <summary>
        /// Gets the map file url for Map files
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public MapFilesResponse GetMapFilesUrlFromDb(MapFilesRequest model)
        {
            var response = new MapFilesResponse { MapFilesList = new List<MapFilesModel>() };
            var context = new GuardianAvionicsEntities();
            try
            {
                var modifiedDate = Misc.GetDate(model.LastUpdatedDateTime) ?? new DateTime();

                var mList = context.MapFiles.Where(m => m.LastUpdated >= modifiedDate && !m.Deleted);

                if (mList == null)
                    return MapFilesResponse.Exception();
                var s3BucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketMapFilePath;


                mList.ToList().ForEach(m => response.MapFilesList.Add
                    (new MapFilesModel
                    {
                        Id = m.Id,
                        IsDeleted = m.Deleted,
                        NameOfMapFile = m.Name,
                        UrlForDown = (s3BucketPath + m.Url),
                        Title = m.Title,
                        FileSize = m.FileSize
                    }
                    ));
                response.LastUpdatedDateTime = Misc.GetStringOfDate(DateTime.UtcNow);
                response.ResponseCode = "0000";
                response.ResponseMessage = "Success";

            }
            catch (Exception e)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception";
            }
            finally
            {
                context.Dispose();
            }
            return response;
        }

        public PassengerMapFileResponse GetPassengerMapFileUrl()
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                try
                {
                    var objMapFile = context.PassengerProMapFiles.Where(m => !m.Deleted);
                    if (objMapFile == null)
                        return new PassengerMapFileResponse();
                    var s3BucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketMapFilePath;
                    var response = new PassengerMapFileResponse();
                    response.PassengerMapFileList = objMapFile
                                                    .Select(a => new PassengerProMapFileModel
                                                    {
                                                        FileName = a.Name,
                                                        FileSizeInMB = a.FileSize,
                                                        Region = a.PassengerProRegionMaster.Region,
                                                        RegionId = a.PassengerProRegionMasterId,
                                                        ZoomLevel = a.ZoomLevel,
                                                        URl = s3BucketPath + a.Name,
                                                        Title = a.Title
                                                    }).OrderBy(o => o.Region)
                                                    .ToList();

                    response.LastUpdatedDateTime = Misc.GetStringOfDate(DateTime.UtcNow);
                    response.RegionsList = context.PassengerProRegionMasters.Select(a => new ListItems { text = a.Region, value = a.Id }).OrderBy(o => o.text).ToList();

                    response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                    return response;
                }
                catch (Exception e)
                {
                    return new PassengerMapFileResponse
                    {
                        ResponseCode = "9999",
                        ResponseMessage = e.Message
                    };
                }
                finally
                {
                    context.Dispose();
                }
            }
            catch (Exception e)
            {

            }

            return PassengerMapFileResponse.Exception();
        }
        #endregion

        public string GetUrlDownloadKmlFileForMultipleFlights(int[] flightIds)
        {
            return GetUrlForHostedKmlFile(flightIds);
        }
        public string GetUrlDownloadFdrFileForMultipleFlights(int[] flightIds)
        {
            return GetUrlForHostedFdrFile(flightIds);
        }


        public GeneralResponse ParseLiveData(LiveDataRequestModel objRequest, int profileId)
        {
            var response = ValidateLiveDataRequest(objRequest.AircraftId, objRequest.PilotId, objRequest.CoPilotId, objRequest.EngineType, objRequest.UniqueId);

            if (response.ResponseMessage != Enumerations.LiveData.Success.GetStringValue())
            {
                return response;
            }


            StringBuilder strLine = new StringBuilder();
            bool isNewFlight = false;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            var context = new GuardianAvionicsEntities();

            var addPilotLog = context.PilotLogs.Create();
            var pilotLog = new PilotLog();
            string[] header = new string[] { };
            StringBuilder sbHeader;
            bool isFlightEnd = false;
            DataTable dt_AirframeDataLog = new DataTable();
            int? aeroUnitId = null;
            int flightTimeInPilotLog = 0;  //calculate in second
            string route = "";
            string wayPoint = "";
            dt_AirframeDataLog.Columns.Add("Id", typeof(int));
            dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
            dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
            dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));
            int pilotLogid = 0;
            DataRow dr;
            int liveDataMaxSpeed = 0;

            List<string> arrEngineParam = new List<string>();

            try
            {
                //Check that the aircraft is having Aero Unit or not
                var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objRequest.AircraftId);

                if (string.IsNullOrEmpty(aircraft.AeroUnitNo))
                {
                    pilotLog = context.PilotLogs.FirstOrDefault(f => f.AircraftId == objRequest.AircraftId && f.UniqeId == objRequest.UniqueId && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                }
                else
                {
                    var aeroUnitMaster = context.AeroUnitMasters.FirstOrDefault(a => a.AircraftId == aircraft.Id && !a.Blocked && !a.Deleted);
                    aeroUnitId = aeroUnitMaster.Id;
                    pilotLog = context.PilotLogs.FirstOrDefault(f => f.AircraftId == objRequest.AircraftId && f.UniqeId == objRequest.UniqueId && f.AeroUnitMasterId == aeroUnitMaster.Id && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                }

                if (pilotLog == null)
                {
                    //Before creating the new flight first set the finished status of the previous flights

                    var prevLogs = context.PilotLogs.Where(p => p.AircraftId == objRequest.AircraftId && !p.Finished && p.FlightDataType == (int)Enumerations.FlightDataType.LiveData).ToList();
                    prevLogs.ForEach(f => f.Finished = true);


                    //Create New Flight

                    addPilotLog.AircraftId = aircraft.Id;
                    addPilotLog.PilotId = objRequest.PilotId;
                    addPilotLog.LastUpdated = DateTime.UtcNow;
                    addPilotLog.Deleted = false;
                    addPilotLog.Actual = "00:00:00";
                    addPilotLog.CrossCountry = "00:00:00";
                    addPilotLog.Date = DateTime.UtcNow;
                    addPilotLog.DayPIC = "00:00:00";

                    addPilotLog.Hood = "00:00:00";
                    addPilotLog.NightPIC = "00:00:00";
                    addPilotLog.Sim = "00:00:00";
                    addPilotLog.Finished = isFlightEnd;
                    addPilotLog.IsSavedOnDropbox = false;
                    addPilotLog.UniqeId = objRequest.UniqueId;
                    addPilotLog.CoPilotId = objRequest.CoPilotId;
                    //  addPilotLogLive.FlightId = objRequest.UniqueId;
                    addPilotLog.AeroUnitMasterId = aeroUnitId;
                    addPilotLog.IsEmailSent = false;
                    addPilotLog.CommandRecFrom = string.IsNullOrEmpty(objRequest.EngineType) ? null : objRequest.EngineType;
                    addPilotLog.FlightDataType = (int)Enumerations.FlightDataType.LiveData;
                    addPilotLog.LiveDataMaxSpeed = liveDataMaxSpeed;
                    context.PilotLogs.Add(addPilotLog);
                    context.SaveChanges();

                    pilotLogid = addPilotLog.Id;
                }
                else
                {
                    var prevLogs = context.PilotLogs.Where(p => p.AircraftId == objRequest.AircraftId && p.Id < pilotLog.Id && !p.Finished && p.FlightDataType == (int)Enumerations.FlightDataType.LiveData).ToList();
                    prevLogs.ForEach(f => f.Finished = true);

                    pilotLogid = pilotLog.Id;
                    //Edit Pilot log
                    pilotLog.PilotId = objRequest.PilotId;
                    pilotLog.LastUpdated = DateTime.UtcNow;

                    pilotLog.Finished = isFlightEnd;
                    pilotLog.CoPilotId = objRequest.CoPilotId;
                    liveDataMaxSpeed = pilotLog.LiveDataMaxSpeed ?? 0;
                    if (string.IsNullOrEmpty(pilotLog.CommandRecFrom))
                    {
                        pilotLog.CommandRecFrom = string.IsNullOrEmpty(objRequest.EngineType) ? null : objRequest.EngineType;
                    }

                    context.SaveChanges();

                    flightTimeInPilotLog = (int)TimeSpan.Parse(pilotLog.DayPIC).TotalSeconds;
                }

                //Read the data and insert into the data table
                using (System.IO.StringReader reader = new System.IO.StringReader(objRequest.FlightData))
                {
                    strLine.Clear();

                    while (true)
                    {
                        try
                        {

                            sbHeader = new StringBuilder(reader.ReadLine());

                            if (string.IsNullOrEmpty(sbHeader.ToString()))
                            {
                                break;
                            }

                            if (sbHeader.ToString().Substring(0, 1) == "H")
                            {

                                if (!string.IsNullOrEmpty(aircraft.AeroUnitNo))
                                {
                                    sbHeader.Replace("TIT-L", "TIT1");
                                    sbHeader.Replace("TIT-R", "TIT2");
                                    sbHeader.Replace("OIL-T", "OILT");
                                    sbHeader.Replace("OIL-P", "OILP");
                                    sbHeader.Replace(",OAT,", ",Oat,");
                                    sbHeader.Replace("WP REQ", "REQ");
                                    sbHeader.Replace("H:M", "HM");
                                    sbHeader.Replace("BAT2", "VOLTS2");
                                    sbHeader.Replace(",BAT,", ",VOLTS,");
                                    sbHeader.Replace("FUEL-F", "FF");
                                    sbHeader.Replace(",TIT,", ",TIT1,");
                                    sbHeader.Replace(",TAT,", ",TotalAircraftTime,");
                                    sbHeader.Replace("ENG-Time", "EngineTime");
                                    sbHeader.Replace("ELE-TRIM-POS", "ElevatorTrimPosition");
                                    sbHeader.Replace("UNIT-IND1", "UnitsIndicator");
                                    sbHeader.Replace("UNIT-IND1", "UnitsIndicator");
                                    sbHeader.Replace("FLAP-POS", "FlapPosition");
                                    sbHeader.Replace("UNIT-IND2", "UnitsIndicator2");
                                    sbHeader.Replace("CARB-TEMP", "CarbTemp");
                                    sbHeader.Replace("UNIT-IND3", "UnitsIndicator3");
                                    sbHeader.Replace("COOL-PRES", "CoolantPressure");
                                    sbHeader.Replace("UNIT-IND4", "UnitsIndicator4");
                                    sbHeader.Replace("COOL-TEMP", "CoolantTemperature");
                                    sbHeader.Replace("UNIT-IND5", "UnitsIndicator5");
                                    sbHeader.Replace("UNIT-IND6", "UnitsIndicator6");
                                    sbHeader.Replace("AIL-TRIM-POS", "AileronTrimPosition");
                                    sbHeader.Replace("UNIT-IND7", "UnitsIndicator7");
                                    sbHeader.Replace("RUD-TRIM-POS", "RubberTrimPosition");
                                    sbHeader.Replace("UNIT-IND8", "UnitsIndicator8");
                                    sbHeader.Replace(",FQ3,", ",FuelQty3,");
                                    sbHeader.Replace("UNIT-IND9", "UnitsIndicator9");
                                    sbHeader.Replace(",FQ4,", ",FuelQty4,");
                                    sbHeader.Replace("UNIT-IND10", "UnitsIndicator10");
                                    sbHeader.Replace("DIS-IN1", "DiscreteInput1");
                                    sbHeader.Replace("DIS-IN2", "DiscreteInput2");
                                    sbHeader.Replace("DIS-IN3", "DiscreteInput3");
                                    sbHeader.Replace("DIS-IN4", "DiscreteInput4");
                                    sbHeader.Replace(",IGN,", ",IgnStatus,");
                                    sbHeader.Replace(",SEN,", ",SensorStatus,");
                                    sbHeader.Replace(",THR,", ",ThrottlePosition,");
                                    sbHeader.Replace(",BARO,", ",Baro,");
                                    sbHeader.Replace(",AIRT,", ",Airtemp,");
                                    sbHeader.Replace(",ECUT,", ",EcuTemp,");
                                    sbHeader.Replace(",SEN1,", ",Sen1,");
                                    sbHeader.Replace(",SEN2,", ",Sen2,");
                                    sbHeader.Replace(",SEN3,", ",Sen3,");
                                    sbHeader.Replace(",SEN4,", ",Sen4,");
                                    sbHeader.Replace(",SEN5,", ",Sen5,");
                                }
                            }

                            arrEngineParam = sbHeader.ToString().Split(',').ToList();
                            //arrEngineParam[0] will identify that the line contains values or header or flight end
                            // H for Header
                            // E for Flight End
                            // V for engine values
                            switch (arrEngineParam[0])
                            {
                                case "H":
                                    {
                                        arrEngineParam.RemoveAt(0);
                                        header = arrEngineParam.ToArray();
                                        break;
                                    }
                                case "V":
                                    {
                                        arrEngineParam.RemoveAt(0);
                                        dictionary.Add("Date", Misc.FormatedDateFromMMDDYY(arrEngineParam[0], arrEngineParam[1]).ToString());
                                        for (int i = 2; i < header.Length - 1; i++)
                                        {
                                            dictionary.Add(header[i], arrEngineParam[i]);
                                        }

                                        if (dictionary.ContainsKey("Speed"))
                                        {
                                            liveDataMaxSpeed = Convert.ToInt32(dictionary["Speed"]) > liveDataMaxSpeed ? Convert.ToInt32(dictionary["Speed"]) : liveDataMaxSpeed;
                                        }

                                        dr = dt_AirframeDataLog.NewRow();
                                        dr[0] = 0;
                                        dr[1] = pilotLogid;
                                        dr[2] = dictionary["Date"];
                                        dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(dictionary);
                                        dt_AirframeDataLog.Rows.Add(dr);
                                        dictionary.Clear();
                                        break;
                                    }
                                case "E":
                                    {
                                        isFlightEnd = true;
                                        break;
                                    }
                                case "FP":
                                    {
                                        //Flight Plan
                                        arrEngineParam.RemoveRange(0, 2);
                                        if (arrEngineParam.Count > 0)
                                        {
                                            route = string.Join(",", arrEngineParam);
                                        }
                                        break;
                                    }
                                case "WP":
                                    {
                                        //Flight Plan
                                        arrEngineParam.RemoveRange(0, 1);
                                        if (arrEngineParam.Count > 0)
                                        {
                                            wayPoint = arrEngineParam[0];
                                        }
                                        break;
                                    }

                            }
                        }
                        catch (Exception ex)
                        {
                            break;
                        }
                    }
                }


                if (dt_AirframeDataLog.Rows.Count == 0)
                {
                    if (pilotLog == null)
                    {
                        addPilotLog.Finished = isFlightEnd;

                    }
                    else
                    {
                        pilotLog.Finished = isFlightEnd;
                        //Check that this is the last live data flight
                        //var chkForLastLiveFlight = context.PilotLogs.FirstOrDefault(f => f.Id > pilotLog.Id && f.AircraftId == objRequest.AircraftId && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                        //if (chkForLastLiveFlight == null)
                        //{
                        //    pilotLog.Finished = isFlightEnd;
                        //}
                    }
                    context.SaveChanges();
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LiveData.Success).ToString(),
                        ResponseMessage = Enumerations.LiveData.Success.GetStringValue()
                    };
                }

                //Now calculate the time of datalog which gets in API

                DateTime startDate = Convert.ToDateTime(dt_AirframeDataLog.Rows[0]["GPRMCDate"]);
                DateTime EndDate = Convert.ToDateTime(dt_AirframeDataLog.Rows[dt_AirframeDataLog.Rows.Count - 1]["GPRMCDate"]);

                int totalSecondInAPIData = (int)(EndDate - startDate).TotalSeconds + 1;

                int totalFlightTimeInSec = totalSecondInAPIData + ((pilotLog == null) ? 0 : flightTimeInPilotLog);

                TimeSpan time = TimeSpan.FromSeconds(totalFlightTimeInSec);

                if (pilotLog == null)
                {
                    addPilotLog.DayPIC = time.ToString(@"hh\:mm\:ss");
                    addPilotLog.Finished = isFlightEnd;
                    addPilotLog.LiveDataMaxSpeed = liveDataMaxSpeed;
                    if (!string.IsNullOrEmpty(route))
                    {
                        addPilotLog.Route = route;
                    }
                    if (!string.IsNullOrEmpty(wayPoint))
                    {
                        addPilotLog.WayPoint = wayPoint;
                    }
                }
                else
                {
                    pilotLog.DayPIC = time.ToString(@"hh\:mm\:ss");
                    pilotLog.Finished = isFlightEnd;
                    pilotLog.LiveDataMaxSpeed = liveDataMaxSpeed;
                    pilotLog.Route = (route == "") ? null : route;
                    if (!string.IsNullOrEmpty(route))
                    {
                        pilotLog.Route = route;
                    }
                    if (!string.IsNullOrEmpty(wayPoint))
                    {
                        pilotLog.WayPoint = wayPoint;
                    }
                    //Check that this is the last live data flight
                    //var chkForLastLiveFlight = context.PilotLogs.FirstOrDefault(f => f.Id > pilotLog.Id && f.AircraftId == objRequest.AircraftId && f.FlightDataType == (int)Enumerations.FlightDataType.LiveData);
                    //if (chkForLastLiveFlight == null)
                    //{
                    //    pilotLog.Finished = isFlightEnd;
                    //}

                }
                context.SaveChanges();

                SqlParameter[] param = new SqlParameter[3];

                param[0] = new SqlParameter();
                param[0].ParameterName = "@aircraftId";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = aircraft.Id;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@pilotLogId";
                param[1].SqlDbType = SqlDbType.Int;
                param[1].Value = pilotLogid;


                param[2] = new SqlParameter();
                param[2].ParameterName = "tbAirframeDatalog";
                param[2].SqlDbType = SqlDbType.Structured;
                param[2].Value = dt_AirframeDataLog;



                if (dt_AirframeDataLog.Rows.Count > 0)
                {
                    int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirframeDataLogLive", param));
                }

            }
            catch (Exception exx)
            {

                var error = context.ErrorLogs.Create();
                error.ErrorDate = DateTime.UtcNow;
                error.ErrorDetails = Newtonsoft.Json.JsonConvert.SerializeObject(exx);
                error.ErrorSource = "API - GetLiveData";
                error.IsEmailSend = false;
                error.LastUpdateDate = DateTime.UtcNow;
                error.ModuleName = "API";
                error.RequestParameters = "";
                error.ResolvedStatus = false;
                context.ErrorLogs.Add(error);
                context.SaveChanges();

                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LiveData.Exception).ToString(),
                    ResponseMessage = Enumerations.LiveData.Exception.GetStringValue()
                };
            }
            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.LiveData.Success).ToString(),
                ResponseMessage = Enumerations.LiveData.Success.GetStringValue()
            };
        }

        public GeneralResponse ValidateLiveDataRequest(int aircraftId, int? pilotId, int? copilotId, string engineType, long uniqueId)
        {
            var context = new GuardianAvionicsEntities();

            if (context.AircraftProfiles.FirstOrDefault(f => f.Id == aircraftId) == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LiveData.AircraftIdNotFound).ToString(),
                    ResponseMessage = Enumerations.LiveData.AircraftIdNotFound.GetStringValue()
                };
            }

            if (pilotId != null)
            {
                if (context.Profiles.FirstOrDefault(f => f.Id == pilotId) == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LiveData.InvalidPilotId).ToString(),
                        ResponseMessage = Enumerations.LiveData.InvalidPilotId.GetStringValue()
                    };
                }
            }

            if (copilotId != null)
            {
                if (context.Profiles.FirstOrDefault(f => f.Id == copilotId) == null)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.LiveData.InvalidCoPilotId).ToString(),
                        ResponseMessage = Enumerations.LiveData.InvalidCoPilotId.GetStringValue()
                    };
                }
            }

            if (uniqueId == 0)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.LiveData.InvalidUniqueId).ToString(),
                    ResponseMessage = Enumerations.LiveData.InvalidUniqueId.GetStringValue()
                };
            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.LiveData.Success).ToString(),
                ResponseMessage = Enumerations.LiveData.Success.GetStringValue()
            };
        }



        public void temp()
        {
            StringBuilder strLine = new StringBuilder();
            List<string> arrEngineParam = new List<string>();
            using (System.IO.StringReader reader = new System.IO.StringReader(" H,Date,Time,Latitude,Longitude,Speed,Altitude,Heading,Pitch,Roll\nV,10/05/2016 16:49:06,37.335152,-122.032555,5,0,190.899994,0.000000,0.000000\nV,10/05/2016 16:49:07,37.335011,-122.032722,15,0,246.089996,0.000000,0.000000\nV,10/05/2016 16:49:08,37.334976,-122.032814,17,0,251.369995,0.000000,0.000000\nV,10/05/2016 16:49:09,37.334949,-122.032921,20,0,251.720001,0.000000,0.000000\nV,10/05/2016 16:49:10,37.334923,-122.033043,23,0,254.179993,0.000000,0.000000\nV,10/05/2016 16:49:11,37.334892,-122.033180,26,0,254.880005,0.000000,0.000000\nV,10/05/2016 16:49:12,37.334858,-122.033340,28,0,254.880005,0.000000,0.000000\nV,10/05/2016 16:49:13,37.334820,-122.033508,30,0,255.940002,0.000000,0.000000\nV,10/05/2016 16:49:14,37.334782,-122.033699,33,0,255.589996,0.000000,0.000000\nV,10/05/2016 16:49:15,37.334747,-122.033890,35,0,257.339996,0.000000,0.000000\nV,10/05/2016 16:49:16,37.334709,-122.034111,37,0,257.700012,0.000000,0.000000\nV,10/05/2016 16:49:17,37.334675,-122.034325,39,0,259.450012,0.000000,0.000000\nV,10/05/2016 16:49:18,37.334644,-122.034554,40,0,261.910004,0.000000,0.000000\nV,10/05/2016 16:49:19,37.334618,-122.034790,42,0,263.320007,0.000000,0.000000\nV,10/05/2016 16:49:20,37.334602,-122.035034,43,0,265.079987,0.000000,0.000000\nV,10/05/2016 16:49:21,37.334587,-122.035294,45,0,265.079987,0.000000,0.000000\nV,10/05/2016 16:49:22,37.334564,-122.035561,46,0,266.480011,0.000000,0.000000\nV,10/05/2016 16:49:23,37.334560,-122.035835,47,0,267.190002,0.000000,0.000000\nV,10/05/2016 16:49:24,37.334549,-122.036110,47,0,267.890015,0.000000,0.000000\nV,10/05/2016 16:49:25,37.334541,-122.036385,48,0,268.950012,0.000000,0.000000\nV,10/05/2016 16:49:26,37.334541,-122.036667,48,0,269.299988,0.000000,0.000000\nV,10/05/2016 16:49:27,37.334538,-122.036949,49,0,269.299988,0.000000,0.000000\nV,10/05/2016 16:49:28,37.334538,-122.037247,50,0,269.649994,0.000000,0.000000\nV,10/05/2016 16:49:29,37.334538,-122.037537,50,0,268.950012,0.000000,0.000000\nV,10/05/2016 16:49:30,37.334526,-122.037834,51,0,269.299988,0.000000,0.000000\nV,10/05/2016 16:49:31,37.334518,-122.038139,52,0,268.950012,0.000000,0.000000\nV,10/05/2016 16:49:32,37.334515,-122.038452,53,0,267.890015,0.000000,0.000000\nV,10/05/2016 16:49:33,37.334503,-122.038757,54,0,267.890015,0.000000,0.000000\nV,10/05/2016 16:49:34,37.334496,-122.039078,55,0,268.239990,0.000000,0.000000\nV,10/05/2016 16:49:35,37.334488,-122.039413,56,0,267.890015,0.000000,0.000000\nV,10/05/2016 16:49:36,37.334476,-122.039742,57,0,267.890015,0.000000,0.000000"))
            {
                while (true)
                {
                    arrEngineParam = reader.ReadLine().Split(',').ToList();
                }
            }

        }

        public List<FlightDuration> GetLiveDataFlightDuration(string flightIds)
        {
            List<FlightDuration> objList = new List<FlightDuration>();
            var context = new GuardianAvionicsEntities();

            List<int> idsList = flightIds.Split(',').Select(int.Parse).ToList();
            try
            {
                objList = context.PilotLogs.Where(f => idsList.Contains(f.Id)).Select(s => new FlightDuration
                {
                    logId = s.Id,
                    Duration = s.DayPIC,
                    IsFinished = s.Finished
                }).ToList();

                List<int> deletedLiveLogs = idsList.Where(p => !objList.Any(p2 => p2.logId == p)).ToList();

                foreach (var item in deletedLiveLogs)
                {
                    objList.Add(new FlightDuration
                    {
                        logId = item,
                        Duration = "",
                        IsFinished = true,
                        IsDeleted = true
                    });
                }


            }
            catch (Exception ex)
            {

            }
            return objList;
        }


        public List<DataLogListing> ListJpiDataDemo(int pageNo, int totalRecordFetch, int timeInterval, out int count, out string labelText, out string engineCommandType, out bool isDataAvailableForFlight, out string aircraftImageURL, out string pilotImageURL, out string pilotName, out int flightTimeInterval, DateTime? StartDateForInnerData, int RequestDataForLevel)
        {
            labelText = string.Empty;
            count = 0;
            engineCommandType = "JPI";
            isDataAvailableForFlight = false;
            aircraftImageURL = "";
            pilotImageURL = "";
            pilotName = "";
            flightTimeInterval = 0;
            try
            {
                //if (profileId < 1) return new List<DataLogListing>();

                using (var context = new GuardianAvionicsEntities())
                {
                    engineCommandType = "";
                    var flightName = "";
                    labelText = "";
                    Misc objMisc = new Misc();
                    //labelText += "Total Flight Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(plog.DayPIC));

                    string flightNo = "1";
                    string RegNo = "Demo";
                    string fileName = ""; //aircraftImageUrl
                    string pilotImageName = ""; //pilotImageUrl
                    pilotName = "Demo User";
                    //string path = ConfigurationReader.ImageServerPathKey;
                    string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;

                    aircraftImageURL = Constants.AircraftDefaultImage;
                    pilotImageURL = Constants.UserProfileDefaultImage;


                    //SqlParameter[] param = new SqlParameter[1];
                    //param[0] = new SqlParameter();
                    //param[0].ParameterName = "pilotLog";
                    //param[0].SqlDbType = SqlDbType.Int;
                    //param[0].Value = pilotLogId;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDemoFlightTime");


                    var response = new List<AirframeDatalog>();
                    var dataLogListTemp = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    isDataAvailableForFlight = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        DateTime? dateFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString());    // airDatalog.GPRMCDate;
                        decimal sec = Convert.ToDecimal(dateFrom.Value.Second);
                        DateTime endDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndTime"].ToString());

                        if (RequestDataForLevel != 0)
                        {
                            sec = StartDateForInnerData.Value.Second;
                            endDate = StartDateForInnerData.Value.AddMinutes(timeInterval);
                            if (RequestDataForLevel == 1)
                            {

                                //Condition 1
                                // response = response.Where(r => r.GPRMCDate >= StartDateForInnerData && r.GPRMCDate < endDate && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id);

                                SqlParameter[] param = new SqlParameter[6];

                                param[0] = new SqlParameter();
                                param[0].ParameterName = "startDate";
                                param[0].SqlDbType = SqlDbType.DateTime;
                                param[0].Value = StartDateForInnerData;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "endDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = endDate;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "sec";
                                param[2].SqlDbType = SqlDbType.Decimal;
                                param[2].Value = sec;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "timeinterval";
                                param[3].SqlDbType = SqlDbType.Int;
                                param[3].Value = 0;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "condition";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 1;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "totalrecordFetch";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDemoDataLogByTimeInterval", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();
                            }
                            else
                            {
                                //Condition 2
                                endDate = StartDateForInnerData.Value.AddMinutes(1);
                                // response = response.Where(r => r.GPRMCDate > StartDateForInnerData && r.GPRMCDate < endDate).OrderBy(o => o.Id);

                                SqlParameter[] param = new SqlParameter[6];

                                param[0] = new SqlParameter();
                                param[0].ParameterName = "startDate";
                                param[0].SqlDbType = SqlDbType.DateTime;
                                param[0].Value = StartDateForInnerData;

                                param[1] = new SqlParameter();
                                param[1].ParameterName = "endDate";
                                param[1].SqlDbType = SqlDbType.DateTime;
                                param[1].Value = endDate;

                                param[2] = new SqlParameter();
                                param[2].ParameterName = "sec";
                                param[2].SqlDbType = SqlDbType.Decimal;
                                param[2].Value = sec;

                                param[3] = new SqlParameter();
                                param[3].ParameterName = "timeinterval";
                                param[3].SqlDbType = SqlDbType.Int;
                                param[3].Value = 0;

                                param[4] = new SqlParameter();
                                param[4].ParameterName = "condition";
                                param[4].SqlDbType = SqlDbType.Int;
                                param[4].Value = 2;

                                param[5] = new SqlParameter();
                                param[5].ParameterName = "totalrecordFetch";
                                param[5].SqlDbType = SqlDbType.Int;
                                param[5].Value = 1;

                                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDemoDataLogByTimeInterval", param);
                                response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                                {
                                    Id = m.Field<int>("ID"),
                                    GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                    PilotLogId = m.Field<int>("PilotLogId"),
                                    DataLog = m.Field<string>("DataLog")
                                }).ToList();
                            }
                        }
                        else
                        {

                            // get the flight time duration
                            int flightTime = endDate.Subtract(dateFrom ?? DateTime.Now).Minutes;
                            if (flightTime > 10)
                            {
                                flightTimeInterval = 10;
                            }
                            else if (flightTime > 5)
                            {
                                flightTimeInterval = 5;
                            }
                            else if (flightTime > 2)
                            {
                                flightTimeInterval = 2;
                            }
                            else if (flightTime >= 1)
                            {
                                flightTimeInterval = 1;
                            }
                            else
                            {
                                flightTimeInterval = 0;
                            }


                            if (timeInterval > flightTimeInterval)
                            {
                                timeInterval = flightTimeInterval;
                            }

                            //Condition 3
                            // response = response.Where(r => r.GPRMCDate.Value.Minute % timeInterval == 0 && (decimal)r.GPRMCDate.Value.Second / sec == (decimal)1.0).OrderBy(o => o.Id).Skip(totalRecordFetch).Take(60);
                            SqlParameter[] param = new SqlParameter[6];

                            param[0] = new SqlParameter();
                            param[0].ParameterName = "startDate";
                            param[0].SqlDbType = SqlDbType.DateTime;
                            param[0].Value = dateFrom;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "endDate";
                            param[1].SqlDbType = SqlDbType.DateTime;
                            param[1].Value = DateTime.Now;

                            param[2] = new SqlParameter();
                            param[2].ParameterName = "sec";
                            param[2].SqlDbType = SqlDbType.Decimal;
                            param[2].Value = sec;

                            param[3] = new SqlParameter();
                            param[3].ParameterName = "timeinterval";
                            param[3].SqlDbType = SqlDbType.Int;
                            param[3].Value = timeInterval;

                            param[4] = new SqlParameter();
                            param[4].ParameterName = "condition";
                            param[4].SqlDbType = SqlDbType.Int;
                            if (flightTimeInterval == 0)
                            {
                                param[4].Value = 1;
                            }
                            else
                            {
                                param[4].Value = 3;
                            }


                            param[5] = new SqlParameter();
                            param[5].ParameterName = "totalrecordFetch";
                            param[5].SqlDbType = SqlDbType.Int;
                            param[5].Value = totalRecordFetch;

                            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetDemoDataLogByTimeInterval", param);
                            response = ds.Tables[0].AsEnumerable().Select(m => new AirframeDatalog()
                            {
                                Id = m.Field<int>("ID"),
                                GPRMCDate = m.Field<DateTime>("GPRMCDate"),
                                PilotLogId = m.Field<int>("PilotLogId"),
                                DataLog = m.Field<string>("DataLog")
                            }).ToList();
                        }
                    }
                    else
                    {
                        isDataAvailableForFlight = false;
                    }
                    response.ForEach(f => dataLogListTemp.Add(jsonSerializer.Deserialize<DataLogListing>(f.DataLog)));
                    if (dataLogListTemp.Count > 0)
                    {
                        dataLogListTemp.ForEach(a =>
                        {
                            a.Time = new DateTime(DateTime.Parse(a.Date, new CultureInfo("en-US", true)).Ticks).ToString("HH:mm:ss");
                            a.Date = (Convert.ToDateTime(a.Date)).ToShortDateString();
                            a.AircraftNNumber = RegNo;
                            a.FlightNumber = flightNo;

                        });
                    }

                    //CommonHelper objHelper = new CommonHelper();
                    //dataLogListTemp = objHelper.ParseJPIColourCode(dataLogListTemp, plog.AircraftId);
                    return dataLogListTemp;
                } // using
            }
            catch (Exception)
            {
                return new List<DataLogListing>();
            }
        }
    }
}