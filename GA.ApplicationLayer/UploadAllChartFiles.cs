﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GA.DataLayer;
using GA.Common;
using Ionic.Zip;
using System.Data.SQLite;
using System.Data;
using System.Web;
using System.IO;
//using NLog;
using System.Data.SqlClient;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;

namespace GA.ApplicationLayer
{
    public class UploadAllChartFiles
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private SQLiteConnection sqlite;
        public List<string> stateList;
        public int CurrStateCount = 0;
        public int TotalState = 0;
        public int ChartTypeCount = 0;
        public List<string> ChartType = new List<string>() { "hi", "lo", "sec" };
        public string fileName = string.Empty;
        public string version = string.Empty;
        public string password = string.Empty;
        public double fileSize = 0.0;
        public int totalFileDownloaded = 0;
        public string randomKey = "";

        public System.Text.StringBuilder sbPlates = new System.Text.StringBuilder();
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];

        public void UploadFile(HttpContext httpContext)
        {
            ChartProgressBar.strResponse.Clear();

            randomKey = httpContext.Request.Params["RandomKey"].ToString();

            version = httpContext.Request.Params["Version"].ToString();
            password = httpContext.Request.Params["Password"].ToString();

            GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary.Add(randomKey, 0);


            var context = new GuardianAvionicsEntities();
            stateList = context.States.Select(s => s.StateName).Take(2).ToList();
            TotalState = stateList.Count;

           // UploadAllFiles();
        }


        //public void UploadAllFiles()
        //{

        //    System.Net.WebClient webClient = new System.Net.WebClient();

        //    if (ChartTypeCount == 3)
        //    {
        //        ChartTypeCount = 0;
        //        CurrStateCount = CurrStateCount + 1;

        //    }

        //    if (CurrStateCount == TotalState)
        //    {
        //        SetPlates();
        //    }
        //    string StateName = stateList[CurrStateCount].ToString().Replace(" ", "");
        //    fileName = StateName + "." + ChartType[ChartTypeCount].ToString().ToUpper() + ".MBTiles.zip";

        //    string FileUrl = "http://chartdata.seattleavionics.com/charts/" + version + @"/us/" + ChartType[ChartTypeCount] + "/" + fileName;
        //    webClient.DownloadFileAsync(new Uri(FileUrl), ConfigurationReader.ChartFilePath + fileName);
        //    ChartTypeCount = ChartTypeCount + 1;

        //    webClient.DownloadFileCompleted += (s, e) =>
        //    {
        //        fileSize = Math.Round((new System.IO.FileInfo(ConfigurationReader.ChartFilePath + fileName).Length / 1024f) / 1024f, 2);

        //        //ChartProgressBar.dictionary[randomKey] = 85;
        //        try
        //        {
        //            using (var zip = ZipFile.Read(ConfigurationReader.ChartFilePath + fileName))
        //            {

        //                zip.Password = password;
        //                zip.ExtractProgress += zip_ExtractProgress;
        //                zip.ExtractAll(ConfigurationReader.ChartFilePath + "UnZipFiles");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ChartProgressBar.strResponse.Append("Error occured while downloading the file - " + fileName + Environment.NewLine);
        //            totalFileDownloaded = totalFileDownloaded + 1;
        //            GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = Convert.ToInt32((totalFileDownloaded * 100) / (TotalState * 3));
        //            UploadAllFiles();
        //        }

        //    };



        //}


        //public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        //{
        //    if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
        //    {

        //        try
        //        {
        //            string sqliteFileName = fileName.Replace(".zip", ".sqlite");
        //            sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePath + sqliteFileName);
        //            string query = "select * from metadata";
        //            DataTable dt = new DataTable();
        //            dt = selectQuery(query);
        //            var objEntity = new GuardianAvionicsEntities();
        //            string InsertOrUpdate = "Update";
        //            string name = dt.Rows[0][1].ToString();
        //            var chartFileDetails = objEntity.ChartDetails.FirstOrDefault(f => f.Name == name);
        //            if (chartFileDetails == null)
        //            {
        //                InsertOrUpdate = "Insert";
        //                chartFileDetails = objEntity.ChartDetails.Create();
        //            }

        //            string columnName = string.Empty;
        //            string region = string.Empty;
        //            string shortRegion = string.Empty;
        //            string subRegion = string.Empty;
        //            string shortSubRegion = string.Empty;
        //            string layerType = string.Empty;
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                columnName = dt.Rows[i][0].ToString();
        //                switch (columnName)
        //                {
        //                    case "name": { chartFileDetails.Name = dt.Rows[i][1].ToString(); break; }
        //                    case "short_name": { chartFileDetails.ShortName = dt.Rows[i][1].ToString(); break; }
        //                    case "type": { chartFileDetails.Type = dt.Rows[i][1].ToString(); break; }
        //                    case "version": { chartFileDetails.Version = dt.Rows[i][1].ToString(); break; }
        //                    case "description": { chartFileDetails.Description = dt.Rows[i][1].ToString(); break; }
        //                    case "format": { chartFileDetails.Format = dt.Rows[i][1].ToString(); break; }
        //                    case "exp_date": { chartFileDetails.ExpDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
        //                    case "expiration": { chartFileDetails.Expiration = Convert.ToDateTime(dt.Rows[i][1]); break; }
        //                    case "region": { region = dt.Rows[i][1].ToString(); break; }
        //                    case "short_region": { shortRegion = dt.Rows[i][1].ToString(); break; }
        //                    case "subregion": { subRegion = dt.Rows[i][1].ToString(); break; }
        //                    case "short_subregion": { shortSubRegion = dt.Rows[i][1].ToString(); break; }
        //                    case "provider": { chartFileDetails.Provider = dt.Rows[i][1].ToString(); break; }
        //                    case "short_provider": { chartFileDetails.ShortProvider = dt.Rows[i][1].ToString(); break; }
        //                    case "copyright": { chartFileDetails.Copyright = dt.Rows[i][1].ToString(); break; }
        //                    case "create_date": { chartFileDetails.CreateDate = Convert.ToDateTime(dt.Rows[i][1]); break; }
        //                    case "layer_type": { layerType = dt.Rows[i][1].ToString(); break; }
        //                    case "short_layer_type": { chartFileDetails.ShortLayerType = dt.Rows[i][1].ToString(); break; }
        //                    case "layer_sort": { chartFileDetails.LayerSort = dt.Rows[i][1].ToString(); break; }
        //                    case "min_zoom": { chartFileDetails.MinZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
        //                    case "max_zoom": { chartFileDetails.MaxZoom = Convert.ToInt32(dt.Rows[i][1]); break; }
        //                    case "bounds": { chartFileDetails.Bounds = dt.Rows[i][1].ToString(); break; }
        //                }
        //            }
        //            chartFileDetails.FileSize = (decimal)fileSize;
        //            chartFileDetails.FileName = fileName;
        //            chartFileDetails.Password = new Encryption().Encrypt(password, Constants.GlobalEncryptionKey);
        //            chartFileDetails.CountryId = new AdminHelper().SetCountry(region, shortRegion);
        //            chartFileDetails.StateId = new AdminHelper().SetState(subRegion, shortSubRegion);
        //            chartFileDetails.ChartTypeId = new AdminHelper().SetChart(layerType);
        //            chartFileDetails.LayerType = layerType;
        //            chartFileDetails.LastUpdateDate = DateTime.UtcNow;


        //            if (InsertOrUpdate == "Insert")
        //            {
        //                objEntity.ChartDetails.Add(chartFileDetails);
        //                objEntity.SaveChanges();
        //            }
        //            else
        //            {
        //                objEntity.SaveChanges();
        //            }


        //            //ChartProgressBar.dictionary[randomKey] = 100;
        //            //if (ChartProgressBar.dictionary.ContainsKey(randomKey))
        //            //{
        //            //    // ChartProgressBar.dictionary.Remove(randomKey);
        //            //}

        //            System.IO.File.Delete(ConfigurationReader.ChartFilePath + @"UnZipFiles\" + sqliteFileName);
        //            totalFileDownloaded = totalFileDownloaded + 1;

        //            ChartProgressBar.strResponse.Append("File - " + fileName + " processed successfully." + Environment.NewLine);
        //            GA.DataTransfer.Classes_for_Web.ChartProgressBar.dictionary[randomKey] = Convert.ToInt32((totalFileDownloaded * 100) / (TotalState * 3));
        //            UploadAllFiles();
        //        }
        //        catch (Exception ex)
        //        {
        //            ChartProgressBar.strResponse.Append("Error occured while processing the file - " + fileName + Environment.NewLine);
        //        }
        //    }

        //}


        //public DataTable selectQuery(string query)
        //{
        //    SQLiteDataAdapter ad;
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        SQLiteCommand cmd;
        //        sqlite.Open();  //Initiate connection to the db
        //        cmd = sqlite.CreateCommand();
        //        cmd.CommandText = query;  //set the passed query
        //        ad = new SQLiteDataAdapter(cmd);
        //        ad.Fill(dt); //fill the datasource
        //    }
        //    catch (SQLiteException ex)
        //    {
        //        //Add your exception code here.
        //    }
        //    sqlite.Close();
        //    return dt;
        //}




        //public void SetPlates()
        //{

        //    System.Net.WebClient webClient = new System.Net.WebClient();
        //    logger.Fatal("Plates Text file downloaded Process Start." + Environment.NewLine);
        //    webClient.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.TXT.zip"), ConfigurationReader.PlateFilePath + "PlatesText.zip");


        //    webClient.DownloadFileCompleted += (s, e) =>
        //    {
        //        logger.Fatal("Plates Text file downloaded. " + Environment.NewLine);
        //        if (Directory.Exists(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText"))
        //        {
        //            Directory.Delete(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText", true);
        //        }

        //        using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "PlatesText.zip"))
        //        {
        //            zip.Password = "595685";
        //            zip.ExtractProgress += zip_ExtractProgressText;
        //            zip.ExtractAll(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText");
        //        }
        //    };
        //}


        //public void zip_ExtractProgressText(object sender, ExtractProgressEventArgs exx)
        //{
        //    if (exx.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
        //    {
        //        try
        //        {
        //            logger.Fatal("Text file unzip successfully. " + Environment.NewLine);


        //            DataTable dt = new DataTable("Airports");
        //            dt.Columns.Add("AirportName", typeof(string));   //0
        //            dt.Columns.Add("Military", typeof(string)); //1
        //            dt.Columns.Add("apt_ident", typeof(string));  //2
        //            dt.Columns.Add("icao_ident", typeof(string)); //3
        //            dt.Columns.Add("airport_name_Id", typeof(int));     //4
        //            dt.Columns.Add("city_name_Id", typeof(int)); //5 
        //            dt.Columns.Add("Country", typeof(string)); //6 
        //            dt.Columns.Add("X", typeof(decimal)); //7 
        //            dt.Columns.Add("Y", typeof(decimal)); //8 
        //            dt.Columns.Add("CityId", typeof(decimal)); //9 

        //            DataRow dr = null;
        //            string sep = "\t";
        //            foreach (var line in System.IO.File.ReadLines(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText\Airports.txt").Skip(7))
        //            {
        //                dr = dt.NewRow();
        //                string[] arrColumns = line.Split(sep.ToCharArray());
        //                dr[0] = arrColumns[0];
        //                dr[1] = arrColumns[1];
        //                dr[2] = arrColumns[2];
        //                dr[3] = arrColumns[3];
        //                dr[4] = Convert.ToInt16(arrColumns[4]);
        //                dr[5] = Convert.ToInt16(arrColumns[5]);
        //                dr[6] = arrColumns[6];
        //                dr[7] = Convert.ToDecimal(arrColumns[7]);
        //                dr[8] = Convert.ToDecimal(arrColumns[8]);
        //                dr[9] = 1;
        //                dt.Rows.Add(dr);

        //            }

        //            SqlParameter[] param = new SqlParameter[1];
        //            param[0] = new SqlParameter();
        //            param[0].ParameterName = "tbAirport";
        //            param[0].SqlDbType = SqlDbType.Structured;
        //            param[0].Value = dt;

        //            if (dt.Rows.Count > 0)
        //            {
        //                int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirports", param));
        //            }



        //            System.Net.WebClient webClient = new System.Net.WebClient();
        //            logger.Fatal("Plates SQLite file downloaded Process Start." + Environment.NewLine);
        //            webClient.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates.sqlite.zip"), ConfigurationReader.PlateFilePath + "Plates.zip");


        //            webClient.DownloadFileCompleted += (s, e) =>
        //            {
        //                logger.Fatal("Plates SQLite file downloaded. " + Environment.NewLine);
        //                if (File.Exists(ConfigurationReader.PlateFilePath + @"UnZipFiles\Plates.sqlite"))
        //                {
        //                    File.Delete(ConfigurationReader.PlateFilePath + @"UnZipFiles\Plates.sqlite");
        //                }

        //                using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "Plates.zip"))
        //                {
        //                    zip.Password = "595685";
        //                    zip.ExtractProgress += zip_ExtractProgresssqlite;
        //                    zip.ExtractAll(ConfigurationReader.PlateFilePath + "UnZipFiles");
        //                }
        //            };


        //            System.Net.WebClient webClientPnfFile = new System.Net.WebClient();
        //            logger.Fatal("Plates PNG file downloaded Process Start." + Environment.NewLine);
        //            webClientPnfFile.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/" + version + "/" + version + ".Plates1024.PNG.zip"), ConfigurationReader.PlateFilePath + "PlatesPNG.zip");
        //            webClientPnfFile.DownloadFileCompleted += (s, e) =>
        //            {
        //                logger.Fatal("Plates SQLite file downloaded Successfully." + Environment.NewLine);
        //                if (Directory.Exists(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG"))
        //                {
        //                    Directory.Delete(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG", true);
        //                }
        //                logger.Fatal("Plates SQLite file starts unZip." + Environment.NewLine);
        //                using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "PlatesPNG.zip"))
        //                {
        //                    zip.Password = "595685";
        //                    zip.ExtractProgress += zip_ExtractProgressPNG;
        //                    zip.ExtractAll(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");
        //                }
        //            };

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
        //        }
        //    }
        //}


        //public void zip_ExtractProgresssqlite(object sender, ExtractProgressEventArgs e)
        //{
        //    if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
        //    {
        //        try
        //        {
        //            logger.Fatal("sqlite file unzip successfully. " + Environment.NewLine);
        //            sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePathForPlates + "Plates.sqlite");
        //            string query = "select * from charts";
        //            DataTable dt = new DataTable();
        //            dt = selectQuery(query);
        //            SetPlatesInToDB(dt);
        //            // tempProcess();

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
        //        }
        //    }
        //}


        //public void zip_ExtractProgressPNG(object sender, ExtractProgressEventArgs e)
        //{
        //    if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
        //    {
        //        logger.Fatal("Plates PNG File unextract successfully." + Environment.NewLine);
        //        DirectoryInfo DirInfo = new DirectoryInfo(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");

        //        try
        //        {
        //            DataTable dt = new DataTable("PNGDetails");
        //            dt.Columns.Add("FileName", typeof(string));   //0
        //            dt.Columns.Add("ListPrice", typeof(int));  //2
        //            DataRow dr = null;
        //            foreach (FileInfo fi in DirInfo.EnumerateFiles())
        //            {
        //                dr = dt.NewRow();
        //                dr[0] = fi.Name;
        //                dr[1] = fi.Length;
        //                dt.Rows.Add(dr);
        //            }

        //            UpdateFileSizePlates(dt);


        //        }
        //        catch (Exception Ex)
        //        {
        //            logger.Fatal("Exception in zip_ExtractProgressPNG  " + Newtonsoft.Json.JsonConvert.SerializeObject(Ex) + " " + Environment.NewLine);
        //        }

        //    }
        //}

        //public void SetPlatesInToDB(DataTable dt)
        //{
        //    logger.Fatal("Start inserting plates data into DB." + Environment.NewLine);
        //    var context = new GuardianAvionicsEntities();
        //    dt.Columns[2].Caption = "ChartSeq";
        //    dt.Columns[5].Caption = "Civil";
        //    dt.Columns[6].Caption = "Faanfd15";
        //    dt.Columns[7].Caption = "Faanfd18";
        //    dt.Columns[8].Caption = "Copter";
        //    dt.Columns.Remove("Airports_Id");

        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter();
        //    param[0].ParameterName = "tbPlate";
        //    param[0].SqlDbType = SqlDbType.Structured;
        //    param[0].Value = dt;

        //    //foreach (DataRow dr in dt.Rows)
        //    //{
        //    //    dr["Civil"] = "abc";
        //    //    dr.EndEdit();
        //    //}
        //    //dt.AcceptChanges();

        //    if (dt.Rows.Count > 0)
        //    {
        //        int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlates", param));
        //    }

        //    logger.Fatal("Plates data inserted successfully in DB." + Environment.NewLine);

        //}


        //public void UpdateFileSizePlates(DataTable dt)
        //{
        //    logger.Fatal("Plates PNG file Size updation start " + Environment.NewLine);
        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter();
        //    param[0].ParameterName = "tbPlateFileSize";
        //    param[0].SqlDbType = SqlDbType.Structured;
        //    param[0].Value = dt;

        //    if (dt.Rows.Count > 0)
        //    {
        //        int invID = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlatesFileSize", param));
        //    }
        //    logger.Fatal("Plates PNG file Size updation completed " + Environment.NewLine);
        //    CreateTextFileForPlates();


        //}

        //public void CreateTextFileForPlates()
        //{
        //    logger.Fatal("Plates PNG file - start creating JSON. " + Environment.NewLine);
        //    var context = new GuardianAvionicsEntities();

        //    var objState = context.States.Select(s => s.Id).ToList();
        //    PlatesResponseModel objPaltesResponse = new PlatesResponseModel();
        //    var stateList = new List<PlateStates>();

        //    foreach (int stateId in objState)
        //    {
        //        objPaltesResponse.StateList = context.States.Where(s => s.Id == stateId).Select(s => new PlateStates
        //        {
        //            Id = s.Id,
        //            ShortName = s.ShortName,
        //            State_Code_Id = s.State_Code_Id,
        //            StateName = s.StateName,
        //            CityList = s.Cities.Where(wc=> wc.City_Name_Id != null).Select(c => new PlateCitys
        //            {
        //                City_Name_Id = c.City_Name_Id ?? 0,
        //                CityName = c.CityName,
        //                Id = c.Id,
        //                State_Code_Id = c.State_Code_Id,
        //                StateId = c.StateId,
        //                Volume = c.Volume,
        //                AirportList = c.Airports.Where(wa=> wa.airport_name_Id != null).Select(a => new PlateAirPorts
        //                {
        //                    airport_name_Id = a.airport_name_Id,
        //                    AirportName = a.AirportName,
        //                    apt_ident = a.apt_ident,
        //                    city_name_Id = a.city_name_Id,
        //                    CityId = a.CityId,
        //                    Country = a.Country,
        //                    icao_ident = a.icao_ident,
        //                    Id = a.Id,
        //                    Military = a.Military,
        //                    X = a.X,
        //                    Y = a.Y,
        //                    PlateList = a.Plates.Select(p => new PlateDetails
        //                    {
        //                        Airport_Name_Id = p.Airport_Name_Id,
        //                        AirportId = p.AirportId,
        //                        Chart_Code = p.Chart_Code,
        //                        Chart_Name = p.Chart_Name,
        //                        ChartSeq = p.ChartSeq,
        //                        Civil = p.Civil,
        //                        Copter = p.Copter,
        //                        Draw_Bottom = p.Draw_Bottom,
        //                        Draw_Left = p.Draw_Left,
        //                        Draw_Right = p.Draw_Right,
        //                        Draw_Top = p.Draw_Top,
        //                        ExcludeAreas = p.ExcludeAreas,
        //                        Faanfd15 = p.Faanfd15,
        //                        Faanfd18 = p.Faanfd18,
        //                        File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
        //                        Id = p.Id,
        //                        Invalid = Convert.ToBoolean(p.Invalid),
        //                        LastChanged = p.LastChanged,
        //                        Lat1 = p.Lat1,
        //                        Lat2 = p.Lat2,
        //                        Long1 = p.Long1,
        //                        Long2 = p.Long2,
        //                        Orientation = p.Orientation,
        //                        UserAction = p.UserAction,
        //                        ValidWithinRadius = p.ValidWithinRadius,
        //                        ValidWithinX = p.ValidWithinX,
        //                        ValidWithinY = p.ValidWithinY,
        //                        X1 = p.X1,
        //                        X2 = p.X2,
        //                        Y1 = p.Y1,
        //                        Y2 = p.Y2
        //                    }).ToList()
        //                }).ToList()
        //            }).ToList()
        //        }).ToList();

        //        System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + @"JsonFiles\" + stateId.ToString() + ".txt", Newtonsoft.Json.JsonConvert.SerializeObject(objPaltesResponse));
        //    }

        //    logger.Fatal("Plates PNG file - creating JSON Completed " + Environment.NewLine);


        //}

    }

}
