﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
using GA.Common;
using GA.DataTransfer.Classes_for_Services;
using System.IO;
using Newtonsoft.Json.Linq;

namespace GA.ApplicationLayer
{
    public class ConversionHelper
    {

        public LoginModel DecryptLoginAPIRequest(string cipherText, int profileId)
        {
            //Decrypt this text from G.. Key

            string key = "";

            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = new Encryption().Decrypt(cipherText, key);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<LoginModel>(jsonStr);
        }

        public SyncUserDataModel DecryptSyncUserData(string cipherText, int profileId)
        {
            //Decrypt this text from G.. Key

            string key = "";

            if (profileId != 0)
            {
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = (new Encryption().Decrypt(cipherText, key)).Replace("deletedMedicalCertificates", "DeletedMedicalCertificate"); ;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<SyncUserDataModel>(jsonStr);
        }

        public string EncryptSyncUserData(SyncUserDataResponseModel syncUserDataResponseModel, int profileId)
        {

            //string key = (new Encryption().Encrypt(PasswordAndEmailId, Constants.GlobalEncryptionKey)).Substring(0, 16);
            //string key = new AdminHelper().GetKeyForEncryption(loginResponseModel.ProfileId);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataResponseModel), key);
        }

        public string EncryptNewLoginAPIResponse(NewLoginResponseModel loginResponseModel, string PasswordAndEmailId)
        {

            //string key = (new Encryption().Encrypt(PasswordAndEmailId, Constants.GlobalEncryptionKey)).Substring(0, 16);
            //string key = new AdminHelper().GetKeyForEncryption(loginResponseModel.ProfileId);
            string key = Constants.GlobalEncryptionKey;
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(loginResponseModel), key);
        }

        public string EncryptLoginAPIResponse(LoginResponseModel loginResponseModel, string PasswordAndEmailId)
        {

            //string key = (new Encryption().Encrypt(PasswordAndEmailId, Constants.GlobalEncryptionKey)).Substring(0, 16);
            //string key = new AdminHelper().GetKeyForEncryption(loginResponseModel.ProfileId);
            string key = Constants.GlobalEncryptionKey;
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(loginResponseModel), key);
        }

     

        public UserModel DecryptRegisterUserAPI(string cipherText)
        {
            
            UserModel usermodel = new UserModel();
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            jsonStr = jsonStr.Replace("securityQuestion", "SecurityQuestionId");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            usermodel = jsonSerializer.Deserialize<UserModel>(jsonStr);
            return usermodel;
        }

        public string EncryptRegisterUserAPIResponse(RegistrationResponseModel registrationResponseModel, string PasswordAndEmailId)
        {
            //string key = (new Encryption().Encrypt(PasswordAndEmailId.ToLower(), Constants.GlobalEncryptionKey)).Substring(0, 16);
             return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(registrationResponseModel), Constants.GlobalEncryptionKey);
        }


        public RegisterAndLoginByThirdParty DecryptThirdPartyLoginAPIRequest(string cipherText)
        {
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<RegisterAndLoginByThirdParty>(jsonStr);
        }

        public string EncryptThirdPartyLoginAPIResponse(NewLoginResponseModel loginResponseModel)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(loginResponseModel), Constants.GlobalEncryptionKey);
        }

        public ProfileUpdateRequestModel DecryptUserProfileUpdateAPIRequest(string cipherText, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(cipherText, key)).Replace("deletedMedicalCertificates", "DeletedMedicalCertificate");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ProfileUpdateRequestModel>(jsonStr);
        }


        public string EncryptUserProfileUpdateAPIResponse(ProfileUpdateResponseModel profileUpdateResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(profileUpdateResponseModel), key);
        }

        public ResetPasswordModel DecryptResetPasswordAPIRequest(string cipherText)
        {
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ResetPasswordModel>(jsonStr);
        }

        public AeroUnitNumberRequestModel DecryptIsFMSRegisterAPIRequest(string cipherText)
        {
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AeroUnitNumberRequestModel>(jsonStr);
        }

        public AeroUnitNumberRequestModel DecryptDeleteAeroUnitAPIRequest(string cipherText)
        {
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AeroUnitNumberRequestModel>(jsonStr);
        }

        public SendPushMessageOnFlightStartRequestModel DecryptSendPushMessageOnFlightStartAPIRequest(string cipherText, int profileId)
        {
            string key = "";
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
              key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = new Encryption().Decrypt(cipherText, key);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<SendPushMessageOnFlightStartRequestModel>(jsonStr);
         }

        public AeroUnitRequestModel DecryptISFMSRegisterWithAircraftAPIRequest(string cipherText, int profileId)
        {
            string key = "";

            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = new Encryption().Decrypt(cipherText, key);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AeroUnitRequestModel>(jsonStr);
        }

        public string EncryptISFMSRegisterWithAircraftAPIResponse(AircraftProfileResponseModel aircraftProfileResponseModel, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(aircraftProfileResponseModel), key);
        }

        public AircraftProfileUpdateModel DecryptUpdateAircraftProfileAPIRequest(string Text, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            string jsonStr = "";
            if (profileId != 0)
            {
                 jsonStr = (new Encryption().Decrypt(Text, key)).Replace("deletedAircraft", " ").Replace("areoUnitNumber", "AeroUnitNo").Replace("otherManufacturer", "OtherAircraftManufacturer").Replace("otherModel", "OtheAircraftrModel").Replace("componentManufacturerModelMapping", "AircraftComponentMakeAndModels");
            }
            else
            {
                 jsonStr = (new Encryption().Decrypt(Text, Constants.GlobalEncryptionKey)).Replace("deletedAircraft", " ").Replace("areoUnitNumber", "AeroUnitNo").Replace("otherManufacturer", "OtherAircraftManufacturer").Replace("otherModel", "OtheAircraftrModel").Replace("componentManufacturerModelMapping", "AircraftComponentMakeAndModels");
            }
          
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AircraftProfileUpdateModel>(jsonStr);
        }

        public string EncryptUpdateAircraftProfileAPIResponse(AircraftProfileResponseModel aircraftProfileResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(aircraftProfileResponseModel), key);
        }


        public GetPrefereceModel DecryptGetPreferenceAPIRequest(string cipherText, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(cipherText, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<GetPrefereceModel>(jsonStr);
        }

        public UpdatePreferenceModel DecryptUpdatePreferenceAPIRequest(string cipherText, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(cipherText, key)).Replace("emailId", "Email");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<UpdatePreferenceModel>(jsonStr);
        }

        public string EncryptUpdatePreferenceAPIResponse(UpdatePreferenceResponseModel updatePreferenceResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(updatePreferenceResponseModel), key);
        }

        public string EncryptGetPreferenceAPIResponse(GetPreferenceResponseModel getPreferenceResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(getPreferenceResponseModel), key);
        }
        


        public LogBookRequestModel DecryptLogBookAPIRequest(string cipherText, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(cipherText, key)).Replace("logBookId", "Id");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<LogBookRequestModel>(jsonStr);
        }
        //********************************************
        public AirframeDataLogRequestModel DecryptAirFrameDataLogAPIRequest(string cipherText, int profileId)
        {
            //profileId = 482;
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(cipherText, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AirframeDataLogRequestModel>(jsonStr);
        }
        public string EncryptAirFrameDataLogAPIResponse(AirFrameDatalogResponse airFrameDatalog, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(airFrameDatalog), key);
        }
        //*********************************************
        public string EncryptLogBookAPIResponse(LogBookResponseModel logBookResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(logBookResponseModel), key);
        }

        public UpdateLogBookForPilotAndCopilot DecryptUpdateLogWithPilotAndCoPilotAPIRequest(string text, int profileId)
        {

            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);

            string jsonStr = (new Encryption().Decrypt(text, key)).Replace("logBookId", "Id");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<UpdateLogBookForPilotAndCopilot>(jsonStr);
        }

        public AircraftSearchRequest DecryptAircraftSearchAPIRequest(string text, int profileId)
        {
            string key = "";

            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                  key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = new Encryption().Decrypt(text, key);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AircraftSearchRequest>(jsonStr);
        }

        public string EncryptAircraftSearchAPIResponse(AircraftSearchResponse aircraftSearchResponse, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(aircraftSearchResponse), key);
        }

        public AircraftModifyNnumberRequest DecryptCreateUpdateAircraftNNumberAPIRequest(string text, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }

            string jsonStr = (new Encryption().Decrypt(text, key)).Replace("areoUnitNumber", "AeroUnitNo").Replace("otherManufacturer", "OtherAircraftManufacturer").Replace("otherModel", "OtheAircraftrModel").Replace("componentManufacturerModelMapping", "AircraftComponentMakeAndModels").Replace("ownerId", "OwnerEmailId");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AircraftModifyNnumberRequest>(jsonStr);
        }

        public string EncryptCreateUpdateAircraftNNumberAPIResponse(AircraftModifyNnumberResponse aircraftModifyNnumberResponse, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }

            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(aircraftModifyNnumberResponse), key);
        }

        public string EncryptGetDocumentAPIResponse(DocumentResponseModel documentResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(documentResponseModel), key);
        }

        public ManufacturereModelListRequest DecryptGetFieldsForDropDownlistAndConstantsAPIRequest(string text)
        {
            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ManufacturereModelListRequest>(jsonStr);
        }

        public string EncryptGetFieldsForDropDownlistAndConstantsAPIResponse(ManufacturereModelListResponse manufacturereModelListResponse, int profileId)
        {
           
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(manufacturereModelListResponse), Constants.GlobalEncryptionKey);
        }

        public MapFilesRequest DecryptGetMapFilesUrlAPIREquest(string text)
        {
            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<MapFilesRequest>(jsonStr);
        }

        public string EncryptGetMapFilesUrlAPIResponse(MapFilesResponse mapFilesResponse)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(mapFilesResponse), Constants.GlobalEncryptionKey);
        }

        public string EncryptGetPassengerMapFileUrlAPIResponse(PassengerMapFileResponse passengerMapFileResponse)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(passengerMapFileResponse), Constants.GlobalEncryptionKey);
        }

        public string EncryptGetHardwareFirmwareFileUrlAPIResponse(HardwareFirmwareFileResponseModel hardwareFirmwareFileResponseModel)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(hardwareFirmwareFileResponseModel), Constants.GlobalEncryptionKey);
        }

        public string EncryptGetPlatesDataAPIResponse(PlatesResponseModel objResponse)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(objResponse), Constants.GlobalEncryptionKey);
        }

        public PlatesRequestModel DecryptGetPlatesDataAPIRequest(string text)
        {

            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<PlatesRequestModel>(jsonStr);
        }

        public RequestToOwnerForAircraftModel DecryptRequestToOwnerForAircraftAPIRequest(string text, int profileId)
        {

            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }

            string jsonStr = (new Encryption().Decrypt(text, key)); // .Replace("isQueAndAnsVerified", "IsQueAnsVerify");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<RequestToOwnerForAircraftModel>(jsonStr);
        }


        public GetNotificationRequest DecryptRequestToGetNotificationAPIRequest(string text, int profileId)
        {

            string key = "";
            if (profileId != 0)
            {
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }

            string jsonStr = (new Encryption().Decrypt(text, key)); 
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<GetNotificationRequest>(jsonStr);
        }


        public AeroUnitNumberRequestModel DecryptIsFmsExistsAPIRequest(string text)
        {

            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<AeroUnitNumberRequestModel>(jsonStr);
        }

        public ChangePasswordRequetModel DecryptChangePasswordAPIRequest(string text, int profileId)
        {

            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }

            string jsonStr = (new Encryption().Decrypt(text, key)); // .Replace("isQueAndAnsVerified", "IsQueAnsVerify");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ChangePasswordRequetModel>(jsonStr);
        }

        public ResetPasswordModel DecryptGetSecurityQuestionByEmailIdAPIRequest(string text)
        {
            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ResetPasswordModel>(jsonStr);
        }

        public RequestOTPVeriftcationModel DecryptOTPRequestModel(string text)
        {
            string jsonStr = new Encryption().Decrypt(text, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<RequestOTPVeriftcationModel>(jsonStr);
        }
        public ChartDetailsRequestModel DecryptGetChartDetails(string text, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            string jsonStr = new Encryption().Decrypt(text, key);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ChartDetailsRequestModel>(jsonStr);
        }

        public string EncryptGetSecurityQuestionByEmailIdAPIResponse(SecurityQueByEmailIdResponseModel securityQueByEmailIdResponseModel)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(securityQueByEmailIdResponseModel), Constants.GlobalEncryptionKey);
        }

        public string EncryptGetOTPResponse(OTPVerificationcode OTPResponse)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(OTPResponse), Constants.GlobalEncryptionKey);
        }

        public Stream DecryptGetDataFileAPIRequest(Stream file)
        {


            int streamEnd = Convert.ToInt32(file.Length);
            byte[] buffer = new byte[streamEnd];
            file.Read(buffer, 0, streamEnd);


            return new Encryption().DecryptTemp(buffer, Constants.GlobalEncryptionKey);
             
        }


        public IPassengerRequestModel DecryptIPassengerAPIRequest(string text, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
                //key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = (new Encryption().Decrypt(text, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<IPassengerRequestModel>(jsonStr);
        }

        public string EncryptIPassengerAPIResponse(IPassengerResponseModel ipassengerResponseModel, int profileId)
        {
            //string passwordEmail = new AdminHelper().GetUserNameAndPasswordByIdForEncryption(profileId);
            //string key = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(ipassengerResponseModel), key);
        }

        public string EncryptUpdatePlateResponseModel(PlateList updatePlatesResponseModel, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(updatePlatesResponseModel), key);
        }


        public string EncryptGetChartDetails(ChartDetailsResponseModel chartDetailsResponseModel, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(chartDetailsResponseModel), key);
        }

        public string EncryptNotificationAPI(NotificationResponse notificationResponse, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(notificationResponse), key);
        }



        public UpdateNotificationStatusRequest DecryptRequestToUpdateNotificationAPIRequest(string text, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = (new Encryption().Decrypt(text, key));  
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<UpdateNotificationStatusRequest>(jsonStr);
        }





        public string EncryptKMLFileAPI(KMLFileResponseModel kmlFileResponse, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(kmlFileResponse), key);
        }

        public string EncryptUpdateNotificationResponseAPI(UpdateNotificationStatusResponse updateNotificationStatusResponse, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(updateNotificationStatusResponse), key);
        }


        public KMLFileRequestModel DecryptRequestForKMLFile(string text, int profileId)
        {
            string key = "";
            if (profileId != 0)
            {
                key = new AdminHelper().GetKeyForEncryption(profileId);
            }
            else
            {
                key = Constants.GlobalEncryptionKey;
            }
            string jsonStr = (new Encryption().Decrypt(text, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<KMLFileRequestModel>(jsonStr);
        }

        public Stream convertToByteArray(Stream file)
        {
            byte[] buffer = null;
            Stream stream = null;
            using (MemoryStream ms = new MemoryStream())
            {
                file.CopyTo(ms);
                int size = ms.ToArray().Length;
                buffer = new byte[ms.ToArray().Length -26 ];

                ms.Position =12;
                ms.Read(buffer, 0, size- 26);
             
            stream = new MemoryStream(buffer);
            }
            return stream;
        }


        public LiveDataRequestModel DecryptRequestToGetLiveDataAPIRequest( RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<LiveDataRequestModel>(jsonStr);
        }


        public RegisterMaintenanceUserRequest DecryptRequestToRegisterMaintenanceUserAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<RegisterMaintenanceUserRequest>(jsonStr);
        }


        public string EncryptRegisterMaintenanceUserAPI(RegisterMaintenanceUserResponse response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }


        public IssueModel DecryptRequestToGenerateIssueAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<IssueModel>(jsonStr);
        }

        public string EncryptGenerateIssueAPI(GenerateIssueResponseModel response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }

        public ReassignIssueRequestModel DecryptReassignIssueAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<ReassignIssueRequestModel>(jsonStr);
        }

        public IssueModel DecryptUpdateIssueAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<IssueModel>(jsonStr);
        }

        public MaintenanceUserRequest DecrypGetMaintenanceUserListAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<MaintenanceUserRequest>(jsonStr);
        }

        public string EncryptGetMaintenanceUserListAPI(MaintenanceUserResponse response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }


        public SquawkListRequest DecrypGetSquawkListAPIRequest(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<SquawkListRequest>(jsonStr);
        }

        public string EncryptSquawkListAPI(SquawkListResponse response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }

        public SubscriptionResponseModel decGetSub(string data )
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(58);
            string jsonStr = (new Encryption().Decrypt(data, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<SubscriptionResponseModel>(jsonStr);
        }

        public UsAirportRequestModel DecryptGetUsAirportList(RequestModel objRequest)
        {
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<UsAirportRequestModel>(jsonStr);
        }

        public string EncryptAirportListAPI(UsAirportResponseModel response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }

        public string EncryptSubscriptionAPI(SubscriptionResponseModel response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }

        public ValidateReceiptRequestModel DecryptValidateReceipt(RequestModel objRequest)
        {
            ExceptionHandler.ReportError(new Exception(), "Request for DecryptValidateReceipt");
            string key = "";
            key = new AdminHelper().GetKeyForEncryption(objRequest.requestId);
            try
            {
                string jsonStr = (new Encryption().Decrypt(objRequest.request, key));
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                return jsonSerializer.Deserialize<ValidateReceiptRequestModel>(jsonStr);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(ex, "Exception in DecryptValidateReceipt");
                return new ValidateReceiptRequestModel();
            }
            return new ValidateReceiptRequestModel();
        }

        public string EncryptSubscriptionAPI(ValidateReceiptResponseMOdel response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }

        public string EncryptPassengerProLoginAPI(PassengerProLoginResponse response, int profileId)
        {
            string key = new AdminHelper().GetKeyForEncryption(profileId);
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(response), key);
        }
        //gp
        public FuelQuantityModel DecryptInsertAircraftDetailsAPIRequest(string cipherText)
        {
            string jsonStr = new Encryption().Decrypt(cipherText, Constants.GlobalEncryptionKey);
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            FuelQuantityModel aa = jsonSerializer.Deserialize<FuelQuantityModel>(jsonStr);
            return jsonSerializer.Deserialize<FuelQuantityModel>(jsonStr);
        }
        public string EncryptInsertAircraftDetailsAPIRequest(GeneralResponse fuelQuantity)
        {
            return new Encryption().Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(fuelQuantity), Constants.GlobalEncryptionKey);
        }
    }
}
