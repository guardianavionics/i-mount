﻿using GA.Common;
using GA.DataLayer;
using System;
using System.Text;
using System.Collections.Generic;
using GA.DataTransfer;
using System.Globalization;
using System.IO;
using System.Xml;
using GA.DataTransfer.Classes_for_Web;


namespace GA.ApplicationLayer
{
    public class CommonHelper
    {
        public PList plist;
        public Dictionary<string, dynamic> objDict;
        public string color = "";
        public string colorCode = "G"; //G stands for black, Y for yellow, R for red
        public bool isRangeExistForValue = false;
        public List<object> dictGetMinMaxValue = new List<object>();
        public Dictionary<string, object> arrMainMaxValue = new Dictionary<string, object>();
        public double min = 0;
        public double max = 0;
        public double keyMinValue = 0;
        public double keyMaxValue = 0;
        public bool isKeyPresent = true;
        public string plistHead = "";
        public List<string> plistHeads = new List<string>();

        //public static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static Profile GetProfileObject()
        {
            //logger.Trace("Logging");

            var profileObj = new Profile { Deleted = false, DateCreated = DateTime.UtcNow, LastUpdated = DateTime.UtcNow };

            return profileObj;
        }

        public static AircraftProfile GetAircraftProfile()
        {
            var airObj = new AircraftProfile
            {
                Deleted = false,
                LastUpdated = DateTime.UtcNow,
                Registration = "NGAV12",
                Capacity = "201",
                //TaxiFuel = "5",
                TaxiFuel = 5, //ChangeDataType
                //AdditionalFuel = "5",
                AdditionalFuel = 5, //ChangeDataType
                FuelUnit = Enumerations.FuelUnit.LIT.GetStringValue(),
                WeightUnit = Enumerations.WeightUnit.LBS.GetStringValue(),
                SpeedUnit = Enumerations.SpeedUnit.KTS.GetStringValue(),
                VerticalSpeed = Enumerations.VerticalSpeedUnit.FTMIN.GetStringValue(),
                Power = 65,
                RPM = 2400,
                MP = 24,
                Altitude = 2500,
                FuelBurnCruise = 34.00,
                TASCruise = 115,
                RateOfClimb = 650,
                TASClimb = 73,
                RateOfDescent = 500,
                FuelBurnRateOfDescent = 500,
                TASDescent = 125,
                IsAmericaAircraft = true,
                ImageUrl = "",
                EngineType = Enumerations.EngineType.SingleEngine.ToString(),
                UniqueId = DateTime.UtcNow.Ticks
            };

            return airObj;
        }


        internal static Preference GetPreference()
        {
            var obj = new Preference
            {
                DistanceUnit = Enumerations.Distance.NM.GetStringValue(),
                SpeedUnit = Enumerations.SpeedUnit.KTS.GetStringValue(),
                VerticalSpeedUnit = Enumerations.VerticalSpeedUnit.FTMIN.GetStringValue(),
                FuelUnit = Enumerations.FuelUnit.GAL.GetStringValue(),
                WeightUnit = Enumerations.WeightUnit.LBS.GetStringValue(),
                ShowCabin = false,
                ShowPitch = false,
                AudioRecording = false,
                SwitchToEnginForAlarm = false,
                DisplayAltitude = 0,
                LastUpdateDate = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow,
                LastUpdateDateForEmailList = DateTime.UtcNow

            };

            return obj;
        }

        internal static PushNotification CreatePushNotificationObject(int profileId, string tokenId, bool? IsValid)
        {
            return new PushNotification
            {

                ProfileId = profileId,
                TokenId = tokenId,
                NumberOfTimeFailed = 0,
                IsValid = IsValid != null && (bool)IsValid,
            };
        }


        public StringBuilder GetHeaderMenusForUser(string userType)
        {
            StringBuilder str = new StringBuilder();
            if (userType == Enumerations.UserType.Manufacturer.ToString())
            {
                str.Append(" <li class=\"nav-item\">@Html.ActionLink(GA.DataTransfer.GAResource.PilotSummary, \"PilotSummaryDetails\", \"Home\", new { @class = \"sel\" })</li>");
            }
            else
            {
                str.Append(" <li class=\"nav-item\">@Html.ActionLink(GA.DataTransfer.GAResource.PilotSummary, \"PilotSummaryDetails\", \"Home\", new { @class = \"sel\" })</li>");
                str.Append("<li class=\"nav-item\">@Html.ActionLink(GA.DataTransfer.GAResource.PilotLogPageName, \"FlightList\", \"Home\")</li>");
                str.Append("<li class=\"nav-item\">@Html.ActionLink(GA.DataTransfer.GAResource.DatalogPageName, \"ListAllJPIData\", \"Data\")</li>");
                str.Append("");
                str.Append("");
                str.Append("");
                str.Append("");
            }

            return str;
        }

        public List<DataLogListing> ParseJPIColourCode(List<DataLogListing> dataLogList, int aircraftId)
        {
            try
            {
                if (!File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
                {
                    return dataLogList;
                }

                plist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
                objDict = plist;
                plistHeads.Add("Meter");
                plistHeads.Add("Gauge");
                plistHeads.Add("Scroller");
                plistHeads.Add("DontShow");
                plistHeads.Add("EngineTemp");
                isKeyPresent = true;

                dataLogList.ForEach(d =>
                {
                    d.PitchColor = "";
                    d.RollColor = "";
                    d.YawColor = "";
                    d.LatitudeColor = "";
                    d.LongitudeColor = "";
                    d.FQLColor = string.IsNullOrEmpty(d.FQL) ? "" : getColorByKey("FQL", Convert.ToDouble(d.FQL));
                    d.FQRColor = string.IsNullOrEmpty(d.FQR) ? "" : getColorByKey("FQR", Convert.ToDouble(d.FQR));
                    d.FFColor = string.IsNullOrEmpty(d.FF) ? "" : getColorByKey("FF", Convert.ToDouble(d.FF));
                    d.OILPColor = string.IsNullOrEmpty(d.OILP) ? "" : getColorByKey("OIL-P", Convert.ToDouble(d.OILP));
                    d.OILTColor = string.IsNullOrEmpty(d.OILT) ? "" : getColorByKey("OIL-T", Convert.ToDouble(d.OILT));
                    d.MAPColor = string.IsNullOrEmpty(d.MAP) ? "" : getColorByKey("MAP", Convert.ToDouble(d.MAP));
                    d.RPMColor = string.IsNullOrEmpty(d.RPM) ? "" : getColorByKey("RPM", Convert.ToDouble(d.RPM));
                    d.Cht1Color = string.IsNullOrEmpty(d.Cht1) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht1));
                    d.Cht2Color = string.IsNullOrEmpty(d.Cht2) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht2));
                    d.Cht3Color = string.IsNullOrEmpty(d.Cht3) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht3));
                    d.Cht4Color = string.IsNullOrEmpty(d.Cht4) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht4));
                    d.Cht5Color = string.IsNullOrEmpty(d.Cht5) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht5));
                    d.Cht6Color = string.IsNullOrEmpty(d.Cht6) ? "" : getColorByKey("CHT", Convert.ToDouble(d.Cht6));
                    d.Egt1Color = string.IsNullOrEmpty(d.Egt1) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt1));
                    d.Egt2Color = string.IsNullOrEmpty(d.Egt2) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt2));
                    d.Egt3Color = string.IsNullOrEmpty(d.Egt3) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt3));
                    d.Egt4Color = string.IsNullOrEmpty(d.Egt4) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt4));
                    d.Egt5Color = string.IsNullOrEmpty(d.Egt5) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt5));
                    d.Egt6Color = string.IsNullOrEmpty(d.Egt6) ? "" : getColorByKey("EGT", Convert.ToDouble(d.Egt6));
                    d.OatColor = string.IsNullOrEmpty(d.Oat) ? "" : getColorByKey("OAT", Convert.ToDouble(d.Oat));
                    d.AMPColor = string.IsNullOrEmpty(d.AMP) ? "" : getColorByKey("AMP", Convert.ToDouble(d.AMP));
                    d.AMP2Color = string.IsNullOrEmpty(d.AMP2) ? "" : getColorByKey("AMP2", Convert.ToDouble(d.AMP2));
                    d.CDTColor = string.IsNullOrEmpty(d.CDT) ? "" : getColorByKey("CDT", Convert.ToDouble(d.CDT));
                    d.CLDColor = string.IsNullOrEmpty(d.CLD) ? "" : getColorByKey("CLD", Convert.ToDouble(d.CLD));
                    d.ENDColor = string.IsNullOrEmpty(d.END) ? "" : getColorByKey("END", Convert.ToDouble(d.END));
                    d.FPColor = string.IsNullOrEmpty(d.FP) ? "" : getColorByKey("FP", Convert.ToDouble(d.FP));
                    d.HPColor = string.IsNullOrEmpty(d.HP) ? "" : getColorByKey("HP", Convert.ToDouble(d.HP));
                    d.IATColor = string.IsNullOrEmpty(d.IAT) ? "" : getColorByKey("IAT", Convert.ToDouble(d.IAT));
                    d.MPGColor = string.IsNullOrEmpty(d.MPG) ? "" : getColorByKey("MPG", Convert.ToDouble(d.MPG));
                    d.REMColor = string.IsNullOrEmpty(d.REM) ? "" : getColorByKey("REM", Convert.ToDouble(d.REM));
                    d.REQColor = string.IsNullOrEmpty(d.REQ) ? "" : getColorByKey("REQ", Convert.ToDouble(d.REQ));
                    d.RESColor = string.IsNullOrEmpty(d.RES) ? "" : getColorByKey("RES", Convert.ToDouble(d.RES));
                    d.USDColor = string.IsNullOrEmpty(d.USD) ? "" : getColorByKey("USD", Convert.ToDouble(d.USD));
                    d.HMColor = string.IsNullOrEmpty(d.HM) ? "" : getColorByKey("HM", Convert.ToDouble(d.HM));
                    d.OPColor = string.IsNullOrEmpty(d.OP) ? "" : getColorByKey("OP", Convert.ToDouble(d.OP));
                    d.TitColor = string.IsNullOrEmpty(d.Tit) ? "" : getColorByKey("TIT", Convert.ToDouble(d.Tit));
                    d.TIT1Color = string.IsNullOrEmpty(d.TIT1) ? "" : getColorByKey("TIT1", Convert.ToDouble(d.TIT1));
                    d.TIT2Color = string.IsNullOrEmpty(d.TIT2) ? "" : getColorByKey("TIT2", Convert.ToDouble(d.TIT2));
                    d.TotalAircraftTimeColor = "";
                    d.EngineTimeColor = "";
                    d.ElevatorTrimPositionColor = "";
                    d.UnitsIndicatorColor = "";
                    d.UnitsIndicator2Color = "";
                    d.UnitsIndicator3Color = "";
                    d.UnitsIndicator4Color = "";
                    d.UnitsIndicator5Color = "";
                    d.UnitsIndicator6Color = "";
                    d.UnitsIndicator7Color = "";
                    d.UnitsIndicator8Color = "";
                    d.UnitsIndicator9Color = "";
                    d.UnitsIndicator10Color = "";
                    d.FlapPositionColor = "";
                    d.CarbTempColor = string.IsNullOrEmpty(d.CarbTemp) ? "" : getColorByKey("CARB-TEMP", Convert.ToDouble(d.CarbTemp));
                    d.CoolantPressureColor = string.IsNullOrEmpty(d.CoolantPressure) ? "" : getColorByKey("COOL-PRES", Convert.ToDouble(d.CoolantPressure));
                    d.CoolantTemperatureColor = string.IsNullOrEmpty(d.CoolantTemperature) ? "" : getColorByKey("COOL-TEMP", Convert.ToDouble(d.CoolantTemperature));
                    d.AileronTrimPositionColor = "";
                    d.RubberTrimPositionColor = "";
                    d.FuelQty3Color = string.IsNullOrEmpty(d.FuelQty3) ? "" : getColorByKey("FQ3", Convert.ToDouble(d.FuelQty3));
                    d.FuelQty4Color = string.IsNullOrEmpty(d.FuelQty4) ? "" : getColorByKey("FQ4", Convert.ToDouble(d.FuelQty4));
                    d.DiscreteInput1Color = "";
                    d.DiscreteInput2color = "";
                    d.DiscreteInput3Color = "";
                    d.DiscreteInput4Color = "";
                    d.IgnStatusColor = "";
                    d.SensorStatusColor = "";
                    d.ThrottlePositionColor = "";
                    d.BaroColor = "";
                    d.AirtempColor = string.IsNullOrEmpty(d.Airtemp) ? "" : getColorByKey("AIRT", Convert.ToDouble(d.Airtemp));
                    d.EcuTempColor = "";
                    d.BatteryvoltageColor = string.IsNullOrEmpty(d.Batteryvoltage) ? "" : getColorByKey("BAT", Convert.ToDouble(d.Batteryvoltage));
                    d.Sen1Color = "";
                    d.Sen2Color = "";
                    d.Sen3Color = "";
                    d.Sen4Color = "";
                    d.Sen5Color = "";
                    //d.Time =d.Time;
                    //d.Date = (Convert.ToDateTime(d.Date)).ToShortDateString();

                });
            }
            catch (Exception ex)
            {
                return dataLogList;
            }

            return dataLogList;
        }

        public string getColorByKey(string key, double value)
        {
            try
            {
                foreach (string strHeadName in plistHeads)
                {
                    var IndicatorList = objDict[strHeadName] as List<object>;

                    foreach (Dictionary<string, dynamic> p in IndicatorList)
                    {

                        if (p["identifier"] == key)
                        {
                            isKeyPresent = true;
                            keyMaxValue = Convert.ToDouble(p["max"]);
                            dictGetMinMaxValue = p["separator"] as List<object>;
                            for (int i = 0; i < dictGetMinMaxValue.Count; i++)
                            {
                                arrMainMaxValue = dictGetMinMaxValue[i] as Dictionary<string, object>;
                                min = Convert.ToDouble(arrMainMaxValue["min"]);
                                max = Convert.ToDouble(arrMainMaxValue["max"]);
                                if ((value >= min && (value < max || value == keyMaxValue)) || (value >= min && value <= max && arrMainMaxValue["color"].ToString() == "O"))
                                {
                                    colorCode = arrMainMaxValue["color"].ToString();
                                    //color = colorCode == "O" ? "red" : (colorCode == "G" ? "green" : "yellow");
                                    color = colorCode == "O" ? "red" : (colorCode == "G" ? "" : (colorCode == "Y" ? "yellow" : (colorCode == "B" ? "black;color:white" : "#E6E6E6")));
                                    return color;
                                    //isRangeExistForValue = true;
                                    //break;
                                }
                            }
                            if (!isRangeExistForValue)
                            {
                                color = "red";
                                return color;
                            }
                            //break;
                        }
                    }

                    if (!isKeyPresent)
                    {
                        color = "";
                        return color;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }


        public List<MFDSettings> ParsePListForMFD(int aircraftId)
        {
            if (!File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
            {
                return new List<MFDSettings>();
            }

            try
            {
                plist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
            }
            catch (Exception ex)
            {
                plist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\EnginemasterFiles\JPIDefault.xml");
            }
            objDict = plist;
            plistHeads.Clear();
            plistHeads.Add("Speed Marking-Speed");
            plistHeads.Add("Round Gauge-Meter");
            plistHeads.Add("Line Gauge-Gauge");
            plistHeads.Add("Scroller-Scroller");
            plistHeads.Add("Dont Show-DontShow");
            plistHeads.Add("Engine Temperature-EngineTemp");

            List<string> listMFD = new List<string>(new string[] { "SPEED_MARKINGS", "RPM", "MAP", "CHT", "EGT", "TIT", "FF", "CDT", "FP", "OIL-P", "OIL-T", "BAT", "FQL", "FQR", "AMP" });
            List<MFDSettings> objMFDList = new List<MFDSettings>();

            MFDSettings objMFD;
            List<seperator> seperatorList;
            foreach (string strHeadName in plistHeads)
            {
                if (!objDict.ContainsKey(strHeadName.Split('-')[1]))
                {
                    continue;
                }

                var IndicatorList = objDict[strHeadName.Split('-')[1]] as List<object>;
                var IndicatorList1 = new Dictionary<string, dynamic>();
                if (strHeadName.Split('-')[1] == "Speed")
                {
                    var plistSpeed = objDict[strHeadName.Split('-')[1]] as PList;
                    IndicatorList = new List<object>();
                    IndicatorList1 = plistSpeed["SPEED_MARKINGS"] as Dictionary<string, dynamic>;
                    IndicatorList.Add(IndicatorList1);
                    IndicatorList1 = plistSpeed["SPEED_V"] as Dictionary<string, dynamic>;

                }
                if (IndicatorList.Count > 0)
                {

                    foreach (Dictionary<string, dynamic> p in IndicatorList)
                    {

                        if (p.ContainsKey("identifier"))
                        {
                            if (!listMFD.Contains(p["identifier"]))
                            {
                                continue;
                            }
                        }
                        objMFD = new MFDSettings();
                        objMFD.identifier = p["identifier"];

                        if (p.ContainsKey("max"))
                        {
                            objMFD.max = p["max"].ToString();
                        }
                        if (p.ContainsKey("min"))
                        {
                            objMFD.min = p["min"].ToString();
                        }


                        string identifier = string.Empty;
                        if (strHeadName.Split('-')[1] == "Speed")
                        {
                            identifier = p["type"];
                        }
                        else
                        {
                            identifier = p["identifier"];
                        }

                        string identifierFullName = string.Empty;
                        identifierFullName = GetFullNameOfIdentifier(identifier);
                        seperatorList = new List<seperator>();
                        if (p["type"] != "GENERAL")
                        {

                            dictGetMinMaxValue = p["separator"] as List<object>;

                            for (int i = 0; i < dictGetMinMaxValue.Count; i++)
                            {
                                arrMainMaxValue = dictGetMinMaxValue[i] as Dictionary<string, object>;
                                colorCode = arrMainMaxValue["color"].ToString();
                                color = colorCode == "O" ? "red" : (colorCode == "G" ? "green" : (colorCode == "Y" ? "yellow" : (colorCode == "B" ? "black" : "#E6E6E6")));

                                seperatorList.Add(new seperator
                                {
                                    color = color,
                                    max = arrMainMaxValue["max"].ToString(),
                                    min = arrMainMaxValue["min"].ToString()
                                });
                            }
                        }

                        if (seperatorList.Count > 0)
                        {
                            objMFD.seperatorList = seperatorList;
                        }

                        objMFDList.Add(objMFD);
                    }


                }

            }

            return objMFDList;
        }



        public StringBuilder ParsePListForEngineSetting(int aircraftId)
        {

            int totalRnage = 0;  //Difference of max and Min. Ex. For Oil-t min 0 and max 50 then totalRange = 50
            int totalRangeUptoTableCell = 0;
            int CurrentCellWidth = 0;
            double customCurrentCellWidth = 0.0;
            bool showLabel = true;


            plist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");

            objDict = plist;
            plistHeads.Clear();
            plistHeads.Add("Speed Marking-Speed");
            plistHeads.Add("Round Gauge-Meter");
            plistHeads.Add("Line Gauge-Gauge");
            plistHeads.Add("Scroller-Scroller");
            plistHeads.Add("Dont Show-DontShow");
            plistHeads.Add("Engine Temperature-EngineTemp");
            plistHeads.Add("EngineTempCylinderCount");

            List<string> htmlString = new List<string>();
            StringBuilder strBuilder = new StringBuilder();
            StringBuilder strTable = new StringBuilder();
            StringBuilder mainDiv = new StringBuilder();
            StringBuilder strOriginalRangeWithColor = new StringBuilder();
            StringBuilder strdropdownList = new StringBuilder();
            //mainDiv.Append(" <div id=\"accordion\" style=\"width:600px\">");
            mainDiv.Append(" <div class=\"accordion\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\" style=\"width:600px\">");


            var id = 0;
            var hid = "";
            var pid = "";
            var phref = "";
            foreach (string strHeadName in plistHeads)
            {

                id = id + 1;
                hid = "H" + id.ToString();
                pid = "C" + id.ToString();
                phref = "#C" + id.ToString();

                if (strHeadName == "EngineTempCylinderCount")
                {
                    int cylinderCount = 4;
                     
                    if (plist.ContainsKey("EngineTempCylinderCount"))
                    {
                        cylinderCount = Convert.ToInt32(plist["EngineTempCylinderCount"]);
                    }
                    StringBuilder strCylinder = new StringBuilder("<div id=\"div_EngCount\" style=\"width:550px;margin-top:50px;float:left;\">");
                    strCylinder.Append("<div style=\"width:550px;float:left; \">");
                    strCylinder.Append("<div style=\"width:500px;float:left\">");
                    strCylinder.Append("<div style=\"width:250px;float:left;font-size:13px\"> &nbsp;&nbsp;&nbsp; <b> Number Of Cylinders</b> </div>");
                    strCylinder.Append("<div style=\"width:250px;float:left;text-align:center;\"> </div>");
                    strCylinder.Append("</div>");
                    strCylinder.Append("<div style=\"width:500px;float:left;text-align:center;font-size:14px\">");
                    if (cylinderCount == 4)
                    {
                        strCylinder.Append("<div onclick=\"cylinderClick('div4Cylinder')\" id=\"div4Cylinder\" class=\"cylinderDivActive\" style=\"cursor:pointer;width:20%;float:left; margin-left:30%;border:1px solid;\">4 Cylinder</div>");
                        strCylinder.Append("<div onclick=\"cylinderClick('div6Cylinder')\" id=\"div6Cylinder\" class=\"cylinderDivIncativeActive\" style=\"cursor:pointer;width:20%;float:left;border:1px solid\">6 Cylinder</div>");
                    }
                    else {
                        strCylinder.Append("<div onclick=\"cylinderClick('div4Cylinder')\" id=\"div4Cylinder\" class=\"cylinderDivIncativeActive\" style=\"cursor:pointer;width:20%; float:left;margin-left:40%;border:1px solid;\">4 Cylinder</div>");
                        strCylinder.Append("<div onclick=\"cylinderClick('div6Cylinder')\" id=\"div6Cylinder\" class=\"cylinderDivActive\" style=\"cursor:pointer;width:20%;float:left; border:1px solid\">6 Cylinder</div>");
                    }
                    
                    strCylinder.Append("</div>");
                    strCylinder.Append("</div>");

                    mainDiv.Append("<div onclick=\"EngineTabClick();\" class=\"panel\"><a class=\"panel-heading\" role=\"tab\" id=" + hid + " data-toggle=\"collapse\" data-parent=\"#accordion\" href=" + phref + " aria-expanded=\"false\" aria-controls=" + pid + "> <div class=\"clearfix\"> <h4 class=\"panel-title col-xs-11\">" + "Engine Temperature Cylinders" + "</h4> <i class=\"fa arrow col-xs-1 text-right no-padding pull-right\"></i>  </div>  </a> <div id=" + pid + " class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=" + hid + "><div class=\"panel-body\"><div id=\"" + "div_EngineCylinder" + "\">" + strCylinder.ToString() + "</div></div></div></div>");
                    mainDiv.Append("</div>");
                    continue;
                }

                if (!objDict.ContainsKey(strHeadName.Split('-')[1]))
                {
                    continue;
                }

               

                //mainDiv.Append("<h3>" + strHeadName.Split('-')[0] + "</h3> ");


                strTable.Clear();  // have all the tables for single Head (Like Guage) in HTML string

                var IndicatorList = objDict[strHeadName.Split('-')[1]] as List<object>;
                var IndicatorList1 = new Dictionary<string, dynamic>();
                if (strHeadName.Split('-')[1] == "Speed")
                {
                    var plistSpeed = objDict[strHeadName.Split('-')[1]] as PList;
                    IndicatorList = new List<object>();
                    IndicatorList1 = plistSpeed["SPEED_MARKINGS"] as Dictionary<string, dynamic>;
                    IndicatorList.Add(IndicatorList1);
                    IndicatorList1 = plistSpeed["SPEED_V"] as Dictionary<string, dynamic>;




                }
                if (IndicatorList.Count > 0)
                {

                    foreach (Dictionary<string, dynamic> p in IndicatorList)
                    {
                        strBuilder.Clear();
                        if (p.ContainsKey("audioForHigh"))
                        {
                            strBuilder.Append("<key>audioForHigh</key><string>" + p["audioForHigh"] + "</string>");
                        }
                        if (p.ContainsKey("audioForLow"))
                        {
                            strBuilder.Append("<key>audioForLow</key><string>" + p["audioForLow"] + "</string>");
                        }
                        if (p.ContainsKey("decimalPlaces"))
                        {
                            strBuilder.Append("<key>decimalPlaces</key><string>" + p["decimalPlaces"] + "</string>");
                        }
                        if (p.ContainsKey("identifier"))
                        {
                            strBuilder.Append("<key>identifier</key><string>" + p["identifier"] + "</string>");
                        }
                        if (p.ContainsKey("max"))
                        {
                            strBuilder.Append("<key>max</key><real>" + p["max"] + "</real>");
                            totalRnage = Convert.ToInt32(p["max"]) - Convert.ToInt32(p["min"]);
                            keyMaxValue = Convert.ToDouble(p["max"]);
                        }
                        if (p.ContainsKey("min"))
                        {
                            strBuilder.Append("<key>min</key><real>" + p["min"] + "</real>");
                        }

                        if (p.ContainsKey("minorTickIncrement"))
                        {
                            strBuilder.Append("<key>minorTickIncrement</key><real>" + p["minorTickIncrement"] + "</real>");
                        }
                        if (p.ContainsKey("tickIncrement"))
                        {
                            strBuilder.Append("<key>tickIncrement</key><real>" + p["tickIncrement"] + "</real>");
                        }
                        if (p.ContainsKey("title"))
                        {
                            strBuilder.Append("<key>title</key><string>" + p["title"] + "</string>");
                        }
                        if (p.ContainsKey("type"))
                        {
                            strBuilder.Append("<key>type</key><string>" + p["type"] + "</string>");
                        }
                        if (p.ContainsKey("unit"))
                        {
                            strBuilder.Append("<key>unit</key><string>" + p["unit"] + "</string>");
                        }

                        if (p.ContainsKey("showTickFromMinLimit"))
                        {
                            strBuilder.Append("<key>showTickFromMinLimit</key><real>" + p["showTickFromMinLimit"] + "</real>");
                        }
                        if (p.ContainsKey("showTickToMaxLimit"))
                        {
                            strBuilder.Append("<key>showTickToMaxLimit</key><string>" + p["showTickToMaxLimit"] + "</string>");
                        }
                        if (p.ContainsKey("showLabel"))
                        {
                            string showLabelString = Convert.ToString(p["showLabel"]);
                            strBuilder.Append("<key>showLabel</key><" + showLabelString.ToLower() + " />");
                        }

                        strdropdownList.Clear();
                        string identifier = string.Empty;
                        string IdentifierForRPM = string.Empty;
                        if (strHeadName.Split('-')[1] == "Speed")
                        {
                            identifier = p["type"];
                        }
                        else
                        {
                            identifier = p["identifier"];
                        }
                        if (identifier == "RPM")
                        {
                            IdentifierForRPM = identifier;
                            identifier = "RPMTest";
                        }
                        strdropdownList.Append("<span style=\"font-size:13px\"> Display On </span>&nbsp;<select id=\"ddl_" + identifier + "\" style=\"width:100px;font-size:12px\"    onchange=\"OnDDLDisplayChange(this.id)\">");

                        if (strHeadName.Split('-')[1] == "Gauge")
                        {
                            strdropdownList.Append("<option value=\"Gauge\" selected=\"selected\" >Line Gauge</option>");
                            strdropdownList.Append("<option value=\"Scroller\" >Scroller</option>");
                            strdropdownList.Append("<option value=\"DontShow\" >DontShow</option>");
                            strdropdownList.Append("</select>");
                        }
                        else if (strHeadName.Split('-')[1] == "Scroller")
                        {
                            strdropdownList.Append("<option value=\"Scroller\" selected=\"selected\" >Scroller</option>");
                            if (p["type"] != "GENERAL")
                            {
                                strdropdownList.Append("<option value=\"Gauge\" >Line Gauge</option>");
                                strdropdownList.Append("<option value=\"DontShow\" >DontShow</option>");
                                strdropdownList.Append("</select>");
                            }
                            else
                            {
                                strdropdownList.Append("<option value=\"DontShow\" >DontShow</option>");
                                strdropdownList.Append("</select>");
                            }
                        }
                        else if (strHeadName.Split('-')[1] == "DontShow")
                        {
                            strdropdownList.Append("<option value=\"DontShow\" selected=\"selected\" >DontShow</option>");
                            if (p["type"] != "GENERAL")
                            {
                                strdropdownList.Append("<option value=\"Gauge\" >Line Gauge</option>");
                                strdropdownList.Append("<option value=\"Scroller\" >Scroller</option>");
                                strdropdownList.Append("</select>");
                            }
                            else
                            {
                                strdropdownList.Append("<option value=\"Scroller\" >Scroller</option>");
                                strdropdownList.Append("</select>");
                            }
                        }
                        else
                        {
                            strdropdownList.Clear();
                        }

                        string identifierFullName = string.Empty;
                        identifierFullName = GetFullNameOfIdentifier(identifier);
                        if (identifier == "RPMTest")
                        {
                            identifierFullName = GetFullNameOfIdentifier(IdentifierForRPM);
                        }
                            if (p["type"] != "GENERAL")
                        {
                            dictGetMinMaxValue = p["separator"] as List<object>;
                            //strTable.Append("<div id=\"div_" + identifier + "\" style=\"width: 550px;margin-top: 50px;float: left; \"><div style=\"width: 550px; float: left;\"> <div style=\"width: 500px; float: left\"><div style=\"width:250px;float:left;font-size:13px\"> &nbsp;&nbsp;&nbsp; <b> " + identifier + "</b> </div><div style=\"width:250px;float:left;text-align:center;\">" + strdropdownList.ToString() + " </div></div></div> <div style=\"width:550px;float:left;height:30px;\"><div class=\"colorpickerwrap clearfix\" id=\"divColor_" + identifier + "\"> <div onclick=\"OnSelectColorForTh('" + identifier + "','green')\" class=\"colorbox green\"></div> <div onclick=\"OnSelectColorForTh('" + identifier + "','yellow')\" class=\"colorbox yellow\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','red')\" class=\"colorbox red\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','black')\" class=\"colorbox black\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','white')\" class=\"colorbox white last\"></div><span style=\"display:none;\" id=\"spnToHoldTableCellId_" + identifier + "\"></span></div></div>  <div style=\"width: 550px; float: left\"><div style=\"width: 550px; float: left\"><table id=\"" + identifier + "\" type=\"" + strHeadName.Split('-')[1] + "\" width=\"500px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                            strTable.Append("<div id=\"div_" + identifier + "\" style=\"width: 550px;margin-top: 50px;float: left; \"><div style=\"width: 550px; float: left;\"> <div style=\"width: 500px; float: left\"><div style=\"width:250px;float:left;font-size:13px\"> &nbsp;&nbsp;&nbsp; <b> " + identifierFullName + "</b> </div><div style=\"width:250px;float:left;text-align:center;\">" + strdropdownList.ToString() + " </div></div></div> <div style=\"width:550px;float:left;height:30px;\"><div class=\"colorpickerwrap clearfix\" id=\"divColor_" + identifier + "\"> <div onclick=\"OnSelectColorForTh('" + identifier + "','green')\" class=\"colorbox green\"></div> <div onclick=\"OnSelectColorForTh('" + identifier + "','yellow')\" class=\"colorbox yellow\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','red')\" class=\"colorbox red\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','black')\" class=\"colorbox black\"></div><div onclick=\"OnSelectColorForTh('" + identifier + "','white')\" class=\"colorbox white last\"></div><span style=\"display:none;\" id=\"spnToHoldTableCellId_" + identifier + "\"></span></div></div>  <div style=\"width: 550px; float: left\"><div style=\"width: 550px; float: left\"><table tbidentifier=\"tblColResize\" id=\"" + identifier + "\" type=\"" + strHeadName.Split('-')[1] + "\" width=\"500px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                            strTable.Append("<tr>");
                            CurrentCellWidth = 0;
                            totalRangeUptoTableCell = 0;
                            strOriginalRangeWithColor.Clear();
                            strOriginalRangeWithColor.Append("<div id=\"divOriginalRangeWithColor_" + identifier + "\" style=\"display:none\">");
                            for (int i = 0; i < dictGetMinMaxValue.Count; i++)
                            {
                                arrMainMaxValue = dictGetMinMaxValue[i] as Dictionary<string, object>;
                                min = Convert.ToDouble(arrMainMaxValue["min"]);
                                max = Convert.ToDouble(arrMainMaxValue["max"]);
                                if (arrMainMaxValue.ContainsKey("showLabel"))
                                {
                                    showLabel = Convert.ToBoolean(arrMainMaxValue["showLabel"]);
                                }
                                else
                                {
                                    showLabel = true;
                                }
                                colorCode = arrMainMaxValue["color"].ToString();
                                color = colorCode == "O" ? "red" : (colorCode == "G" ? "green" : (colorCode == "Y" ? "yellow" : (colorCode == "B" ? "black" : "#E6E6E6")));
                                strOriginalRangeWithColor.Append(min.ToString() + "~" + max.ToString() + "~" + color + "~" + showLabel.ToString().ToLower() + "^"); // ^ is use as a special character to split the string

                                if (i == dictGetMinMaxValue.Count - 1)
                                {
                                    CurrentCellWidth = Convert.ToInt32(Convert.ToDouble(500 / (double)totalRnage) * Convert.ToInt32(max - min));
                                    customCurrentCellWidth = Convert.ToDouble(Convert.ToDouble(500 / (double)totalRnage) * Convert.ToDouble(max - min));
                                    totalRangeUptoTableCell = totalRangeUptoTableCell + CurrentCellWidth;

                                    if (dictGetMinMaxValue.Count == 1)
                                    {
                                        strTable.Append("<th id=\"" + identifier + "_th" + i.ToString() + "\" customCellWidth =\"" + customCurrentCellWidth + "\" ondblclick=\"CreateNode(this.id,'" + identifier + "');\" onclick=\"OnTableCellClickForColor('" + identifier + "', this.id)\"  style=\"background-color:" + color + ";width:" + CurrentCellWidth.ToString() + "px;text-align: right\" ><span id=\"spnThWidth_" + identifier + "_th" + i.ToString() + "\" style=\"font-size:9px;position:absolute;margin-top:25px;margin-left:-5px;color:black;text-shadow:none\">" + max.ToString() + "</span> </th>");
                                    }
                                    else
                                    {
                                        strTable.Append("<th id=\"" + identifier + "_th" + i.ToString() + "\" customCellWidth =\"" + customCurrentCellWidth + "\" ondblclick=\"CreateNode(this.id,'" + identifier + "');\" onclick=\"OnTableCellClickForColor('" + identifier + "', this.id)\"  style=\"background-color:" + color + ";width:" + CurrentCellWidth.ToString() + "px;text-align: right\" ><span style=\"display:none\" id=\"spnThWidth_" + identifier + "_th" + i.ToString() + "\" style=\"font-size:9px;position:absolute;margin-top:25px;margin-left:-5px;color:black;text-shadow:none\">" + max.ToString() + "</span> </th>");
                                    }

                                }
                                else
                                {
                                    CurrentCellWidth = Convert.ToInt32(Convert.ToDouble(500 / (double)totalRnage) * Convert.ToInt32(max - min));
                                    customCurrentCellWidth = Convert.ToDouble(Convert.ToDouble(500 / (double)totalRnage) * Convert.ToDouble(max - min));
                                    totalRangeUptoTableCell = totalRangeUptoTableCell + CurrentCellWidth;
                                    strTable.Append("<th id=\"" + identifier + "_th" + i.ToString() + "\" customCellWidth =\"" + customCurrentCellWidth + "\" ondblclick=\"CreateNode(this.id,'" + identifier + "');\"  onclick=\"OnTableCellClickForColor('" + identifier + "', this.id)\"   style=\"background-color:" + color + ";width:" + CurrentCellWidth.ToString() + "px;text-align: right\" ><span id=\"spnThWidth_" + identifier + "_th" + i.ToString() + "\" style=\"font-size:9px;position:absolute;margin-top:25px;margin-left:-5px;color:black;text-shadow:none\">" + max.ToString() + "</span> </th>");
                                }
                            }
                            strOriginalRangeWithColor.Append("</div>");
                            StringBuilder strSPEED_V = new StringBuilder();
                            if (strHeadName.Split('-')[1] == "Speed")
                            {
                                string stallDiv = "";
                                if (IndicatorList1.ContainsKey("STALL"))
                                {
                                    stallDiv = "<div style=\"width: 100px; float: left;text-align:right\">  STALL : <input type=\"text\" id=\"spnStall\" onkeydown=\"validateNumberOnly(event)\" value=\"" + IndicatorList1["STALL"] + "\" style=\"width:40px\">  </div>";
                                }
                                strSPEED_V.Append("<div style=\"width: 550px; float: left;margin-top:25px\"> <div style=\"width: 500px; float: left;font-size:13px\"> <div style=\"width: 200px; float: left\"> Vx : <input type=\"text\" id=\"spnVx\" style=\"width:40px\" onkeydown=\"validateNumberOnly(event, this.id)\" value=\"" + IndicatorList1["VX"] + "\">  </div>  <div style=\"width: 200px; float: left;text-align:left\">  Vne : <input type=\"text\" id=\"spnVne\" onkeydown=\"validateNumberOnly(event)\" value=\"" + IndicatorList1["VNE"] + "\" style=\"width:40px\">  </div>"+ stallDiv + " </div>  </div>");
                                strSPEED_V.Append("<div style=\"width: 550px; float: left;margin-top:25px\"> <div style=\"width: 500px; float: left;font-size:13px\"> <div style=\"width: 200px; float: left\"> Vy : <input type=\"text\" id=\"spnVy\" style=\"width:40px\" onkeydown=\"validateNumberOnly(event, this.id)\" value=\"" + IndicatorList1["VY"] + "\">  </div>  <div style=\"width: 200px; float: left;text-align:left\">  Vmc : <input type=\"text\" id=\"spnVmc\" onkeydown=\"validateNumberOnly(event)\" value=\"" + IndicatorList1["VMC"] + "\" style=\"width:40px\">  </div> </div>  </div>");
                            }
                            //<script> var onSampleResized = function (e) { var columns = $(e.currentTarget).find(\"th\"); var tablename = columns[0].id.toString().split('_')[0]; var msg = \"a\"; var min = 0; var max = 0; var cellWidth = 0; var i = 0; var totalWidth = 0; var customWidth = 0.0; var CurrentWidth = 0.0; var spanValueToBeChange = false; min = $(\"#spnMin_\" + columns[i].id.toString().split('_')[0]).val(); max = $(\"#spnMax_\" + columns[i].id.toString().split('_')[0]).val(); var arrRangeWithColor1 = [];    if ($(\"#divOriginalRangeWithColor_\" + tablename).text().substr($(\"#divOriginalRangeWithColor_\" + tablename).text().length - 1) == \"^\") {arrRangeWithColor1 = $(\"#divOriginalRangeWithColor_\" + tablename).text().slice(0, -1).split('^'); } else { arrRangeWithColor1 = $(\"#divOriginalRangeWithColor_\" + tablename).text().split('^');} totalWidth = parseInt(min); columns.each(function () { if (spanValueToBeChange == false) { customWidth = parseFloat($('#' + columns[i].id).attr(\"customcellwidth\")); CurrentWidth = parseFloat($(this)[0].style.width.replace(\"px\", '')); if (customWidth - CurrentWidth > 2 || CurrentWidth - customWidth > 2) { spanValueToBeChange = true; cellWidth = parseInt((CurrentWidth * (parseInt(max) - parseInt(min))) / parseFloat(500)); $(this).attr(\"customcellwidth\", CurrentWidth.toString()); if (i == 0) { $(\"#spnThWidth_\" + columns[i].id.toString()).text((totalWidth + cellWidth).toString()); } else { $(\"#spnThWidth_\" + columns[i].id.toString()).text((parseInt($(\"#spnThWidth_\" + columns[i - 1].id.toString()).text()) + cellWidth).toString()); } var newRange = \"\"; var arr = arrRangeWithColor1[i].split('~'); arr[1] = $(\"#spnThWidth_\" + columns[i].id.toString()).text(); newRange = arr[1]; arrRangeWithColor1[i] = arr[0] + \"~\" + arr[1] + \"~\" + arr[2] + \"~\" + arr[3]; var arr2 = arrRangeWithColor1[i + 1].split('~'); arrRangeWithColor1[i + 1] = newRange + \"~\" + arr2[1] + \"~\" + arr2[2] + \"~\" + arr2[3]; var newArrRangeWithColor1 = arrRangeWithColor1.join('^'); $(\"#divOriginalRangeWithColor_\" + tablename).text(newArrRangeWithColor1); $(\"#\" + columns[i + 1].id.toString()).attr(\"customcellwidth\", parseFloat($(\"#\" + columns[i + 1].id.toString())[0].style.width.replace(\"px\", ''))); } } i += 1; }) }; $(\"#" + identifier + "\").colResizable({ liveDrag: true,  gripInnerHtml: \"<div class='grip'></div>\",   draggingClass: \"dragging\", onResize: onSampleResized });</script>
                            strTable.Append("</tr></table>" + strOriginalRangeWithColor.ToString() + "<div id=\"divKeyValueDetails_" + identifier + "\" style=\"display:none\" >" + strBuilder.ToString() + "</div>" + "</div>  </div> <div style=\"width:550px;float:left;height:30px\"> &nbsp;</div> <div style=\"width: 550px; float: left\"> <div style=\"width: 500px; float: left;font-size:13px\"> <div style=\"width: 250px; float: left\"> Min : <input id=\"spnMin_" + identifier + "\" type=\"text\" style=\"width:40px\" onkeydown = \"validateNumberOnly(event, this.id)\" onchange=\"funOnMinValueChange(this.id)\" value=\"" + p["min"] + "\">  </div>  <div style=\"width: 250px; float: left;text-align:right\">  Max : <input id=\"spnMax_" + identifier + "\" type=\"text\"  onkeydown = \"validateNumberOnly(event)\" onchange=\"funOnMaxValueChange(this.id)\" value=\"" + p["max"] + "\" style=\"width:40px\">  </div> </div>  </div>" + strSPEED_V.ToString() + "</div>");
                        }
                        else
                        {
                            //border:0px
                            //strTable.Append("<div id=\"div_" + identifier + "\" style=\"width: 550px;margin-top:35px;float:left\"><div style=\"width: 550px; float: left\"><div style=\"width: 550px; float: left\"><table id=\"" + identifier + "\" type=\"General\" style=\"width:500px;margin-left:10px;text-align:left;font-size:13px\"><tr><td style=\"width:50%;border-bottom:0px;border-left:0px\"> <b>" + identifier + "</b></td><td style=\"border-bottom:0px;border-left:0px\">&nbsp; " + strdropdownList.ToString() + " </td> </tr><tr><td style=\"border-bottom:0px;border-left:0px\"  colspan=\"2\"> Possible Values</td></tr><tr><td style=\"border-bottom:0px;border-left:0px\" colspan=\"2\"> " + p["ExpectedValues"] + "</td> </tr> </table></div></div></div>");
                            strTable.Append("<div id=\"div_" + identifierFullName + "\" style=\"width: 550px;margin-top:35px;float:left\"><div style=\"width: 550px; float: left\"><div style=\"width: 550px; float: left\"><table id=\"" + identifier + "\" type=\"General\" style=\"width:500px;margin-left:10px;text-align:left;font-size:13px\"><tr><td style=\"width:50%;border-bottom:0px;border-left:0px\"> <b>" + identifier + "</b></td><td style=\"border-bottom:0px;border-left:0px\">&nbsp; " + strdropdownList.ToString() + " </td> </tr><tr><td style=\"border-bottom:0px;border-left:0px\"  colspan=\"2\"> Possible Values</td></tr><tr><td style=\"border-bottom:0px;border-left:0px\" colspan=\"2\"> " + p["ExpectedValues"] + "</td> </tr> </table></div></div></div>");
                        }
                    }
                    if (id == 1)
                    {
                        mainDiv.Append("  <div onclick=\"EngineTabClick();\" class=\"panel\"><a class=\"panel-heading\" role=\"tab\" id=" + hid + " data-toggle=\"collapse\" data-parent=\"#accordion\" href=" + phref + " aria-expanded=\"true\" aria-controls=" + pid + "> <div class=\"clearfix\"> <h4 class=\"panel-title col-xs-11\">" + strHeadName.Split('-')[0] + "</h4> <i class=\"fa arrow col-xs-1 text-right no-padding pull-right\"></i>  </div>  </a> <div id=" + pid + " class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=" + hid + "><div class=\"panel-body\"><div id=\"" + strHeadName.Split('-')[1] + "\">" + strTable.ToString() + "</div></div></div></div>");
                    }
                    else
                    {
                        mainDiv.Append("  <div onclick=\"EngineTabClick();\" class=\"panel\"><a class=\"panel-heading\" role=\"tab\" id=" + hid + " data-toggle=\"collapse\" data-parent=\"#accordion\" href=" + phref + " aria-expanded=\"false\" aria-controls=" + pid + "> <div class=\"clearfix\"> <h4 class=\"panel-title col-xs-11\">" + strHeadName.Split('-')[0] + "</h4> <i class=\"fa arrow col-xs-1 text-right no-padding pull-right\"></i>  </div>  </a> <div id=" + pid + " class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=" + hid + "><div class=\"panel-body\"><div id=\"" + strHeadName.Split('-')[1] + "\">" + strTable.ToString() + "</div></div></div></div>");
                    }

                }
                else
                {
                    mainDiv.Append("<div id=\"" + strHeadName.Split('-')[1] + "\"></div>");
                    //htmlString.Add("");
                }
            }
            mainDiv.Append("</div>");
            return mainDiv;
        }



        public void ResetEngineSetting(int aircraftId, string ComponentManufacturerName)
        {
            StringBuilder strXML = new StringBuilder();

            try
            {
                string engineMasterFileName = "";

                if (ComponentManufacturerName == "JPI")
                {
                    engineMasterFileName = "JPI_Setting.xml";
                }
                else if (ComponentManufacturerName == "Garmin3X")
                {
                    engineMasterFileName = "GarminJPIHeader.xml";
                }
                else
                {
                    engineMasterFileName = "ULPowerJPIHeader.xml";
                }

                PList EngineMasterFilePlist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\EnginemasterFiles\" + engineMasterFileName.ToString());


                string CategoryName = "";  //like Meter, Scroller ....

                StringBuilder strScroller = new StringBuilder();
                string strXMLForSingleCategory = "";
                List<string> allKeysOfCategory = new List<string>();
                bool flag = false; // user for ExtraGuage and Scroller as the ExtraGuage is also shown in scroller

                strXML.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\"><dict>");

                foreach (var objDict in EngineMasterFilePlist)
                {

                    CategoryName = objDict.Key;
                    allKeysOfCategory.Clear();
                    foreach (var objKey in objDict.Value)
                    {
                        allKeysOfCategory.Add(objKey.Key);
                    }




                    if (CategoryName != "ExtraGuage" && CategoryName != "Scroller")
                    {
                        strXML.Append("<key>" + CategoryName + "</key>");
                        strXMLForSingleCategory = GetXmlStringForSingleCategory(CategoryName, allKeysOfCategory);
                        if (strXMLForSingleCategory == "")
                        {
                            strXML.Append("<array />");
                        }
                        else
                        {
                            strXML.Append("<array>" + strXMLForSingleCategory + "</array>");
                        }
                    }
                    else
                    {

                        if (!flag)
                        {
                            strXML.Append("<key>" + CategoryName + "</key>");
                            if (CategoryName != "Scroller")
                            {
                                strXMLForSingleCategory = GetXmlStringForSingleCategory(CategoryName, allKeysOfCategory);
                                if (strXMLForSingleCategory == "")
                                {
                                    strXML.Append("<array />");
                                }
                                else
                                {
                                    strXML.Append("<array>" + strXMLForSingleCategory + "</array>");
                                }
                            }
                            else
                            {
                                //CategoryName Scroller
                                strXML.Append("<array>");
                                foreach (var objKey in objDict.Value)
                                {
                                    strXML.Append("<dict>");
                                    strXML.Append("<key>ExpectedValues</key>");
                                    strXML.Append("<string>" + objKey.Value["ExpectedValues"] + "</string>");
                                    //strXML.Append("<identifier>" + objKey.Value["identifier"] + "</identifier>");
                                    strXML.Append("<key>identifier</key>");
                                    strXML.Append("<string>" + objKey.Key + "</string>");
                                    strXML.Append("<key>type</key>");
                                    strXML.Append("<string>GENERAL</string>");
                                    strXML.Append("</dict>");
                                }
                                strXML.Append("^^^^");
                                strXML.Append("</array>");
                            }

                            flag = true;
                        }
                        else
                        {
                            if (CategoryName != "Scroller")
                            {
                                strXMLForSingleCategory = GetXmlStringForSingleCategory(CategoryName, allKeysOfCategory);
                                if (strXMLForSingleCategory == "")
                                {
                                    strXML.Append("");
                                }
                                else
                                {
                                    strXML.Replace("^^^^", strXMLForSingleCategory);
                                }
                            }
                            else
                            {
                                strScroller.Clear();
                                //CategoryName Scroller
                                foreach (var objKey in objDict.Value)
                                {
                                    strScroller.Append("<dict>");
                                    strScroller.Append("<key>ExpectedValues</key><string>" + objKey.Value["ExpectedValues"] + "</string>");
                                    strScroller.Append("<key>identifier</key><string>" + objKey.Key + "</string>");
                                    strScroller.Append("<key>type</key><string>GENERAL</string>");
                                    strScroller.Append("</dict>");
                                }

                                strXML.Replace("^^^^", strScroller.ToString());

                            }
                        }
                    }
                }
                strXML.Append("</dict></plist>");

            }
            catch (Exception ex)
            {
                throw ex;
            }


            if (File.Exists(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml"))
            {
                File.Delete(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
            }
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(strXML.ToString());
            }
            catch (Exception ex)
            {
                //logger.Fatal("Error while saving the XML file (Update Aircraft) " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
            }
            doc.Save(ConfigurationReader.EngineConfigXMLFilePath + @"\" + aircraftId.ToString() + ".xml");
        }


        public string GetXmlStringForSingleCategory(string categoryName, List<string> allKeysInCategory)
        {
            StringBuilder strXMl = new StringBuilder();
            PList JPISettingBaseFilePlist = new PList(ConfigurationReader.EngineConfigXMLFilePath + @"\EnginemasterFiles\JPI_Setting.xml");

            var IndicatorList = JPISettingBaseFilePlist[categoryName] as Dictionary<string, dynamic>;
            var showLable = "";
            //var IndicatorList = JPISettingBaseFilePlist[categoryName] as List<object>;
            List<object> dictGetMinMaxValue = new List<object>();
            if (IndicatorList.Count > 0)
            {
                foreach (Dictionary<string, dynamic> p in IndicatorList.Values)
                {
                    if (!allKeysInCategory.Contains(p["identifier"]))
                    {
                        continue;
                    }
                    strXMl.Append("<dict>");

                    if (p.ContainsKey("audioForHigh"))
                    {
                        strXMl.Append("<key>audioForHigh</key><string>" + p["audioForHigh"] + "</string>");
                    }
                    if (p.ContainsKey("audioForLow"))
                    {
                        strXMl.Append("<key>audioForLow</key><string>" + p["audioForLow"] + "</string>");
                    }
                    if (p.ContainsKey("decimalPlaces"))
                    {
                        strXMl.Append("<key>decimalPlaces</key><string>" + p["decimalPlaces"] + "</string>");
                    }
                    if (p.ContainsKey("identifier"))
                    {
                        strXMl.Append("<key>identifier</key><string>" + p["identifier"] + "</string>");
                    }
                    if (p.ContainsKey("max"))
                    {
                        strXMl.Append("<key>max</key><real>" + p["max"] + "</real>");
                    }
                    if (p.ContainsKey("min"))
                    {
                        strXMl.Append("<key>min</key><real>" + p["min"] + "</real>");
                    }
                    if (p.ContainsKey("minorTickIncrement"))
                    {
                        strXMl.Append("<key>minorTickIncrement</key><real>" + p["minorTickIncrement"] + "</real>");
                    }
                    if (p.ContainsKey("tickIncrement"))
                    {
                        strXMl.Append("<key>tickIncrement</key><real>" + p["tickIncrement"] + "</real>");
                    }
                    if (p.ContainsKey("title"))
                    {
                        strXMl.Append("<key>title</key><string>" + p["title"] + "</string>");
                    }
                    if (p.ContainsKey("type"))
                    {
                        strXMl.Append("<key>type</key><string>" + p["type"] + "</string>");
                    }
                    if (p.ContainsKey("unit"))
                    {
                        strXMl.Append("<key>unit</key><string>" + p["unit"] + "</string>");
                    }
                    if (p.ContainsKey("showLabel"))
                    {
                        showLable = Convert.ToString(p["showLabel"]);
                        strXMl.Append("<key>showLabel</key><" + showLable.ToLower() + " />");
                    }
                    if (p.ContainsKey("showTickFromMinLimit"))
                    {
                        strXMl.Append("<key>showTickFromMinLimit</key><real>" + p["showTickFromMinLimit"] + "</real>");
                    }
                    if (p.ContainsKey("showTickToMaxLimit"))
                    {
                        strXMl.Append("<key>showTickToMaxLimit</key><string>" + p["showTickToMaxLimit"] + "</string>");
                    }

                    strXMl.Append("<key>separator</key>");
                    dictGetMinMaxValue = p["separator"] as List<object>;
                    strXMl.Append("<array>");
                    for (int i = 0; i < dictGetMinMaxValue.Count; i++)
                    {
                        strXMl.Append("<dict>");
                        arrMainMaxValue = dictGetMinMaxValue[i] as Dictionary<string, object>;
                        strXMl.Append("<key>color</key><string>" + arrMainMaxValue["color"] + "</string>");
                        strXMl.Append("<key>max</key><real>" + arrMainMaxValue["max"] + "</real>");
                        strXMl.Append("<key>min</key><real>" + arrMainMaxValue["min"] + "</real>");

                        if (arrMainMaxValue.ContainsKey("showLabel"))
                        {
                            showLable = arrMainMaxValue["showLabel"].ToString();
                            strXMl.Append("<key>showLabel</key><" + showLable.ToLower() + " />");
                        }
                        else
                        {
                            strXMl.Append("<key>showLabel</key><true /></string>");
                        }

                        strXMl.Append("</dict>");
                    }
                    strXMl.Append("</array>");
                    strXMl.Append("</dict>");

                    if (categoryName == "ExtraGuage")
                    {
                        strXMl.Append("^^^^");  // Special character append because ExtraGuage is shown in the scroller
                    }
                }
            }
            return strXMl.ToString();
        }

        public string GetColorCode()
        {
            return colorCode;
        }

        public void SetColorCode(string code)
        {
            colorCode = code;
        }

        public string GetFullNameOfIdentifier(string key)
        {
            string fullName = string.Empty;
            switch (key)
            {
                case "FF": { fullName = "Fuel Flow"; break; }
                case "BAT": { fullName = "Battery Voltage"; break; }
                case "AMP": { fullName = "Ammeter"; break; }
                case "OIL-P": { fullName = "Oil Pressure"; break; }
                case "OIL-T": { fullName = "Oil Temperature"; break; }
                case "FQL": { fullName = "Fuel Quantity Left"; break; }
                case "FQR": { fullName = "Fuel Quantity Right"; break; }
                case "FP": { fullName = "Fuel Pressure"; break; }
                case "CHT": { fullName = "Cylender Head Temperature"; break; }
                case "EGT": { fullName = "Exhaust Gas Temperature"; break; }
                case "TIT": { fullName = "Turbine Inlet Temperature"; break; }
                case "RPM": { fullName = "Tachometer (RPM)"; break; }
                case "MAP": { fullName = "Manifold Pressure (In. Hg)"; break; }
                default: { fullName = key; break; }


            }

            return fullName;
        }


    }
}
