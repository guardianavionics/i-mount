﻿using System.Data.Entity;
using System.Globalization;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Web;
using GA.DataTransfer.Classes_for_Services;
using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;
using GA.Common;
using GA.DataLayer;
using System.Net;
//using NLog;
using SharpKml.Dom;
using Ionic.Zip;
using System.Data.SQLite;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data.Entity;

namespace GA.ApplicationLayer
{
    public class AdminHelper
    {
        private SQLiteConnection sqlite;
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public System.Text.StringBuilder sbPlates = new System.Text.StringBuilder();
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];


        public List<UserInfo> GetActiveUser()
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<UserInfo>(); // AdminHome { UserList = new List<UserInfo>(), ManufacturerUserList = new List<UserInfo>(), InActiveUserList = new List<UserInfo>() };
            response = context.Profiles
                                         .Where(a => !a.Deleted && a.Id != 1 && !a.IsBlocked)
                                         .Select(b => new UserInfo
                                         {
                                             ProfileId = b.Id,
                                             FirstName = b.UserDetail.FirstName ?? string.Empty,
                                             LastName = b.UserDetail.LastName ?? string.Empty,
                                             //IsBlock = b.IsBlocked,
                                             EmailId = b.EmailId
                                         })
                                         .ToList();
            return response;
        }

        public List<UserInfo> GetInActiveUser()
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<UserInfo>(); // AdminHome { UserList = new List<UserInfo>(), ManufacturerUserList = new List<UserInfo>() };
            response = context.Profiles
                                         .Where(a => !a.Deleted && a.Id != 1 && a.IsBlocked)
                                         .Select(b => new UserInfo
                                         {
                                             ProfileId = b.Id,
                                             FirstName = b.UserDetail.FirstName ?? string.Empty,
                                             LastName = b.UserDetail.LastName ?? string.Empty,
                                             //IsBlock = b.IsBlocked,
                                             EmailId = b.EmailId
                                         })
                                         .ToList();
            return response;
        }


        public AdminHome GetActiveInActiveUsers()
        {
            var response = new AdminHome { UserList = new List<UserInfo>(), InActiveUserList = new List<UserInfo>() };

            response.UserList = GetActiveUser();
            response.InActiveUserList = GetInActiveUser();
            return response;
        }

        /// <summary>
        /// gets the list of all the user.
        /// </summary>
        /// <returns></returns>
        public AdminHome GetUserList(bool isBlocked)
        {

            var context = new GuardianAvionicsEntities();
            var response = new AdminHome { UserList = new List<UserInfo>(), ManufacturerUserList = new List<UserInfo>() };
            try
            {
                response.UserList = context.Profiles
                                           .Where(a => !a.Deleted && a.Id != 1 && a.IsBlocked == isBlocked)
                                           .Select(b => new UserInfo
                                           {
                                               ProfileId = b.Id,
                                               FirstName = b.UserDetail.FirstName ?? string.Empty,
                                               LastName = b.UserDetail.LastName ?? string.Empty,
                                               //IsBlock = b.IsBlocked,
                                               EmailId = b.EmailId
                                           })
                                           .ToList();

                response.ManufacturerUserList = context.MappingAircraftManufacturerAndUsers.Include(i => i.Profile).Include(i => i.AircraftComponentManufacturer)
                                           .Where(a => !a.Profile.Deleted && a.Profile.Id != 1 && a.Profile.IsBlocked == isBlocked && a.Profile.UserMaster.UserType == "Manufacturer")
                                           .Select(b => new UserInfo
                                           {
                                               ProfileId = b.ProfileId,
                                               FirstName = b.Profile.UserDetail.FirstName ?? string.Empty,
                                               LastName = b.Profile.UserDetail.LastName ?? string.Empty,
                                               ComponentManufacturer = b.AircraftComponentManufacturer.Name,
                                               EmailId = b.Profile.EmailId
                                           })
                                           .ToList();
                context.Dispose();
                context = null;
                return response;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);

                return new AdminHome();
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public bool IsManufacturerExists(string manufacturerName ,int id)
        {
            var context = new GuardianAvionicsEntities();
            AircraftComponentManufacturer manufacturer = new AircraftComponentManufacturer();
            if (id == 0)
            {
                manufacturer = context.AircraftComponentManufacturers.FirstOrDefault(f => f.Name == manufacturerName && !f.Deleted);
            }
            else
            {
                manufacturer = context.AircraftComponentManufacturers.FirstOrDefault(f => f.Name == manufacturerName && !f.Deleted &&f.Id==id);
            }
            var response = (manufacturer == null) ? false : true;
            return response;
        }

        public List<UserInfo> GetmanufacturerUserList()
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<UserInfo>();
            try
            {
                response = context.MappingAircraftManufacturerAndUsers.Include(i => i.Profile).Include(i => i.AircraftComponentManufacturer)
                                           .Where(a => !a.Profile.Deleted && a.Profile.Id != 1 && a.Profile.IsBlocked == false && a.Profile.UserMaster.UserType == "Manufacturer")
                                           .Select(b => new UserInfo
                                           {
                                               ProfileId = b.ProfileId,
                                               FirstName = b.Profile.UserDetail.FirstName ?? string.Empty,
                                               LastName = b.Profile.UserDetail.LastName ?? string.Empty,
                                               ComponentManufacturer = b.AircraftComponentManufacturer.Name,
                                               EmailId = b.Profile.EmailId
                                           })
                                           .ToList();
                context.Dispose();
                context = null;
                return response;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return new List<UserInfo>();
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public List<UserInfo> BlockUnblockUser(int profileId, string isBlocked)
        {
            var context = new GuardianAvionicsEntities();
            var response = new List<UserInfo>();
            try
            {
                var profile = context.Profiles.FirstOrDefault(pr => pr.Id == profileId);
                profile.IsBlocked = Convert.ToBoolean(isBlocked);
                context.SaveChanges();
                if (isBlocked == "true")
                {
                    response = GetActiveUser();
                }
                else
                {
                    response = GetInActiveUser();
                }
                return response;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return new List<UserInfo>();
            }
            finally
            {
                context.Dispose();
            }
        }

        public bool SendPushNotificationToAllUser(string message)
        {
            var context = new GuardianAvionicsEntities();
            bool result = true;
            try
            {
                var profileIds = context.Profiles.Where(p => !p.Deleted && !p.IsBlocked).Select(s => s.Id).ToArray();
                var objPushNotification = context.PushNotifications.Where(p => profileIds.Contains(p.ProfileId) && p.IsValid && !string.IsNullOrEmpty(p.TokenId)).Select(p => p.TokenId).Distinct().ToArray();
                if (objPushNotification.Any())
                {
                    //PushNotificationIPhone obj = new PushNotificationIPhone();
                    //obj.Push(objPushNotification, message);
                    IphonePushNotification objPushMessage = new IphonePushNotification();
                    foreach (var id in objPushNotification)
                    {
                        objPushMessage.PushToiPhone(id, message, "");
                    }
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                ////logger.Fatal("Exception ", ex);
                return false;
            }
        }


        public bool SendPushNotificationToUsers(string usernames, string message)
        {
            var arrUsernames = usernames.Split(',').ToList();
            arrUsernames = arrUsernames.Where(f => f.Length > 0).ToList();
            arrUsernames.ForEach(f => f = f.Substring(1, f.Length - 1));
            var context = new GuardianAvionicsEntities();
            bool result = true;
            try
            {
                var profileIds = context.UserDetails.Where(u => arrUsernames.Contains(u.FirstName + " " + u.LastName)).Select(s => s.ProfileId).ToArray();
                var objPushNotification = context.PushNotifications.Where(p => profileIds.Contains(p.ProfileId) && p.IsValid && !string.IsNullOrEmpty(p.TokenId)).Select(p => p.TokenId).ToArray();
                if (objPushNotification.Any())
                {
                    IphonePushNotification obj = new IphonePushNotification();
                    //obj.Push(objPushNotification, message);

                    foreach (var tokenId in objPushNotification)
                    {
                        obj.PushToiPhone(tokenId, message, "SendByAdminFromWeb");
                    }
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                ////logger.Fatal("Exception ", ex);
                return false;
            }
        }


        public string[] SearchUser(string keyword, bool blockstatus, string searchBy, string userType)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                string[] userlist = null;
                if (searchBy == "First Name")
                {
                    userlist = context.Profiles
                        .Where(p => (p.UserDetail.FirstName.Contains(keyword)) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                        .Select(a => a.UserDetail.FirstName).Take(20)
                        .ToArray();
                }
                else if (searchBy == "Last Name")
                {
                    userlist = context.Profiles
                        .Where(p => (p.UserDetail.LastName.Contains(keyword)) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                        .Select(a => a.UserDetail.LastName).Take(20)
                        .ToArray();
                }
                else
                {
                    userlist = context.Profiles.
                        Where(p => p.EmailId.Contains(keyword) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                       .Select(a => a.EmailId).Take(20)
                       .ToArray();
                }
                return userlist;
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                ////logger.Fatal("Exception ", ex);
                return new string[1];
            }
            finally
            {
                context.Dispose();
            }
        }

        public List<UserInfo> DeleteManufactureruser(int id)
        {
            var response = new List<UserInfo>();
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == id);

            context.SaveChanges();
            var userDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == id);
            var mappingCompManuAndUser = context.MappingAircraftManufacturerAndUsers.Where(s => s.ProfileId == id);
            var preference = context.Preferences.FirstOrDefault(f => f.ProfileId == id);
            if (profile.IsSubscriptionVisible ==true)
            {
                profile.CurrentSubscriptionId = null;
                var mapSubscriptionAndUSers = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.ProfileId == id);
                context.MapSubscriptionAndUSers.Remove(mapSubscriptionAndUSers);
            }
           
            context.Preferences.Remove(preference);
            context.MappingAircraftManufacturerAndUsers.RemoveRange(mappingCompManuAndUser);
            context.UserDetails.Remove(userDetails);
           
            context.Profiles.Remove(profile);

            context.SaveChanges();

            return GetmanufacturerUserList();
        }

        public AdminHome GetUserDetailsByKeyWord(string keyword, bool blockstatus, string searchBy, string userType)
        {
            var response = new AdminHome { UserList = new List<UserInfo>(), ManufacturerUserList = new List<UserInfo>() };
            var context = new GuardianAvionicsEntities();
            try
            {
                var responsetemp = new List<UserInfo>();
                var userlist = (searchBy == "UserName") ? (context.Profiles.Where(p => (p.UserDetail.FirstName.Contains(keyword) || p.UserDetail.LastName.Contains(keyword)) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType).Select(a => (a.UserDetail.FirstName + " " + a.UserDetail.LastName)).ToArray()) : context.Profiles.Where(p => p.EmailId.Contains(keyword) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType).Select(a => a.EmailId).ToArray();
                if (searchBy == "First Name")
                {
                    var arr = keyword.Split(' ');
                    responsetemp = context.Profiles
                                             .Where(p => !p.Deleted && p.Id != 1 && (p.UserDetail.FirstName).Contains(keyword) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                                             .Select(b => new UserInfo
                                             {
                                                 ProfileId = b.Id,
                                                 FirstName = b.UserDetail.FirstName ?? string.Empty,
                                                 LastName = b.UserDetail.LastName ?? string.Empty,
                                                 EmailId = b.EmailId
                                             })
                                             .ToList();
                }
                else if (searchBy == "Last Name")
                {
                    responsetemp = context.Profiles
                                            .Where(p => !p.Deleted && p.Id != 1 && (p.UserDetail.LastName).Contains(keyword) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                                            .Select(b => new UserInfo
                                            {
                                                ProfileId = b.Id,
                                                FirstName = b.UserDetail.FirstName ?? string.Empty,
                                                LastName = b.UserDetail.LastName ?? string.Empty,
                                                EmailId = b.EmailId
                                            })
                                            .ToList();
                }
                else
                {
                    responsetemp = context.Profiles
                                           .Where(p => !p.Deleted && p.Id != 1 && p.EmailId.Contains(keyword) && p.IsBlocked == blockstatus && p.UserMaster.UserType == userType)
                                           .Select(b => new UserInfo
                                           {
                                               ProfileId = b.Id,
                                               FirstName = b.UserDetail.FirstName ?? string.Empty,
                                               LastName = b.UserDetail.LastName ?? string.Empty,
                                               EmailId = b.EmailId
                                           })
                                           .ToList();
                }
                if (userType == "Manufacturer")
                {
                    response.ManufacturerUserList = responsetemp;
                }
                else
                {
                    response.UserList = responsetemp;
                }
                return response;
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                ////logger.Fatal("Exception ", ex);
                return new AdminHome();
            }
        }

        public bool IsMapFileExists(string mapFileName)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var mapFileNamelist = context.MapFiles.Where(m => m.Name == mapFileName && !m.Deleted).Select(s => s.Name).ToList();
                return (mapFileNamelist.Any() ? mapFileNamelist.Contains(mapFileName) : false);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return false;
            }
            finally
            {
                context.Dispose();
            }
        }

        public void DeleteMapFile(System.Web.HttpPostedFileBase file)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var fileName = file.FileName;
                var mapFile = context.MapFiles.Where(dc => dc.Name == fileName && !dc.Deleted);

                if (mapFile.Any())
                {
                    foreach (var d in mapFile)
                    {
                        // delete document
                        d.Deleted = true;

                        var filePath = ConfigurationReader.MapFileUrl + @"\" + d.Name;
                        var ZipfilePath = ConfigurationReader.MapFileUrl + @"\" + d.Url;
                        if (File.Exists(ZipfilePath))
                            File.Delete(ZipfilePath);
                        if (File.Exists(filePath))
                            File.Delete(filePath);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        public bool SaveMapFile(MapFile mapFile)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    context.MapFiles.Add(mapFile);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return false;
            }
        }
        //public void SaveMapFile(System.Web.HttpPostedFileBase file)
        //{
        //    var context = new GuardianAvionicsEntities();
        //    try
        //    {
        //        var fileName = file.FileName;
        //        string uniqueFileName = DateTime.Now.Ticks.ToString();
        //        string url = "";
        //        string fileExt = file.FileName.Split('.')[1];
        //        string path = "";
        //        if (fileExt.ToUpper() == "ZIP" || fileExt.ToUpper() == "RAR")
        //        {
        //            path = ConfigurationReader.MapFileUrl + @"\" + uniqueFileName + "." + fileExt;
        //            file.SaveAs(path);
        //        }
        //        else
        //        {
        //            path = ConfigurationReader.MapFileUrl + @"\" + fileName;
        //            file.SaveAs(path);
        //        }


        //        if (fileExt.ToUpper() != "ZIP" || fileExt.ToUpper() != "RAR")
        //        {
        //            url = uniqueFileName + ".zip";
        //            string zipPath = ConfigurationReader.MapFileUrl + @"\" + url;

        //            ZipFile objZip = new ZipFile();
        //            objZip.AddFile(path, "");
        //            objZip.Save(zipPath);
        //            File.Delete(path);
        //        }

        //        var mapFile = new MapFile
        //        {
        //            Deleted = false,
        //            LastUpdated = DateTime.UtcNow,
        //            Url = url,
        //            FileSize = (Convert.ToInt32(file.ContentLength) / 1024) / 1024,
        //            Name = fileName
        //        };
        //        context.MapFiles.Add(mapFile);
        //        context.SaveChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        //ExceptionHandler.ReportError(e);
        //        //////logger.Fatal("Exception ", e);
        //    }
        //    finally
        //    {
        //        context.Dispose();
        //    }
        //}

        public bool SavePassengerProMapFile(PassengerProMapFile passengerProMapFile)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    context.PassengerProMapFiles.Add(passengerProMapFile);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return false;
            }
        }

        public AviationChartModel GetMapFiles()
        {
            var aviationChartModel = new AviationChartModel();

            var context = new GuardianAvionicsEntities();
            try
            {
                aviationChartModel.MapFileList = context.MapFiles.Where(m => !m.Deleted).ToList();

                aviationChartModel.PassengerMapFileList = context.PassengerProMapFiles.Where(m => !m.Deleted).Select(a => new PassengerProMapFileModel
                {
                    FileName = a.Name,
                    FileSizeInMB = a.FileSize,
                    Region = a.PassengerProRegionMaster.Region,
                    RegionId = a.PassengerProRegionMasterId,
                    ZoomLevel = a.ZoomLevel,
                    URl = ConfigurationReader.MapFileUrl + @"/" + a.Name,
                    Title = a.Title,
                    Id = a.Id
                }).ToList();

                aviationChartModel.chartListByStateModel = new List<ChartListByStateModel>();
                //aviationChartModel.chartListByStateModel

                var result = (from p in context.ChartDetails
                              group p by p.State.StateName into g
                              select new
                              {
                                  StateName = g.Key,
                                  ChartDetailModelList = g.Select(s => new
                                  {

                                      //Bounds = s.Bounds,
                                      ChartType = s.ChartTypeMaster.ChartType,
                                      //Copyright = s.Copyright,
                                      CreateDate = (s.CreateDate),
                                      Description = s.Description,
                                      ExpDate = (s.ExpDate),
                                      FileName = s.FileName,
                                      FileSize = s.FileSize,
                                      Format = s.Format,
                                      MaxZoom = s.MaxZoom,
                                      MinZoom = s.MinZoom,
                                      Id = s.Id.ToString(),
                                      Name = s.Name,
                                      Provider = s.Provider,
                                      Type = s.Type,
                                      Version = s.Version
                                  }).ToList()
                              }).ToList();

                List<ChartDetailModel> ChartDetailModelList;
                foreach (var obj in result)
                {
                    ChartDetailModelList = new List<ChartDetailModel>();
                    obj.ChartDetailModelList.ForEach(s => ChartDetailModelList.Add(new ChartDetailModel
                    {
                        ChartType = s.ChartType,
                        //Copyright = s.Copyright,
                        CreateDate = Misc.GetStringOnlyDateFormat2(s.CreateDate),
                        Description = s.Description,
                        ExpDate = Misc.GetStringOnlyDateFormat2(s.ExpDate),
                        FileName = s.FileName,
                        FileSize = s.FileSize,
                        Format = s.Format,
                        MaxZoom = s.MaxZoom,
                        MinZoom = s.MinZoom,
                        Id = s.Id.ToString(),
                        Name = s.Name,
                        Provider = s.Provider,
                        Type = s.Type,
                        Version = s.Version
                    }));

                    aviationChartModel.chartListByStateModel.Add(new ChartListByStateModel
                    {
                        StateName = obj.StateName,
                        ChartDetailModelList = ChartDetailModelList
                    });
                }

                return aviationChartModel;
            }
            catch (Exception e)
            {
                ////////logger.Fatal("Exception ", e);
                return aviationChartModel;
            }
            finally
            {
                context.Dispose();
            }
        }


        public List<ChartListByStateModel> GetChartDetailsForWeb()
        {
            var chartListByStateModel = new List<ChartListByStateModel>();
            //aviationChartModel.chartListByStateModel
            var context = new GuardianAvionicsEntities();
            var result = (from p in context.ChartDetails
                          group p by p.State.StateName into g
                          select new
                          {
                              StateName = g.Key,
                              ChartDetailModelList = g.Select(s => new
                              {

                                  //Bounds = s.Bounds,
                                  ChartType = s.ChartTypeMaster.ChartType,
                                  //Copyright = s.Copyright,
                                  CreateDate = (s.CreateDate),
                                  Description = s.Description,
                                  ExpDate = (s.ExpDate),
                                  FileName = s.FileName,
                                  FileSize = s.FileSize,
                                  Format = s.Format,
                                  MaxZoom = s.MaxZoom,
                                  MinZoom = s.MinZoom,
                                  Id = s.Id.ToString(),
                                  Name = s.Name,
                                  Provider = s.Provider,
                                  Type = s.Type,
                                  Version = s.Version
                              }).ToList()
                          }).ToList();

            List<ChartDetailModel> ChartDetailModelList;
            foreach (var obj in result)
            {
                ChartDetailModelList = new List<ChartDetailModel>();
                obj.ChartDetailModelList.ForEach(s => ChartDetailModelList.Add(new ChartDetailModel
                {
                    ChartType = s.ChartType,
                    //Copyright = s.Copyright,
                    CreateDate = Misc.GetStringOnlyDateFormat2(s.CreateDate),
                    Description = s.Description,
                    ExpDate = Misc.GetStringOnlyDateFormat2(s.ExpDate),
                    FileName = s.FileName,
                    FileSize = s.FileSize,
                    Format = s.Format,
                    MaxZoom = s.MaxZoom,
                    MinZoom = s.MinZoom,
                    Id = s.Id.ToString(),
                    Name = s.Name,
                    Provider = s.Provider,
                    Type = s.Type,
                    Version = s.Version
                }));

                chartListByStateModel.Add(new ChartListByStateModel
                {
                    StateName = obj.StateName,
                    ChartDetailModelList = ChartDetailModelList
                });
            }

            return chartListByStateModel;
        }


        public List<MapFile> GetMapFileList()
        {
            var context = new GuardianAvionicsEntities();
            var mapFileList = context.MapFiles.Where(m => !m.Deleted).ToList();
            if (mapFileList.Count <= 0)
            {
                return new List<MapFile>();
            }
            return mapFileList;
        }

        public List<PassengerProMapFileModel> GetPassengerProMapFileList()
        {
            var context = new GuardianAvionicsEntities();
            var passengerProMapFiles = context.PassengerProMapFiles.Where(m => !m.Deleted).Select(a => new PassengerProMapFileModel
            {
                FileName = a.Name,
                FileSizeInMB = a.FileSize,
                Region = a.PassengerProRegionMaster.Region,
                RegionId = a.PassengerProRegionMasterId,
                ZoomLevel = a.ZoomLevel,
                URl = ConfigurationReader.MapFileUrl + @"/" + a.Name,
                Title = a.Title,
                Id = a.Id
            }).ToList();
            if (passengerProMapFiles.Count <= 0)
            {
                return new List<PassengerProMapFileModel>();
            }
            return passengerProMapFiles;
        }

        public bool DeleteMapFileById(int id)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var mapFile = context.MapFiles.FirstOrDefault(dc => dc.Id == id);

                if (mapFile != default(MapFile) && !string.IsNullOrEmpty(mapFile.Url))
                {
                    // delete document
                    mapFile.Deleted = true;
                    context.SaveChanges();

                    var filePath = ConfigurationReader.MapFileUrl + @"\" + mapFile.Url;
                    if (File.Exists(filePath))
                        File.Delete(filePath);

                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return false;
            }
            finally
            {
                context.Dispose();
            }
        }


        public bool DeletePassengerProMapFileById(int id)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var passengerProMapFiles = context.PassengerProMapFiles.FirstOrDefault(p => p.Id == id);

                if (passengerProMapFiles != null)
                {
                    // delete document
                    passengerProMapFiles.Deleted = true;
                    context.SaveChanges();

                    var filePath = ConfigurationReader.MapFileUrl + @"\" + passengerProMapFiles.Name;
                    if (File.Exists(filePath))
                        File.Delete(filePath);

                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                ////////logger.Fatal("Exception ", e);
                return false;
            }
            finally
            {
                context.Dispose();
            }
        }

        public List<ListItems> GetRegionList()
        {
            var context = new GuardianAvionicsEntities();
            var regionList = context.PassengerProRegionMasters.Where(p => !p.Deleted)
                    .Select(s => new ListItems() { text = s.Region, value = s.Id })
                    .ToList();


            if (regionList.Count <= 0)
            {
                return new List<ListItems>();
            }
            return regionList;
        }

        public string GetUserNameAndPasswordByIdForEncryption(int id)
        {
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == id);
            if (profile != null)
            {
                return ((new Misc().Decrypt(profile.Password)) + (profile.EmailId).ToLower());
            }
            return "";


        }

        public string GetKeyForEncryption(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                if (profile != null)
                {
                    return profile.RecordId;
                }
            }
            catch(Exception ex)
            {

            }
            return "";
        }

        #region Aero Unit

        /// <summary>
        /// Gets all the areo units.
        /// </summary>
        /// <returns></returns>
        public AeroUnitsModel GetAeroUnitsDetails()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var aeroUnits = context.AeroUnitMasters.Where(a => !a.Deleted).Include(a => a.AircraftProfile).ToList();

                if (aeroUnits.Count > 0)
                {
                    var aeroUnitsModel = new AeroUnitsModel
                    {
                        UnlinkedAreoUnits = aeroUnits.Where(a => a.AircraftId == null).Select(a => new UnlinkedAreoUnit
                        {
                            Date = String.Format("{0:MMM dd yyyy}", a.CreateDate),// a.CreateDate.ToShortDateString(),
                            UnitSerialNumber = a.AeroUnitNo
                        }).ToList(),
                        LinkedAeroUnits = aeroUnits.Where(a => a.AircraftId != null && !a.Blocked).Select(a => new LinkedAeroUnit
                        {
                            Id = a.Id,
                            AeroUnitNumber = a.AeroUnitNo,
                            AircraftId = a.AircraftId,
                            AircraftTelNumber = a.AircraftProfile.Registration,
                            AircraftSerialNo = a.AircraftProfile.AircraftSerialNo,
                            DateCreated = String.Format("{0:MMM dd yyyy}", a.CreateDate)// a.CreateDate.ToShortDateString()
                        }).ToList(),
                        PreviouslyLinkedAeroUnits = aeroUnits.Where(a => a.AircraftId != null && a.Blocked).Select(a => new LinkedAeroUnit
                        {
                            Id = a.Id,
                            AeroUnitNumber = a.AeroUnitNo,
                            AircraftId = a.AircraftId,
                            AircraftTelNumber = a.AircraftProfile.Registration,
                            DateCreated = String.Format("{0:MMM dd yyyy}", a.CreateDate) // a.CreateDate.ToShortDateString()
                        }).ToList()
                    };
                    return aeroUnitsModel;
                }

                return new AeroUnitsModel();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return new AeroUnitsModel();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Gets all the areo Units which are not linked to any aircraft. These may include areo units which did not pass the test or which are not linked
        /// </summary>
        /// <param name="keyword">keyword for search</param>
        /// <returns></returns>
        public List<UnlinkedAreoUnit> GetUnlinkedAeroUnits()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var unlinkedUnits = context.AeroUnitMasters.Where(a => a.AircraftId == null && !a.Deleted)
                                            .OrderBy(o => o.CreateDate)
                                            .AsEnumerable()
                                            .Select(s => new UnlinkedAreoUnit
                                            {
                                                Date = String.Format("{0:MMM dd yyyy}", s.CreateDate), // s.CreateDate.ToShortDateString(),
                                                UnitSerialNumber = s.AeroUnitNo,
                                            }).ToList();

                if (unlinkedUnits.Count > 0)
                    return unlinkedUnits;

                return new List<UnlinkedAreoUnit>();

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return new List<UnlinkedAreoUnit>();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Gets all the areo units which are linked to any aircraft.
        /// </summary>
        /// <param name="keyword">keyword for search</param>
        /// <returns></returns>
        public List<LinkedAeroUnit> GetLinkedAeroUnits()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var linkedAeroUnits = context.AeroUnitMasters.Where(a => a.AircraftId != null && !a.Deleted && !a.Blocked)
                                            .OrderBy(o => o.CreateDate)
                                            .AsEnumerable()
                                            .Select(s => new LinkedAeroUnit
                                            {
                                                Id = s.Id,
                                                AeroUnitNumber = s.AeroUnitNo,
                                                AircraftId = s.AircraftId,
                                                AircraftTelNumber = s.AircraftProfile.Registration,
                                                AircraftSerialNo = s.AircraftProfile.AircraftSerialNo,
                                                DateCreated = String.Format("{0:MMM dd yyyy}", s.CreateDate) // s.CreateDate.ToShortDateString()
                                            }).ToList();

                if (linkedAeroUnits.Count > 0)
                    return linkedAeroUnits;

                return new List<LinkedAeroUnit>();

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return new List<LinkedAeroUnit>();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Gets all the areo units which are Blocked.
        /// </summary>
        /// <param name="keyword">keyword for search</param>
        /// <returns></returns>
        public List<LinkedAeroUnit> GetPreviouslyLinkedAeroUnits()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var prevLinkedAeroUnits = context.AeroUnitMasters.Where(a => a.AircraftId != null && !a.Deleted && a.Blocked)
                                            .OrderBy(o => o.CreateDate)
                                            .AsEnumerable()
                                            .Select(s => new LinkedAeroUnit
                                            {
                                                Id = s.Id,
                                                AeroUnitNumber = s.AeroUnitNo,
                                                AircraftId = s.AircraftId,
                                                AircraftTelNumber = s.AircraftProfile.Registration,
                                                DateCreated = String.Format("{0:MMM dd yyyy}", s.CreateDate) // s.CreateDate.ToShortDateString()
                                            }).ToList();

                if (prevLinkedAeroUnits.Count > 0)
                    return prevLinkedAeroUnits;

                return new List<LinkedAeroUnit>();

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return new List<LinkedAeroUnit>();
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Gets all the areo units which are Blocked.
        /// </summary>
        /// <param name="keyword">keyword for search</param>
        /// <returns></returns>
        public List<LinkedAeroUnit> GetReplacedAeroUnits()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var replacedAeroUnits = context.AeroUnitMasters.Include(a => a.ReasonMaster).Where(a => a.AircraftId == null && !a.Deleted && a.Replaced)
                                            .OrderBy(o => o.CreateDate)
                                            .AsEnumerable();
                var list = new List<LinkedAeroUnit>();
                foreach (var unit in replacedAeroUnits)
                {
                    var replacedUnit = new LinkedAeroUnit
                    {
                        Id = unit.Id,
                        AeroUnitNumber = unit.AeroUnitNo,
                        ReplacedWithUnitNumber = unit.ReplacedWithUnitNo,
                        ReplacedReason = unit.ReasonMaster.ReasonText,
                        Comment = unit.ReplaceComment,
                        ReplacedDate = unit.ReplacedDate
                    };
                    var airCraft = context.AircraftProfiles.FirstOrDefault(ap => ap.AeroUnitNo == unit.ReplacedWithUnitNo);
                    if (airCraft != null)
                    {
                        replacedUnit.AircraftTelNumber = airCraft.Registration;
                    }
                    list.Add(replacedUnit);
                }
                return list;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return new List<LinkedAeroUnit>();
            }
            finally
            {
                context.Dispose();
            }
        }


        /// <summary>
        /// Get all unlinked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public string[] UnlinkedUnitList(string keyword)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                return context.AeroUnitMasters.Where(a => a.AeroUnitNo.Contains(keyword) && !a.Deleted && a.AircraftId == null)
                                                           .Select(s => s.AeroUnitNo)
                                                           .ToArray();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //gger.Fatal("Exception Keyword = " + keyword, e);
                return new string[1];
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Get all linked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public string[] LinkedUnitList(string keyword)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                return context.AeroUnitMasters.Where(a => a.AeroUnitNo.Contains(keyword) && !a.Deleted && !a.Blocked && a.AircraftId != null)
                                                           .Select(s => s.AeroUnitNo)
                                                           .ToArray();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //ogger.Fatal("Exception Keyword = " + keyword, e);
                return new string[1];
            }
            finally
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Get all Prevoiusly linked areo unit numbers for specified keyword and return the array of areo unit number.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public string[] PreviuoslyLinkedUnitList(string keyword)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                return context.AeroUnitMasters.Where(a => a.AeroUnitNo.Contains(keyword) && !a.Deleted && a.Blocked && a.AircraftId != null)
                                                           .Select(s => s.AeroUnitNo)
                                                           .ToArray();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                // //logger.Fatal("Exception Keyword = " + keyword, e);
                return new string[1];
            }
            finally
            {
                context.Dispose();
            }
        }


        ///// <summary>
        ///// Gets the areo Units which are not linked to any aircraft and matches the keyword. 
        ///// These may include areo units which did not pass the test or which are not linked
        ///// </summary>
        ///// <returns></returns>
        //public List<AreoUnitNotUsed> GetUnLinkedUnitDetailsByKeyWord(string keyword)
        //{
        //    var context = new GuardianAvionicsEntities();
        //    try
        //    {
        //        var unlinkedUnits = context.AeroUnitMasters.Where(a => a.AircraftId == null
        //                                                               && !a.Deleted
        //                                                               && a.AeroUnitNo.Contains(keyword))
        //            .OrderBy(o => o.CreateDate)
        //            .AsEnumerable()
        //            .Select(s => new AreoUnitNotUsed
        //            {
        //                Date = Misc.GetStringOfDate(s.CreateDate),
        //                UnitSerialNumber = s.AeroUnitNo,
        //            })
        //            .ToList();

        //        return unlinkedUnits.Any() ? unlinkedUnits : new List<AreoUnitNotUsed>();
        //    }
        //    catch (Exception e)
        //    {
        //        //ExceptionHandler.ReportError(e);
        //        //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(keyword), e);
        //        return new List<AreoUnitNotUsed>();
        //    }
        //    finally
        //    {
        //        context.Dispose();
        //    }
        //}

        /// <summary>
        /// Gets the ratio of all the unlinked units to linked units
        /// </summary>
        /// <returns></returns>
        public string GetLinkedUnlinkedUnitRatio()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var unLinked = context.AeroUnitMasters.Count(a => a.AircraftId == null && !a.Deleted);
                var allUnits = context.AeroUnitMasters.Count();

                return unLinked + "/" + allUnits;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return string.Empty;
            }
            finally
            {
                context.Dispose();
            }
        }

        public List<UnlinkedAreoUnit> DeleteAeroUnits(string aeroUnitNo)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.AeroUnitNo == aeroUnitNo && a.AircraftId == null);
                if (objAeroUnit != null)
                {
                    context.AeroUnitMasters.Remove(objAeroUnit);
                    context.SaveChanges();
                }

                return GetUnlinkedAeroUnits();

            }
            catch (Exception e)
            {
                //////logger.Fatal("Exception ", e);
                return new List<UnlinkedAreoUnit>();
            }
        }



        public List<LinkedAeroUnit> UnBlockedAeroUnit(string aeroUnitNo)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var objAeroUnit = context.AeroUnitMasters.FirstOrDefault(a => a.Blocked && a.AeroUnitNo == aeroUnitNo);
                if (objAeroUnit != null)
                {
                    objAeroUnit.Blocked = false;
                    context.SaveChanges();
                }
                return GetPreviouslyLinkedAeroUnits();
            }
            catch (Exception ex)
            {
                // //logger.Fatal("Exception " + ex);
                return new List<LinkedAeroUnit>();
            }
        }


        public List<LinkedAeroUnit> BlockAeroUnit(int id, string reason)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var aeroUnitMaster = context.AeroUnitMasters.Include(a => a.AircraftProfile).FirstOrDefault(a => a.Id == id);
                if (aeroUnitMaster != null)
                {
                    aeroUnitMaster.Blocked = true;
                    aeroUnitMaster.BlockedReason = reason;
                    if (aeroUnitMaster.AircraftProfile != null)
                    {
                        aeroUnitMaster.AircraftProfile.LastUpdated = DateTime.UtcNow;
                    }
                    context.SaveChanges();
                }
                return GetLinkedAeroUnits();
            }
            catch (Exception e)
            {
                //////logger.Fatal("Exception ", e);
                return new List<LinkedAeroUnit>();
            }
        }
        public object ChangeAeroUnit(int id, int reason, string newSrNo, string comment)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var aeroUnitMaster = context.AeroUnitMasters.Include(a => a.AircraftProfile).FirstOrDefault(a => a.Id == id);
                var newAeroUnitMaster = context.AeroUnitMasters.Include(a => a.AircraftProfile).FirstOrDefault(a => a.AeroUnitNo == newSrNo && a.Deleted==false);
                if (newAeroUnitMaster == null)
                {
                    return new { status = false, message = "Did not find new Aero Unit Number" };
                }
                else if (newAeroUnitMaster.Blocked)
                {
                    return new { status = false, message = "New Aero Unit Number is blocked." };
                }
                else if (newAeroUnitMaster.AircraftId != null)
                {
                    return new { status = false, message = "New Aero Unit Number is linked with " + newAeroUnitMaster.AircraftProfile.Registration };
                }
                if (aeroUnitMaster != null && newAeroUnitMaster != null)
                {
                    newAeroUnitMaster.AircraftId = aeroUnitMaster.AircraftId;

                    aeroUnitMaster.Replaced = true;
                    aeroUnitMaster.ReasonId= reason;
                    aeroUnitMaster.ReplaceComment = comment;
                    aeroUnitMaster.ReplacedWithUnitNo = newSrNo;
                    aeroUnitMaster.ReplacedDate = DateTime.Now;

                    aeroUnitMaster.AircraftId = null;
                    context.SaveChanges();
                }

                if (newAeroUnitMaster.AircraftProfile != null)
                {
                    newAeroUnitMaster.AircraftProfile.LastUpdated = DateTime.UtcNow;
                    newAeroUnitMaster.AircraftProfile.AeroUnitNo = newSrNo;
                    newAeroUnitMaster.AircraftProfile.LastTransactionId = "0";
                    newAeroUnitMaster.Replaced = false;
                    newAeroUnitMaster.ReasonId = null;
                    context.SaveChanges();
                }
                return new { status = true, message = "Aero Unit changed" };
            }
            catch (Exception e)
            {
                return new { status = true, message = "Aero Unit not changed, please contact system admin for the help." };
            }
        }
        #endregion Areo Unit


        #region ConfigurationSetting

        public List<ListItems> UpdateDeleteEngine(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objEngine = context.Engines.FirstOrDefault(e => e.Id == id);
                if (objEngine != null)
                {
                    if (isUpdate)
                    {
                        objEngine.Name = name;
                    }
                    else
                    {
                        objEngine.Deleted = true;
                    }

                    context.SaveChanges();

                    objResponse = context.Engines.Where(e => !e.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isEngineExists(string engineName,int id)
        {
            var response = false;
            var engine = new Engine();
            var context = new GuardianAvionicsEntities();
            if (id != 0)
            {
                 engine = context.Engines.FirstOrDefault(f => f.Name == engineName && !f.Deleted && f.Id != id);
            }
            else
            {
                 engine = context.Engines.FirstOrDefault(f => f.Name == engineName && !f.Deleted);
            }
            if (engine != null)
            {
                response = true;
            }
            return response;

        }

        public List<ListItems> AddEngineName(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                Engine objEngine = new Engine() { Name = name, Deleted = false, LastUpdated = DateTime.UtcNow };
                context.Engines.Add(objEngine);
                context.SaveChanges();

                objResponse = context.Engines.Where(e => !e.Deleted).Select(s => new ListItems
                {
                    text = s.Name,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public List<ListItems> UpdateDeleteRatingType(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objRating = context.RatingTypes.FirstOrDefault(e => e.Id == id);
                if (objRating != null)
                {
                    if (isUpdate)
                    {
                        objRating.Name = name;
                    }
                    else
                    {
                        objRating.Deleted = true;
                    }

                    context.SaveChanges();

                    objResponse = context.RatingTypes.Where(e => !e.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isRatingTypeExists(string ratingTypeName , int id)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();
            RatingType ratingType = new RatingType();
            if (id == 0)
            {
                ratingType = context.RatingTypes.FirstOrDefault(f => f.Name == ratingTypeName && !f.Deleted);
            }
            else
            {
                ratingType = context.RatingTypes.FirstOrDefault(f => f.Name == ratingTypeName && !f.Deleted&&f.Id!= id);
            }
            if (ratingType != null)
            {
                response = true;
            }
            return response;

        }


        public List<ListItems> AddRatingType(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                RatingType objRating = new RatingType() { Name = name, Deleted = false, LastUpdated = DateTime.UtcNow };
                context.RatingTypes.Add(objRating);
                context.SaveChanges();

                objResponse = context.RatingTypes.Where(e => !e.Deleted).Select(s => new ListItems
                {
                    text = s.Name,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public List<ListItems> UpdateDeletePropeller(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objPropeller = context.Propellers.FirstOrDefault(e => e.Id == id);
                if (objPropeller != null)
                {
                    if (isUpdate)
                    {
                        objPropeller.Name = name;
                    }
                    else
                    {
                        objPropeller.Deleted = true;
                    }
                    context.SaveChanges();
                    objResponse = context.Propellers.Where(e => !e.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isPropellerExists(string propellerName,int id)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();
            Propeller propeller = new Propeller();
            if(id==0)
            {
                 propeller = context.Propellers.FirstOrDefault(f => f.Name == propellerName && !f.Deleted);
            }
            else
            {
                propeller = context.Propellers.FirstOrDefault(f => f.Name == propellerName && !f.Deleted &&f.Id!=id);
            }
            if (propeller != null)
            {
                response = true;
            }
            return response;

        }


        public List<ListItems> AddPropeller(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                Propeller objPropeller = new Propeller() { Name = name, Deleted = false, LastUpdated = DateTime.UtcNow };
                context.Propellers.Add(objPropeller);
                context.SaveChanges();

                objResponse = context.Propellers.Where(e => !e.Deleted).Select(s => new ListItems
                {
                    text = s.Name,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public List<ListItems> UpdateDeleteMedCertClass(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objMedCertClass = context.MedicalCertClasses.FirstOrDefault(e => e.Id == id);
                if (objMedCertClass != null)
                {
                    if (isUpdate)
                    {
                        objMedCertClass.Name = name;
                    }
                    else
                    {
                        objMedCertClass.Deleted = true;

                        var objMedicalCertificate = context.MedicalCertificates.Where(m => m.MedicalCertClassId == id);
                        if (objMedicalCertificate != null)
                        {
                            objMedicalCertificate.ToList().ForEach(m => m.MedicalCertClassId = null);
                        }
                    }


                    context.SaveChanges();

                    objResponse = context.MedicalCertClasses.Where(e => !e.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isMedCertClassExists(string medCertClassName,int id)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();
            MedicalCertClass medicalCertClasses = new MedicalCertClass();
            if (id == 0)
            {
                medicalCertClasses = context.MedicalCertClasses.FirstOrDefault(f => f.Name == medCertClassName && !f.Deleted);
            }
            else
            {
                medicalCertClasses = context.MedicalCertClasses.FirstOrDefault(f => f.Name == medCertClassName && !f.Deleted&&f.Id==id);
            }
            if (medicalCertClasses != null)
            {
                response = true;
            }
            return response;

        }

        public List<ListItems> AddMedCertClass(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                MedicalCertClass objMedCertClass = new MedicalCertClass() { Name = name, Deleted = false, LastUpdated = DateTime.UtcNow };
                context.MedicalCertClasses.Add(objMedCertClass);
                context.SaveChanges();

                objResponse = context.MedicalCertClasses.Where(e => !e.Deleted).Select(s => new ListItems
                {
                    text = s.Name,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }



        public List<ListItems> UpdateDeleteAircraftManufacturer(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objAircraftManufacturer = context.AircraftManufacturers.FirstOrDefault(e => e.Id == id);
                if (objAircraftManufacturer != null)
                {
                    if (isUpdate)
                    {
                        objAircraftManufacturer.Name = name;
                    }
                    else
                    {
                        objAircraftManufacturer.Deleted = true;
                        objAircraftManufacturer.AircraftModelLists.ToList().ForEach(m => { m.Deleted = true; });
                        var objAircraftProfile = context.AircraftProfiles.Where(a => a.Make == id).ToList();
                        objAircraftProfile.ForEach(a => { a.Make = null; a.Model = null; });

                    }

                    context.SaveChanges();

                    objResponse = context.AircraftManufacturers.Where(e => !e.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isComponentManufacturerExists(string componentManufacturerName)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();

            var aircraftComponentManufacturers = context.AircraftComponentManufacturers.FirstOrDefault(f => f.Name == componentManufacturerName && !f.Deleted);
            if (aircraftComponentManufacturers != null)
            {
                response = true;
            }
            return response;

        }

        public List<ComponentManufacturerAndUser> UpdateDeleteComponentManufacturer(string name, int id, bool isUpdate)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ComponentManufacturerAndUser>();

            try
            {
                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.FirstOrDefault(e => e.Id == id);
                if (aircraftComponentManufacturers != null)
                {
                    if (isUpdate)
                    {
                        aircraftComponentManufacturers.Name = name;
                    }
                    else
                    {
                        aircraftComponentManufacturers.Deleted = true;
                        aircraftComponentManufacturers.AircraftComponentModels.ToList().ForEach(m => { m.Deleted = true; });

                        var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(a => a.ComponentManufacturerId == id).ToList();
                        aircraftComponentMakeAndModels.ForEach(a =>
                        {
                            a.ComponentManufacturerId = null;
                            a.ComponentModelId = null;
                        });
                    }

                    context.SaveChanges();

                    var ComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                    objResponse = ComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                    {
                        Id = s.Id,
                        Name = s.ManufacturerName,
                        EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                    }).ToList();

                    return objResponse;
                }
                return objResponse;
            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return objResponse;
            }
        }


        public List<ComponentManufacturerAndUser> DeleteUserFromComponentManufacturer(string userEmailId)
        {
            var response = new List<ComponentManufacturerAndUser>();


            var context = new GuardianAvionicsEntities();
            try
            {
                var profile = context.Profiles.FirstOrDefault(f => f.EmailId == userEmailId);
                if (profile != null)
                {
                    var userDetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == profile.Id);
                    var mappingCompManuAndUser = context.MappingAircraftManufacturerAndUsers.Where(s => s.ProfileId == profile.Id);
                    var preference = context.Preferences.FirstOrDefault(f => f.ProfileId == profile.Id);

                    context.Preferences.Remove(preference);
                    context.MappingAircraftManufacturerAndUsers.RemoveRange(mappingCompManuAndUser);
                    context.UserDetails.Remove(userDetails);
                    context.Profiles.Remove(profile);

                    context.SaveChanges();

                }

                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                response = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();

                return response;

            }
            catch (Exception ex)
            {
                //logger.Fatal("Exception while DeleteUserFromComponentManufacturer " + ex.Message + Environment.NewLine + Environment.NewLine + ex.InnerException);
                return response;
            }

        }

        public bool isAircraftManufacturerExists(string aircraftManufacturerName,int id)
        {
            var response = false;
            var aircraftManufacturers = new AircraftManufacturer();
            var context = new GuardianAvionicsEntities();
            if (id == 0)
            {
                 aircraftManufacturers = context.AircraftManufacturers.FirstOrDefault(f => f.Name == aircraftManufacturerName && !f.Deleted);
            }
            else
            {
                 aircraftManufacturers = context.AircraftManufacturers.FirstOrDefault(f => f.Name == aircraftManufacturerName && !f.Deleted&&f.Id!=id);
            }
            if (aircraftManufacturers != null)
            {
                response = true;
            }
            return response;

        }

        public bool isAircraftModelExists(string modelName, int makeId)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();

            var objResponse = context.AircraftModelLists.FirstOrDefault(e => !e.Deleted && e.AircraftManufacturerId == makeId && e.ModelName == modelName);
            if (objResponse != null)
            {
                response = true;
            }
            return response;

        }

        public List<ListItems> AddAircraftManufacturer(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                AircraftManufacturer objAircraftManufacturer = new AircraftManufacturer() { Name = name, Deleted = false, LastUpdated = DateTime.UtcNow };
                context.AircraftManufacturers.Add(objAircraftManufacturer);
                context.SaveChanges();

                objResponse = context.AircraftManufacturers.Where(e => !e.Deleted).Select(s => new ListItems
                {
                    text = s.Name,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        /// <summary>
        /// currently there is no use of temp parameter 
        /// </summary>
        /// <param name="manufacturerName"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        public int SetComponentManufacturer(string manufacturerName)
        {
            var context = new GuardianAvionicsEntities();

            try
            {
                AircraftComponentManufacturer aircraftComponentManufacturer = new AircraftComponentManufacturer() { Name = manufacturerName, Deleted = false, LastUpdateDate = DateTime.UtcNow, CreateDate = DateTime.UtcNow };
                context.AircraftComponentManufacturers.Add(aircraftComponentManufacturer);
                context.SaveChanges();
                return aircraftComponentManufacturer.Id;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return 0;
            }
        }

        public List<ComponentManufacturerAndUser> AddComponentManufacturer(string name)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ComponentManufacturerAndUser>();
            objResponse = new List<ComponentManufacturerAndUser>();
            try
            {
                AircraftComponentManufacturer aircraftComponentManufacturer = new AircraftComponentManufacturer() { Name = name, Deleted = false, LastUpdateDate = DateTime.UtcNow, CreateDate = DateTime.UtcNow };
                context.AircraftComponentManufacturers.Add(aircraftComponentManufacturer);
                context.SaveChanges();

                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                objResponse = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();

                return objResponse;
            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ComponentManufacturerAndUser>();
            }
        }

        public List<ListItems> UpdateDeleteAircraftModel(string name, int id, bool isUpdate, int makeId)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                var objAircraftModel = context.AircraftModelLists.FirstOrDefault(e => e.Id == id);
                if (objAircraftModel != null)
                {
                    if (isUpdate)
                    {
                        objAircraftModel.ModelName = name;
                    }
                    else
                    {
                        objAircraftModel.Deleted = true;
                        var objModel = context.AircraftProfiles.Where(a => a.Model == id).ToList();
                        objModel.ForEach(m => { m.Model = null; });
                    }

                    context.SaveChanges();

                    objResponse = context.AircraftModelLists.Where(e => !e.Deleted && e.AircraftManufacturerId == makeId).Select(s => new ListItems
                    {
                        text = s.ModelName,
                        value = s.Id,
                    }).ToList();


                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public List<ComponentModel_ConfigSetting> UpdateDeleteComponentModel(string name, int id, bool isUpdate, int componentId, int componentManufacturerId, string fileName, string uniqueNameForURL, int fileSize, string mimeType)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ComponentModel_ConfigSetting>();

            try
            {
                var aircraftComponentModels = context.AircraftComponentModels.FirstOrDefault(e => e.Id == id);
                if (aircraftComponentModels != null)
                {
                    if (isUpdate)
                    {
                        aircraftComponentModels.Name = name;
                        aircraftComponentModels.AircraftComponentId = componentId;
                        //aircraftComponentModels.AircraftComponentManufacturerId = componentManufacturerId;

                        if (fileName != "")
                        {
                            //First remove the existing doc from path and update the document table
                            if (!string.IsNullOrEmpty(aircraftComponentModels.UserMannual))
                            {
                                new Misc().DeleteFileFromS3Bucket(aircraftComponentModels.UniqueName, "Doc");
                                //if (File.Exists(ConfigurationReader.DocumentPathKey + @"\" + aircraftComponentModels.UniqueName))
                                //{
                                //    File.Delete(ConfigurationReader.DocumentPathKey + @"\" + aircraftComponentModels.UniqueName);
                                //}

                                var docUpdate = context.Documents.FirstOrDefault(f => f.FileName == aircraftComponentModels.UserMannual && f.Url == aircraftComponentModels.UniqueName);
                                if (docUpdate != null)
                                {
                                    docUpdate.Url = uniqueNameForURL;
                                    docUpdate.FileSize = fileSize;
                                    docUpdate.MimeType = mimeType;
                                }

                            }
                            aircraftComponentModels.UserMannual = fileName;
                            aircraftComponentModels.UniqueName = uniqueNameForURL;
                            aircraftComponentModels.MimeType = mimeType;
                            aircraftComponentModels.FileSize = fileSize;
                        }
                        aircraftComponentModels.LastUpdateDate = DateTime.UtcNow;
                    }
                    else
                    {
                        aircraftComponentModels.Deleted = true;

                        if (!string.IsNullOrEmpty(aircraftComponentModels.UserMannual))
                        {
                            new Misc().DeleteFileFromS3Bucket(aircraftComponentModels.UniqueName, "Doc");
                            //if (File.Exists(ConfigurationReader.DocumentPathKey + @"\" + aircraftComponentModels.UniqueName))
                            //{
                            //    File.Delete(ConfigurationReader.DocumentPathKey + @"\" + aircraftComponentModels.UniqueName);
                            //}

                            var docUpdate = context.Documents.FirstOrDefault(f => f.FileName == aircraftComponentModels.UserMannual && f.Url == aircraftComponentModels.UniqueName);
                            if (docUpdate != null)
                            {
                                context.Documents.Remove(docUpdate);
                            }
                        }
                        var aircraftComponentMakeAndModels = context.AircraftComponentMakeAndModels.Where(a => a.ComponentModelId == id).ToList();
                        aircraftComponentMakeAndModels.ForEach(m => { m.ComponentModelId = null; });
                    }
                    context.SaveChanges();

                    objResponse = context.AircraftComponentModels.Where(e => !e.Deleted && e.AircraftComponentManufacturerId == componentManufacturerId).Select(s => new ComponentModel_ConfigSetting()
                    {
                        Id = s.Id,
                        componentName = s.AircraftComponent.ComponentName,
                        FileName = s.UserMannual,
                        ManufacturerName = s.AircraftComponentManufacturer.Name,
                        ModelName = s.Name,
                        CompnentId = s.AircraftComponentId,
                        ComponentManufacturerId = s.AircraftComponentManufacturerId
                    }).ToList();
                }
                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ComponentModel_ConfigSetting>();
            }
        }



        public List<ListItems> AddAircraftModel(string name, int makeId)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new List<ListItems>();
            try
            {
                AircraftModelList objAircraftModel = new AircraftModelList() { ModelName = name, Deleted = false, LastUpdated = DateTime.UtcNow, AircraftManufacturerId = makeId };
                context.AircraftModelLists.Add(objAircraftModel);
                context.SaveChanges();

                objResponse = context.AircraftModelLists.Where(e => !e.Deleted && e.AircraftManufacturerId == makeId).Select(s => new ListItems
                {
                    text = s.ModelName,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new List<ListItems>();
            }
        }

        public bool isComponenttModelExists(string compomentModelName)
        {
            var response = false;
            var context = new GuardianAvionicsEntities();

            var aircraftComponentModels = context.AircraftComponentModels.FirstOrDefault(f => f.Name == compomentModelName && !f.Deleted);
            if (aircraftComponentModels != null)
            {
                response = true;
            }
            return response;

        }

        public GetConstants AddComponentModel(AircraftComponentModel aircraftComponentModel)
        {
            var context = new GuardianAvionicsEntities();
            var objResponse = new GetConstants();
            objResponse.ComponentManufacturerAndUserList = new List<ComponentManufacturerAndUser>();
            try
            {
                context.AircraftComponentModels.Add(aircraftComponentModel);
                context.SaveChanges();

                objResponse.ComponentModel = context.AircraftComponentModels.Where(e => !e.Deleted && e.AircraftComponentManufacturerId == aircraftComponentModel.AircraftComponentManufacturerId).Select(s => new ComponentModel_ConfigSetting()
                {
                    Id = s.Id,
                    FileName = s.UserMannual,
                    ManufacturerName = s.AircraftComponentManufacturer.Name,
                    ModelName = s.Name,
                    componentName = s.AircraftComponent.ComponentName,
                    CompnentId = s.AircraftComponentId,
                    ComponentManufacturerId = s.AircraftComponentManufacturerId
                }).ToList();

                objResponse.ComponentList =
                    context.AircraftComponents.Where(rt => !rt.Deleted).Select(s => new ListItems
                    {
                        text = s.ComponentName,
                        value = s.Id,
                    }).ToList();

                //objResponse.ComponentManufacturer = context.AircraftComponentManufacturers.Where(c => !c.Deleted).Select(s => new ListItems
                //                                        {
                //                                            Text = s.Name,
                //                                            Value = s.Id
                //                                        }).ToList();
                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                objResponse.ComponentManufacturerAndUserList = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();


                return objResponse;

            }
            catch (Exception e)
            {
                ////logger.Fatal("Exception ", e);
                return new GetConstants();
            }
        }

        public List<ListItems> GetComponentManufacturerList()
        {
            var context = new GuardianAvionicsEntities();
            var compManuList = new List<ListItems>();

            compManuList.AddRange(
                    context.AircraftComponentManufacturers.Where(c => !c.Deleted).Select(s => new ListItems
                    {
                        text = s.Name,
                        value = s.Id
                    }).ToList());
            return compManuList;
        }



        public List<ListItems> CreateDocumentForAll(System.Web.HttpPostedFileBase file)
        {
            var objResponse = new List<ListItems>();
            var context = new GuardianAvionicsEntities();
            try
            {
                var fileName = file.FileName;

                string path = ConfigurationReader.DocumentPathKey + @"\" + fileName;

                file.SaveAs(path);

                var doc = new GA.DataLayer.Document
                {
                    Deleted = false,
                    IsForAll = true,
                    LastUpdated = DateTime.UtcNow,
                    ProfileId = 0,
                    Url = fileName,
                };
                context.Documents.Add(doc);
                context.SaveChanges();

                objResponse = context.Documents.Where(d => !d.Deleted && d.IsForAll).Select(s => new ListItems
                {
                    text = s.Url,
                    value = s.Id,
                }).ToList();

                return objResponse;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //////logger.Fatal("Exception ", e);
                return objResponse;
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        #endregion ConfigurationSetting

        public HardwareFirmwareFileResponseModel GetHardwareFirmwareFileListAPI()
        {
            HardwareFirmwareFileResponseModel response = new HardwareFirmwareFileResponseModel();
            try
            {
                response.HardwareFirmwareUpdateModelList = GetHardwareFirmwareFileList();
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
            }
            catch (Exception ex)
            {
                return new HardwareFirmwareFileResponseModel
                {
                    ResponseCode = "9999",
                    ResponseMessage = ex.Message
                };
            }
            return response;
        }

        public List<HardwareFirmwareUpdateModel> GetHardwareFirmwareFileList()
        {
            List<HardwareFirmwareUpdateModel> hardwareFirmwareUpdateModel = new List<HardwareFirmwareUpdateModel>();

            var context = new GuardianAvionicsEntities();
             
            string s3BucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketHardwareFirmwareFilePath;
            hardwareFirmwareUpdateModel = context.HardwareFirmwareFiles.Where(m => !m.IsDeleted).OrderByDescending(o => o.Id).Select(a => new HardwareFirmwareUpdateModel
            {
                Id = a.Id,
                FileName = a.FileName,
                Version = a.Version,
                VersionSummary = a.VersionSummary,
                CreateDate = a.CreateDate,
                //FileUrl = ConfigurationReader.ServerUrl + @"/MapFiles/" + a.ZipFileName
                FileUrl = s3BucketPath + a.ZipFileName,
                IsLatest = a.IsLatestVersion ?? false
            }).ToList();

            return hardwareFirmwareUpdateModel;

        }

        public GeneralResponse SavehardwareFirmwareData(HardwareFirmwareFile hardwareFirmwareFile)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
               var hardwareFirmware = context.HardwareFirmwareFiles.FirstOrDefault(f => f.IsLatestVersion == true);
                if (hardwareFirmware != null)
                {
                    hardwareFirmware.IsLatestVersion = false;
                }
                context.HardwareFirmwareFiles.Add(hardwareFirmwareFile);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception";
            }
            response.ResponseCode = "0";
            response.ResponseMessage = "Success";
            return response;

        }

        public GeneralResponse UpdateHardwareFirmwareFile(HardwareFirmwareUpdateModel model)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                HardwareFirmwareFile firmware = context.HardwareFirmwareFiles.FirstOrDefault(f => f.Id == model.Id);
                if (firmware == null)
                {
                    response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.InvalidRequest).ToString();
                    response.ResponseMessage = Enumerations.HardwareFirmwareEnum.InvalidRequest.GetStringValue();
                }
                else {
                    if (model.IsLatest ?? false)
                    {
                        var firmwareLatest = context.HardwareFirmwareFiles.FirstOrDefault(f => f.IsLatestVersion == true);
                        if (firmwareLatest != null)
                        {
                            firmwareLatest.IsLatestVersion = false;
                        }
                        
                    }
                    firmware.IsLatestVersion = model.IsLatest??false;
                    firmware.Version = model.Version;
                    firmware.VersionSummary = model.VersionSummary;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.HardwareFirmwareEnum.Exception.GetStringValue();
            }

            response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.Success).ToString();
            response.ResponseMessage = Enumerations.HardwareFirmwareEnum.Success.GetStringValue();
            return response;
        }

        public GeneralResponse DeleteFirmwareFile(int id)
        {
            var context = new GuardianAvionicsEntities();
            GeneralResponse response = new GeneralResponse();
            try
            {
                HardwareFirmwareFile firmware = context.HardwareFirmwareFiles.FirstOrDefault(f => f.Id == id);
                if (firmware == null)
                {
                    response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.InvalidRequest).ToString();
                    response.ResponseMessage = Enumerations.HardwareFirmwareEnum.InvalidRequest.GetStringValue();
                }
                else
                {
                    new Misc().DeleteFileFromS3Bucket(firmware.ZipFileName, "HardWareFirmware");
                    context.HardwareFirmwareFiles.Remove(firmware);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.Exception).ToString();
                response.ResponseMessage = Enumerations.HardwareFirmwareEnum.Exception.GetStringValue();
            }
            response.ResponseCode = ((int)Enumerations.HardwareFirmwareEnum.Success).ToString();
            response.ResponseMessage = Enumerations.HardwareFirmwareEnum.Success.GetStringValue();
            return response;
        }

        public void SetKeys()
        {
            var context = new GuardianAvionicsEntities();

            var objProfileList = context.Profiles.ToList();
            string passwordEmail = "";
            foreach (var profile in objProfileList)
            {
                passwordEmail = ((new Misc().Decrypt(profile.Password)) + (profile.EmailId).ToLower());
                profile.RecordId = (new Encryption().Encrypt(passwordEmail, Constants.GlobalEncryptionKey)).Substring(0, 16);

            }
            context.SaveChanges();
        }


        //public UpdatedPlatesResponseModel GetUpdatedPlates(DateTime lastUpdatedDate)
        //{

        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter();
        //    param[0].ParameterName = "lastUpdatedDate";
        //    param[0].SqlDbType = SqlDbType.DateTime;
        //    param[0].Value = lastUpdatedDate;

        //    DataSet ds = (Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetStateIdOfUpdatedPlates", param));
        //    var stateIdList = ds.Tables[0].AsEnumerable().Select(m => new
        //    {
        //        StateId = m.Field<int>("StateId"),
        //        TotalFileSizeInMB = m.Field<decimal>("TotalFileSizeInMB")
        //    }).ToList();

        //    var idList = stateIdList.Select(s => s.StateId).ToList();
        //    var context = new GuardianAvionicsEntities();
        //    //objPaltesResponse.StateList
        //    UpdatedPlatesResponseModel objResponse = new UpdatedPlatesResponseModel();
        //    objResponse.StateList = new List<PlateStates>();
        //    objResponse.StateList = context.States.Where(s => idList.Contains(s.Id) && s.State_Code_Id != null).ToList().Select(s => new PlateStates
        //    {
        //        Id = s.Id,
        //        ShortName = s.ShortName,
        //        State_Code_Id = s.State_Code_Id,
        //        StateName = s.StateName,
        //        CityList = s.Cities.Where(wc => wc.City_Name_Id != null && wc.State_Code_Id != null).Select(c => new PlateCitys
        //        {
        //            City_Name_Id = c.City_Name_Id ?? 0,
        //            CityName = c.CityName,
        //            Id = c.Id,
        //            State_Code_Id = c.State_Code_Id,
        //            StateId = c.StateId,
        //            Volume = c.Volume,
        //            AirportList = c.Airports.Where(wa => wa.airport_name_Id != null && wa.city_name_Id != null).Select(a => new PlateAirPorts
        //            {
        //                airport_name_Id = a.airport_name_Id,
        //                AirportName = a.AirportName,
        //                apt_ident = a.apt_ident,
        //                city_name_Id = a.city_name_Id,
        //                CityId = a.CityId,
        //                Country = a.Country,
        //                icao_ident = a.icao_ident,
        //                Id = a.Id,
        //                Military = a.Military,
        //                X = a.X,
        //                Y = a.Y,
        //                PlateList = a.Plates.Where(wp => wp.Airport_Name_Id != null).Select(p => new PlateDetails
        //                {
        //                    Airport_Name_Id = p.Airport_Name_Id,
        //                    AirportId = p.AirportId,
        //                    Chart_Code = p.Chart_Code,
        //                    Chart_Name = p.Chart_Name,
        //                    ChartSeq = p.ChartSeq,
        //                    Civil = p.Civil,
        //                    Copter = p.Copter,
        //                    Draw_Bottom = p.Draw_Bottom,
        //                    Draw_Left = p.Draw_Left,
        //                    Draw_Right = p.Draw_Right,
        //                    Draw_Top = p.Draw_Top,
        //                    ExcludeAreas = p.ExcludeAreas,
        //                    Faanfd15 = p.Faanfd15,
        //                    Faanfd18 = p.Faanfd18,
        //                    //File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
        //                    File_Name = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/" + p.File_Name,
        //                    Id = p.Id,
        //                    Invalid = p.Invalid == "true" ? true : false,
        //                    LastChanged = p.LastChanged,
        //                    Lat1 = p.Lat1,
        //                    Lat2 = p.Lat2,
        //                    Long1 = p.Long1,
        //                    Long2 = p.Long2,
        //                    Orientation = p.Orientation,
        //                    UserAction = p.UserAction,
        //                    ValidWithinRadius = p.ValidWithinRadius,
        //                    ValidWithinX = p.ValidWithinX,
        //                    ValidWithinY = p.ValidWithinY,
        //                    X1 = p.X1,
        //                    X2 = p.X2,
        //                    Y1 = p.Y1,
        //                    Y2 = p.Y2,
        //                    FileSize = p.FileSize == null ? (long)0.0 : Math.Round(((long)p.FileSize / 1024f) / 1024f, 6)
        //                }).ToList()
        //            }).ToList()
        //        }).ToList()
        //    }).ToList();

        //    decimal totalSize = 0;
        //    objResponse.StateList.ForEach(f => f.CityList.ForEach(c => c.AirportList.ForEach(a => a.PlateList.RemoveAll(p => Convert.ToDateTime(p.LastChanged) <= lastUpdatedDate))));
        //    objResponse.StateList.ForEach(f =>
        //    {
        //        totalSize = totalSize + stateIdList.FirstOrDefault(s => s.StateId == f.Id).TotalFileSizeInMB;
        //    });
        //    objResponse.TotalPlateSizeInMB = totalSize;

        //    objResponse.ResponseCode = "0000";
        //    objResponse.ResponseMessage = "Succcess";
        //    return objResponse;
        //}


        public ChartDetailsResponseModel GetChartDetails(DateTime? lastUpdateDate, DateTime? lastChangedDataTimeForPlate, int[] idList)
        {
            ChartDetailsResponseModel response = new ChartDetailsResponseModel();
            List<ChartDetailModel> ChartDetailsList = new List<ChartDetailModel>();
            List<ChartState> chartStateList = new List<ChartState>();
            var context = new GuardianAvionicsEntities();

            try
            {

                var udate = lastUpdateDate ?? DateTime.MinValue;

                var objList = context.ChartDetails.Where(w => w.LastUpdateDate > udate).GroupBy(cm => new
                {
                    StateId = cm.StateId,
                    StateName = cm.State.StateName,
                    ShortName = cm.State.ShortName
                }).Select(g => new
                {
                    StateId = g.Key.StateId,
                    StateName = g.Key.StateName,
                    ShortName = g.Key.ShortName,
                    PlateMaxExpiryDate = "",
                    ChartDetailsList = g.Select(s => new
                    {
                        Country = s.Country.CountryName,
                        CountryId = s.CountryId,
                        State = s.State.StateName,
                        StateId = s.StateId,
                        ChartType = s.LayerType,
                        ChartTypeId = s.ChartTypeId,
                        Name = s.Name,
                        ShortName = s.ShortName,
                        Type = s.Type,
                        Version = s.Version,
                        Description = s.Description,
                        Format = s.Format,
                        ExpDate = (s.ExpDate),
                        Expiration = (s.Expiration),
                        Provider = s.Provider,
                        shortProvider = s.ShortProvider,
                        Copyright = s.Copyright,
                        CreateDate = (s.CreateDate),
                        ShortChartType = s.ShortLayerType,
                        MinZoom = s.MinZoom,
                        MaxZoom = s.MaxZoom,
                        Bounds = s.Bounds,
                        FileName = s.FileName,
                        FileSize = s.FileSize,
                        Password = s.Password,
                        Id = s.Id
                    }).ToList()

                }).ToList();

                string s3BucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketMapFilePath;
                chartStateList = objList.Select(s => new ChartState
                {
                   CountryId = 1,
                   Country = "US",
                    StateId = s.StateId,
                    StateName = s.StateName,
                    ShortName = s.ShortName,
                    PlateMaxExpiryDate = "",
                    PlateTotalFileSizeInMB = null,
                    PlateFilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGForAllState + "/" + s.StateId.ToString() + ".zip",
                    ChartDetailsList = s.ChartDetailsList.Select(c => new ChartDetailModel
                    {
                        Country = c.Country,
                        CountryId = c.CountryId,
                        ChartType = c.ChartType,
                        ChartTypeId = c.ChartTypeId,
                        Name = c.Name,
                        ShortName = c.ShortName,
                        Type = c.Type,
                        Version = c.Version,
                        Description = c.Description,
                        Format = c.Format,
                        ExpDate = Misc.GetStringOnlyDateFormat2(c.ExpDate),
                        Expiration = Misc.GetStringOnlyDateFormat2(c.Expiration),
                        Provider = c.Provider,
                        shortProvider = c.shortProvider,
                        Copyright = c.Copyright,
                        CreateDate = Misc.GetStringOnlyDateFormat2(c.CreateDate),
                        ShortChartType = c.ShortChartType,
                        MinZoom = c.MinZoom,
                        MaxZoom = c.MaxZoom,
                        Bounds = c.Bounds,
                        //FileName = ConfigurationReader.ServerUrl + @"/MapFiles/" + c.FileName,
                        FileName = s3BucketPath + c.FileName,
                        FileSize = c.FileSize,
                        Password = c.Password,
                        Id = c.Id.ToString()
                    }).ToList()
                }).ToList();



                DataSet ds = (Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetPlateExpDateByState"));
                var platesExpDateByStateList = ds.Tables[0].AsEnumerable().Select(m => new
                {
                    StateId = m.Field<int>("ID"),
                    MaxExpDate = m.Field<DateTime>("MaxExpDate"),
                    TotalFileSizeInMB = m.Field<decimal>("TotalFileSizeInMB")
                }).ToList();


                chartStateList.ForEach(f =>
                {
                    var plateExpDataAndSize = platesExpDateByStateList.FirstOrDefault(p => p.StateId == f.StateId);
                    if (plateExpDataAndSize == null)
                    {
                        f.PlateMaxExpiryDate = null;
                        f.PlateTotalFileSizeInMB = null;
                    }
                    else
                    {
                        f.PlateMaxExpiryDate = Misc.GetStringOnlyDateFormat2(plateExpDataAndSize.MaxExpDate);
                        f.PlateTotalFileSizeInMB = plateExpDataAndSize.TotalFileSizeInMB;
                    }
                });
                response.AirportNavAndFreqUpdateDate = new AirportNavAndFreqStatusApp();
                var objNavStatus = context.AirportNavAndFreqStatus.FirstOrDefault();
                if (objNavStatus == null)
                {
                    response.AirportNavAndFreqUpdateDate.EndDate = null;
                    response.AirportNavAndFreqUpdateDate.StartDate = null;
                    response.AirportNavAndFreqUpdateDate.ModifyDate = null;
                    response.AirportNavAndFreqUpdateDate.FileSizeInMB = null;
                    response.AirportNavAndFreqUpdateDate.FileURL = null;
                }
                else
                {
                    response.AirportNavAndFreqUpdateDate.EndDate = Misc.GetStringOnlyDateFormat2(objNavStatus.EndDate);
                    response.AirportNavAndFreqUpdateDate.StartDate = Misc.GetStringOnlyDateFormat2(objNavStatus.StartDate);
                    response.AirportNavAndFreqUpdateDate.ModifyDate = Misc.GetStringOnlyDateFormat2(objNavStatus.ModifyDate);
                    response.AirportNavAndFreqUpdateDate.FileSizeInMB = objNavStatus.FileSizeInMB;
                    response.AirportNavAndFreqUpdateDate.FileURL = ConfigurationReader.s3BucketURL + "JsonMisc/AirportNavaidAndFrequency.txt";
                }

                response.PlateList = new PlateList();
                response.PlateList.PlateStateList = new List<PlateStates>();
                if (lastChangedDataTimeForPlate != null)
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "lastUpdatedDate";
                    param[0].SqlDbType = SqlDbType.DateTime;
                    param[0].Value = lastChangedDataTimeForPlate;

                    ds = (Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetStateIdOfUpdatedPlates", param));
                    var stateIdList = ds.Tables[0].AsEnumerable().Select(m => new
                    {
                        StateId = m.Field<int>("StateId"),
                        TotalFileSizeInMB = m.Field<decimal>("TotalFileSizeInMB")
                    }).ToList();

                    stateIdList.RemoveAll(f => !idList.Contains(f.StateId));

                    response.PlateList.PlateStateList = context.States.Where(s => idList.Contains(s.Id) && s.State_Code_Id != null).ToList().Select(s => new PlateStates
                    {
                        Id = s.Id,
                        ShortName = s.ShortName,
                        State_Code_Id = s.State_Code_Id,
                        StateName = s.StateName,
                        PlateFilePath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGForAllState + "/" + s.Id.ToString() + ".zip",
                        PlatesFileSizeInMB = s.PlateFileSizeInMB,
                        CityList = s.Cities.Where(wc => wc.City_Name_Id != null && wc.State_Code_Id != null).Select(c => new PlateCitys
                        {
                            City_Name_Id = c.City_Name_Id ?? 0,
                            CityName = c.CityName,
                            Id = c.Id,
                            State_Code_Id = c.State_Code_Id,
                            StateId = c.StateId,
                            Volume = c.Volume,
                            AirportList = c.Airports.Where(wa => wa.airport_name_Id != null && wa.city_name_Id != null).Select(a => new PlateAirPorts
                            {
                                airport_name_Id = a.airport_name_Id,
                                AirportName = a.AirportName,
                                apt_ident = a.apt_ident,
                                city_name_Id = a.city_name_Id,
                                CityId = a.CityId,
                                Country = a.Country,
                                icao_ident = a.icao_ident,
                                Id = a.Id,
                                Military = a.Military,
                                X = a.X,
                                Y = a.Y,
                                PlateList = a.Plates.Where(wp => wp.Airport_Name_Id != null).Select(p => new PlateDetails
                                {
                                    Airport_Name_Id = p.Airport_Name_Id,
                                    AirportId = p.AirportId,
                                    Chart_Code = p.Chart_Code,
                                    Chart_Name = p.Chart_Name,
                                    ChartSeq = p.ChartSeq,
                                    Civil = p.Civil,
                                    Copter = p.Copter,
                                    Draw_Bottom = p.Draw_Bottom,
                                    Draw_Left = p.Draw_Left,
                                    Draw_Right = p.Draw_Right,
                                    Draw_Top = p.Draw_Top,
                                    ExcludeAreas = p.ExcludeAreas,
                                    Faanfd15 = p.Faanfd15,
                                    Faanfd18 = p.Faanfd18,
                                    File_Name = p.File_Name,
                                    //File_Name = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/" + p.File_Name,
                                    Id = p.Id,
                                    Invalid = p.Invalid == "true" ? true : false,
                                    LastChanged = p.LastChanged,
                                    Lat1 = p.Lat1,
                                    Lat2 = p.Lat2,
                                    Long1 = p.Long1,
                                    Long2 = p.Long2,
                                    Orientation = p.Orientation,
                                    UserAction = p.UserAction,
                                    ValidWithinRadius = p.ValidWithinRadius,
                                    ValidWithinX = p.ValidWithinX,
                                    ValidWithinY = p.ValidWithinY,
                                    X1 = p.X1,
                                    X2 = p.X2,
                                    Y1 = p.Y1,
                                    Y2 = p.Y2,
                                    FileSize = p.FileSize == null ? (long)0.0 : Math.Round(((long)p.FileSize / 1024f) / 1024f, 6)
                                }).ToList()
                            }).ToList()
                        }).ToList()
                    }).ToList();


                    decimal totalSize = 0;
                    response.PlateList.PlateStateList.ForEach(f => f.CityList.ForEach(c => c.AirportList.ForEach(a => a.PlateList.RemoveAll(p => Convert.ToDateTime(p.LastChanged) <= lastChangedDataTimeForPlate))));
                    response.PlateList.PlateStateList.ForEach(f =>
                    {
                        totalSize = totalSize + stateIdList.FirstOrDefault(s => s.StateId == f.Id).TotalFileSizeInMB;
                    });
                    response.PlateList.TotalPlateSizeInMB = totalSize;
                }
            }
            catch (Exception ex)
            {
                return new ChartDetailsResponseModel
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Exception"
                };
            }
            response.ChartStateList = chartStateList;
            response.ResponseCode = "0";
            response.ResponseMessage = "Succcess";
            return response;
        }
        /// <summary>
        /// return 0 if country already exist else return country Id
        /// </summary>
        /// <param name="countryName"></param>
        /// <param name="shortName"></param>
        /// <returns></returns>
        public int SetCountry(string countryName, string shortName)
        {
            var context = new GuardianAvionicsEntities();
            var country = context.Countries.FirstOrDefault(f => f.CountryName == countryName);
            if (country != null)
            {
                return country.Id;
            }
            else
            {
                country = context.Countries.Create();
                country.CountryName = countryName;
                country.ShortName = shortName;
                context.Countries.Add(country);
                context.SaveChanges();
            }
            return country.Id;
        }

        /// <summary>
        /// return 0 if state already exist else return state Id
        /// </summary>
        /// <param name="countryName"></param>
        /// <param name="shortName"></param>
        /// <returns></returns>
        public int SetState(string stateName, string shortName)
        {
            var context = new GuardianAvionicsEntities();
            var state = context.States.FirstOrDefault(f => f.StateName == stateName);
            if (state != null)
            {
                return state.Id;
            }
            else
            {
                state = context.States.Create();
                state.StateName = stateName;
                state.ShortName = shortName;
                context.States.Add(state);
                context.SaveChanges();

            }
            return state.Id;
        }

        public int SetChart(string chartType)
        {
            var context = new GuardianAvionicsEntities();
            var chart = context.ChartTypeMasters.FirstOrDefault(f => f.ChartType == chartType);
            if (chart != null)
            {
                return chart.Id;
            }
            else
            {
                chart = context.ChartTypeMasters.Create();
                chart.ChartType = chartType;

                context.ChartTypeMasters.Add(chart);
                context.SaveChanges();

            }
            return chart.Id;
        }

        public List<ChartListByStateModel> GetChartDetails()
        {
            var context = new GuardianAvionicsEntities();
            List<ChartListByStateModel> response = new List<ChartListByStateModel>();

            response = (from p in context.ChartDetails
                        group p by p.State.StateName into g
                        select new ChartListByStateModel
                        {
                            StateName = g.Key,
                            ChartDetailModelList = g.Select(s => new ChartDetailModel
                            {

                                //Bounds = s.Bounds,
                                ChartType = s.ChartTypeMaster.ChartType,
                                //Copyright = s.Copyright,
                                CreateDate = Misc.GetStringOnlyDateFormat2(s.CreateDate),
                                Description = s.Description,
                                ExpDate = Misc.GetStringOnlyDateFormat2(s.ExpDate),
                                FileName = s.FileName,
                                FileSize = s.FileSize,
                                Format = s.Format,
                                MaxZoom = s.MaxZoom,
                                MinZoom = s.MinZoom,
                                Id = s.Id.ToString(),
                                Name = s.Name,
                                Provider = s.Provider,
                                Type = s.Type,
                                Version = s.Version
                            }).ToList()
                        }).ToList();

            return response;

        }

        public void DeleteChartFile(int id)
        {
            var context = new GuardianAvionicsEntities();
            var chartDetail = context.ChartDetails.FirstOrDefault(f => f.Id == id);
            if (chartDetail != null)
            {
                context.ChartDetails.Remove(chartDetail);
                context.SaveChanges();
            }
        }




        public void zip_ExtractProgressText(object sender, ExtractProgressEventArgs exx)
        {
            if (exx.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                try
                {
                    //logger.Fatal("Text file unzip successfully. " + Environment.NewLine);


                    DataTable dt = new DataTable("Airports");
                    dt.Columns.Add("AirportName", typeof(string));   //0
                    dt.Columns.Add("Military", typeof(string)); //1
                    dt.Columns.Add("apt_ident", typeof(string));  //2
                    dt.Columns.Add("icao_ident", typeof(string)); //3
                    dt.Columns.Add("airport_name_Id", typeof(int));     //4
                    dt.Columns.Add("city_name_Id", typeof(int)); //5 
                    dt.Columns.Add("Country", typeof(string)); //6 
                    dt.Columns.Add("X", typeof(decimal)); //7 
                    dt.Columns.Add("Y", typeof(decimal)); //8 
                    dt.Columns.Add("CityId", typeof(decimal)); //9 

                    DataRow dr = null;
                    string sep = "\t";
                    foreach (var line in System.IO.File.ReadLines(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesText\Airports.txt").Skip(7))
                    {
                        dr = dt.NewRow();
                        string[] arrColumns = line.Split(sep.ToCharArray());
                        dr[0] = arrColumns[0];
                        dr[1] = arrColumns[1];
                        dr[2] = arrColumns[2];
                        dr[3] = arrColumns[3];
                        dr[4] = Convert.ToInt16(arrColumns[4]);
                        dr[5] = Convert.ToInt16(arrColumns[5]);
                        dr[6] = arrColumns[6];
                        dr[7] = Convert.ToDecimal(arrColumns[7]);
                        dr[8] = Convert.ToDecimal(arrColumns[8]);
                        dr[9] = 1;
                        dt.Rows.Add(dr);

                    }

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "tbAirport";
                    param[0].SqlDbType = SqlDbType.Structured;
                    param[0].Value = dt;

                    if (dt.Rows.Count > 0)
                    {
                        int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetAirports", param));
                    }



                    WebClient webClient = new WebClient();
                    //logger.Fatal("Plates SQLite file downloaded Process Start." + Environment.NewLine);
                    webClient.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/1604/1604.Plates.sqlite.zip"), ConfigurationReader.PlateFilePath + "Plates.zip");


                    webClient.DownloadFileCompleted += (s, e) =>
                    {
                        //logger.Fatal("Plates SQLite file downloaded. " + Environment.NewLine);
                        if (File.Exists(ConfigurationReader.PlateFilePath + @"UnZipFiles\Plates.sqlite"))
                        {
                            File.Delete(ConfigurationReader.PlateFilePath + @"UnZipFiles\Plates.sqlite");
                        }

                        using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "Plates.zip"))
                        {
                            zip.Password = "595685";
                            zip.ExtractProgress += zip_ExtractProgress;
                            zip.ExtractAll(ConfigurationReader.PlateFilePath + "UnZipFiles");
                        }
                    };


                    //WebClient webClientPnfFile = new WebClient();
                    ////logger.Fatal("Plates PNG file downloaded Process Start." + Environment.NewLine);
                    //webClientPnfFile.DownloadFileAsync(new Uri("http://chartdata.seattleavionics.com/oem/1604/1604.Plates1024.PNG.zip"), ConfigurationReader.PlateFilePath + "PlatesPNG.zip");
                    //webClientPnfFile.DownloadFileCompleted += (s, e) =>
                    //{
                    //    //logger.Fatal("Plates SQLite file downloaded Successfully." + Environment.NewLine);
                    //    if (Directory.Exists(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG"))
                    //    {
                    //        Directory.Delete(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG", true);
                    //    }
                    //    //logger.Fatal("Plates SQLite file starts unZip." + Environment.NewLine);
                    //    using (var zip = ZipFile.Read(ConfigurationReader.PlateFilePath + "PlatesPNG.zip"))
                    //    {
                    //        zip.Password = "595685";
                    //        zip.ExtractProgress += zip_ExtractProgressPNG;
                    //        zip.ExtractAll(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");
                    //    }
                    //};

                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
                }
            }
        }




        public void zip_ExtractProgressPNG(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                //logger.Fatal("Plates PNG File unextract successfully." + Environment.NewLine);
                DirectoryInfo DirInfo = new DirectoryInfo(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");

                try
                {
                    DataTable dt = new DataTable("PNGDetails");
                    dt.Columns.Add("FileName", typeof(string));   //0
                    dt.Columns.Add("ListPrice", typeof(int));  //2
                    DataRow dr = null;
                    foreach (FileInfo fi in DirInfo.EnumerateFiles())
                    {
                        dr = dt.NewRow();
                        dr[0] = fi.Name;
                        dr[1] = fi.Length;
                        dt.Rows.Add(dr);
                    }

                    UpdateFileSizePlates(dt);


                }
                catch (Exception Ex)
                {
                    //logger.Fatal("Exception in zip_ExtractProgressPNG  " + Newtonsoft.Json.JsonConvert.SerializeObject(Ex) + " " + Environment.NewLine);
                }

            }
        }


        public void tempProcess()
        {
            DirectoryInfo DirInfo = new DirectoryInfo(ConfigurationReader.PlateFilePath + @"UnZipFiles\PlatesPNG");

            try
            {
                DataTable dt = new DataTable("PNGDetails");
                dt.Columns.Add("FileName", typeof(string));   //0
                dt.Columns.Add("ListPrice", typeof(int));  //2
                DataRow dr = null;
                foreach (FileInfo fi in DirInfo.EnumerateFiles())
                {
                    dr = dt.NewRow();
                    dr[0] = fi.Name;
                    dr[1] = fi.Length;
                    dt.Rows.Add(dr);
                }

                UpdateFileSizePlates(dt);


            }
            catch (Exception Ex)
            {

            }
        }

        public void UpdateFileSizePlates(DataTable dt)
        {
            //logger.Fatal("Plates PNG file Size updation start " + Environment.NewLine);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "tbPlateFileSize";
            param[0].SqlDbType = SqlDbType.Structured;
            param[0].Value = dt;

            if (dt.Rows.Count > 0)
            {
                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlatesFileSize", param));
            }
            //logger.Fatal("Plates PNG file Size updation completed " + Environment.NewLine);
            CreateTextFileForPlates();


        }

        public void CreateTextFileForPlates()
        {
            //logger.Fatal("Plates PNG file - start creating JSON. " + Environment.NewLine);
            var context = new GuardianAvionicsEntities();

            var objState = context.States.Select(s => s.Id).ToList();
            PlatesResponseModel objPaltesResponse = new PlatesResponseModel();
            var stateList = new List<PlateStates>();

            foreach (int stateId in objState)
            {
                objPaltesResponse.StateList = context.States.Where(s => s.Id == stateId).Select(s => new PlateStates
                {
                    Id = s.Id,
                    ShortName = s.ShortName,
                    State_Code_Id = s.State_Code_Id,
                    StateName = s.StateName,
                    CityList = s.Cities.Where(wc => wc.City_Name_Id != null).Select(c => new PlateCitys
                    {
                        City_Name_Id = c.City_Name_Id ?? 0,
                        CityName = c.CityName,
                        Id = c.Id,
                        State_Code_Id = c.State_Code_Id,
                        StateId = c.StateId,
                        Volume = c.Volume,
                        AirportList = c.Airports.Where(wa => wa.airport_name_Id != null).Select(a => new PlateAirPorts
                        {
                            airport_name_Id = a.airport_name_Id,
                            AirportName = a.AirportName,
                            apt_ident = a.apt_ident,
                            city_name_Id = a.city_name_Id,
                            CityId = a.CityId,
                            Country = a.Country,
                            icao_ident = a.icao_ident,
                            Id = a.Id,
                            Military = a.Military,
                            X = a.X,
                            Y = a.Y,
                            PlateList = a.Plates.Select(p => new PlateDetails
                            {
                                Airport_Name_Id = p.Airport_Name_Id,
                                AirportId = p.AirportId,
                                Chart_Code = p.Chart_Code,
                                Chart_Name = p.Chart_Name,
                                ChartSeq = p.ChartSeq,
                                Civil = p.Civil,
                                Copter = p.Copter,
                                Draw_Bottom = p.Draw_Bottom,
                                Draw_Left = p.Draw_Left,
                                Draw_Right = p.Draw_Right,
                                Draw_Top = p.Draw_Top,
                                ExcludeAreas = p.ExcludeAreas,
                                Faanfd15 = p.Faanfd15,
                                Faanfd18 = p.Faanfd18,
                                File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
                                Id = p.Id,
                                Invalid = Convert.ToBoolean(p.Invalid),
                                LastChanged = p.LastChanged,
                                Lat1 = p.Lat1,
                                Lat2 = p.Lat2,
                                Long1 = p.Long1,
                                Long2 = p.Long2,
                                Orientation = p.Orientation,
                                UserAction = p.UserAction,
                                ValidWithinRadius = p.ValidWithinRadius,
                                ValidWithinX = p.ValidWithinX,
                                ValidWithinY = p.ValidWithinY,
                                X1 = p.X1,
                                X2 = p.X2,
                                Y1 = p.Y1,
                                Y2 = p.Y2
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

                System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + @"JsonFiles\" + stateId.ToString() + ".txt", Newtonsoft.Json.JsonConvert.SerializeObject(objPaltesResponse));
            }

            //logger.Fatal("Plates PNG file - creating JSON Completed " + Environment.NewLine);


        }


        public void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                try
                {
                    //logger.Fatal("sqlite file unzip successfully. " + Environment.NewLine);
                    sqlite = new SQLiteConnection(ConfigurationReader.SqliteFilePathForPlates + "Plates.sqlite");
                    string query = "select * from charts";
                    DataTable dt = new DataTable();
                    dt = selectQuery(query);
                    SetPlatesInToDB(dt);
                    tempProcess();

                }
                catch (Exception ex)
                {
                    //logger.Fatal("Exception in  zip_ExtractProgress method " + Newtonsoft.Json.JsonConvert.SerializeObject(ex) + " " + Environment.NewLine);
                }
            }
        }



        public void SetPlatesInToDB(DataTable dt)
        {
            //logger.Fatal("Start inserting plates data into DB." + Environment.NewLine);
            var context = new GuardianAvionicsEntities();
            dt.Columns[2].Caption = "ChartSeq";
            dt.Columns[5].Caption = "Civil";
            dt.Columns[6].Caption = "Faanfd15";
            dt.Columns[7].Caption = "Faanfd18";
            dt.Columns[8].Caption = "Copter";
            dt.Columns.Remove("Airports_Id");

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "tbPlate";
            param[0].SqlDbType = SqlDbType.Structured;
            param[0].Value = dt;

            //foreach (DataRow dr in dt.Rows)
            //{
            //    dr["Civil"] = "abc";
            //    dr.EndEdit();
            //}
            //dt.AcceptChanges();

            if (dt.Rows.Count > 0)
            {
                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetPlates", param));
            }

            //logger.Fatal("Plates data inserted successfully in DB." + Environment.NewLine);

        }

        public DataTable selectQuery(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();

            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            sqlite.Close();
            return dt;
        }

        public List<PlateStates> GetPlatesData(int[] arr)
        {
            string s3BucketPath = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlatePNGAndTxtFolder + "/PlatesPNG/";
            var context = new GuardianAvionicsEntities();
            var stateList = context.States.Where(s => arr.Contains(s.Id)).Select(s => new PlateStates
            {
                Id = s.Id,
                ShortName = s.ShortName,
                State_Code_Id = s.State_Code_Id,
                StateName = s.StateName,
                CityList = s.Cities.Where(wc => wc.City_Name_Id != null).Select(c => new PlateCitys
                {
                    City_Name_Id = c.City_Name_Id ?? 0,
                    CityName = c.CityName,
                    Id = c.Id,
                    State_Code_Id = c.State_Code_Id,
                    StateId = c.StateId,
                    Volume = c.Volume,
                    AirportList = c.Airports.Where(wa => wa.airport_name_Id != null).Select(a => new PlateAirPorts
                    {
                        airport_name_Id = a.airport_name_Id,
                        AirportName = a.AirportName,
                        apt_ident = a.apt_ident,
                        city_name_Id = a.city_name_Id,
                        CityId = a.CityId,
                        Country = a.Country,
                        icao_ident = a.icao_ident,
                        Id = a.Id,
                        Military = a.Military,
                        X = a.X,
                        Y = a.Y,
                        PlateList = a.Plates.Select(p => new PlateDetails
                        {
                            Airport_Name_Id = p.Airport_Name_Id,
                            AirportId = p.AirportId,
                            Chart_Code = p.Chart_Code,
                            Chart_Name = p.Chart_Name,
                            ChartSeq = p.ChartSeq,
                            Civil = p.Civil,
                            Copter = p.Copter,
                            Draw_Bottom = p.Draw_Bottom,
                            Draw_Left = p.Draw_Left,
                            Draw_Right = p.Draw_Right,
                            Draw_Top = p.Draw_Top,
                            ExcludeAreas = p.ExcludeAreas,
                            Faanfd15 = p.Faanfd15,
                            Faanfd18 = p.Faanfd18,
                            File_Name = s3BucketPath + p.File_Name,
                            Id = p.Id,
                            Invalid = Convert.ToBoolean(p.Invalid),
                            LastChanged = p.LastChanged,
                            Lat1 = p.Lat1,
                            Lat2 = p.Lat2,
                            Long1 = p.Long1,
                            Long2 = p.Long2,
                            Orientation = p.Orientation,
                            UserAction = p.UserAction,
                            ValidWithinRadius = p.ValidWithinRadius,
                            ValidWithinX = p.ValidWithinX,
                            ValidWithinY = p.ValidWithinY,
                            X1 = p.X1,
                            X2 = p.X2,
                            Y1 = p.Y1,
                            Y2 = p.Y2
                        }).ToList()
                    }).ToList()
                }).ToList()
            }).ToList();

            return stateList;
        }

        public List<ListItems> GetEngines()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var response = new List<ListItems>();
                response.AddRange
                   (
                       context.Engines.Where(rt => !rt.Deleted).Select(s => new ListItems
                       {
                           text = s.Name,
                           value = s.Id,
                       }).ToList()
                   );

                return response;
            }
            catch (Exception ex)
            {
                return new List<ListItems>();
            }
        }


        public List<ListItems> GetRatingTypes()
        {
            var context = new GuardianAvionicsEntities();
            try
            {


                var response = new List<ListItems>();
                response.AddRange(
                     context.RatingTypes.Where(rt => !rt.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id,
                     }).ToList());

                return response;
            }
            catch (Exception ex)
            {
                return new List<ListItems>();
            }
        }


        public List<ListItems> GetPropellers()
        {
            var context = new GuardianAvionicsEntities();
            try
            {


                var response = new List<ListItems>();
                response.AddRange(
                     context.Propellers.Where(rt => !rt.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id,
                     }).ToList());

                return response;
            }
            catch (Exception ex)
            {
                return new List<ListItems>();
            }
        }

        public List<ListItems> GetMedicalCertificateClass()
        {
            var context = new GuardianAvionicsEntities();
            try
            {


                var response = new List<ListItems>();
                response.AddRange
                    (
                         context.MedicalCertClasses.Where(rt => !rt.Deleted).Select(s => new ListItems
                         {
                             text = s.Name,
                             value = s.Id,
                         }).ToList()
                    );

                return response;
            }
            catch (Exception ex)
            {
                return new List<ListItems>();
            }
        }
        public string GetIconURL(string fileExt, string mimeType)
        {
            mimeType = mimeType ?? "";
            string url = "";
            url = ConfigurationReader.ServerUrl + "/Images/FileIcons/";

            if (mimeType.Contains("audio"))
            {
                return url += "File-Audio-icon.png";
            }
            else if (mimeType.Contains("video"))
            {
                return url += "file-video-icon.png";
            }
            else
            {
                string[] arrext = fileExt.Split('.');
                switch (arrext[arrext.Length - 1])
                {

                    case "doc":
                        {
                            url += "doc.png";
                        }
                        break;
                    case "docx":
                        {
                            url += "doc.png";
                        }
                        break;
                    case "jpg":
                        {
                            url += "jpg.png";
                        }
                        break;
                    case "pdf":
                        {
                            url += "pdf24.png";
                        }
                        break;
                    case "png":
                        {
                            url += "png.png";
                        }
                        break;
                    case "ppt":
                        {
                            url += "ppt.png";
                        }
                        break;
                    case "xls":
                        {
                            url += "xls.png";
                        }
                        break;
                    case "xlsx":
                        {
                            url += "xls.png";
                        }
                        break;
                    case "zip":
                        {
                            url += "zip.png";
                        }
                        break;
                    case "txt":
                        {
                            url += "txt.png";
                        }
                        break;
                    case "csv":
                        {
                            url += "csv.png";
                        }
                        break;
                    default:
                        {
                            url += "File-Types-Default-icon.png";
                            break;
                        }
                }
            }



            return url;
        }

        public List<DocumentModelWeb> GetFlightBag()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var response = new List<DocumentModelWeb>();

                var path = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketDocPath;
                var docs = context.Documents.Where(d => d.IsForAll && !d.Deleted && d.FileSize != null).ToList();
                if (docs.Count != 0)
                {
                    int count = docs.Count;
                    foreach (var d in docs)
                    {
                        response.Add(new DocumentModelWeb
                        {
                            //Deleted = d.Deleted,
                            SNO = count,
                            Id = d.Id,
                            LastUpdateDate = Misc.GetStringOfDate(d.LastUpdated),
                            IconUrl = GetIconURL(d.FileName, d.MimeType),
                            Name = d.FileName,
                            IsForAll = d.IsForAll,
                            CanDeleteDocument = (!d.IsForAll && d.AircraftId == null),
                            Title = d.Title,
                            FileSize = ToFileSize(Convert.ToInt64(d.FileSize ?? 0)),
                            Url = path + d.Url
                        });
                        count = count - 1;
                    }
                    return response;
                }
                return new List<DocumentModelWeb>();
            }
            catch (Exception ex)
            {
                return new List<DocumentModelWeb>();
            }
        }

        public string ToFileSize(long source)
        {
            const int byteConversion = 1024;
            double bytes = Convert.ToDouble(source);

            if (bytes >= Math.Pow(byteConversion, 3)) //GB Range
            {
                return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 3), 2), " GB");
            }
            else if (bytes >= Math.Pow(byteConversion, 2)) //MB Range
            {
                return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 2), 2), " MB");
            }
            else if (bytes >= byteConversion) //KB Range
            {
                return string.Concat(Math.Round(bytes / byteConversion, 2), " KB");
            }
            else //Bytes
            {
                return string.Concat(bytes, " Bytes");
            }
        }

        public List<ListItems> GetAircraftManufacturer()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var response = new List<ListItems>();
                response.AddRange(
                     context.AircraftManufacturers.Where(a => !a.Deleted).Select(s => new ListItems
                     {
                         text = s.Name,
                         value = s.Id
                     }).ToList());
                return response;
            }
            catch (Exception ex)
            {
                return new List<ListItems>();
            }
        }


        public List<ComponentManufacturerAndUser> GetComponentManufacturer()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var response = new List<ComponentManufacturerAndUser>();
                var aircraftComponentManufacturers = context.AircraftComponentManufacturers.Include(i => i.MappingAircraftManufacturerAndUsers).Where(c => !c.Deleted).Select(s => new { Id = s.Id, mappingAircraftManufacturerAndUsers = s.MappingAircraftManufacturerAndUsers.FirstOrDefault(), ManufacturerName = s.Name }).ToList();
                response = aircraftComponentManufacturers.Select(s => new ComponentManufacturerAndUser
                {
                    Id = s.Id,
                    Name = s.ManufacturerName,
                    EmailId = (s.mappingAircraftManufacturerAndUsers == null) ? "" : s.mappingAircraftManufacturerAndUsers.Profile.EmailId
                }).ToList();
                return response;
            }
            catch (Exception ex)
            {
                return new List<ComponentManufacturerAndUser>();
            }
        }


        public List<ComponentModel_ConfigSetting> GetComponentModelList(int componentManuId)
        {
            var response = new List<ComponentModel_ConfigSetting>();
            var context = new GuardianAvionicsEntities();
            try
            {
                response =
                   context.AircraftComponentModels.Where(w => !w.Deleted && w.AircraftComponentManufacturerId == componentManuId)
                       .Select(s => new ComponentModel_ConfigSetting()
                       {
                           Id = s.Id,
                           componentName = s.AircraftComponent.ComponentName,
                           CompnentId = s.AircraftComponentId,
                           FileName = s.UserMannual,
                           ManufacturerName = s.AircraftComponentManufacturer.Name,
                           ComponentManufacturerId = s.AircraftComponentManufacturerId,
                           ModelName = s.Name
                       }).OrderBy(o => o.CompnentId).ThenBy(a => a.ComponentManufacturerId).ToList();


            }
            catch (Exception ex)
            {
                //logger.Fatal("Execption on Selecting the Component Model List " + ex);
                return new List<ComponentModel_ConfigSetting>();
            }

            return response;
        }


        public void UpdateFileSizeInJson()
        {
            var context = new GuardianAvionicsEntities();

            var objState = context.States.Select(s => s.Id).ToList();
            PlatesResponseModel objPaltesResponse = new PlatesResponseModel();
            var stateList = new List<PlateStates>();

            foreach (int stateId in objState)
            {
                objPaltesResponse.StateList = context.States.Where(s => s.Id == stateId).Select(s => new PlateStates
                {
                    Id = s.Id,
                    ShortName = s.ShortName,
                    State_Code_Id = s.State_Code_Id,
                    StateName = s.StateName,
                    CityList = s.Cities.Where(wc => wc.City_Name_Id != null).Select(c => new PlateCitys
                    {
                        City_Name_Id = c.City_Name_Id ?? 0,
                        CityName = c.CityName,
                        Id = c.Id,
                        State_Code_Id = c.State_Code_Id,
                        StateId = c.StateId,
                        Volume = c.Volume,
                        AirportList = c.Airports.Where(wa => wa.airport_name_Id != null).Select(a => new PlateAirPorts
                        {
                            airport_name_Id = a.airport_name_Id,
                            AirportName = a.AirportName,
                            apt_ident = a.apt_ident,
                            city_name_Id = a.city_name_Id,
                            CityId = a.CityId,
                            Country = a.Country,
                            icao_ident = a.icao_ident,
                            Id = a.Id,
                            Military = a.Military,
                            X = a.X,
                            Y = a.Y,
                            PlateList = a.Plates.Select(p => new PlateDetails
                            {
                                Airport_Name_Id = p.Airport_Name_Id,
                                AirportId = p.AirportId,
                                Chart_Code = p.Chart_Code,
                                Chart_Name = p.Chart_Name,
                                ChartSeq = p.ChartSeq,
                                Civil = p.Civil,
                                Copter = p.Copter,
                                Draw_Bottom = p.Draw_Bottom,
                                Draw_Left = p.Draw_Left,
                                Draw_Right = p.Draw_Right,
                                Draw_Top = p.Draw_Top,
                                ExcludeAreas = p.ExcludeAreas,
                                Faanfd15 = p.Faanfd15,
                                Faanfd18 = p.Faanfd18,
                                File_Name = ConfigurationReader.ServerUrl + "/Plates/" + p.File_Name,
                                Id = p.Id,
                                Invalid = Convert.ToBoolean(p.Invalid),
                                LastChanged = p.LastChanged,
                                Lat1 = p.Lat1,
                                Lat2 = p.Lat2,
                                Long1 = p.Long1,
                                Long2 = p.Long2,
                                Orientation = p.Orientation,
                                UserAction = p.UserAction,
                                ValidWithinRadius = p.ValidWithinRadius,
                                ValidWithinX = p.ValidWithinX,
                                ValidWithinY = p.ValidWithinY,
                                X1 = p.X1,
                                X2 = p.X2,
                                Y1 = p.Y1,
                                Y2 = p.Y2,
                                FileSize = Math.Round(((long)p.FileSize / 1024f) / 1024f, 6)
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

                System.IO.File.WriteAllText(ConfigurationReader.PlateFilePath + @"JsonFiles\" + stateId.ToString() + ".txt", Newtonsoft.Json.JsonConvert.SerializeObject(objPaltesResponse));
            }
        }

        public DashboardModel GetDashboardDetails()
        {
            DashboardModel objResponse = new DashboardModel
            {
                LastFlightsList = new List<DashBoardLastFlights>(),
                TopRegisterAircraftList = new List<DashboardTopRegisterAircraft>(),
                TopRegisterUserList = new List<DashBoardTopRegisterUser>()
            };


            var context = new GuardianAvionicsEntities();

            #region DashBoardUser

            objResponse.TotalUserRegistered = context.Profiles.Where(p => !p.Deleted).Count();

            var objUserProfile = context.Profiles.Where(o => o.UserTypeId != (int)Enumerations.UserType.Maintenance).OrderByDescending(o => o.Id).Take(5);

            if (objUserProfile != null)
            {
                objUserProfile.ToList().ForEach(s => objResponse.TopRegisterUserList.Add(new DashBoardTopRegisterUser
                {
                    CompanyName = s.UserDetail.CompanyName,
                    EmailId = s.EmailId,
                    Name = s.UserDetail.FirstName + " " + s.UserDetail.LastName,
                    ProfileId = s.Id,
                    ImagePath = String.IsNullOrEmpty(s.UserDetail.ImageUrl) ? Constants.UserProfileDefaultImage : ConfigurationReader.ImageServerPathKey + s.UserDetail.ImageUrl
                }));

                var profile = objUserProfile.FirstOrDefault();

                if (profile != null)
                {
                    objResponse.LastRegisterUserName = profile.UserDetail.FirstName + " " + profile.UserDetail.LastName;
                    objResponse.LastRegisterUserEmailId = profile.EmailId;
                    objResponse.LastRegisterUserCompanyName = profile.UserDetail.CompanyName;

                }
                else
                {
                    objResponse.LastRegisterUserName = "Not Available";
                    objResponse.LastRegisterUserEmailId = "Not Available";
                    objResponse.LastRegisterUserCompanyName = "Not Available";
                }

            }
            else
            {
                objResponse.LastRegisterUserName = "Not Available";
                objResponse.LastRegisterUserEmailId = "Not Available";
                objResponse.LastRegisterUserCompanyName = "Not Available";
            }
            #endregion DashBoardUser

            #region DashBoardAircraft
            objResponse.TotalAircraftRegistered = context.AircraftProfiles.Where(p => !p.Deleted).Count();

            var objAircraftProfile = context.AircraftProfiles.OrderByDescending(o => o.Id).Take(5);

            if (objAircraftProfile != null)
            {
                objAircraftProfile.ToList().ForEach(s => objResponse.TopRegisterAircraftList.Add(new DashboardTopRegisterAircraft
                {
                    AircraftId = s.Id,
                    OwnerName = s.Profile != null ? s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName : "Not Available",
                    SerialNo = s.AircraftSerialNo,
                    TailNo = s.Registration,
                    ImagePath = String.IsNullOrEmpty(s.ImageUrl) ? Constants.AircraftDefaultImage : ConfigurationReader.ImageServerPathKey + s.ImageUrl
                }));

                var aircraftProfile = objAircraftProfile.FirstOrDefault();

                if (aircraftProfile != null)
                {
                    objResponse.LastRegisterAircraftTailNo = aircraftProfile.Registration;
                    objResponse.LastRegisterAircraftMake = aircraftProfile.Make == null ? "" : aircraftProfile.AircraftManufacturer.Name;
                    objResponse.LastRegisterAircraftModel = aircraftProfile.Model == null ? "" : aircraftProfile.AircraftModelList.ModelName;
                }
                else
                {
                    objResponse.LastRegisterAircraftTailNo = "Not Available";
                    objResponse.LastRegisterAircraftMake = "Not Available";
                    objResponse.LastRegisterAircraftModel = "Not Available";
                }

            }
            else
            {
                objResponse.LastRegisterAircraftTailNo = "Not Available";
                objResponse.LastRegisterAircraftMake = "Not Available";
                objResponse.LastRegisterAircraftModel = "Not Available";
            }
            #endregion DashBoardAircraft

            #region DashBoardFlight
            objResponse.TotalFlights = context.PilotLogs.Where(p => p.Finished).Count();

            var objFlightLogs = context.PilotLogs.Where(p => p.Finished).OrderByDescending(o => o.Id).Take(100);

            if (objFlightLogs != null)
            {
                objFlightLogs.ToList().ForEach(s => objResponse.LastFlightsList.Add(new DashBoardLastFlights
                {
                    AircraftTailNo = s.AircraftProfile.Registration,
                    LogId = s.Id,
                    PilotName = s.PilotId == null ? "Not Available" : s.Profile.UserDetail.FirstName + " " + s.Profile.UserDetail.LastName,
                    ImagePath = String.IsNullOrEmpty(s.AircraftProfile.ImageUrl) ? Constants.AircraftDefaultImage : ConfigurationReader.ImageServerPathKey + s.AircraftProfile.ImageUrl,
                    Duration = Misc.ConvertMinToOneTenthOFTime(new Misc().ConvertHHMMToMinutes(s.DayPIC))
                }));

                var flightLog = objFlightLogs.FirstOrDefault();

                if (flightLog != null)
                {
                    objResponse.LastFlightByAircraft = flightLog.AircraftProfile.Registration;
                    objResponse.LastFlightByPilot = flightLog.PilotId == null ? "Not Available" : flightLog.Profile.UserDetail.FirstName + " " + flightLog.Profile.UserDetail.LastName;
                    objResponse.LastFlightDuration = Misc.ConvertMinToOneTenthOFTime(new Misc().ConvertHHMMToMinutes(flightLog.DayPIC));

                }
                else
                {
                    objResponse.LastFlightByAircraft = "Not Available";
                    objResponse.LastFlightByPilot = "Not Available";
                    objResponse.LastFlightDuration = "Not Available";
                }

            }
            else
            {
                objResponse.LastFlightByAircraft = "Not Available";
                objResponse.LastFlightByPilot = "Not Available";
                objResponse.LastFlightDuration = "Not Available";
            }
            #endregion DashBoardFlight

            #region AeroUnit

            var aeroUnits = context.AeroUnitMasters.ToList();

            objResponse.TotalUnits = aeroUnits.Count();
            objResponse.TotalLinkedUnit = aeroUnits.Where(a => a.AircraftId != null && !a.Blocked).ToList().Count();
            objResponse.TotalUnLinkedUnit = aeroUnits.Where(a => a.AircraftId == null).ToList().Count();
            var objAeroUnit = aeroUnits.Where(a => a.AircraftId != null && !a.Blocked).FirstOrDefault();
            if (objAeroUnit != null)
            {
                objResponse.LastUnitLinked = objAeroUnit.AeroUnitNo;
            }
            else
            {
                objResponse.LastUnitLinked = "Not Available";
            }
            #endregion AeroUnit



            return objResponse;

        }

        public List<FeatureMaster> GetFeatureList()
        {

            var context = new GuardianAvionicsEntities();
            return context.FeatureMasters.Where(w => w.IsEnable).ToList();

        }

        public int SetSubscription(AddSubscriptionViewModel subViewModel)
        {


            var context = new GuardianAvionicsEntities();

            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    var subMaster = context.SubscriptionMasters.Create();
                    int maxItemId = context.SubscriptionMasters.Max(m => m.SubItemId);
                    subMaster.SubItemId = maxItemId + 1;
                    subMaster.SubItemName = subViewModel.Name;
                    subMaster.SubDetails = subViewModel.Details;
                    subMaster.Status = true;
                    context.SubscriptionMasters.Add(subMaster);
                    context.SaveChanges();

                    var subPaymentMonthly = context.SubscriptionPaymentDefinitions.Create();
                    subPaymentMonthly.Amount = subViewModel.AmountMonthly;
                    subPaymentMonthly.SubscriptionItemId = subMaster.SubItemId;
                    subPaymentMonthly.TrialFrequency = "None";
                    subPaymentMonthly.RegularFrequency = "Monthly";
                    subPaymentMonthly.InAppProductId = subViewModel.InappProductIdMonthly;
                    subPaymentMonthly.PayPalProductId = subViewModel.PaypalItemIdMonthly;
                    subPaymentMonthly.PaypalButtonId = subViewModel.PaypalButtonIdMonthly;
                    subPaymentMonthly.Type = "Regular";
                    subPaymentMonthly.Frequency = "Weekly";
                    context.SubscriptionPaymentDefinitions.Add(subPaymentMonthly);
                    context.SaveChanges();

                    var subPaymentYearly = context.SubscriptionPaymentDefinitions.Create();
                    subPaymentYearly.Amount = subViewModel.AmountYearly;
                    subPaymentYearly.SubscriptionItemId = subMaster.SubItemId;
                    subPaymentYearly.TrialFrequency = "None";
                    subPaymentYearly.RegularFrequency = "Yearly";
                    subPaymentYearly.InAppProductId = subViewModel.InappProductIdYearly;
                    subPaymentYearly.PayPalProductId = subViewModel.PaypalItemIdYearly;
                    subPaymentYearly.PaypalButtonId = subViewModel.PaypalButtonIdYearly;
                    subPaymentYearly.Type = "Regular";
                    subPaymentYearly.Frequency = "Weekly";
                    context.SubscriptionPaymentDefinitions.Add(subPaymentYearly);
                    context.SaveChanges();
                    var mapSubAndFeatureList = subViewModel.FeatureList.Where(f => f.IsEnable == true).Select(s => new SubscriptionAndFeatureMapping
                    {
                        FeatureId = s.Id,
                        SubscriptionItemId = subMaster.SubItemId
                    }).ToList();

                    context.SubscriptionAndFeatureMappings.AddRange(mapSubAndFeatureList);
                    context.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return 2; //Exception
                }
                return 1;
            }

        }

        #region Errorlogs

        public List<ErrorLogModel> GetErrorLogs()
        {
            List<ErrorLogModel> errorLogList = new List<ErrorLogModel>();
            string ErrorLogConnectionString = System.Configuration.ConfigurationManager.AppSettings["ErrorLogConnection"];
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "serverName";
            param[0].SqlDbType = SqlDbType.NVarChar;
            param[0].Value = ConfigurationReader.ServerName;

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ErrorLogConnectionString, CommandType.StoredProcedure, "spGetErrorLogs", param);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    errorLogList = (from rw in ds.Tables[0].AsEnumerable()
                                    select new ErrorLogModel()
                                    {
                                        Id = Convert.ToInt32(rw["Id"]),
                                        RequestId = Convert.ToInt32(rw["RequestId"]),
                                        Response = rw["Response"].ToString(),
                                        Module = rw["Module"].ToString(),
                                        MethodName = rw["MethodName"].ToString(),
                                        ServerName = rw["ServerName"].ToString(),
                                        Date = Convert.ToDateTime(rw["Date"]),
                                        IsResolved = Convert.ToBoolean(rw["IsResolved"]),
                                        Remark = rw["Remark"].ToString()
                                    }).ToList();
                }
            }
            return errorLogList;
        }

        public List<ErrorLogModel> SearchErrorLog(string emailId, DateTime? startDate, DateTime? endDate, string status)
        {
            List<ErrorLogModel> errorLogList = new List<ErrorLogModel>();
            int profileId = 0;
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.EmailId == emailId);
            if (profile != null)
            {
                profileId = profile.Id;
            }
            string ErrorLogConnectionString = System.Configuration.ConfigurationManager.AppSettings["ErrorLogConnection"];
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter();
            param[0].ParameterName = "profileId";
            param[0].SqlDbType = SqlDbType.Int;
            param[0].Value = profileId;

            param[1] = new SqlParameter();
            param[1].ParameterName = "startDate";
            param[1].SqlDbType = SqlDbType.DateTime;
            param[1].Value = startDate;

            param[2] = new SqlParameter();
            param[2].ParameterName = "endDate";
            param[2].SqlDbType = SqlDbType.DateTime;
            param[2].Value = startDate;

            param[3] = new SqlParameter();
            param[3].ParameterName = "status";
            param[3].SqlDbType = SqlDbType.NVarChar;
            param[3].Value = status;

            param[4] = new SqlParameter();
            param[4].ParameterName = "server";
            param[4].SqlDbType = SqlDbType.NVarChar;
            param[4].Value = ConfigurationReader.ServerName;

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ErrorLogConnectionString, CommandType.StoredProcedure, "spSearchErrorLogs", param);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    errorLogList = (from rw in ds.Tables[0].AsEnumerable()
                                    select new ErrorLogModel()
                                    {
                                        Id = Convert.ToInt32(rw["Id"]),
                                        RequestId = Convert.ToInt32(rw["RequestId"]),
                                        Response = rw["Response"].ToString(),
                                        Module = rw["Module"].ToString(),
                                        MethodName = rw["MethodName"].ToString(),
                                        ServerName = rw["ServerName"].ToString(),
                                        Date = Convert.ToDateTime(rw["Date"]),
                                        IsResolved = Convert.ToBoolean(rw["IsResolved"]),
                                        Remark = rw["Remark"].ToString()
                                    }).ToList();
                }
            }
            return errorLogList;
        }

        public ErrorLogModel GetErrorLogById(int id)
        {
            ErrorLogModel errorLogModel = new ErrorLogModel();
            string ErrorLogConnectionString = System.Configuration.ConfigurationManager.AppSettings["ErrorLogConnection"];
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "id";
            param[0].SqlDbType = SqlDbType.Int;
            param[0].Value = id;

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ErrorLogConnectionString, CommandType.StoredProcedure, "spGetErrorLogById", param);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow datarow = ds.Tables[0].Rows[0] as DataRow;
                    errorLogModel.EncryptRequest = datarow["EncryptRequest"].ToString();
                    errorLogModel.JsonRequest = datarow["JsonRequest"].ToString();
                    errorLogModel.EncryptedResponse = datarow["EncryptedResponse"].ToString();
                    errorLogModel.JsonResponse = datarow["JsonResponse"].ToString();
                }
            }
            return errorLogModel;
        }

        #endregion Errorlogs
    }
}
