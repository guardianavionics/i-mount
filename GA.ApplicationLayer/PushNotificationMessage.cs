﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using GA.Common;
//using NLog;

namespace GA.ApplicationLayer
{
    public class PushNotificationMessage
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public bool PushToiPhone(string[] arrDeviceId, string alertMsg)
        {
            int port = 2195;
            String hostname = "gateway.sandbox.push.apple.com";

            //------- Without the last argument it works only locally, but if we deploy or publish it, this code doesn't work ------- !
            //X509Certificate2 clientCertificate = new X509Certificate2(Server.MapPath("~/ck.p12"), "moreenergyapn", X509KeyStorageFlags.MachineKeySet);

            var certificatePath = ConfigurationReader.CertificatePath;
            X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, "", X509KeyStorageFlags.MachineKeySet);

            X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

            TcpClient client = new TcpClient(hostname, port);

            SslStream sslStream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);

            try
            {
                sslStream.AuthenticateAsClient(hostname, certificatesCollection, System.Security.Authentication.SslProtocols.Tls, false);
            }
            catch (Exception e)
            {
                string exce = e.Message.ToString() + "-------" + e.StackTrace.ToString() + "-------" + e.InnerException.ToString();

                client.Close();

                return false;
            }

            foreach (var deviceId in arrDeviceId)
            {
                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0);  //------- The command
                writer.Write((byte)0);  //------- The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //------- The deviceId length (big-endian second byte)
                writer.Write(HexStringToByteArray(deviceId.ToUpper()));

                String payload = "{\"aps\":{\"alert\":\"" + alertMsg + "\",\"badge\":0,\"sound\":\"default\"}}";

                writer.Write((byte)0);
                writer.Write((byte)payload.Length);
                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();
                byte[] array = memoryStream.ToArray();
                sslStream.Write(array);
                sslStream.Flush();
            }
            client.Close();

            return true;
        }

        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static byte[] HexStringToByteArray(String s)
        {
            s = s.Replace(" ", "");

            byte[] buffer = new byte[s.Length / 2];

            for (int i = 0; i < s.Length; i += 2)
            {
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            }

            return buffer;
        }

    }
}

