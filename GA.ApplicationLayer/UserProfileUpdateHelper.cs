﻿using GA.Common;
using GA.DataLayer;
using GA.DataTransfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using NLog;
using System.Text;
namespace GA.ApplicationLayer
{
    public class UserProfileUpdateHelper
    {
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ProfileUpdateResponseModel UpdateUserProfile(ProfileUpdateRequestModel profileDetail)
        {

            var date = profileDetail.LastUpdateDateTime == null ? DateTime.MinValue : Misc.GetDate(profileDetail.LastUpdateDateTime);
            var lastUpdateDate = DateTime.UtcNow;
            var objProfile = new ProfileUpdateResponseModel
            {
                LastUpdateDate = Misc.GetStringOfDate(lastUpdateDate),
            };

            objProfile.IsUpdateAvailableForProfile = false;
            objProfile.IsUpdateAvailableForMedicalCertificate = false;
            objProfile.IsUpdateAvailableForLicenseRating = false;

            bool isUpdateAvailableOnServerForProfile = false;
            bool isUpdateAvailableOnServerForMedicalCert = false;
            bool isUpdateAvailableOnServerForLicense = false;

            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    StringBuilder str = new StringBuilder();
                    #region validations

                    // if no profileid do not match return error code.
                    if (String.IsNullOrEmpty(profileDetail.ProfileId))
                    {
                        return new ProfileUpdateResponseModel
                        {
                            ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.ProfileIdNotFound).ToString(),
                            ResponseMessage = Enumerations.UserProfileUpdateCodes.ProfileIdNotFound.GetStringValue()
                        };
                    }
                    var id = Convert.ToInt32(profileDetail.ProfileId);
                    var profile = context.Profiles.FirstOrDefault(p => p.Id == id && p.Deleted == false);

                    // if no profileid do not match return error code.
                    if (profile == default(Profile) || profile.Id == 0)
                    {
                        return new ProfileUpdateResponseModel
                        {
                            ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.ProfileIdNotFound).ToString(),
                            ResponseMessage = Enumerations.UserProfileUpdateCodes.ProfileIdNotFound.GetStringValue()
                        };
                    }

                    // if user is blocked
                    if (profile.IsBlocked)
                    {
                        return new ProfileUpdateResponseModel
                        {
                            ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty).ToString(),
                            ResponseMessage = Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty.GetStringValue()
                        };
                    }

                    #endregion validations

                    #region update user details

                    if (profile.LastUpdated > date)
                    {
                        isUpdateAvailableOnServerForProfile = true;
                    }
                    if (profile.LastUpdateDateForMedicalCertificate > date)
                    {
                        isUpdateAvailableOnServerForMedicalCert = true;
                    }
                    if (profile.LastUpdateDateForLicenseRating > date)
                    {
                        isUpdateAvailableOnServerForLicense = true;
                    }

                    // update the user data
                    var userDetail = context.UserDetails.FirstOrDefault(u => u.ProfileId == profile.Id);
                    if (profileDetail.IsUpdateAvailableForProfile)
                    {
                        if (userDetail != default(UserDetail))
                        {
                            // take a variable and save current date time in it . then save this date time in all the modified
                            // values and also return this date time only.
                            if (!String.IsNullOrEmpty(profileDetail.FirstName) && !String.IsNullOrEmpty(profileDetail.LastName))
                            {
                                if (string.IsNullOrEmpty(profileDetail.EmailId))
                                {
                                    return new ProfileUpdateResponseModel
                                    {
                                        ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.UserBlocked).ToString(),
                                        ResponseMessage = Enumerations.UserProfileUpdateCodes.UserBlocked.GetStringValue()
                                    };
                                }
                                //check for email changed or not
                                if (profileDetail.EmailId != profile.EmailId)
                                {
                                    //User wants to update there email also
                                    //Now check that other wether the new email id already exist or not
                                    var newProfile = context.Profiles.FirstOrDefault(p => p.EmailId == profileDetail.EmailId);
                                    if (newProfile != null)
                                    {
                                        //email already belongs to new user
                                        return new ProfileUpdateResponseModel
                                        {
                                            ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.EmailIdAlreadyExistWithOtherProfile).ToString(),
                                            ResponseMessage = Enumerations.UserProfileUpdateCodes.EmailIdAlreadyExistWithOtherProfile.GetStringValue()
                                        };
                                    }
                                }
                                str.Append("First name and last name is not null ");
                                userDetail.FirstName = profileDetail.FirstName;
                                userDetail.LastName = profileDetail.LastName;
                                userDetail.PhoneNumber = profileDetail.PhoneNumber;

                                if (!string.IsNullOrEmpty(profileDetail.DateOfBirth))
                                {
                                    // there will be no case in which user wants to update previously saved date to null or string.Empty
                                    // save date in db
                                    userDetail.DateOfBirth = Misc.GetDate(profileDetail.DateOfBirth);
                                }

                                profile.EmailId = profileDetail.EmailId;
                                profile.LastUpdated = DateTime.UtcNow;
                                str.Append("Zip code passed profileDetail.ZipCode = " + profileDetail.ZipCode);
                                userDetail.StreetAddress = profileDetail.StreetAddress;
                                userDetail.City = profileDetail.City;
                                userDetail.State = profileDetail.State;
                                userDetail.ZipCode = profileDetail.ZipCode;
                                userDetail.CompanyName = profileDetail.CompanyName;

                                #region pushNotification

                                var tokenList = context.PushNotifications.Where(pn => pn.TokenId == profileDetail.OldTokenId || pn.TokenId == profileDetail.NewTokenId).ToList();

                                if (tokenList.Count > 0)
                                {
                                    context.PushNotifications.RemoveRange(tokenList);
                                }

                                if (!string.IsNullOrEmpty(profileDetail.NewTokenId))
                                {
                                    var isOn = profileDetail.IsPushNotificationOn == "1" ? true : false;
                                    context.PushNotifications.Add(CommonHelper.CreatePushNotificationObject(profile.Id, profileDetail.NewTokenId, isOn));
                                }
                                try
                                {
                                    context.SaveChanges();
                                }
                                catch (Exception e)
                                {

                                    // cause of exception 1) constraint : TokenId is not Unique
                                }

                                #endregion pushNotification

                                context.SaveChanges();
                                str.Append("DataSaved");
                            }

                            if (profileDetail.ImageOption == Enumerations.ImageOptions.SaveImage.GetStringValue())
                            {
                                if (!string.IsNullOrEmpty(userDetail.ImageUrl))
                                {
                                    new Misc().DeleteFileFromS3Bucket(userDetail.ImageUrl, "Image");
                                }
                                if (!string.IsNullOrEmpty(profileDetail.ImageName))
                                {
                                    // assuming it is base64 encoded image
                                    try
                                    {
                                        byte[] bytes = Convert.FromBase64String(profileDetail.ImageName);
                                        string path = ConfigurationReader.ImagePathKey;
                                        string fileName = new UserHelper().GenerateUniqueImageNameUser(profileDetail.ProfileId); // GenerateUniqueImageName();
                                        string fullOutputPath = path + "/" + fileName;
                                        using (var imageFile = new FileStream(fullOutputPath, FileMode.Create))
                                        {
                                            imageFile.Write(bytes, 0, bytes.Length);
                                            imageFile.Flush();
                                        }
                                        new Misc().UploadFile(fullOutputPath, fileName, "Image");
                                        File.Delete(fullOutputPath);
                                        userDetail.ImageUrl = fileName;
                                    }
                                    catch (Exception eee)
                                    {
                                        userDetail.ImageUrl = string.Empty;
                                        // either save or return error
                                    }
                                }
                                else
                                {
                                    userDetail.ImageUrl = string.Empty;
                                }

                            }
                            else if (profileDetail.ImageOption == Enumerations.ImageOptions.DeleteImage.GetStringValue())
                            {

                                if (!string.IsNullOrEmpty(userDetail.ImageUrl))
                                {
                                    new Misc().DeleteFileFromS3Bucket(userDetail.ImageUrl, "Image");
                                }
                                userDetail.ImageUrl = string.Empty;
                            }
                            context.SaveChanges();
                        }
                    }
                    else if (isUpdateAvailableOnServerForProfile)
                    {
                        objProfile.IsUpdateAvailableForProfile = true;
                        // send user profile image
                        if (!String.IsNullOrEmpty(userDetail.ImageUrl))
                        {
                            objProfile.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + userDetail.ImageUrl;
                        }
                        else
                        {
                            objProfile.ImageName = userDetail.ImageUrl ?? string.Empty;
                        }

                        str.Append(" userDetail.ZipCode = " + userDetail.ZipCode);
                        objProfile.PhoneNumber = userDetail.PhoneNumber;
                        objProfile.FirstName = userDetail.FirstName;
                        objProfile.LastName = userDetail.LastName;
                        objProfile.DateOfBirth = Misc.GetStringOnlyDate(userDetail.DateOfBirth);
                        objProfile.StreetAddress = userDetail.StreetAddress;
                        objProfile.City = userDetail.City;
                        objProfile.State = userDetail.State;
                        objProfile.ZipCode = userDetail.ZipCode;
                        objProfile.CompanyName = userDetail.CompanyName;
                        objProfile.EmailId = profile.EmailId;
                    }
                    if (!String.IsNullOrEmpty(userDetail.ImageUrl))
                    {
                        objProfile.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + userDetail.ImageUrl;
                    }
                    else
                    {
                        objProfile.ImageName = userDetail.ImageUrl ?? string.Empty;
                    }

                    // logger.Fatal(str.ToString());

                    #endregion update user details

                    #region deletion

                    //delete all the deleted medical certificates
                    var medCertList = new List<MedicalCertificate>();
                    if (profileDetail.DeletedMedicalCertificate != null)
                    {
                        if (profileDetail.DeletedMedicalCertificate.Length > 0)
                        {
                            profile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                            isUpdateAvailableOnServerForMedicalCert = true;

                            foreach (var medCer in profileDetail.DeletedMedicalCertificate)
                            {
                                var objMedtbl = context.MedicalCertificates.FirstOrDefault(m => m.Id == medCer);

                                if (objMedtbl != default(MedicalCertificate))
                                {
                                    medCertList.Add(objMedtbl);
                                }
                            }
                            if (medCertList.Count > 0)
                            {
                                context.MedicalCertificates.RemoveRange(medCertList);
                                context.SaveChanges();
                            }
                        }
                    }


                    //delete ratings

                    var ratingList = new List<Rating>();
                    if (profileDetail.DeletedRatings.Length > 0)
                    {
                        isUpdateAvailableOnServerForLicense = true;
                        profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                    }
                    foreach (var rating in profileDetail.DeletedRatings)
                    {
                        var objRatingTbl = context.Ratings.FirstOrDefault(r => r.Id == rating);
                        if (objRatingTbl != default(Rating))
                        {
                            ratingList.Add(objRatingTbl);

                        }
                    }
                    if (ratingList.Count > 0)
                    {
                        context.Ratings.RemoveRange(ratingList);
                        context.SaveChanges();
                    }


                    //delete lisenses
                    if (profileDetail.DeletedLicences.Length > 0)
                    {
                        isUpdateAvailableOnServerForLicense = true;
                        profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                    }
                    foreach (var license in profileDetail.DeletedLicences)
                    {
                        var objLicenseTbl = context.Licenses.FirstOrDefault(l => l.Id == license);

                        if (objLicenseTbl != default(License))
                        {
                            ratingList = context.Ratings.Where(r => r.LicenseId == license).ToList();
                            if (ratingList.Count > 0)
                            {
                                context.Ratings.RemoveRange(ratingList);
                            }
                            context.Licenses.Remove(objLicenseTbl);
                            context.SaveChanges();
                        }
                    }

                    #endregion deletion

                    #region medical certificate

                    // list of all the new added medical certificate sent by user , it has to be again send to the user with there id's.
                    //update and add , medical certificates

                    if (profileDetail.MedicalCertificateList.Count > 0)
                    {
                        isUpdateAvailableOnServerForMedicalCert = true;
                        profile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                    }
                    foreach (var medCer in profileDetail.MedicalCertificateList)
                    {
                        var medCerTbl = context.MedicalCertificates.FirstOrDefault(mc => mc.UniqueId == medCer.UniqueId);

                        if (medCerTbl == default(MedicalCertificate))
                        //if (medCer.Id == 0)
                        {
                            // create
                            // add certificate
                            var med =
                                new MedicalCertificate
                                {
                                    Deleted = false,
                                    LastUpdated = lastUpdateDate,
                                    ProfileId = profile.Id,
                                    ReferenceNumber = medCer.ReferenceNumber,
                                    Type = medCer.Type,
                                    ValidUntil = Misc.GetDate(medCer.ValidUntil),
                                    UniqueId = medCer.UniqueId,
                                    MedicalCertClassId = medCer.ClassId,
                                };

                            context.MedicalCertificates.Add(med);
                            context.SaveChanges();

                            // Add to NewAddedMedCer to send in Response.

                        }
                        else
                        {
                            //update certificate
                            if (!medCerTbl.Deleted)
                            {
                                // update
                                medCerTbl.Type = medCer.Type;
                                medCerTbl.ValidUntil = Misc.GetDate(medCer.ValidUntil);
                                medCerTbl.ReferenceNumber = medCer.ReferenceNumber;
                                medCerTbl.LastUpdated = lastUpdateDate;
                                medCerTbl.MedicalCertClassId = medCer.ClassId;
                                context.SaveChanges();
                            }
                        }
                    }

                    #endregion medical certificate

                    #region license

                    // update and add , licenses
                    if (profileDetail.LicensesList.Count > 0)
                    {
                        isUpdateAvailableOnServerForLicense = true;
                        profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                    }
                    foreach (var licenses in profileDetail.LicensesList)
                    {
                        var objlicenseTbl = context.Licenses.FirstOrDefault(li => li.UniqueId == licenses.UniqueId);

                        if (objlicenseTbl == default(License))
                        {
                            // create license
                            var licenseObj =
                                new License
                                {
                                    Deleted = false,
                                    LastUpdated = lastUpdateDate,
                                    ProfileId = profile.Id,
                                    ReferenceNumber = licenses.ReferenceNumber,
                                    Type = licenses.Type,
                                    ValidUntil = Misc.GetDate(licenses.ValidUntil),
                                    Ratings = new List<Rating>(),
                                    UniqueId = licenses.UniqueId,
                                };

                            context.Licenses.Add(licenseObj);
                            // we need to add ratings for this license so saving it
                            context.SaveChanges();

                            // create ratings which are not in ratings table (previously created)
                            foreach (var rating in licenses.Ratingslist.Where(r => !(context.Ratings.Any(ra => ra.UniqueId == r.UniqueId))))
                            {
                                var rate = new Rating
                                {
                                    Deleted = false,
                                    LastUpdated = lastUpdateDate,
                                    Type = rating.Type,
                                    ValidUntil = Misc.GetDate(rating.ValidUntil),
                                    LicenseId = licenseObj.Id,
                                    UniqueId = rating.UniqueId,
                                };

                                licenseObj.Ratings.Add(rate);
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            //update license


                            //var objlicenseTbl = context.Licenses.FirstOrDefault(l => l.Id == licenses.Id && l.Deleted == false);

                            if (!objlicenseTbl.Deleted) // != default(License)
                            {
                                objlicenseTbl.ReferenceNumber = licenses.ReferenceNumber;
                                objlicenseTbl.Type = licenses.Type;
                                objlicenseTbl.ValidUntil = Misc.GetDate(licenses.ValidUntil);
                                objlicenseTbl.LastUpdated = lastUpdateDate;
                                context.SaveChanges();

                                #region update ratings

                                // update each rating of the license
                                foreach (var rate in licenses.Ratingslist)
                                {
                                    //var objRateTbl = objlicenseTbl.Ratings.FirstOrDefault(a => a.Id == rate.Id && a.Deleted == false);
                                    var objRateTbl = context.Ratings.FirstOrDefault(r => r.UniqueId == rate.UniqueId);

                                    if (objRateTbl == default(Rating))
                                    {
                                        // create rating
                                        var newRating = new Rating();
                                        newRating.LastUpdated = lastUpdateDate;
                                        newRating.LicenseId = objlicenseTbl.Id;
                                        newRating.Type = rate.Type;
                                        newRating.ValidUntil = Misc.GetDate(rate.ValidUntil);
                                        newRating.UniqueId = rate.UniqueId;
                                        context.Ratings.Add(newRating);
                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        // update rating

                                        //var objRateTbl = objlicenseTbl.Ratings.FirstOrDefault(a => a.Id == rate.Id && a.Deleted == false);

                                        if (!objRateTbl.Deleted)
                                        {
                                            objRateTbl.Type = rate.Type;
                                            objRateTbl.ValidUntil = Misc.GetDate(rate.ValidUntil);
                                            objRateTbl.LastUpdated = lastUpdateDate;
                                            context.SaveChanges();
                                        }
                                    }
                                }//for loop for adding ratings

                                #endregion update ratings

                            } // in update license case
                        }
                    }

                    #endregion license

                    #region setting response

                    var NewAddedMedCer = new List<MedicalCertificateModel>();

                    if (isUpdateAvailableOnServerForMedicalCert)
                    {
                        objProfile.IsUpdateAvailableForMedicalCertificate = true;
                        var medCerListToSend = context.MedicalCertificates.Where(l => l.Deleted == false && l.ProfileId == id).ToList();

                        NewAddedMedCer.AddRange(medCerListToSend.Select(med => new MedicalCertificateModel
                        {
                            Id = med.Id,
                            ReferenceNumber = med.ReferenceNumber,
                            Type = med.Type,
                            ValidUntil = Misc.GetStringOnlyDate(med.ValidUntil),
                            UniqueId = med.UniqueId ?? default(long),
                            ClassId = med.MedicalCertClassId
                        }));
                    }
                    var NewAddedLicense = new List<LicensesModel>();

                    if (isUpdateAvailableOnServerForLicense)
                    {
                        objProfile.IsUpdateAvailableForLicenseRating = true;
                        var licenseListToSend = context.Licenses.Where(l => l.Deleted == false && l.ProfileId == id).ToList();

                        foreach (License lic in licenseListToSend)
                        {
                            var licenseModel = new LicensesModel
                            {
                                Id = lic.Id,
                                ReferenceNumber = lic.ReferenceNumber,
                                Type = lic.Type,
                                ValidUntil = Misc.GetStringOnlyDate(lic.ValidUntil),
                                Ratingslist = new List<RatingModel>(),
                                UniqueId = lic.UniqueId ?? default(long),
                            };

                            foreach (Rating rate in lic.Ratings)
                            {
                                licenseModel.Ratingslist.Add(
                                new RatingModel
                                {
                                    Id = rate.Id,
                                    Type = rate.Type,
                                    ValidUntil = Misc.GetStringOnlyDate(rate.ValidUntil),
                                    UniqueId = rate.UniqueId ?? default(long),
                                });

                            }
                            NewAddedLicense.Add(licenseModel);
                        }
                    }
                    objProfile.LicensesList = NewAddedLicense;
                    objProfile.MedicalCertificateList = NewAddedMedCer;
                    objProfile.ProfileId = profile.Id;

                    objProfile.Subscription = new DataTransfer.Classes_for_Services.ValidateReceiptSubscriptionModel();

                    if (profile.CurrentSubscriptionId != null)
                    {
                        objProfile.IsSubscriptionVisible = profile.IsSubscriptionVisible ?? false;
                        var lastSub = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                        var pendingRenewInfo = context.PendingRenewalInfoes.FirstOrDefault(f => f.MapSubAndUserId == lastSub.Id);

                        var isUserCurrSubActive = new SubscriptionHelper().IsSubscriptionActive(lastSub.Id);
                        //objProfile.Subscription.AutoRenewStatus = isUserCurrSubActive;
                        var payDefination = lastSub.SubscriptionPaymentDefinition;
                        objProfile.Subscription.SubscriptionId = payDefination.Id;
                        objProfile.Subscription.ProductId = payDefination.InAppProductId;
                        objProfile.Subscription.PlanName = payDefination.SubscriptionMaster.SubItemName;
                        objProfile.Subscription.Detail = payDefination.SubscriptionMaster.SubDetails;
                        objProfile.Subscription.PurchaseDate = Misc.GetStringOfDate(lastSub.InappPurchaseDate);
                        objProfile.Subscription.IsTrialPeriod = lastSub.IsInTrialMode ?? true;
                        //if (inAppAutoRenewStatus == true || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Active || (lastSub.IsInTrialMode == true && lastSub.Status == true))
                        if (isUserCurrSubActive)
                        {
                            objProfile.Subscription.FeatureList = new List<FeatureMaster>();
                            var featureList = context.SubscriptionAndFeatureMappings.Where(w => w.SubscriptionItemId == lastSub.SubscriptionPaymentDefinition.SubscriptionItemId).Select(s => s.FeatureId).ToList();
                            objProfile.Subscription.FeatureList = context.FeatureMasters.Where(w => featureList.Contains(w.Id)).ToList();
                            objProfile.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                            objProfile.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                            objProfile.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                            objProfile.Subscription.StatusMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                        }
                        if (lastSub.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
                        {
                            objProfile.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.InappPurchase;
                            objProfile.Subscription.ExpirationDate = Misc.GetStringOfDate(lastSub.InappReceipts.OrderByDescending(o => o.ExpireDate).FirstOrDefault().ExpireDate);
                            //if (pendingRenewInfo.AutoRenewStatus == true)
                            //{
                            //    objProfile.Subscription.FeatureList = new List<FeatureMaster>();
                            //    var featureList = context.SubscriptionAndFeatureMappings.Where(w => w.SubscriptionItemId == lastSub.SubscriptionPaymentDefinition.SubscriptionItemId).Select(s => s.FeatureId).ToList();
                            //    objProfile.Subscription.FeatureList = context.FeatureMasters.Where(w => featureList.Contains(w.Id)).ToList();
                            //    objProfile.ResponseCode = "0";
                            //    objProfile.ResponseMessage = "Success";
                            //    objProfile.Subscription.StatusCode = "0";
                            //    objProfile.Subscription.StatusMessage = "Success";
                            //}
                            if (isUserCurrSubActive == false)
                            {
                                objProfile.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                objProfile.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                objProfile.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                objProfile.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                objProfile.Subscription.InAppStatusCode = Convert.ToString(lastSub.PendingRenewalInfoes.FirstOrDefault().ExpirationIntent);
                            }
                        }
                        else if (lastSub.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
                        {
                            var paypalReceipt = lastSub.SubscriptionTransactions.OrderByDescending(o => o.PaymentDate).FirstOrDefault();
                            if (paypalReceipt != null)
                            {
                                switch (lastSub.SubscriptionPaymentDefinition.RegularFrequency)
                                {
                                    case "Monthly":
                                        {
                                            objProfile.Subscription.ExpirationDate = Misc.GetStringOfDate(paypalReceipt.PaymentDate.Value.AddMonths(1));
                                            break;
                                        }
                                    case "Yearly":
                                        {
                                            objProfile.Subscription.ExpirationDate = Misc.GetStringOfDate(paypalReceipt.PaymentDate.Value.AddYears(1));
                                            break;
                                        }
                                }
                            }
                            objProfile.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.Paypal;
                            // if (lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Cancelled || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Suspended || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Completed)
                            if (isUserCurrSubActive == false)
                            {
                                objProfile.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                objProfile.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                objProfile.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                objProfile.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                objProfile.Subscription.InAppStatusCode = "1";

                            }
                        }
                        else
                        {
                            objProfile.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.Free;
                            objProfile.Subscription.ExpirationDate = Misc.GetStringOfDate(new SubscriptionHelper().GetExpiryDateForFreePlan(lastSub.InappPurchaseDate));
                            if (isUserCurrSubActive == false)
                            {
                                objProfile.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                objProfile.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                objProfile.Subscription.InAppStatusCode = "1";
                            }
                            else
                            {
                                objProfile.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                objProfile.Subscription.StatusMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                            }
                            objProfile.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                            objProfile.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                        }
                    }
                    else
                    {
                        objProfile.Subscription = null;
                    }
                    objProfile.ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.Success).ToString();
                    objProfile.ResponseMessage = Enumerations.UserProfileUpdateCodes.Success.GetStringValue();

                    return objProfile;

                    #endregion setting response
                }
            }
            catch (Exception ex)
            {
                objProfile.ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.Exception).ToString();
                objProfile.ResponseMessage = Enumerations.UserProfileUpdateCodes.Exception.GetStringValue();
                return objProfile;
            }
        }

        string GenerateUniqueImageName()
        {
            string fileName = "gaImage_" + DateTime.UtcNow.Ticks + ".png";
            return fileName;
        }

    }

}