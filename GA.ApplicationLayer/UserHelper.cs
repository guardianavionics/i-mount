﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using GA.DataLayer;
using GA.DataTransfer;
using System.IO;
using GA.Common;
using System.Text.RegularExpressions;
using ImageResizer;
using GA.DataTransfer.Classes_for_Services;
using GA.DataTransfer.Classes_for_Web;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using EntityFramework.BulkInsert.Extensions;
using System.Data.SqlTypes;
using Newtonsoft.Json;
//using NLog;
//using NLog;
using WebGrease.Css.Extensions;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Security.Cryptography;
using GA.CommonForParseDataFile;
using Microsoft.ApplicationBlocks.Data;
using System.Threading.Tasks;
using PayPalSDK;
using Newtonsoft.Json.Linq;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;

namespace GA.ApplicationLayer
{
    /// <summary>
    /// All the fuctions related to user login , profile , pilot log
    /// </summary>
    public class UserHelper
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["DataBaseConnection"];
        //private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        bool isNewFlight = true;
        int rowCount = 0;
        List<DataLogWithoutUnit> objDatalogList;

        public DataTable dt_AirframeDataLog;
        bool isFinished = false;


        /// <summary>
        /// Test service connection
        /// </summary>
        /// <returns></returns>
        public string TestConnection()
        {
            return Boolean.TrueString;
        }


        #region registration and reset password

        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="userModel">User details</param>
        /// <returns>RegistrationResponseModel object</returns>
        public RegistrationResponseModel RegisterUser(UserModel userModel, bool isRegisterByWeb)
        {

            var registrationResponse = new RegistrationResponseModel();

            if (!isRegisterByWeb)
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new RegistrationResponseModel
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };
            }
            using (var context = new GuardianAvionicsEntities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        registrationResponse = Validate(userModel, isRegisterByWeb);
                        if (registrationResponse.ResponseCode != ((int)Enumerations.RegistrationReturnCodes.Success).ToString())
                            return registrationResponse;

                        if (context.Profiles.Any(p => String.Equals(p.EmailId.ToLower(), userModel.EmailId.ToLower())))
                        {
                            // do not register
                            return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.DuplicateEmailId);
                        }

                        // deleted, datecreated, lastupdated will have the default values at the time of creation
                        var profile = CommonHelper.GetProfileObject();
                        var userDetail = new UserDetail();

                        int userTypeId = context.UserMasters.FirstOrDefault(f => f.UserType == userModel.UserType).Id;
                        userModel.EmailId = userModel.EmailId.ToLower();
                        profile.EmailId = userModel.EmailId;
                        profile.Password = new Misc().Encrypt(userModel.Password);
                        profile.ThirdPartyId = null;
                        profile.UserTypeId = userTypeId;
                        profile.LastUpdated = DateTime.UtcNow;
                        profile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                        profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                        profile.RecordId = (new Encryption().Encrypt((userModel.Password + userModel.EmailId).ToLower(), Constants.GlobalEncryptionKey)).Substring(0, 16);

                        profile.IsSubscriptionVisible = (ConfigurationReader.IsSubscriptionEnable == Boolean.TrueString) ? true : false;
                        context.Profiles.Add(profile);

                        context.SaveChanges();

                        userDetail.City = userModel.City;
                        userDetail.FirstName = userModel.FirstName;
                        userDetail.LastName = userModel.LastName;
                        userDetail.PhoneNumber = userModel.PhoneNumber;
                        userDetail.CountryCode = userModel.CountryCode;
                        userDetail.ProfileId = profile.Id;
                        userDetail.SecurityAnswer = userModel.SecurityAnswer;
                        userDetail.State = userModel.State;
                        userDetail.StreetAddress = userModel.StreetAddress;
                        userDetail.SecurityQuestionId = userModel.SecurityQuestionId;

                        userDetail.DateOfBirth = Misc.GetDate(userModel.DateOfBirth);
                        userDetail.CompanyName = userModel.CompanyName;

                        if (userModel.IsRegisterByAdmin)
                        {
                            var obj = context.MappingAircraftManufacturerAndUsers.Create();
                            obj.AircraftComponentManufacturerId = userModel.ManufacturerId ?? 0;
                            obj.ProfileId = profile.Id;
                            context.MappingAircraftManufacturerAndUsers.Add(obj);
                        }
                        else
                        {
                            string message = "<p>Congratulation, new user has been registered on " + ConfigurationReader.ServerUrl + ". </p> <br/> <table border=\"1\" style=\"width:50%\"><tr><td>Email Id</td><td>" + userModel.EmailId + "</td> </tr>  <tr>   <td>First Name</td>  <td>" + userModel.FirstName + "</td></tr><tr><td>Last Name</td> <td>" + userModel.LastName + "</td></tr> <tr><td>Address</td><td>" + userModel.StreetAddress + "</td></tr> <tr> <td>City</td> <td>" + userModel.City + "</td> </tr> <tr> <td>State</td> <td>" + userModel.State + "</td> </tr><tr> <td>Phone Number</td> <td>" + userModel.PhoneNumber + "</td> </tr><tr> <td>Company Name</td><td>" + userModel.CompanyName + "</td> </tr></table>";
                            Misc.SendEmailCommon("<p>Date - " + DateTime.Now.ToString() + "</p>" + message, "User Registration");
                        }

                        int zip;
                        if (Int32.TryParse(userModel.ZipCode, out zip))
                            userDetail.ZipCode = zip;

                        context.UserDetails.Add(userDetail);
                        context.SaveChanges();

                        var userPreference = CommonHelper.GetPreference();
                        userPreference.ProfileId = profile.Id;
                        context.Preferences.Add(userPreference);
                        context.SaveChanges();
                        transaction.Commit();
                        SetSubscriptionToUserForFreeUse(profile.Id);

                        return registrationResponse.Create(profile.Id, Enumerations.RegistrationReturnCodes.Success); //aircraftProfile.Id, aircraftProfile.UniqueId ?? default(long), 
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.Exception);
                    }
                }
            }

        }

        /// <summary>
        /// Validates user profile for regiteration
        /// </summary>
        /// <param name="userModel">User details</param>
        /// <returns>RegistrationResponseModel object</returns>
        RegistrationResponseModel Validate(UserModel userModel, bool isRegisterByWeb)
        {
            var registrationResponse = new RegistrationResponseModel();
            if (string.IsNullOrEmpty(userModel.EmailId))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.EmailIdNotFound);
            }

            if (!Regex.IsMatch(userModel.EmailId, @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                                                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.EmailIdInWrongFormat);
            }

            if (string.IsNullOrEmpty(userModel.Password))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.PasswordNotFound);
            }

            // password length should greater then 5 digits

            if (userModel.Password.Length < 6)
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.PasswordShortInLength);
            }

            if (string.IsNullOrEmpty(userModel.FirstName))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.FirstNameNotFound);
            }

            if (string.IsNullOrEmpty(userModel.LastName))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.LastNameNotFound);
            }

            if (string.IsNullOrEmpty(userModel.SecurityAnswer))
            {
                return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.SecurityAnswerNotFound);
            }

            //if (string.IsNullOrEmpty(userModel.AircraftTailNo) && !isRegisterByWeb)
            //{
            //    return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.AircraftTailNoNotFound);
            //}

            return registrationResponse.Create(0, Enumerations.RegistrationReturnCodes.Success);
        }


        /// <summary>
        /// Sends email containing password of the user , if the email Id exists
        /// </summary>
        /// <param name="userDetails">Object of  ResetPasswordModel</param>
        /// <returns>GeneralResponse objcet</returns>
        public GeneralResponse ResetPassword(ResetPasswordModel userDetails)
        {
            var response = new GeneralResponse();

            try
            {


                if (string.IsNullOrEmpty(userDetails.EmailId))
                {
                    response.ResponseCode = ((int)Enumerations.ResetPassword.EmailIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.ResetPassword.EmailIdNotFound.GetStringValue();
                    return response;
                }

                using (var context = new GuardianAvionicsEntities())
                {
                    var profile = context.Profiles.FirstOrDefault(p => p.EmailId == userDetails.EmailId && !p.Deleted);

                    if (profile == default(Profile))
                    {
                        response.ResponseCode = ((int)Enumerations.ResetPassword.ProfileDoNotExist).ToString();
                        response.ResponseMessage = Enumerations.ResetPassword.ProfileDoNotExist.GetStringValue();
                        return response;
                    }

                    //string message = "Hi " + profile.UserDetail.FirstName + "," + "\n password of your account at " + ConfigurationReader.ServerUrl + " is :" + new Misc().Decrypt(profile.Password);

                    //Misc.SendEmail(userDetails.EmailId, message);

                    string emailResponse = SemdMailForResetPassword(userDetails.EmailId);
                    if (emailResponse == "Your account is blocked. Please contact support@guardianavionicsdata.com." || emailResponse == "Email Id is not registered" || emailResponse == "Error !")
                    {
                        response.ResponseCode = ((int)Enumerations.ResetPassword.Exception).ToString();
                        response.ResponseMessage = Enumerations.ResetPassword.Exception.GetStringValue();
                    }
                    else
                    {

                    }
                    response.ResponseCode = ((int)Enumerations.ResetPassword.Success).ToString();
                    response.ResponseMessage = Enumerations.ResetPassword.Success.GetStringValue();
                    return response;
                }
            }
            catch (Exception ex)
            {

                response.ResponseCode = ((int)Enumerations.ResetPassword.Exception).ToString();
                response.ResponseMessage = Enumerations.ResetPassword.Exception.GetStringValue();
                return response;
            }
        }

        /// <summary>
        /// Checks the security question
        /// </summary>
        /// <param name="Answer">Answer</param>
        /// <param name="profileId">profile Id</param>
        /// <returns>True or false</returns>
        public bool CheckSecurityQuestion(string Answer, int profileId)
        {
            throw new NotImplementedException();
        }

        public bool checkEmailExist(string emailId)
        {
            bool IsEmailExist = true;
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(pr => pr.EmailId == emailId);
            if (profile == null)
            {
                IsEmailExist = false;
            }

            return IsEmailExist;
        }

        /// <summary>
        /// Checks user's email id and sends email on that email id contining password of account
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public bool CheckEmailId(string Email)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var profile = context.Profiles.FirstOrDefault(pr => pr.EmailId == Email);

                    if (profile == default(Profile))
                    { return false; }
                    var messageBody = "Hi \n password of your account at " + ConfigurationReader.ServerUrl + " is :" + new Misc().Decrypt(profile.Password);

                    //send password through email
                    Misc.SendEmail(profile.EmailId, messageBody);
                    return true;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(Email), e);
                return false;
            }

        }


        public string ValidateResetPasswordUrl(string id)
        {
            string message = string.Empty;
            var context = new GuardianAvionicsEntities();
            try
            {
                var objResetPassword = context.ResetPasswords.FirstOrDefault(p => p.UniqueCode == id && !p.Deleted);
                if (objResetPassword != null)
                {
                    if (objResetPassword.IsUniqueCodeUsed ?? false)
                    {
                        message = "The link sent in the email cannot not be used multiple times.";
                    }
                    else
                    {
                        if (objResetPassword.DateCreated.AddDays(2) >= DateTime.UtcNow)
                        {
                            message = "Success," + objResetPassword.Profile.EmailId;
                        }
                        else
                        {
                            message = "Url expired to reset the password.";
                        }
                    }
                }
                else
                {
                    message = "Url expired to reset the password.";
                }
            }
            catch (Exception)
            {
                message = "Error !. Please Contact administrator";
            }
            return message;
        }

        public string SemdMailForResetPassword(string userEmail)
        {
            string response = string.Empty;
            var context = new GuardianAvionicsEntities();
            try
            {
                var objProfile = context.Profiles.FirstOrDefault(p => p.EmailId == userEmail);
                if (objProfile != null)
                {
                    var resetPasswordList = context.ResetPasswords.Where(w => w.ProfileId == objProfile.Id);
                    if (resetPasswordList != null)
                    {
                        context.ResetPasswords.RemoveRange(resetPasswordList);
                        context.SaveChanges();

                    }

                    if (!objProfile.Deleted)
                    {
                        ResetPassword objResetPassword = new ResetPassword();
                        objResetPassword.DateCreated = DateTime.UtcNow;
                        objResetPassword.ProfileId = objProfile.Id;
                        objResetPassword.Deleted = false;
                        objResetPassword.UniqueCode = Misc.RandomString(15);

                        context.ResetPasswords.Add(objResetPassword);
                        context.SaveChanges();
                        string url = ConfigurationReader.ServerUrlToResetPassword + "/Home/ChangePassword?id=" + objResetPassword.UniqueCode;
                        string messageBody = GA.Common.ResourcrFiles.UserProfile.Messages.ResetPasswordEmailData.ToString().Replace("((USEREMAILID))", userEmail).Replace("((RESETPASSWORDURL))", "<a id=\"lbtnReset\" href=\"" + url + "\"  >Reset Pasword</a>");
                        //string messageBody = "Password reset request has been recieved for the account with username " + userEmail + ". To set a new pasword, please visit at <a id=\"lbtnReset\" href=\"" + url + "\"  >Reset Pasword</a> ";
                        Misc.SendEmail(userEmail, messageBody);
                        response = "Email has been sent on " + userEmail;
                    }
                    else
                    {
                        response = GA.Common.ResourcrFiles.UserProfile.Messages.BlockedUser.ToString();
                    }
                }
                else
                {
                    response = GA.Common.ResourcrFiles.UserProfile.Messages.EmailIdNotExist.ToString();
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(userEmail), e);
                response = "Error !";
            }
            finally
            {
                context.Dispose();
            }
            return response;
        }

        /// <summary>
        /// Gets the security question
        /// </summary>
        /// <param name="email">EmailId of user</param>
        /// <returns></returns>
        public string GetSecurityQuestion(string email)
        {
            throw new NotImplementedException();
        }

        public SecurityQueByEmailIdResponseModel GetSecurityQuestionByEmailId(ResetPasswordModel objEmail)
        {
            var context = new GuardianAvionicsEntities();
            var response = new SecurityQueByEmailIdResponseModel();
            try
            {
                if (string.IsNullOrEmpty(objEmail.EmailId))
                {
                    response.ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty).ToString();
                    response.ResponseMessage = Enumerations.LoginReturnCodes.EmailIdCannotNullOrEmpty.GetStringValue();
                    return response;
                }

                var objUserDetails = context.UserDetails.FirstOrDefault(u => u.Profile.EmailId == objEmail.EmailId);
                if (objUserDetails != null)
                {
                    if (!objUserDetails.Profile.Deleted)
                    {
                        if (objUserDetails.Profile.ThirdPartyAccountType == null)
                        {
                            response.SecurityQuestion = objUserDetails.SecurityQuestion.Question;
                            response.SecurityAnswer = objUserDetails.SecurityAnswer;
                            response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString();
                            response.ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue();
                        }
                        else
                        {
                            if (objUserDetails.Profile.ThirdPartyAccountType == 1)
                            {
                                response.ResponseCode = ((int)Enumerations.LoginReturnCodes.AlreadyRegisterWithFaceBookAccount).ToString();
                                response.ResponseMessage = Enumerations.LoginReturnCodes.AlreadyRegisterWithFaceBookAccount.GetStringValue();
                            }
                            else
                            {
                                //ThirdPartyAccountType = 2
                                response.ResponseCode = ((int)Enumerations.LoginReturnCodes.AlreadyRegisterWithGmailAccount).ToString();
                                response.ResponseMessage = Enumerations.LoginReturnCodes.AlreadyRegisterWithGmailAccount.GetStringValue();
                            }

                        }
                    }
                    else
                    {
                        response.ResponseCode = ((int)Enumerations.LoginReturnCodes.UserBlocked).ToString();
                        response.ResponseMessage = Enumerations.LoginReturnCodes.UserBlocked.GetStringValue();
                    }

                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.LoginReturnCodes.EmailIdDoNotExist).ToString();
                    response.ResponseMessage = Enumerations.LoginReturnCodes.EmailIdDoNotExist.GetStringValue();

                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue();
            }
            finally
            {
                context.Dispose();
            }
            return response;


        }


        public OTPVerificationcode GenerateOTPForAuthentication(string Phonenumber, string emailId)
        {
            OTPVerificationcode response = new OTPVerificationcode();
            try
            {
                using (GuardianAvionicsEntities context = new GuardianAvionicsEntities())
                {
                    var IsExist = context.Profiles.Where(x => x.EmailId == emailId).ToList();
                    if (IsExist.Count == 0)
                    {
                        string randomstring = DoWork();
                        if (emailId.Length > 0)
                        {
                            SendOtpOnMail(randomstring, emailId);
                        }
                        if (Phonenumber.Length > 0)
                        {
                            string accountSid = ConfigurationReader.TwiloAccountSid.ToString();
                            string authToken = ConfigurationReader.TwiloAuthToken.ToString();
                            string FromUserNumber = ConfigurationReader.TwiloFromuserNumber.ToString();
                            var client = new TwilioRestClient(accountSid, authToken);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            var message = MessageResource.Create(
                                body: "Use the code " + randomstring + " to verify your guardian account .To keep your account safe , never share your OTP with anyone ",
                                from: new Twilio.Types.PhoneNumber(FromUserNumber),
                                to: new Twilio.Types.PhoneNumber(Phonenumber), //Add here your number
                                client: client
                            );
                        }
                        response.senTOTP = randomstring;
                        response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Success).ToString();
                        response.ResponseMessage = Enumerations.LoginReturnCodes.Success.GetStringValue();
                    }
                    else
                    {
                        response.ResponseCode = "2";
                        response.ResponseMessage = "Email Id already exists.";
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                if(ex.Message== "The 'To' number "+ Phonenumber + " is not a valid phone number.")
                {
                    response.ResponseCode = "1";
                    response.ResponseMessage = Phonenumber + " is not a valid phone number.";
                }
                else
                {
                    response.ResponseCode = ((int)Enumerations.LoginReturnCodes.Exception).ToString();
                    response.ResponseMessage = Enumerations.LoginReturnCodes.Exception.GetStringValue();
                }
                return response;
            }
        }
        public void SendOtpOnMail(string randomstring, string emailId)
        {
            var LogHeler = new LoginHelper();
            string result = LogHeler.SendOTPEmail(randomstring, emailId);
        }

        public string DoWork()
        {
            string _numbers = "0123456789";
            Random random = new Random();
            StringBuilder builder = new StringBuilder(6);
            string numberAsString = "";
            int numberAsNumber = 0;
            for (var i = 0; i < 6; i++)
            {
                builder.Append(_numbers[random.Next(0, _numbers.Length)]);
            }
            numberAsString = builder.ToString();
            numberAsNumber = int.Parse(numberAsString);

            return numberAsString;
        }



        public bool ValidateSecurityAnswer(string emailId, string securityAnswer)
        {
            bool result = false;
            var Context = new GuardianAvionicsEntities();
            try
            {
                var objProfile = Context.UserDetails.FirstOrDefault(p => p.Profile.EmailId == emailId && p.SecurityAnswer == securityAnswer);
                if (objProfile != null)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                result = false;
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
            return result;
        }

        /// <summary>
        /// Sets the values for security questions dropdwon list.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UserModelWeb SetSequrityQuetions(UserModelWeb model)
        {
            try
            {
                model.DateOfBirth = new DateTime(2000, 1, 1).Date;

                using (var context = new GuardianAvionicsEntities())
                {
                    var quetions = context.SecurityQuestions.Where(x=>x.IsActive==true).ToList();

                    if (quetions.Count > 0)
                    {
                        var sList = quetions.Select(q => new ListItems
                        {
                            text = q.Question,
                            value = q.Id,
                        }).ToList();

                        model.SecurityQuestionsList = sList;
                    }
                    else
                    {
                        model.SecurityQuestionsList = new List<ListItems>
                        {
                        new ListItems
                        {
                            text = string.Empty,
                            value = 1
                        }
                    };
                    }

                    model.ComponentManufacturerList = new List<ListItems>();
                    model.ComponentManufacturerList = context.AircraftComponentManufacturers.Where(a => !a.Deleted && !context.MappingAircraftManufacturerAndUsers.Select(s => s.AircraftComponentManufacturerId).ToList().Contains(a.Id)).Select(c => new ListItems { text = c.Name, value = c.Id }).ToList();
                    model.ComponentManufacturerList = model.ComponentManufacturerList.OrderBy(m => m.value).ToList();
                    return model;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(model), e);
                return model;
            }
        }

        /// <summary>
        /// Gets the security question from sequrity question id
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public string GetSecurityQuestionFromModel(int p)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var qes = context.SecurityQuestions.FirstOrDefault(s => s.Id == p);

                    if (qes.Question == string.Empty)
                    {
                        return context.SecurityQuestions.First().Question;
                    }

                    return qes.Question;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return string.Empty;
            }

        }

        #endregion registration and reset password


        #region Preference

        /// <summary>
        /// Updates user preference
        /// </summary>
        /// <param name="profileUpdate">UpdatePreferenceModel object , preference to update</param>
        /// <returns>UpdatePreferenceResponseModel object</returns>
        public UpdatePreferenceResponseModel UpdatePreference(UpdatePreferenceModel profileUpdate)
        {
            var response = new UpdatePreferenceResponseModel();

            try
            {
                if (ConfigurationReader.DepricateCurrentVersion == Boolean.TrueString)
                    return new UpdatePreferenceResponseModel
                    {
                        ResponseCode = ((int)Enumerations.RegistrationReturnCodes.DepricateCurrentVersion).ToString(),
                        ResponseMessage = Enumerations.RegistrationReturnCodes.DepricateCurrentVersion.GetStringValue()
                    };

                var objResponse = ValidateUpdatePreference(profileUpdate);
                if (objResponse.ResponseCode != Enumerations.UpdatePreferenceCode.Success.GetStringValue())
                    return objResponse;

                using (var context = new GuardianAvionicsEntities())
                {
                    var id = Convert.ToInt32(profileUpdate.ProfileId);
                    var profile = context.Profiles.FirstOrDefault(p => p.Id == id && p.Deleted == false);

                    // if profileid do not match in profile table return error code.
                    if (profile == default(Profile) || profile.Id == 0)
                    {
                        response.ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.ProfileIdNotFound).ToString();
                        response.ResponseMessage = Enumerations.UserProfileUpdateCodes.ProfileIdNotFound.GetStringValue();
                        return response;
                    }

                    var objPreference = context.Preferences.FirstOrDefault(p => p.ProfileId == profile.Id);
                    if (objPreference == default(Preference))
                        return response.Exception();


                    if (profileUpdate.IsUpdateAvailableForEmailList)
                    {
                        objPreference.LastUpdateDateForEmailList = DateTime.UtcNow;
                        //if the list have any data then  delete all record from database and insert new data  
                        if (profileUpdate.EmailList != null)
                        {
                            if (profileUpdate.EmailList.Any())
                            {
                                objPreference.LastUpdateDateForEmailList = DateTime.UtcNow;
                                context.UserEmails.RemoveRange(context.UserEmails.Where(x => x.ProfileId == profile.Id));
                                profileUpdate.EmailList.ForEach(e => context.UserEmails.Add(new UserEmail
                                {
                                    EmailId = e.Email,
                                    IsEnabled = e.IsEnabled,
                                    Profile = profile
                                }));

                            }
                            else
                            {
                                context.UserEmails.RemoveRange(context.UserEmails.Where(x => x.ProfileId == profile.Id));
                            }
                        }
                        else
                        {
                            context.UserEmails.RemoveRange(context.UserEmails.Where(x => x.ProfileId == profile.Id));
                        }
                        context.SaveChanges();
                    }


                    if (profileUpdate.IsUpdateAvailableForPreference)
                    {
                        // Update preference for it
                        objPreference.SpeedUnit = profileUpdate.SpeedUnit;
                        objPreference.DefaultAirportIdentifier = profileUpdate.DefaultAirportIdentifier;
                        objPreference.DistanceUnit = profileUpdate.DistanceUnit;
                        objPreference.FuelUnit = profileUpdate.FuelUnit;
                        objPreference.WeightUnit = profileUpdate.WeightUnit;
                        objPreference.VerticalSpeedUnit = profileUpdate.VerticalSpeedUnit;
                        objPreference.ShowCabin = profileUpdate.ShowCabin;
                        objPreference.ShowPitch = profileUpdate.ShowPitch;
                        objPreference.SwitchToEnginForAlarm = profileUpdate.SwitchToEnginForAlarm;
                        objPreference.AudioRecording = profileUpdate.AudioRecording;
                        objPreference.DisplayAltitude = profileUpdate.DisplayAltitude;
                        objPreference.LastUpdateDate = DateTime.UtcNow;
                        objPreference.IsDropboxSync = profileUpdate.IsDropboxSync;
                        objPreference.AccessToken = profileUpdate.AccessToken;
                        //objPreference.DropBoxToken = profileUpdate.Token;
                        //objPreference.DropBoxTokenSecret = profileUpdate.TokenSecret;

                        context.SaveChanges();
                    }

                    response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                }

                return response;
            }
            catch (Exception ex)
            {
                return response.Exception();
            }
        }

        /// <summary>
        /// Valildates UpdatePreferenceModel object.
        /// </summary>
        /// <param name="profileUpdate">UpdatePreferenceModel object</param>
        /// <returns>A UpdatePreferenceResponseModel object with response code set for exception or success</returns>
        public UpdatePreferenceResponseModel ValidateUpdatePreference(UpdatePreferenceModel profileUpdate)
        {
            var response = new UpdatePreferenceResponseModel();

            // if no profileid is null
            if (String.IsNullOrEmpty(profileUpdate.ProfileId))
            {
                response.ResponseCode = ((int)Enumerations.UserProfileUpdateCodes.ProfileIdNotFound).ToString();
                response.ResponseMessage = Enumerations.UserProfileUpdateCodes.ProfileIdNotFound.GetStringValue();
                return response;
            }

            //if distance is null
            if (String.IsNullOrEmpty(profileUpdate.DistanceUnit))
            {
                response.ResponseCode = Enumerations.UpdatePreferenceCode.DistanceNotFound.GetStringValue();
                response.ResponseMessage = Enumerations.UpdatePreferenceCode.DistanceNotFound.ToString();
                return response;
            }

            //if speed is null
            if (String.IsNullOrEmpty(profileUpdate.SpeedUnit))
            {
                response.ResponseCode = Enumerations.UpdatePreferenceCode.SpeedNotFound.GetStringValue();
                response.ResponseMessage = Enumerations.UpdatePreferenceCode.SpeedNotFound.ToString();
                return response;
            }

            //if fuel is null
            if (String.IsNullOrEmpty(profileUpdate.FuelUnit))
            {
                response.ResponseCode = Enumerations.UpdatePreferenceCode.FuelNotFound.GetStringValue();
                response.ResponseMessage = Enumerations.UpdatePreferenceCode.FuelNotFound.ToString();
                return response;
            }

            //if vertical speed is null
            if (String.IsNullOrEmpty(profileUpdate.VerticalSpeedUnit))
            {
                response.ResponseCode = Enumerations.UpdatePreferenceCode.VerticalSpeedNotFound.GetStringValue();
                response.ResponseMessage = Enumerations.UpdatePreferenceCode.VerticalSpeedNotFound.ToString();
                return response;
            }

            //if weight unit is null
            if (String.IsNullOrEmpty(profileUpdate.WeightUnit))
            {
                response.ResponseCode = Enumerations.UpdatePreferenceCode.WeightNotFound.GetStringValue();
                response.ResponseMessage = Enumerations.UpdatePreferenceCode.WeightNotFound.ToString();
                return response;
            }

            ////Check for Email List 
            if (profileUpdate.EmailList != null)
            {
                if (profileUpdate.EmailList.Any())
                {
                    if (profileUpdate.EmailList.Count() > 5)
                    {
                        response.ResponseCode = Enumerations.UpdatePreferenceCode.EmailCountCrossMaxLimit.GetStringValue();
                        response.ResponseMessage = Enumerations.UpdatePreferenceCode.EmailCountCrossMaxLimit.ToString();
                        return response;
                    }


                    var IsDistinctEmail = profileUpdate.EmailList
                                            .GroupBy(x => new { x.Email })
                                            .Select(group => new { Email = group.Key, Count = group.Count() }).Where(x => x.Count > 1).ToList();
                    if (IsDistinctEmail.Any())
                    {
                        response.ResponseCode = Enumerations.UpdatePreferenceCode.InsertDistinctEmailOnly.GetStringValue();
                        response.ResponseMessage = Enumerations.UpdatePreferenceCode.InsertDistinctEmailOnly.ToString();
                        return response;
                    }

                    const string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

                    //foreach (var emailList in profileUpdate.EmailList)
                    //{
                    //    if (!Regex.IsMatch(emailList.Email, pattern))
                    //    {
                    //        response.ResponseCode = Enumerations.UpdatePreferenceCode.InvalidEmailId.GetStringValue();
                    //        response.ResponseMessage = Enumerations.UpdatePreferenceCode.InvalidEmailId.ToString();
                    //        return response;
                    //    }
                    //}

                    if (profileUpdate.EmailList.Any(emailList => !Regex.IsMatch(emailList.Email, pattern)))
                    {
                        response.ResponseCode = Enumerations.UpdatePreferenceCode.InvalidEmailId.GetStringValue();
                        response.ResponseMessage = Enumerations.UpdatePreferenceCode.InvalidEmailId.ToString();
                        return response;
                    }

                }
            }

            //// dropbox token and token secret
            //if (profileUpdate.Token == null || profileUpdate.TokenSecret == null)
            //{
            //    return response.Exception();
            //}

            // if all the fields are validated then Give response of success
            response.ResponseCode = Enumerations.UpdatePreferenceCode.Success.GetStringValue();
            response.ResponseMessage = Enumerations.UpdatePreferenceCode.Success.ToString();
            return response;
        }

        /// <summary>
        /// Sends the preference for User identified by profile Id
        /// </summary>
        /// <param name="obj">Preference class object containing profile Id</param>
        /// <returns>Preference for the user</returns>
        public GetPreferenceResponseModel GetPreference(GetPrefereceModel obj)
        {
            var profileId = obj.ProfileId;
            var response = new GetPreferenceResponseModel();
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {

                    var profile = new Profile();
                    bool profileNotFound = false;

                    if (profileId != 0)
                    {
                        profile = context.Profiles.FirstOrDefault(p => p.Id == profileId && !p.Deleted);

                        if (profile == default(Profile))
                            profileNotFound = true;
                    }
                    else
                        profileNotFound = true;

                    if (profileNotFound)
                    {
                        response.ResponseCode = ((int)Enumerations.GetPreference.ProfileIdNotFound).ToString();
                        response.ResponseMessage = Enumerations.GetPreference.ProfileIdNotFound.GetStringValue();
                        return response;
                    }

                    response.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);
                    var objPreference = context.Preferences.FirstOrDefault(p => p.ProfileId == profile.Id);

                    var udate = Misc.GetDate(obj.LastUpdatedDate) ?? DateTime.MinValue;
                    if (objPreference.LastUpdateDateForEmailList > udate)
                    {

                        var objUserEmail = profile.UserEmails.Select(x => new PreferenceEmailListModel { Email = x.EmailId, IsEnabled = x.IsEnabled }).ToList();
                        if (objUserEmail.Count > 0)
                        {
                            response.EmailList = objUserEmail;
                        }
                        else
                        {
                            response.EmailList = new List<PreferenceEmailListModel>();
                        }
                        response.IsUpdateAvailableForEmailList = true;
                    }
                    else
                    {
                        response.IsUpdateAvailableForEmailList = false;
                        response.EmailList = new List<PreferenceEmailListModel>();
                    }
                    response.IsUpdateAvailableForPreference = false;
                    if (objPreference.LastUpdateDate > udate)
                    {
                        response.IsUpdateAvailableForPreference = true;
                        if (objPreference != default(Preference))
                        {
                            response.DefaultAirportIdentifier = objPreference.DefaultAirportIdentifier;
                            response.DistanceUnit = objPreference.DistanceUnit;
                            response.FuelUnit = objPreference.FuelUnit;
                            response.SpeedUnit = objPreference.SpeedUnit;
                            response.VerticalSpeedUnit = objPreference.VerticalSpeedUnit;
                            response.WeightUnit = objPreference.WeightUnit;
                            response.ShowCabin = objPreference.ShowCabin ?? false;
                            response.ShowPitch = objPreference.ShowPitch ?? false;
                            response.SwitchToEnginForAlarm = objPreference.SwitchToEnginForAlarm ?? false;
                            response.AudioRecording = objPreference.AudioRecording ?? false;
                            response.DisplayAltitude = objPreference.DisplayAltitude ?? 0;

                            response.IsDropboxSync = Convert.ToBoolean(objPreference.IsDropboxSync);
                            response.AccessToken = objPreference.AccessToken;
                            //response.Token = objPreference.DropBoxToken ?? string.Empty;
                            //response.TokenSecret = objPreference.DropBoxTokenSecret ?? string.Empty;

                            response.ResponseCode = ((int)Enumerations.GetPreference.Success).ToString();
                            response.ResponseMessage = Enumerations.GetPreference.Success.GetStringValue();
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                response.ResponseCode = ((int)Enumerations.GetPreference.Exception).ToString();
                response.ResponseMessage = Enumerations.GetPreference.Exception.GetStringValue();
                return response;
            }

            // if objPreference == default(Preference) then return exception
            response.ResponseCode = ((int)Enumerations.GetPreference.Exception).ToString();
            response.ResponseMessage = Enumerations.GetPreference.Exception.GetStringValue();

            return response;
        }

        /// <summary>
        /// Sets Id's for dropdown list for all the units in PreferenceModelWeb , web
        /// </summary>
        /// <param name="Objmodel"></param>
        /// <param name="objPreference"></param>
        /// <returns></returns>
        public PreferenceModelWeb SetIdForAllUnits(PreferenceModelWeb Objmodel, UpdatePreferenceModel objPreference)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var distanceId = context.MeasurementUnits.FirstOrDefault(d => d.MeasurementUnitName == objPreference.DistanceUnit);
                    var speedId = context.MeasurementUnits.FirstOrDefault(s => s.MeasurementUnitName == objPreference.SpeedUnit);
                    var verticalId = context.MeasurementUnits.FirstOrDefault(v => v.MeasurementUnitName == objPreference.VerticalSpeedUnit);
                    var fuelId = context.MeasurementUnits.FirstOrDefault(f => f.MeasurementUnitName == objPreference.FuelUnit);
                    var weightId = context.MeasurementUnits.FirstOrDefault(w => w.MeasurementUnitName == objPreference.WeightUnit);

                    if (distanceId != default(MeasurementUnit))
                        Objmodel.DistanceUnitId = distanceId.Id;

                    if (speedId != default(MeasurementUnit))
                        Objmodel.SpeedUnitId = speedId.Id;

                    if (verticalId != default(MeasurementUnit))
                        Objmodel.VerticalSpeedUnitId = verticalId.Id;

                    if (fuelId != default(MeasurementUnit))
                        Objmodel.FuelUnitId = fuelId.Id;

                    if (weightId != default(MeasurementUnit))
                        Objmodel.WeightUnitId = weightId.Id;
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return Objmodel;
        }

        /// <summary>
        /// gets values for all id's of drop down list.
        /// </summary>
        /// <param name="preferenceEntity"></param>
        /// <param name="preference"></param>
        /// <returns></returns>
        public UpdatePreferenceModel SetValuesForAllIds(UpdatePreferenceModel preferenceEntity, PreferenceModelWeb preference)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var distance = context.MeasurementUnits.FirstOrDefault(d => d.Id == preference.DistanceUnitId);
                    var speed = context.MeasurementUnits.FirstOrDefault(d => d.Id == preference.SpeedUnitId);
                    var vSpeed = context.MeasurementUnits.FirstOrDefault(d => d.Id == preference.VerticalSpeedUnitId);
                    var fuel = context.MeasurementUnits.FirstOrDefault(d => d.Id == preference.FuelUnitId);
                    var weight = context.MeasurementUnits.FirstOrDefault(d => d.Id == preference.WeightUnitId);

                    if (distance != default(MeasurementUnit))
                        preferenceEntity.DistanceUnit = distance.MeasurementUnitName;

                    if (speed != default(MeasurementUnit))
                        preferenceEntity.SpeedUnit = speed.MeasurementUnitName;

                    if (vSpeed != default(MeasurementUnit))
                        preferenceEntity.VerticalSpeedUnit = vSpeed.MeasurementUnitName;

                    if (fuel != default(MeasurementUnit))
                        preferenceEntity.FuelUnit = fuel.MeasurementUnitName;

                    if (weight != default(MeasurementUnit))
                        preferenceEntity.WeightUnit = weight.MeasurementUnitName;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return preferenceEntity;
        }

        /// <summary>
        /// Sets dropdown list id's and values for all the units of preference page
        /// </summary>
        /// <param name="Objmodel"></param>
        /// <returns></returns>
        public PreferenceModelWeb SetSelectListForAllUnits(PreferenceModelWeb Objmodel)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    Objmodel.VerticalSpeed = new List<ListItems>();
                    Objmodel.Distance = new List<ListItems>();
                    Objmodel.Speed = new List<ListItems>();
                    Objmodel.Fuel = new List<ListItems>();
                    Objmodel.Weight = new List<ListItems>();

                    //var distanceDb = context.MeasurementUnits.Where(d => d.Measurement.MeasurementName == "DistanceUnit").ToList();

                    Objmodel.Distance = context.MeasurementUnits
                        .Where(d => d.Measurement.MeasurementName == "DistanceUnit")
                        .Select(s => new ListItems
                        {
                            text = s.MeasurementUnitName,
                            value = s.Id,
                        })
                        .ToList();

                    //foreach (var dis in distanceDb)
                    //{
                    //    Objmodel.Distance.Add(new ListItems
                    //    {
                    //        Text = dis.MeasurementUnitName,
                    //        Value = dis.Id,
                    //    });
                    //}

                    var speedDb = context.MeasurementUnits.Where(s => s.Measurement.MeasurementName == "SpeedUnit").ToList();

                    foreach (var spd in speedDb)
                    {
                        Objmodel.Speed.Add(new ListItems
                        {
                            text = spd.MeasurementUnitName,
                            value = spd.Id,
                        });
                    }

                    var fuelDb = context.MeasurementUnits.Where(f => f.Measurement.MeasurementName == "FuelUnit").ToList();

                    foreach (var fl in fuelDb)
                    {
                        Objmodel.Fuel.Add(new ListItems
                        {
                            text = fl.MeasurementUnitName,
                            value = fl.Id,
                        });
                    }

                    var weightDb = context.MeasurementUnits.Where(f => f.Measurement.MeasurementName == "WeightUnit").ToList();

                    foreach (var wt in weightDb)
                    {
                        Objmodel.Weight.Add(new ListItems
                        {
                            text = wt.MeasurementUnitName,
                            value = wt.Id,
                        });
                    }

                    var verticalDb = context.MeasurementUnits.Where(v => v.Measurement.MeasurementName == "VerticalSpeedUnit").ToList();

                    foreach (var vspd in verticalDb)
                    {
                        Objmodel.VerticalSpeed.Add(new ListItems
                        {
                            text = vspd.MeasurementUnitName,
                            value = vspd.Id,
                        });
                    }

                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(Objmodel), e);
            }

            return Objmodel;

        }//SetSelectListForAllUnits


        #endregion Preference


        #region user profile

        public UserInfo GetUserById(int profileId)
        {
            UserInfo response = new UserInfo();
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            if (profile != null)
            {
                response.TId = profile.Id.ToString();
                response.EmailId = profile.EmailId;
                response.FirstName = profile.UserDetail == null ? "" : profile.UserDetail.FirstName;
                response.LastName = profile.UserDetail == null ? "" : profile.UserDetail.LastName;
            }
            return response;

        }

        public void SetUserQuickBloxId(int profileId, string quickbloxId)
        {
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            if (profile != null)
            {
                profile.QuickbloxId = quickbloxId;
                context.SaveChanges();
            }
        }

        public List<string> GetAllAircraftByProfileId(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            List<int> profileIdList = new AircraftHelper().GetAircraftList(profileId).Where(w => w.OwnerProfileId != null).Select(s => s.OwnerProfileId ?? 0).ToList();
           List<string> quickBloxList = context.Profiles.Where(w => w.Id != profileId && profileIdList.Contains(w.Id) && w.QuickbloxId != null).Select(s => s.QuickbloxId).ToList();
            return quickBloxList;
        }

        /// <summary>
        /// Gets user profile
        /// </summary>
        /// <param name="profileId">user profile Id</param>
        /// <returns>User profile</returns>
        public UserProfileWeb GetUserProfile(int profileId)
        {
            var response = new UserProfileWeb();

            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    response.LicensesList = new List<LicensesModelWeb>();
                    response.MedicalCertificateList = new List<MedicalCertificateModelWeb>();

                    var profile = context.Profiles.FirstOrDefault(p => p.Id == profileId && !p.Deleted);

                    var helper = new LoginHelper();
                    // ReSharper disable once PossibleNullReferenceException
                    var loginData = new LoginModel { EmailId = profile.EmailId, Password = profile.Password };
                    loginData.Password = new Misc().Decrypt(loginData.Password);
                    LoginResponseModel loginResponse = helper.LoginUser(loginData);

                    //if (loginResponse.ResponseCode == Enumerations.LoginReturnCodes.Success.GetStringValue())
                    if (true)
                    {
                        Misc objMisc = new Misc();
                        
                        //change request for med cert. Should be only one record. For now we maintain the list with 1 record to maintain the UI
                        var medicalCertificateList = (context.MedicalCertificates.Where(m => m.ProfileId == profile.Id && m.Deleted == false).OrderByDescending(o => o.Id).Take(1).Select(m => m)).ToList();

                        response.Countries = context.Countries.Select(x => new SelectListItem()
                        {
                            Text = "(" + x.ShortName + ") " + x.CountryCode,
                            Value =x.CountryCode
                        }).ToList();
                        response.Countries.Add(new SelectListItem() { Text = "--", Value = "0" });
                        foreach (var certificate in medicalCertificateList)
                        {
                            response.MedicalCertificateList.Add(
                                new MedicalCertificateModelWeb
                                {
                                    ReferenceNumber = certificate.ReferenceNumber,
                                    Type = certificate.Type,
                                    ValidUntil = Misc.GetStringOnlyDate(certificate.ValidUntil),
                                    //Id = certificate.Id,
                                    TId = objMisc.Encrypt(certificate.Id.ToString()),
                                    UniqueId = certificate.UniqueId ?? default(long),
                                    ClassId = certificate.MedicalCertClassId,
                                    ClassName = certificate.MedicalCertClassId == null ? "" : certificate.MedicalCertClass.Name,
                                }
                            );
                        }

                        var lincenseList = (context.Licenses.Where(l => l.ProfileId == profile.Id && l.Deleted == false).Select(l => l)).ToList();



                        foreach (var license in lincenseList)
                        {
                            var ratingsList = (context.Ratings.Where(r => r.LicenseId == license.Id && r.Deleted == false).Select(l => l)).ToList();

                            var objLicenseModel = new LicensesModelWeb { Ratingslist = new List<RatingModelWeb>(), TId = objMisc.Encrypt(license.Id.ToString()) };

                            foreach (var rating in ratingsList)
                            {
                                objLicenseModel.Ratingslist.Add(
                                new RatingModelWeb
                                {
                                    TId = objMisc.Encrypt(rating.Id.ToString()),
                                    Type = rating.Type,
                                    ValidUntil = Misc.GetStringOnlyDate(rating.ValidUntil),
                                    UniqueId = rating.UniqueId ?? default(long),
                                }
                                );
                            }

                            objLicenseModel.ReferenceNumber = license.ReferenceNumber;
                            objLicenseModel.Type = license.Type;
                            objLicenseModel.ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil);
                            objLicenseModel.UniqueId = license.UniqueId ?? default(long);

                            response.LicensesList.Add(objLicenseModel);
                        }

                        response.MedicalCertificateList.ForEach(m => { m.ValidUntil = Misc.dateToMMDDYYYY(m.ValidUntil); }); //DateFormateChange
                        var userdetails = profile.UserDetail;
                        response.LicensesList.ForEach(l => { l.ValidUntil = Misc.dateToMMDDYYYY(l.ValidUntil); }); //DateFormateChange
                        response.FirstName = userdetails.FirstName;
                        response.LastName = userdetails.LastName;
                        response.EmailId = profile.EmailId;
                        response.PhoneNumber = userdetails.PhoneNumber;
                        //response.DateOfBirth = (DateTime)Misc.GetDate(loginResponse.DateOfBirth);
                        response.DateOfBirth = Misc.GetStringOnlyDateUS(userdetails.DateOfBirth);
                        response.StreetAddress = userdetails.StreetAddress;
                        response.City = userdetails.City;
                        response.State = userdetails.State;
                        response.ZipCode = userdetails.ZipCode.ToString();
                        response.CompanyName = userdetails.CompanyName;
                        response.CountryCode = userdetails.CountryCode==null?"0": userdetails.CountryCode;
                        response.TwoFactorAuthentication = Convert.ToBoolean(userdetails.TwoStepAuthentication);
                        response.IsSubscriptionVisible = profile.IsSubscriptionVisible ?? false;

                        if (!String.IsNullOrEmpty(userdetails.ImageUrl))
                        {
                            //string path = ConfigurationReader.ImageServerPathKey;
                            string path = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages;
                            response.ProfileImagePath = path + userdetails.ImageUrl;
                        }
                        else
                        {
                            response.ProfileImagePath = Constants.UserProfileDefaultImage;
                        }


                        var medVaild = context.MedicalCertificates
                                      .Where(mc => mc.ProfileId == profileId
                                                && !mc.Deleted
                                                && mc.ValidUntil != null)
                                      .ToList()
                                      .OrderBy(od => od.ValidUntil)
                                      .ToList()
                                      .FirstOrDefault();

                        if (medVaild == default(MedicalCertificate))
                        {
                            response.MedicalValidUntil = Constants.NotApplicable;
                            response.MedicalType = Constants.NotApplicable;
                        }
                        else
                        {
                            if (medVaild.ValidUntil >= System.DateTime.UtcNow.Date)
                                response.MedicalValidUntil = Misc.GetStringOnlyDateUS(medVaild.ValidUntil);
                            else
                                response.MedicalValidUntil = Constants.Expired;
                            response.MedicalType = medVaild.MedicalCertClass.Name;
                        }

                        // set BFR due date of license
                        var licenseBFR = context.Licenses
                                .Where(li => li.ProfileId == profileId
                                           && !li.Deleted
                                           && li.ValidUntil != null)
                                .OrderBy(ob => ob.ValidUntil)
                                .ToList()
                                .FirstOrDefault();

                        if (licenseBFR == default(License))
                        {
                            response.BfrValidUnitl = Constants.NotApplicable;
                            response.Type = Constants.NotApplicable;

                        }
                        else
                        {
                            if (licenseBFR.ValidUntil >= System.DateTime.UtcNow.Date)
                            {
                                response.BfrValidUnitl = Misc.GetStringOnlyDateUS(licenseBFR.ValidUntil);
                            }
                            else
                            {
                                response.BfrValidUnitl = Constants.Expired;
                            }
                            response.Type = licenseBFR.Type;
                        }


                        // Pilot log related data

                        // get all the pilot logs for the user
                        var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId) && !pl.Deleted && pl.Finished && pl.DayPIC != null && pl.DayPIC != "")
                                        .OrderByDescending(ob => ob.Date)
                                        .ToList();

                        if (pilotLogs.Count > 0)
                        {
                            // hours flown last 90 days

                            double totalTime = 0.0;

                            DateTime date = System.DateTime.UtcNow - new TimeSpan(90, 0, 0, 0);

                            pilotLogs.Where(pl => pl.Date >= date)
                                   .ToList()
                                   .ForEach(a => totalTime = totalTime + objMisc.ConvertHHMMToMinutes(a.DayPIC));

                            response.FilghtFlownLast90Days = Misc.ConvertMinToOneTenthOFTime(totalTime);
                            totalTime = 0.0;
                            // total flight flown

                            pilotLogs.ForEach(a => totalTime = totalTime + objMisc.ConvertHHMMToMinutes(a.DayPIC));
                            //resposne.totalFlightTime = (int)totalTime.TotalHours;
                            response.TotalFlightTime = Misc.ConvertMinToOneTenthOFTime(totalTime);
                        }
                        else
                        {

                            response.FilghtFlownLast90Days = "00:0 ";
                            response.TotalFlightTime = "00:0";
                        }

                        response.RatingsList = new List<RatingModelWeb>();
                    }


                    response.SubViewModelList = new PaypalPaymentHelper().GetAllReceiptBySubProfileId(profile.Id);


                    return response;
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                //logger.Fatal("Exception ", ex);
                return response;
            }

        }

        public List<ListItems> GetMedicalCertClass()
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var medicalCertificateClass = new List<ListItems>();
                medicalCertificateClass = (from m in context.MedicalCertClasses
                                           where !m.Deleted
                                           select new ListItems
                                           {
                                               text = m.Name,
                                               value = m.Id
                                           }).ToList();
                if (medicalCertificateClass == null)
                {
                    medicalCertificateClass = new List<ListItems>();
                }
                return medicalCertificateClass;

            }
            catch (Exception ex)
            {
                //ExceptionHandler.ReportError(ex);
                //logger.Fatal("Exception ", ex);
                return new List<ListItems>();
            }
        }



        public List<MedicalCertificateModelWeb> GetMedicalCertificateList(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var ObjmedicalCertificateList = (context.MedicalCertificates.Where(m => m.ProfileId == profileId && m.Deleted == false).OrderByDescending(o => o.Id).Take(1).Select(m => m)).ToList();
            var response = new List<MedicalCertificateModelWeb>();
            Misc objMisc = new Misc();
            foreach (var certificate in ObjmedicalCertificateList)
            {
                response.Add(new MedicalCertificateModelWeb
                {
                    ReferenceNumber = certificate.ReferenceNumber,
                    Type = certificate.Type,
                    ValidUntil = Misc.dateToMMDDYYYY(Misc.GetStringOnlyDate(certificate.ValidUntil)),
                    //Id = certificate.Id,
                    TId = objMisc.Encrypt(certificate.Id.ToString()),
                    UniqueId = certificate.UniqueId ?? default(long),
                    ClassId = certificate.MedicalCertClassId,
                    ClassName = certificate.MedicalCertClassId == null ? "" : certificate.MedicalCertClass.Name,
                });
            }
            return response;
        }


        public List<LicensesModelWeb> GetLicenseList(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var objLoginResponse = new List<LicensesModelWeb>();
            Misc objMisc = new Misc();
            var lincenseList = (context.Licenses.Where(l => l.ProfileId == profileId && l.Deleted == false).Select(l => l)).ToList();
            foreach (var license in lincenseList)
            {
                var ratingsList = (context.Ratings.Where(r => r.LicenseId == license.Id && r.Deleted == false).Select(l => l)).ToList();
                var objLicenseModel = new LicensesModelWeb { Ratingslist = new List<RatingModelWeb>(), TId = objMisc.Encrypt(license.Id.ToString()) };
                foreach (var rating in ratingsList)
                {
                    objLicenseModel.Ratingslist.Add(
                    new RatingModelWeb
                    {
                        TId = objMisc.Encrypt(rating.Id.ToString()),
                        Type = rating.Type,
                        ValidUntil = (Misc.GetStringOnlyDate(rating.ValidUntil)),
                        UniqueId = rating.UniqueId ?? default(long),
                    });
                }
                objLicenseModel.ReferenceNumber = license.ReferenceNumber;
                objLicenseModel.Type = license.Type;
                objLicenseModel.ValidUntil = Misc.dateToMMDDYYYY(Misc.GetStringOnlyDate(license.ValidUntil));
                objLicenseModel.UniqueId = license.UniqueId ?? default(long);
                objLoginResponse.Add(objLicenseModel);
            }
            return objLoginResponse;
        }

        /// <summary>
        /// Get medical certificate to edit for a medical certificate Id , web
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MedicalCertificateModel GetMedicalCertificateToEditWeb(int? id)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var med = context.MedicalCertificates.FirstOrDefault(m => m.Id == id && !m.Deleted);

                    if (med != default(MedicalCertificate))
                    {
                        var medCer = new MedicalCertificateModel
                        {
                            Id = med.Id,
                            ReferenceNumber = med.ReferenceNumber,
                            Type = med.Type,
                            //ValidUntil = Misc.GetStringOnlyDate(med.ValidUntil),
                            ValidUntil = Misc.GetStringOnlyDateUS(med.ValidUntil), //DateFormateChange
                            //UniqueId = med.UniqueId == null ? default(long) : (long)med.UniqueId,
                            ClassId = med.MedicalCertClassId ?? 0,
                        };
                        return medCer;
                    }
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return new MedicalCertificateModel();
        }

        /// <summary>
        /// Saves or updates medical certificate from web interface.
        /// </summary>
        /// <param name="medCertificate"></param>
        /// <returns></returns>
        public List<MedicalCertificateModelWeb> SaveCertificateWeb(MedicalCertificate medCertificate)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var medCertificateEdited = context.MedicalCertificates.FirstOrDefault(m => m.Id == medCertificate.Id);
                    var profile = context.Profiles.FirstOrDefault(f => f.Id == medCertificate.ProfileId);
                    if (profile != null)
                    {
                        profile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                    }
                    if (medCertificate.Id != 0 && medCertificateEdited != default(MedicalCertificate))
                    {
                        // update medical certifcate
                        medCertificateEdited.LastUpdated = medCertificate.LastUpdated;
                        medCertificateEdited.ReferenceNumber = medCertificate.ReferenceNumber;
                        medCertificateEdited.Type = medCertificate.Type;
                        medCertificateEdited.ValidUntil = medCertificate.ValidUntil;
                        medCertificateEdited.MedicalCertClassId = medCertificate.MedicalCertClassId == 0 ? null : medCertificate.MedicalCertClassId;
                        context.SaveChanges();

                    }
                    else
                    {
                        // save new certificate
                        var newMedical = new MedicalCertificate
                        {
                            LastUpdated = medCertificate.LastUpdated,
                            ProfileId = medCertificate.ProfileId,
                            ReferenceNumber = medCertificate.ReferenceNumber,
                            Type = medCertificate.Type,
                            ValidUntil = medCertificate.ValidUntil,
                            UniqueId = DateTime.UtcNow.Ticks,
                            MedicalCertClassId = medCertificate.MedicalCertClassId == 0 ? null : medCertificate.MedicalCertClassId,
                        };

                        context.MedicalCertificates.Add(newMedical);
                        context.SaveChanges();
                    }


                }
                return GetMedicalCertificateList(medCertificate.ProfileId);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(medCertificate), e);
                return new List<MedicalCertificateModelWeb>();
            }

        }

        /// <summary>
        /// Delete medical certificate deleted from web.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<MedicalCertificateModelWeb> DeleteMedicalCertificateWeb(int? id, int profileId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var medCer = context.MedicalCertificates.FirstOrDefault(m => m.Id == id && !m.Deleted);
                    if (medCer != null)
                    {

                        var profile = context.Profiles.FirstOrDefault(f => f.Id == medCer.ProfileId);
                        if (profile != null)
                        {
                            profile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                        }

                        context.MedicalCertificates.Remove(medCer);
                    }
                    context.SaveChanges();
                    return GetMedicalCertificateList(profileId);
                }
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
                return new List<MedicalCertificateModelWeb>();
            }
        }

        public string GenerateUniqueImageNameUser(string profileId)
        {
            var fileName = "ProfileImage_" + profileId + "_" + DateTime.UtcNow.Ticks + ".png";
            return fileName;
        }

        /// <summary>
        /// Updates user profile on web
        /// </summary>
        /// <param name="userData">UserProfileWeb object</param>
        /// <param name="profileId">profile Id</param>
        /// <returns></returns>
        public string SaveUserProfileWeb(UserProfileWeb userData, int profileId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (profileId == 0) return Boolean.FalseString;
                    var profiles = context.Profiles.FirstOrDefault(p => p.Id == profileId);

                    if (userData.EmailId != profiles.EmailId)
                    {
                        var validateProfile = context.Profiles.FirstOrDefault(p => p.EmailId == userData.EmailId);
                        if (validateProfile != null)
                        {
                            return "Email Id already exists.";
                        }
                    }


                    var userDetail = context.UserDetails.FirstOrDefault(p => p.ProfileId == profileId);

                    if (userDetail == default(UserDetail)) return Boolean.FalseString;

                    userDetail.FirstName = userData.FirstName;
                    userDetail.LastName = userData.LastName;
                    userDetail.PhoneNumber = userData.PhoneNumber;
                    // userDetail.DateOfBirth = Misc.GetDate(userData.DateOfBirth);
                    userDetail.DateOfBirth = string.IsNullOrEmpty(userData.DateOfBirth) ? null : Misc.GetDateUS(userData.DateOfBirth); //DateFormateChange
                    userDetail.StreetAddress = userData.StreetAddress;
                    userDetail.City = userData.City;
                    userDetail.State = userData.State;
                    userDetail.CompanyName = userData.CompanyName;
                    userDetail.CountryCode = userData.CountryCode;
                    userDetail.ZipCode = userData.ZipCode == "" ? (int?)null : Convert.ToInt32(userData.ZipCode);
                    profiles.EmailId = userData.EmailId;
                    userDetail.TwoStepAuthentication = userData.TwoFactorAuthentication;
                    profiles.LastUpdated = DateTime.UtcNow;
                    context.SaveChanges();


                    if (userData.UserProfileImage != null)
                    {
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////
                        //logger.Fatal("Image path = " + ConfigurationReader.ImagePathKey);
                        var path = ConfigurationReader.ImagePathKey;
                        //string fileName = "ProfileImage" + profileId.ToString() + ".png";
                        string fileName = GenerateUniqueImageNameUser(profileId.ToString());

                        string fullPath = path + @"\" + fileName;

                        // convert httpPostedFileBase to bytes array
                        Stream imgStream = userData.UserProfileImage.InputStream;
                        var memoryStream = new MemoryStream();
                        imgStream.CopyTo(memoryStream);
                        byte[] imgBytes = memoryStream.ToArray();

                        // resize image

                        // resize width and height
                        var r = new ResizeSettings { MaxHeight = 300, MaxWidth = 300 };
                        try
                        {
                            if (!string.IsNullOrEmpty(userDetail.ImageUrl))
                            {
                                //if (File.Exists(ConfigurationReader.ImagePathKey + @"\" + userDetail.ImageUrl))
                                //{
                                //    File.Delete(ConfigurationReader.ImagePathKey + @"\" + userDetail.ImageUrl);
                                //}
                                new Misc().DeleteFileFromS3Bucket(userDetail.ImageUrl, "Image");
                            }
                            ImageBuilder.Current.Build(imgBytes, fullPath, r);
                            new Misc().UploadFile(fullPath, fileName, "Image");
                            // update db
                            userDetail.ImageUrl = fileName;
                            context.SaveChanges();
                            System.IO.File.Delete(fullPath);
                            return Boolean.TrueString;
                        }
                        catch (Exception e)
                        {
                            return e.Message;
                        }
                        context.SaveChanges();
                    }
                    //////////////////////////////////////////////////////////////////////////////////////////////////////
                    return Boolean.TrueString;
                } // using
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId=" + profileId + " " + Newtonsoft.Json.JsonConvert.SerializeObject(userData), e);
                return Boolean.FalseString;
            }

        }// SaveUserProfileWeb

        /// <summary>
        /// Saves user profile image on server  , compress it
        /// </summary>
        /// <param name="userData"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public string SaveUserProfileImage(HttpPostedFileBase userData, int profileId)
        {


            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (profileId == 0) return Boolean.FalseString;
                    var userDetail = context.UserDetails.FirstOrDefault(p => p.ProfileId == profileId);

                    if (userDetail == default(UserDetail)) return Boolean.FalseString;
                    if (userData == null) return Boolean.FalseString;
                    // generate path for saving image
                    var path = ConfigurationReader.ImagePathKey;

                    if (string.IsNullOrEmpty(path)) return Boolean.FalseString;

                    var airHelper = new AircraftHelper();
                    var fileName = airHelper.GenerateUniqueImageName();
                    var fullPath = path + "/" + fileName;

                    // convert httpPostedFileBase to bytes array
                    Stream imgStream = userData.InputStream;
                    var memoryStream = new MemoryStream();
                    imgStream.CopyTo(memoryStream);
                    byte[] imgBytes = memoryStream.ToArray();

                    // resize image

                    // resize width and height
                    var r = new ResizeSettings { MaxHeight = 300, MaxWidth = 300 };

                    try
                    {
                        // using ImageResizer nuget package , resize the image

                        // resize image
                        ImageResizer.ImageBuilder.Current.Build(imgBytes, fullPath, r);

                        // update db
                        userDetail.ImageUrl = fileName;
                        context.SaveChanges();

                        return Boolean.TrueString;
                    }
                    catch (Exception ex)
                    {
                        //ExceptionHandler.ReportError(ex);
                        //logger.Fatal("Exception ProfileId = " + profileId, ex);
                        return ex.Message;

                    }
                }// using
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId = " + profileId, e);
                return Boolean.FalseString;
            }

        }// saveUserProfileImage

        /// <summary>
        /// Update and saves license for web
        /// </summary>
        /// <param name="saveLicense"></param>
        /// <returns></returns>
        public List<LicensesModelWeb> SaveLicenseWeb(License saveLicense)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (saveLicense != default(License))
                    {
                        var licenseToEdit = context.Licenses.FirstOrDefault(m => m.Id == saveLicense.Id);

                        if (saveLicense.Id != null && licenseToEdit != default(License))
                        {
                            // update

                            if (!licenseToEdit.Deleted)
                            {
                                licenseToEdit.LastUpdated = saveLicense.LastUpdated;
                                licenseToEdit.ReferenceNumber = saveLicense.ReferenceNumber;
                                licenseToEdit.Type = saveLicense.Type;
                                licenseToEdit.ValidUntil = saveLicense.ValidUntil;

                                //context.SaveChanges();
                            }
                        }
                        else
                        {
                            // save new license
                            var objLicense = new License
                            {
                                LastUpdated = saveLicense.LastUpdated,
                                ProfileId = saveLicense.ProfileId,
                                ReferenceNumber = saveLicense.ReferenceNumber,
                                Type = saveLicense.Type,
                                ValidUntil = saveLicense.ValidUntil,
                                UniqueId = DateTime.UtcNow.Ticks,
                            };

                            context.Licenses.Add(objLicense);
                            //context.SaveChanges();

                        }
                        var profile = context.Profiles.FirstOrDefault(f => f.Id == saveLicense.ProfileId);
                        if (profile != null)
                        {
                            profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                        }
                        context.SaveChanges();
                        return GetLicenseList(saveLicense.ProfileId);
                    }

                }
                return GetLicenseList(saveLicense.ProfileId);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(saveLicense), e);
                return GetLicenseList(saveLicense.ProfileId);
            }

        }

        /// <summary>
        /// Saves and updates ratings for web
        /// </summary>
        /// <param name="saveRatings"></param>
        /// <returns></returns>
        public List<LicensesModelWeb> SaveRatingsWeb(Rating saveRatings, int profileId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var ratingsToEdit = context.Ratings.FirstOrDefault(rt => rt.Id == saveRatings.Id);

                    if (saveRatings.Id != 0 && ratingsToEdit != default(Rating))
                    {
                        // update
                        if (!ratingsToEdit.Deleted)
                        {
                            ratingsToEdit.LastUpdated = saveRatings.LastUpdated;
                            ratingsToEdit.Type = saveRatings.Type;
                            ratingsToEdit.ValidUntil = saveRatings.ValidUntil;
                            //context.SaveChanges();
                        }
                    }
                    else
                    {
                        // save new license
                        Rating objRatings = new Rating
                        {
                            LastUpdated = saveRatings.LastUpdated,
                            LicenseId = saveRatings.LicenseId,
                            Type = saveRatings.Type,
                            ValidUntil = saveRatings.ValidUntil,
                            UniqueId = DateTime.UtcNow.Ticks,
                        };

                        context.Ratings.Add(objRatings);
                        //context.SaveChanges();
                    }

                    var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                    if (profile != null)
                    {
                        profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                    }

                    context.SaveChanges();

                    return GetLicenseList(profileId);
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(saveRatings), e);
                return GetLicenseList(profileId);
            }

        }

        /// <summary>
        /// Gets license Id for a rating
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetLicenseIdFromRatingId(int? id)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (id != 0)
                    {
                        var rateJoinlicense = from r in context.Ratings
                                              join l in context.Licenses
                                                  on r.LicenseId equals l.Id
                                              where r.Id == id && !r.Deleted && !l.Deleted
                                              select l;

                        //var license = context.Licenses.Select(l => l.Ratings.FirstOrDefault(r => r.LicenseId == id));

                        if (rateJoinlicense != default(License))
                        {
                            return rateJoinlicense.FirstOrDefault().Id;
                        }

                    }

                    return 0;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return 0;
            }

        }

        /// <summary>
        /// Get ratings for a rating Id , web
        /// </summary>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public RatingModel GetRatingsToEditWeb(int? ratingId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (ratingId != 0)
                    {
                        var rating = context.Ratings.FirstOrDefault(r => r.Id == ratingId && !r.Deleted);

                        if (rating != default(Rating))
                        {
                            return new RatingModel
                            {
                                Id = rating.Id,
                                Type = rating.Type,
                                //ValidUntil = Misc.GetStringOnlyDate(rating.ValidUntil),
                                ValidUntil = Misc.GetStringOnlyDateUS(rating.ValidUntil), //DateFormateChange
                                //UniqueId = rating.UniqueId == null ? default(long) : (long)rating.UniqueId,
                            };
                        }
                    }
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return new RatingModel();
        }

        /// <summary>
        /// Get license for a license Id , web
        /// </summary>
        /// <param name="licenseId"></param>
        /// <returns></returns>
        public LicensesModel GetLicenseToEditWeb(int? licenseId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    if (licenseId != 0)
                    {
                        var license = context.Licenses.FirstOrDefault(r => r.Id == licenseId && !r.Deleted);

                        if (license != default(License))
                        {
                            return new LicensesModel
                            {
                                Id = license.Id,
                                Type = license.Type,
                                //ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil),
                                ValidUntil = Misc.GetStringOnlyDateUS(license.ValidUntil),
                                ReferenceNumber = license.ReferenceNumber,
                                //UniqueId = license.UniqueId == null ? default(long) : (long)license.UniqueId,
                            };
                        }
                    }
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return new LicensesModel();

        }

        /// <summary>
        /// Delete rating for given rating Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<LicensesModelWeb> DeleteRatings(int? id, int profileId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var rate = context.Ratings.FirstOrDefault(m => m.Id == id && !m.Deleted);
                    if (rate != default(Rating))
                    {

                        var profile = context.Profiles.FirstOrDefault(f => f.Id == rate.License.ProfileId);
                        if (profile != null)
                        {
                            profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                        }
                        context.Ratings.Remove(rate);
                        context.SaveChanges();
                    }
                }
                return GetLicenseList(profileId);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new List<LicensesModelWeb>();
            }

        }

        /// <summary>
        /// Delete license for given license Id web
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<LicensesModelWeb> DeleteLicense(int? id, int profileId)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var license = context.Licenses.FirstOrDefault(m => m.Id == id);
                    if (license != null)
                    {
                        var ratingList = context.Ratings.Where(r => r.LicenseId == id).ToList();
                        if (ratingList.Count != 0)
                        {
                            context.Ratings.RemoveRange(ratingList);
                        }

                        var profile = context.Profiles.FirstOrDefault(f => f.Id == license.ProfileId);
                        if (profile != null)
                        {
                            profile.LastUpdateDateForLicenseRating = DateTime.UtcNow;
                        }
                        context.Licenses.Remove(license);
                    }
                    context.SaveChanges();
                    return GetLicenseList(profileId);
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new List<LicensesModelWeb>();
            }
        }

        /// <summary>
        /// Gets ratings list for a given license
        /// </summary>
        /// <param name="licenseId"></param>
        /// <returns></returns>
        public List<RatingModelWeb> GetRatingsList(int? licenseId)
        {
            try
            {
                if (licenseId > 0)
                {
                    using (var context = new GuardianAvionicsEntities())
                    {
                        Misc objMisc = new Misc();
                        var ratingsList = context.Ratings.Where(m => m.LicenseId == licenseId && !m.Deleted).ToList();
                        if (ratingsList != default(List<Rating>))
                        {
                            var rList = new List<RatingModelWeb>();
                            foreach (var rate in ratingsList)
                            {
                                rList.Add(new RatingModelWeb
                                {
                                    TId = objMisc.Encrypt(rate.Id.ToString()),
                                    Type = rate.Type,
                                    //ValidUntil = Misc.GetStringOnlyDate(rate.ValidUntil),
                                    ValidUntil = rate.ValidUntil == null ? string.Empty : ((DateTime)rate.ValidUntil).ToShortDateString(), //DateFormateChange
                                });
                            }
                            return rList;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
            return new List<RatingModelWeb>();
        }

        /// <summary>
        /// Gets user profile image url from db or default image
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <returns></returns>
        public dynamic setUserProfileImageSource(int userProfileId)
        {
            try
            {
                if (userProfileId != default(int))
                {
                    using (var context = new GuardianAvionicsEntities())
                    {
                        var objUser = context.UserDetails.FirstOrDefault(p => p.ProfileId == userProfileId);

                        if (objUser != default(UserDetail))
                        {
                            var fileName = objUser.ImageUrl;
                            string path = ConfigurationReader.ImageServerPathKey;

                            if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(path))
                                return (path + "/" + fileName);
                            //else
                            //    return "~/Image/GA/profile_pic.png";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return string.Empty;
            }
            return Constants.UserProfileDefaultImage;
        }// setAircraftImageSource

        public GetUserNameByEmailResponseModel GetUserNameByEmailId(GetUserNameByEmailRequestModel request)
        {
            var Context = new GuardianAvionicsEntities();
            var response = new GetUserNameByEmailResponseModel();
            try
            {
                var profile = Context.Profiles.FirstOrDefault(p => p.EmailId == request.EmailId);
                if (profile == null)
                {
                    response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.EmailIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.RegistrationReturnCodes.EmailIdNotFound.GetStringValue();
                }
                else
                {
                    response.FirstName = string.IsNullOrEmpty(profile.UserDetail.FirstName)
                        ? string.Empty
                        : profile.UserDetail.FirstName;
                    response.LastName = string.IsNullOrEmpty(profile.UserDetail.LastName)
                        ? string.Empty
                        : profile.UserDetail.LastName;
                    response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                    response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = e.Message;
            }
            return response;
        }


        public string GetUserNameByProfileId(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var userdetails = context.UserDetails.FirstOrDefault(f => f.ProfileId == profileId);
            if (userdetails != null)
            {
                return userdetails.FirstName;
            }
            return "";
        }

        #endregion user profile


        #region Pilot logbook


        /// <summary>
        /// Saves and updates pilot log book , if aircraft is not created for pilot log then it create aircraft.
        /// </summary>
        /// <param name="logbook"></param>
        /// <returns></returns>
        /// 
        public AirFrameDatalogResponse GetAirframeDatalog(AirframeDataLogRequestModel airframeDatalog)
        {
            var response = new AirFrameDatalogResponse();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "pilotlogId";
                param[0].SqlDbType = SqlDbType.Int;
                //param[0].Value = 7163;
                param[0].Value = airframeDatalog.PilotLogId;
                List<AirFrameLogValue> airframelog = new List<AirFrameLogValue>();
                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "getAircraftDatalogByPilotLogId", param);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AirFrameLogValue table = new AirFrameLogValue();
                    dynamic data= JObject.Parse(dr[0].ToString());
                    table.Latitude = Convert.ToDouble(data["Latitude"]);
                    table.Longitude = Convert.ToDouble(data["Longitude"]);
                    table.Speed = Convert.ToDouble(data["Speed"]);
                    table.Date = Convert.ToDateTime(data["Date"]);
                    table.Altitude = Convert.ToDouble(data["Altitude"]);
                    if (data["Heading"] != null)
                    {
                        table.Heading = Convert.ToDouble(data["Heading"]);
                    }
                    airframelog.Add(table);
                }
                response.AirFrameDataLogs = airframelog;
                response.ResponseCode = "0";
                response.ResponseMessage = "Success";
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception Occur";
            }
            return response;
        }


        public LogBookResponseModel SaveLogBook(LogBookRequestModel logbook)
        {
            var response = new LogBookResponseModel
            {
                LogBookList = new List<LogBookModelWithAircraftDetail>(),
                LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow)
            };

            try
            {

                // validate parameters
                #region valdiation

                LogBookRequestModel logbookModelValidated;
                var objGen = ValidateLogBook(logbook, out logbookModelValidated);

                if (objGen.ResponseCode == ((int)Enumerations.Logbook.Exception).ToString())
                {
                    // ExceptionHandler.ReportError(new Exception("pilot log validation failed"), Newtonsoft.Json.JsonConvert.SerializeObject(logbook));
                    //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(logbook), "pilot log validation failed");
                    return response.Exception();
                }

                logbook = logbookModelValidated;

                #endregion valdiation

                using (var context = new GuardianAvionicsEntities())
                {
                    LogBookModelWithAircraftDetail responseLog;

                    if (logbook.LogBookList.Any())
                    {
                        foreach (var lbObj in logbook.LogBookList)
                        {
                            responseLog = new LogBookModelWithAircraftDetail();

                            // validate logbook
                            if (lbObj == default(LogBookModel))
                            {
                                //logger.Fatal("Exception log of pilotlog service " + Newtonsoft.Json.JsonConvert.SerializeObject(logbook));
                                return response.Exception();
                            }

                            //#region aircraft

                            //#endregion aircraft

                            #region pilot log

                            // check if tag is saved

                            // initialised pilot log for saving in dropbox
                            // ReSharper disable once RedundantAssignment
                            var log = new PilotLog();
                            log = context.PilotLogs.FirstOrDefault(pl => pl.Id == lbObj.Id);

                            if (log != default(PilotLog))
                            {

                                // update log book
                                log.Actual = CheckDayPicFormat(lbObj.Actual);

                                log.CrossCountry = CheckDayPicFormat(lbObj.CrossCountry);
                                //log.Date = Misc.GetDate(lbObj.Date) ?? DateTime.UtcNow;

                                log.DayPIC = CheckDayPicFormat(lbObj.DayPIC);
                                //  log.Finished = lbObj.Finished;
                                log.Hood = CheckDayPicFormat(lbObj.Hood);
                                log.IFRAppchs = lbObj.IFRAppchs;
                                log.LastUpdated = DateTime.UtcNow;
                                log.NightPIC = CheckDayPicFormat(lbObj.NightPIC);
                                log.Remark = lbObj.Remark;
                                log.Route = lbObj.Route;
                                log.Sim = CheckDayPicFormat(lbObj.Sim);
                                log.UniqeId = lbObj.UniqueId;

                                context.SaveChanges();

                                responseLog.Id = log.Id;
                                responseLog.UniqueId = log.UniqeId ?? default(long);
                            }

                            #endregion pilot log

                            // check if this responseLog had new aircraft profile for it or a new tag for it.

                            //if (lbObj.AircraftId < 1 || lbObj.Id < 1)
                            response.LogBookList.Add(responseLog);

                            //#region dropbox

                        } //for each loop
                    }

                    // send updated logs in response 
                    #region response

                    var udate = Misc.GetDate(logbook.LastUpdateDate) ?? DateTime.MinValue;
                    var profile = context.Profiles.FirstOrDefault(p => p.Id == logbook.ProfileId);


                    var uLogList = new List<PilotLog>();

                    if (logbook.LastUpdateDate == null)
                    {
                        uLogList = context.PilotLogs.Where(p => (p.PilotId == logbook.ProfileId || p.CoPilotId == logbook.ProfileId || p.AircraftProfile.OwnerProfileId == logbook.ProfileId) && !p.Deleted).Include(i => i.AircraftProfile)
                            .OrderByDescending(o => o.Date).ToList();
                    }
                    else
                    {
                        uLogList = context.PilotLogs.Where(p => (p.PilotId == logbook.ProfileId || p.CoPilotId == logbook.ProfileId || p.AircraftProfile.OwnerProfileId == logbook.ProfileId) && p.LastUpdated >= udate && !p.Deleted).Include(i => i.AircraftProfile)
                            .OrderByDescending(o => o.Date).ToList();
                    }



                    List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                    aircraftMappedWithManufacturerUserList = new UnitDataHelper().GetAircraftListForManufacturerUser(logbook.ProfileId);

                    var aircraftIdLogs = context.PilotLogs.Where(w => w.PilotId == logbook.ProfileId || w.CoPilotId == logbook.ProfileId).Select(s => s.AircraftId).Distinct().ToList();
                    var aList = context.AircraftProfiles.Where(ap => aircraftMappedWithManufacturerUserList.Contains(ap.Id) || aircraftIdLogs.Contains(ap.Id) || context.MappingAircraftAndPilots
                        .Where(map => map.ProfileId == logbook.ProfileId && !map.Deleted)
                        .Select(s => s.AircraftId)
                        .ToList()
                        .Contains(ap.Id) && !ap.Deleted).Select(s => s.Id).ToList();

                    uLogList = uLogList.Where(p => aList.Contains(p.AircraftId)).ToList();

                    if (uLogList.Any())
                    {
                        if (logbook.LastUpdateDate != null)
                        {
                            var uRemoveLog = new List<PilotLog>();
                            uRemoveLog.AddRange(uLogList.Where(log => response.LogBookList.Any(r => r.Id == log.Id)));
                            foreach (var a in uRemoveLog)
                                uLogList.Remove(a);
                        }
                        response.LogBookList = new List<LogBookModelWithAircraftDetail>();

                        // add all the logs of uLogList into response.LogBookList
                        var aircraftProfile = new AircraftProfile();
                        foreach (var log in uLogList)
                        {
                            // if not added in response add this log in response
                            responseLog = new LogBookModelWithAircraftDetail();
                            responseLog = ConvertPilotLogToLogBookModel(log);
                            aircraftProfile = context.AircraftProfiles.FirstOrDefault(a => a.Id == log.AircraftId);
                            if (aircraftProfile != null)
                            {
                                //responseLog.Airacraft = AircraftProfileResponseModel.Create(aircraftProfile, null);
                                responseLog.AircraftId = aircraftProfile.Id;
                            }

                            response.LogBookList.Add(responseLog);
                        }
                    }
                    #endregion response
                } // using

                response.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);
                response.ResponseCode = ((int)Enumerations.Logbook.Success).ToString();
                response.ResponseMessage = Enumerations.Logbook.Success.GetStringValue();
                return response;
            }
            catch (Exception e)
            {
                response.ResponseCode = ((int)Enumerations.Logbook.Exception).ToString();
                response.ResponseMessage = Enumerations.Logbook.Exception.GetStringValue();
                return response;
            }
        }


        public GeneralResponse UpdateLogWithPilotAndCoPilot(UpdateLogBookForPilotAndCopilot model)
        {
            var context = new GuardianAvionicsEntities();
            var response = new GeneralResponse();

            try
            {
                var log = context.PilotLogs.FirstOrDefault(p => p.Id == model.logBookModel.Id);
                var objprofile = context.Profiles.FirstOrDefault(p => p.Id == model.logBookModel.ProfileId && !p.Deleted);
                if (log == null)
                {
                    response.ResponseCode = ((int)Enumerations.Logbook.InvalidLogId).ToString();
                    response.ResponseMessage = Enumerations.Logbook.InvalidLogId.GetStringValue();
                    return response;
                }
                if (objprofile == null)
                {
                    response.ResponseCode = ((int)Enumerations.Logbook.ProfileIdNotFound).ToString();
                    response.ResponseMessage = Enumerations.Logbook.ProfileIdNotFound.GetStringValue();
                    return response;
                }
                if (!string.IsNullOrEmpty(model.logBookModel.PilotEmailId) &&
                    !string.IsNullOrEmpty(model.logBookModel.CoPilotEmailId) &&
                    model.logBookModel.PilotEmailId == model.logBookModel.CoPilotEmailId)
                {
                    response.ResponseCode = ((int)Enumerations.Logbook.PilotAndCoPilotEmailIdCannotBeSame).ToString();
                    response.ResponseMessage = Enumerations.Logbook.PilotAndCoPilotEmailIdCannotBeSame.GetStringValue();
                    return response;
                }

                log.Actual = CheckDayPicFormat(model.logBookModel.Actual);

                if (log.Actual == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for Actual";
                    return response;
                }
                log.CrossCountry = CheckDayPicFormat(model.logBookModel.CrossCountry);
                if (log.CrossCountry == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for CrossCountry";
                    return response;
                }

                log.DayPIC = CheckDayPicFormat(model.logBookModel.DayPIC);
                if (log.DayPIC == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for DayPIC";
                    return response;
                }

                log.Hood = CheckDayPicFormat(model.logBookModel.Hood);
                if (log.Hood == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for Hood";
                    return response;
                }

                log.NightPIC = CheckDayPicFormat(model.logBookModel.NightPIC);
                if (log.NightPIC == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for NightPIC";
                    return response;
                }

                log.Sim = CheckDayPicFormat(model.logBookModel.Sim);
                if (log.Sim == "Invalid Format")
                {
                    response.ResponseCode = "101";
                    response.ResponseMessage = "Invalid Time format for Sim";
                    return response;
                }
                log.Date = Misc.GetDate(model.logBookModel.Date) ?? DateTime.UtcNow;

                //log.Finished = model.logBookModel.Finished;

                log.IFRAppchs = model.logBookModel.IFRAppchs;
                log.LastUpdated = DateTime.UtcNow;

                log.Remark = model.logBookModel.Remark;
                log.Route = model.logBookModel.Route;

                log.UniqeId = model.logBookModel.UniqueId;

                int? oldPilotId = log.PilotId;
                int? oldCoPilotId = log.CoPilotId;

                if (log.AircraftProfile.OwnerProfileId == model.logBookModel.ProfileId)
                {
                    if (!string.IsNullOrEmpty(model.logBookModel.PilotEmailId))
                    {
                        var profilePilot = context.Profiles.FirstOrDefault(p => p.EmailId == model.logBookModel.PilotEmailId);
                        if (profilePilot != null)
                        {
                            log.PilotId = profilePilot.Id;
                            new AircraftHelper().MappingAircraftAndPilot(log.AircraftId, profilePilot.Id);
                        }
                        else
                        {
                            return new GeneralResponse()
                            {
                                ResponseCode = ((int)Enumerations.Logbook.PilotEmailIdNotExist).ToString(),
                                ResponseMessage = Enumerations.Logbook.PilotEmailIdNotExist.GetStringValue()
                            };
                        }
                    }
                    else
                    {
                        log.PilotId = null;
                    }

                    if (oldPilotId != null)
                    {
                        if (oldPilotId != log.PilotId)
                        {
                            //Now update the profile for old pilot if exists
                            var oldPilotProfile = context.Profiles.FirstOrDefault(p => p.Id == oldPilotId);
                            if (oldPilotProfile != null)
                            {
                                oldPilotProfile.ClearAllData = true;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(model.logBookModel.CoPilotEmailId))
                    {
                        var profileOfCoPilot =
                            context.Profiles.FirstOrDefault(p => p.EmailId == model.logBookModel.CoPilotEmailId);
                        if (profileOfCoPilot != null)
                        {
                            log.CoPilotId = profileOfCoPilot.Id;
                            new AircraftHelper().MappingAircraftAndPilot(log.AircraftId, profileOfCoPilot.Id);
                        }
                        else
                        {
                            return new GeneralResponse()
                            {
                                ResponseCode = ((int)Enumerations.Logbook.CoPilotEmailIdNotExist).ToString(),
                                ResponseMessage = Enumerations.Logbook.CoPilotEmailIdNotExist.GetStringValue()
                            };
                        }
                    }
                    else
                    {
                        log.CoPilotId = null;
                    }

                    if (oldCoPilotId != null)
                    {
                        if (oldCoPilotId != log.CoPilotId)
                        {
                            //Now update the profile for old pilot if exists
                            var oldCoPilotProfile = context.Profiles.FirstOrDefault(p => p.Id == oldCoPilotId);
                            if (oldCoPilotProfile != null)
                            {
                                oldCoPilotProfile.ClearAllData = true;
                            }
                        }
                    }
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {

                return new GeneralResponse()
                {
                    ResponseCode = ((int)Enumerations.Logbook.Exception).ToString(),
                    ResponseMessage = Enumerations.Logbook.Exception.GetStringValue()
                };
            }
            return new GeneralResponse()
            {
                ResponseCode = ((int)Enumerations.Logbook.Success).ToString(),
                ResponseMessage = Enumerations.Logbook.Success.GetStringValue()
            };
        }

        /// <summary>
        /// Checks for two colons in string and converts all the substrings to integer , if they are not present returns Constants.DayPICDefaultValue (00:00:00)
        /// </summary>
        /// <param name="dayPic"></param>
        /// <returns></returns>
        private static string CheckDayPicFormat(string dayPic)
        {
            try
            {
                var array = dayPic.Split(':');

                if (array.Length != 3)
                    //return Constants.DayPICDefaultValue;
                    return "Invalid Format";

                int i;
                if (Int32.TryParse(array[0], out i) && Int32.TryParse(array[1], out i) && Int32.TryParse(array[2], out i))
                    return dayPic;

                //return Constants.DayPICDefaultValue;
                return "Invalid Format";
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, dayPic);
                //logger.Fatal("Exception dayPic =" + dayPic, e);
                //return Constants.DayPICDefaultValue;
                return "Invalid Format";
            }
        }

        /// <summary>
        /// Funtion of parameterised thread , used for uploading data on dropbox
        /// </summary>
        /// <param name="obj"></param>
        private void DropbBoxParameteres(object obj)
        {
            var logbook = (DropboxParameterisedThread)obj;
            try
            {
                new UnitDataHelper().UpdateDropBox(logbook.Id, logbook.ProfileId, new GuardianAvionicsEntities(), logbook.identity);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(obj), e);
            }
        }

        /// <summary>
        /// Converts pilotLogModel into LogBookModel
        /// </summary>
        /// <param name="log"></param>
        /// <param name="responseLog"></param>
        /// <returns></returns>
        public LogBookModelWithAircraftDetail ConvertPilotLogToLogBookModel(PilotLog log)
        {
            var responseLog = new LogBookModelWithAircraftDetail();
            try
            {
                //***
                responseLog.Actual = log.Actual;
                // responseLog.AircraftId = log.AircraftId;
                responseLog.CrossCountry = log.CrossCountry;
                responseLog.Date = Misc.GetStringOfDate(log.Date);
                responseLog.DayPIC = log.DayPIC;
                // responseLog.Finished = log.Finished;
                responseLog.Hood = log.Hood;
                responseLog.Id = log.Id;
                responseLog.IFRAppchs = log.IFRAppchs;
                responseLog.NightPIC = log.NightPIC;
                //responseLog.ProfileId = log.PilotId;
                responseLog.ProfileId = log.PilotId ?? 0; //Update 
                responseLog.Remark = log.Remark;
                responseLog.Route = log.Route;
                responseLog.Sim = log.Sim;
                responseLog.PilotEmailId = log.PilotId == null ? null : log.Profile.EmailId;
                responseLog.CoPilotEmailId = log.CoPilotId == null ? null : log.Profile1.EmailId;
                responseLog.AircraftId = log.AircraftId;
                responseLog.FlightId = log.FlightId ?? 0;

            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

            return responseLog;
        }

        /// <summary>
        /// Updates LogBookModel.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <param name="lbObj"></param>
        private void updateLogbook(PilotLog log, GuardianAvionicsEntities context, LogBookModel lbObj)
        {
            try
            {
                //***
                log.Actual = lbObj.Actual;
                log.CrossCountry = lbObj.CrossCountry;
                log.Date = Misc.GetDate(lbObj.Date) ?? DateTime.UtcNow;
                log.DayPIC = lbObj.DayPIC;
                // log.Finished = lbObj.Finished;
                log.Hood = lbObj.Hood;
                log.IFRAppchs = lbObj.IFRAppchs;
                log.LastUpdated = DateTime.UtcNow;
                log.NightPIC = lbObj.NightPIC;
                log.Remark = lbObj.Remark;
                log.Route = lbObj.Route;
                log.Sim = lbObj.Sim;

                context.SaveChanges();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }

        }

        /// <summary>
        /// Convert LogBookModel object to PilotLog object.
        /// </summary>
        /// <param name="lbObj"></param>
        /// <param name="aircraftId"></param>
        /// <returns></returns>
        private PilotLog SavePilotLog(LogBookModel lbObj, int aircraftId)
        {
            //if (lbObj.AircraftId < 1 && aircraftId < 1)
            //    return new PilotLog();

            PilotLog pLog = new PilotLog();
            try
            {
                pLog.Actual = CheckDayPicFormat(lbObj.Actual);
                if (pLog.Actual == "Invalid Format")
                {
                    pLog.Actual = "00:00:00";
                }

                pLog.CrossCountry = CheckDayPicFormat(lbObj.CrossCountry);
                if (pLog.CrossCountry == "Invalid Format")
                {
                    pLog.CrossCountry = "00:00:00";
                }

                pLog.DayPIC = CheckDayPicFormat(lbObj.DayPIC);
                if (pLog.DayPIC == "Invalid Format")
                {
                    pLog.DayPIC = "00:00:00";
                }

                pLog.Hood = CheckDayPicFormat(lbObj.Hood);
                if (pLog.Hood == "Invalid Format")
                {
                    pLog.Hood = "00:00:00";
                }

                pLog.NightPIC = CheckDayPicFormat(lbObj.NightPIC);
                if (pLog.NightPIC == "Invalid Format")
                {
                    pLog.NightPIC = "00:00:00";
                }

                pLog.Sim = CheckDayPicFormat(lbObj.Sim);
                if (pLog.Sim == "Invalid Format")
                {
                    pLog.Sim = "00:00:00";
                }

                pLog.Deleted = false;
                pLog.PilotId = lbObj.ProfileId;
                pLog.UniqeId = lbObj.UniqueId;
                pLog.Date = Misc.GetDate(lbObj.Date) ?? DateTime.UtcNow;
                pLog.Remark = lbObj.Remark;
                pLog.Route = lbObj.Route;
                pLog.IFRAppchs = lbObj.IFRAppchs;
                pLog.LastUpdated = DateTime.UtcNow;

                using (var context = new GuardianAvionicsEntities())
                {
                    context.PilotLogs.Add(pLog);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return new PilotLog();
            }

            return pLog;
        }

        /// <summary>
        /// validates profileId and profile existance
        /// for each logbook 
        ///     logbook id or unique id
        ///     aircraft id otherwise its aicraft creation so aircraft object and unique id in that object
        ///     Date , dayPic cant be null or empty    
        ///     Route cant be null it can be empty
        /// </summary>
        /// <param name="logbook"></param>
        /// <param name="outLogbook"></param>
        /// <returns></returns>
        private GeneralResponse ValidateLogBook(LogBookRequestModel logbook, out LogBookRequestModel outLogbook)
        {
            try
            {
                outLogbook = new LogBookRequestModel
                {
                    LastUpdateDate = logbook.LastUpdateDate,
                    ProfileId = logbook.ProfileId,
                    LogBookList = new List<LogBookModel>(),
                };

                if (logbook.ProfileId < 1)
                {
                    return new GeneralResponse
                    {
                        ResponseCode = ((int)Enumerations.Logbook.Exception).ToString()
                    };
                }

                using (var context = new GuardianAvionicsEntities())
                {
                    var profile = context.Profiles.FirstOrDefault(p => p.Id == logbook.ProfileId && !p.Deleted);
                    if (profile == default(Profile))
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Logbook.InvalidProfileId).ToString()
                        };
                    }
                }


                if (logbook.LogBookList.Any())
                {
                    foreach (var logbookObj in logbook.LogBookList)
                    {
                        var exception = logbookObj.ProfileId < 1;

                        if (logbookObj.Id < 1)
                            if (logbookObj.UniqueId == default(long))
                                exception = true;

                        // check for logbookObj.Airacraft != null  done on 7/28/14
                        //if (logbookObj.AircraftId < 1)
                        //    if (logbookObj.Airacraft != null && logbookObj.Airacraft.UniqueId == default(long))
                        //        exception = true;

                        if (String.IsNullOrEmpty(logbookObj.Date))
                            logbookObj.Date = Misc.GetStringOfDate((DateTime)SqlDateTime.MinValue);

                        if (String.IsNullOrEmpty(logbookObj.DayPIC))
                            logbookObj.DayPIC = Constants.DayPICDefaultValue;

                        if (logbookObj.Route == null)
                            logbookObj.Route = string.Empty;


                        if (exception)
                            return new GeneralResponse
                            {
                                ResponseCode = ((int)Enumerations.Logbook.Exception).ToString()
                            };

                        // if all conditions are satisfactory or we have edited Date , DayPIC or route we add edited logbookObj to response.
                        outLogbook.LogBookList.Add(logbookObj);
                    }
                }
            }
            catch (Exception e)
            {
                outLogbook = new LogBookRequestModel();
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(logbook), e);
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Logbook.Exception).ToString()
                };
            }

            // set values for out variable.
            outLogbook = logbook;

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.Logbook.Success).ToString()
            };
        }

        /// <summary>
        /// Gets the pilot log summary.
        /// </summary>
        /// <param name="profileId">Profile Id</param>
        /// <param name="pageNo">Page No.</param>
        /// <param name="totalCount">Total number of pilot logs</param>
        /// <returns>List of logbookmodelweb</returns>
        public List<LogBookModelWeb> GetPilotLogs(int profileId, int pageNo, out int totalCount)
        {
            try
            {
                totalCount = 0;
                if (profileId > 0)
                    using (var context = new GuardianAvionicsEntities())
                    {
                        var profile = context.Profiles.Include(a => a.PilotLogs).FirstOrDefault(p => p.Id == profileId);
                        if (profile != default(Profile))
                        {
                            //var plList = context.PilotLogs.Where(a => a.Profile.Id == profileId).OrderByDescending(a => a.Date);

                            var pilotLogs =
                                context.PilotLogs.Where(
                                    p =>
                                        p.PilotId == profileId || p.CoPilotId == profileId ||
                                        p.AircraftProfile.OwnerProfileId == profileId).OrderByDescending(a => a.Date).ToList();

                            totalCount = pilotLogs.Count();
                            if (pilotLogs.Count > 0)
                            {
                                //var pl = pilotLogs.AsEnumerable().OrderByDescending(a => a.Date);

                                var lbList = new List<LogBookModelWeb>();
                                var aircraft = new AircraftProfile();

                                foreach (var a in pilotLogs)
                                {
                                    //if (a.AircraftId != default(int))
                                    //{
                                    //    aircraft = context.AircraftProfiles.FirstOrDefault(ap => ap.Id == a.AircraftId);
                                    //    if (aircraft == default(AircraftProfile))
                                    //        aircraft = new AircraftProfile();
                                    //}
                                    lbList.Add(GetLogBookModelWeb(a));
                                }
                                return lbList;
                            }
                        }
                    }
                return new List<LogBookModelWeb>();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId=" + profileId, e);
                totalCount = 0;
                return new List<LogBookModelWeb>();
            }
        }

        /// <summary>
        /// Creates LogbookModelWeb class object from pilotlog object and aircraftProfile object
        /// </summary>
        /// <param name="log">Pilot log object</param>
        /// <param name="aircraft">AircraftProfile object</param>
        /// <returns>logbookmodelweb object</returns>

        //private LogBookModelWeb GetLogBookModelWeb(PilotLog log, AircraftProfile aircraft)
        private LogBookModelWeb GetLogBookModelWeb(PilotLog log)
        {
            var responseLog = new LogBookModelWeb();
            string[] arrTime;
            double timeInOneTenthOfHour = 0;
            responseLog.Actual = CheckDayPicFormat(log.Actual);
            if (responseLog.Actual == "Invalid Format" || string.IsNullOrEmpty(responseLog.Actual) || responseLog.Actual == "00:00:00")
            {
                responseLog.Actual = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.Actual.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.Actual = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.Actual = responseLog.Actual == "0" ? "00.0" : responseLog.Actual;
            }

            responseLog.CrossCountry = CheckDayPicFormat(log.CrossCountry);
            if (responseLog.CrossCountry == "Invalid Format" || string.IsNullOrEmpty(responseLog.CrossCountry) || responseLog.CrossCountry == "00:00:00")
            {
                responseLog.CrossCountry = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.CrossCountry.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.CrossCountry = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.CrossCountry = responseLog.CrossCountry == "0" ? "00.0" : responseLog.CrossCountry;
            }

            responseLog.DayPIC = CheckDayPicFormat(log.DayPIC);
            if (responseLog.DayPIC == "Invalid Format" || string.IsNullOrEmpty(responseLog.DayPIC) || responseLog.DayPIC == "00:00:00")
            {
                responseLog.DayPIC = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.DayPIC.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.DayPIC = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.DayPIC = responseLog.DayPIC == "0" ? "00.0" : responseLog.DayPIC;
            }

            responseLog.Hood = CheckDayPicFormat(log.Hood);
            if (responseLog.Hood == "Invalid Format" || string.IsNullOrEmpty(responseLog.Hood) || responseLog.Hood == "00:00:00")
            {
                responseLog.Hood = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.Hood.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.Hood = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.Hood = responseLog.Hood == "0" ? "00.0" : responseLog.Hood;
            }


            responseLog.NightPIC = CheckDayPicFormat(log.NightPIC);
            if (responseLog.NightPIC == "Invalid Format" || string.IsNullOrEmpty(responseLog.NightPIC) || responseLog.NightPIC == "00:00:00")
            {
                responseLog.NightPIC = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.NightPIC.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.NightPIC = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.NightPIC = responseLog.NightPIC == "0" ? "00.0" : responseLog.NightPIC;
            }

            responseLog.Sim = CheckDayPicFormat(log.Sim);
            if (responseLog.Sim == "Invalid Format" || string.IsNullOrEmpty(responseLog.Sim) || responseLog.Sim == "00:00:00")
            {
                responseLog.Sim = "00.0";
            }
            else
            {
                arrTime = null;
                arrTime = responseLog.Sim.Split(':');
                timeInOneTenthOfHour = ((Convert.ToDouble(arrTime[0]) * 60) + Convert.ToDouble(arrTime[1])) / 60;
                responseLog.Sim = (Math.Round(Convert.ToDouble(timeInOneTenthOfHour), 1, MidpointRounding.ToEven)).ToString(); // Rounds to even
                responseLog.Sim = responseLog.Sim == "0" ? "00.0" : responseLog.Sim;
            }

            responseLog.Finished = log.Finished;
            responseLog.Date = Misc.GetStringOnlyDateUS(log.Date) ?? string.Empty;
            responseLog.Time = Convert.ToDateTime(log.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            responseLog.Id = log.Id;
            responseLog.IFRAppchs = log.IFRAppchs ?? string.Empty;
            //ProfileId = log.PilotId,
            responseLog.ProfileId = log.PilotId; // after updation of pilot log table, set pilotId as nullable
            responseLog.Remark = log.Remark ?? string.Empty;
            responseLog.Route = log.Route ?? string.Empty;
            responseLog.ArivalTo = string.Empty;
            responseLog.DepartureFrom = string.Empty;
            responseLog.IfrAppchs1 = Convert.ToInt32(String.IsNullOrEmpty(log.IFRAppchs) ? "0" : log.IFRAppchs);
            responseLog.IfrAppchsList = new List<ListItems>();
            responseLog.CoPilotId = log.CoPilotId;
            responseLog.CoPilotEmailId = (log.Profile1 == null) ? "" : log.Profile1.EmailId;
            responseLog.PilotEmailId = (log.Profile == null) ? "" : log.Profile.EmailId;

            responseLog.IfrAppchsList = setIfrAppchsList();

            bool isFileExist = false;
            Stream str = new Misc().ReadFileFromS3("AudioFile", log.AircraftId.ToString() + "-" + log.FlightId.ToString() + ".m4a", out isFileExist);
            if (isFileExist)
            {
                responseLog.AudioFileName = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketAudioFilePath + log.AircraftId.ToString() + "-" + log.FlightId.ToString() + ".m4a";
            }
            else
            {
                responseLog.AudioFileName = "";
            }
            //if (File.Exists(ConfigurationReader.AudioFilePath + log.AircraftId.ToString() + "-" + log.FlightId.ToString() + ".m4a"))
            //{
            //    responseLog.AudioFileName = ConfigurationReader.ServerUrl + "/Audio/" + log.AircraftId.ToString() + "-" + log.FlightId.ToString() + ".m4a";
            //}
            //else
            //{
            //    responseLog.AudioFileName = "";
            //}

            // check if data is available for this flight.
            // set IsDataPresentForFlight variable

            var context = new GuardianAvionicsEntities();
            try
            {
                responseLog.IsDataPresentForFlight = context.AirframeDatalogs.Any(a => a.PilotLogId == log.Id);
            }
            catch (Exception e)
            {
                responseLog.IsDataPresentForFlight = false;
            }
            finally
            {
                context.Dispose();
            }

            // initialising variables , if we cant set values below

            try
            {
                if (log.Route != null)
                {
                    if (log.Route.Contains('-'))
                    {
                        var route = log.Route.Split('-');
                        var last = route.Length;

                        responseLog.DepartureFrom = route[0];
                        responseLog.ArivalTo = route[last - 1];
                    }
                }
            }
            catch (Exception e)
            {
                responseLog.ArivalTo = string.Empty;
                responseLog.DepartureFrom = string.Empty;
            }
            responseLog.AircraftModel = log.AircraftProfile.AircraftModelList == null ? string.Empty : log.AircraftProfile.AircraftModelList.ModelName;
            responseLog.AircraftRegistration = log.AircraftProfile.Registration ?? string.Empty;
            //responseLog.AircraftModel = aircraft.AircraftModelList == null ? string.Empty : aircraft.AircraftModelList.ModelName;
            //responseLog.AircraftRegistration = aircraft.Registration ?? string.Empty;

            return responseLog;
        }




        /// <summary>
        /// updates pilot log book
        /// </summary>
        /// <param name="lb"></param>
        public GeneralResponse UpdateLogBook(LogBookModelWeb lb)
        {
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    bool isPilotEmailValid = true;
                    bool isCoPilotEmailValid = true;

                    if (!string.IsNullOrEmpty(lb.PilotEmailId))
                    {
                        isPilotEmailValid = (context.Profiles.FirstOrDefault(f => f.EmailId == lb.PilotEmailId) != null);
                    }

                    if (!string.IsNullOrEmpty(lb.CoPilotEmailId))
                    {
                        isCoPilotEmailValid = (context.Profiles.FirstOrDefault(f => f.EmailId == lb.CoPilotEmailId) != null);
                    }


                    if (isPilotEmailValid == false && isCoPilotEmailValid == false)
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Logbook.BothPilotAndCoPilotEmailIdNotFound).ToString(),
                            ResponseMessage = Enumerations.Logbook.BothPilotAndCoPilotEmailIdNotFound.GetStringValue()
                        };
                    }
                    else if (!isPilotEmailValid)
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Logbook.PilotEmailIdNotFound).ToString(),
                            ResponseMessage = Enumerations.Logbook.PilotEmailIdNotFound.GetStringValue()
                        };
                    }
                    else if (!isCoPilotEmailValid)
                    {
                        return new GeneralResponse
                        {
                            ResponseCode = ((int)Enumerations.Logbook.CoPilotEmailIdNotFound).ToString(),
                            ResponseMessage = Enumerations.Logbook.CoPilotEmailIdNotFound.GetStringValue()
                        };
                    }

                    if (lb.Id > 0)
                    {
                        // valid log book
                        var log = context.PilotLogs.FirstOrDefault(p => p.Id == lb.Id);

                        if (log == default(PilotLog))
                        {
                            return new GeneralResponse
                            {
                                ResponseCode = ((int)Enumerations.Logbook.InvalidLogId).ToString(),
                                ResponseMessage = Enumerations.Logbook.InvalidLogId.GetStringValue()
                            };
                        }
                        else
                        {
                            int? oldPilotId = log.PilotId;
                            int? oldCoPilotId = log.CoPilotId;

                            if (log.AircraftProfile.OwnerProfileId == lb.ProfileId)
                            {

                                if (!string.IsNullOrEmpty(lb.PilotEmailId))
                                {
                                    var profilePilot = context.Profiles.FirstOrDefault(p => p.EmailId == lb.PilotEmailId);
                                    //log.PilotId = profile != null ? (int?)profile.Id : null;

                                    if (profilePilot != null)
                                    {
                                        log.PilotId = profilePilot.Id;
                                        //Check that this pilot id is mapped with the aircraft in the current log
                                        //var mappingAircraftAndPilot =
                                        //    context.MappingAircraftAndPilots.FirstOrDefault(
                                        //        m =>
                                        //            m.AircraftId == log.AircraftId && m.ProfileId == profilePilot.Id &&
                                        //            !m.Deleted);

                                        //if (mappingAircraftAndPilot != null)
                                        //{
                                        //    var obj = new MappingAircraftAndPilot();
                                        //    obj.AircraftId = log.AircraftId;
                                        //    obj.ProfileId = profilePilot.Id;
                                        //    obj.Deleted = false;
                                        //    obj.CreateDate = DateTime.UtcNow;
                                        //    context.MappingAircraftAndPilots.Add(obj);
                                        //}
                                        new AircraftHelper().MappingAircraftAndPilot(log.AircraftId, profilePilot.Id);
                                    }
                                    else
                                    {
                                        log.PilotId = null;
                                    }
                                }
                                else
                                {
                                    log.PilotId = null;
                                }

                                if (oldPilotId != null)
                                {
                                    if (oldPilotId != log.PilotId)
                                    {
                                        //Now update the profile for old pilot if exists
                                        var oldPilotProfile = context.Profiles.FirstOrDefault(p => p.Id == oldPilotId);
                                        if (oldPilotProfile != null)
                                        {
                                            oldPilotProfile.ClearAllData = true;
                                        }
                                    }
                                }


                                if (!string.IsNullOrEmpty(lb.CoPilotEmailId))
                                {
                                    var profileCoPilot = context.Profiles.FirstOrDefault(p => p.EmailId == lb.CoPilotEmailId);

                                    if (profileCoPilot != null)
                                    {
                                        log.CoPilotId = profileCoPilot.Id;

                                        //var mappingAircraftAndPilot =
                                        //    context.MappingAircraftAndPilots.FirstOrDefault(
                                        //        m =>
                                        //            m.AircraftId == log.AircraftId && m.ProfileId == profileCoPilot.Id &&
                                        //            !m.Deleted);
                                        //if (mappingAircraftAndPilot != null)
                                        //{
                                        //    var obj = new MappingAircraftAndPilot();
                                        //    obj.AircraftId = log.AircraftId;
                                        //    obj.ProfileId = profileCoPilot.Id;
                                        //    obj.Deleted = false;
                                        //    obj.CreateDate = DateTime.UtcNow;
                                        //    context.MappingAircraftAndPilots.Add(obj);
                                        //}
                                        new AircraftHelper().MappingAircraftAndPilot(log.AircraftId, profileCoPilot.Id);
                                    }
                                    else
                                    {
                                        log.CoPilotId = null;
                                    }
                                }
                                else
                                {
                                    log.CoPilotId = null;
                                }

                                if (oldCoPilotId != null)
                                {
                                    if (oldCoPilotId != log.CoPilotId)
                                    {
                                        //Now update the profile for old pilot if exists
                                        var oldCoPilotProfile = context.Profiles.FirstOrDefault(p => p.Id == oldCoPilotId);
                                        if (oldCoPilotProfile != null)
                                        {
                                            oldCoPilotProfile.ClearAllData = true;
                                        }
                                    }
                                }
                            }

                            //log.Actual = lb.Actual;
                            log.Actual = Misc.convertOneTenthTimeToHHMMSS(lb.Actual);

                            //log.CrossCountry = lb.CrossCountry;
                            log.CrossCountry = Misc.convertOneTenthTimeToHHMMSS(lb.CrossCountry);

                            log.Date = Misc.GetDateUS(lb.Date) ?? DateTime.UtcNow;
                            #region Adding Time to date

                            // for adding toTime to toDate

                            string[] tarray = lb.Time.Split(':');
                            log.Date = log.Date.Add(new TimeSpan(Convert.ToInt16(tarray[0]), Convert.ToInt16(tarray[1]), Convert.ToInt16(tarray[2])));

                            #endregion Adding Time to date

                            //log.DayPIC = lb.DayPIC;
                            log.DayPIC = Misc.convertOneTenthTimeToHHMMSS(lb.DayPIC);

                            log.Hood = lb.Hood;
                            log.Hood = Misc.convertOneTenthTimeToHHMMSS(lb.Hood);

                            log.IFRAppchs = lb.IfrAppchs1.ToString();

                            log.LastUpdated = DateTime.UtcNow;

                            log.NightPIC = lb.NightPIC;
                            log.NightPIC = Misc.convertOneTenthTimeToHHMMSS(lb.NightPIC);

                            log.Remark = lb.Remark;
                            log.Route = lb.Route;

                            log.Sim = lb.Sim;
                            log.Sim = Misc.convertOneTenthTimeToHHMMSS(lb.Sim);

                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception " + Newtonsoft.Json.JsonConvert.SerializeObject(lb), e);
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.Logbook.Exception).ToString(),
                    ResponseMessage = Enumerations.Logbook.Exception.GetStringValue()
                };
            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.Logbook.Success).ToString(),
                ResponseMessage = Enumerations.Logbook.Success.GetStringValue()
            };
        }

        /// <summary>
        /// Gets pilot log , from pilot log id
        /// </summary>
        /// <param name="id">pilotlog Id</param>
        /// <returns>LogbookModelWeb object</returns>
        public LogBookModelWeb EditPilotLog(int id, out double latitude, out double longitude)//, out string fileName)
        {

            LogBookModelWeb logBookModelWeb = new LogBookModelWeb();
            bool isDataPresentForFlight = false;
            latitude = 0.0;
            longitude = 0.0;
            //fileName = string.Empty;

            if (id == default(int))
                return new LogBookModelWeb();

            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var pilotLog = context.PilotLogs.Include(a => a.AircraftProfile).FirstOrDefault(pl => pl.Id == id);

                    var dataLogList = new List<DataLogListing>();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    //var airframedatalog = context.AirframeDatalogs.Where(a => a.PilotLogId == id).OrderBy(o => o.Id).Take(100).Select(s => s.DataLog);


                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter();
                    param[0].ParameterName = "pilotLog";
                    param[0].SqlDbType = SqlDbType.Int;
                    param[0].Value = id;

                    DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGet100DataLogByLogId", param);


                    //if (airframedatalog != null)
                    //{
                    //    foreach (var airframedata in airframedatalog)
                    //    {
                    //        dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                    //    }

                    //    var datalog = dataLogList.FirstOrDefault(d => d.Latitude != null && d.Longitude != null && d.Altitude != null);

                    //    if (datalog != null)
                    //    {
                    //        latitude = Convert.ToDouble(datalog.Latitude);
                    //        longitude = Convert.ToDouble(datalog.Longitude);

                    //    }
                    //}

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            isDataPresentForFlight = true;
                            var airframedatalog = ds.Tables[0].AsEnumerable()
                           .Select(r => r.Field<string>("DataLog"))
                           .ToList();

                            foreach (var airframedata in airframedatalog)
                            {
                                dataLogList.Add(jsonSerializer.Deserialize<DataLogListing>(airframedata));
                            }

                            var datalog = dataLogList.FirstOrDefault(d => d.Latitude != null && d.Longitude != null && d.Altitude != null);

                            if (datalog != null)
                            {
                                latitude = Convert.ToDouble(datalog.Latitude);
                                longitude = Convert.ToDouble(datalog.Longitude);
                            }
                        }
                    }

                    if (pilotLog == default(PilotLog))
                        return logBookModelWeb;

                    logBookModelWeb = GetLogBookModelWeb(pilotLog);
                    logBookModelWeb.IsDataPresentForFlight = isDataPresentForFlight;
                    return logBookModelWeb;
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
                return new LogBookModelWeb();
            }
        }

        /// <summary>
        /// Gets flight listing for profile Id
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public List<LogBookModelWeb> GetFlightList(int profileId)
        {
            try
            {
                if (profileId == default(int))
                    return new List<LogBookModelWeb>();

                //AircraftDetail
                using (var context = new GuardianAvionicsEntities())
                {
                    var pilotLogList =
                        context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || pl.CoPilotId == profileId) && pl.FlightDataType != null) //&& pl.Finished  remove condition
                            .Include(i => i.AircraftProfile)
                            .OrderByDescending(o => o.Date)
                            // .ThenByDescending(o => o.Id)
                            .ToList();
                    List<int> aircraftMappedWithManufacturerUserList = new List<int>();
                    aircraftMappedWithManufacturerUserList = new UnitDataHelper().GetAircraftListForManufacturerUser(profileId);


                    var aList = context.AircraftProfiles.Where(ap => ap.OwnerProfileId == profileId || aircraftMappedWithManufacturerUserList.Contains(ap.Id) || context.MappingAircraftAndPilots
                        .Where(map => map.ProfileId == profileId && !map.Deleted)
                        .Select(s => s.AircraftId)
                        .ToList()
                        .Contains(ap.Id) && !ap.Deleted).Select(s => s.Id).ToList();

                    pilotLogList = pilotLogList.Where(p => aList.Contains(p.AircraftId)).ToList();
                    //Check the pilot is owner or not. If owner then search by aircrafts last flight else direct search by pilotId
                    //var aircraftList = context.AircraftProfiles.Where(a => a.OwnerProfileId == profileId).Select(s => s.Id).ToList();
                    var count = pilotLogList.Count;
                    Misc objMisc = new Misc();

                    var plogs = pilotLogList.Select((s, index) => new LogBookModelWeb
                    {
                        SNO = count - index,
                        // Text = (count - index).ToString() + " Flight " + s.FlightId + " - " + s.AircraftProfile.Registration.ToUpper() + " - " + Misc.GetStringOnlyDateUS(s.Date) + " " + (s.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture) + " (Time : " + Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(s.DayPIC)) + ")",
                        IfrAppchs1 = 0,
                        IfrAppchsList = setIfrAppchsList(),
                        Id = s.Id,
                        FlightId = s.FlightId ?? 0,
                        AircraftRegistration = s.AircraftProfile.Registration,
                        Date = Misc.GetStringOnlyDateUS(s.Date) + " " + (s.Date).ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        TotalFlightDuration = s.FlightDataType == Convert.ToInt32(Enumerations.FlightDataType.LiveData) ? s.DayPIC : Misc.ConvertMinToOneTenthOFTime(objMisc.ConvertHHMMToMinutes(s.DayPIC)),
                        FlightDataType = ((Enumerations.FlightDataType)s.FlightDataType).ToString(),
                        Finished = s.Finished,
                        UniqeId = s.UniqeId ?? 0,
                        AircraftId = s.AircraftId
                    }).ToList();

                    if (plogs.Any())
                        return plogs;
                }
                return new List<LogBookModelWeb>();
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId=" + profileId, e);
                return new List<LogBookModelWeb>();
            }


        }

        public LogBookModelWeb GetLiveFlightByUserId(int profileId)
        {
            LogBookModelWeb response = new LogBookModelWeb();
            var context = new GuardianAvionicsEntities();
            var pilotLog = context.PilotLogs.FirstOrDefault(pl => (pl.PilotId == profileId || pl.AircraftProfile.OwnerProfileId == profileId || pl.CoPilotId == profileId) && pl.FlightDataType == (int)Enumerations.FlightDataType.LiveData && pl.Finished == false);
            if (pilotLog != null)
            {
                response.TId = new Misc().Encrypt(pilotLog.Id.ToString());
                response.AircraftId = pilotLog.AircraftId;
                response.UniqeId = pilotLog.UniqeId ?? 0;
            }
            return response;
        }

        /// <summary>
        /// sets select list for IFR Appchs
        /// </summary>
        /// <returns></returns>
        public List<ListItems> setIfrAppchsList()
        {
            var lst = new List<ListItems>();

            for (int i = 0; i <= 25; i++)
            {
                lst.Add(new ListItems
                {
                    text = i.ToString(),
                    value = i
                });
            }

            return lst;
        }

        /// <summary>
        /// Converts dayPic into proper time and adds it into date object.
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="dayPic">DayPIC in format 000:00:00 hours:minutes:seconds where hours can be greater than 23</param>
        /// <returns>sum of day pic and date , in standard format</returns>
        public static DateTime getEndDateTime(DateTime date, string dayPic)
        {
            try
            {
                var t = new TimeSpan();

                if (dayPic.Contains(':'))
                {   // day pic = 000:00:01
                    var hrMinSec = dayPic.Split(':');

                    var hour = Convert.ToInt32(hrMinSec[0]);
                    var min = Convert.ToInt32(hrMinSec[1]);
                    var sec = Convert.ToInt32(hrMinSec[2]);
                    var day = 0;

                    if (hour > 23)
                    {
                        day = hour / 24;
                        hour = hour % 24;
                    }

                    t = new TimeSpan(day, hour, min, sec);
                }
                else
                {
                    // day pic only contains seconds
                    TimeSpan.TryParse(dayPic, out t);
                }

                return date.Add(t);
            }
            catch (Exception)
            {
                return date;
            }
        }

        /// <summary>
        /// Converts DayPic into number of hours
        /// eg if dayPIC = 26:30:32 
        /// Equivalent hours = 26.5 hours
        /// </summary>
        /// <param name="dayPic"></param>
        /// <returns></returns>
        public string GetTimeInHoursFromDayPic(string dayPic)
        {
            try
            {
                if (dayPic.Contains(':'))
                {
                    // day pic = 000:00:01
                    var hrMinSec = dayPic.Split(':');

                    var hour = Convert.ToDouble(hrMinSec[0]);
                    var min = Convert.ToDouble(hrMinSec[1]);
                    var sec = Convert.ToDouble(hrMinSec[2]);
                    var day = 0;

                    double inHours = Math.Round((hour + min / 60 + sec / (60 * 60)), 2);

                    return inHours.ToString();
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception dayPic=" + dayPic, e);
                return string.Empty;
            }
        }


        #endregion Pilot logbook


        #region Dropbox


        /// <summary>
        /// Saves dropbox access token and token secret for user profile
        /// </summary>
        /// <param name="p"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool SaveDropboxAccessToken(string[] p, int profileId)
        {
            try
            {
                if (p[0] == null || p[1] == null)
                    return false;

                using (var context = new GuardianAvionicsEntities())
                {
                    var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);
                    {
                        if (preference == default(Preference))
                            return false;

                        preference.IsDropboxSync = true;
                        preference.DropBoxToken = p[0];
                        preference.DropBoxTokenSecret = p[1];

                        context.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId=" + profileId + " " + Newtonsoft.Json.JsonConvert.SerializeObject(p), e);
                return false;
            }

        }

        /// <summary>
        /// Gets the value for dropbox status : login or logout.
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool SetValueForDropBox(int profileId)
        {
            if (profileId == 0)
                return false;

            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);
                    {
                        if (preference == default(Preference))
                            return false;

                        return (preference.IsDropboxSync != null) && (bool)preference.IsDropboxSync;
                    }
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileId= " + profileId, e);
                return false;
            }
        }

        public List<ListItems> setViewBagAircraftListing(int profileId)
        {
            var detailList = new List<ListItems>();

            if (profileId > 0)
                using (var context = new GuardianAvionicsEntities())
                {
                    // Remove ProfileId From Table
                    var aList = context.AircraftProfiles.Where(ap => context.MappingAircraftAndPilots
                                                                            .Where(map => map.ProfileId == profileId && map.Deleted == true)
                                                                            .Select(s => s.AircraftId)
                                                                            .ToList()
                                                                            .Contains(ap.Id));
                    if (aList != default(List<ListItems>))
                    {
                        detailList.AddRange(from a in aList let make = a.AircraftManufacturer.Name let registration = a.Registration select new ListItems { value = a.Id, text = "Aircraft " + make + " " + registration });
                    }
                }

            return detailList;
        }
        /// <summary>
        /// disables dropbox sync
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool DropboxUnSync(int profileId)
        {
            if (profileId == 0)
                return false;
            try
            {
                using (var context = new GuardianAvionicsEntities())
                {
                    var preference = context.Preferences.FirstOrDefault(pr => pr.ProfileId == profileId);
                    {
                        if (preference == default(Preference))
                            return false;

                        preference.IsDropboxSync = false;
                        preference.LastUpdateDate = DateTime.UtcNow;
                        context.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception profileid=" + profileId, e);
                return false;
            }
        }


        #endregion Dropbox


        #region Pilot log summary


        /// <summary>
        /// Sets values for pilot summary for user
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public PilotSummaryModel GetPilotSummary(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var resposne = new PilotSummaryModel();
            try
            {
                // get user name and image.
                var userDetails = context.UserDetails.FirstOrDefault(ud => ud.ProfileId == profileId);
                if (userDetails == default(UserDetail))
                {
                    context.Dispose();
                    return resposne;
                }

                resposne.pilotName = (userDetails.FirstName ?? string.Empty) + " " + (userDetails.LastName ?? string.Empty);
                if (string.IsNullOrEmpty(userDetails.ImageUrl))
                    resposne.pilotImage = Constants.UserProfileDefaultImage;
                else
                    resposne.pilotImage = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + userDetails.ImageUrl;
                //resposne.pilotImage = ConfigurationReader.ImageServerPathKey + userDetails.ImageUrl;

                // set valid until date of medical certificate
                var medVaild = context.MedicalCertificates
                                      .Where(mc => mc.ProfileId == profileId
                                                && !mc.Deleted
                                                && mc.ValidUntil != null)
                                      .ToList()
                                      .OrderBy(od => od.ValidUntil)
                                      .ToList()
                                      .FirstOrDefault();

                if (medVaild == default(MedicalCertificate))
                {
                    resposne.medicalValidUntil = Constants.NotApplicable;
                    resposne.MedicalType = Constants.NotApplicable;
                }
                else
                {
                    if (medVaild.ValidUntil > System.DateTime.UtcNow.Date)
                        resposne.medicalValidUntil = Misc.GetStringOnlyDateUS(medVaild.ValidUntil);
                    else
                        resposne.medicalValidUntil = Constants.Expired;
                    resposne.MedicalType = medVaild.MedicalCertClass.Name;
                }

                // set BFR due date of license
                var licenseBFR = context.Licenses
                        .Where(li => li.ProfileId == profileId
                                   && !li.Deleted
                                   && li.ValidUntil != null)
                        .OrderBy(ob => ob.ValidUntil)
                        .ToList()
                        .FirstOrDefault();

                if (licenseBFR == default(License))
                {
                    resposne.bfrValidUnitl = Constants.NotApplicable;
                    resposne.type = Constants.NotApplicable;
                    resposne.referenceNumber = Constants.NotApplicable;
                }
                else
                {
                    if (licenseBFR.ValidUntil > System.DateTime.UtcNow.Date)
                    {
                        resposne.bfrValidUnitl = Misc.GetStringOnlyDateUS(licenseBFR.ValidUntil);
                    }
                    else
                    {
                        resposne.bfrValidUnitl = Constants.Expired;
                    }

                    resposne.type = licenseBFR.Type;
                    resposne.referenceNumber = licenseBFR.ReferenceNumber;
                }
                //resposne.bfrValidUnitl = (licenseBFR == default(License) ? string.Empty : Misc.GetStringOnlyDate(licenseBFR.ValidUntil));

                // set aircrft list
                resposne.aircraftList = new List<AircraftSummary>();
                resposne.aircraftList = context.AircraftProfiles
                                                        .Where(ap => context.MappingAircraftAndPilots
                                                                            .Where(map => map.ProfileId == profileId && map.Deleted == false)
                                                                            .Select(s => s.AircraftId)
                                                                            .ToList().Contains(ap.Id) && !ap.Deleted)
                                                        .Select(a => new AircraftSummary
                                                        {
                                                            aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + a.ImageUrl),
                                                            //aircraftImage = string.IsNullOrEmpty(a.ImageUrl) ? Constants.AircraftDefaultImage : (ConfigurationReader.ImageServerPathKey + @"/" + a.ImageUrl),
                                                            aircraftNNumber = a.Registration ?? string.Empty,
                                                            aircraftType = a.Make == null ? "N/A" : a.AircraftManufacturer.Name,
                                                            AircraftId = a.Id
                                                            //totalTachTime = (a.TachTime == null ? 0 : (double)a.TachTime) + (a.TachTimeOffset == null ? 0 : (double)a.TachTimeOffset),
                                                        }).ToList();
                // Pilot log related data

                // get all the pilot logs for the user
                var pilotLogs = context.PilotLogs.Where(pl => (pl.PilotId == profileId || pl.CoPilotId == profileId) && !pl.Deleted && pl.Finished && pl.DayPIC != null && pl.DayPIC != "")
                                .OrderByDescending(ob => ob.Date)
                                .ToList();

                if (pilotLogs.Count > 0)
                {
                    // last flight    
                    var lastFlight = pilotLogs.FirstOrDefault();

                    //resposne.lastFlight = Misc.GetStringOnlyDate(lastFlight.Date);
                    resposne.lastFlight = Misc.GetStringOnlyDateUS(lastFlight.Date);

                    // hours flown last 90 days

                    double totalTime = 0.0;
                    Misc objMisc = new Misc();
                    DateTime date = System.DateTime.UtcNow - new TimeSpan(90, 0, 0, 0);

                    pilotLogs.Where(pl => pl.Date >= date)
                           .ToList()
                           .ForEach(a => totalTime = totalTime + objMisc.ConvertHHMMToMinutes(a.DayPIC));

                    resposne.filghtFlownLast90Days = Misc.ConvertMinToOneTenthOFTime(totalTime);
                    totalTime = 0.0;
                    // total flight flown

                    pilotLogs.ForEach(a => totalTime = totalTime + objMisc.ConvertHHMMToMinutes(a.DayPIC));
                    //resposne.totalFlightTime = (int)totalTime.TotalHours;
                    resposne.totalFlightTime = Misc.ConvertMinToOneTenthOFTime(totalTime);
                }
                else
                {
                    resposne.lastFlight = Constants.NotApplicable;
                    resposne.filghtFlownLast90Days = "00:0 ";
                    resposne.totalFlightTime = "00:0";
                }

                context.Dispose();
                return resposne;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ProfileID=" + profileId, e);
                return new PilotSummaryModel();
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }
        }

        /// <summary>
        /// Converts DatePic into timeSpan
        /// </summary>
        /// <param name="dayPic"></param>
        /// <returns></returns>
        public TimeSpan ConvertDayPicToTimeSpan(string dayPic)
        {
            //ExceptionHandler.ReportError(new Exception(dayPic), "Value of dayPIC");
            try
            {
                var t = new TimeSpan();

                if (dayPic.Contains(':'))
                {   // day pic = 000:00:01
                    var hrMinSec = dayPic.Split(':');

                    var hour = Convert.ToInt32(hrMinSec[0]);
                    var min = Convert.ToInt32(hrMinSec[1]);
                    var sec = Convert.ToInt32(hrMinSec[2]);
                    var day = 0;

                    if (hour > 23)
                    {
                        day = hour / 24;
                        hour = hour % 24;
                    }

                    t = new TimeSpan(day, hour, min, sec);
                }
                else
                {
                    // day pic only contains seconds
                    TimeSpan.TryParse(dayPic, out t);
                }

                return t;
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e, " the value of dayPic is " + dayPic + "");
                //logger.Fatal("Exception dayPic= " + dayPic, e);
                return new TimeSpan();
            }

        }


        #endregion Pilot log summary

        #region FaceBook

        public NewLoginResponseModel verifyThirdPartyLogin(RegisterAndLoginByThirdParty objModel, bool isRequestByWeb)
        {
            NewLoginResponseModel objLoginResponse = new NewLoginResponseModel();
            var context = new GuardianAvionicsEntities();

            #region FB Validation

            if (string.IsNullOrEmpty(objModel.SocialLoginAccountId))
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.ThirdPartyIdCannotNullOrEmpty).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.ThirdPartyIdCannotNullOrEmpty.GetStringValue();
                return objLoginResponse;
            }
            //else if (string.IsNullOrEmpty(objModel.EmailId))
            //{
            //    objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.EmailIdCannotNullOrEmpty).ToString();
            //    objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.EmailIdCannotNullOrEmpty.GetStringValue();
            //    return objLoginResponse;
            //}
            if (objModel.EmailId == ""|| objModel.EmailId ==null)
            {
                var getuserDetail = context.Profiles.Where(x =>x.ThirdPartyId == objModel.SocialLoginAccountId).FirstOrDefault();//x.ThirdPartyAccountType == objModel.SocialLoginAccountType && 
                if (getuserDetail != null)
                {
                    objModel.EmailId = getuserDetail.EmailId;
                    var details = context.UserDetails.Where(x => x.ProfileId == getuserDetail.Id).FirstOrDefault();
                    if (details != null)
                    {
                        objModel.FirstName = details.FirstName;
                        objModel.LastName = details.LastName;
                    }
                }
            }
            

            if (string.IsNullOrEmpty(objModel.FirstName))
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.FirstNameNotFound).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.FirstNameNotFound.GetStringValue();
                return objLoginResponse;
            }

            if (string.IsNullOrEmpty(objModel.LastName))
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.LastNameNotFound).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.LastNameNotFound.GetStringValue();
                return objLoginResponse;
            }

            if (string.IsNullOrEmpty(Convert.ToString(objModel.SocialLoginAccountType)))
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.ThirdPartyAccountTypeCannotNullOrEmpty).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.ThirdPartyAccountTypeCannotNullOrEmpty.GetStringValue();
                return objLoginResponse;
            }


            if (objModel.SocialLoginAccountType < 1 || objModel.SocialLoginAccountType > 3)
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.IncorrectValueOfThirdPartyAccountType).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.IncorrectValueOfThirdPartyAccountType.GetStringValue();
                return objLoginResponse;
            }

            #endregion FB Validation
            var objProfile = context.Profiles.FirstOrDefault(p => p.EmailId == objModel.EmailId && !p.Deleted);

            if (objProfile == null)
            {
                //Here there is not any account registered with this emailid so here we are registering with this email id
                var profile = CommonHelper.GetProfileObject();
                var userDetail = new UserDetail();
                var userTypeId = context.UserMasters.FirstOrDefault(f => f.UserType == "User").Id;
                string password = Misc.RandomString(6);
                profile.EmailId = objModel.EmailId;
                profile.Password = new Misc().Encrypt(password);
                profile.IsBlocked = false;
                profile.ThirdPartyId = objModel.SocialLoginAccountId;
                profile.ThirdPartyAccountType = objModel.SocialLoginAccountType;
                profile.UserTypeId = userTypeId;
                profile.RecordId = (new Encryption().Encrypt((password + objModel.EmailId).ToLower(), Constants.GlobalEncryptionKey)).Substring(0, 16);

                profile.IsSubscriptionVisible = (ConfigurationReader.IsSubscriptionEnable == Boolean.TrueString) ? true : false;
                context.Profiles.Add(profile);
                context.SaveChanges();

                SetSubscriptionToUserForFreeUse(profile.Id);

                userDetail.FirstName = objModel.FirstName;
                userDetail.LastName = objModel.LastName;
                userDetail.ProfileId = profile.Id;
                userDetail.SecurityAnswer = string.Empty;
                userDetail.SecurityQuestionId = 1;
                userDetail.CompanyName = objModel.CompanyName;
                userDetail.City = objModel.City;
                userDetail.State = objModel.State;
                userDetail.StreetAddress = objModel.StreetAddress;
                userDetail.ZipCode = objModel.ZipCode;
                userDetail.PhoneNumber = objModel.PhoneNumber;
                userDetail.DateOfBirth = objModel.DateOfBirth;

                context.UserDetails.Add(userDetail);
                context.SaveChanges();

                var userPreference = CommonHelper.GetPreference();
                userPreference.ProfileId = profile.Id;

                context.Preferences.Add(userPreference);
                context.SaveChanges();

                objProfile = context.Profiles.FirstOrDefault(p => p.EmailId == objModel.EmailId && !p.Deleted);
                objLoginResponse.UserType = objProfile.UserMaster.UserType;
                objLoginResponse.ProfileId = profile.Id;
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.Success).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.Success.GetStringValue();
                objLoginResponse.FirstName = profile.UserDetail.FirstName;
                objLoginResponse.SocialLoginAccountType = profile.ThirdPartyAccountType ?? 0;
                objLoginResponse.EmailId = profile.EmailId;
                objLoginResponse.SocialLoginAccountId = profile.ThirdPartyId;
                objLoginResponse.Key = profile.RecordId;
                objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                objLoginResponse.CountryCode = profile.UserDetail.CountryCode;

            }
            else
            {
                if (objProfile.UserDetail != null)
                {
                    if (!string.IsNullOrEmpty(objProfile.UserDetail.ImageUrl))
                    {
                        objLoginResponse.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + objProfile.UserDetail.ImageUrl;
                    }
                    else
                    {
                        objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                    }
                }
                else
                {
                    objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
                }
                objLoginResponse.Key = objProfile.RecordId;
            }
            objLoginResponse.UserType = objProfile.UserMaster.UserType;
            if (objProfile.ThirdPartyId == null)
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithGuardian).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.AlreadyRegisterWithGuardian.GetStringValue();
                return objLoginResponse;
            }

            //Now check theat user is logged in with Gmail or FB
            //if (objProfile.ThirdPartyAccountType != objModel.SocialLoginAccountType)
            //{
            //    switch (objProfile.ThirdPartyAccountType)
            //    {

            //        case 1:
            //            {
            //                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithFaceBookAccount).ToString();
            //                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.AlreadyRegisterWithFaceBookAccount.GetStringValue();
            //            }
            //            break;
            //        case 2:
            //            {
            //                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithGmailAccount).ToString();
            //                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.AlreadyRegisterWithGmailAccount.GetStringValue();
            //            }
            //            break;
            //        case 3:
            //            {
            //                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.AlreadyRegisterWithAppleAccount).ToString();
            //                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.AlreadyRegisterWithAppleAccount.GetStringValue();
            //            }
            //            break;

            //    }
            //    return objLoginResponse;
            //}


            if (objProfile.IsBlocked)
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.UserBlocked).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.UserBlocked.GetStringValue();
                return objLoginResponse;
            }

            if (objProfile.Id == 1)
            {
                objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.AdminCannotLogin).ToString();
                objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.AdminCannotLogin.GetStringValue();
                return objLoginResponse;
            }


            objLoginResponse.ResponseCode = ((int)Enumerations.ThirdPartyLogin.Success).ToString();
            objLoginResponse.ResponseMessage = Enumerations.ThirdPartyLogin.Success.GetStringValue();
            objLoginResponse.ProfileId = objProfile.Id;
            objLoginResponse.FirstName = objProfile.UserDetail.FirstName;
            objLoginResponse.SocialLoginAccountType = objProfile.ThirdPartyAccountType ?? 0;

            if (isRequestByWeb)
            {
                return objLoginResponse;
            }


            ////////////////////////////////////////////////////////////////////////////////////////////

            //objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1)));
            objLoginResponse.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);


            var userData = context.UserDetails.FirstOrDefault(u => u.ProfileId == objProfile.Id);

            // ReSharper disable once PossibleNullReferenceException
            objLoginResponse.FirstName = userData.FirstName;
            objLoginResponse.LastName = userData.LastName;
            objLoginResponse.StreetAddress = userData.StreetAddress;
            objLoginResponse.City = userData.City;
            objLoginResponse.State = userData.State;
            objLoginResponse.ZipCode = userData.ZipCode.ToString();
            objLoginResponse.PhoneNumber = userData.PhoneNumber;
            objLoginResponse.DateOfBirth = Misc.GetStringOnlyDate(userData.DateOfBirth);
            objLoginResponse.CompanyName = userData.CompanyName;
            objLoginResponse.CountryCode = userData.CountryCode;
            objLoginResponse.EmailId = objModel.EmailId;
            // save token id



            // send user profile image
            if (!String.IsNullOrEmpty(userData.ImageUrl))
            {
                objLoginResponse.ImageName = ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + userData.ImageUrl;
            }
            else
            {
                objLoginResponse.ImageName = Constants.UserProfileDefaultImage;
            }


            //System.Diagnostics.Debug.Write("\n Time taken on login page linq " + DateTime.UtcNow.TimeOfDay + " " + DateTime.UtcNow.Millisecond + Environment.NewLine);

            // send medical certificates

            objLoginResponse.MedicalCertificateList = new List<MedicalCertificateModel>();

            objLoginResponse.MedicalCertificateList.AddRange(
                context.MedicalCertificates.Where(m => m.ProfileId == objProfile.Id && m.Deleted == false).ToList()
                    .Select(a => new MedicalCertificateModel
                    {
                        ReferenceNumber = a.ReferenceNumber,
                        Type = a.Type,
                        ValidUntil = Misc.GetStringOnlyDate(a.ValidUntil),
                        Id = a.Id,
                        UniqueId = a.UniqueId ?? default(long),
                        ClassId = a.MedicalCertClassId,
                        ClassName = a.MedicalCertClassId == null ? "" : a.MedicalCertClass.Name,
                    }
                           )
                    );


            // send licenses

            // objLoginResponse.LicensesList is null , initialise it with empty list.
            objLoginResponse.LicensesList = new List<LicensesModel>();

            objLoginResponse.LicensesList.AddRange
                (
                    context.Licenses.Where(l => l.ProfileId == objProfile.Id && l.Deleted == false).ToList()
                    .Select(license => new LicensesModel
                    {
                        Id = license.Id,
                        Ratingslist =
                                (context.Ratings.Where(r => r.LicenseId == license.Id && !r.Deleted)).ToList()
                                    .Select(a => new RatingModel
                                    {
                                        Id = a.Id,
                                        Type = a.Type,
                                        ValidUntil = Misc.GetStringOnlyDate(a.ValidUntil),
                                        UniqueId = a.UniqueId ?? default(long),
                                    }).ToList(),
                        ReferenceNumber = license.ReferenceNumber,
                        Type = license.Type,
                        ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil),
                        UniqueId = license.UniqueId ?? default(long),
                    })
                );


            var medicalCertificateList = (context.MedicalCertificates.Where(m => m.ProfileId == objProfile.Id && m.Deleted == false).Select(m => m)).ToList();
            objLoginResponse.MedicalCertificateList = new List<MedicalCertificateModel>();
            foreach (var certificate in medicalCertificateList)
            {
                objLoginResponse.MedicalCertificateList.Add(
                    new MedicalCertificateModel
                    {
                        ReferenceNumber = certificate.ReferenceNumber,
                        Type = certificate.Type,
                        ValidUntil = Misc.GetStringOnlyDate(certificate.ValidUntil),
                        Id = certificate.Id,
                        UniqueId = certificate.UniqueId ?? default(long),
                        ClassId = certificate.MedicalCertClassId,
                        ClassName = certificate.MedicalCertClassId == null ? "" : certificate.MedicalCertClass.Name,
                    }
                );
            }

            var lincenseList = (context.Licenses.Where(l => l.ProfileId == objProfile.Id && l.Deleted == false).Select(l => l)).ToList();

            objLoginResponse.LicensesList = new List<LicensesModel>();

            foreach (var license in lincenseList)
            {
                var ratingsList = (context.Ratings.Where(r => r.LicenseId == license.Id && r.Deleted == false).Select(l => l)).ToList();

                var objLicenseModel = new LicensesModel { Ratingslist = new List<RatingModel>(), Id = license.Id };

                foreach (var rating in ratingsList)
                {
                    objLicenseModel.Ratingslist.Add(
                    new RatingModel
                    {
                        Id = rating.Id,
                        Type = rating.Type,
                        ValidUntil = Misc.GetStringOnlyDate(rating.ValidUntil),
                        UniqueId = rating.UniqueId ?? default(long),
                    }
                    );
                }

                objLicenseModel.ReferenceNumber = license.ReferenceNumber;
                objLicenseModel.Type = license.Type;
                objLicenseModel.ValidUntil = Misc.GetStringOnlyDate(license.ValidUntil);
                objLicenseModel.UniqueId = license.UniqueId ?? default(long);

                objLoginResponse.LicensesList.Add(objLicenseModel);
            }

            // set deleted in response

            if (!String.IsNullOrEmpty(objModel.LastUpdateDateTime))
            {
                var LastUpdateddate = Misc.GetDate(objModel.LastUpdateDateTime);
                var deletedMedical = context.MedicalCertificates.Where(m => m.ProfileId == objProfile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();
                var deletedRate = context.Ratings.Where(m => m.License.ProfileId == objProfile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted && !m.License.Deleted).Select(m => m.Id).ToList();
                var deletedLicense = context.Licenses.Where(m => m.ProfileId == objProfile.Id && m.LastUpdated >= LastUpdateddate && m.Deleted).Select(m => m.Id).ToList();

                if (deletedLicense.Count > 0)
                    objLoginResponse.DeletedLicences = deletedLicense.ToArray();
                else
                    objLoginResponse.DeletedLicences = new int[] { };

                if (deletedMedical.Count > 0)
                    objLoginResponse.DeletedMedicalCertificate = deletedMedical.ToArray();
                else
                    objLoginResponse.DeletedLicences = new int[] { };

                if (deletedRate.Count > 0)
                    objLoginResponse.DeletedRatings = deletedRate.ToArray();
                else
                    objLoginResponse.DeletedLicences = new int[] { };
            }

            //logger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(objLoginResponse));
            return objLoginResponse;
        }

        //public LogBookResponseModel FaceBookLogin(UserModel objUSerModel)
        //{ 

        //}

        #endregion FaceBook




        // code used for testing

        #region code for testing

        public void Fill()
        {
            /*
            try
            {
                // MASTERANSI mast
                string fileName = "MASTERANSI.txt";
                string dirName = Path.GetDirectoryName(@"E:\test\");
                DataTable dt;

                using (OleDbConnection cn =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + fileName, cn))
                    {
                        dt = new DataTable("MASTER");
                        adapter.Fill(dt);
                    }
                }

                var airRef = dt.AsEnumerable()
                    .Select(o => new AircraftMasterFile
                    {
                        NNumber = o.Field<string>(0),
                        SERIALNUMBER = o.Field<string>(1),
                        MFR_MDL_CODE = o.Field<string>(2),
                        ManufactureYear = o.Field<string>(4),

                    }).ToList();

                var options = new BulkInsertOptions { 
                    NotifyAfter = 1000,
                };

                //options.Callback = new SqlRowsCopiedEventHandler(TestCallback);


                var context = new GuardianAvionicsEntities();
                context.BulkInsert(airRef);

                context.SaveChanges();

                context.Dispose();

                //SaveDataInChunks(airRef, new GuardianAvionicsEntities());

                //var context = new GuardianAvionicsEntities();
                //context.Configuration.AutoDetectChangesEnabled = false;
                //context.Configuration.ValidateOnSaveEnabled = false;

                //int count = 0;

                //foreach (var aRef in airRef)
                //{
                //    ++count;

                //    if (count == 319561) // 319000
                //    {
                //        break;
                //    }

                //    context = AddToContext(context, aRef, count);
                //}

                //context.SaveChanges();
                //context.Dispose();




            }
            catch (Exception e)
            {

            }
            
            try{

                string fileName1 = "ACFTREFANSI.txt";
                string dirName1 = Path.GetDirectoryName(@"E:\test\");
                DataTable dt1;

                using (OleDbConnection cn1 =
                    new OleDbConnection(
                                        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + dirName1 + ";" +
                "Extended Properties=\"text;HDR=Yes;FMT=Delimited;\""))
                {
                    // Open the connection 
                    cn1.Open();

                    // Set up the adapter 
                    using (OleDbDataAdapter adapter1 = new OleDbDataAdapter("SELECT * FROM " + fileName1, cn1))
                    {
                        dt1 = new DataTable("MASTER");
                        adapter1.Fill(dt1);
                    }
                }

                var airRef1 = dt1.AsEnumerable()
                    .Select(o => new AircraftReferenceFile
                    {
                        MFR_MDL_CODE = o.Field<string>(0),
                        ManufacturerName = o.Field<string>(1),
                        ModelName = o.Field<string>(2),
                        NumberOfEngine = o.Field<string>(7),

                    }).ToList();

                var context = new GuardianAvionicsEntities();
                context.BulkInsert(airRef1);

                context.SaveChanges();

                context.Dispose();

                //var context = new GuardianAvionicsEntities();
                //context.Configuration.AutoDetectChangesEnabled = false;
                //context.Configuration.ValidateOnSaveEnabled = false;

                //int count = 0;

                //foreach (var aRef in airRef)
                //{
                //    ++count;
                //    context = AddToContextRef(context, aRef, count);
                //}

                //context.SaveChanges();
                //context.Dispose();
            }
            catch (Exception e)
            {

            }*/
        }

        //private void TestCallback(object sender, SqlRowsCopiedEventArgs e)
        //{

        //}

        //private void TestCallback(object obj, SqlRowUpdatedEventArgs e)
        //{

        //}

        public void SaveDataInChunks(IEnumerable<AircraftMasterFile> myList, GuardianAvionicsEntities context)
        {
            int totalCount = myList.Count();
            int count = totalCount;
            int skip = 0;
            int take = 1000;

            if (totalCount < 1000)
            {
                context.AircraftMasterFiles.AddRange(myList);
                context.SaveChanges();
            }
            else
            {
                // go for skip and take

                /* Total count 5000
                    Skip    Count   Saved Records
                 *  0       5000    1000
                 *  1000    4000    + 1000 = 2000
                 *  2000    3000    + 1000 = 3000                 
                 *  3000    2000    + 1000 = 4000
                 *  4000    1000    + 1000 = 5000
                 *  5000    0       loop ends                                   
                 */

                /* Total count 5100
                    Skip    Count   Saved Records
                 *  0       5100    1000
                 *  1000    4100    + 1000 = 2000
                 *  2000    3100    + 1000 = 3000                 
                 *  3000    2100    + 1000 = 4000
                 *  4000    1100    + 1000 = 5000
                 *  5000    100     loop ends                                   
                 */

                while (count / 1000 > 0)
                {
                    var listToSave = myList.Skip(skip).Take(take);

                    context.AircraftMasterFiles.AddRange(listToSave);
                    context.SaveChanges();

                    skip = skip + 1000;
                    count = count - 1000;
                }

                if (count > 0)
                {
                    var listToSave = myList.Skip(skip).Take(count);
                    context.AircraftMasterFiles.AddRange(listToSave);
                    context.SaveChanges();
                }
            }
        }

        private GuardianAvionicsEntities AddToContextRef(GuardianAvionicsEntities context, AircraftReferenceFile aRef, int count)
        {
            context.AircraftReferenceFiles.Add(aRef);
            if (count % 1000 == 0)
            {
                context.SaveChanges();
                context.Dispose();
                context = new GuardianAvionicsEntities();
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
            }

            return context;
        }

        private GuardianAvionicsEntities AddToContext(GuardianAvionicsEntities context, AircraftMasterFile aRef, int count)
        {
            context.AircraftMasterFiles.Add(aRef);
            if (count % 2 == 0)
            {
                context.SaveChanges();
                context.Dispose();
                context = new GuardianAvionicsEntities();
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
            }

            return context;
        }

        #endregion code for testing

        // code used for testing




        /// <summary>
        /// Adds two values of the format of date (00:00:00) . In case of exception returns Default Day PIC.
        /// </summary>
        /// <param name="old"></param>
        /// <param name="toAdd"></param>
        /// <returns></returns>
        private string AddDates(string old, string toAdd)
        {
            try
            {
                old = CheckDayPicFormat(old);
                toAdd = CheckDayPicFormat(toAdd);

                // if one of the vales among old and toAdd is == Constants.DayPICDefaultValue , then don't add them
                //if (old == Constants.DayPICDefaultValue)
                if (old == "Invalid Format")
                {
                    return toAdd;
                }

                //if (toAdd == Constants.DayPICDefaultValue)
                if (toAdd == "Invalid Format")
                {
                    return old;
                }

                // neither old nor toAdd is == Constants.DayPICDefaultValue

                var oldArray = old.Split(':');
                var toAddArray = toAdd.Split(':');

                string value = string.Empty;
                int adder = 0;

                for (int i = 2; i > -1; i--)
                {
                    int con1, con2;

                    Int32.TryParse(oldArray[i], out con1);
                    Int32.TryParse(toAddArray[i], out con2);

                    if (i != 0)
                    {
                        //adder = (con1 + con2 + adder) % 60;
                        value = ":" + ((con1 + con2 + adder) % 60) + value;
                        adder = (con1 + con2 + adder) / 60;
                    }
                    else
                        value = (con1 + con2 + adder).ToString(CultureInfo.InvariantCulture) + value;
                }

                return value;
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
            }

            return Constants.DayPICDefaultValue;
        }

        /// <summary>
        /// Adds pilotLog object to FlyingSummaryOld object. (hood , sim , actual , day , night , crosscountry)
        /// </summary>
        /// <param name="pilotLog"></param>
        /// <param name="objmodel"></param>
        /// <returns></returns>
        public PreferenceModelWeb AddToOldFlyingSummary(PilotLog pilotLog, PreferenceModelWeb objmodel)
        {
            try
            {
                objmodel.FlyingSummaryOld.Hood = AddDates(objmodel.FlyingSummaryOld.Hood, pilotLog.Hood);
                objmodel.FlyingSummaryOld.Actual = AddDates(objmodel.FlyingSummaryOld.Actual, pilotLog.Actual);
                objmodel.FlyingSummaryOld.CrossCountry = AddDates(objmodel.FlyingSummaryOld.CrossCountry, pilotLog.CrossCountry);
                objmodel.FlyingSummaryOld.Sim = AddDates(objmodel.FlyingSummaryOld.Sim, pilotLog.Sim);
                objmodel.FlyingSummaryOld.Day = AddDates(objmodel.FlyingSummaryOld.Day, pilotLog.DayPIC);
                objmodel.FlyingSummaryOld.Night = AddDates(objmodel.FlyingSummaryOld.Night, pilotLog.NightPIC);
            }
            catch (Exception e)
            {
                //ExceptionHandler.ReportError(e);
                //logger.Fatal("Exception ", e);
            }
            return objmodel;
        }

        public double ConvertHHMMToMinutes(string time)
        {
            string[] arr = time.Split(':');
            return ((Convert.ToInt32(arr[0]) * 60) + Convert.ToInt32(arr[1]));
        }

        public string ConvertMinToHHMM(double totalMinutes)
        {
            string minute = "0";
            string hours = "0";

            if (totalMinutes < 60)
            {
                minute = Convert.ToString(totalMinutes);
            }
            else
            {

                minute = Convert.ToString(totalMinutes % 60);
            }

            hours = Convert.ToString(Convert.ToInt32((Int32)totalMinutes / 60));
            if (Convert.ToDouble(hours) < 1)
            {
                hours = "00";
            }
            else if (Convert.ToDouble(hours) < 10)
            {
                hours = "0" + hours;
            }

            if (Convert.ToDouble(minute) < 10)
            {
                minute = "0" + minute;
            }


            //return (hours + ":" + minute + ":00");
            return (hours + ":" + minute);
        }

        public void testa(int r)
        {
            try
            {
                string a = "";
                try
                {
                    int q = 10 / r;
                }
                catch (Exception ex)
                {

                }

            }
            catch (Exception ex)
            {

            }
        }

        public class TempPilotLog
        {
            public int AircraftId { get; set; }
            public double TotalHoursFlown { get; set; }
            public DateTime Date { get; set; }
            public string AircraftRegNo { get; set; }
            public string SerialNo { get; set; }
            public int? OwnerProfileId { get; set; }
            public int PilotLogId { get; set; }
            public double TotalGallons { get; set; }
            public double TotalMiles { get; set; }

        }



        public double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }



        /// <summary>
        /// Create Flying summary for user.
        /// </summary>
        /// <param name="objmodel"></param>
        /// <returns></returns>
        public PreferenceModelWeb GetFlyingSummary(PreferenceModelWeb objmodel)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                // making flying summary for pilot log

                // profile Id
                var profileId = Convert.ToInt32(objmodel.ProfileId);

                // all the pilot logs
                var allPilotLog = context.PilotLogs.Where(p => p.PilotId == profileId)
                    .ToList();

                // last pilot log
                var lastPilotLog = allPilotLog.FirstOrDefault(a => a.Id == allPilotLog.Max(b => b.Id));

                objmodel = SetDefaultValuesFlightSummary(objmodel);
                // if no last pilot log then no pilot log exists.
                if (lastPilotLog == default(PilotLog))
                    return objmodel;

                double Night = 0;
                double Day = 0;
                double CrossCountry = 0;
                double Actual = 0;
                double Sim = 0;
                double Hood = 0;

                Misc misc = new Misc();
                allPilotLog.Where(p => p.Id < allPilotLog.Max(b => b.Id)).ToList().ForEach(a =>
                {
                    Night += misc.ConvertHHMMToMinutes(a.NightPIC ?? "00:00:00");
                    Day += misc.ConvertHHMMToMinutes(a.DayPIC ?? "00:00:00");
                    CrossCountry += misc.ConvertHHMMToMinutes(a.CrossCountry ?? "00:00:00");
                    Actual += misc.ConvertHHMMToMinutes(a.Actual ?? "00:00:00");
                    Sim += misc.ConvertHHMMToMinutes(a.Sim ?? "00:00:00");
                    Hood += misc.ConvertHHMMToMinutes(a.Hood ?? "00:00:00");

                });

                objmodel.FlyingSummaryOld.Night = Misc.ConvertMinToOneTenthOFTime(Night);
                objmodel.FlyingSummaryOld.Day = Misc.ConvertMinToOneTenthOFTime(Day);
                objmodel.FlyingSummaryOld.CrossCountry = Misc.ConvertMinToOneTenthOFTime(CrossCountry);
                objmodel.FlyingSummaryOld.Actual = Misc.ConvertMinToOneTenthOFTime(Actual);
                objmodel.FlyingSummaryOld.Sim = Misc.ConvertMinToOneTenthOFTime(Sim);
                objmodel.FlyingSummaryOld.Hood = Misc.ConvertMinToOneTenthOFTime(Hood);

                objmodel.FlyingSummaryNew.Night = lastPilotLog.NightPIC != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.NightPIC)) : "00.0";
                objmodel.FlyingSummaryNew.Day = lastPilotLog.DayPIC != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.DayPIC)) : "00.0";
                objmodel.FlyingSummaryNew.CrossCountry = lastPilotLog.CrossCountry != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.CrossCountry)) : "00.0";
                objmodel.FlyingSummaryNew.Actual = lastPilotLog.Actual != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.Actual)) : "00.0";
                objmodel.FlyingSummaryNew.Sim = lastPilotLog.Sim != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.Sim)) : "00.0";
                objmodel.FlyingSummaryNew.Hood = lastPilotLog.Hood != null ? Misc.ConvertMinToOneTenthOFTime(misc.ConvertHHMMToMinutes(lastPilotLog.Hood)) : "00.0";
                objmodel.FlyingSummaryNew.Id = lastPilotLog.Id;

                objmodel.FlyingSummaryTotal.Night = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.Night, objmodel.FlyingSummaryNew.Night);
                objmodel.FlyingSummaryTotal.Day = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.Day, objmodel.FlyingSummaryNew.Day);
                objmodel.FlyingSummaryTotal.CrossCountry = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.CrossCountry, objmodel.FlyingSummaryNew.CrossCountry);
                objmodel.FlyingSummaryTotal.Actual = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.Actual, objmodel.FlyingSummaryNew.Actual);
                objmodel.FlyingSummaryTotal.Sim = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.Sim, objmodel.FlyingSummaryNew.Sim);
                objmodel.FlyingSummaryTotal.Hood = misc.addTimeInOneTenthFormat(objmodel.FlyingSummaryOld.Hood, objmodel.FlyingSummaryNew.Hood);

                //objmodel = AddToNewFlyingSummary(lastPilotLog, objmodel);

                return objmodel;
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
                return objmodel;
            }
            finally
            {
                context.Dispose();
            }

        }

        /// <summary>
        /// Sets Actual , CrossCountry , Day , Night , Sim , Hood with default value ( 00:00:00 ) for Old Flying Summary and new Flying Summary.
        /// </summary>
        /// <param name="objmodel"></param>
        /// <returns></returns>
        private PreferenceModelWeb SetDefaultValuesFlightSummary(PreferenceModelWeb objmodel)
        {
            objmodel.FlyingSummaryOld = new PilotFlyingSummary
            {
                Actual = Constants.DayPICDefaultValue,
                CrossCountry = Constants.DayPICDefaultValue,
                Day = Constants.DayPICDefaultValue,
                Hood = Constants.DayPICDefaultValue,
                Night = Constants.DayPICDefaultValue,
                Sim = Constants.DayPICDefaultValue
            };

            objmodel.FlyingSummaryNew = new PilotFlyingSummary
            {
                Actual = Constants.DayPICDefaultValue,
                CrossCountry = Constants.DayPICDefaultValue,
                Day = Constants.DayPICDefaultValue,
                Hood = Constants.DayPICDefaultValue,
                Night = Constants.DayPICDefaultValue,
                Sim = Constants.DayPICDefaultValue
            };

            objmodel.FlyingSummaryTotal = new PilotFlyingSummary
            {
                Actual = Constants.DayPICDefaultValue,
                CrossCountry = Constants.DayPICDefaultValue,
                Day = Constants.DayPICDefaultValue,
                Hood = Constants.DayPICDefaultValue,
                Night = Constants.DayPICDefaultValue,
                Sim = Constants.DayPICDefaultValue
            };

            return objmodel;
        }


        public bool UpdateLastPilotLogFlyingSummary(PilotFlyingSummary obj)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                try
                {
                    var pilotLog = context.PilotLogs.FirstOrDefault(p => p.PilotId == obj.ProfileId && p.Id == obj.Id);

                    if (pilotLog == default(PilotLog))
                        return false;

                    pilotLog.Actual = Misc.convertOneTenthTimeToHHMMSS(obj.Actual);
                    pilotLog.Hood = Misc.convertOneTenthTimeToHHMMSS(obj.Hood);
                    pilotLog.Sim = Misc.convertOneTenthTimeToHHMMSS(obj.Sim);
                    pilotLog.CrossCountry = Misc.convertOneTenthTimeToHHMMSS(obj.CrossCountry);
                    pilotLog.DayPIC = Misc.convertOneTenthTimeToHHMMSS(obj.Day);
                    pilotLog.NightPIC = Misc.convertOneTenthTimeToHHMMSS(obj.Night);

                    context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    //logger.Fatal("Exception ", e);
                }
                finally
                {
                    context.Dispose();
                }
            }
            catch (Exception eOut)
            {
                //logger.Fatal("Exception ", eOut);
            }

            return false;
        }

        /// <summary>
        /// checks the input password with password saved in database
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool CheckCurrentPassword(string oldPassword, int profileId)
        {
            var context = new GuardianAvionicsEntities();
            try
            {
                var password = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                if (password == default(Profile)) return false;
                return password.Password == oldPassword;
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
                return false;
            }
        }

        public Profile GetProfileDetailById(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            return context.Profiles.FirstOrDefault(f => f.Id == profileId);
        }

        public Profile GetProfileDetailByEmailId(string emailId)
        {
            var context = new GuardianAvionicsEntities();
            return context.Profiles.FirstOrDefault(f => f.EmailId == emailId);
        }

        /// <summary>
        /// Sets new password for user
        /// </summary>
        /// <param name="password"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public string SetNewPassword(string password, string oldPassword, int profileId)
        {
            var message = "";
            var context = new GuardianAvionicsEntities();
            try
            {
                var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                if (profile == default(Profile))
                {
                    message = "Invalid Profile Details";
                }
                else if (new Misc().Decrypt(profile.Password) != oldPassword)
                {
                    message = "Incorrect Old Password";
                }
                else
                {
                    profile.Password = new Misc().Encrypt(password);
                    context.SaveChanges();
                    message = "Password changed successfully";
                }
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception ", e);
                return "Error";
            }
            return message;
        }

        public List<UserEmail> GetUserEmailList(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var objUserEmail = context.UserEmails.Where(w => w.ProfileId == profileId).ToList();
            if (objUserEmail.Count == 0)
            {
                objUserEmail = new List<UserEmail>();
            }
            return objUserEmail;
        }


        public void ParseData(string filePath, int aircarftId, int profileId, int parseDataFileId)
        {

            var context = new GuardianAvionicsEntities();

            objDatalogList = new List<DataLogWithoutUnit>();

            foreach (var line in System.IO.File.ReadLines(filePath))
            {
                rowCount++;
                string[] arrColumns = line.Split(',');


                if (arrColumns.Count() == 1)
                {
                    isFinished = (arrColumns[0].ToString().ToLower() == "end");
                }
                else
                {
                    objDatalogList.Add(new DataLogWithoutUnit
                    {
                        Date = Convert.ToDateTime((rowCount == 1) ? (arrColumns[0].Substring(arrColumns[0].Length - 19, 19)) : arrColumns[0]),
                        Latitude = arrColumns[1],
                        Longitude = arrColumns[2],
                        Speed = arrColumns[3],
                        Altitude = arrColumns[4],
                        Yaw = arrColumns[5],
                        Pitch = arrColumns[6],
                        Roll = arrColumns[7],
                        SNO = rowCount
                    });
                }

            }

            if (rowCount == 0)
            {
                UpdateParseDataFileStatus(parseDataFileId, 1, "No record found in the file");
                return;
            }

            var pilotLogs = context.PilotLogs.OrderByDescending(o => o.FlightId).FirstOrDefault(p => p.AeroUnitMasterId == null && p.PilotId == profileId && p.AircraftId == aircarftId);
            if (pilotLogs == null)
            {
                dt_AirframeDataLog = new DataTable();

                dt_AirframeDataLog.Columns.Add("Id", typeof(int));
                dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
                dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
                dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));


                var pilotLogToBeInsert = context.PilotLogs.Create();
                pilotLogToBeInsert.AircraftId = aircarftId;
                pilotLogToBeInsert.PilotId = profileId;
                pilotLogToBeInsert.LastUpdated = DateTime.UtcNow;
                pilotLogToBeInsert.Date = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;
                pilotLogToBeInsert.Deleted = false;
                pilotLogToBeInsert.Actual = "00:00:00";
                pilotLogToBeInsert.CrossCountry = "00:00:00";

                var datetimediff = (int)(objDatalogList.OrderByDescending(o => o.SNO).FirstOrDefault().Date - objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date).TotalMinutes;

                pilotLogToBeInsert.DayPIC = new GA.CommonForParseDataFile.Common().ConvertMinToHHMM(datetimediff);
                pilotLogToBeInsert.Hood = "00:00:00";
                pilotLogToBeInsert.NightPIC = "00:00:00";
                pilotLogToBeInsert.Remark = "";
                pilotLogToBeInsert.Route = "";
                pilotLogToBeInsert.Sim = "00:00:00";
                pilotLogToBeInsert.Finished = isFinished;
                pilotLogToBeInsert.IFRAppchs = "";
                pilotLogToBeInsert.IsSavedOnDropbox = false;
                pilotLogToBeInsert.CoPilotId = null;
                pilotLogToBeInsert.FlightId = 1;
                pilotLogToBeInsert.AeroUnitMasterId = null;
                pilotLogToBeInsert.IsEmailSent = false;
                pilotLogToBeInsert.CommandRecFrom = null;
                pilotLogToBeInsert.DropBoxErrorMsg = null;
                context.PilotLogs.Add(pilotLogToBeInsert);
                context.SaveChanges();

                foreach (var datalog in objDatalogList)
                {
                    DataRow dr = dt_AirframeDataLog.NewRow();
                    dr[0] = 0;
                    dr[1] = pilotLogToBeInsert.Id;
                    dr[2] = datalog.Date;
                    dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                    dt_AirframeDataLog.Rows.Add(dr);
                }

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@flag";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = 1;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@aircraftId";
                param[1].SqlDbType = SqlDbType.Int;
                param[1].Value = aircarftId;


                param[2] = new SqlParameter();
                param[2].ParameterName = "tbAirframeDatalog";
                param[2].SqlDbType = SqlDbType.Structured;
                param[2].Value = dt_AirframeDataLog;

                if (dt_AirframeDataLog.Rows.Count > 0)
                {
                    int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                }

                UpdateParseDataFileStatus(parseDataFileId, 1, null);
            }
            else
            {
                //Check that the new data is continue with the last flight
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@aircraftId";
                param[0].SqlDbType = SqlDbType.Int;
                param[0].Value = aircarftId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@pilotId";
                param[1].SqlDbType = SqlDbType.Int;
                param[1].Value = profileId;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@startDateInDataFile";
                param[2].SqlDbType = SqlDbType.DateTime;
                param[2].Value = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;

                param[3] = new SqlParameter();
                param[3].ParameterName = "@EndDateInDataFile";
                param[3].SqlDbType = SqlDbType.DateTime;
                param[3].Value = objDatalogList.OrderByDescending(o => o.SNO).FirstOrDefault().Date;


                DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "spGetStatusToInsertDefaultLog", param);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string flagStatus = ds.Tables[0].Rows[0][0].ToString();
                        int pilotLogId = Convert.ToInt32(ds.Tables[0].Rows[0][1]);
                        dt_AirframeDataLog = new DataTable();

                        dt_AirframeDataLog.Columns.Add("Id", typeof(int));
                        dt_AirframeDataLog.Columns.Add("PilotLogId", typeof(int));
                        dt_AirframeDataLog.Columns.Add("GPRMCDate", typeof(DateTime));
                        dt_AirframeDataLog.Columns.Add("DataLog", typeof(string));

                        if (flagStatus == "1")
                        {
                            //Create New Flight
                            int? flightId = pilotLogs.FlightId;

                            var pilotLogToBeInsert = context.PilotLogs.Create();
                            pilotLogToBeInsert.AircraftId = aircarftId;
                            pilotLogToBeInsert.PilotId = profileId;
                            pilotLogToBeInsert.LastUpdated = DateTime.UtcNow;
                            pilotLogToBeInsert.Date = objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date;
                            pilotLogToBeInsert.Deleted = false;
                            pilotLogToBeInsert.Actual = "00:00:00";
                            pilotLogToBeInsert.CrossCountry = "00:00:00";

                            var datetimediff = (objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date - objDatalogList.OrderBy(o => o.SNO).FirstOrDefault().Date).TotalMinutes;

                            pilotLogToBeInsert.DayPIC = new GA.CommonForParseDataFile.Common().ConvertMinToHHMM(datetimediff);
                            pilotLogToBeInsert.Hood = "00:00:00";
                            pilotLogToBeInsert.NightPIC = "00:00:00";
                            pilotLogToBeInsert.Remark = "";
                            pilotLogToBeInsert.Route = "";
                            pilotLogToBeInsert.Sim = "00:00:00";
                            pilotLogToBeInsert.Finished = isFinished;
                            pilotLogToBeInsert.IFRAppchs = "";
                            pilotLogToBeInsert.IsSavedOnDropbox = false;
                            pilotLogToBeInsert.CoPilotId = null;
                            pilotLogToBeInsert.FlightId = flightId + 1;
                            pilotLogToBeInsert.AeroUnitMasterId = null;
                            pilotLogToBeInsert.IsEmailSent = false;
                            pilotLogToBeInsert.CommandRecFrom = null;
                            pilotLogToBeInsert.DropBoxErrorMsg = null;
                            context.PilotLogs.Add(pilotLogToBeInsert);
                            context.SaveChanges();

                            foreach (var datalog in objDatalogList)
                            {
                                DataRow dr = dt_AirframeDataLog.NewRow();
                                dr[0] = 0;
                                dr[1] = pilotLogToBeInsert.Id;
                                dr[2] = datalog.Date;
                                dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                                dt_AirframeDataLog.Rows.Add(dr);
                            }

                            param = new SqlParameter[3];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "@flag";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = 1;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "@aircraftId";
                            param[1].SqlDbType = SqlDbType.Int;
                            param[1].Value = aircarftId;


                            param[2] = new SqlParameter();
                            param[2].ParameterName = "tbAirframeDatalog";
                            param[2].SqlDbType = SqlDbType.Structured;
                            param[2].Value = dt_AirframeDataLog;

                            if (dt_AirframeDataLog.Rows.Count > 0)
                            {
                                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                            }

                        }
                        else if (flagStatus == "2")
                        {
                            //Append Data to the existing flight

                            var pilotLogToUpdate = context.PilotLogs.FirstOrDefault(f => f.Id == pilotLogId);
                            pilotLogToUpdate.LastUpdated = DateTime.UtcNow;
                            pilotLogToUpdate.DayPIC = new GA.CommonForParseDataFile.Common().ConvertMinToHHMM(Convert.ToDouble(ds.Tables[0].Rows[0]["TotalFlightTime"]));
                            pilotLogToUpdate.Finished = isFinished;
                            context.SaveChanges();

                            foreach (var datalog in objDatalogList)
                            {
                                DataRow dr = dt_AirframeDataLog.NewRow();
                                dr[0] = 0;
                                dr[1] = pilotLogId;
                                dr[2] = datalog.Date;
                                dr[3] = Newtonsoft.Json.JsonConvert.SerializeObject(datalog);
                                dt_AirframeDataLog.Rows.Add(dr);
                            }

                            param = new SqlParameter[3];
                            param[0] = new SqlParameter();
                            param[0].ParameterName = "@flag";
                            param[0].SqlDbType = SqlDbType.Int;
                            param[0].Value = 1;

                            param[1] = new SqlParameter();
                            param[1].ParameterName = "@aircraftId";
                            param[1].SqlDbType = SqlDbType.Int;
                            param[1].Value = aircarftId;


                            param[2] = new SqlParameter();
                            param[2].ParameterName = "tbAirframeDatalog";
                            param[2].SqlDbType = SqlDbType.Structured;
                            param[2].Value = dt_AirframeDataLog;

                            if (dt_AirframeDataLog.Rows.Count > 0)
                            {
                                int invID = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "spSetDataLogForAircraftWithoutUnit", param));
                            }
                        }

                        UpdateParseDataFileStatus(parseDataFileId, 1, null);
                    }
                    else
                    {
                        UpdateParseDataFileStatus(parseDataFileId, 1, "Not handle the case for previous flights");
                    }
                }
                else
                {
                    UpdateParseDataFileStatus(parseDataFileId, 1, "Not handle the case for previous flights");
                }

            }

            //Here we get all the data from the file
            //Now check that the data is for new flight or previous flight
            //There are four possible cases
            //1. Flight data is for new flight
            //2. continue data to the last flight


        }


        public void UpdateParseDataFileStatus(int id, int status, string remark)
        {
            var context = new GuardianAvionicsEntities();
            var obj = context.ParseDataFiles.FirstOrDefault(f => f.Id == id);

            obj.StatusId = status;
            obj.Remark = remark;
            context.SaveChanges();
        }


        public RegisterMaintenanceUserResponse RegisterMaintenanceUser(RegisterMaintenanceUserRequest objRequest)
        {
            var validateResponse = ValidateRegisterMaintenanceUser(objRequest);
            if (validateResponse.ResponseMessage != Enumerations.MaintenanceUser.Success.GetStringValue())
            {
                return new RegisterMaintenanceUserResponse
                {
                    ResponseCode = validateResponse.ResponseCode,
                    ResponseMessage = validateResponse.ResponseMessage
                };
            }

            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.EmailId == objRequest.EmailId);
            var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objRequest.AircraftId);
            int profileId = 0;
            try
            {

                if (profile == null)
                {
                    Profile addProfile = new Profile();

                    addProfile.EmailId = objRequest.EmailId;
                    addProfile.Password = new Misc().Encrypt("zzzzzzzzzz");
                    addProfile.DateCreated = DateTime.UtcNow;
                    addProfile.LastUpdated = DateTime.UtcNow;
                    addProfile.Deleted = false;
                    addProfile.IsBlocked = false;
                    addProfile.ThirdPartyId = null;
                    addProfile.UserTypeId = context.UserMasters.FirstOrDefault(f => f.UserType == "Maintenance").Id;
                    addProfile.RecordId = (new Encryption().Encrypt((Misc.RandomString(8) + objRequest.EmailId).ToLower(), Constants.GlobalEncryptionKey)).Substring(0, 16);
                    addProfile.LastUpdateDateForMedicalCertificate = DateTime.UtcNow;
                    addProfile.LastUpdateDateForLicenseRating = DateTime.UtcNow;

                    context.Profiles.Add(addProfile);
                    context.SaveChanges();
                    //SetSubscriptionToUserForFreeUse(profile.Id);
                    UserDetail userDetail = new UserDetail();

                    userDetail.FirstName = "";
                    userDetail.LastName = "";
                    userDetail.ProfileId = addProfile.Id;
                    userDetail.SecurityAnswer = string.Empty;
                    userDetail.SecurityQuestionId = 1;
                    userDetail.CompanyName = "";
                    userDetail.City = "";
                    userDetail.State = "";
                    userDetail.StreetAddress = "";
                    userDetail.ZipCode = null;
                    userDetail.PhoneNumber = "";
                    userDetail.DateOfBirth = null;

                    context.UserDetails.Add(userDetail);
                    context.SaveChanges();

                    var userPreference = CommonHelper.GetPreference();
                    userPreference.ProfileId = addProfile.Id;

                    context.Preferences.Add(userPreference);
                    context.SaveChanges();

                    MappingMaintenanceUserAndAircraft objMapping = new MappingMaintenanceUserAndAircraft();
                    objMapping.AircraftId = objRequest.AircraftId;
                    objMapping.ProfileId = addProfile.Id;
                    objMapping.IsDeleted = false;
                    objMapping.CreateDate = DateTime.UtcNow;
                    objMapping.LastUpdateDate = DateTime.UtcNow;
                    context.MappingMaintenanceUserAndAircrafts.Add(objMapping);
                    context.SaveChanges();
                    profileId = addProfile.Id;

                    ResetPassword objResetPassword = new ResetPassword();
                    objResetPassword.DateCreated = DateTime.UtcNow;
                    objResetPassword.ProfileId = addProfile.Id;
                    objResetPassword.Deleted = false;
                    objResetPassword.UniqueCode = Misc.RandomString(15);
                    objResetPassword.IsUniqueCodeUsed = false;

                    context.ResetPasswords.Add(objResetPassword);

                    aircraft.LastUpdateDateMaintenanceUser = DateTime.UtcNow;
                    context.SaveChanges();
                    string url = ConfigurationReader.ServerUrlToResetPassword + "/Home/RegMaintenanceUser?id=" + objResetPassword.UniqueCode;

                    string message = aircraft.Profile.UserDetail.FirstName + " " + aircraft.Profile.UserDetail.LastName + " has assigned you as maintenance user for their aircraft Tail " + aircraft.Registration;
                    string resetPasswordUrl = "<p>You can setup your profile using below URL to access the details. <br />" + url + "</p>";

                    SendEmailToMaintenanceUser("", message, objRequest.EmailId, resetPasswordUrl);
                }
                else
                {
                    profileId = profile.Id;
                    if (profile.UserMaster.UserType != "Maintenance")
                    {
                        return new RegisterMaintenanceUserResponse
                        {
                            ResponseCode = ((int)Enumerations.MaintenanceUser.DuplicateEmailId).ToString(),
                            ResponseMessage = Enumerations.MaintenanceUser.DuplicateEmailId.GetStringValue()
                        };
                    }

                    var mappUserAndAircraft = context.MappingMaintenanceUserAndAircrafts.FirstOrDefault(f => f.AircraftId == objRequest.AircraftId && f.ProfileId == profile.Id);
                    if (mappUserAndAircraft != null)
                    {
                        return new RegisterMaintenanceUserResponse
                        {
                            ResponseCode = ((int)Enumerations.MaintenanceUser.MaintenanceUserAlreadyMappedWithSameAircraft).ToString(),
                            ResponseMessage = Enumerations.MaintenanceUser.MaintenanceUserAlreadyMappedWithSameAircraft.GetStringValue()
                        };
                    }
                    else
                    {
                        aircraft.LastUpdateDateMaintenanceUser = DateTime.UtcNow;
                        var objMapping = context.MappingMaintenanceUserAndAircrafts.Create();
                        objMapping.AircraftId = objRequest.AircraftId;
                        objMapping.CreateDate = DateTime.UtcNow;
                        objMapping.IsDeleted = false;
                        objMapping.LastUpdateDate = DateTime.UtcNow;
                        objMapping.ProfileId = profile.Id;
                        context.MappingMaintenanceUserAndAircrafts.Add(objMapping);
                        context.SaveChanges();


                        string message = aircraft.Profile.UserDetail.FirstName + " " + aircraft.Profile.UserDetail.FirstName + " has assigned you as maintenance user for their aircraft Tail " + aircraft.Registration;

                        if (profile.UserDetail != null)
                        {
                            SendEmailToMaintenanceUser(profile.UserDetail.FirstName + " " + profile.UserDetail.LastName, message, objRequest.EmailId, "");
                        }
                        else
                        {
                            SendEmailToMaintenanceUser("", message, objRequest.EmailId, "");
                        }
                        var arrPushNotification = context.PushNotifications.Where(f => f.ProfileId == profile.Id).Select(p => p.TokenId).Distinct().ToArray();

                        IphonePushNotification objPush = new IphonePushNotification();

                        foreach (var id in arrPushNotification)
                        {
                            objPush.PushToiPhone(id, message, "");
                        }
                    }
                }

                return new RegisterMaintenanceUserResponse
                {
                    ProfileId = profileId,
                    ResponseCode = ((int)Enumerations.MaintenanceUser.Success).ToString(),
                    ResponseMessage = Enumerations.MaintenanceUser.Success.GetStringValue()
                };

            }
            catch (Exception ex)
            {
                return new RegisterMaintenanceUserResponse
                {
                    ResponseCode = ((int)Enumerations.MaintenanceUser.Exception).ToString(),
                    ResponseMessage = Enumerations.MaintenanceUser.Exception.GetStringValue()
                };


            }
            return new RegisterMaintenanceUserResponse();
        }


        public GeneralResponse ValidateRegisterMaintenanceUser(RegisterMaintenanceUserRequest objRequest)
        {

            var context = new GuardianAvionicsEntities();



            if (string.IsNullOrEmpty(objRequest.EmailId))
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.MaintenanceUser.EmailIdNotFound).ToString(),
                    ResponseMessage = Enumerations.RegistrationReturnCodes.EmailIdNotFound.GetStringValue()
                };
            }

            if (!Misc.validateEmailId(objRequest.EmailId))
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.MaintenanceUser.EmailIdInWrongFormat).ToString(),
                    ResponseMessage = Enumerations.MaintenanceUser.EmailIdInWrongFormat.GetStringValue()
                };
            }



            var aircraft = context.AircraftProfiles.FirstOrDefault(f => f.Id == objRequest.AircraftId);
            if (aircraft == null)
            {
                return new GeneralResponse
                {
                    ResponseCode = ((int)Enumerations.MaintenanceUser.AircraftIdNotFound).ToString(),
                    ResponseMessage = Enumerations.MaintenanceUser.AircraftIdNotFound.GetStringValue()
                };
            }

            return new GeneralResponse
            {
                ResponseCode = ((int)Enumerations.MaintenanceUser.Success).ToString(),
                ResponseMessage = Enumerations.MaintenanceUser.Success.GetStringValue()
            };


        }


        public void SendEmailToMaintenanceUser(string Name, string message, string emailTo, string resetPasswordUrl, string emailSubject = "")
        {
            if (string.IsNullOrEmpty(emailSubject))
            {
                emailSubject = "Assign aircraft for maintenance";
            }

            Name = Name == "" ? "User" : Name;
            try
            {
                var cid = Guid.NewGuid().ToString();
                var pathHtmlFile = ConfigurationManager.AppSettings["EmailTemplatePath"] + "email.html";
                string htmlBody = System.IO.File.ReadAllText(pathHtmlFile).Replace("\n", "").Replace("\r", "").Replace("\t", "");


                string h1 = string.Format(htmlBody,
                    Name,
                   message,
                   ConfigurationReader.s3BucketURL + ConfigurationReader.s3BucketImages + ConfigurationReader.s3BucketDefaultImages + "logo1.png", //cid,
                    resetPasswordUrl
                    );
                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(h1, null, MediaTypeNames.Text.Html);


                LinkedResource inline = new LinkedResource(ConfigurationManager.AppSettings["ImagePath"] + @"\DefaultImages\logo1.png", contentType: new ContentType("image/png"));
                inline.ContentId = cid;//Guid.NewGuid().ToString();
                inline.ContentType.Name = "Logo";

                avHtml.LinkedResources.Add(inline);
                MailMessage mail = new MailMessage();
                mail.AlternateViews.Add(avHtml);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailIdForSendingEmail"]);
                mail.To.Add(emailTo);
                mail.Subject = emailSubject;
                mail.Body = h1;
                mail.IsBodyHtml = true;
                SmtpClient smpt = new SmtpClient(ConfigurationManager.AppSettings["Host"], Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));
                smpt.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                smpt.UseDefaultCredentials = false;
                smpt.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailIdForSendingEmail"], ConfigurationManager.AppSettings["EmailPasswordForSendingEmail"]);
                smpt.DeliveryMethod = SmtpDeliveryMethod.Network;
                Task.Factory.StartNew(() => { smpt.Send(mail); });
            }
            catch (Exception e)
            {
                //logger.Fatal("Exception : " + Newtonsoft.Json.JsonConvert.SerializeObject(e));
            }
        }


        public GeneralResponse AddMaintainanceUserDetails(RegMaintenenceUserModel objModel)
        {
            try
            {
                var context = new GuardianAvionicsEntities();
                var profile = context.Profiles.FirstOrDefault(f => f.Id == objModel.ProfileId);
                profile.Password = new Misc().Encrypt(objModel.Password);
                profile.RecordId = (new Encryption().Encrypt((objModel.Password + profile.EmailId).ToLower(), Constants.GlobalEncryptionKey)).Substring(0, 16);


                profile.UserDetail.FirstName = objModel.FirstName;
                profile.UserDetail.LastName = objModel.LastName;
                profile.UserDetail.SecurityQuestionId = 1;
                profile.UserDetail.SecurityAnswer = "123";
                profile.UserDetail.ProfileId = objModel.ProfileId;


                var resetPassword = context.ResetPasswords.FirstOrDefault(f => f.UniqueCode == objModel.uniqueId);
                resetPassword.IsUniqueCodeUsed = true;




                var aircraftIdList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.ProfileId == objModel.ProfileId).Select(s => s.AircraftId).Distinct().ToList();

                var aircraftList = context.AircraftProfiles.Where(a => aircraftIdList.Contains(a.Id));
                aircraftList.ForEach(f => f.LastUpdateDateMaintenanceUser = DateTime.UtcNow);


                context.SaveChanges();

                var ownerProfileList = context.MappingMaintenanceUserAndAircrafts.Where(f => f.ProfileId == objModel.ProfileId).Select(s => s.AircraftProfile.OwnerProfileId).Distinct().ToList();

                var pushNotificationList = context.PushNotifications.Where(f => ownerProfileList.Contains(f.ProfileId)).Select(s => s.TokenId).Distinct().ToList();

                IphonePushNotification objPush = new IphonePushNotification();

                string pushMessage = objModel.FirstName + " " + objModel.LastName + " has completed their registration as a maintenance user.";

                foreach (var tokenId in pushNotificationList)
                {
                    objPush.PushToiPhone(tokenId, pushMessage, "");
                }

            }
            catch (Exception ex)
            {
                return new GeneralResponse
                {
                    ResponseCode = "9999",
                    ResponseMessage = "Exception"
                };
            }
            return new GeneralResponse
            {
                ResponseCode = "0",
                ResponseMessage = "Success"
            };
        }


        public RegMaintenenceUserModel GetUserDetailByResetPassUniqueId(string uniqueCode)
        {
            var context = new GuardianAvionicsEntities();
            var response = new RegMaintenenceUserModel();
            var resetPassword = context.ResetPasswords.FirstOrDefault(f => f.UniqueCode == uniqueCode);

            response.ProfileId = resetPassword.ProfileId;
            response.uniqueId = resetPassword.UniqueCode;
            response.EmailId = resetPassword.Profile.EmailId;

            return response;

        }


        public MaintenanceUserResponse GetMaintenanceUserList(int profileId, string lastUpdateDate)
        {

            MaintenanceUserResponse response = new MaintenanceUserResponse();

            try
            {
                response.LastUpdateDate = Misc.GetStringOfDate(DateTime.UtcNow);
                var udate = Misc.GetDate(lastUpdateDate) ?? DateTime.MinValue;
                List<MaintenanceUserModel> userList = new List<MaintenanceUserModel>();
                var context = new GuardianAvionicsEntities();
                var aircraftIds = new List<int>();
                if (string.IsNullOrEmpty(lastUpdateDate))
                {
                    aircraftIds = context.AircraftProfiles.Where(f => f.OwnerProfileId == profileId).Select(s => s.Id).ToList();
                }
                else
                {
                    aircraftIds = context.AircraftProfiles.Where(f => f.OwnerProfileId == profileId && f.LastUpdateDateMaintenanceUser >= udate).Select(s => s.Id).ToList();
                }

                if (aircraftIds.Count == 0)
                {
                    response.MaintenanceUserList = new List<MaintenanceUserModel>();
                    response.ResponseCode = "0";
                    response.ResponseMessage = "Success";
                    return response;
                }

                var aaa = context.MappingMaintenanceUserAndAircrafts.Where(f => aircraftIds.Contains(f.AircraftId) && !f.IsDeleted).GroupBy(p => p.Profile,
                                   p => p.AircraftId,
                                   (key, g) => new
                                   {
                                       Id = key.Id,
                                       EmailId = key.EmailId,
                                       FirstName = (key.UserDetail == null) ? "" : key.UserDetail.FirstName,
                                       LastName = (key.UserDetail == null) ? "" : key.UserDetail.LastName,
                                       IsRegister = (key.UserDetail == null) ? false : (string.IsNullOrEmpty(key.UserDetail.FirstName) ? false : true),
                                       LinkedAircrafts = g
                                   });
                response.MaintenanceUserList = new List<MaintenanceUserModel>();
                aaa.ForEach(f => response.MaintenanceUserList.Add(new MaintenanceUserModel
                {
                    Id = f.Id,
                    EmailId = f.EmailId,
                    FirstName = f.FirstName,
                    LastName = f.LastName,
                    IsRegister = f.IsRegister,
                    LinkedAircrafts = f.LinkedAircrafts.ToArray()
                }));
                response.ResponseCode = "0";
                response.ResponseMessage = "Success";
            }
            catch (Exception ex)
            {
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception";
            }
            return response;
        }


        public UsAirportResponseModel GetUsAirportList(string lastUpdatedDate)
        {
            try
            {
                UsAirportResponseModel objResponse = new UsAirportResponseModel();
                var context = new GuardianAvionicsEntities();
                var udate = Misc.GetDate(lastUpdatedDate) ?? DateTime.MinValue;

                var plateVersions = context.PlateVersions.FirstOrDefault(f => f.LastUpdatedDate > udate);
                if (plateVersions != null)
                {
                    //objResponse.FileURL = ConfigurationReader.s3BucketURL + ConfigurationReader.BucketPlateJsonFolder + "/" + "USAirportList.txt";
                    var objNavStatus = context.AirportNavAndFreqStatus.FirstOrDefault();
                    if (objNavStatus == null)
                    {
                        objResponse.EndDate = null;
                        objResponse.StartDate = null;
                        objResponse.ModifyDate = null;
                        objResponse.FileSizeInMB = null;
                        objResponse.FileURL = null;
                    }
                    else
                    {
                        objResponse.EndDate = Misc.GetStringOnlyDateFormat2(objNavStatus.EndDate);
                        objResponse.StartDate = Misc.GetStringOnlyDateFormat2(objNavStatus.StartDate);
                        objResponse.ModifyDate = Misc.GetStringOnlyDateFormat2(objNavStatus.ModifyDate);
                        objResponse.FileSizeInMB = objNavStatus.FileSizeInMB;
                        objResponse.FileURL = ConfigurationReader.s3BucketURL + "JsonMisc/AirportNavaidAndFrequency.txt";
                    }
                }
                else
                {
                    objResponse.EndDate = null;
                    objResponse.StartDate = null;
                    objResponse.ModifyDate = null;
                    objResponse.FileSizeInMB = null;
                    objResponse.FileURL = null;
                }

                objResponse.ResponseCode = ((int)Enumerations.UsAirportFile.Success).ToString();
                objResponse.ResponseMessage = Enumerations.UsAirportFile.Success.GetStringValue();
                objResponse.LastUpdateDateTime = Misc.GetStringOfDate(DateTime.UtcNow);
                return objResponse;
            }
            catch (Exception ex)
            {
                return new UsAirportResponseModel
                {
                    ResponseCode = ((int)Enumerations.UsAirportFile.Exception).ToString(),
                    ResponseMessage = Enumerations.UsAirportFile.Exception.GetStringValue(),
                };
            }
        }

        public GA.DataTransfer.Classes_for_Web.SubscriptionDetailsModel GetSubscriptionByUser(int profileId)
        {
            DataTransfer.Classes_for_Web.SubscriptionDetailsModel response = new DataTransfer.Classes_for_Web.SubscriptionDetailsModel();
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            SubscriptionHelper subHelper = new SubscriptionHelper();
            if (profile != null)
            {
                if (profile.CurrentSubscriptionId != null)
                {
                    var subDetail = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                    if (subDetail != null)
                    {
                        if (subHelper.IsSubscriptionActive(profile.CurrentSubscriptionId ?? 0))
                        {
                            response.IsSubscriptionPurchased = true;
                            response.SubscriptionStatus = subDetail.PaypalStatusMaster.PaypalStatus;
                            response.SubscriptionName = subDetail.SubscriptionPaymentDefinition.SubscriptionMaster.SubItemName;
                            response.SubProdileId = subDetail.PaypalSubscriptionProfileId;
                            response.UserSubMapID = subDetail.Id;
                            response.PurchasedFrom = subDetail.SubscriptionSourceMaster.PurchaseBy;
                        }
                    }
                }
            }
            return response;
        }

        public void MarkUserSubscriptionAsCancel(int userSubMapId)
        {
            var context = new GuardianAvionicsEntities();
            var userSubMapping = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == userSubMapId);
            if (userSubMapping != null)
            {
                userSubMapping.PaypalStatusId = (int)Enumerations.PaypalStatus.Cancelled;
                context.SaveChanges();
            }
        }


        public SubscriptionResponseModel GetSubscription()
        {
            var context = new GuardianAvionicsEntities();
            var response = new SubscriptionResponseModel();
            try
            {
                response.FeatureList = context.FeatureMasters.OrderBy(o => o.Sno).Select(s => new FeatureMasterModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    GroupType = s.GroupType,
                    IsEnable = s.IsEnable,
                    KeyCode = s.KeyCode,
                    SNO = s.Sno
                }).ToList();

                response.SubscriptionList = context.SubscriptionMasters.Select(s => new SubscriptionModel
                {
                    Id = s.SubItemId,
                    PlanName = s.SubItemName,
                    Detail = s.SubDetails,
                    Status = s.Status,
                    ProductList = s.SubscriptionPaymentDefinitions.Where(w => w.RegularFrequency == "Monthly" || w.RegularFrequency == "Yearly").Select(sd => new DataTransfer.Classes_for_Services.SubscriptionDetailsModel
                    {
                        Id = sd.Id,
                        SubscriptionId = sd.SubscriptionItemId,
                        Amount = Math.Round(sd.Amount, 2),
                        TrialFrequency = sd.TrialFrequency,
                        RegularFrequency = sd.RegularFrequency,
                        InAppProductId = sd.InAppProductId,
                    }).ToList(),
                    FeatureIdList = s.SubscriptionAndFeatureMappings.Select(f => f.FeatureId).ToList()
                }).ToList();
                response.ResponseCode = Enumerations.Subscription.Success.GetStringValue();
                response.ResponseMessage = Enumerations.Subscription.Success.ToString();

            }
            catch (Exception ex)
            {
                response.ResponseCode = Enumerations.Subscription.Exception.GetStringValue();
                response.ResponseMessage = Enumerations.Subscription.Exception.ToString();
            }
            return response;
        }


        public SubTableModel GetSubscriptionWeb(int profileId = 0)
        {
            var context = new GuardianAvionicsEntities();
            var response = new SubTableModel();
            try
            {
                response.FeatureList = context.FeatureMasters.Select(s => new FeatureMasterModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    GroupType = s.GroupType,
                    IsEnable = false,
                    KeyCode = s.KeyCode,
                    SNO = s.Sno
                }).ToList();

                var tempList = response.FeatureList;

                response.SubscriptionList = context.SubscriptionMasters.Select(s => new SubDetailTableModel
                {
                    Id = s.SubItemId,
                    SubItemName = s.SubItemName,
                    SubDetails = s.SubDetails,
                    Status = s.Status,
                    SubscriptionDetailList = s.SubscriptionPaymentDefinitions.Select(sd => new DataTransfer.Classes_for_Services.SubscriptionDetailsModel
                    {
                        Id = sd.Id,
                        SubscriptionId = sd.SubscriptionItemId,
                        Amount = sd.Amount,
                        TrialFrequency = sd.TrialFrequency,
                        RegularFrequency = sd.RegularFrequency,
                        InAppProductId = sd.InAppProductId,
                        PaypalBId = sd.PaypalButtonId
                    }).ToList(),
                    FeatureIdList = context.FeatureMasters.Select(f => new FeatureMasterModel
                    {
                        Id = f.Id,
                        Name = f.Name,
                        GroupType = f.GroupType,
                        IsEnable = false,
                        KeyCode = f.KeyCode
                    }).ToList(),
                    SubFeatureList = s.SubscriptionAndFeatureMappings.Select(map => new FeatureMasterModel
                    {
                        Id = map.FeatureId,
                        Name = "",
                        GroupType = "",
                        IsEnable = false,
                        KeyCode = ""
                    }).ToList()
                }).ToList();

                foreach (var sub in response.SubscriptionList)
                {
                    sub.FeatureIdList.Join(sub.SubFeatureList, (allSub) => allSub.Id, (mapSubAndFeature) => mapSubAndFeature.Id, (allSub, mapSubAndFeature) =>
                    {
                        allSub.IsEnable = (allSub.Id == mapSubAndFeature.Id);
                        return allSub;
                    }).ToList();
                }
                response.IsCurrSubPurchaseByInappAndActive = false;
                response.CurrentPaymentDefinationId = "0";
                if (profileId != 0)
                {
                    var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                    if (profile.CurrentSubscriptionId != null)
                    {
                        SubscriptionHelper subHelper = new SubscriptionHelper();
                        var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                        if (subHelper.IsSubscriptionActive(mapSubAndUser.Id))
                        {
                            response.CurrentPaymentDefinationId = mapSubAndUser.SubscriptionPaymentDefinition.Id.ToString();
                            if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
                            {
                                response.IsCurrSubPurchaseByInappAndActive = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        //public bool IsSubscriptionActive(int mapSubAndUserId)
        //{
        //    var isSubActive = false;
        //    var context = new GuardianAvionicsEntities();
        //    var lastReceipt = new SubscriptionTransaction();
        //    var lastInappReceipt = new InappReceipt();
        //    var expDate = DateTime.UtcNow;
        //    var mapSubAndUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == mapSubAndUserId);
        //    if (mapSubAndUser.PaypalStatusId == (int)Enumerations.PaypalStatus.Cancelled)
        //    {
        //        return false;
        //    }
        //    if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
        //    {

        //        lastReceipt = mapSubAndUser.SubscriptionTransactions.OrderByDescending(f => f.PaymentDate).FirstOrDefault();
        //        if (lastReceipt != null)
        //        {
        //            switch (mapSubAndUser.SubscriptionPaymentDefinition.RegularFrequency)
        //            {
        //                case "Monthly":
        //                    {
        //                        expDate = lastReceipt.PaymentDate.Value.AddMonths(1);
        //                        break;
        //                    }
        //                case "Yearly":
        //                    {
        //                        expDate = lastReceipt.PaymentDate.Value.AddYears(1);
        //                        break;
        //                    }
        //            }
        //            if ((expDate - DateTime.UtcNow).TotalMinutes >= 0)
        //            {
        //                isSubActive = true;
        //            }
        //            else
        //            {
        //                isSubActive = false;
        //            }
        //        }
        //        else
        //        {
        //            isSubActive = false;
        //        }
        //    }
        //    else if (mapSubAndUser.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
        //    {
        //        lastInappReceipt = mapSubAndUser.InappReceipts.OrderByDescending(o => o.ExpireDate).FirstOrDefault();
        //        if (lastInappReceipt != null)
        //        {
        //            isSubActive = ((lastInappReceipt.ExpireDate - DateTime.UtcNow).TotalMinutes >= 0);
        //        }
        //        else
        //        {
        //            isSubActive = false;
        //        }
        //    }
        //    else
        //    {
        //        isSubActive = ((new UserHelper().GetExpiryDateForFreePlan(mapSubAndUser.InappPurchaseDate) - DateTime.UtcNow).TotalMinutes >= 0);
        //    }

        //    return isSubActive;
        //}

        public List<FeatureMasterModel> SetFeatureByUser(int profileId)
        {
            List<FeatureMasterModel> response = new List<FeatureMasterModel>();
            var context = new GuardianAvionicsEntities();

            response = context.FeatureMasters.Select(f => new FeatureMasterModel
            {
                Id = f.Id,
                Name = f.Name,
                // IsEnable = true,
            }).ToList();

            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
            if (profile.CurrentSubscriptionId == null)
            {


                DateTime expdate = new SubscriptionHelper().GetExpiryDateForFreePlan(profile.DateCreated);
                if (DateTime.UtcNow < expdate)
                {
                    response.ForEach(f => f.IsEnable = true);
                }
                else
                {
                    response.ForEach(f => f.IsEnable = false);
                }
            }
            else
            {
                SubscriptionHelper subHelper = new SubscriptionHelper();
                int currSub = profile.CurrentSubscriptionId ?? 0;
                var mapSubAndUSer = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == currSub);
                if (subHelper.IsSubscriptionActive(mapSubAndUSer.Id))
                {
                    var SubFeatureList = mapSubAndUSer.SubscriptionPaymentDefinition.SubscriptionMaster.SubscriptionAndFeatureMappings.Select(map => new FeatureMasterModel
                    {
                        Id = map.FeatureId,
                        IsEnable = false
                    }).ToList();

                    response.Join(SubFeatureList, (allSub) => allSub.Id, (mapSubAndFeature) => mapSubAndFeature.Id, (allSub, mapSubAndFeature) =>
                    {
                        allSub.IsEnable = (allSub.Id == mapSubAndFeature.Id);
                        return allSub;
                    }).ToList();
                }
                else
                {
                    response.ForEach(f => f.IsEnable = false);
                }
            }
            return response;
        }

        public ValidateReceiptResponseMOdel ValidateReceipt(ValidateReceiptRequestModel validateReceiptRequestModel, int profileId)
        {
            ValidateReceiptResponseMOdel objResponse = new ValidateReceiptResponseMOdel();
            objResponse.Subscription = new ValidateReceiptSubscriptionModel();
            InappReceiptResponseModel inappReceiptResponseModel = new InappReceiptResponseModel();
            //validateReceiptRequestModel.ReceiptData = "";
            //validateReceiptRequestModel.Password = "7baea1bb64f74c12bfbbf0cf5863a0ac";
            //profileId = 345;
            StringBuilder strLog = new StringBuilder();
            var context = new GuardianAvionicsEntities();
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(validateReceiptRequestModel);
            data = data.Replace("receiptdata", "receipt-data");
            ExceptionHandler.ReportError(new Exception(), data);
            string URL = ConfigurationReader.InAppURL;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(data);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = responseReader.ReadToEnd();
                            ExceptionHandler.ReportError(new Exception(), "InApp Receipt response");
                            ExceptionHandler.ReportError(new Exception(), response);
                            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                            inappReceiptResponseModel = json_serializer.Deserialize<InappReceiptResponseModel>(response);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.ExceptionFromItunes).ToString();
                objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.ExceptionFromItunes.GetStringValue();
                objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                return objResponse;
            }
            strLog.Append("AAAAAAA" + Environment.NewLine);
            // return new ValidateReceiptResponseMOdel();
            try
            {
                if (inappReceiptResponseModel.Status == 0)
                {
                    bool isCurrSubIsPaypanAndActive = false;
                    bool isInappRequestForActiveSubscription = false;
                    bool isEligibleForRefund = false;
                    string frequency = "";
                    string subProfileId = "";
                    string transactionId = "";
                    DateTime purchaseDate = DateTime.UtcNow;
                    decimal subAmount = 0;
                    decimal subDays = 0;
                    decimal subRemainingDays = 0;
                    decimal refundAmount = 0;
                    PaypalPaymentHelper objHelper = new PaypalPaymentHelper();
                    var userCurrentSubscription = new MapSubscriptionAndUSer();
                    SubscriptionHelper subHelper = new SubscriptionHelper();
                    //Check the user has any current subscription from paypal or not.
                    var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);
                    strLog.Append("BBBBBBBBBB" + Environment.NewLine);
                    if (inappReceiptResponseModel.Latest_Receipt_Info != null)
                    {
                        if (inappReceiptResponseModel.Latest_Receipt_Info.Count != 0)
                        {
                            var subDefinationList = context.SubscriptionPaymentDefinitions.ToList();

                            var originalTransactionIdList = inappReceiptResponseModel.Latest_Receipt_Info.Select(s => s.Original_Transaction_Id).ToList();
                            var subMappWithOtherUser = context.MapSubscriptionAndUSers.FirstOrDefault(f => originalTransactionIdList.Contains(f.InappOriginalTransactionId) && f.ProfileId != profileId);
                            if (subMappWithOtherUser != null)
                            {
                                objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.OriginalTransactionIdAlreadyMapWithOtherUser).ToString();
                                objResponse.Subscription.StatusMessage = "Subscription with Original transaction id = " + subMappWithOtherUser.InappOriginalTransactionId + " already mapped with other user.";
                                return objResponse;
                            }

                        }
                    }
                    strLog.Append("CCCCCCCCCC" + Environment.NewLine);
                    if (profile != null)
                    {

                        if (profile.CurrentSubscriptionId != null)
                        {
                            if (inappReceiptResponseModel.Latest_Receipt_Info != null)
                            {
                                if (inappReceiptResponseModel.Latest_Receipt_Info.Count > 0)
                                {
                                    var inappLastReceipt = inappReceiptResponseModel.Latest_Receipt_Info[inappReceiptResponseModel.Latest_Receipt_Info.Count - 1];
                                    isInappRequestForActiveSubscription = ((Convert.ToDateTime(inappLastReceipt.Expires_Date.Replace(" Etc/GMT", "")) - DateTime.UtcNow).TotalMinutes >= 0);
                                    if (isInappRequestForActiveSubscription)
                                    {
                                        strLog.Append("isInappRequestForActiveSubscription is true" + Environment.NewLine);
                                    }
                                    else
                                    {
                                        strLog.Append("isInappRequestForActiveSubscription is false" + Environment.NewLine);
                                    }
                                }
                            }
                            userCurrentSubscription = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                            if (subHelper.IsSubscriptionActive(userCurrentSubscription.Id))
                            {
                                strLog.Append("IsSubscriptionActive is true" + Environment.NewLine);
                            }
                            else
                            {
                                strLog.Append("IsSubscriptionActive is false" + Environment.NewLine);
                            }
                            if (userCurrentSubscription.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal && subHelper.IsSubscriptionActive(userCurrentSubscription.Id))
                            {
                                strLog.Append("purchase by paypal and true" + Environment.NewLine);
                                subProfileId = userCurrentSubscription.PaypalSubscriptionProfileId;

                                //user's current subscription is active and purchased by paypal
                                //Now we need to cancel the subscription and refund to the customer
                                isCurrSubIsPaypanAndActive = true;
                                subAmount = userCurrentSubscription.SubscriptionPaymentDefinition.Amount;
                                frequency = userCurrentSubscription.SubscriptionPaymentDefinition.RegularFrequency;

                                purchaseDate = userCurrentSubscription.InappPurchaseDate; //this column is common for both paypal purchase date and inapppurchasedate
                                var lastReceipt = context.SubscriptionTransactions.OrderBy(o => o.PaymentDate).FirstOrDefault(f => f.MapSubAndUserId == userCurrentSubscription.Id);
                                if (lastReceipt != null)
                                {
                                    transactionId = lastReceipt.TransactionId;
                                    var daysCountAfterRenewal = (decimal)(DateTime.UtcNow - (lastReceipt.PaymentDate ?? DateTime.UtcNow)).TotalDays;
                                    subDays = (frequency == "Weekly") ? 7 : ((frequency == "Monthly") ? 30 : 365);
                                    if (daysCountAfterRenewal < subDays)
                                    {
                                        //eligible for refund
                                        isEligibleForRefund = true;
                                        decimal totalDays = (decimal)(DateTime.UtcNow - purchaseDate).TotalDays;

                                        subRemainingDays = (frequency == "Weekly") ? (7 - totalDays) : ((frequency == "Monthly") ? (30 - totalDays) : (365 - totalDays));
                                        subDays = (frequency == "Weekly") ? 7 : ((frequency == "Monthly") ? 30 : 365);
                                        refundAmount = (subAmount / subDays) * subRemainingDays;
                                    }
                                }
                                strLog.Append("DDDDDDD" + Environment.NewLine);
                                if (isCurrSubIsPaypanAndActive == true && isInappRequestForActiveSubscription == true)
                                {
                                    strLog.Append("isCurrSubIsPaypanAndActive == true && isInappRequestForActiveSubscription == true" + Environment.NewLine);
                                    //Here user has a current subscription purchase by paypal and is active
                                    //Now we first refund the amount to the customer and then cancel the subscription
                                    //Get the amount and timeinterval for subscription.
                                    PaypalAgreement paypalAgreement = new PaypalAgreement();
                                    if (isEligibleForRefund)
                                    {
                                        strLog.Append("isEligibleForRefund = true  " + Environment.NewLine);
                                        strLog.Append("subProfileId = " + subProfileId + " transactionId = " + transactionId + " refundAmount = " + Convert.ToString(refundAmount) + Environment.NewLine);
                                        paypalAgreement.RefundPAypal(subProfileId, transactionId, Convert.ToString(refundAmount));
                                    }
                                    //Now cancel Subscription
                                    paypalAgreement.CancelAgreement(subProfileId, "Cancellation while request for inapp purchase");
                                    strLog.Append("CancelAgreement called " + Environment.NewLine);
                                    userCurrentSubscription.PaypalStatusId = (int)Enumerations.PaypalStatus.Cancelled;
                                    context.SaveChanges();
                                    strLog.Append("EEEEEEEEEEEEE " + Environment.NewLine);
                                }
                            }

                        }
                    }

                    strLog.Append("Receipt reading start " + Environment.NewLine);
                    if (inappReceiptResponseModel.Latest_Receipt_Info != null)
                    {
                        if (inappReceiptResponseModel.Latest_Receipt_Info.Count != 0)
                        {
                            var subDefinationList = context.SubscriptionPaymentDefinitions.ToList();
                            string originalTransactionId = "";
                            string inAppProductId = "";
                            MapSubscriptionAndUSer mapSubscriptionAndUSer;
                            InappReceipt newReceipt;
                            InappReceipt receiptDB;
                            var receiptList = new List<InappReceipt>();
                            foreach (var receipt in inappReceiptResponseModel.Latest_Receipt_Info)
                            {
                                originalTransactionId = receipt.Original_Transaction_Id;
                                inAppProductId = receipt.Product_Id;
                                var mapSubAndUSer = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.ProfileId == profileId && f.InappOriginalTransactionId == originalTransactionId && f.InAppProductId == receipt.Product_Id);
                                if (mapSubAndUSer == null)
                                {
                                    var userSubList = context.MapSubscriptionAndUSers.Where(f => f.ProfileId == profileId && f.InappOriginalTransactionId == originalTransactionId).ToList();
                                    userSubList.ForEach(f => f.PaypalStatusId = (int)Enumerations.PaypalStatus.Cancelled);
                                    context.SaveChanges();
                                    //var userSubIdList = userSubList.Select(s => s.Id).ToList();
                                    //var pendRenInfoList = context.PendingRenewalInfoes.Where(f => userSubIdList.Contains(f.MapSubAndUserId ?? 0));
                                    //pendRenInfoList.ForEach(f => { f.ExpirationIntent = 1; f.AutoRenewStatus = false; });
                                    //context.SaveChanges();
                                    //Map user and sub
                                    mapSubscriptionAndUSer = new MapSubscriptionAndUSer
                                    {
                                        ProfileId = profileId,
                                        SubId = subDefinationList.FirstOrDefault(f => f.InAppProductId == receipt.Product_Id).Id,
                                        SubscriptionSourceId = (int)Enumerations.SubscriptionSourceEnum.InappPurchase,
                                        InappPurchaseDate = Convert.ToDateTime(receipt.Original_Purchase_Date.Replace(" Etc/GMT", "")),
                                        InappOriginalTransactionId = receipt.Original_Transaction_Id,
                                        InAppProductId = receipt.Product_Id,
                                        PaypalStatusId = (int)Enumerations.PaypalStatus.Active
                                    };
                                    context.MapSubscriptionAndUSers.Add(mapSubscriptionAndUSer);
                                    context.SaveChanges();
                                    strLog.Append("FFFFFFFF " + Environment.NewLine);
                                    if (isInappRequestForActiveSubscription)
                                    {
                                        context.Profiles.FirstOrDefault(f => f.Id == profileId).CurrentSubscriptionId = mapSubscriptionAndUSer.Id;
                                        context.SaveChanges();
                                    }
                                    strLog.Append("GGGGGGGGG " + Environment.NewLine);
                                    newReceipt = new InappReceipt
                                    {
                                        Quantity = receipt.Quantity,
                                        TransactionID = receipt.Transaction_Id,
                                        PurchaseDate = Convert.ToDateTime(receipt.Purchase_Date.Replace(" Etc/GMT", "")),
                                        ExpireDate = Convert.ToDateTime(receipt.Expires_Date.Replace(" Etc/GMT", "")),
                                        WebOrderLineItemId = receipt.Web_Order_Line_Item_Id,
                                        IsTrialPeriod = receipt.Is_Trial_Period,
                                        MapSubAndUserId = mapSubscriptionAndUSer.Id
                                    };
                                    receiptList.Add(newReceipt);
                                    Task.Factory.StartNew(() => { objHelper.SendEmail(newReceipt, mapSubscriptionAndUSer.Id, profileId, Enumerations.PaypalStatus.Active.ToString()); });
                                }
                                else
                                {
                                    receiptDB = context.InappReceipts.FirstOrDefault(f => f.MapSubAndUserId == mapSubAndUSer.Id && f.TransactionID == receipt.Transaction_Id);
                                    if (receiptDB == null)
                                    {
                                        newReceipt = new InappReceipt
                                        {
                                            Quantity = receipt.Quantity,
                                            TransactionID = receipt.Transaction_Id,
                                            PurchaseDate = Convert.ToDateTime(receipt.Purchase_Date.Replace(" Etc/GMT", "")),
                                            ExpireDate = Convert.ToDateTime(receipt.Expires_Date.Replace(" Etc/GMT", "")),
                                            WebOrderLineItemId = receipt.Web_Order_Line_Item_Id,
                                            IsTrialPeriod = receipt.Is_Trial_Period,
                                            MapSubAndUserId = mapSubAndUSer.Id
                                        };
                                        receiptList.Add(newReceipt);
                                        Task.Factory.StartNew(() => { objHelper.SendEmail(newReceipt, mapSubAndUSer.Id, profileId, Enumerations.PaypalStatus.Active.ToString()); });
                                        strLog.Append("HHHHHHH " + Environment.NewLine);
                                    }
                                }

                            }
                            if (receiptList.Count > 0)
                            {
                                context.InappReceipts.AddRange(receiptList);

                                context.SaveChanges();
                                strLog.Append("IIIIIIIIIIIIIIIII " + Environment.NewLine);
                            }
                            var inappLasrReceipt = inappReceiptResponseModel.Latest_Receipt_Info[inappReceiptResponseModel.Latest_Receipt_Info.Count - 1];
                            var mapSubAndUSerForLastReceipt = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.ProfileId == profileId && f.InappOriginalTransactionId == inappLasrReceipt.Original_Transaction_Id && f.InAppProductId == inappLasrReceipt.Product_Id);
                            if (isInappRequestForActiveSubscription)
                            {
                                var userSubList = context.MapSubscriptionAndUSers.Where(f => f.ProfileId == profileId && f.InappOriginalTransactionId == originalTransactionId && f.InAppProductId != inappLasrReceipt.Product_Id && f.PaypalStatusId != (int)Enumerations.PaypalStatus.Active).ToList();
                                userSubList.ForEach(f => f.PaypalStatusId = (int)Enumerations.PaypalStatus.Cancelled);
                                context.SaveChanges();
                                profile.CurrentSubscriptionId = mapSubAndUSerForLastReceipt.Id;
                                mapSubAndUSerForLastReceipt.PaypalStatusId = (int)Enumerations.PaypalStatus.Active;
                                context.SaveChanges();
                            }
                            strLog.Append("JJJJJJJJJJJJJJ " + Environment.NewLine);
                            var pendingList = new List<PendingRenewalInfo>();
                            PendingRenewalInfo newPendingInfo;
                            foreach (var pendingRenewal in inappReceiptResponseModel.Pending_Renewal_Info)
                            {
                                var mapSubAndUSer = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.ProfileId == profileId && f.InappOriginalTransactionId == originalTransactionId && f.InAppProductId == pendingRenewal.Product_Id);
                                if (mapSubAndUSer != null)
                                {
                                    newPendingInfo = context.PendingRenewalInfoes.FirstOrDefault(f => f.MapSubAndUserId == mapSubAndUSer.Id);

                                    if (newPendingInfo == null)
                                    {
                                        newPendingInfo = new PendingRenewalInfo
                                        {
                                            ExpirationIntent = pendingRenewal.Expiration_Intent,
                                            IsInBillingRetryPeriod = pendingRenewal.Is_In_Billing_Retry_Period == 1 ? true : false,
                                            AutoRenewStatus = pendingRenewal.Auto_Renew_Status == 1 ? true : false,
                                            AutoRenewProductId = pendingRenewal.Auto_Renew_Product_Id,
                                            ProductId = pendingRenewal.Product_Id,
                                            MapSubAndUserId = mapSubAndUSer.Id
                                        };
                                        context.PendingRenewalInfoes.Add(newPendingInfo);
                                        context.SaveChanges();
                                        //if (pendingRenewal.Auto_Renew_Status == 0)
                                        //{
                                        //    Task.Factory.StartNew(() => { objHelper.SendEmail(new InappReceipt(), mapSubAndUSer.Id, profileId, Enumerations.PaypalStatus.Cancelled.ToString()); });
                                        //}
                                        //newPendingInfo.MapSubscriptionAndUSer.Profile.CurrentSubscriptionId = mapSubAndUSer.Id;
                                        //context.SaveChanges();
                                    }
                                    else
                                    {
                                        newPendingInfo.ExpirationIntent = pendingRenewal.Expiration_Intent;
                                        newPendingInfo.IsInBillingRetryPeriod = pendingRenewal.Is_In_Billing_Retry_Period == 1 ? true : false;
                                        newPendingInfo.AutoRenewStatus = pendingRenewal.Auto_Renew_Status == 1 ? true : false;
                                        newPendingInfo.AutoRenewProductId = pendingRenewal.Auto_Renew_Product_Id;
                                        newPendingInfo.ProductId = pendingRenewal.Product_Id;
                                        context.SaveChanges();
                                    }
                                }
                            }
                            strLog.Append("KKKKKKKKKKKKKKKKKK " + Environment.NewLine);
                            //var pendingRenInfo = inappReceiptResponseModel.Pending_Renewal_Info.FirstOrDefault();
                            profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);


                            if (profile.CurrentSubscriptionId != null)
                            {
                                var lastSub = context.MapSubscriptionAndUSers.FirstOrDefault(f => f.Id == profile.CurrentSubscriptionId);
                                var pendingRenewInfo = context.PendingRenewalInfoes.FirstOrDefault(f => f.MapSubAndUserId == lastSub.Id);

                                var isUserCurrSubActive = new SubscriptionHelper().IsSubscriptionActive(lastSub.Id);
                                //objProfile.Subscription.AutoRenewStatus = isUserCurrSubActive;
                                var payDefination = lastSub.SubscriptionPaymentDefinition;
                                objResponse.Subscription = new ValidateReceiptSubscriptionModel();
                                objResponse.Subscription.SubscriptionId = payDefination.Id;
                                objResponse.Subscription.ProductId = payDefination.InAppProductId;
                                objResponse.Subscription.PlanName = payDefination.SubscriptionMaster.SubItemName;
                                objResponse.Subscription.Detail = payDefination.SubscriptionMaster.SubDetails;
                                objResponse.Subscription.PurchaseDate = Misc.GetStringOfDate(lastSub.InappPurchaseDate);
                                objResponse.Subscription.IsTrialPeriod = lastSub.IsInTrialMode ?? true;
                                //if (inAppAutoRenewStatus == true || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Active || (lastSub.IsInTrialMode == true && lastSub.Status == true))
                                if (isUserCurrSubActive)
                                {
                                    objResponse.Subscription.FeatureList = new List<FeatureMaster>();
                                    var featureList = context.SubscriptionAndFeatureMappings.Where(w => w.SubscriptionItemId == lastSub.SubscriptionPaymentDefinition.SubscriptionItemId).Select(s => s.FeatureId).ToList();
                                    objResponse.Subscription.FeatureList = context.FeatureMasters.Where(w => featureList.Contains(w.Id)).ToList();
                                    objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                    objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                    objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                    objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                }
                                if (lastSub.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.InappPurchase)
                                {
                                    objResponse.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.InappPurchase;
                                    objResponse.Subscription.ExpirationDate = Misc.GetStringOfDate(lastSub.InappReceipts.OrderByDescending(o => o.ExpireDate).FirstOrDefault().ExpireDate);

                                    if (isUserCurrSubActive == false)
                                    {
                                        objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                        objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                        objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                        objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                        objResponse.Subscription.InAppStatusCode = Convert.ToString(lastSub.PendingRenewalInfoes.FirstOrDefault().ExpirationIntent);
                                    }
                                }
                                else if (lastSub.SubscriptionSourceId == (int)Enumerations.SubscriptionSourceEnum.Paypal)
                                {
                                    var paypalReceipt = lastSub.SubscriptionTransactions.OrderByDescending(o => o.PaymentDate).FirstOrDefault();
                                    if (paypalReceipt != null)
                                    {
                                        switch (lastSub.SubscriptionPaymentDefinition.RegularFrequency)
                                        {
                                            case "Weekly":
                                                {
                                                    objResponse.Subscription.ExpirationDate = Misc.GetStringOfDate(paypalReceipt.PaymentDate.Value.AddDays(7));
                                                    break;
                                                }
                                            case "Monthly":
                                                {
                                                    objResponse.Subscription.ExpirationDate = Misc.GetStringOfDate(paypalReceipt.PaymentDate.Value.AddMonths(1));
                                                    break;
                                                }
                                            case "Yearly":
                                                {
                                                    objResponse.Subscription.ExpirationDate = Misc.GetStringOfDate(paypalReceipt.PaymentDate.Value.AddYears(1));
                                                    break;
                                                }
                                        }
                                    }
                                    objResponse.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.Paypal;
                                    // if (lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Cancelled || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Suspended || lastSub.PaypalStatusId == (int)Enumerations.PaypalStatus.Completed)
                                    if (isUserCurrSubActive == false)
                                    {
                                        objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                        objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                        objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                        objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                        objResponse.Subscription.InAppStatusCode = "1";

                                    }
                                }
                                else
                                {
                                    objResponse.Subscription.Source = (int)Enumerations.SubscriptionSourceEnum.Free;
                                    objResponse.Subscription.ExpirationDate = Misc.GetStringOfDate(new SubscriptionHelper().GetExpiryDateForFreePlan(lastSub.InappPurchaseDate));
                                    if (isUserCurrSubActive == false)
                                    {
                                        objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse).ToString();
                                        objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.SubscriptionAutoRenewalIsFalse.GetStringValue();
                                        objResponse.Subscription.InAppStatusCode = "1";
                                    }
                                    else
                                    {
                                        objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                        objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                    }
                                    objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                    objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                }
                            }
                            else
                            {
                                objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                                objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                                objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.NoSubscriptionAvailableForThisUSer).ToString();
                                objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.NoSubscriptionAvailableForThisUSer.GetStringValue();
                            }
                            ExceptionHandler.ReportError(new Exception(), strLog.ToString() + Environment.NewLine);
                            return objResponse;
                        }
                        else
                        {
                            //No Receipt available
                        }
                    }
                    else
                    {
                        //No Receipt available
                    }
                }
                else
                {
                    //No Receipt details
                    objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.InappStatusInvalid).ToString();
                    objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.InappStatusInvalid.GetStringValue();
                    objResponse.Subscription.InAppStatusCode = Convert.ToString(inappReceiptResponseModel.Status);
                    objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
                    objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
                    return objResponse;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(new Exception(), "Exceiption  = " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                ExceptionHandler.ReportError(new Exception(), "Strlogg in exception =" + strLog.ToString() + Environment.NewLine);
                objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Exception).ToString();
                objResponse.ResponseMessage = Enumerations.ValidateReceipt.Exception.GetStringValue();
                return objResponse;
            }
            objResponse.Subscription.StatusCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
            objResponse.Subscription.StatusMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
            objResponse.ResponseCode = ((int)Enumerations.ValidateReceipt.Success).ToString();
            objResponse.ResponseMessage = Enumerations.ValidateReceipt.Success.GetStringValue();
            ExceptionHandler.ReportError(new Exception(), "Strlogg =" + strLog.ToString() + Environment.NewLine);
            return objResponse;
        }

        /// <summary>
        /// When new user is registered then assign the subscription (paln 5) to this user for  6 months
        /// </summary>
        /// <param name="profileId"></param>
        public void SetSubscriptionToUserForFreeUse(int profileId)
        {
            var context = new GuardianAvionicsEntities();
            var profile = context.Profiles.FirstOrDefault(f => f.Id == profileId);

            var mapSubAndUSer = context.MapSubscriptionAndUSers.Create();
            mapSubAndUSer.ProfileId = profileId;
            mapSubAndUSer.SubId = context.SubscriptionMasters.FirstOrDefault(f => f.SubItemName == "Plan 5").SubscriptionPaymentDefinitions.FirstOrDefault(f => f.RegularFrequency == "Monthly").Id;
            mapSubAndUSer.InappPurchaseDate = DateTime.UtcNow;
            mapSubAndUSer.InappOriginalTransactionId = "00";
            //mapSubAndUSer.Status = true;
            mapSubAndUSer.IsInTrialMode = true;
            mapSubAndUSer.SubscriptionSourceId = (int)Enumerations.SubscriptionSourceEnum.Free;
            context.MapSubscriptionAndUSers.Add(mapSubAndUSer);
            context.SaveChanges();
            profile.CurrentSubscriptionId = mapSubAndUSer.Id;
            context.SaveChanges();
        }





    }
}