﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Script.Serialization;
using GA.ApplicationLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using NLog;
using GA.Common;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace GuardianAvionics
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GuardianService : IGuardianService
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public ResponseModel RegisterUser(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UserModel userModel = new UserModel();
            try
            {
                userModel = new ConversionHelper().DecryptRegisterUserAPI(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "RegisterUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error occurs while decrypt the request"
                });
                #endregion APIErrorLog
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
            }

            var objUserHelper = new UserHelper();
            userModel.IsRegisterByAdmin = false;
            userModel.ManufacturerId = 0;
            userModel.UserType = "User";
            var response = objUserHelper.RegisterUser(userModel, false);

            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(userModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "RegisterUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error occurs while register user"
                });
            }
            #endregion APIErrorLog

            if (response.ResponseCode != ((int)Enumerations.RegistrationReturnCodes.Success).ToString())
            {
                responseModel.ResponseCode = response.ResponseCode;
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.Response = "";
                return responseModel;
            }

            var objLoginHelper = new LoginHelper();
            LoginModel loginModel = new LoginModel
            {
                EmailId = userModel.EmailId,
                Password = userModel.Password
            };
            var loginResponse = objLoginHelper.UserLoginAPI(loginModel);

            #region APIErrorLog
            if (loginResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(userModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "RegisterUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error occurs while register user, user register successfully but error in UserLoginAPI"
                });
            }
            #endregion APIErrorLog

            if (loginResponse.ResponseMessage == Enumerations.LoginReturnCodes.Success.GetStringValue())
            {
                responseModel.ResponseMessage = loginResponse.ResponseMessage;
                responseModel.ResponseCode = loginResponse.ResponseCode;
                string email = loginModel.EmailId.ToLower();
                responseModel.Response = new ConversionHelper().EncryptNewLoginAPIResponse(loginResponse, loginModel.Password + email);
            }
            else
            {
                responseModel.ResponseMessage = loginResponse.ResponseMessage;
                responseModel.ResponseCode = loginResponse.ResponseCode;
                responseModel.Response = "";
            }
            return responseModel;
        }


        public ResponseModel GetIPassengerDetails(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            IPassengerRequestModel objRequest;
            try
            {
                objRequest = new ConversionHelper().DecryptIPassengerAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetIPassengerDetails",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error occurs while decrypt the request"
                });
                #endregion APIErrorLog
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
            }
            var response = new IPassengerHelper().GetIPassengerMediaDetailsAPI(objRequest.AircraftId);

            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objRequest),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetIPassengerDetails",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog

            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptIPassengerAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        public ResponseModel LoginUser(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            LoginModel loginModel = new LoginModel();
            try
            {
                loginModel = new ConversionHelper().DecryptLoginAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "LoginUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
              
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog
            }

            var objLoginHelper = new LoginHelper();
            var response = objLoginHelper.UserLoginAPI(loginModel);

            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(loginModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "LoginUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            if (response.ResponseMessage == "Success")
            {
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.ResponseCode = response.ResponseCode;
                string email = loginModel.EmailId.ToLower();
                responseModel.Response = new ConversionHelper().EncryptNewLoginAPIResponse(response, loginModel.Password + email);
            }
            else
            {
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.ResponseCode = response.ResponseCode;
                responseModel.Response = "";
            }
            return responseModel;
        }
        public ResponseModel SyncUserData(RequestModel requestModel)
        {
            SyncUserDataResponseModel syncResponse = new SyncUserDataResponseModel();

            ResponseModel response = new ResponseModel();
            SyncUserDataModel syncUserDataModel = new ConversionHelper().DecryptSyncUserData(requestModel.request, requestModel.requestId);
            // Log.Info("Request SyncUserData " + Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel));
            var objUpdateHelper = new UserProfileUpdateHelper();
            syncResponse.UserProfileResponse = objUpdateHelper.UpdateUserProfile(syncUserDataModel.SyncUserProfile);

            var objAircraftHelper = new AircraftHelper();
            syncResponse.AircraftProfileResponse = objAircraftHelper.UpdateAircraftProfile(syncUserDataModel.SyncAircraftProfile);


            var objHelper = new UserHelper();
            syncResponse.LogBookResponse = objHelper.SaveLogBook(syncUserDataModel.syncLogBook);

            GetPrefereceModel getPrefereceModel = new GetPrefereceModel();
            getPrefereceModel.ProfileId = requestModel.requestId;
            syncResponse.PreferenceResponse = objHelper.GetPreference(getPrefereceModel);

            syncResponse.NotificationResponse = new AircraftHelper().GetNotification(requestModel.requestId, syncUserDataModel.SyncNotification.LastUpdateDate);

            syncResponse.MaintenanceUserResponse = objHelper.GetMaintenanceUserList(requestModel.requestId, syncUserDataModel.SyncMaintenanceUser.LastUpdateDate);

            syncResponse.SquawkResponse = objAircraftHelper.GetSquawkList(requestModel.requestId, syncUserDataModel.SyncSquawkList.LastUpdateDate);

            syncResponse.DropDownListResponse = objAircraftHelper.GetFieldsForDropDownlist(syncUserDataModel.SyncDropdownListData);

            // Log.Info("Response SyncUserData " + Newtonsoft.Json.JsonConvert.SerializeObject(syncResponse));
            response.Response = new ConversionHelper().EncryptSyncUserData(syncResponse, requestModel.requestId);
            // Log.Info("Encrypted SyncUserData response  " + response.Response);
            return response;
        }
        public ResponseModel ThirdPartyLogin(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            RegisterAndLoginByThirdParty objUserModel = new ConversionHelper().DecryptThirdPartyLoginAPIRequest(requestModel.request);

            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request FaceBookLogin " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(objUserModel));
            }
            var objLoginHelper = new UserHelper();
            var response = objLoginHelper.verifyThirdPartyLogin(objUserModel, false);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            if (response.ResponseMessage != Enumerations.LoginReturnCodes.Success.ToString())
            {
                responseModel.Response = "";
            }
            else
            {
                responseModel.Response = new ConversionHelper().EncryptThirdPartyLoginAPIResponse(response);
            }
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response FaceBookLogin " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }
        public ResponseModel UserProfileUpdate(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ProfileUpdateRequestModel profileDetail = new ConversionHelper().DecryptUserProfileUpdateAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request UserProfileUpdate " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
            }
            var objUpdateHelper = new UserProfileUpdateHelper();
            var response = objUpdateHelper.UpdateUserProfile(profileDetail);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUserProfileUpdateAPIResponse(response, requestModel.requestId);

            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response UserProfileUpdate " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }
        public ResponseModel UpdateAircraftProfile(RequestModel requestModel)
        {
            Log.Info("Request UpdateAircraftProfile");
            ResponseModel responseModel = new ResponseModel();
            AircraftProfileUpdateModel profileDetail = new ConversionHelper().DecryptUpdateAircraftProfileAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request UpdateAircraftProfile " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail));
            }
            var objAircraftHelper = new AircraftHelper();
            var response = objAircraftHelper.UpdateAircraftProfile(profileDetail);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdateAircraftProfileAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response UpdateAircraftProfile " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }
        public ResponseModel UpdatePreference(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UpdatePreferenceModel profileUpdate = new ConversionHelper().DecryptUpdatePreferenceAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request UpdatePreference " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(profileUpdate));
            }
            var objUserHelper = new UserHelper();
            var response = objUserHelper.UpdatePreference(profileUpdate);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response UpdatePreference " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdatePreferenceAPIResponse(response, requestModel.requestId);

            return responseModel;
        }
        public ResponseModel GetPreference(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();
            GetPrefereceModel getPrefereceModel = new ConversionHelper().DecryptGetPreferenceAPIRequest(requestModel.request, requestModel.requestId);

            getPrefereceModel.ProfileId = requestModel.requestId;
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetPreference " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(getPrefereceModel));
            }
            var objUserHelper = new UserHelper();
            var response = objUserHelper.GetPreference(getPrefereceModel);

            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetPreferenceAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetPreference " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));

            }
            return responseModel;
        }
        public GeneralResponse ResetPassword(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();
            ResetPasswordModel userDetails = new ConversionHelper().DecryptResetPasswordAPIRequest(requestModel.request);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request ResetPassword " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(userDetails));
            }
            var objUserHelper = new UserHelper();
            var response = objUserHelper.ResetPassword(userDetails);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response ResetPassword " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        //This API is not in use
        //public FileUploadResponseModel UploadDataString(FileUploadRequestModel dataAsString)
        //{
        //    var identifier = System.DateTime.UtcNow.Ticks;
        //    if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
        //    {
        //        Log.Info("Request UploadDataString " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(dataAsString));
        //    }
        //    var objUnitData = new UnitDataHelper();
        //    var response = objUnitData.UploadStringData(dataAsString);
        //    if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
        //    {
        //        Log.Info("Response UploadDataString " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
        //    }
        //    return response;
        //}
        public ResponseModel LogBook(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();
            LogBookRequestModel logbook = new ConversionHelper().DecryptLogBookAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request LogBook " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(logbook));
            }
            var objHelper = new UserHelper();
            var logbookRes = objHelper.SaveLogBook(logbook);
            responseModel.ResponseCode = logbookRes.ResponseCode;
            responseModel.ResponseMessage = logbookRes.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptLogBookAPIResponse(logbookRes, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response LogBook " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(logbookRes));
            }
            return responseModel;
        }
        public GeneralResponse UpdateLogWithPilotAndCoPilot(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UpdateLogBookForPilotAndCopilot model = new ConversionHelper().DecryptUpdateLogWithPilotAndCoPilotAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request UpdateLogWithPilotAndCoPilot " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var objHelper = new UserHelper();
            var logbookRes = objHelper.UpdateLogWithPilotAndCoPilot(model);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response UpdateLogWithPilotAndCoPilot " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(logbookRes));
            }
            return logbookRes;
        }

        public ResponseModel AircraftSearch(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            AircraftSearchRequest aircraft = new ConversionHelper().DecryptAircraftSearchAPIRequest(requestModel.request, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request AircraftSearch " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(aircraft));
            }
            var response = new AircraftHelper().SearchAircraft(aircraft);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptAircraftSearchAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response AircraftSearch " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }

        public ResponseModel CreateUpdateAircraftNNumber(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            AircraftModifyNnumberRequest aircraft = new ConversionHelper().DecryptCreateUpdateAircraftNNumberAPIRequest(requestModel.request, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request CreateUpdateAircraftNNumber " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(aircraft));
            }
            var response = new AircraftHelper().ModifyAircraftNNumber(aircraft);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptCreateUpdateAircraftNNumberAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response CreateUpdateAircraftNNumber " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }

        /// <summary>
        /// Sends as response all the related documents for the user
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public ResponseModel GetDocument(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            DocumnetRequestModel document = new DocumnetRequestModel();
            document.ProfileId = requestModel.requestId;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetDocument " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(document));
            }
            var response = new UnitDataHelper().GetDocumentForApp(document);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetDocumentAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetDocument " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }

        ///// <summary>
        ///// Verify subscription of user from apple server
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //public SubscriptionResponseModel SubscriptionVerification(SubscriptionRequestModel model)
        //{
        //    var helper = new UserHelper();
        //    return helper.SubscriptionVerification(model);
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResponseModel GetFieldsForDropDownlistAndConstants(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            ManufacturereModelListRequest model = new ConversionHelper().DecryptGetFieldsForDropDownlistAndConstantsAPIRequest(requestModel.request);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetFieldsForDropDownlistAndConstants " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new AircraftHelper().GetFieldsForDropDownlist(model);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetFieldsForDropDownlistAndConstantsAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetFieldsForDropDownlistAndConstants " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }


        /// <summary>
        /// Currently this API is not in use.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GetUserNameByEmailResponseModel GetUserNameByEmailId(GetUserNameByEmailRequestModel model)
        {
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetUserNameByEmailId " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new UserHelper().GetUserNameByEmailId(model);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetUserNameByEmailId " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        public ResponseModel GetMapFilesUrl(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            MapFilesRequest model = new ConversionHelper().DecryptGetMapFilesUrlAPIREquest(requestModel.request);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetMapFilesUrl " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new UnitDataHelper().GetMapFilesUrlFromDb(model);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetMapFilesUrlAPIResponse(response);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetMapFilesUrl " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }

        public ResponseModel GetPassengerMapFileUrl()
        {
            var identifier = System.DateTime.UtcNow.Ticks;
            ResponseModel responseModel = new ResponseModel();
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetPassengerMapFileUrl ");
            }
            var response = new UnitDataHelper().GetPassengerMapFileUrl();
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetPassengerMapFileUrlAPIResponse(response);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetPassengerMapFileUrl " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }


        //public GeneralResponse IsFmsExists(RequestModel requestModel)
        //{
        //    ResponseModel responseModel = new ResponseModel();
        //    var identifier = System.DateTime.UtcNow.Ticks;
        //    AeroUnitNumberRequestModel model = new ConversionHelper().DecryptIsFmsExistsAPIRequest(requestModel.request);
        //    if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
        //    {
        //        Log.Info("Request IsFmsExists " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
        //    }
        //    var response = new AircraftHelper().IsFmsExists(model);
        //    if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
        //    {
        //        Log.Info("Response IsFmsExists " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
        //    }
        //    return response;
        //}

        public GeneralResponse DeleteAeroUnit(RequestModel requestModel)
        {

            AeroUnitNumberRequestModel model = new ConversionHelper().DecryptDeleteAeroUnitAPIRequest(requestModel.request);

            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request DeleteAeroUnit " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new AircraftHelper().DeleteAeroUnit(model);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response DeleteAeroUnit " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        public GeneralResponse RegisterFMS(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            AeroUnitNumberRequestModel model = new ConversionHelper().DecryptIsFMSRegisterAPIRequest(requestModel.request);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request IsFMSRegister " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new AircraftHelper().RegisterFMS(model);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response IsFMSRegister " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        public ResponseModel ISFMSRegisterWithAircraft(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();

            AeroUnitRequestModel model = new ConversionHelper().DecryptISFMSRegisterWithAircraftAPIRequest(requestModel.request, requestModel.requestId);
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request ISFMSRegisterWithAircraft " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new AircraftHelper().ISFMSRegisterWithAircraft(model);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptISFMSRegisterWithAircraftAPIResponse(response, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response ISFMSRegisterWithAircraft " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }


        public ResponseModel SendPushMessageOnFlightStart(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            SendPushMessageOnFlightStartRequestModel obj = new ConversionHelper().DecryptSendPushMessageOnFlightStartAPIRequest(requestModel.request, requestModel.requestId);
            Log.Info("Request SendPushMessageOnFlightStart " + Newtonsoft.Json.JsonConvert.SerializeObject(obj));
            AircraftHelper objHelper = new AircraftHelper();
            return objHelper.SendPushMessageAfterFlightStart(requestModel.requestId, obj.AircraftId, obj.DeviceTokenId);
        }


        public GeneralResponse ChangePassword(RequestModel requestModel)
        {

            var identifier = System.DateTime.UtcNow.Ticks;
            ChangePasswordRequetModel model = new ConversionHelper().DecryptChangePasswordAPIRequest(requestModel.request, requestModel.requestId);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request ChangePassword " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(model));
            }
            var response = new LoginHelper().ChangePassword(model);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response ChangePassword " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        public SecurityQuestionRequestModel SecurityQuestion()
        {
            var identifier = System.DateTime.UtcNow.Ticks;
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request SecurityQuestion " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject("SecurityQuestion"));
            }
            var response = new LoginHelper().GetSecurityQuestion();
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response SecurityQuestion " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return response;
        }

        public ResponseModel GetSecurityQuestionByEmailId(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            var identifier = System.DateTime.UtcNow.Ticks;
            ResetPasswordModel objEmail = new ConversionHelper().DecryptGetSecurityQuestionByEmailIdAPIRequest(requestModel.request);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Request GetSecurityQuestionByEmailId " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(objEmail));
            }
            var response = new UserHelper().GetSecurityQuestionByEmailId(objEmail);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetSecurityQuestionByEmailIdAPIResponse(response);
            if (Convert.ToBoolean(ConfigurationReader.IsLoggerEnabled))
            {
                Log.Info("Response GetSecurityQuestionByEmailId " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            return responseModel;
        }

        public GeneralResponse GetDataFile(Stream file)
        {
            var identifier = System.DateTime.UtcNow.Ticks;
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            Log.Info("GetDataFile Request " + identifier + " : Start Decrypt File Data  " + Newtonsoft.Json.JsonConvert.SerializeObject(woc));

            int? applicationheader = null;
            int profileId = 0;
            long uniqueId = 0;

            if (woc.Headers["AircraftId"] != null)
            {
                applicationheader = Convert.ToInt32(woc.Headers["AircraftId"]);
            }
            else
            {
                return new GeneralResponse()
                {
                    ResponseCode = ((int)Enumerations.GetDataFile.AircraftIdNotFound).ToString(),
                    ResponseMessage = Enumerations.GetDataFile.AircraftIdNotFound.GetStringValue()
                };
            }

            if (woc.Headers["ProfileId"] != null)
            {
                profileId = Convert.ToInt32(woc.Headers["ProfileId"]);
            }
            else
            {
                return new GeneralResponse()
                {
                    ResponseCode = ((int)Enumerations.GetDataFile.ProfileIdNotFound).ToString(),
                    ResponseMessage = Enumerations.GetDataFile.ProfileIdNotFound.GetStringValue()
                };
            }

            if (woc.Headers["UniqueId"] != null)
            {
                uniqueId = Convert.ToInt64(woc.Headers["UniqueId"]);
            }
            else
            {
                return new GeneralResponse()
                {
                    ResponseCode = ((int)Enumerations.GetDataFile.UniqueIdNotFound).ToString(),
                    ResponseMessage = Enumerations.GetDataFile.UniqueIdNotFound.GetStringValue()
                };
            }
            var response = new UnitDataHelper().GetDataFile(file, applicationheader, profileId, uniqueId);
            Log.Info("GetDataFile Response " + identifier + " " + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            return response;
        }

        //public ResponseModel GetUpdatedPlates(RequestModel requestModel)
        //{
        //    ResponseModel responseModel = new ResponseModel();
        //    ChartDetailsRequestModel obj = new ConversionHelper().DecryptGetChartDetails(requestModel.request, requestModel.requestId);
        //    var response = new AdminHelper().GetUpdatedPlates(obj.LastUpdateDateTime ?? DateTime.UtcNow);
        //    responseModel.Response = new ConversionHelper().EncryptUpdatePlateResponseModel(response, requestModel.requestId);
        //    responseModel.ResponseCode = response.ResponseCode;
        //    responseModel.ResponseMessage = response.ResponseMessage;
        //    return responseModel;
        //}

        public ResponseModel GetChartDetails(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ChartDetailsRequestModel obj = new ConversionHelper().DecryptGetChartDetails(requestModel.request, requestModel.requestId);
            var response = new AdminHelper().GetChartDetails(obj.LastUpdateDateTime, obj.LastChangedDataTimeForPlate, obj.StateIds);

            responseModel.Response = new ConversionHelper().EncryptGetChartDetails(response, requestModel.requestId);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            return responseModel;
        }

        public GeneralResponse ResetLastTransactionId(int aircraftId)
        {
            var objHelper = new AircraftHelper();
            var response = objHelper.SetLastTransactionId(aircraftId);
            return response;
        }

        public string Test(string type)
        {
            //  GA.Common.PushNotificationIPhone abc = new PushNotificationIPhone();
            //string[] arr = { "2c9e264ee1e40c8962ca4ea2ad5839c0da04b1a8a84264a010a771bea247ab6d" };
            //abc.Push(arr, "hello");
            GA.Common.IphonePushNotification abc = new IphonePushNotification();
            // abc.PushToiPhone("", 2195, "", "4f04bf375b6c7339e4d7e89299169ec7ecc1d70630db9861d5604b52fdc506e0");
            return "";
        }

        public GeneralResponse TestStoreData(string type)
        {
            var identifier = System.DateTime.UtcNow.Ticks;
            Log.Info("Request Test " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(type));
            var response = new UnitDataHelper().TestStoreData();
            Log.Info("Response Test " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            return response;
        }

        public ResponseModel GetHardwareFirmwareUploadFiles()
        {
            ResponseModel responseModel = new ResponseModel();
            Log.Info("Request GethardwareFirmwareUploadFiles ");
            var response = new AdminHelper().GetHardwareFirmwareFileListAPI();
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetHardwareFirmwareFileUrlAPIResponse(response);
            return responseModel;
        }

        public void StartDataFilePArsingWithThread()
        {
            SetQueus obj = new SetQueus();
            obj.SetQueusForParsingDataFiles();
        }


        public GeneralResponse GetAudioFile(Stream file)
        {
            Log.Info("Start Decrypt Audio File Data");
            var identifier = System.DateTime.UtcNow.Ticks;
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            string fileName = woc.Headers["FileName"];
            var response = new UnitDataHelper().GetAudioFile(file, fileName);
            return response;
        }

        public ResponseModel GetPlatesData(RequestModel requestModel)
        {

            PlatesResponseModel objResponse = new PlatesResponseModel();
            ResponseModel responseModel = new ResponseModel();
            try
            {
                PlatesRequestModel objPlate = new ConversionHelper().DecryptGetPlatesDataAPIRequest(requestModel.request);
                objResponse.StateList = new AdminHelper().GetPlatesData(objPlate.arrStateId);
                responseModel.ResponseCode = "0";
                responseModel.ResponseMessage = "Succcess";
                responseModel.Response = new ConversionHelper().EncryptGetPlatesDataAPIResponse(objResponse);
            }
            catch (Exception ex)
            {
                responseModel.Response = null;
                responseModel.ResponseCode = "9999";
                responseModel.ResponseMessage = "Exception";
            }
            return responseModel;
        }

        public GeneralResponse SendRequestToOwnerForAircraft(RequestModel objModel)
        {
            RequestToOwnerForAircraftModel obj = new ConversionHelper().DecryptRequestToOwnerForAircraftAPIRequest(objModel.request, objModel.requestId);
            GeneralResponse response = new AircraftHelper().SendRequestToOwnerForAircraft(obj);
            return response;
        }


        public ResponseModel GetNotifications(RequestModel objModel)
        {
            ResponseModel response = new ResponseModel();

            GetNotificationRequest obj = new ConversionHelper().DecryptRequestToGetNotificationAPIRequest(objModel.request, objModel.requestId);

            NotificationResponse notificationResponse = new AircraftHelper().GetNotification(objModel.requestId, obj.LastUpdateDate);
            try
            {
                response.Response = new ConversionHelper().EncryptNotificationAPI(notificationResponse, objModel.requestId);
                response.ResponseCode = "0";
                response.ResponseMessage = "Success";
            }
            catch (Exception ex)
            {
                response.Response = "";
                response.ResponseCode = "9999";
                response.ResponseMessage = "Exception";
            }

            return response;
        }

        public ResponseModel GetKMLFile(RequestModel requestModel)
        {
            Log.Info("GetKMLFile Request");

            ResponseModel responseModel = new ResponseModel();
            KMLFileRequestModel objRequest = new ConversionHelper().DecryptRequestForKMLFile(requestModel.request, requestModel.requestId);

            var response = new UnitDataHelper().GetKMLFileURL(objRequest.FlightId);

            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            Log.Info("Response GetKMLFile " + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            responseModel.Response = new ConversionHelper().EncryptKMLFileAPI(response, requestModel.requestId);
            return responseModel;
        }


        public ResponseModel UpdateNotificationStatus(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();

            UpdateNotificationStatusRequest obj = new ConversionHelper().DecryptRequestToUpdateNotificationAPIRequest(requestModel.request, requestModel.requestId);

            //UpdateNotificationStatusRequest obj = new UpdateNotificationStatusRequest();
            //obj.StatusId = 2;
            //obj.NotificationId = 5;
            var response = new AircraftHelper().UpdateNotificationStatus(obj);

            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdateNotificationResponseAPI(response, requestModel.requestId);
            return responseModel;
        }


        public GeneralResponse GetLiveData(RequestModel requestModel)
        {
            LiveDataRequestModel obj = new ConversionHelper().DecryptRequestToGetLiveDataAPIRequest(requestModel);
            var identifier = System.DateTime.UtcNow.Ticks;
            Log.Info("Request GetLiveData " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(obj));
            UnitDataHelper objHelper = new UnitDataHelper();
            var response = objHelper.ParseLiveData(obj, requestModel.requestId);
            Log.Info("Response GetLiveData " + identifier + Newtonsoft.Json.JsonConvert.SerializeObject(response));
            return response;
        }


        public ResponseModel RegisterMaintenanceUser(RequestModel requestModel)
        {

            var response = new ResponseModel();
            RegisterMaintenanceUserRequest obj = new ConversionHelper().DecryptRequestToRegisterMaintenanceUserAPIRequest(requestModel);

            RegisterMaintenanceUserResponse objResponse = new UserHelper().RegisterMaintenanceUser(obj);
            response.ResponseCode = objResponse.ResponseCode;
            response.ResponseMessage = objResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptRegisterMaintenanceUserAPI(objResponse, requestModel.requestId);
            return response;
        }

        public ResponseModel GenerateIssue(RequestModel requestModel)
        {

            var response = new ResponseModel();
            IssueModel obj = new ConversionHelper().DecryptRequestToGenerateIssueAPIRequest(requestModel);
            GenerateIssueResponseModel objResponse = new AircraftHelper().GenerateIssue(obj);
            response.ResponseCode = objResponse.ResponseCode;
            response.ResponseMessage = objResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptGenerateIssueAPI(objResponse, requestModel.requestId);

            return response;
        }


        public GeneralResponse AssignIssueToOtherUser(RequestModel requestModel)
        {
            ReassignIssueRequestModel obj = new ConversionHelper().DecryptReassignIssueAPIRequest(requestModel);
            GeneralResponse objResponse = new AircraftHelper().AssignIssueToOtherUser(obj);
            return objResponse;

        }

        public GeneralResponse UpdateIssue(RequestModel requestModel)
        {

            IssueModel obj = new ConversionHelper().DecryptUpdateIssueAPIRequest(requestModel);
            return new AircraftHelper().UpdateIssue(obj);
        }

        public GeneralResponse CloseIssue(RequestModel requestModel)
        {

            IssueModel obj = new ConversionHelper().DecryptUpdateIssueAPIRequest(requestModel);
            return new AircraftHelper().CloseIssueAPI(requestModel.requestId, obj.Id, obj.Comment);
        }

        public ResponseModel GetMaintenanceUserList(RequestModel objRequest)
        {
            var response = new ResponseModel();
            var reqModel = new ConversionHelper().DecrypGetMaintenanceUserListAPIRequest(objRequest);

            var userResponse = new UserHelper().GetMaintenanceUserList(objRequest.requestId, reqModel.LastUpdateDate);
            response.ResponseCode = userResponse.ResponseCode;
            response.ResponseMessage = userResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptGetMaintenanceUserListAPI(userResponse, objRequest.requestId);
            return response;
        }

        public ResponseModel GetSquawkList(RequestModel objRequest)
        {

            var response = new ResponseModel();
            var reqModel = new ConversionHelper().DecrypGetSquawkListAPIRequest(objRequest);

            var squawkResponse = new AircraftHelper().GetSquawkList(objRequest.requestId, reqModel.LastUpdateDate);
            response.ResponseCode = squawkResponse.ResponseCode;
            response.ResponseMessage = squawkResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptSquawkListAPI(squawkResponse, objRequest.requestId);
            return response;
        }


        public ResponseModel GetUsAirportList(RequestModel requestModel)
        {

            var response = new ResponseModel();
            var reqModel = new ConversionHelper().DecryptGetUsAirportList(requestModel);

            var apiResponse = new UserHelper().GetUsAirportList(reqModel.LastUpdateDateTime);
            response.ResponseCode = apiResponse.ResponseCode;
            response.ResponseMessage = apiResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptAirportListAPI(apiResponse, requestModel.requestId);
            return response;
        }


        public ResponseModel GetSubscription(RequestModel requestModel)
        {
            var response = new ResponseModel();
            var subList = new UserHelper().GetSubscription();
            response.ResponseCode = subList.ResponseCode;
            response.ResponseMessage = subList.ResponseMessage;
            response.Response = new ConversionHelper().EncryptSubscriptionAPI(subList, requestModel.requestId);
            return response;

        }


        public ResponseModel ValidateReceipt(RequestModel requestModel)
        {
            ExceptionHandler.ReportError(new Exception(), "Request for ValidateReceipt");
            ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(requestModel));

            var response = new ResponseModel();
            try
            {

                var reqModel = new ConversionHelper().DecryptValidateReceipt(requestModel);
                ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(reqModel));
                var validaterecResModel = new UserHelper().ValidateReceipt(reqModel, requestModel.requestId);
                response.ResponseCode = validaterecResModel.ResponseCode;
                response.ResponseMessage = validaterecResModel.ResponseMessage;
                ExceptionHandler.ReportError(new Exception(), "Response for ValidateReceipt");
                ExceptionHandler.ReportError(new Exception(), Newtonsoft.Json.JsonConvert.SerializeObject(validaterecResModel));

                response.Response = new ConversionHelper().EncryptSubscriptionAPI(validaterecResModel, requestModel.requestId);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ReportError(ex, "Exception occurs while processing validate receipt");
            }
            return response;
        }


        public ResponseModel PassengerProInFlightContent(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            PassengerProLoginResponse passengerProResponse = new AircraftHelper().PassengerProInFlightContent(requestModel.requestId);
            responseModel.ResponseCode = passengerProResponse.ResponseCode;
            responseModel.ResponseMessage = passengerProResponse.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptPassengerProLoginAPI(passengerProResponse, requestModel.requestId);
            return responseModel;
        }

    }
}