using GA.DataTransfer.Classes_for_Services;
using Mvc.Mailer;

namespace GuardianAvionics.Mailers
{
    public interface IUserMailer
    {
        MvcMailMessage Welcome();
        MvcMailMessage GoodBye();


        MvcMailMessage PilotLogSummary(PilotLogSummaryEmailModel modlel);
    }
}