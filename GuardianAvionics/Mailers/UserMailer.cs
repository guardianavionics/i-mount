using System.Collections.Generic;
using System.Net.Mail;
using GA.DataTransfer.Classes_for_Services;
using Mvc.Mailer;

namespace GuardianAvionics.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer 	
	{
		public UserMailer()
		{
			MasterName="_Layout";
		}
		
		public virtual MvcMailMessage Welcome()
		{
			//ViewBag.Data = someObject;
			return Populate(x =>
			{
				x.Subject = "Welcome";
				x.ViewName = "Welcome";
				x.To.Add("some-email@example.com");
			});
		}
 
		public virtual MvcMailMessage GoodBye()
		{
			//ViewBag.Data = someObject;
			return Populate(x =>
			{
				x.Subject = "GoodBye";
				x.ViewName = "GoodBye";
				x.To.Add("some-email@example.com");
			});
		}

        public virtual MvcMailMessage PilotLogSummary(PilotLogSummaryEmailModel model)
        {
            model = new PilotLogSummaryEmailModel
            {
                EndHobbsTime = "2 hrs",
                EndTachTime = "3 hrs",
                EstimatedFuelUsed = "10 Gallons",
                FlightCompletedOn = "1/14/14",
                LengthOfFlight = "1 hrs",
                Nnumber = "02143526",
                PilotName = "prabal",
                StartHobbsTime = "103",
                StartTachTime = "10.3",
            };

            ViewData.Model = model;

            var resources = new Dictionary<string, string>();
            resources["logo"] = System.Web.HttpContext.Current.Server.MapPath("~/Image/GA/logo.png");// "~/Image/GA/logo.png";
            //PopulateBody(a, "PilotSummaryEmail", resources);

            var a = Populate(z =>
            {
                z.Subject = "End of flight notification from the Pilot FMS App";
                z.ViewName = "PilotSummaryEmail";
                z.To.Add("Prabal.Khajanchi@ideavate.com");
                z.From = new MailAddress("Prabal.Khajanchi@ideavate.com");
                z.LinkedResources = resources;
            });


            return a;


            //var mailMessage = new MailMessage
            //{
            //    Subject = "End of flight notification from the Pilot FMS App",
            //    From = new MailAddress(ConfigurationReader.EmailId),
            //};

            //mailMessage.To.Add("Prabal.Khajanchi@ideavate.com");

            //var resources = new Dictionary<string, string>();
            //resources["logo"] =  System.Web.HttpContext.Current.Server.MapPath("~/Image/GA/logo.png");
            //PopulateBody(mailMessage, "PilotSummaryEmail", resources);

            //return (MvcMailMessage) mailMessage;

        }



 	}
}