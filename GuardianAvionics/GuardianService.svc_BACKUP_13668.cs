﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Script.Serialization;
using GA.ApplicationLayer;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using NLog;
using GA.Common;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace GuardianAvionics
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GuardianService : IGuardianService
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public ResponseModel RegisterUser(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UserModel userModel = new UserModel();
            try
            {
                userModel = new ConversionHelper().DecryptRegisterUserAPI(requestModel.request);
            }
            catch (Exception ex)
            {
                
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
            }

            var objUserHelper = new UserHelper();
            userModel.IsRegisterByAdmin = false;
            userModel.ManufacturerId = 0;
            userModel.UserType = "User";
            var response = objUserHelper.RegisterUser(userModel, false);

            

            if (response.ResponseCode != ((int)Enumerations.RegistrationReturnCodes.Success).ToString())
            {
                responseModel.ResponseCode = response.ResponseCode;
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.Response = "";
                return responseModel;
            }

            var objLoginHelper = new LoginHelper();
            LoginModel loginModel = new LoginModel
            {
                EmailId = userModel.EmailId,
                Password = userModel.Password
            };
            var loginResponse = objLoginHelper.UserLoginAPI(loginModel);

            

            if (loginResponse.ResponseMessage == Enumerations.LoginReturnCodes.Success.GetStringValue())
            {
                responseModel.ResponseMessage = loginResponse.ResponseMessage;
                responseModel.ResponseCode = loginResponse.ResponseCode;
                string email = loginModel.EmailId.ToLower();
                responseModel.Response = new ConversionHelper().EncryptNewLoginAPIResponse(loginResponse, loginModel.Password + email);
            }
            else
            {
                responseModel.ResponseMessage = loginResponse.ResponseMessage;
                responseModel.ResponseCode = loginResponse.ResponseCode;
                responseModel.Response = "";
            }
            return responseModel;
        }


        public ResponseModel GetIPassengerDetails(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            IPassengerRequestModel objRequest;
            try
            {
                objRequest = new ConversionHelper().DecryptIPassengerAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
            }
            var response = new IPassengerHelper().GetIPassengerMediaDetailsAPI(objRequest.AircraftId);
<<<<<<< HEAD

            

=======
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objRequest),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetIPassengerDetails",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
>>>>>>> APIErrorLogs
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptIPassengerAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        public ResponseModel LoginUser(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            LoginModel loginModel = new LoginModel();
            try
            {
                loginModel = new ConversionHelper().DecryptLoginAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
<<<<<<< HEAD
                
=======
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "LoginUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while desrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog
>>>>>>> APIErrorLogs
            }

            var objLoginHelper = new LoginHelper();
            var response = objLoginHelper.UserLoginAPI(loginModel);
<<<<<<< HEAD

            
=======
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(loginModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "LoginUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
>>>>>>> APIErrorLogs
            if (response.ResponseMessage == "Success")
            {
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.ResponseCode = response.ResponseCode;
                string email = loginModel.EmailId.ToLower();
                responseModel.Response = new ConversionHelper().EncryptNewLoginAPIResponse(response, loginModel.Password + email);
            }
            else
            {
                responseModel.ResponseMessage = response.ResponseMessage;
                responseModel.ResponseCode = response.ResponseCode;
                responseModel.Response = "";
            }
            return responseModel;
        }
        public ResponseModel SyncUserData(RequestModel requestModel)
        {
            SyncUserDataResponseModel syncResponse = new SyncUserDataResponseModel();
            ResponseModel response = new ResponseModel();
            SyncUserDataModel syncUserDataModel = new SyncUserDataModel();
            try
            {
                syncUserDataModel = new ConversionHelper().DecryptSyncUserData(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });

                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }

            var objUpdateHelper = new UserProfileUpdateHelper();
            syncResponse.UserProfileResponse = objUpdateHelper.UpdateUserProfile(syncUserDataModel.SyncUserProfile);
            #region APIErrorLog
            if (syncResponse.UserProfileResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.UserProfileResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api UpdateUserProfile"
                });
            }
            #endregion APIErrorLog

            var objAircraftHelper = new AircraftHelper();
            syncResponse.AircraftProfileResponse = objAircraftHelper.UpdateAircraftProfile(syncUserDataModel.SyncAircraftProfile);
            #region APIErrorLog
            if (syncResponse.AircraftProfileResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.AircraftProfileResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api UpdateAircraftProfile"
                });
            }
            #endregion APIErrorLog

            var objHelper = new UserHelper();
            syncResponse.LogBookResponse = objHelper.SaveLogBook(syncUserDataModel.syncLogBook);
            #region APIErrorLog
            if (syncResponse.LogBookResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.LogBookResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api SaveLogBook"
                });
            }
            #endregion APIErrorLog

            GetPrefereceModel getPrefereceModel = new GetPrefereceModel();
            getPrefereceModel.ProfileId = requestModel.requestId;
            syncResponse.PreferenceResponse = objHelper.GetPreference(getPrefereceModel);
            #region APIErrorLog
            if (syncResponse.PreferenceResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.PreferenceResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api GetPreference"
                });
            }
            #endregion APIErrorLog

            syncResponse.NotificationResponse = new AircraftHelper().GetNotification(requestModel.requestId, syncUserDataModel.SyncNotification.LastUpdateDate);
            #region APIErrorLog
            if (syncResponse.NotificationResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.NotificationResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api GetNotification"
                });
            }
            #endregion APIErrorLog

            syncResponse.MaintenanceUserResponse = objHelper.GetMaintenanceUserList(requestModel.requestId, syncUserDataModel.SyncMaintenanceUser.LastUpdateDate);
            #region APIErrorLog
            if (syncResponse.MaintenanceUserResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.MaintenanceUserResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api GetMaintenanceUserList"
                });
            }
            #endregion APIErrorLog

            syncResponse.SquawkResponse = objAircraftHelper.GetSquawkList(requestModel.requestId, syncUserDataModel.SyncSquawkList.LastUpdateDate);
            #region APIErrorLog
            if (syncResponse.SquawkResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.SquawkResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api GetSquawkList"
                });
            }
            #endregion APIErrorLog

            syncResponse.DropDownListResponse = objAircraftHelper.GetFieldsForDropDownlist(syncUserDataModel.SyncDropdownListData);
            #region APIErrorLog
            if (syncResponse.DropDownListResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(syncUserDataModel),
                    RequestId = requestModel.requestId,
                    Response = syncResponse.DropDownListResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "SyncUserData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error in sync api GetFieldsForDropDownlist"
                });
            }
            #endregion APIErrorLog

            response.Response = new ConversionHelper().EncryptSyncUserData(syncResponse, requestModel.requestId);
            return response;
        }
        public ResponseModel ThirdPartyLogin(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            RegisterAndLoginByThirdParty objUserModel;
            try
            {
                objUserModel = new ConversionHelper().DecryptThirdPartyLoginAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "ThirdPartyLogin",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog
            }
            var objLoginHelper = new UserHelper();
            var response = objLoginHelper.verifyThirdPartyLogin(objUserModel, false);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objUserModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "ThirdPartyLogin",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            if (response.ResponseMessage != Enumerations.LoginReturnCodes.Success.ToString())
            {
                responseModel.Response = "";
            }
            else
            {
                responseModel.Response = new ConversionHelper().EncryptThirdPartyLoginAPIResponse(response);
            }
            return responseModel;
        }
        public ResponseModel UserProfileUpdate(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ProfileUpdateRequestModel profileDetail;
            try
            {
                profileDetail = new ConversionHelper().DecryptUserProfileUpdateAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "UserProfileUpdate",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }

            var objUpdateHelper = new UserProfileUpdateHelper();
            var response = objUpdateHelper.UpdateUserProfile(profileDetail);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "UserProfileUpdate",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUserProfileUpdateAPIResponse(response, requestModel.requestId);
            return responseModel;
        }
        public ResponseModel UpdateAircraftProfile(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            AircraftProfileUpdateModel profileDetail;
            try
            {
                profileDetail = new ConversionHelper().DecryptUpdateAircraftProfileAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "UpdateAircraftProfile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }

            var objAircraftHelper = new AircraftHelper();
            var response = objAircraftHelper.UpdateAircraftProfile(profileDetail);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(profileDetail),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "UpdateAircraftProfile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdateAircraftProfileAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        public ResponseModel UpdatePreference(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UpdatePreferenceModel profileUpdate;
            try
            {
                profileUpdate = new ConversionHelper().DecryptUpdatePreferenceAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "UpdatePreference",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var objUserHelper = new UserHelper();
            var response = objUserHelper.UpdatePreference(profileUpdate);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(profileUpdate),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "UpdatePreference",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdatePreferenceAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        public ResponseModel GetPreference(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            GetPrefereceModel getPrefereceModel;
            try
            {
                getPrefereceModel = new ConversionHelper().DecryptGetPreferenceAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetPreference",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            getPrefereceModel.ProfileId = requestModel.requestId;
            var objUserHelper = new UserHelper();
            var response = objUserHelper.GetPreference(getPrefereceModel);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(getPrefereceModel),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetPreference",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetPreferenceAPIResponse(response, requestModel.requestId);
            return responseModel;
        }
        public GeneralResponse ResetPassword(RequestModel requestModel)
        {
            GeneralResponse responseModel = new GeneralResponse();
            ResetPasswordModel userDetails;
            try
            {
                userDetails = new ConversionHelper().DecryptResetPasswordAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "ResetPassword",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var objUserHelper = new UserHelper();
            var response = objUserHelper.ResetPassword(userDetails);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(userDetails),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "ResetPassword",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }

        public ResponseModel LogBook(RequestModel requestModel)
        {

            ResponseModel responseModel = new ResponseModel();
            LogBookRequestModel logbook;
            try
            {
                logbook = new ConversionHelper().DecryptLogBookAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "LogBook",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var objHelper = new UserHelper();
            var logbookRes = objHelper.SaveLogBook(logbook);
            #region APIErrorLog
            if (logbookRes.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(logbook),
                    RequestId = requestModel.requestId,
                    Response = logbookRes.ResponseMessage,
                    Module = "API",
                    MethodName = "LogBook",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = logbookRes.ResponseCode;
            responseModel.ResponseMessage = logbookRes.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptLogBookAPIResponse(logbookRes, requestModel.requestId);
            return responseModel;
        }
        public GeneralResponse UpdateLogWithPilotAndCoPilot(RequestModel requestModel)
        {
            GeneralResponse responseModel = new GeneralResponse();
            UpdateLogBookForPilotAndCopilot model;
            try
            {
                model = new ConversionHelper().DecryptUpdateLogWithPilotAndCoPilotAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "UpdateLogWithPilotAndCoPilot",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var objHelper = new UserHelper();
            var logbookRes = objHelper.UpdateLogWithPilotAndCoPilot(model);
            #region APIErrorLog
            if (logbookRes.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = logbookRes.ResponseMessage,
                    Module = "API",
                    MethodName = "UpdateLogWithPilotAndCoPilot",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return logbookRes;
        }

        public ResponseModel AircraftSearch(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            AircraftSearchRequest aircraft;
            try
            {
                aircraft = new ConversionHelper().DecryptAircraftSearchAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "AircraftSearch",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new AircraftHelper().SearchAircraft(aircraft);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(aircraft),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "AircraftSearch",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptAircraftSearchAPIResponse(response, requestModel.requestId);

            return responseModel;
        }

        public ResponseModel CreateUpdateAircraftNNumber(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            AircraftModifyNnumberRequest aircraft;
            try
            {
                aircraft = new ConversionHelper().DecryptCreateUpdateAircraftNNumberAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "CreateUpdateAircraftNNumber",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new AircraftHelper().ModifyAircraftNNumber(aircraft);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(aircraft),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "CreateUpdateAircraftNNumber",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptCreateUpdateAircraftNNumberAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        /// <summary>
        /// Sends as response all the related documents for the user
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public ResponseModel GetDocument(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            DocumnetRequestModel document = new DocumnetRequestModel();
            document.ProfileId = requestModel.requestId;
            var response = new UnitDataHelper().GetDocumentForApp(document);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(document),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetDocument",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetDocumentAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResponseModel GetFieldsForDropDownlistAndConstants(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ManufacturereModelListRequest model;
            try
            {
                model = new ConversionHelper().DecryptGetFieldsForDropDownlistAndConstantsAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetFieldsForDropDownlistAndConstants",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new AircraftHelper().GetFieldsForDropDownlist(model);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetFieldsForDropDownlistAndConstants",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetFieldsForDropDownlistAndConstantsAPIResponse(response, requestModel.requestId);
            return responseModel;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GetUserNameByEmailResponseModel GetUserNameByEmailId(GetUserNameByEmailRequestModel model)
        {
            var response = new UserHelper().GetUserNameByEmailId(model);
            return response;
        }

        public ResponseModel GetMapFilesUrl(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            MapFilesRequest model;
            try
            {
                model = new ConversionHelper().DecryptGetMapFilesUrlAPIREquest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetMapFilesUrl",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new UnitDataHelper().GetMapFilesUrlFromDb(model);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetMapFilesUrl",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetMapFilesUrlAPIResponse(response);
            return responseModel;
        }

        public ResponseModel GetPassengerMapFileUrl()
        {
            ResponseModel responseModel = new ResponseModel();
            var response = new UnitDataHelper().GetPassengerMapFileUrl();
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "",
                    RequestId = 0,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetPassengerMapFileUrl",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetPassengerMapFileUrlAPIResponse(response);
            return responseModel;
        }

        public GeneralResponse DeleteAeroUnit(RequestModel requestModel)
        {
            GeneralResponse response = new GeneralResponse();
            AeroUnitNumberRequestModel model;
            try
            {
                model = new ConversionHelper().DecryptDeleteAeroUnitAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "DeleteAeroUnit",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog  
            }
            response = new AircraftHelper().DeleteAeroUnit(model);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "DeleteAeroUnit",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }

        public GeneralResponse RegisterFMS(RequestModel requestModel)
        {
            GeneralResponse responseModel = new GeneralResponse();
            AeroUnitNumberRequestModel model;
            try
            {
                model = new ConversionHelper().DecryptIsFMSRegisterAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "RegisterFMS",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            responseModel = new AircraftHelper().RegisterFMS(model);
            #region APIErrorLog
            if (responseModel.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = responseModel.ResponseMessage,
                    Module = "API",
                    MethodName = "RegisterFMS",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return responseModel;
        }

        public ResponseModel ISFMSRegisterWithAircraft(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            AeroUnitRequestModel model;
            try
            {
                model = new ConversionHelper().DecryptISFMSRegisterWithAircraftAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "ISFMSRegisterWithAircraft",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new AircraftHelper().ISFMSRegisterWithAircraft(model);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "ISFMSRegisterWithAircraft",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptISFMSRegisterWithAircraftAPIResponse(response, requestModel.requestId);
            return responseModel;
        }

        public ResponseModel SendPushMessageOnFlightStart(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            SendPushMessageOnFlightStartRequestModel obj;
            try
            {
                obj = new ConversionHelper().DecryptSendPushMessageOnFlightStartAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "SendPushMessageOnFlightStart",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            AircraftHelper objHelper = new AircraftHelper();
            responseModel = objHelper.SendPushMessageAfterFlightStart(requestModel.requestId, obj.AircraftId, obj.DeviceTokenId);
            #region APIErrorLog
            if (responseModel.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = responseModel.ResponseMessage,
                    Module = "API",
                    MethodName = "SendPushMessageOnFlightStart",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return responseModel;
        }


        public GeneralResponse ChangePassword(RequestModel requestModel)
        {
            GeneralResponse response = new GeneralResponse();
            ChangePasswordRequetModel model;
            try
            {
                model = new ConversionHelper().DecryptChangePasswordAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "ChangePassword",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog  
            }
            response = new LoginHelper().ChangePassword(model);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "ChangePassword",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }

        public SecurityQuestionRequestModel SecurityQuestion()
        {
            SecurityQuestionRequestModel response = new SecurityQuestionRequestModel
            {
                SecurityQuestionList = new List<ListItems>()
            };
            try
            {
                response = new LoginHelper().GetSecurityQuestion();
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "",
                    RequestId = 0,
                    Response = "",
                    Module = "API",
                    MethodName = "SecurityQuestion",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
                return response;
                #endregion APIErrorLog  
            }
            return response;
        }

        public ResponseModel GetSecurityQuestionByEmailId(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ResetPasswordModel objEmail;
            try
            {
                objEmail = new ConversionHelper().DecryptGetSecurityQuestionByEmailIdAPIRequest(requestModel.request);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetSecurityQuestionByEmailId",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new UserHelper().GetSecurityQuestionByEmailId(objEmail);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objEmail),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetSecurityQuestionByEmailId",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetSecurityQuestionByEmailIdAPIResponse(response);
            return responseModel;
        }

        public GeneralResponse GetDataFile(Stream file)
        {
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            GeneralResponse response = new GeneralResponse();
            int? applicationheader = null;
            int profileId = 0;
            long uniqueId = 0;
            bool isAircraftIdExist = false;
            bool isProfileIdExist = false;
            bool isUniqueIdExist = false;

            if (woc.Headers["AircraftId"] != null)
            {
                applicationheader = Convert.ToInt32(woc.Headers["AircraftId"]);
                isAircraftIdExist = true;
            }


            if (woc.Headers["ProfileId"] != null)
            {
                profileId = Convert.ToInt32(woc.Headers["ProfileId"]);
                isProfileIdExist = true;
            }


            if (woc.Headers["UniqueId"] != null)
            {
                uniqueId = Convert.ToInt64(woc.Headers["UniqueId"]);
                isUniqueIdExist = true;
            }

            if (!isAircraftIdExist || !isProfileIdExist || !isUniqueIdExist)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "",
                    RequestId = 0,
                    Response = "",
                    Module = "API",
                    MethodName = "GetDataFile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = (isAircraftIdExist ? "" : "AircraftId Not Exist ") + " " + (isProfileIdExist ? "" : "ProfileId Not Exist ") + " " + (isUniqueIdExist ? "" : "UniqueId Not Exist ")
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
                #endregion APIErrorLog  
            }

            string encryptedFileName = "";

            try
            {
                response = new UnitDataHelper().GetDataFile(file, applicationheader, profileId, uniqueId, out encryptedFileName);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "",
                    RequestId = 0,
                    Response = "",
                    Module = "API",
                    MethodName = "GetDataFile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while store the datafile " + encryptedFileName + " and Header details are AircraftId = " + Convert.ToString(applicationheader ?? 0) + ", ProfileId = " + Convert.ToString(profileId) + ", UniqueId = " + Convert.ToString(uniqueId)
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Success).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Success.GetStringValue();
                return response;
                #endregion APIErrorLog  
            }
            return response;
        }


        public ResponseModel GetChartDetails(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            ChartDetailsRequestModel obj;
            try
            {
                obj = new ConversionHelper().DecryptGetChartDetails(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "",
                    Module = "API",
                    MethodName = "GetChartDetails",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "Error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog  
            }
            var response = new AdminHelper().GetChartDetails(obj.LastUpdateDateTime, obj.LastChangedDataTimeForPlate, obj.StateIds);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetChartDetails",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.Response = new ConversionHelper().EncryptGetChartDetails(response, requestModel.requestId);
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            return responseModel;
        }

        public GeneralResponse ResetLastTransactionId(int aircraftId)
        {
            GeneralResponse response = new GeneralResponse();
            var objHelper = new AircraftHelper();
            response = objHelper.SetLastTransactionId(aircraftId);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "aircraftId =" + aircraftId.ToString(),
                    RequestId = 0,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "ResetLastTransactionId",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }


        public ResponseModel GetHardwareFirmwareUploadFiles()
        {
            ResponseModel responseModel = new ResponseModel();
            var response = new AdminHelper().GetHardwareFirmwareFileListAPI();
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = "",
                    JsonRequest = "",
                    RequestId = 0,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetHardwareFirmwareUploadFiles",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptGetHardwareFirmwareFileUrlAPIResponse(response);
            return responseModel;
        }

        public void StartDataFilePArsingWithThread()
        {
            SetQueus obj = new SetQueus();
            obj.SetQueusForParsingDataFiles();
        }


        public GeneralResponse GetAudioFile(Stream file)
        {
            IncomingWebRequestContext woc = WebOperationContext.Current.IncomingRequest;
            string fileName = woc.Headers["FileName"];
            var response = new UnitDataHelper().GetAudioFile(file, fileName);
            return response;
        }

        public ResponseModel GetPlatesData(RequestModel requestModel)
        {

            PlatesResponseModel objResponse = new PlatesResponseModel();
            PlatesRequestModel objPlate = new PlatesRequestModel();
            ResponseModel responseModel = new ResponseModel();
            try
            {
                try
                {
                    objPlate = new ConversionHelper().DecryptGetPlatesDataAPIRequest(requestModel.request);
                }
                catch (Exception ex)
                {
                    #region APIErrorLog
                    new ErrorHelper().SerErrorLog(new ErrorLogModel
                    {
                        EncryptRequest = requestModel.request,
                        JsonRequest = "",
                        RequestId = requestModel.requestId,
                        Response = "Exception",
                        Module = "API",
                        MethodName = "GetPlatesData",
                        ServerName = ConfigurationReader.ServerName,
                        Date = DateTime.UtcNow,
                        IsResolved = false,
                        Remark = "Error while decrytp the data"
                    });

                    responseModel.Response = null;
                    responseModel.ResponseCode = "9999";
                    responseModel.ResponseMessage = "Exception";
                    return responseModel;
                    #endregion APIErrorLog
                }
                objResponse.StateList = new AdminHelper().GetPlatesData(objPlate.arrStateId);
                responseModel.ResponseCode = "0";
                responseModel.ResponseMessage = "Succcess";
                responseModel.Response = new ConversionHelper().EncryptGetPlatesDataAPIResponse(objResponse);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objPlate),
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetPlatesData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
                responseModel.Response = null;
                responseModel.ResponseCode = "9999";
                responseModel.ResponseMessage = "Exception";
                #endregion APIErrorLog
            }
            return responseModel;
        }

        public GeneralResponse SendRequestToOwnerForAircraft(RequestModel objModel)
        {
            GeneralResponse response = new GeneralResponse();
            RequestToOwnerForAircraftModel obj = new RequestToOwnerForAircraftModel();
            try
            {
                obj = new ConversionHelper().DecryptRequestToOwnerForAircraftAPIRequest(objModel.request, objModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objModel.request,
                    JsonRequest = "",
                    RequestId = objModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "SendRequestToOwnerForAircraft",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            response = new AircraftHelper().SendRequestToOwnerForAircraft(obj);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = objModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "SendRequestToOwnerForAircraft",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }


        public ResponseModel GetNotifications(RequestModel objModel)
        {
            ResponseModel response = new ResponseModel();
            GetNotificationRequest obj;
            try
            {
                obj = new ConversionHelper().DecryptRequestToGetNotificationAPIRequest(objModel.request, objModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objModel.request,
                    JsonRequest = "",
                    RequestId = objModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetNotifications",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            NotificationResponse notificationResponse = new AircraftHelper().GetNotification(objModel.requestId, obj.LastUpdateDate);
            #region APIErrorLog
            if (notificationResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = objModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetNotifications",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = notificationResponse.ResponseCode;
            response.ResponseMessage = notificationResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptNotificationAPI(notificationResponse, objModel.requestId);
            return response;
        }

        public ResponseModel GetKMLFile(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            KMLFileRequestModel objRequest;
            try
            {
                objRequest = new ConversionHelper().DecryptRequestForKMLFile(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetKMLFile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog

            }
            var response = new UnitDataHelper().GetKMLFileURL(objRequest.FlightId);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(objRequest),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetKMLFile",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptKMLFileAPI(response, requestModel.requestId);
            return responseModel;
        }


        public ResponseModel UpdateNotificationStatus(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            UpdateNotificationStatusRequest obj;
            try
            {
                obj = new ConversionHelper().DecryptRequestToUpdateNotificationAPIRequest(requestModel.request, requestModel.requestId);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "UpdateNotificationStatus",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                responseModel.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                responseModel.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return responseModel;
                #endregion APIErrorLog
            }
            var response = new AircraftHelper().UpdateNotificationStatus(obj);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "UpdateNotificationStatus",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = response.ResponseCode;
            responseModel.ResponseMessage = response.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptUpdateNotificationResponseAPI(response, requestModel.requestId);
            return responseModel;
        }


        public GeneralResponse GetLiveData(RequestModel requestModel)
        {
            GeneralResponse response = new GeneralResponse();
            LiveDataRequestModel obj;
            try
            {
                obj = new ConversionHelper().DecryptRequestToGetLiveDataAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetLiveData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            UnitDataHelper objHelper = new UnitDataHelper();
            response = objHelper.ParseLiveData(obj, requestModel.requestId);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "GetLiveData",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }


        public ResponseModel RegisterMaintenanceUser(RequestModel requestModel)
        {
            var response = new ResponseModel();
            RegisterMaintenanceUserRequest obj;
            try
            {
                obj = new ConversionHelper().DecryptRequestToRegisterMaintenanceUserAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "RegisterMaintenanceUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            RegisterMaintenanceUserResponse objResponse = new UserHelper().RegisterMaintenanceUser(obj);
            #region APIErrorLog
            if (objResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = objResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "RegisterMaintenanceUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = objResponse.ResponseCode;
            response.ResponseMessage = objResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptRegisterMaintenanceUserAPI(objResponse, requestModel.requestId);
            return response;
        }

        public ResponseModel GenerateIssue(RequestModel requestModel)
        {
            var response = new ResponseModel();
            IssueModel obj;
            try
            {
                obj = new ConversionHelper().DecryptRequestToGenerateIssueAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GenerateIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            GenerateIssueResponseModel objResponse = new AircraftHelper().GenerateIssue(obj);
            #region APIErrorLog
            if (objResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = objResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "GenerateIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = objResponse.ResponseCode;
            response.ResponseMessage = objResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptGenerateIssueAPI(objResponse, requestModel.requestId);

            return response;
        }


        public GeneralResponse AssignIssueToOtherUser(RequestModel requestModel)
        {
            GeneralResponse objResponse = new GeneralResponse();
            ReassignIssueRequestModel obj;
            try
            {
                obj = new ConversionHelper().DecryptReassignIssueAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "AssignIssueToOtherUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                objResponse.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                objResponse.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return objResponse;
                #endregion APIErrorLog
            }
            objResponse = new AircraftHelper().AssignIssueToOtherUser(obj);
            #region APIErrorLog
            if (objResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = objResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "AssignIssueToOtherUser",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return objResponse;
        }

        public GeneralResponse UpdateIssue(RequestModel requestModel)
        {
            GeneralResponse response = new GeneralResponse();
            IssueModel obj;
            try
            {
                obj = new ConversionHelper().DecryptUpdateIssueAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "UpdateIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            response = new AircraftHelper().UpdateIssue(obj);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "UpdateIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }

        public GeneralResponse CloseIssue(RequestModel requestModel)
        {
            GeneralResponse response = new GeneralResponse();
            IssueModel obj;
            try
            {
                obj = new ConversionHelper().DecryptUpdateIssueAPIRequest(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "CloseIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            response = new AircraftHelper().CloseIssueAPI(requestModel.requestId, obj.Id, obj.Comment);
            #region APIErrorLog
            if (response.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(obj),
                    RequestId = requestModel.requestId,
                    Response = response.ResponseMessage,
                    Module = "API",
                    MethodName = "CloseIssue",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            return response;
        }

        public ResponseModel GetMaintenanceUserList(RequestModel objRequest)
        {
            var response = new ResponseModel();
            MaintenanceUserRequest reqModel;
            try
            {
                reqModel = new ConversionHelper().DecrypGetMaintenanceUserListAPIRequest(objRequest);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objRequest.request,
                    JsonRequest = "",
                    RequestId = objRequest.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetMaintenanceUserList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            var userResponse = new UserHelper().GetMaintenanceUserList(objRequest.requestId, reqModel.LastUpdateDate);
            #region APIErrorLog
            if (userResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objRequest.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(reqModel),
                    RequestId = objRequest.requestId,
                    Response = userResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "GetMaintenanceUserList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = userResponse.ResponseCode;
            response.ResponseMessage = userResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptGetMaintenanceUserListAPI(userResponse, objRequest.requestId);
            return response;
        }

        public ResponseModel GetSquawkList(RequestModel objRequest)
        {

            var response = new ResponseModel();
            SquawkListRequest reqModel;
            try
            {
                reqModel = new ConversionHelper().DecrypGetSquawkListAPIRequest(objRequest);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objRequest.request,
                    JsonRequest = "",
                    RequestId = objRequest.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetSquawkList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            var squawkResponse = new AircraftHelper().GetSquawkList(objRequest.requestId, reqModel.LastUpdateDate);
            #region APIErrorLog
            if (squawkResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = objRequest.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(reqModel),
                    RequestId = objRequest.requestId,
                    Response = squawkResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "GetSquawkList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = squawkResponse.ResponseCode;
            response.ResponseMessage = squawkResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptSquawkListAPI(squawkResponse, objRequest.requestId);
            return response;
        }


        public ResponseModel GetUsAirportList(RequestModel requestModel)
        {

            var response = new ResponseModel();
            UsAirportRequestModel reqModel;
            try
            {
                reqModel = new ConversionHelper().DecryptGetUsAirportList(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "GetUsAirportList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            var apiResponse = new UserHelper().GetUsAirportList(reqModel.LastUpdateDateTime);
            #region APIErrorLog
            if (apiResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(reqModel),
                    RequestId = requestModel.requestId,
                    Response = apiResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "GetUsAirportList",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = apiResponse.ResponseCode;
            response.ResponseMessage = apiResponse.ResponseMessage;
            response.Response = new ConversionHelper().EncryptAirportListAPI(apiResponse, requestModel.requestId);
            return response;
        }


        public ResponseModel GetSubscription(RequestModel requestModel)
        {
            var response = new ResponseModel();
            var subList = new UserHelper().GetSubscription();
            #region APIErrorLog
            if (subList.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = subList.ResponseMessage,
                    Module = "API",
                    MethodName = "GetSubscription",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = subList.ResponseCode;
            response.ResponseMessage = subList.ResponseMessage;
            response.Response = new ConversionHelper().EncryptSubscriptionAPI(subList, requestModel.requestId);
            return response;

        }


        public ResponseModel ValidateReceipt(RequestModel requestModel)
        {
            var response = new ResponseModel();
            GA.DataTransfer.Classes_for_Web.ValidateReceiptRequestModel reqModel;
            try
            {
                reqModel = new ConversionHelper().DecryptValidateReceipt(requestModel);
            }
            catch (Exception ex)
            {
                #region APIErrorLog
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = "Exception",
                    Module = "API",
                    MethodName = "ValidateReceipt",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = "error while decrypt the data"
                });
                response.ResponseCode = ((int)Enumerations.RegistrationReturnCodes.Exception).ToString();
                response.ResponseMessage = Enumerations.RegistrationReturnCodes.Exception.GetStringValue();
                return response;
                #endregion APIErrorLog
            }
            var validaterecResModel = new UserHelper().ValidateReceipt(reqModel, requestModel.requestId);
            #region APIErrorLog
            if (validaterecResModel.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(reqModel),
                    RequestId = requestModel.requestId,
                    Response = validaterecResModel.ResponseMessage,
                    Module = "API",
                    MethodName = "ValidateReceipt",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            response.ResponseCode = validaterecResModel.ResponseCode;
            response.ResponseMessage = validaterecResModel.ResponseMessage;
            response.Response = new ConversionHelper().EncryptSubscriptionAPI(validaterecResModel, requestModel.requestId);
            return response;
        }


        public ResponseModel PassengerProInFlightContent(RequestModel requestModel)
        {
            ResponseModel responseModel = new ResponseModel();
            PassengerProLoginResponse passengerProResponse = new AircraftHelper().PassengerProInFlightContent(requestModel.requestId);
            #region APIErrorLog
            if (passengerProResponse.ResponseCode == ((int)Enumerations.RegistrationReturnCodes.Exception).ToString())
            {
                new ErrorHelper().SerErrorLog(new ErrorLogModel
                {
                    EncryptRequest = requestModel.request,
                    JsonRequest = "",
                    RequestId = requestModel.requestId,
                    Response = passengerProResponse.ResponseMessage,
                    Module = "API",
                    MethodName = "PassengerProInFlightContent",
                    ServerName = ConfigurationReader.ServerName,
                    Date = DateTime.UtcNow,
                    IsResolved = false,
                    Remark = ""
                });
            }
            #endregion APIErrorLog
            responseModel.ResponseCode = passengerProResponse.ResponseCode;
            responseModel.ResponseMessage = passengerProResponse.ResponseMessage;
            responseModel.Response = new ConversionHelper().EncryptPassengerProLoginAPI(passengerProResponse, requestModel.requestId);
            return responseModel;
        }

    }
}