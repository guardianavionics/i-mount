﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using GA.DataTransfer;
using GA.DataTransfer.Classes_for_Services;
using System.ComponentModel; // description tag is in this namespace
//using System.EnterpriseServices;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace GuardianAvionics
{
    [ServiceContract]
    public interface IGuardianService
    {


        [Description("Create and register a new user , with user preference")]
        [OperationContract]
        [WebInvoke(Method = "POST",
        UriTemplate = "/RegisterUser", BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel RegisterUser(RequestModel userModel);

        [Description("Authenticates user to login in the system , return all the user data in response.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/LoginUser", ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        ResponseModel LoginUser(RequestModel req);


        [Description("Sync User Data")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/SyncUserData", ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        ResponseModel SyncUserData(RequestModel syncUserDataModel);

        [Description("Login by Facebook or Gmail, if account is not register  then Register this user as FB, Gmail user in our Website. ThirdPartyAccountType = 1 for FB and ThirdPartyAccountType = 2 for Gmail")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/ThirdPartyLogin", ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        ResponseModel ThirdPartyLogin(RequestModel objUserModel);



        [Description("Update the User Profile.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
            , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UserProfileUpdate")]
        ResponseModel UserProfileUpdate(RequestModel requestModel);

        [Description("Update the Aircraft Profile.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
            , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateAircraftProfile")]
        ResponseModel UpdateAircraftProfile(RequestModel requestModel);

        [Description("Update the Default values, User Email List, Restore Aircraft, Pilot Flying Summary and Changed Password")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
            , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdatePreference")]
        ResponseModel UpdatePreference(RequestModel requestModel);

        [Description("Get the Default values, User Email List and Changed Password")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
            , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPreference")]
        ResponseModel GetPreference(RequestModel requestModel);

        [Description("Send the Password to user's email ID")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
            , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/ResetPassword")]
        GeneralResponse ResetPassword(RequestModel requestModel);


        [Description("get aircraftData logs")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetAirframeDatalogList"
        , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ResponseModel GetAirframeDatalogList(RequestModel requestModel);


        //[Description("Upload JPI Data")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/UploadDataString"
        //    , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        //FileUploadResponseModel UploadDataString(FileUploadRequestModel dataAsString);

        [Description("Save Pilot Logs")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/LogBook"
        , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ResponseModel LogBook(RequestModel requestModel);

        [Description("Save Pilot Logs with Pilot and CoPilot Id")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/UpdateLogWithPilotAndCoPilot"
            , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        GeneralResponse UpdateLogWithPilotAndCoPilot(RequestModel requestModel);

        [Description("Searches aircraft master file and aircraft reference file with tail number for aircraft profile fields.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/AircraftSearch"
            , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]

        ResponseModel AircraftSearch(RequestModel requestModel);


        [Description("Creates Aircraft and updates aircraft details when N-Number is modified.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/CreateUpdateAircraftNNumber"
            , ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]

        ResponseModel CreateUpdateAircraftNNumber(RequestModel requestModel);

        [Description("Send server url for documents which are created or updated.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetDocument"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        ResponseModel GetDocument(RequestModel requestModel);


        // subscription verification service
        //[Description("Checks the subscription of User to Application from apple server.")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SubscriptionVerification"
        //    , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        //SubscriptionResponseModel SubscriptionVerification(SubscriptionRequestModel model);



        //*123
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Description("Sends the list of all values of all dropdown list and other constant values.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetFieldsForDropDownlistAndConstants"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        ResponseModel GetFieldsForDropDownlistAndConstants(RequestModel requestModel);

        [Description("Get the User FirstName and LastName by EmailId")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetUserNameByEmailId"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GetUserNameByEmailResponseModel GetUserNameByEmailId(GetUserNameByEmailRequestModel model);

        [Description("Service for fetching url for map file download")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetMapFilesUrl"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        ResponseModel GetMapFilesUrl(RequestModel requestModel);

        [Description("Service for fetching url for passenger map file download")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetPassengerMapFileUrl"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetPassengerMapFileUrl();

        //[Description("Search AeroUnit, If already exists then return True else return False")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/IsFmsExists"
        //   , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //GeneralResponse IsFmsExists(RequestModel requestModel);


        [Description("To Delete the Aero Unit")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/DeleteAeroUnit"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse DeleteAeroUnit(RequestModel requestModel);

        [Description("Search AeroUnit, If already exists then return True and If not exists then first make entry in table and return False")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/RegisterFMS"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse RegisterFMS(RequestModel requestModel);


        [Description("Check that the AeroUnit is Register with Any Aircraft or Not")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/ISFMSRegisterWithAircraft"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel ISFMSRegisterWithAircraft(RequestModel requestModel);


        [Description("Sends the push notification message to the Aircraft owner when the pilot starts taking the flight.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SendPushMessageOnFlightStart"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel SendPushMessageOnFlightStart(RequestModel requestModel);


        [Description("Change Password")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/ChangePassword"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse ChangePassword(RequestModel requestModel);


        [Description("Get All Security Questions")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SecurityQuestion"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SecurityQuestionRequestModel SecurityQuestion();


        [Description("Get Security Questions By EmailId")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetSecurityQuestionByEmailId"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetSecurityQuestionByEmailId(RequestModel requestModel);

        [Description("Get OTP on EmailId and phonemunber")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SendVerificationOTPOnPhoneandEmail"
          , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel SendVerificationOTPOnPhoneandEmail(RequestModel requestModel);

        [Description("Get the  demo data file")]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetDemoFlightDataFile"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse GetDemoFlightDataFile(Stream file);

        [Description("Get the .data file")]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetDataFile"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse GetDataFile(Stream file);

        [Description("Get the Audio file")]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetAudioFile"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse GetAudioFile(Stream file);

        [Description("Set the Last Transaction Id of the aircraft")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/ResetLastTransactionId"
           , ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse ResetLastTransactionId(int aircraftId);

        //[Description("For testing of email")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/test"
        //    , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //string Test(string type);


        //[Description("For testing of email")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/TestStoreData"
        //    , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //GeneralResponse TestStoreData(string type);

        [Description("To Get All Hardware Firmware Files")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetHardwareFirmwareUploadFiles"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetHardwareFirmwareUploadFiles();





        [Description("For Testing Purpose")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/StartDataFilePArsingWithThread"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void StartDataFilePArsingWithThread();



        [Description("To Get IPassenger Details")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetIPassengerDetails"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetIPassengerDetails(RequestModel requestModel);

        [Description("To Get Chart Details")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetChartDetails"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetChartDetails(RequestModel requestModel);

        [Description("To Get Chart Details")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetChartDetailss"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ChartDetailsResponseModel GetChartDetailss(RequestModel requestModel);
        //[Description("To Get only updated plates details")]
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetUpdatedPlates"
        //   , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //ResponseModel GetUpdatedPlates(RequestModel requestModel);

        [Description("To Get Plates Details")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetPlatesData"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetPlatesData(RequestModel requestModel);

        [Description("Send request to access the aircraft ")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/SendRequestToOwnerForAircraft"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse SendRequestToOwnerForAircraft(RequestModel objModel);

        [Description("Get all the notification for user")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetNotifications"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetNotifications(RequestModel objModel);


        [Description("Response to Notification request")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/UpdateNotificationStatus"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel UpdateNotificationStatus(RequestModel requestModel);


        [Description("Response to KML File request")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetKMLFile"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetKMLFile(RequestModel requestModel);

        [Description("Parse Live Data")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetLiveData"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse GetLiveData(RequestModel requestModel);


        [Description("Maintenance User Registration")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/RegisterMaintenanceUser"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel RegisterMaintenanceUser(RequestModel requestModel);


        [Description("Generate Issue related to aircraft and assign to the maintenence user.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GenerateIssue"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GenerateIssue(RequestModel requestModel);



        [Description("Assign an existing issue to the another maintenance user.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/AssignIssueToOtherUser"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse AssignIssueToOtherUser(RequestModel requestModel);

        [Description("To set the status of an issue.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/UpdateIssue"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse UpdateIssue(RequestModel requestModel);

        [Description("To get the maintenance user list.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetMaintenanceUserList"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetMaintenanceUserList(RequestModel objRequest);


        [Description("To get the squawk list.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetSquawkList"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetSquawkList(RequestModel objRequest);

        [Description("To close the issue.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/CloseIssue"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneralResponse CloseIssue(RequestModel requestModel);


        [Description("Get the text file URl for Us Airport List")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetUsAirportList"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetUsAirportList(RequestModel requestModel);

        [Description("Get the Subscription plan List")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/GetSubscription"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel GetSubscription(RequestModel requestModel);

        [Description("Validate Receipt")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/ValidateReceipt"
         , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel ValidateReceipt(RequestModel requestModel);

     

        [Description("get all aircraft with ipassenger details")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/PassengerProInFlightContent"
      , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResponseModel PassengerProInFlightContent(RequestModel requestModel);
        //gp
        [Description("Insert the Aircraft Detail.")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
          , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/InsertAircraftDetails")]
        ResponseModel InsertAircraftDetails(RequestModel requestModel);

        [Description("Get detail")]
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json
         , ResponseFormat = WebMessageFormat.Json, UriTemplate = "/getfueldetail")]
        ResponseModel getfueldetail(RequestModel requestModel);

    }
}
