﻿using System.ComponentModel.DataAnnotations;

namespace GA.DataLayer
{
    [MetadataType(typeof(Profile_Validations))]
    public partial class Profile
    {

    }
    public class Profile_Validations
    {
        [Required(ErrorMessage="Required")]
        public string EmailId { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
