//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Document
    {
        public Document()
        {
            this.AdminDocsDeletedFromUsers = new HashSet<AdminDocsDeletedFromUser>();
        }
    
        public int Id { get; set; }
        public string Url { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public bool Deleted { get; set; }
        public int ProfileId { get; set; }
        public bool IsForAll { get; set; }
        public Nullable<int> AircraftId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public Nullable<int> FileSize { get; set; }
        public string MimeType { get; set; }
    
        public virtual AircraftProfile AircraftProfile { get; set; }
        public virtual ICollection<AdminDocsDeletedFromUser> AdminDocsDeletedFromUsers { get; set; }
    }
}
