//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerType
    {
        public CustomerType()
        {
            this.Invoice = new HashSet<Invoice>();
            this.InventoryCustomer = new HashSet<InventoryCustomer>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    
        public virtual ICollection<Invoice> Invoice { get; set; }
        public virtual ICollection<InventoryCustomer> InventoryCustomer { get; set; }
    }
}
