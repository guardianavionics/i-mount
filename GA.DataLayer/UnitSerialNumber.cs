//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnitSerialNumber
    {
        public UnitSerialNumber()
        {
            this.UnitStatusHistories = new HashSet<UnitStatusHistory>();
            this.SalesReturn = new HashSet<SalesReturn>();
            this.MappingInvoiceAndUnit = new HashSet<MappingInvoiceAndUnit>();
            this.RMAUnit = new HashSet<RMAUnit>();
        }
    
        public int Id { get; set; }
        public Nullable<int> SerialNumber { get; set; }
        public int UnitCurrentStatusId { get; set; }
        public Nullable<int> PurchaseOrderId { get; set; }
        public Nullable<int> InvoiceId { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public Nullable<int> BoardSerialNumber { get; set; }
        public Nullable<bool> IsSerialNoInSync { get; set; }
        public string SoftwareLevel { get; set; }
        public string NewPartNumber { get; set; }
        public string NewModelNumber { get; set; }
        public Nullable<int> PartNumberId { get; set; }
    
        public virtual Invoice Invoice { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual UnitStatus UnitStatus { get; set; }
        public virtual ICollection<UnitStatusHistory> UnitStatusHistories { get; set; }
        public virtual ICollection<SalesReturn> SalesReturn { get; set; }
        public virtual ICollection<MappingInvoiceAndUnit> MappingInvoiceAndUnit { get; set; }
        public virtual ICollection<RMAUnit> RMAUnit { get; set; }
    }
}
