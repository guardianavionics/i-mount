//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnitStatusHistory
    {
        public int Id { get; set; }
        public int UnitSerialNumberId { get; set; }
        public int StatusId { get; set; }
        public System.DateTime DateOfStatusChanged { get; set; }
    
        public virtual UnitStatus UnitStatus { get; set; }
        public virtual UnitSerialNumber UnitSerialNumber { get; set; }
    }
}
