//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnitStatus
    {
        public UnitStatus()
        {
            this.UnitStatusHistory = new HashSet<UnitStatusHistory>();
            this.UnitSerialNumbers = new HashSet<UnitSerialNumber>();
            this.StatusRMA = new HashSet<StatusRMA>();
        }
    
        public int Id { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
    
        public virtual ICollection<UnitStatusHistory> UnitStatusHistory { get; set; }
        public virtual ICollection<UnitSerialNumber> UnitSerialNumbers { get; set; }
        public virtual ICollection<StatusRMA> StatusRMA { get; set; }
    }
}
