//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class KitAssembly
    {
        public KitAssembly()
        {
            this.KitAssemblyItem = new HashSet<KitAssemblyItem>();
        }
    
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string KitImage { get; set; }
        public Nullable<decimal> SuggestedSalesPrice { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string Comment { get; set; }
    
        public virtual ICollection<KitAssemblyItem> KitAssemblyItem { get; set; }
    }
}
