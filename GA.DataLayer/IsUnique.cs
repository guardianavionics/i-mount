﻿using GA.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GA.DataTransfer;

namespace GA.DataLayer
{
    public class IsUnique : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext vContext)
        {
            if (value != null)
            {
                var context = new GuardianAvionicsEntities();
                UpdateProfileViewModel obj = validationContext.ObjectInstance as AircraftProfileModelWeb;

                if (context.AircraftProfiles.Any(ap => ap.Registration == value))
                    return new ValidationResult("Duplicate registration number.");

            }
            return ValidationResult.Success;
        }

    }
}
