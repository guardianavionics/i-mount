//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GA.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChartDetail
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int ChartTypeId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }
        public string Format { get; set; }
        public Nullable<System.DateTime> ExpDate { get; set; }
        public Nullable<System.DateTime> Expiration { get; set; }
        public string Provider { get; set; }
        public string ShortProvider { get; set; }
        public string Copyright { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string LayerType { get; set; }
        public string ShortLayerType { get; set; }
        public string LayerSort { get; set; }
        public Nullable<int> MinZoom { get; set; }
        public Nullable<int> MaxZoom { get; set; }
        public string Bounds { get; set; }
        public string FileName { get; set; }
        public Nullable<decimal> FileSize { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public string Password { get; set; }
    
        public virtual ChartTypeMaster ChartTypeMaster { get; set; }
        public virtual Country Country { get; set; }
        public virtual State State { get; set; }
    }
}
