﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;

namespace DownloadHandler
{
    public class ZipHandler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {

            string fileName = context.Request.Url.AbsoluteUri.Split('/')[context.Request.Url.AbsoluteUri.Split('/').Length - 1];
            string relativeFilePath = string.Format("~/MapFiles/{0}", fileName);
            string filePath = context.Server.MapPath(relativeFilePath);

            try
            {

                switch (context.Request.HttpMethod.ToUpper())
                { //support Get and head method
                    case "GET":
                    case "HEAD":
                        break;
                    default:
                        context.Response.StatusCode = 501;
                        return;
                }
                if (!File.Exists(filePath))
                {
                    context.Response.StatusCode = 404;
                    return;
                }


                long startBytes = 0;
                int packetSize = 1024 * 10; //read in block，every block 10K bytes
                FileStream myFile = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                BinaryReader br = new BinaryReader(myFile);
                long fileLength = myFile.Length;

                //int sleep = (int)Math.Ceiling(1000.0 * packetSize / speed);//the number of millisecond
                string lastUpdateTiemStr = File.GetLastWriteTimeUtc(filePath).ToString("r");
                string eTag = HttpUtility.UrlEncode(fileName, Encoding.UTF8) + lastUpdateTiemStr;

                //validate whether the file is too large
                if (myFile.Length > Int32.MaxValue)
                {
                    context.Response.StatusCode = 413;
                    return;
                }

                if (context.Request.Headers["If-Range"] != null)
                {

                    if (context.Request.Headers["If-Range"].Replace("\"", "") != eTag)
                    {
                        context.Response.StatusCode = 412;
                        return;
                    }
                }
                if (context.Request.Headers["Range"] != null)
                {
                    context.Response.StatusCode = 206;
                    string[] range = context.Request.Headers["Range"].Split(new char[] { '=', '-' });
                    startBytes = Convert.ToInt64(range[1]);
                    if (startBytes < 0 || startBytes >= fileLength)
                    {
                        return;
                    }
                }         
                try
                {

                    context.Response.Clear();
                    context.Response.Buffer = false;
                    context.Response.AddHeader("Content-MD5", GetMD5Hash(myFile));
                    context.Response.AddHeader("Accept-Ranges", "bytes");
                    context.Response.AppendHeader("ETag", "\"" + eTag + "\"");
                    context.Response.AppendHeader("Last-Modified", lastUpdateTiemStr);
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                    context.Response.AddHeader("Content-Length", (fileLength - startBytes).ToString());
                    context.Response.AddHeader("Connection", "Keep-Alive");
                    context.Response.ContentEncoding = Encoding.UTF8;

                    if (startBytes > 0)
                    {
                        context.Response.AddHeader("Content-Range", string.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength));
                    }

                    //send data
                    br.BaseStream.Seek(startBytes, SeekOrigin.Begin);
                    int maxCount = (int)Math.Ceiling((fileLength - startBytes + 0.0) / packetSize);//download in block
                    for (int i = 0; i < maxCount && context.Response.IsClientConnected; i++)
                    {
                        context.Response.BinaryWrite(br.ReadBytes(packetSize));
                        context.Response.Flush();

                        //if (sleep > 1) Thread.Sleep(sleep);
                    }
                }
                catch
                {
                    return;
                }
                finally
                {
                    br.Close();
                    myFile.Close();
                }
            }
            catch
            {
                return;
            }


        }

        public string GetMD5Hash(FileStream stream)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(stream);
            byte[] hash = md5.Hash;
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.Append(string.Format("{0:X2}", b));
            }
            return sb.ToString();
        }

        #endregion
    }
}

#region Old Code
    // **************************************************

    //System.IO.Stream oStream = null;

    //try
    //{
    //    // Open the file
    //    oStream =
    //        new System.IO.FileStream
    //            (path: strPathName,
    //            mode: System.IO.FileMode.Open,
    //            share: System.IO.FileShare.Read,
    //            access: System.IO.FileAccess.Read);

    //    // **************************************************
    //    context.Response.Buffer = false;

    //    // Setting the unknown [ContentType]
    //    // will display the saving dialog for the user
    //    context.Response.ContentType = "application/octet-stream";

    //    // With setting the file name,
    //    // in the saving dialog, user will see
    //    // the [strFileName] name instead of [download]!
    //    context.Response.AddHeader("Content-Disposition", "attachment; filename=test-" + strFileName);

    //    long lngFileLength = oStream.Length;

    //    // Notify user (client) the total file length
    //    context.Response.AddHeader("Content-Length", lngFileLength.ToString());
    //    // **************************************************

    //    // Total bytes that should be read
    //    long lngDataToRead = lngFileLength;

    //    // Read the bytes of file
    //    while (lngDataToRead > 0)
    //    {
    //        // The below code is just for testing! So we commented it!
    //        //System.Threading.Thread.Sleep(200);

    //        // Verify that the client is connected or not?
    //        if (context.Response.IsClientConnected)
    //        {
    //            // 8KB
    //            int intBufferSize = 8 * 1024;

    //            // Create buffer for reading [intBufferSize] bytes from file
    //            byte[] bytBuffers =
    //                new System.Byte[intBufferSize];

    //            // Read the data and put it in the buffer.
    //            int intTheBytesThatReallyHasBeenReadFromTheStream =
    //                oStream.Read(buffer: bytBuffers, offset: 0, count: intBufferSize);

    //            // Write the data from buffer to the current output stream.
    //            context.Response.OutputStream.Write
    //                (buffer: bytBuffers, offset: 0,
    //                count: intTheBytesThatReallyHasBeenReadFromTheStream);

    //            // Flush (Send) the data to output
    //            // (Don't buffer in server's RAM!)
    //            context.Response.Flush();

    //            lngDataToRead =
    //                lngDataToRead - intTheBytesThatReallyHasBeenReadFromTheStream;
    //        }
    //        else
    //        {
    //            // Prevent infinite loop if user disconnected!
    //            lngDataToRead = -1;
    //        }
    //    }
    //}
    //catch { }
    //finally
    //{
    //    if (oStream != null)
    //    {
    //        //Close the file.
    //        oStream.Close();
    //        oStream.Dispose();
    //        oStream = null;
    //    }
    //    context.Response.Close();
    //}                   
#endregion
