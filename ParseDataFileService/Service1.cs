﻿using System.ServiceProcess;
using System.Text;
using System;
using System.Threading;

using NLog;
using Newtonsoft.Json.Linq;

namespace ParseDataFileService
{
    public partial class Service1 : ServiceBase
    {
        // Timer timer = new Timer();
        private Timer parseDataTimer = null;
        private readonly int TimerInterval = 60000;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public Service1()
        {
            InitializeComponent();
            //Debug 01  UnComment Below Statement
            OnStart(new string[0]);
        }

        protected override void OnStart(string[] args)
        {
            int startin = 60 - DateTime.Now.Second;
            int dueTime = startin * 1000;
            SetProcessingTimer(dueTime);
        }

        protected override void OnStop()
        {
        }

        public void SetProcessingTimer(int dueTime)
        {
            TimerCallback timerDelegate = new TimerCallback(ProcessItems);
            //Debug 02 UnComment below statement
            //ProcessItems(new Object());

            // Create a timer that signals the delegate to invoke 
            // extracting, its a one click timer
            parseDataTimer = new Timer(timerDelegate, null, dueTime, TimerInterval);
        }

        private void StopProcessingTimer()
        {
            parseDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void StartProcessingTimer(int timerInterval)
        {
            parseDataTimer.Change(timerInterval, timerInterval);
        }

        public void ProcessItems(Object stateInfo)
        {
            try
            {
                System.Diagnostics.Debugger.Launch();
                // Debug 03  Comment below statement
                StopProcessingTimer();
                GA.CommonForParseDataFile.Common objCommon = new GA.CommonForParseDataFile.Common();
                objCommon.SetLiveDataFlightStatus();
                ParseDataFile parseDataFile = new ParseDataFile();
                parseDataFile.ParseDataFiles();
                StartProcessingTimer(TimerInterval);
            }
            catch (Exception ex)
            {
                logger.Fatal("Exception : Service Catch Block " + Newtonsoft.Json.JsonConvert.SerializeObject(ex));
                SetProcessingTimer(TimerInterval);
            }
        }

    }
}
