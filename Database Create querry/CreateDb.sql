USE [GuardianAvionicsPhase2]
GO
/****** Object:  Table [dbo].[RatingTypes]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RatingTypes](
	[Name] [nvarchar](1000) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SecurityQuestions]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SecurityQuestions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SecurityQuestion] [varchar](1500) NOT NULL,
 CONSTRAINT [PK_SecurityQuestions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Measurement]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Measurement](
	[Id] [int] NOT NULL,
	[MeasurementName] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Measurement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MedicalCertClass]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCertClass](
	[Name] [nvarchar](1000) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Engine]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Engine](
	[Name] [nvarchar](1000) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Url] [nvarchar](1000) NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ProfileId] [int] NOT NULL,
	[IsForAll] [bit] NOT NULL,
 CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AircraftReferenceFile]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AircraftReferenceFile](
	[MFR-MDL-CODE] [nvarchar](400) NULL,
	[ManufacturerName] [nvarchar](400) NULL,
	[ModelName] [nvarchar](400) NULL,
	[NumberOfEngine] [nvarchar](400) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_AircraftReferenceFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AircraftMasterFile]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AircraftMasterFile](
	[NNumber] [nvarchar](400) NULL,
	[SERIALNUMBER] [nvarchar](400) NULL,
	[MFR_MDL_CODE] [nvarchar](400) NULL,
	[ManufactureYear] [nvarchar](400) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_AircraftMasterFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AircraftManufacturer]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AircraftManufacturer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AircraftManufacturer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MapFiles]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapFiles](
	[Name] [nvarchar](1000) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Url] [nvarchar](1000) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_MapFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Propeller]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Propeller](
	[Name] [nvarchar](1000) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailId] [nvarchar](400) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[IsBlocked] [bit] NOT NULL,
 CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Preferences]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preferences](
	[DefaultAirportIdentifier] [nvarchar](400) NULL,
	[DistanceUnit] [nvarchar](100) NOT NULL,
	[SpeedUnit] [nvarchar](50) NOT NULL,
	[VerticalSpeedUnit] [nvarchar](50) NOT NULL,
	[FuelUnit] [nvarchar](50) NOT NULL,
	[WeightUnit] [nvarchar](50) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[DropBoxToken] [nvarchar](1000) NULL,
	[DropBoxTokenSecret] [nvarchar](1000) NULL,
	[IsDropboxSync] [bit] NULL,
 CONSTRAINT [PK_Preference] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Licenses]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Licenses](
	[Type] [nvarchar](200) NULL,
	[ValidUntil] [datetime] NULL,
	[ReferenceNumber] [nvarchar](400) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[UniqueId] [bigint] NULL,
 CONSTRAINT [PK_License] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PushNotifications]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PushNotifications](
	[TokenId] [varchar](1000) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[NumberOfTimeFailed] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsValid] [bit] NOT NULL,
 CONSTRAINT [PK_PushNotifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MedicalCertificates]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCertificates](
	[Type] [nvarchar](200) NULL,
	[ValidUntil] [datetime] NULL,
	[ReferenceNumber] [nvarchar](400) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[UniqueId] [bigint] NULL,
 CONSTRAINT [PK_MedicalCertificates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MeasurementUnit]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MeasurementUnit](
	[Id] [int] NOT NULL,
	[MeasurementUnitName] [varchar](200) NOT NULL,
	[MesurementId] [int] NOT NULL,
 CONSTRAINT [PK_MeasurementUnit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AircraftModelList]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AircraftModelList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelName] [varchar](100) NOT NULL,
	[AircraftManufacturerId] [int] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AircraftModels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserEmails]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserEmails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[EmailId] [nvarchar](100) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_UserEmails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDetails](
	[FirstName] [nvarchar](400) NULL,
	[LastName] [nvarchar](400) NULL,
	[StreetAddress] [nvarchar](400) NULL,
	[City] [nvarchar](400) NULL,
	[State] [nvarchar](400) NULL,
	[ZipCode] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[ImageUrl] [nvarchar](600) NULL,
	[SecurityAnswer] [nvarchar](600) NULL,
	[ProfileId] [int] NOT NULL,
	[SecurityQuestion] [nvarchar](650) NULL,
	[CompanyName] [nvarchar](400) NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ResetPasswords]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResetPasswords](
	[Id] [int] NOT NULL,
	[ProfileId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[UniqueCode] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_ResetPasswords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[Type] [nvarchar](200) NULL,
	[ValidUntil] [datetime] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LicenseId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[UniqueId] [bigint] NULL,
 CONSTRAINT [PK_Ratings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AircraftProfiles]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AircraftProfiles](
	[Registration] [nvarchar](400) NULL,
	[Make] [int] NOT NULL,
	[Model] [int] NOT NULL,
	[Color] [nvarchar](200) NULL,
	[HomeBase] [nvarchar](400) NULL,
	[Capacity] [nvarchar](50) NULL,
	[TaxiFuel] [nvarchar](50) NULL,
	[AdditionalFuel] [nvarchar](50) NULL,
	[FuelUnit] [nvarchar](50) NULL,
	[WeightUnit] [nvarchar](50) NULL,
	[SpeedUnit] [nvarchar](50) NULL,
	[VerticalSpeed] [nvarchar](50) NULL,
	[Power] [float] NULL,
	[RPM] [float] NULL,
	[MP] [float] NULL,
	[Altitude] [float] NULL,
	[FuelBurnCruise] [float] NULL,
	[TASCruise] [float] NULL,
	[RateOfClimb] [float] NULL,
	[FuelBurnRateClimb] [float] NULL,
	[TASClimb] [float] NULL,
	[RateOfDescent] [float] NULL,
	[FuelBurnRateOfDescent] [float] NULL,
	[TASDescent] [float] NULL,
	[IsAmericaAircraft] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Deleted] [bit] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImageUrl] [nvarchar](600) NULL,
	[EngineType] [nvarchar](400) NULL,
	[AeroUnitNo] [nvarchar](600) NULL,
	[AircraftType] [nvarchar](400) NULL,
	[AircraftSerialNo] [nvarchar](600) NULL,
	[AircraftYear] [int] NULL,
	[HobbsTime] [float] NULL,
	[HobbsTimeOffset] [float] NULL,
	[TachTime] [float] NULL,
	[TachTimeOffset] [float] NULL,
	[EngineMFGType] [nvarchar](400) NULL,
	[EngineTBO] [float] NULL,
	[Engine1LastMOHDate] [datetime2](7) NULL,
	[Engine1TimeSinceOH] [float] NULL,
	[PropMFG] [nvarchar](400) NULL,
	[Prop1TBO] [int] NULL,
	[Prop1OHDueDate] [datetime2](7) NULL,
	[Prop1Time] [float] NULL,
	[Engine2LastMOHDate] [datetime2](7) NULL,
	[Engine2TimeSinceOH] [float] NULL,
	[Prop2OHDueDate] [datetime2](7) NULL,
	[Prop2Time] [float] NULL,
	[UniqueId] [bigint] NULL,
	[EngineMonitor] [bit] NOT NULL,
	[Comm] [bit] NOT NULL,
	[Transponder] [bit] NOT NULL,
 CONSTRAINT [PK_AircraftProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PilotLogs]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PilotLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AircraftId] [int] NOT NULL,
	[ProfileId] [int] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Actual] [nvarchar](400) NULL,
	[CoPilot] [nvarchar](400) NULL,
	[CrossCountry] [nvarchar](400) NULL,
	[Date] [datetime] NOT NULL,
	[DayPIC] [nvarchar](400) NOT NULL,
	[Hood] [nvarchar](400) NULL,
	[NightPIC] [nvarchar](400) NULL,
	[Remark] [nvarchar](1000) NULL,
	[Route] [nvarchar](1000) NOT NULL,
	[Sim] [nvarchar](400) NULL,
	[Finished] [bit] NOT NULL,
	[IFRAppchs] [nvarchar](400) NULL,
	[IsSavedOnDropbox] [bit] NULL,
	[UniqeId] [bigint] NULL,
 CONSTRAINT [PK_PilotLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PilotAircraftMapping]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PilotAircraftMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[AircraftId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PilotAircraftMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AeroUnitMaster]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AeroUnitMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AeroUnitNo] [nvarchar](50) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](250) NULL,
	[AircraftId] [int] NULL,
 CONSTRAINT [PK_AeroUnitMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MappingAircraftAndPilot]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MappingAircraftAndPilot](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[AircraftId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_MappingAircraftAndPilot] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MappingAircraftAndPilot', @level2type=N'COLUMN',@level2name=N'Deleted'
GO
/****** Object:  Table [dbo].[UnitDatas]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UnitDatas](
	[AccountNumber] [varchar](400) NULL,
	[UnitSerialNumber] [nvarchar](400) NULL,
	[AircraftId] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[PilotLogId] [int] NOT NULL,
 CONSTRAINT [PK_UnitDatas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JPIUnitDatas]    Script Date: 10/13/2014 11:46:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JPIUnitDatas](
	[IAS] [float] NULL,
	[Volt1] [float] NULL,
	[Volt2] [float] NULL,
	[AMP1] [float] NULL,
	[AMP2] [float] NULL,
	[FQL] [float] NULL,
	[FQR] [float] NULL,
	[FF] [float] NULL,
	[OilP] [float] NULL,
	[OilT] [float] NULL,
	[MAP] [float] NULL,
	[RPM] [float] NULL,
	[Cht1] [float] NULL,
	[Cht2] [float] NULL,
	[Cht3] [float] NULL,
	[Cht4] [float] NULL,
	[Cht5] [float] NULL,
	[Cht6] [float] NULL,
	[Egt1] [float] NULL,
	[Egt2] [float] NULL,
	[Egt3] [float] NULL,
	[Egt4] [float] NULL,
	[Egt5] [float] NULL,
	[Egt6] [float] NULL,
	[Tit] [float] NULL,
	[Oat] [float] NULL,
	[DateTime] [datetime] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnitDataId] [int] NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[WayPoint] [nvarchar](400) NULL,
	[GpsAltitude] [float] NULL,
	[AltB] [float] NULL,
	[BravoA] [float] NULL,
	[AltMSL] [float] NULL,
	[GndSpd] [float] NULL,
	[VSpd] [float] NULL,
	[Pitch] [float] NULL,
	[Roll] [float] NULL,
	[LatAc] [float] NULL,
	[NormAc] [float] NULL,
	[HDG] [float] NULL,
	[TRK] [float] NULL,
	[HSIS] [float] NULL,
	[CRS] [float] NULL,
	[NAV1] [float] NULL,
	[NAV2] [float] NULL,
	[COM1] [float] NULL,
	[COM2] [float] NULL,
	[HCDI] [float] NULL,
	[VCDI] [float] NULL,
	[WndSpd] [float] NULL,
	[WndDr] [float] NULL,
	[WptDst] [float] NULL,
	[WptBrg] [float] NULL,
	[MagVar] [float] NULL,
	[AfcsOn] [float] NULL,
	[RollM] [float] NULL,
	[PitchM] [float] NULL,
	[RollC] [float] NULL,
	[PitchC] [float] NULL,
	[VSpdG] [float] NULL,
	[GPSfix] [float] NULL,
	[HAL] [float] NULL,
	[VAL] [float] NULL,
	[HPLwas] [float] NULL,
	[HPLft] [float] NULL,
	[VPLwas] [float] NULL,
	[UTCOfst] [datetime] NULL,
	[TAS] [float] NULL,
 CONSTRAINT [PK_JPIUnitDatas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_AeroUnitMaster_Deleted]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AeroUnitMaster] ADD  CONSTRAINT [DF_AeroUnitMaster_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF_AircraftProfiles_EngineMonitor]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AircraftProfiles] ADD  CONSTRAINT [DF_AircraftProfiles_EngineMonitor]  DEFAULT ((1)) FOR [EngineMonitor]
GO
/****** Object:  Default [DF_AircraftProfiles_Comm]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AircraftProfiles] ADD  CONSTRAINT [DF_AircraftProfiles_Comm]  DEFAULT ((1)) FOR [Comm]
GO
/****** Object:  Default [DF_AircraftProfiles_Transponder]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AircraftProfiles] ADD  CONSTRAINT [DF_AircraftProfiles_Transponder]  DEFAULT ((1)) FOR [Transponder]
GO
/****** Object:  Default [DF_MappingAircraftAndPilot_Deleted]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[MappingAircraftAndPilot] ADD  CONSTRAINT [DF_MappingAircraftAndPilot_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF_PilotAircraftMapping_Deleted]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PilotAircraftMapping] ADD  CONSTRAINT [DF_PilotAircraftMapping_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF_PushNotifications_IsValid]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PushNotifications] ADD  CONSTRAINT [DF_PushNotifications_IsValid]  DEFAULT ((0)) FOR [IsValid]
GO
/****** Object:  Default [DF_UserEmails_IsEnabled]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[UserEmails] ADD  CONSTRAINT [DF_UserEmails_IsEnabled]  DEFAULT ((0)) FOR [IsEnabled]
GO
/****** Object:  ForeignKey [FK_AeroUnitMaster_AircraftProfiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AeroUnitMaster]  WITH CHECK ADD  CONSTRAINT [FK_AeroUnitMaster_AircraftProfiles] FOREIGN KEY([AircraftId])
REFERENCES [dbo].[AircraftProfiles] ([Id])
GO
ALTER TABLE [dbo].[AeroUnitMaster] CHECK CONSTRAINT [FK_AeroUnitMaster_AircraftProfiles]
GO
/****** Object:  ForeignKey [FK_AircraftModelList_AircraftManufacturer]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AircraftModelList]  WITH CHECK ADD  CONSTRAINT [FK_AircraftModelList_AircraftManufacturer] FOREIGN KEY([AircraftManufacturerId])
REFERENCES [dbo].[AircraftManufacturer] ([Id])
GO
ALTER TABLE [dbo].[AircraftModelList] CHECK CONSTRAINT [FK_AircraftModelList_AircraftManufacturer]
GO
/****** Object:  ForeignKey [FK_AircraftProfiles_AircraftModelList]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[AircraftProfiles]  WITH CHECK ADD  CONSTRAINT [FK_AircraftProfiles_AircraftModelList] FOREIGN KEY([Model])
REFERENCES [dbo].[AircraftModelList] ([Id])
GO
ALTER TABLE [dbo].[AircraftProfiles] CHECK CONSTRAINT [FK_AircraftProfiles_AircraftModelList]
GO
/****** Object:  ForeignKey [FK_JPIUnitDatas_UnitDatas]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[JPIUnitDatas]  WITH CHECK ADD  CONSTRAINT [FK_JPIUnitDatas_UnitDatas] FOREIGN KEY([UnitDataId])
REFERENCES [dbo].[UnitDatas] ([Id])
GO
ALTER TABLE [dbo].[JPIUnitDatas] CHECK CONSTRAINT [FK_JPIUnitDatas_UnitDatas]
GO
/****** Object:  ForeignKey [FK_License_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[Licenses]  WITH CHECK ADD  CONSTRAINT [FK_License_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[Licenses] CHECK CONSTRAINT [FK_License_Profiles]
GO
/****** Object:  ForeignKey [FK_MappingAircraftAndPilot_AircraftProfiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[MappingAircraftAndPilot]  WITH CHECK ADD  CONSTRAINT [FK_MappingAircraftAndPilot_AircraftProfiles] FOREIGN KEY([AircraftId])
REFERENCES [dbo].[AircraftProfiles] ([Id])
GO
ALTER TABLE [dbo].[MappingAircraftAndPilot] CHECK CONSTRAINT [FK_MappingAircraftAndPilot_AircraftProfiles]
GO
/****** Object:  ForeignKey [FK_MappingAircraftAndPilot_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[MappingAircraftAndPilot]  WITH CHECK ADD  CONSTRAINT [FK_MappingAircraftAndPilot_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[MappingAircraftAndPilot] CHECK CONSTRAINT [FK_MappingAircraftAndPilot_Profiles]
GO
/****** Object:  ForeignKey [FK_MeasurementUnit_Measurement]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[MeasurementUnit]  WITH CHECK ADD  CONSTRAINT [FK_MeasurementUnit_Measurement] FOREIGN KEY([MesurementId])
REFERENCES [dbo].[Measurement] ([Id])
GO
ALTER TABLE [dbo].[MeasurementUnit] CHECK CONSTRAINT [FK_MeasurementUnit_Measurement]
GO
/****** Object:  ForeignKey [FK_MedicalCertificates_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[MedicalCertificates]  WITH CHECK ADD  CONSTRAINT [FK_MedicalCertificates_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[MedicalCertificates] CHECK CONSTRAINT [FK_MedicalCertificates_Profiles]
GO
/****** Object:  ForeignKey [FK_PilotAircraftMapping_AircraftProfiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PilotAircraftMapping]  WITH CHECK ADD  CONSTRAINT [FK_PilotAircraftMapping_AircraftProfiles] FOREIGN KEY([AircraftId])
REFERENCES [dbo].[AircraftProfiles] ([Id])
GO
ALTER TABLE [dbo].[PilotAircraftMapping] CHECK CONSTRAINT [FK_PilotAircraftMapping_AircraftProfiles]
GO
/****** Object:  ForeignKey [FK_PilotAircraftMapping_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PilotAircraftMapping]  WITH CHECK ADD  CONSTRAINT [FK_PilotAircraftMapping_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[PilotAircraftMapping] CHECK CONSTRAINT [FK_PilotAircraftMapping_Profiles]
GO
/****** Object:  ForeignKey [FK_PilotLogs_AircraftProfiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PilotLogs]  WITH CHECK ADD  CONSTRAINT [FK_PilotLogs_AircraftProfiles] FOREIGN KEY([AircraftId])
REFERENCES [dbo].[AircraftProfiles] ([Id])
GO
ALTER TABLE [dbo].[PilotLogs] CHECK CONSTRAINT [FK_PilotLogs_AircraftProfiles]
GO
/****** Object:  ForeignKey [FK_PilotLogs_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PilotLogs]  WITH CHECK ADD  CONSTRAINT [FK_PilotLogs_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[PilotLogs] CHECK CONSTRAINT [FK_PilotLogs_Profiles]
GO
/****** Object:  ForeignKey [FK_Preference_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[Preferences]  WITH CHECK ADD  CONSTRAINT [FK_Preference_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[Preferences] CHECK CONSTRAINT [FK_Preference_Profiles]
GO
/****** Object:  ForeignKey [FK_PushNotifications_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[PushNotifications]  WITH CHECK ADD  CONSTRAINT [FK_PushNotifications_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[PushNotifications] CHECK CONSTRAINT [FK_PushNotifications_Profiles]
GO
/****** Object:  ForeignKey [FK_Ratings_Licenses]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_Licenses] FOREIGN KEY([LicenseId])
REFERENCES [dbo].[Licenses] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_Licenses]
GO
/****** Object:  ForeignKey [FK_ResetPasswords_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[ResetPasswords]  WITH CHECK ADD  CONSTRAINT [FK_ResetPasswords_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[ResetPasswords] CHECK CONSTRAINT [FK_ResetPasswords_Profiles]
GO
/****** Object:  ForeignKey [FK_UnitDatas_PilotLogs]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[UnitDatas]  WITH CHECK ADD  CONSTRAINT [FK_UnitDatas_PilotLogs] FOREIGN KEY([PilotLogId])
REFERENCES [dbo].[PilotLogs] ([Id])
GO
ALTER TABLE [dbo].[UnitDatas] CHECK CONSTRAINT [FK_UnitDatas_PilotLogs]
GO
/****** Object:  ForeignKey [FK_UnitDatas_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[UnitDatas]  WITH CHECK ADD  CONSTRAINT [FK_UnitDatas_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[UnitDatas] CHECK CONSTRAINT [FK_UnitDatas_Profiles]
GO
/****** Object:  ForeignKey [FK_UserDetails_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[UserDetails]  WITH CHECK ADD  CONSTRAINT [FK_UserDetails_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[UserDetails] CHECK CONSTRAINT [FK_UserDetails_Profiles]
GO
/****** Object:  ForeignKey [FK_UserEmails_Profiles]    Script Date: 10/13/2014 11:46:20 ******/
ALTER TABLE [dbo].[UserEmails]  WITH CHECK ADD  CONSTRAINT [FK_UserEmails_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO
ALTER TABLE [dbo].[UserEmails] CHECK CONSTRAINT [FK_UserEmails_Profiles]
GO
