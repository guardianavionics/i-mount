﻿using Amazon.S3.IO;
using Amazon.S3;

using Amazon.S3.Model;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime.SharedInterfaces;
using System.Threading;
using GA.Common;
using GA.DataTransfer;
using Ionic.Zip;

namespace GA.AWS
{
    public class AWS : ICoreAmazonS3
    {
        public GeneralResponse DeleteFileOrDirectory(string path, bool isFolder)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                AmazonS3Client s3Client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);
                if (isFolder)
                {
                    path = path.Replace("/", "\\");
                    S3DirectoryInfo dir = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, path);
                    dir.Delete(true);
                }
                else
                {
                    S3FileInfo file = new S3FileInfo(s3Client, ConfigurationReader.S3Bucket, path);
                    file.Delete();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Success).ToString();
            response.ResponseMessage = (isFolder ? "Folder" : "File") + " deleted successfully.";
            return response;
        }
        public void RenameS3Directory(string sourceFolderPath, string destinationFolderName)
        {
            //Rename folder method will move all files and folder from source to destination folder
            AmazonS3Client s3Client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);

            List<string> folderPathList = sourceFolderPath.Split('/').ToList();
            folderPathList[folderPathList.Count - 1] = destinationFolderName;

            //To create folder path contains "/" 
            //To move folder and file path contains "\"
            PutObjectRequest folderRequest = new PutObjectRequest();
            folderRequest.BucketName = ConfigurationReader.S3Bucket;
            string folderKey = string.Join("/", folderPathList) + "/";
            folderRequest.Key = folderKey;
            folderRequest.InputStream = new MemoryStream(new byte[0]);
            PutObjectResponse folderResponse = s3Client.PutObject(folderRequest);

            string bucketName = ConfigurationReader.S3Bucket;
            sourceFolderPath = sourceFolderPath.Replace("/", "\\");
            S3DirectoryInfo source = new S3DirectoryInfo(s3Client, bucketName, sourceFolderPath);
            S3DirectoryInfo destination = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, "");
            //  S3DirectoryInfo asas = source.CopyTo(destination);
            // or 
            S3DirectoryInfo[] dirList = source.GetDirectories();
            S3FileInfo[] fileList = source.GetFiles();


            List<string> arrsourcePath = sourceFolderPath.Split('\\').ToList();
            arrsourcePath[arrsourcePath.Count - 1] = destinationFolderName;
            string DestinationFolderPath = String.Join(@"\", arrsourcePath);

            foreach (var file in fileList)
            {
                file.MoveTo(ConfigurationReader.S3Bucket, DestinationFolderPath + "\\");
                file.Delete();
            }

            foreach (var dir in dirList)
            {
                dir.MoveTo(ConfigurationReader.S3Bucket, DestinationFolderPath);
                dir.Delete();
            }
            source.Delete();
        }

        public void AddSubFoldersInRootOfEngDocs(string sourcePath, string destinationPath)
        {
            sourcePath = sourcePath.Replace('/', '\\');
            destinationPath = destinationPath.Replace('/', '\\');

            //List<string> directoryList = new List<string>();
            //directoryList.Add("Customer");
            //directoryList.Add("Image");
            //directoryList.Add("Production");
            //directoryList.Add("Marketing");
            //directoryList.Add("Engineering");

            AmazonS3Client client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);
            S3DirectoryInfo source = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket, "");
            S3DirectoryInfo destination = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket, "");
            source = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket, sourcePath + @"\" );
            destination = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket, destinationPath + @"\");
            source.CopyTo(destination);
            //foreach (var dir in directoryList)
            //{
            //    source = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket, sourcePath + @"\" + dir);
            //    destination = new S3DirectoryInfo(client, ConfigurationReader.S3Bucket,   destinationPath + @"\");
            //    source.CopyTo(destination);
            //}
        }

        public void CreateFolderOns3(string folderName, string folderPath)
        {
            AmazonS3Client client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);

            PutObjectRequest folderRequest = new PutObjectRequest();

            folderRequest.BucketName = ConfigurationReader.S3Bucket;
            string folderKey = "";
            if (string.IsNullOrEmpty(folderPath))
            {
                folderKey = new Misc().GetS3FolderName("EngineeringDocs") + folderName + "/";
            }
            else
            {
                folderKey = new Misc().GetS3FolderName("EngineeringDocs") + folderPath + "/" + folderName + "/";
            }

            folderRequest.Key = folderKey;
            folderRequest.InputStream = new MemoryStream(new byte[0]);
            PutObjectResponse folderResponse = client.PutObject(folderRequest);
        }


        public void CreateFolderOns3ForGAV(string folderName, string folderPath)
        {
            AmazonS3Client client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);

            PutObjectRequest folderRequest = new PutObjectRequest();

            folderRequest.BucketName = ConfigurationReader.S3Bucket;
            string folderKey = "";
            if (string.IsNullOrEmpty(folderPath))
            {
                folderKey = new Misc().GetS3FolderName("GAVDocs") + folderName + "/";
            }
            else
            {
                folderKey = new Misc().GetS3FolderName("GAVDocs") + folderPath + "/" + folderName + "/";
            }

            folderRequest.Key = folderKey;
            folderRequest.InputStream = new MemoryStream(new byte[0]);
            PutObjectResponse folderResponse = client.PutObject(folderRequest);
        }


        public void RenameFile(string directoryPath, string oldfileName, string newFileName)
        {

            AmazonS3Client s3 = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);

            CopyObjectRequest copyRequest = new CopyObjectRequest();
            copyRequest.SourceBucket = ConfigurationReader.S3Bucket;
            copyRequest.SourceKey = directoryPath + "/" + oldfileName;      //"GuardianTest/EngineeringDocs/A.txt";
            copyRequest.DestinationBucket = ConfigurationReader.S3Bucket;
            copyRequest.DestinationKey = directoryPath + "/" + newFileName; //"GuardianTest/EngineeringDocs/AAA.txt";
            copyRequest.CannedACL = S3CannedACL.PublicRead;
            s3.CopyObject(copyRequest);

            // Delete the original
            DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
            deleteRequest.BucketName = ConfigurationReader.S3Bucket;
            deleteRequest.Key = directoryPath + "/" + oldfileName;
            s3.DeleteObject(deleteRequest);
        }

        public GeneralResponse MoveFolder(string sourceDirectoryPath, string destinationDirectoryPath, Dictionary<string, bool> dicDocumentName)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                AmazonS3Client s3Client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);


                CopyObjectRequest copyRequest = new CopyObjectRequest();
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                copyRequest.SourceBucket = ConfigurationReader.S3Bucket;
                copyRequest.DestinationBucket = ConfigurationReader.S3Bucket;
                copyRequest.CannedACL = S3CannedACL.PublicRead;
                deleteRequest.BucketName = ConfigurationReader.S3Bucket;

                foreach (var doc in dicDocumentName.Where(w => w.Value == false))
                {
                    copyRequest.SourceKey = sourceDirectoryPath + "/" + doc.Key;      //"GuardianTest/EngineeringDocs/A.txt";
                    copyRequest.DestinationKey = destinationDirectoryPath + "/" + doc.Key; //"GuardianTest/EngineeringDocs/AAA.txt";

                    s3Client.CopyObject(copyRequest);

                    // Delete the original
                    deleteRequest.Key = sourceDirectoryPath + "/" + doc.Key;
                    s3Client.DeleteObject(deleteRequest);
                }

                S3DirectoryInfo source = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, "");
                S3DirectoryInfo destination = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, "");
                
                sourceDirectoryPath = sourceDirectoryPath.Replace('/', '\\');
                destinationDirectoryPath = destinationDirectoryPath.Replace('/', '\\');
                foreach (var doc in dicDocumentName.Where(w => w.Value == true))
                {
                    source = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, sourceDirectoryPath + @"\" + doc.Key);
                    destination = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, destinationDirectoryPath + @"\");
                    source.MoveTo(destination);
                    source.Delete();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.ResponseCode = ((int)Enumerations.EngineeringDocEnum.Success).ToString();
            response.ResponseMessage = "Folder moved successfully.";
            return response;
        }


        static void addFiles(ZipFile zip, S3DirectoryInfo dirInfo, string archiveDirectory)
        {
            foreach (var childDirs in dirInfo.GetDirectories())
            {
                //Do not include archieve folder
                if (childDirs.Name != "Archives")
                {
                    var entry = zip.AddDirectoryByName(childDirs.Name);
                    addFiles(zip, childDirs, archiveDirectory + entry.FileName);
                }
            }

            foreach (var file in dirInfo.GetFiles())
            {
                using (var stream = file.OpenRead())
                {
                    zip.AddEntry(archiveDirectory + file.Name, stream);

                    // Save after adding the file because to force the 
                    // immediate read from the S3 Stream since 
                    // we don't want to keep that stream open.
                    zip.Save();
                }
            }
        }

        public void CreateZipFromS3Directory(string zipFilePath, string sourceBucketPath)
        {
            AmazonS3Client s3Client = new AmazonS3Client(ConfigurationReader.AmazonAccessKey, ConfigurationReader.AmazonSecretKey, Amazon.RegionEndpoint.USWest2);

            //zipFilePath = @"c:\temp\data.zip";

            //Ex guardianTest/engdoc/101.... Zip file will add all the dir and files if 101 folder

            sourceBucketPath = sourceBucketPath.Replace('/', '\\');
            S3DirectoryInfo rootDir = new S3DirectoryInfo(s3Client, ConfigurationReader.S3Bucket, sourceBucketPath);
            using (var zip = new ZipFile())
            {
                zip.Name = zipFilePath;
                addFiles(zip, rootDir, "");
            }
        }

        void ICoreAmazonS3.Delete(string bucketName, string objectKey, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.DeleteAsync(string bucketName, string objectKey, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.Deletes(string bucketName, IEnumerable<string> objectKeys, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.DeletesAsync(string bucketName, IEnumerable<string> objectKeys, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        bool ICoreAmazonS3.DoesS3BucketExist(string bucketName)
        {
            throw new NotImplementedException();
        }

        Task<bool> ICoreAmazonS3.DoesS3BucketExistAsync(string bucketName)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.DownloadToFilePath(string bucketName, string objectKey, string filepath, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.DownloadToFilePathAsync(string bucketName, string objectKey, string filepath, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.EnsureBucketExists(string bucketName)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.EnsureBucketExistsAsync(string bucketName)
        {
            throw new NotImplementedException();
        }

        string ICoreAmazonS3.GeneratePreSignedURL(string bucketName, string objectKey, DateTime expiration, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        IList<string> ICoreAmazonS3.GetAllObjectKeys(string bucketName, string prefix, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task<IList<string>> ICoreAmazonS3.GetAllObjectKeysAsync(string bucketName, string prefix, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Stream ICoreAmazonS3.GetObjectStream(string bucketName, string objectKey, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task<Stream> ICoreAmazonS3.GetObjectStreamAsync(string bucketName, string objectKey, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.MakeObjectPublic(string bucketName, string objectKey, bool enable)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.MakeObjectPublicAsync(string bucketName, string objectKey, bool enable)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.UploadObjectFromFilePath(string bucketName, string objectKey, string filepath, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.UploadObjectFromFilePathAsync(string bucketName, string objectKey, string filepath, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        void ICoreAmazonS3.UploadObjectFromStream(string bucketName, string objectKey, Stream stream, IDictionary<string, object> additionalProperties)
        {
            throw new NotImplementedException();
        }

        Task ICoreAmazonS3.UploadObjectFromStreamAsync(string bucketName, string objectKey, Stream stream, IDictionary<string, object> additionalProperties, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
